﻿Imports System.Configuration
Imports System.Xml
Imports Camstar.Utility
Imports Camstar.WCF.Services
Imports Camstar.WCF.ObjectStack
Imports Camstar.XMLClient.API
Imports uMES.LeanManufacturing.ParameterDTO

Public Class CamstarClientAPI
    Private gClient As csiClient
    Private gConnection As csiConnection
    Private gSession As csiSession
    Private gDocument As csiDocument
    Private gService As csiService
    Private gSessionID As Guid

    Private Shared ReadOnly gHost As String = ConfigurationSettings.AppSettings("CamstarServerHost")
    Private Shared ReadOnly gPort As String = ConfigurationSettings.AppSettings("CamstarServerPort")
    Private gUserName As String
    Private gPassword As String

#Region "初始化方法"

    Public Sub New(ByVal p_UserName As String, ByVal p_PassWord As String)
        gUserName = p_UserName
        If String.IsNullOrWhiteSpace(p_PassWord) Then
            Dim Doc As XmlDocument = New XmlDocument()
            Doc.Load(System.AppDomain.CurrentDomain.BaseDirectory + "\UserAssociate.xml")
            For Each elem As XmlElement In Doc.SelectSingleNode("/Users").ChildNodes
                If elem.Name <> "User" Then
                    Continue For
                End If
                If elem.GetAttribute("UserName") = p_UserName Then
                    gPassword = elem.GetAttribute("Password")
                    Exit For
                End If
            Next
        Else
            gPassword = p_PassWord
        End If
    End Sub

    Private Sub InitializeSession()
        gSessionID = Guid.NewGuid()
        gClient = Nothing
        gClient = New csiClient()
        gConnection = Nothing
        gConnection = gClient.createConnection(gHost, gPort)
        gSession = gConnection.createSession(gUserName, gPassword, gSessionID.ToString())
    End Sub


    Private Function CheckForErrors(ByVal ResponseDocument As csiDocument) As String
        Dim csiExceptionData As csiExceptionData
        Dim CompletionMsg As csiDataField
        Dim csiService As csiService
        Dim ErrorMsg As String = ""
        CheckForErrors = ""
        If ResponseDocument.checkErrors() = True Then
            Dim returnedObject As csiService

            returnedObject = ResponseDocument.getService()
            If returnedObject Is Nothing Then
                csiExceptionData = ResponseDocument.exceptionData()
            Else
                csiExceptionData = returnedObject.exceptionData()
            End If

            ErrorMsg = String.Format("Error {0}:{1}", csiExceptionData.getErrorCode().ToString, csiExceptionData.getDescription().ToString)

        Else
            csiService = ResponseDocument.getService()
            If csiService IsNot Nothing Then
                CompletionMsg = csiService.responseData().getResponseFieldByName("CompletionMsg")
                CheckForErrors = CompletionMsg.getValue()
            End If
        End If
        ClearDocument(ResponseDocument.getService().serviceTypeName() + "Doc")
        If ErrorMsg.Length > 1 Then
            Throw New Exception(ErrorMsg)
        End If
    End Function

    Private Sub CreateDocumentandService(ByVal DocumentName As String, ByVal ServiceName As String)
        If DocumentName.Length > 0 Then
            gSession.removeDocument(DocumentName)
        End If

        If gService IsNot Nothing Then
            gService = Nothing
        End If
        gDocument = gSession.createDocument(DocumentName)
        If ServiceName.Length > 0 Then
            gService = gDocument.createService(ServiceName)
        End If
    End Sub

    ''' <summary>
    ''' 清理Document
    ''' </summary>
    ''' <param name="strDocName"></param>
    ''' <remarks></remarks>
    Private Sub ClearDocument(ByVal strDocName As String)
        If String.IsNullOrWhiteSpace(strDocName) Then
            Exit Sub
        End If
        If gSession Is Nothing Then
            Exit Sub
        End If
        If gSession.findDocument(strDocName) IsNot Nothing Then
            gSession.removeDocument(strDocName)
        End If
        gSession = Nothing
        If gConnection Is Nothing Then
            Exit Sub
        End If

        If gConnection.findSession(gSessionID.ToString) IsNot Nothing Then
            gConnection.removeSession(gSessionID.ToString)
        End If

        gConnection = Nothing

        If gClient Is Nothing Then
            Exit Sub
        End If
        gClient.removeConnection(gHost, gPort)
        gClient = Nothing

    End Sub

#End Region


#Region ""
    Private Sub SetDetailValue(ByRef p_Details As csiSubentity, ByVal p_DataEntity As ClientAPIEntity)

        If p_DataEntity.ClientDataTypeEnum = DataTypeEnum.DataField Then

            p_Details.dataField(p_DataEntity.ClientDataName).setValue(p_DataEntity.ClientDataValue)

        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField Then

            p_Details.namedObjectField(p_DataEntity.ClientDataName).setRef(p_DataEntity.ClientDataValue)

        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.RevisionedObjectField Then

            p_Details.revisionedObjectField(p_DataEntity.ClientDataName).setRef(p_DataEntity.ClientDataValue, p_DataEntity.ClientDataVersion, p_DataEntity.ClientDataIsROR)

        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.subentityField Then

            p_Details.subentityField(p_DataEntity.ClientDataName).setObjectId(p_DataEntity.ClientDataValue)
        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.NamedSubentityField Then

            p_Details.namedSubentityField(p_DataEntity.ClientDataName).setName(p_DataEntity.ClientDataValue)
        End If
    End Sub

    Private Sub SetStepDetailValue(ByRef p_Details As csiNamedSubentity, ByVal p_DataEntity As ClientAPIEntity)

        If p_DataEntity.ClientDataTypeEnum = DataTypeEnum.DataField Then

            p_Details.dataField(p_DataEntity.ClientDataName).setValue(p_DataEntity.ClientDataValue)

        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField Then

            p_Details.namedObjectField(p_DataEntity.ClientDataName).setRef(p_DataEntity.ClientDataValue)

        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.RevisionedObjectField Then

            p_Details.revisionedObjectField(p_DataEntity.ClientDataName).setRef(p_DataEntity.ClientDataValue, p_DataEntity.ClientDataVersion, p_DataEntity.ClientDataIsROR)

        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.subentityField Then

            p_Details.subentityField(p_DataEntity.ClientDataName).setObjectId(p_DataEntity.ClientDataValue)
        ElseIf p_DataEntity.ClientDataTypeEnum = DataTypeEnum.NamedSubentityField Then

            p_Details.namedSubentityField(p_DataEntity.ClientDataName).setName(p_DataEntity.ClientDataValue)
        End If
    End Sub

    Private Sub SetInputDataValue(ByRef p_Data As csiObject, ByVal p_Entity As ClientAPIEntity)

        If p_Entity.ClientDataTypeEnum = DataTypeEnum.DataField Then
            If p_Entity.ClientDataName = "QtyRequired" Then
                If String.IsNullOrWhiteSpace(p_Entity.ClientDataValue) Then
                    p_Entity.ClientDataValue = 1
                End If
            End If

            p_Data.dataField(p_Entity.ClientDataName).setValue(p_Entity.ClientDataValue)
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField Then
            p_Data.namedObjectField(p_Entity.ClientDataName).setRef(p_Entity.ClientDataValue)
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.RevisionedObjectField Then
            p_Data.revisionedObjectField(p_Entity.ClientDataName).setRef(p_Entity.ClientDataValue, p_Entity.ClientDataVersion, p_Entity.ClientDataIsROR)
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.ContainerField Then
            p_Data.containerField(p_Entity.ClientDataName).setRef(p_Entity.ClientDataValue, p_Entity.ClientOtherValue)
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.subentityField Then
            p_Data.subentityField(p_Entity.ClientDataName).setObjectId(p_Entity.ClientDataValue)
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.NamedSubentityField Then
            p_Data.namedSubentityField(p_Entity.ClientDataName).setName(p_Entity.ClientDataValue)
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.NamedObjectList Then
            If Not String.IsNullOrWhiteSpace(p_Entity.ClientOtherValue) Then
                Dim intLen As Integer = Convert.ToInt32(p_Entity.ClientOtherValue)
                ''删除原来的
                For i As Integer = intLen - 1 To 0 Step -1
                    p_Data.namedObjectList(p_Entity.ClientDataName).deleteItemByIndex(i)
                Next
            End If

            ''添加新的
            Dim strDataList As New Dictionary(Of Integer, String)
            strDataList = p_Entity.ClientDataValue
            For Each itemvalue As String In strDataList.Values
                p_Data.namedObjectList(p_Entity.ClientDataName).appendItem(itemvalue)
            Next
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.RevisionedObjectList Then
            If Not String.IsNullOrWhiteSpace(p_Entity.ClientOtherValue) Then
                Dim intLen As Integer = Convert.ToInt32(p_Entity.ClientOtherValue)
                ''删除原来的
                For i As Integer = intLen - 1 To 0 Step -1
                    p_Data.revisionedObjectList(p_Entity.ClientDataName).deleteItemByIndex(i)
                Next
            End If

            ''添加新的
            Dim strDataList As New Dictionary(Of Integer, SortedList(Of String, String))
            strDataList = p_Entity.ClientDataValue
            For Each item As SortedList(Of String, String) In strDataList.Values
                p_Data.revisionedObjectList(p_Entity.ClientDataName).appendItem(item("Name"), item("Revision"), False)
            Next
        ElseIf p_Entity.ClientDataTypeEnum = DataTypeEnum.DataList Then
            Dim list As Dictionary(Of Integer, String) = p_Entity.ClientDataValue
            For Each key In list.Keys
                p_Data.dataList(p_Entity.ClientDataName).appendItem(list(key))
            Next
        End If
    End Sub

#End Region


#Region "标准数据模型创建方法 spec product order 等其他自定义数据模型对象"
    ''' <summary>
    ''' 建模对象创建基本方法
    ''' </summary>
    ''' <param name="p_TxnName"></param>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateModel(ByVal p_TxnName As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                   ByVal p_SubentityList As Dictionary(Of String, Dictionary(Of String, List(Of ClientAPIEntity))), _
                                   ByRef strInfo As String) As Boolean

        Dim p_TxnDocName As String = p_TxnName + "Doc"
        '初始化参数
        CreateModel = False
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Try
            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            gService.perform("New")
            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next
            If Not IsNothing(p_SubentityList) Then
                For Each entityListName As String In p_SubentityList.Keys
                    Dim oLsit As csiSubentityList
                    Dim oLsitItem As csiSubentity
                    oLsit = ObjectChanges.subentityList(entityListName)
                    Dim entityList As Dictionary(Of String, List(Of ClientAPIEntity)) = p_SubentityList(entityListName)

                    For Each entityName As String In entityList.Keys
                        Dim _subEntityList As List(Of ClientAPIEntity) = entityList(entityName)
                        oLsitItem = oLsit.appendItem()
                        For j As Integer = 0 To _subEntityList.Count - 1
                            SetInputDataValue(oLsitItem, _subEntityList(j))
                        Next
                    Next
                Next
            End If

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            CreateModel = True

        Catch e As Exception
            strInfo = e.Message
        End Try
    End Function

    ''' <summary>
    ''' 标准建模创建
    ''' </summary>
    ''' <param name="p_TxnDocName"></param>
    ''' <param name="p_TxnName"></param>
    ''' <param name="p_eventName"></param>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateModelByStandard(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                          ByVal p_eventName As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                          ByRef strInfo As String) As Boolean

        CreateModelByStandard = CreateModel(p_TxnName, p_dataEntityList, Nothing, strInfo)
    End Function

    ''' <summary>
    ''' 标准建模创建带一个SubEntityList
    ''' </summary>
    Public Function CreateModelHasSubentityList(ByVal p_TxnDocName As String, ByVal p_TxnName As String, ByVal p_eventName As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_SubentityListName As String, _
                                          ByVal p_SubentityListEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                          ByRef strInfo As String) As Boolean

        Dim subentityList As New Dictionary(Of String, Dictionary(Of String, List(Of ClientAPIEntity)))
        subentityList.Add(p_SubentityListName, p_SubentityListEntity)
        CreateModelHasSubentityList = CreateModel(p_TxnName, p_dataEntityList, subentityList, strInfo)

    End Function

    Public Function CreateModelHasTwoSubentityList(ByVal p_TxnDocName As String, ByVal p_TxnName As String, ByVal p_eventName As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_SubentityListName As String, _
                                          ByVal p_SubentityListEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                          ByVal p_SubentityListName2 As String, ByVal p_CheckPointEntity2 As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                          ByRef strInfo As String) As Boolean

        Dim subentityList As New Dictionary(Of String, Dictionary(Of String, List(Of ClientAPIEntity)))
        subentityList.Add(p_SubentityListName, p_SubentityListEntity)
        CreateModelHasTwoSubentityList = CreateModel(p_TxnName, p_dataEntityList, subentityList, strInfo)
    End Function

    ''' <summary>
    ''' 复制实例
    ''' </summary>
    ''' <param name="p_TxnName"></param>
    ''' <param name="p_SrcName"></param>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="p_SrcVersion"></param> 
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SaveAsModel(ByVal p_TxnName As String, ByVal p_SrcName As String, ByVal p_SrcVersion As String, _
                                         ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                   ByRef strInfo As String) As Boolean
        Dim p_TxnDocName As String = p_TxnName + "Doc"
        '初始化参数
        SaveAsModel = False
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim inputdata2 As csiObject
        Try
            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            inputdata2 = gService.inputData()
            If p_SrcVersion <> "" Then
                inputdata2.revisionedObjectField("ObjectToChange").setRef(p_SrcName, p_SrcVersion, False)
            Else
                inputdata2.namedObjectField("ObjectToChange").setRef(p_SrcName)
            End If
            gService.perform("Load")
            gService.perform("SaveAs")
            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            SaveAsModel = True

        Catch e As Exception
            strInfo = e.Message
        End Try
    End Function
#End Region

#Region "标准数据模型创建版本方法 spec product order 等其他自定义数据模型对象"
    Public Function CreateNewModelRevision(ByVal p_TxnName As String, ByVal p_OldName As String, _
                                   ByVal p_OldVersion As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                   ByVal p_SubentityList As Dictionary(Of String, Dictionary(Of String, List(Of ClientAPIEntity))), _
                                   ByRef strInfo As String) As Boolean

        Dim p_TxnDocName As String = p_TxnName + "Doc"
        CreateNewModelRevision = False
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim inputdata2 As csiObject

        Try
            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            inputdata2 = gService.inputData()
            If p_OldVersion <> "" Then
                inputdata2.revisionedObjectField("ObjectToChange").setRef(p_OldName, p_OldVersion, False)
            Else
                inputdata2.revisionedObjectField("ObjectToChange").setRef(p_OldName, p_OldVersion, True)
            End If

            gService.perform("SaveAsRev")

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges") '导入修改

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next
            If Not IsNothing(p_SubentityList) Then
                For Each entityListName As String In p_SubentityList.Keys
                    Dim oLsit As csiSubentityList
                    Dim oLsitItem As csiSubentity
                    oLsit = ObjectChanges.subentityList(entityListName)
                    Dim entityList As Dictionary(Of String, List(Of ClientAPIEntity)) = p_SubentityList(entityListName)

                    For i As Integer = 0 To entityList.Count - 1
                        Dim CheckPointEntityList As List(Of ClientAPIEntity) = entityList.Item((i).ToString().Trim())
                        oLsitItem = oLsit.appendItem()
                        For j As Integer = 0 To CheckPointEntityList.Count - 1
                            SetInputDataValue(oLsitItem, CheckPointEntityList(j))
                        Next
                    Next
                Next
            End If

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            CreateNewModelRevision = True

        Catch e As Exception
            strInfo = e.Message
        End Try
    End Function

    Public Function CreateVersionModelHasSubentityList(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                          ByVal p_OldName As String, ByVal p_OldVersion As String, ByVal p_UpdateCDOType As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_SubentityListName As String, _
                                          ByVal p_SubentityListEntity As Dictionary(Of String, List(Of ClientAPIEntity)), ByRef strInfo As String) As Boolean
        Dim subentityList As New Dictionary(Of String, Dictionary(Of String, List(Of ClientAPIEntity)))
        subentityList.Add(p_SubentityListName, p_SubentityListEntity)
        CreateVersionModelHasSubentityList = CreateNewModelRevision(p_TxnName, p_OldName, p_OldVersion, p_dataEntityList, subentityList, strInfo)

    End Function
#End Region

#Region "标准数据模型修改方法 spec product order 等其他自定义数据模型对象"
    ''' <summary>
    ''' 建模对象更新基本方法
    ''' </summary>
    ''' <param name="p_TxnName"></param>
    ''' <param name="p_UpdateName"></param>
    ''' <param name="p_UpdateVersion"></param>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="p_SubentityList"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function UpdateModel(ByVal p_TxnName As String, ByVal p_UpdateName As String, ByVal p_UpdateVersion As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                          ByVal p_SubentityList As Dictionary(Of String, Dictionary(Of String, List(Of ClientAPIEntity))), _
                                          ByRef strInfo As String) As Boolean
        Dim p_TxnDocName As String = p_TxnName + "Doc"
        UpdateModel = False
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim inputdata2 As csiObject

        Try

            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            inputdata2 = gService.inputData()
            If p_UpdateVersion <> "" Then
                inputdata2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, False)
            Else
                inputdata2.namedObjectField("ObjectToChange").setRef(p_UpdateName)
            End If

            gService.perform("Load")

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges") '导入修改

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            UpdateModel = True

        Catch e As Exception
            strInfo = e.Message
        End Try
    End Function

    Public Function UpdateModelByStandard(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                          ByVal p_UpdateName As String, ByVal p_UpdateVersion As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                          ByRef strInfo As String) As Boolean

        UpdateModelByStandard = UpdateModel(p_TxnName, p_UpdateName, p_UpdateVersion, p_dataEntityList, Nothing, strInfo)
    End Function

    Public Function UpdateModelHasSubentityList(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                          ByVal p_UpdateName As String, ByVal p_UpdateVersion As String, ByVal p_UpdateCDOType As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_SubentityListName As String, _
                                          ByVal p_CheckPointEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                          ByVal p_SubentityListCount_Old As Integer, ByRef strInfo As String) As Boolean

        '初始化参数
        UpdateModelHasSubentityList = False
        Dim inputData As csiObject
        Dim inputData2 As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim oLsit As csiSubentityList
        Dim oLsitItem As csiSubentity

        Try

            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            inputData2 = gService.inputData()
            If p_UpdateCDOType = "RO" Then
                If p_UpdateVersion <> "" Then
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, False)
                Else
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, True)
                End If
            Else
                inputData2.namedObjectField("ObjectToChange").setRef(p_UpdateName)
            End If

            gService.perform("Load")

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges") '导入修改

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            oLsit = ObjectChanges.subentityList(p_SubentityListName)
            If p_SubentityListCount_Old > 0 Then
                For j = 1 To p_SubentityListCount_Old
                    oLsit.deleteItemByIndex(0)
                Next
            End If
            For i As Integer = 0 To p_CheckPointEntity.Count - 1

                Dim CheckPointEntityList As List(Of ClientAPIEntity) = p_CheckPointEntity.Item((i).ToString().Trim())

                oLsitItem = oLsit.appendItem()

                For j As Integer = 0 To CheckPointEntityList.Count - 1
                    If CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.DataField Then
                        oLsitItem.dataField(CheckPointEntityList(j).ClientDataName).setValue(CheckPointEntityList(j).ClientDataValue)

                    ElseIf CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.NamedObjectField Then
                        oLsitItem.namedObjectField(CheckPointEntityList(j).ClientDataName).setRef(CheckPointEntityList(j).ClientDataValue)

                    ElseIf CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.RevisionedObjectField Then
                        oLsitItem.revisionedObjectField(CheckPointEntityList(j).ClientDataName).setRef(CheckPointEntityList(j).ClientDataValue, CheckPointEntityList(j).ClientDataVersion, False)

                    End If
                Next

            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            UpdateModelHasSubentityList = True

        Catch e As Exception
            UpdateModelHasSubentityList = False
            strInfo = e.Message
        End Try
    End Function

    Public Function UpdateModelHasTwoSubentityList(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                          ByVal p_UpdateName As String, ByVal p_UpdateVersion As String, ByVal p_UpdateCDOType As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_SubentityListName As String, _
                                          ByVal p_CheckPointEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                          ByVal p_SubentityListCount_Old As Integer, ByVal p_SubentityListName2 As String, _
                                          ByVal p_CheckPointEntity2 As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                          ByVal p_SubentityListCount_Old2 As Integer, ByRef strInfo As String) As Boolean

        '初始化参数
        UpdateModelHasTwoSubentityList = False
        Dim inputData As csiObject
        Dim inputData2 As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim oLsit As csiSubentityList
        Dim oLsit2 As csiNamedObjectList
        Dim oLsitItem As csiSubentity
        Dim oLsitItem2 As csiNamedObject
        Try

            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            inputData2 = gService.inputData()
            If p_UpdateCDOType = "RO" Then
                If p_UpdateVersion <> "" Then
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, False)
                Else
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, True)
                End If
            Else
                inputData2.namedObjectField("ObjectToChange").setRef(p_UpdateName)
            End If


            gService.perform("Load")

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges") '导入修改

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            oLsit = ObjectChanges.subentityList(p_SubentityListName)
            If p_SubentityListCount_Old > 0 Then
                For j = 1 To p_SubentityListCount_Old
                    oLsit.deleteItemByIndex(0)
                Next
            End If
            For i As Integer = 0 To p_CheckPointEntity.Count - 1

                Dim CheckPointEntityList As List(Of ClientAPIEntity) = p_CheckPointEntity.Item((i).ToString().Trim())

                oLsitItem = oLsit.appendItem()

                For j As Integer = 0 To CheckPointEntityList.Count - 1
                    If CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.DataField Then
                        oLsitItem.dataField(CheckPointEntityList(j).ClientDataName).setValue(CheckPointEntityList(j).ClientDataValue)

                    ElseIf CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.NamedObjectField Then
                        oLsitItem.namedObjectField(CheckPointEntityList(j).ClientDataName).setRef(CheckPointEntityList(j).ClientDataValue)

                    ElseIf CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.RevisionedObjectField Then
                        oLsitItem.revisionedObjectField(CheckPointEntityList(j).ClientDataName).setRef(CheckPointEntityList(j).ClientDataValue, CheckPointEntityList(j).ClientDataVersion, False)

                    End If
                Next

            Next

            oLsit2 = ObjectChanges.namedObjectList(p_SubentityListName2)
            If p_SubentityListCount_Old2 > 0 Then
                For j = 1 To p_SubentityListCount_Old2
                    oLsit2.deleteItemByIndex(0)
                Next
            End If
            For i = 0 To p_CheckPointEntity2.Count - 1
                Dim CheckPointEntityList As List(Of ClientAPIEntity) = p_CheckPointEntity2.Item((i).ToString().Trim())
                oLsitItem2 = oLsit2.appendItem(CheckPointEntityList(0).ClientDataValue)
                'oLsitItem2.dataField(CheckPointEntityList(0).ClientDataName).setValue(CheckPointEntityList(0).ClientDataValue)
            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            UpdateModelHasTwoSubentityList = True

        Catch e As Exception
            UpdateModelHasTwoSubentityList = False
            strInfo = e.Message
        End Try
    End Function

    Public Function UpdateModelHasNamedSubentityList(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                          ByVal p_UpdateName As String, ByVal p_UpdateVersion As String, ByVal p_UpdateCDOType As String, _
                                          ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_SubentityListName As String, _
                                          ByVal p_CheckPointEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                          ByRef strInfo As String) As Boolean

        '初始化参数
        UpdateModelHasNamedSubentityList = False
        Dim inputData As csiObject
        Dim inputData2 As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim oList As csiNamedSubentityList

        Try

            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            inputData2 = gService.inputData()
            If p_UpdateCDOType = "RO" Then
                If p_UpdateVersion <> "" Then
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, False)
                Else
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, True)
                End If
            Else
                inputData2.namedObjectField("ObjectToChange").setRef(p_UpdateName)
            End If


            gService.perform("Load")

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges") '导入修改

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next


            oList = ObjectChanges.namedSubentityList(p_SubentityListName)

            For i As Integer = 0 To p_CheckPointEntity.Count - 1

                Dim CheckPointEntityList As List(Of ClientAPIEntity) = p_CheckPointEntity.Item((i).ToString().Trim())
                Dim oLsitItem As csiNamedSubentity
                For j As Integer = 0 To CheckPointEntityList.Count - 1
                    If CheckPointEntityList(j).ClientDataName = "Name" Then
                        oLsitItem = oList.changeItemByName(CheckPointEntityList(j).ClientDataValue)
                    End If
                Next
                If IsNothing(oLsitItem) = False Then
                    For j As Integer = 0 To CheckPointEntityList.Count - 1
                        'oLsitItem.namedObjectField(CheckPointEntityList(j).ClientDataName).setRef(CheckPointEntityList(j).ClientDataValue)
                        If CheckPointEntityList(j).ClientDataName <> "Name" Then
                            SetStepDetailValue(oLsitItem, CheckPointEntityList(j))
                        End If
                    Next
                End If
            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            UpdateModelHasNamedSubentityList = True

        Catch e As Exception
            UpdateModelHasNamedSubentityList = False
            strInfo = e.Message
        End Try
    End Function
#End Region

#Region "标准数据模型删除方法"
    Public Function DeleteModel(ByVal p_TxnName As String, ByVal p_UpdateName As String, _
                                       ByVal p_UpdateVersion As String, ByVal p_UpdateCDOType As String, _
                                       ByRef strInfo As String) As Boolean
        Dim p_TxnDocName As String = p_TxnName + "Doc"
        '初始化参数
        DeleteModel = False
        Dim inputData As csiObject
        Dim inputData2 As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Try

            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            inputData2 = gService.inputData()
            If p_UpdateCDOType = "RO" Then
                If p_UpdateVersion <> "" Then
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, False)
                    gService.perform("Delete")
                Else '没有版本删除全部版本
                    inputData2.revisionedObjectField("ObjectToChange").setRef(p_UpdateName, p_UpdateVersion, True)
                    gService.perform("DeleteAllRevisions")
                End If
            Else
                inputData2.namedObjectField("ObjectToChange").setRef(p_UpdateName)
                gService.perform("Delete")
            End If


            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            DeleteModel = True

        Catch e As Exception
            DeleteModel = False
            strInfo = e.Message
        End Try
    End Function

    Public Function DeleteModel(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                          ByVal p_UpdateName As String, ByVal p_UpdateVersion As String, ByVal p_UpdateCDOType As String, _
                                          ByRef strInfo As String) As Boolean

        '初始化参数
        DeleteModel = DeleteModel(p_TxnName, p_UpdateName, p_UpdateVersion, p_UpdateCDOType, strInfo)
    End Function
#End Region


#Region "RunTxnService"
    Public Function RunTxnService(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                              ByVal p_dataEntityList As List(Of ClientAPIEntity), ByRef strInfo As String) As Boolean

        Try
            '初始化参数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            RunTxnService = False

            Dim inputData As csiObject
            inputData = gService.inputData


            '参数赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(inputData, p_dataEntityList(i))
            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            RunTxnService = True

        Catch e As Exception
            RunTxnService = False
            strInfo = e.Message
        End Try
    End Function

    Public Function RunTxnServiceHasSubentity(ByVal p_TxnDocName As String, ByVal p_TxnName As String, _
                                              ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_SubentityListName As String, _
                                              ByVal p_ChildEntity As Dictionary(Of String, List(Of ClientAPIEntity)), ByRef strInfo As String) As Boolean

        Try
            '初始化参数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)
            RunTxnServiceHasSubentity = False

            Dim inputData As csiObject
            inputData = gService.inputData


            '参数赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(inputData, p_dataEntityList(i))
            Next
            Dim oLsit As csiSubentityList

            Dim oLsitItem As csiSubentity

            oLsit = inputData.subentityList(p_SubentityListName)
            For i As Integer = 0 To p_ChildEntity.Count - 1

                Dim ChildEntity As List(Of ClientAPIEntity) = p_ChildEntity.Item((i).ToString().Trim())
                oLsitItem = oLsit.appendItem()
                For j As Integer = 0 To ChildEntity.Count - 1
                    SetDetailValue(oLsitItem, ChildEntity(j))
                Next
            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            RunTxnServiceHasSubentity = True

        Catch e As Exception
            RunTxnServiceHasSubentity = False
            strInfo = e.Message
        End Try
    End Function
#End Region

#Region "Start"
    Public Function Start(ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                          ByVal p_ChildEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                          ByRef strInfo As String) As Boolean

        Try
            '初始化参数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("StartDoc", "Start")
            Start = False

            Dim inputData As csiObject
            inputData = gService.inputData

            '参数赋值
            Dim Details As csiSubentity
            Dim CurrentStatusDetails As csiSubentity
            CurrentStatusDetails = inputData.subentityField("CurrentStatusDetails")
            Details = inputData.subentityField("Details")

            For i As Integer = 0 To p_dataEntityList.Count - 1

                If p_dataEntityList(i).ClientInputTypeEnum = InputTypeEnum.Details Then
                    If p_dataEntityList(i).ClientDataTypeEnum = DataTypeEnum.NamedObjectList Then
                        Dim list As Dictionary(Of Integer, String) = p_dataEntityList(i).ClientDataValue
                        For Each key In list.Keys
                            Details.namedObjectList(p_dataEntityList(i).ClientDataName).appendItem(list(key))
                        Next
                    Else
                        SetDetailValue(Details, p_dataEntityList(i))
                    End If

                ElseIf p_dataEntityList(i).ClientInputTypeEnum = InputTypeEnum.CurrentStatusDetails Then

                    If p_dataEntityList(i).ClientDataName = "Location" Then
                        CurrentStatusDetails.namedSubentityField("Location").setName(p_dataEntityList(i).ClientDataValue)
                    Else
                        SetDetailValue(CurrentStatusDetails, p_dataEntityList(i))
                    End If
                End If
            Next

            Dim childContainers As csiSubentityList = Details.subentityList("ChildContainers")
            ''子批次
            childContainers.removeAllChildren()

            If p_ChildEntity.Count > 0 Then
                Dim _childContainer As csiSubentity
                For intChildCounter As Integer = 0 To p_ChildEntity.Count - 1

                    Dim childEntityList As List(Of ClientAPIEntity) = p_ChildEntity.Item((intChildCounter).ToString().Trim())

                    _childContainer = childContainers.appendItem()
                    _childContainer.setObjectType("StartDetails")

                    For j As Integer = 0 To childEntityList.Count - 1
                        SetDetailValue(_childContainer, childEntityList(j))
                    Next
                Next
            End If

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            Start = True

        Catch e As Exception
            Start = False
            strInfo = e.Message
        End Try
    End Function

    Public Function StartWithAttributes(ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                           ByVal p_ChildEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                           ByVal p_Attributes As Dictionary(Of String, List(Of ClientAPIEntity)), _
                           ByRef strInfo As String) As Boolean

        Try
            '初始化参数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("StartDoc", "Start")
            StartWithAttributes = False

            Dim inputData As csiObject = gService.inputData

            '参数赋值
            Dim Details As csiSubentity
            Dim CurrentStatusDetails As csiSubentity
            CurrentStatusDetails = inputData.subentityField("CurrentStatusDetails")
            Details = inputData.subentityField("Details")

            For i As Integer = 0 To p_dataEntityList.Count - 1

                If p_dataEntityList(i).ClientInputTypeEnum = InputTypeEnum.Details Then
                    'If p_dataEntityList(i).ClientDataTypeEnum = DataTypeEnum.NamedObjectList Then
                    '    Dim list As Dictionary(Of Integer, String) = p_dataEntityList(i).ClientDataValue
                    '    For Each key In list.Keys
                    '        Details.namedObjectList(p_dataEntityList(i).ClientDataName).appendItem(list(key))
                    '    Next
                    'Else
                    '    SetDetailValue(Details, p_dataEntityList(i))
                    'End If
                    SetInputDataValue(Details, p_dataEntityList(i))
                ElseIf p_dataEntityList(i).ClientInputTypeEnum = InputTypeEnum.CurrentStatusDetails Then
                    'If p_dataEntityList(i).ClientDataName = "Location" Then
                    '    CurrentStatusDetails.namedSubentityField("Location").setName(p_dataEntityList(i).ClientDataValue)
                    'Else
                    '    SetDetailValue(CurrentStatusDetails, p_dataEntityList(i))
                    'End If
                    SetInputDataValue(CurrentStatusDetails, p_dataEntityList(i))
                End If
            Next

            Dim childContainers As csiSubentityList = Details.subentityList("ChildContainers")
            ''子批次
            childContainers.removeAllChildren()

            If p_ChildEntity.Count > 0 Then
                Dim _childContainer As csiSubentity
                For intChildCounter As Integer = 0 To p_ChildEntity.Count - 1

                    Dim childEntityList As List(Of ClientAPIEntity) = p_ChildEntity.Item((intChildCounter).ToString().Trim())

                    _childContainer = childContainers.appendItem()
                    _childContainer.setObjectType("StartDetails")

                    For j As Integer = 0 To childEntityList.Count - 1
                        SetDetailValue(_childContainer, childEntityList(j))
                    Next
                Next
            End If

            Dim attributes As csiSubentityList = Details.subentityList("AttributeDetails")
            If p_Attributes.Count > 0 Then
                Dim _attribute As csiSubentity
                For Each strIndex As String In p_Attributes.Keys
                    Dim _attributeEntityList As List(Of ClientAPIEntity) = p_Attributes.Item(strIndex)
                    _attribute = attributes.appendItem()

                    For Each _entity As ClientAPIEntity In _attributeEntityList
                        SetDetailValue(_attribute, _entity)
                    Next
                Next

            End If

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            StartWithAttributes = True

        Catch e As Exception
            StartWithAttributes = False
            strInfo = e.Message
        End Try
    End Function

    '批量开卡
    Public Function ContainerStarts(ByVal containers As List(Of ContainerStartModel), ByRef strInfo As String) As Boolean

        Dim conStarts = New BwStarts()
        Dim conStartsSer = New BwStartsService(New UserProfile(gUserName, gPassword))
        Dim starts = New List(Of Start)

        For Each container In containers

            Dim m = New Start()
            Dim md = New StartDetails()

            md.StartReason = New NamedObjectRef(container.StartReason)
            md.Level = New NamedObjectRef(container.Level)
            md.Owner = New NamedObjectRef(container.Owner)

            md.ContainerName = container.ContainerName
            md.Product = New RevisionedObjectRef(container.ProductName, container.ProductRev)
            md.Qty = Val(container.Qty)
            md.MfgOrder = New NamedObjectRef(container.Mfgorder)

            If Not String.IsNullOrWhiteSpace(container.PlannedStartDate) Then
                md.PlannedStartDate = DateTime.Parse(container.PlannedStartDate)
            End If

            If Not String.IsNullOrWhiteSpace(container.PlannedCompletionDate) Then
                md.PlannedCompletionDate = DateTime.Parse(container.PlannedCompletionDate)
            End If

            md.StartEmployee = New NamedObjectRef(container.StartEmployee)

            '批次属性
            If container.AttributeList IsNot Nothing Then
                If container.AttributeList.Rows.Count > 0 Then
                    Dim attributes = New List(Of StartContainerAttributeDetail)

                    For Each row In container.AttributeList.Rows
                        Dim attr = New StartContainerAttributeDetail()
                        attr.Name = row("Name").ToString()
                        attr.AttributeValue = row("Value").ToString()
                        attr.DataType = TrivialTypeEnum.String

                        attributes.Add(attr)
                    Next
                    md.AttributeDetails = attributes.ToArray()

                End If
            End If

            m.Details = md

            Dim mcs = New CurrentStatusStartDetails()

            Dim workflow = New RevisionedObjectRef(container.WorkflowName, container.WorkflowRev)

            mcs.Workflow = workflow

            m.CurrentStatusDetails = mcs

            starts.Add(m)

        Next


        conStarts.Services = starts.ToArray()
        Dim Result = conStartsSer.ExecuteTransaction(conStarts)

        If Result.IsSuccess Then
            strInfo = Result.Message

        Else
            strInfo = Result.ExceptionData.ToString()

        End If

        Return Result.IsSuccess



    End Function


#End Region

#Region "批次标准操作关闭、打开、标准移动、移入、非标准移动、批次暂停、暂停、开始、拆分、修改数量"

    Public Function ContainerOperByNotStandard(ByVal Container As String, ByVal strLevel As String, ByVal strUser As String, ByRef workFlow As String, ByRef toStep As String, ByRef workFlowVersion As String, ByRef strInfo As String) As Boolean

        Try

            '初始化函数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("MoveNonStdDoc", "MoveNonStd")

            '参数赋值
            Dim inputData As csiObject
            inputData = gService.inputData

            inputData.containerField("Container").setRef(Container, strLevel)
            inputData.namedSubentityField("ToStep").setName(toStep)
            inputData.revisionedObjectField("ToWorkflow").setRef(workFlow, workFlowVersion, False)
            'inputData.namedObjectField("ReportEmployee").setRef(strUser)
            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            ContainerOperByNotStandard = True
        Catch e As Exception
            ContainerOperByNotStandard = False
            strInfo = e.Message
        End Try
    End Function

    Public Function ContainerMaint(ByVal strContainerName As String, ByVal strLevel As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), ByRef strInfo As String) As Boolean

        Try

            '初始化函数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("ContainerMaintDoc", "ContainerMaint")

            '参数赋值
            Dim inputData As csiObject
            inputData = gService.inputData

            inputData.containerField("Container").setRef(strContainerName, strLevel)

            Dim Details As csiSubentity = inputData.subentityField("ServiceDetail")
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetDetailValue(Details, p_dataEntityList(i))
            Next

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            ContainerMaint = True
        Catch e As Exception
            ContainerMaint = False
            strInfo = e.Message
        End Try
    End Function

    Public Function ContainerAtterMaint(ByVal strContainerName As String, ByVal strLevel As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), ByRef strInfo As String) As Boolean

        Try

            '初始化函数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("ContainerAttrMaintDoc", "ContainerAttrMaint")

            '参数赋值
            Dim inputData As csiObject
            inputData = gService.inputData

            inputData.containerField("Container").setRef(strContainerName, strLevel)

            Dim Details As csiSubentity
            For i As Integer = 0 To p_dataEntityList.Count - 1
                Details = inputData.subentityList("ServiceDetails").appendItem()
                Details.dataField("Name").setValue(p_dataEntityList(i).ClientDataName)
                Details.dataField("DataType").setValue("4")
                Details.dataField("AttributeValue").setValue(p_dataEntityList(i).ClientDataValue)
            Next

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            ContainerAtterMaint = True
        Catch e As Exception
            ContainerAtterMaint = False
            strInfo = e.Message
        End Try
    End Function

    Public Function Split(ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal p_ChildEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                          ByVal dtChildContainer As DataTable, ByRef strInfo As String) As Boolean
        Try

            '初始化函数
            Split = False
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("SplitDoc", "Split")

            Dim inputData As csiObject
            inputData = gService.inputData

            '参数赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(inputData, p_dataEntityList(i))
            Next

            inputData.subentityList("ToContainerDetails").removeAllChildren()

            If p_ChildEntity.Count > 0 Then

                Dim ToContainerDetails As csiSubentity
                For intChildCounter As Integer = 0 To p_ChildEntity.Count - 1

                    Dim childEntityList As List(Of ClientAPIEntity) = p_ChildEntity.Item((intChildCounter).ToString().Trim())

                    ToContainerDetails = inputData.subentityList("ToContainerDetails").appendItem()
                    ToContainerDetails.setObjectType("SplitDetails")

                    For j As Integer = 0 To childEntityList.Count - 1
                        SetDetailValue(ToContainerDetails, childEntityList(j))
                    Next
                    If IsNothing(dtChildContainer) Then
                        Continue For
                    End If
                    If dtChildContainer.Rows.Count > 0 Then
                        For Each r As DataRow In dtChildContainer.Rows
                            ToContainerDetails.containerList("ChildContainers").appendItem(r(0).ToString, r(1).ToString)
                        Next
                    End If
                Next
            End If

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            Split = True
        Catch e As Exception
            Split = False
            strInfo = e.Message
        End Try
    End Function

    Public Function ContainerChangeQty(ByVal p_TxnDocName As String, ByVal p_TxnName As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                       ByVal p_SubentityListName As String, ByVal p_SubentityListEntity As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                       ByVal ChangeQtyType As String, ByRef strInfo As String) As Boolean

        Try

            '初始化函数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService(p_TxnDocName, p_TxnName)

            '参数赋值
            Dim inputData As csiObject
            inputData = gService.inputData

            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(inputData, p_dataEntityList(i))
            Next
            inputData.subentityList(p_SubentityListName).removeAllChildren()
            For Each oEntityList As List(Of ClientAPIEntity) In p_SubentityListEntity.Values

                Dim ToContainerDetails As csiSubentity
                ToContainerDetails = inputData.subentityList(p_SubentityListName).appendItem()

                Select Case ChangeQtyType
                    Case "Loss"
                        ToContainerDetails.setObjectType("LossDetails") ''报废（根据操作不同，这里需要做判断修改）
                        ToContainerDetails.dataField("ChangeQtyType").setValue(2)
                    Case "Sell"
                        ToContainerDetails.setObjectType("SellDetails") ''销售（根据操作不同，这里需要做判断修改）
                        ToContainerDetails.dataField("ChangeQtyType").setValue(3)
                    Case "Adjust"
                        ToContainerDetails.setObjectType("AdjustDetails") ''调整（根据操作不同，这里需要做判断修改）
                        ToContainerDetails.dataField("ChangeQtyType").setValue(4)
                    Case "Bonus"
                        ToContainerDetails.setObjectType("BonusDetails") ''额外（根据操作不同，这里需要做判断修改）
                        ToContainerDetails.dataField("ChangeQtyType").setValue(5)
                    Case "Buy"
                        ToContainerDetails.setObjectType("BuyDetails") ''购买（根据操作不同，这里需要做判断修改）
                        ToContainerDetails.dataField("ChangeQtyType").setValue(6)

                End Select

                For j As Integer = 0 To oEntityList.Count - 1
                    SetDetailValue(ToContainerDetails, oEntityList(j))
                Next
            Next

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            ContainerChangeQty = True
        Catch e As Exception
            ContainerChangeQty = False
            strInfo = e.Message
        End Try
    End Function

#Region "批次修改数量"
    Public Function ContainerChangeQty(ByVal p_dataEntityList As List(Of ClientAPIEntity),
                                       ByVal p_SubentityListEntity As Dictionary(Of String, List(Of ClientAPIEntity)),
                                       ByVal ChangeQtyType As String, ByRef strInfo As String) As Boolean

        Try

            '初始化函数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("ChangeQtyDoc", "ChangeQty")

            '参数赋值
            Dim inputData As csiObject
            inputData = gService.inputData

            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(inputData, p_dataEntityList(i))
            Next

            inputData.subentityList("ServiceDetails").removeAllChildren()

            For i As Integer = 0 To p_SubentityListEntity.Count - 1
                Dim ServiceDetails As csiSubentity = inputData.subentityList("ServiceDetails").appendItem()

                Dim EntityList As List(Of ClientAPIEntity) = p_SubentityListEntity.Item(i)

                Select Case ChangeQtyType
                    Case "Loss"
                        ServiceDetails.setObjectType("LossDetails") ''报废（根据操作不同，这里需要做判断修改）
                        ServiceDetails.dataField("ChangeQtyType").setValue(2)
                    Case "Sell"
                        ServiceDetails.setObjectType("SellDetails") ''销售（根据操作不同，这里需要做判断修改）
                        ServiceDetails.dataField("ChangeQtyType").setValue(3)
                    Case "Bonus"
                        ServiceDetails.setObjectType("BonusDetails") ''额外（根据操作不同，这里需要做判断修改）
                        ServiceDetails.dataField("ChangeQtyType").setValue(5)
                    Case "Buy"
                        ServiceDetails.setObjectType("BuyDetails") ''购买（根据操作不同，这里需要做判断修改）
                        ServiceDetails.dataField("ChangeQtyType").setValue(6)
                    Case "Adjust"
                        ServiceDetails.setObjectType("AdjustDetails") ''调整（根据操作不同，这里需要做判断修改）
                        ServiceDetails.dataField("ChangeQtyType").setValue(4)
                End Select

                For j = 0 To EntityList.Count - 1
                    If EntityList(j).ClientDataTypeEnum = DataTypeEnum.DataField Then
                        ServiceDetails.dataField(EntityList(j).ClientDataName).setValue(EntityList(j).ClientDataValue)
                    ElseIf EntityList(j).ClientDataTypeEnum = DataTypeEnum.NamedObjectField Then
                        ServiceDetails.namedObjectField(EntityList(j).ClientDataName).setRef(EntityList(j).ClientDataValue)
                    ElseIf EntityList(j).ClientDataTypeEnum = DataTypeEnum.RevisionedObjectField Then
                        ServiceDetails.revisionedObjectField(EntityList(j).ClientDataName).setRef(EntityList(j).ClientDataValue, EntityList(j).ClientDataVersion, False)
                    End If
                Next
            Next

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            ContainerChangeQty = True
        Catch e As Exception
            ContainerChangeQty = False
            strInfo = e.Message
        End Try
    End Function

#End Region

#End Region

#Region "Associate Or Disassociate"
    Public Function AssociateOrDisassociate(ByVal p_TxnName As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                              ByVal p_ChildContainerListEntity As List(Of ClientAPIEntity), _
                              ByRef strInfo As String) As Boolean

        Try

            '初始化函数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService(p_TxnName & "Doc", p_TxnName)

            '参数赋值
            Dim inputData As csiObject
            inputData = gService.inputData

            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(inputData, p_dataEntityList(i))
            Next

            Dim ChildContainerList As csiContainerList = inputData.containerList("ChildContainers")
            ChildContainerList.removeAllChildren()

            For i As Integer = 0 To p_ChildContainerListEntity.Count - 1
                ChildContainerList.appendItem(p_ChildContainerListEntity(i).ClientDataValue, p_ChildContainerListEntity(i).ClientOtherValue)
            Next

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            AssociateOrDisassociate = True
        Catch e As Exception
            AssociateOrDisassociate = False
            strInfo = e.Message
        End Try
    End Function

#End Region

#Region "工艺创建及创建版本"
    ''' <summary>
    ''' 创建新版本工艺 createby YangSJ 20170914
    ''' </summary>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="dtStepList"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateNewWorkflowRevsion(ByRef strWorkflowName As String, ByRef strOldRevsion As String, ByRef strRevsion As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal dtStepList As DataTable, ByRef strInfo As String) As Boolean
        CreateNewWorkflowRevsion = CreateWorkflowRevsion("MachineMfgWorkFlow", strWorkflowName, strOldRevsion, strRevsion, p_dataEntityList, dtStepList, strInfo)
    End Function

    ''' <summary>
    ''' 创建新版本工艺基本方法 createby YangSJ 20170914
    ''' </summary>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="dtStepList"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateWorkflowRevsion(ByVal WorkflowCDO As String, ByRef strWorkflowName As String, ByRef strOldRevsion As String, ByRef strRevsion As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal dtStepList As DataTable, ByRef strInfo As String) As Boolean
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim Steps As csiNamedSubentityList
        Dim Stepitem As csiNamedSubentity
        Dim Pathlist As csiNamedSubentityList
        Dim PathItem As csiNamedSubentity
        Dim ToStep As csiNamedSubentity
        Dim ToStepParentInfo As csiParentInfo

        Dim inputdata2 As csiObject
        Try

            InitializeSession()
            CreateDocumentandService(WorkflowCDO + "MaintDoc", WorkflowCDO + "Maint")
            inputdata2 = gService.inputData()
            inputdata2.revisionedObjectField("ObjectToChange").setRef(strWorkflowName, strOldRevsion, False)
            gService.perform("SaveAsRev")
            inputData = gService.inputData
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            Steps = ObjectChanges.namedSubentityList("Steps")
            Dim intX As Integer = 20
            Dim intY As Integer = 20
            Dim intBeginX As Integer = 20
            Dim intRowIndex As Integer = 1
            Dim intIncrementalChange As Integer = 90
            Dim intMaxLength = intIncrementalChange * 6
            Dim isIncremental = True
            For i As Integer = 0 To dtStepList.Rows.Count - 1
                Dim dr As DataRow = dtStepList.Rows(i)
                Stepitem = Steps.appendItem(dr("StepName"))
                If dr("ObjType") = "Spec" Then
                    Stepitem.setObjectType("SpecStepChanges")
                    Stepitem.revisionedObjectField("Spec").setRef(dr("ObjName"), dr("ObjRev"), False)
                Else
                    Stepitem.setObjectType("SubWorkflowStepChanges")
                    Stepitem.revisionedObjectField("SubWorkflow").setRef(dr("ObjName"), dr("ObjRev"), False)
                End If

                Stepitem.dataField("Description").setValue(dr("Description").ToString)
                Stepitem.dataField("Name").setValue(dr("StepName"))
                Stepitem.dataField("XLocation").setValue(intX)
                Stepitem.dataField("YLocation").setValue(intY)

                If intRowIndex Mod 6 = 0 Then
                    intY = intY + intIncrementalChange
                End If
                If isIncremental Then
                    intX = intX + intIncrementalChange
                Else
                    intX = intX - intIncrementalChange
                End If

                If intX > intMaxLength Then
                    isIncremental = False
                    intX = intX - intIncrementalChange
                End If
                If intX < intBeginX Then
                    isIncremental = True
                    intX = intBeginX
                End If

                intRowIndex = intRowIndex + 1

                If i = dtStepList.Rows.Count - 1 Then
                    Stepitem.dataField("IsLastStep").setValue("1")
                End If

            Next

            For i As Integer = 0 To dtStepList.Rows.Count - 1
                If i = dtStepList.Rows.Count - 1 Then
                    Exit For
                End If
                Dim dr As DataRow = dtStepList.Rows(i)
                Dim nextdr As DataRow = dtStepList.Rows(i + 1)

                Stepitem = Steps.changeItemByName(dr("StepName"))

                Pathlist = Stepitem.namedSubentityList("Paths")
                Dim pathname As String = DateTime.Now.ToString("yyyyMMddHHmmssffff")
                'pathname = String.Format("{0}到{1}", dr("StepName"), nextdr("StepName"))
                PathItem = Pathlist.appendItem(pathname)
                PathItem.dataField("Name").setValue(pathname)
                PathItem.dataField("Description").setValue(String.Format("加工路线从{0}到{1}", dr("StepName"), nextdr("StepName")))
                ToStep = PathItem.namedSubentityField("ToStep")

                ToStepParentInfo = ToStep.parentInfo()
                ToStepParentInfo.setObjectType(WorkflowCDO + "Changes")
                ToStepParentInfo.setRevisionedObjectRef(strWorkflowName, strRevsion, False)
                ToStep.setName(nextdr("StepName"))
            Next

            gService.setExecute()
            gService.requestData().requestField("CompletionMsg")
            ResponseDocument = gDocument.submit()
            CheckForErrors(ResponseDocument)
            CreateWorkflowRevsion = True
        Catch ex As Exception
            CreateWorkflowRevsion = False
            strInfo = ex.Message
        End Try
    End Function

    ''' <summary>
    ''' 创建新工艺 createby YangSJ 20171219
    ''' </summary>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="dtStepList"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateNewWorkflow(ByRef strWorkflowName As String, ByRef strRevsion As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal dtStepList As DataTable, ByRef strInfo As String) As Boolean
        CreateNewWorkflow = CreateWorkflow("MachineMfgWorkFlow", strWorkflowName, strRevsion, p_dataEntityList, dtStepList, strInfo)
    End Function

    ''' <summary>
    ''' 创建新工艺 createby YangSJ 2018611
    ''' </summary>
    ''' <param name="p_dataEntityList"></param>
    ''' <param name="dtStepList"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateWorkflow(ByVal WorkflowCDO As String, ByRef strWorkflowName As String, ByRef strRevsion As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), ByVal dtStepList As DataTable, ByRef strInfo As String) As Boolean
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim Steps As csiNamedSubentityList
        Dim Stepitem As csiNamedSubentity
        Dim Pathlist As csiNamedSubentityList
        Dim PathItem As csiNamedSubentity
        Dim ToStep As csiNamedSubentity
        Dim ToStepParentInfo As csiParentInfo

        Try

            InitializeSession()
            CreateDocumentandService(WorkflowCDO + "MaintDoc", WorkflowCDO + "Maint")
            gService.perform("New")
            inputData = gService.inputData
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            Steps = ObjectChanges.namedSubentityList("Steps")
            Dim intX As Integer = 20
            Dim intY As Integer = 20
            Dim intBeginX As Integer = 20
            Dim intRowIndex As Integer = 1
            Dim intIncrementalChange As Integer = 90
            Dim intMaxLength = intIncrementalChange * 6
            Dim isIncremental = True
            For i As Integer = 0 To dtStepList.Rows.Count - 1
                Dim dr As DataRow = dtStepList.Rows(i)
                Stepitem = Steps.appendItem(dr("StepName"))
                If dr("ObjType") = "Spec" Then
                    Stepitem.setObjectType("SpecStepChanges")
                    Stepitem.revisionedObjectField("Spec").setRef(dr("ObjName"), dr("ObjRev"), False)
                Else
                    Stepitem.setObjectType("SubWorkflowStepChanges")
                    Stepitem.revisionedObjectField("SubWorkflow").setRef(dr("ObjName"), dr("ObjRev"), False)
                End If

                Stepitem.dataField("Description").setValue(dr("Description"))
                Stepitem.dataField("Name").setValue(dr("StepName"))
                Stepitem.dataField("XLocation").setValue(intX)
                Stepitem.dataField("YLocation").setValue(intY)

                If intRowIndex Mod 6 = 0 Then
                    intY = intY + intIncrementalChange
                End If
                If isIncremental Then
                    intX = intX + intIncrementalChange
                Else
                    intX = intX - intIncrementalChange
                End If

                If intX > intMaxLength Then
                    isIncremental = False
                    intX = intX - intIncrementalChange
                End If
                If intX < intBeginX Then
                    isIncremental = True
                    intX = intBeginX
                End If

                intRowIndex = intRowIndex + 1

                If i = dtStepList.Rows.Count - 1 Then
                    Stepitem.dataField("IsLastStep").setValue("1")
                End If

            Next

            For i As Integer = 0 To dtStepList.Rows.Count - 1
                If i = dtStepList.Rows.Count - 1 Then
                    Exit For
                End If
                Dim dr As DataRow = dtStepList.Rows(i)
                Dim nextdr As DataRow = dtStepList.Rows(i + 1)

                Stepitem = Steps.changeItemByName(dr("StepName"))
                Pathlist = Stepitem.namedSubentityList("Paths")

                Dim pathname As String = DateTime.Now.ToString("yyyyMMddHHmmssffff")
                pathname = String.Format("{0}到{1}", dr("StepName"), nextdr("StepName"))
                PathItem = Pathlist.appendItem(pathname)
                PathItem.dataField("Name").setValue(pathname)
                PathItem.dataField("Description").setValue(String.Format("加工路线从{0}到{1}", dr("StepName"), nextdr("StepName")))
                ToStep = PathItem.namedSubentityField("ToStep")

                ToStepParentInfo = ToStep.parentInfo()
                ToStepParentInfo.setObjectType(WorkflowCDO + "Changes")
                ToStepParentInfo.setRevisionedObjectRef(strWorkflowName, strRevsion, False)
                ToStep.setName(nextdr("StepName"))
            Next

            gService.setExecute()
            gService.requestData().requestField("CompletionMsg")
            ResponseDocument = gDocument.submit()
            CheckForErrors(ResponseDocument)
            CreateWorkflow = True
        Catch ex As Exception
            CreateWorkflow = False
            strInfo = ex.Message
        End Try
    End Function
#End Region

    ''' <summary>
    ''' 用户验证
    ''' </summary>
    ''' <param name="User"></param>
    ''' <param name="Password"></param>
    ''' <param name="strInfo"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function VerifyUser(ByVal User As String, ByVal Password As String, ByRef strInfo As String) As Boolean
        Try
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("ServiceDoc", "Service")

            Dim inputData As csiObject
            inputData = gService.inputData
            inputData.namedObjectField("User").setRef(User)
            inputData.namedObjectField("Employee").setRef(User)
            inputData.dataField("Password").setValue(Password)
            gService.perform("VerifyUser")

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            VerifyUser = True
        Catch e As Exception
            VerifyUser = False
            strInfo = e.Message
        End Try
    End Function

#Region "创建检测项组"
    Public Function CreateCheckPointGroup(ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                      ByVal p_CheckPointEntity As Dictionary(Of String, List(Of ClientAPIEntity)), ByRef strInfo As String) As Boolean
        Try
            Dim inputData As csiObject
            Dim ObjectChanges As csiRevisionedObject
            Dim CheckPoints As csiNamedSubentityList
            Dim CheckPointitem As csiNamedSubentity
            Dim ResponseDocument As csiDocument

            Dim strCheckPointGroup As String = ""

            '初始化函数
            InitializeSession()
            CreateDocumentandService("CheckPointGroupMaintDoc", "CheckPointGroupMaint")
            gService.perform("New")
            inputData = gService.inputData()

            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            For i As Integer = 0 To p_dataEntityList.Count - 1
                If p_dataEntityList(i).ClientDataName.ToString().ToLower() = "name" Then
                    strCheckPointGroup = p_dataEntityList(i).ClientDataValue.ToString().Trim()
                End If

                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next
            CheckPoints = ObjectChanges.subentityList("CheckPoints")

            For i As Integer = 0 To p_CheckPointEntity.Count - 1

                Dim CheckPointEntityList As List(Of ClientAPIEntity) = p_CheckPointEntity.Item((i).ToString().Trim())
                CheckPointitem = CheckPoints.appendItem(i)

                For j As Integer = 0 To CheckPointEntityList.Count - 1
                    If CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.DataField Then
                        CheckPointitem.dataField(CheckPointEntityList(j).ClientDataName).setValue(CheckPointEntityList(j).ClientDataValue)

                    ElseIf CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.NamedObjectField Then
                        CheckPointitem.namedObjectField(CheckPointEntityList(j).ClientDataName).setRef(CheckPointEntityList(j).ClientDataValue)

                    ElseIf CheckPointEntityList(j).ClientDataTypeEnum = DataTypeEnum.RevisionedObjectField Then
                        CheckPointitem.revisionedObjectField(CheckPointEntityList(j).ClientDataName).setRef(CheckPointEntityList(j).ClientDataValue, CheckPointEntityList(j).ClientDataVersion, False)

                    End If
                Next

            Next


            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            CreateCheckPointGroup = True
        Catch e As Exception
            CreateCheckPointGroup = False
            strInfo = e.Message
        End Try

    End Function
#End Region
#Region "创建派工单"
    Public Function CreateDispatching(ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                          ByVal dtChildContainer As DataTable, ByRef strInfo As String) As Boolean

        Try
            '初始化参数
            CreateDispatching = False
            Dim inputData As csiObject
            Dim ResponseDocument As csiDocument
            Dim ObjectChanges As csiRevisionedObject
            InitializeSession()
            CreateDocumentandService("WorkCenterDispatchingMaintDoc", "WorkCenterDispatchingMaint")
            gService.perform("New")
            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            ''零件编号
            If IsNothing(dtChildContainer) = False Then
                If dtChildContainer.Rows.Count > 0 Then
                    For Each r As DataRow In dtChildContainer.Rows
                        ObjectChanges.dataList("ChildContainer").appendItem(r(0).ToString)
                    Next
                End If
            End If

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            CreateDispatching = True

        Catch e As Exception
            CreateDispatching = False
            strInfo = e.Message
        End Try
    End Function
#End Region
#Region "更新派工单"
    Public Function UpdateDispatching(ByVal p_UpdateName As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                      ByVal dtOldChildContainer As DataTable, ByVal dtNewChildContainer As DataTable, _
                                      ByRef strInfo As String) As Boolean

        UpdateDispatching = False
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim inputdata2 As csiObject

        Try

            InitializeSession()
            CreateDocumentandService("WorkCenterDispatchingMaintDoc", "WorkCenterDispatchingMaint")
            inputdata2 = gService.inputData()

            inputdata2.namedObjectField("ObjectToChange").setRef(p_UpdateName)

            gService.perform("load")

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges") '导入修改

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            ''零件编号,先删除旧的，再增加新的
            If IsNothing(dtOldChildContainer) = False Then
                If dtOldChildContainer.Rows.Count > 0 Then
                    For Each r As DataRow In dtOldChildContainer.Rows
                        Dim str As String = r(0).ToString
                        ObjectChanges.dataList("ChildContainer").deleteItemByValue(r(0).ToString)
                    Next
                End If
            End If
            If IsNothing(dtNewChildContainer) = False Then
                If dtNewChildContainer.Rows.Count > 0 Then
                    For Each r As DataRow In dtNewChildContainer.Rows
                        ObjectChanges.dataList("ChildContainer").appendItem(r(0).ToString)
                    Next
                End If
            End If

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            UpdateDispatching = True

        Catch e As Exception
            UpdateDispatching = False
            strInfo = e.Message
        End Try
    End Function
#End Region
#Region "删除派工单"
    Public Function DeleteDispatching(ByVal strWCDName As String, ByRef strInfo As String) As Boolean

        DeleteDispatching = DeleteModel("WorkCenterDispatchingMaint", strWCDName, "", "NDO", strInfo)

    End Function
#End Region
#Region "创建批次零件序号信息"
    Public Function CreateContainerProductNo(ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                          ByVal p_dataProductList As Dictionary(Of String, List(Of ClientAPIEntity)), ByRef strInfo As String) As Boolean

        Try
            '初始化参数
            CreateContainerProductNo = False
            Dim inputData As csiObject
            Dim ResponseDocument As csiDocument
            Dim ObjectChanges As csiRevisionedObject
            InitializeSession()
            CreateDocumentandService("ContainerProductNoMaintDoc", "ContainerProductNoMaint")
            gService.perform("New")
            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            ''零件编号
            Dim ProductNoList As csiNamedSubentityList = ObjectChanges.namedSubentityList("ProductListItem")

            For i As Integer = 0 To p_dataProductList.Count - 1

                Dim ProductListItem As List(Of ClientAPIEntity) = p_dataProductList.Item(i)
                Dim ProductNoItem As csiNamedSubentity = ProductNoList.appendItem(i)

                For j As Integer = 0 To ProductListItem.Count - 1
                    SetStepDetailValue(ProductNoItem, ProductListItem(j))
                Next
            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            CreateContainerProductNo = True

        Catch e As Exception
            CreateContainerProductNo = False
            strInfo = e.Message
        End Try
    End Function
#End Region
#Region "更新批次零件序号信息"
    Public Function UpdateContainerProductNo(ByVal p_UpdateName As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                      ByVal p_dataProductList As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                      ByRef strInfo As String) As Boolean

        UpdateContainerProductNo = False
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim inputdata2 As csiObject

        Try

            InitializeSession()
            CreateDocumentandService("ContainerProductNoMaintDoc", "ContainerProductNoMaint")

            inputdata2 = gService.inputData()
            inputdata2.namedObjectField("ObjectToChange").setRef(p_UpdateName)

            gService.perform("load")

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges") '导入修改

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            ''零件编号
            Dim ProductNoList As csiNamedSubentityList = ObjectChanges.namedSubentityList("ProductListItem")

            For i As Integer = 0 To p_dataProductList.Count - 1

                Dim ProductListItem As List(Of ClientAPIEntity) = p_dataProductList.Item(i)
                Dim ProductNoItem As csiNamedSubentity

                For j As Integer = 0 To ProductListItem.Count - 1
                    If ProductListItem(j).ClientDataName = "Name" Then
                        ProductNoItem = ProductNoList.changeItemByName(ProductListItem(j).ClientDataValue)
                    End If
                    SetStepDetailValue(ProductNoItem, ProductListItem(j))
                Next

            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            UpdateContainerProductNo = True

        Catch e As Exception
            UpdateContainerProductNo = False
            strInfo = e.Message
        End Try
    End Function
#End Region
#Region "更新批次零件序号信息，添加&更新"
    Public Function AddAndUpdateContainerProductNo(ByVal p_UpdateName As String, ByVal p_perform As String, ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                      ByVal p_dataProductList As Dictionary(Of String, List(Of ClientAPIEntity)), _
                                      ByRef strInfo As String) As Boolean

        AddAndUpdateContainerProductNo = False
        Dim inputData As csiObject
        Dim ResponseDocument As csiDocument
        Dim ObjectChanges As csiRevisionedObject
        Dim inputdata2 As csiObject

        Try

            InitializeSession()
            CreateDocumentandService("ContainerProductNoMaintDoc", "ContainerProductNoMaint")

            If p_perform = "load" Then
                inputdata2 = gService.inputData()
                inputdata2.namedObjectField("ObjectToChange").setRef(p_UpdateName)
            End If

            gService.perform(p_perform)

            inputData = gService.inputData()
            ObjectChanges = inputData.revisionedObjectField("ObjectChanges")

            '赋值
            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(ObjectChanges, p_dataEntityList(i))
            Next

            ''零件编号
            Dim ProductNoList As csiNamedSubentityList = ObjectChanges.namedSubentityList("ProductListItem")

            For i As Integer = 0 To p_dataProductList.Count - 1

                Dim ProductListItem As List(Of ClientAPIEntity) = p_dataProductList.Item(i)
                Dim ProductNoItem As csiNamedSubentity
                Dim strOprType As String = ""

                For j As Integer = 0 To ProductListItem.Count - 1
                    If ProductListItem(j).ClientDataName = "OprType" Then
                        If ProductListItem(j).ClientDataValue = "0" Then
                            strOprType = "Update"
                        ElseIf ProductListItem(j).ClientDataValue = "1" Then
                            strOprType = "Add"
                        End If
                    Else
                        If ProductListItem(j).ClientDataName = "Name" Then
                            If strOprType = "Add" Then
                                ProductNoItem = ProductNoList.appendItem(ProductListItem(j).ClientDataValue)
                            ElseIf strOprType = "Update" Then
                                ProductNoItem = ProductNoList.changeItemByName(ProductListItem(j).ClientDataValue)
                            End If
                        ElseIf ProductListItem(j).ClientDataName = "ProductNo" Then
                            ProductNoItem.dataField("Name").setValue(ProductListItem(j).ClientDataValue)
                        End If
                        SetStepDetailValue(ProductNoItem, ProductListItem(j))
                    End If
                Next

            Next

            gService.setExecute() '执行
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            AddAndUpdateContainerProductNo = True

        Catch e As Exception
            AddAndUpdateContainerProductNo = False
            strInfo = e.Message
        End Try
    End Function
#End Region
#Region "报工"
    Public Function WorkingReport(ByVal p_dataEntityList As List(Of ClientAPIEntity), _
                                  ByVal dtChildContainer As DataTable, ByRef strInfo As String) As Boolean

        Try

            '初始化函数
            Dim ResponseDocument As csiDocument
            InitializeSession()
            CreateDocumentandService("WorkingReportDoc", "WorkingReport")

            '参数赋值
            Dim inputData As csiObject
            inputData = gService.inputData

            For i As Integer = 0 To p_dataEntityList.Count - 1
                SetInputDataValue(inputData, p_dataEntityList(i))
            Next

            ''零件编号
            If IsNothing(dtChildContainer) = False Then
                If dtChildContainer.Rows.Count > 0 Then
                    For Each r As DataRow In dtChildContainer.Rows
                        inputData.dataList("ChildContainer").appendItem(r(0).ToString)
                    Next
                End If
            End If

            gService.setExecute()
            gService.requestData.requestField("CompletionMsg")

            ResponseDocument = gDocument.submit()
            strInfo = CheckForErrors(ResponseDocument)
            WorkingReport = True
        Catch e As Exception
            WorkingReport = False
            strInfo = e.Message
        End Try
    End Function
#End Region

End Class
