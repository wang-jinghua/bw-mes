﻿Public Class ClientAPIEntity

    Private m_ClientInputTypeEnum As InputTypeEnum
    Private m_ClientDataTypeEnum As DataTypeEnum
    Private m_ClientDataValue As Object
    Private m_ClientDataVersion As String
    Private m_ClientDataName As String
    Private m_ClientOtherValue As String
    Private m_ClientDataIsROR As Boolean = False

    Public Sub New()
    End Sub

    Public Sub New(ByVal ClientDataName As String, ByVal ClientInputTypeEnum As InputTypeEnum,
                   ByVal ClientDataTypeEnum As DataTypeEnum,
                   ByVal ClientDataValue As Object, ByVal ClientDataVersion As String)
        m_ClientInputTypeEnum = ClientInputTypeEnum
        m_ClientDataTypeEnum = ClientDataTypeEnum
        m_ClientDataValue = ClientDataValue
        m_ClientDataVersion = ClientDataVersion
        m_ClientDataName = ClientDataName
    End Sub

    Public Sub New(ByVal ClientDataName As String, ByVal ClientInputTypeEnum As InputTypeEnum,
               ByVal ClientDataTypeEnum As DataTypeEnum,
               ByVal ClientDataValue As Object, ByVal ClientDataVersion As String, ByVal ClientOtherValue As String)
        m_ClientInputTypeEnum = ClientInputTypeEnum
        m_ClientDataTypeEnum = ClientDataTypeEnum
        m_ClientDataValue = ClientDataValue
        m_ClientDataVersion = ClientDataVersion
        m_ClientDataName = ClientDataName
        m_ClientOtherValue = ClientOtherValue
    End Sub

    Public Sub New(ByVal ClientDataName As String,
                   ByVal ClientDataTypeEnum As DataTypeEnum,
                   ByVal ClientDataValue As Object, ByVal ClientDataVersion As String)
        m_ClientDataTypeEnum = ClientDataTypeEnum
        m_ClientDataValue = ClientDataValue
        m_ClientDataVersion = ClientDataVersion
        m_ClientDataName = ClientDataName
    End Sub

    Public Sub New(ByVal ClientDataName As String,
               ByVal ClientDataTypeEnum As DataTypeEnum,
               ByVal ClientDataValue As Object, ByVal ClientDataVersion As String, ByVal ClientOtherValue As String, ByVal ClientDataIsROR As Boolean)
        m_ClientDataTypeEnum = ClientDataTypeEnum
        m_ClientDataValue = ClientDataValue
        m_ClientDataVersion = ClientDataVersion
        m_ClientDataName = ClientDataName
        m_ClientDataIsROR = ClientDataIsROR
    End Sub

    Public Property ClientOtherValue()
        Get
            Return m_ClientOtherValue
        End Get
        Set(ByVal value)
            m_ClientOtherValue = value
        End Set
    End Property

    Public Property ClientDataName()
        Get
            Return m_ClientDataName
        End Get
        Set(ByVal value)
            m_ClientDataName = value
        End Set
    End Property

    Public Property ClientDataValue()
        Get
            Return m_ClientDataValue
        End Get
        Set(ByVal value)
            m_ClientDataValue = value
        End Set
    End Property

    Public Property ClientDataVersion()
        Get
            Return m_ClientDataVersion
        End Get
        Set(ByVal value)
            m_ClientDataVersion = value
        End Set
    End Property

    Public Property ClientInputTypeEnum()
        Get
            Return m_ClientInputTypeEnum
        End Get
        Set(ByVal value)
            m_ClientInputTypeEnum = value
        End Set
    End Property

    Public Property ClientDataTypeEnum()
        Get
            Return m_ClientDataTypeEnum
        End Get
        Set(ByVal value)
            m_ClientDataTypeEnum = value
        End Set
    End Property

    Public Property ClientDataIsROR()
        Get
            Return m_ClientDataIsROR
        End Get
        Set(ByVal value)
            m_ClientDataIsROR = value
        End Set
    End Property
End Class

Public Enum DataTypeEnum

    ''' <summary>
    ''' 明细数值型
    ''' </summary>
    ''' <remarks></remarks>
    DataField = 0

    ''' <summary>
    ''' 明细NDO类型
    ''' </summary>
    ''' <remarks></remarks>
    NamedObjectField = 1

    ''' <summary>
    ''' 明细RDO
    ''' </summary>
    ''' <remarks></remarks>
    RevisionedObjectField = 2

    ''' <summary>
    ''' 批次
    ''' </summary>
    ''' <remarks></remarks>
    ContainerField = 3

    ''' <summary>
    ''' 子类
    ''' </summary>
    ''' <remarks></remarks>
    subentityField = 4

    ''' <summary>
    ''' 子列表
    ''' </summary>
    ''' <remarks></remarks>
    DataList = 5

    ''' <summary>
    ''' NamedSubentity
    ''' </summary>
    ''' <remarks></remarks>
    NamedSubentityField = 6

    ''' <summary>
    ''' NamedObjectList，值格式Dictionary(Of Integer, String)
    ''' </summary>
    ''' <remarks></remarks>
    NamedObjectList = 7
    ''' <summary>
    ''' RevisionedObjectList,值格式Dictionary(Of Integer, SortedList(Of String, String))
    ''' </summary>
    ''' <remarks></remarks>
    RevisionedObjectList = 8
End Enum


Public Enum InputTypeEnum

    ''' <summary>
    ''' 明细
    ''' </summary>
    ''' <remarks></remarks>
    Details = 0

    ''' <summary>
    ''' 当前状态明细
    ''' </summary>
    ''' <remarks></remarks>
    CurrentStatusDetails = 1

End Enum






