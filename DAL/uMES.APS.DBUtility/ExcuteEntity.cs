﻿/*
'Copyright ?1995-2007, Camstar Systems, Inc. All Rights Reserved.
'Description:批次类
'Copyright (c) : 通力凯顿（北京）系统集成有限公司
'Writer:Wangjh
'create Date:2020-10-20
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uMES.LeanManufacturing.DBUtility
{
   public class ExcuteEntity
    {
        public ExcuteEntity() { }
        public ExcuteEntity(string tableName, ExcuteType excuteType) {
            TableName = tableName; ExcuteType = excuteType;
        }

       public  List<FieldEntity> ExcuteFileds { get; set; }//执行字段

       public List<FieldEntity> WhereFileds { get; set; }//条件

       public string TableName { get; set; }

        public ExcuteType ExcuteType { get;set;}

        /// <summary>
        /// 条件sql 例如：a in (c,d)适用于复杂条件
        /// </summary>
        public string strWhere { get; set; }
    }

    public enum ExcuteType
    {
        select=1,
        selectAll=2,
        insert=3,
        update=4,
        del=5
    }

}
