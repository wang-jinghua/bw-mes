﻿/*
'Copyright ?1995-2007, Camstar Systems, Inc. All Rights Reserved.
'Description:批次类
'Copyright (c) : 通力凯顿（北京）系统集成有限公司
'Writer:Wangjh
'create Date:2020-10-20
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uMES.LeanManufacturing.DBUtility
{
   public class FieldEntity
    {
        public FieldEntity() { }
        public FieldEntity(string name)
        {
            FieldName = name; 
        }
        public FieldEntity(string name, object value, FieldType type)
        {
            FieldName = name; FieldValue = value; FieldType=type;
        }
        public FieldEntity(string name,object  value, FieldType type,string connector) {
            FieldName = name; FieldValue = value; FieldType=type; Connector = connector;
        }

      public  string FieldName { get; set; }

      public object FieldValue { get; set; }
        
      public  FieldType FieldType { get; set; }

        //连接符， >、<、=
        private string _connector="";
        public string Connector { get { return _connector; } set { _connector = value; } }
       
    }

    public enum FieldType
    {
       Str=1,
       Date=2,
       Numer=3
    }


}
