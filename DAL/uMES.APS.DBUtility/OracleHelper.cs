﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
//using System.Data.OracleClient;
using System.Configuration;
using System.Collections;
using System.Data.Odbc;
using uMES.LeanManufacturing.ParameterDTO;
using Oracle.ManagedDataAccess.Client;

namespace uMES.LeanManufacturing.DBUtility
{
    public abstract class OracleHelper
    {
        private static readonly string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        private static readonly string mdcConnectionString = ConfigurationManager.AppSettings["mdcConnectionString"];
        private static readonly string odbcConnectionString = ConfigurationManager.AppSettings["OdbcConnectionString"];


        #region  执行简单SQL语句

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">SQL语句的哈希表（key为sql语句，value是该语句的OracleParameter[]）</param>
        public static void ExecuteSqlTranByHash(Dictionary<string, OracleParameter[]> SQLStringList)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                conn.Open();
                using (OracleTransaction trans = conn.BeginTransaction())
                {
                    OracleCommand cmd = new OracleCommand();
                    try
                    {
                        //循环
                        foreach (var myDE in SQLStringList)
                        {
                            string cmdText = myDE.Key.ToString();//获得键值
                            OracleParameter[] cmdParms = (OracleParameter[])myDE.Value;//通过键值获得值
                            PrepareCommand(cmd, conn, trans, cmdText, cmdParms);
                            int val = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                        //事务提交
                        trans.Commit();
                    }
                    catch
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// 例如数据添加、删除、更新操作
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                using (OracleCommand cmd = new OracleCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        int rows = cmd.ExecuteNonQuery();
                        return rows;
                    }
                    catch (OracleException E)
                    {
                        connection.Close();
                        throw new Exception(E.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// 当所有的执行语句全部执行成功，没有异常后才能进行事务的提交
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static void ExecuteSqlTran(ArrayList SQLStringList)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                conn.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = conn;
                OracleTransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int n = 0; n < SQLStringList.Count; n++)
                    {
                        string strsql = SQLStringList[n].ToString();
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    tx.Commit();//事务提交
                }
                catch (OracleException E)
                {
                    tx.Rollback();//事务回滚
                    throw new Exception(E.Message);
                }
            }
        }
        /// <summary>
        /// 执行带一个存储过程参数的的SQL语句。
        /// </summary>
        /// <param name="SQLString">SQL语句/或者是存储过程</param>
        /// <param name="content">参数内容,比如一个字段是格式复杂的文章，有特殊符号，可以通过这个方式添加</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, string content)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                OracleCommand cmd = new OracleCommand(SQLString, connection);
                OracleParameter myParameter = new OracleParameter("content", OracleDbType.Blob);
                myParameter.Value = content;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (OracleException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }
        /// <summary>
        /// 向数据库里插入图像格式的字段(和上面情况类似的另一种实例)
        /// </summary>
        /// <param name="strSQL">SQL语句</param>
        /// <param name="fs">图像字节,数据库的字段类型为image的情况</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSqlInsertImg(string strSQL, byte[] fs)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                OracleCommand cmd = new OracleCommand(strSQL, connection);
                OracleParameter myParameter = new OracleParameter("fs", OracleDbType.Blob);
                myParameter.Value = fs;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (OracleException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                using (OracleCommand cmd = new OracleCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        object obj = cmd.ExecuteScalar();//统计功能（一行一列）
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (OracleException e)
                    {
                        connection.Close();
                        throw new Exception(e.Message);
                    }
                }
            }
        }
        /// <summary>
        /// 执行查询语句，返回OracleDataReader
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>OracleDataReader</returns>
        public static OracleDataReader ExecuteReader(string strSQL)
        {
            OracleConnection connection = new OracleConnection(connectionString);
            OracleCommand cmd = new OracleCommand(strSQL, connection);
            try
            {
                connection.Open();
                OracleDataReader myReader = cmd.ExecuteReader();
                return myReader;
            }
            catch (OracleException e)
            {
                throw new Exception(e.Message);
            }

        }
        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    OracleDataAdapter command = new OracleDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (OracleException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }

        public static DataSet MdcQuery(string SQLString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    OracleDataAdapter command = new OracleDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (OracleException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }

        public static DataSet QueryByConn(string SQLString, string strConn)
        {
            using (OracleConnection connection = new OracleConnection(strConn))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    OracleDataAdapter command = new OracleDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (OracleException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }
        #endregion

        #region 执行带参数的SQL语句

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, params OracleParameter[] cmdParms)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (OracleException E)
                    {
                        throw new Exception(E.Message);
                    }
                }
            }
        }
        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">SQL语句的哈希表（key为sql语句，value是该语句的OracleParameter[]）</param>
        public static void ExecuteSqlTran(Hashtable SQLStringList)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            {
                conn.Open();
                using (OracleTransaction trans = conn.BeginTransaction())
                {
                    OracleCommand cmd = new OracleCommand();
                    try
                    {
                        //循环
                        foreach (DictionaryEntry myDE in SQLStringList)
                        {
                            string cmdText = myDE.Key.ToString();//获得键值
                            OracleParameter[] cmdParms = (OracleParameter[])myDE.Value;//通过键值获得值
                            PrepareCommand(cmd, conn, trans, cmdText, cmdParms);
                            int val = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                            //事务提交
                            trans.Commit();
                        }
                    }
                    catch
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString, params OracleParameter[] cmdParms)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        object obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (OracleException e)
                    {
                        throw new Exception(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回OracleDataReader
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>OracleDataReader</returns>
        public static OracleDataReader ExecuteReader(string SQLString, params OracleParameter[] cmdParms)
        {
            OracleConnection connection = new OracleConnection(connectionString);
            OracleCommand cmd = new OracleCommand();
            try
            {
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                OracleDataReader myReader = cmd.ExecuteReader();
                cmd.Parameters.Clear();
                return myReader;
            }
            catch (OracleException e)
            {
                throw new Exception(e.Message);
            }

        }

        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString, params OracleParameter[] cmdParms)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                OracleCommand cmd = new OracleCommand();
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, "ds");
                        cmd.Parameters.Clear();
                    }
                    catch (OracleException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    return ds;
                }
            }
        }

        /// <summary>
        ///在存在事务的情况下，新建数据库连接，执行sql语句，此处是关键，上面的所有功能操作都沿此而进行
        /// 此处的sql语句可能是增加、删除、更新、查询等等操作
        /// Prepare a command for execution
        /// </summary>
        /// <param name="cmd">OracleCommand对象</param>
        /// <param name="conn">OracleConnection对象</param>
        /// <param name="trans">OracleTransaction对象</param>
        /// <param name="cmdType">Cmd type e.g. 存储过程或者sql语句</param>
        /// <param name="cmdText">Command text, e.g. Select * from Products</param>
        /// <param name="cmdParms">OracleParameters to use in the command</param>
        private static void PrepareCommand(OracleCommand cmd, OracleConnection conn, OracleTransaction trans, string cmdText, OracleParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (OracleParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }

        #endregion

        #region 存储过程操作

        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>OracleDataReader</returns>
        public static OracleDataReader RunProcedure(string storedProcName, IDataParameter[] parameters)
        {
            OracleConnection connection = new OracleConnection(connectionString);
            OracleDataReader returnReader;
            connection.Open();
            OracleCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.CommandType = CommandType.StoredProcedure;
            returnReader = command.ExecuteReader();
            return returnReader;
        }

        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="tableName">DataSet结果中的表名</param>
        /// <returns>DataSet</returns>
        public static DataSet RunProcedure(string storedProcName, IDataParameter[] parameters, string tableName)
        {
            try
            {
                using (OracleConnection connection = new OracleConnection(connectionString))
                {
                    DataSet dataSet = new DataSet();
                    connection.Open();
                    OracleDataAdapter sqlDA = new OracleDataAdapter();
                    sqlDA.SelectCommand = BuildQueryCommand(connection, storedProcName, parameters);
                    sqlDA.Fill(dataSet, tableName);

                    connection.Close();
                    //trans.Dispose();
                    return dataSet;
                }
            }
            catch (Exception RunProcedureEx)
            {
                throw RunProcedureEx;
            }
        }
        /// <summary>
        /// 构建 OracleCommand 对象(用来返回一个结果集，而不是一个整数值)
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>OracleCommand</returns>
        private static OracleCommand BuildQueryCommand(OracleConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            OracleCommand command = new OracleCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach (OracleParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            return command;
        }

        /// <summary>
        /// 执行存储过程，返回影响的行数		
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="rowsAffected">影响的行数</param>
        /// <returns></returns>
        public static int RunProcedure(string storedProcName, IDataParameter[] parameters, out int rowsAffected)
        {
            int result = 0;
            try
            {
                using (OracleConnection connection = new OracleConnection(connectionString))
                {

                    connection.Open();
                    OracleCommand command = BuildIntCommand(connection, storedProcName, parameters);
                    OracleTransaction trans = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    if (trans != null)
                    {
                        command.Transaction = trans;
                        result = command.ExecuteNonQuery();
                        trans.Commit();
                    }
                }
            }
            catch (Exception RunProcedureEx)
            {
                throw RunProcedureEx;
            }
            rowsAffected = result;
            return result;
        }

        /// <summary>
        /// 执行存储过程，查询，并且有输出参数	
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="rowsAffected">影响的行数</param>
        /// <returns></returns>
        public static string RunProcedureQuery(string storedProcName, IDataParameter[] parameters, out int rowsAffected)
        {
            string outString = "";
            try
            {
                using (OracleConnection connection = new OracleConnection(connectionString))
                {

                    connection.Open();
                    OracleCommand command = BuildIntCommand(connection, storedProcName, parameters);
                    OracleTransaction trans = connection.BeginTransaction(IsolationLevel.ReadCommitted);
                    if (trans != null)
                    {
                        command.Transaction = trans;
                        command.ExecuteNonQuery();
                        trans.Commit();
                    }

                    outString = command.Parameters["TempID"].Value.ToString();//输出参数


                }
            }
            catch (Exception RunProcedureEx)
            {
                throw RunProcedureEx;
            }
            rowsAffected = 0;
            return outString;
        }

        /// <summary>
        /// 创建 OracleCommand 对象实例(用来返回一个整数值)	
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>OracleCommand 对象实例</returns>
        private static OracleCommand BuildIntCommand(OracleConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            OracleCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            return command;
        }

        /// <summary>
        /// 执行存储过程，没有返回任何信息，如插入数据等
        /// </summary>
        /// <param name="procname"></param>
        /// <param name="cmdParms"></param>
        public static void ExecuteProc(string procname, params OracleParameter[] cmdParms)
        {
            using (OracleConnection SqlConn = new OracleConnection(connectionString))
            {
                if (SqlConn.State == ConnectionState.Closed)
                {
                    SqlConn.Open();
                }
                OracleDataAdapter sdar = new OracleDataAdapter();
                sdar.SelectCommand = new OracleCommand();
                sdar.SelectCommand.Connection = SqlConn;
                sdar.SelectCommand.CommandText = procname;
                sdar.SelectCommand.CommandType = CommandType.StoredProcedure;
                if (cmdParms != null)
                {
                    foreach (OracleParameter parm in cmdParms)
                        sdar.SelectCommand.Parameters.Add(parm);
                }
                int i = sdar.SelectCommand.ExecuteNonQuery();
                sdar.Dispose();
            }

        }

        /// <summary>
        /// 执行参数化的Sql语句,返回DataTable
        /// </summary>
        /// <param name="p_ProcedureName">sql语句</param>
        /// <param name="p_OracleParameterArray">Parm数组</param>
        /// <returns>返回DataTable</returns>
        public static DataTable ExeParameDataTable(string p_ProcedureName, OracleParameter[] p_OracleParameterArray)
        {
            try
            {
                using (OracleConnection SqlConn = new OracleConnection(connectionString))
                {
                    if (SqlConn.State == ConnectionState.Closed)
                    {
                        SqlConn.Open();
                    }
                    DataTable dt = new DataTable();
                    OracleDataAdapter da = new OracleDataAdapter(p_ProcedureName, SqlConn);

                    foreach (OracleParameter Sq in p_OracleParameterArray)
                    {
                        da.SelectCommand.Parameters.Add(Sq);
                    }
                    da.Fill(dt);
                    da.SelectCommand.Parameters.Clear();
                    return dt;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 将金额从小写转换成大写字母书写
        /// </summary>
        /// <param name="endMoney">要转化的金额数目</param>
        /// <returns>返回金额大写书写</returns>
        public static string ReturnMaxMoney(double endMoney)
        {
            try
            {
                string returnstring = "";
                using (OracleConnection SqlConn = new OracleConnection(connectionString))
                {
                    if (SqlConn.State == ConnectionState.Closed)
                    {
                        SqlConn.Open();
                    }
                    using (OracleCommand comm = new OracleCommand("MoneytoChinese22", SqlConn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        //输入参数
                        comm.Parameters.Add("ChangeMoney", OracleDbType.Int32);
                        //输出参数
                        comm.Parameters.Add("ReturnDaXie", OracleDbType.Varchar2, 50);
                        //comm.Parameters["ReturnDaXie"].Direction = ParameterDirection.ReturnValue;
                        comm.Parameters["ChangeMoney"].Value = endMoney;
                        comm.Parameters["ReturnDaXie"].Direction = ParameterDirection.Output;
                        comm.ExecuteNonQuery();//执行存储过程
                        returnstring = comm.Parameters["ReturnDaXie"].Value.ToString();
                    }
                }
                return returnstring;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 批量执行Command对象列表
        /// </summary>
        public static void ExecCmdListTrans(List<OracleCommand> list)
        {
            using (OracleConnection OraConn = new OracleConnection(connectionString))
            {
                if (OraConn.State == ConnectionState.Closed)
                {
                    OraConn.Open();
                }
                OracleTransaction trans = OraConn.BeginTransaction();
                try
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].Connection = OraConn;
                        list[i].Transaction = trans;
                        list[i].ExecuteNonQuery();
                    }
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
        }
        #endregion

        public static int ExecuteSql(string SQLString, string ConnStr, params OracleParameter[] cmdParms)
        {
            using (OracleConnection connection = new OracleConnection(ConnStr))
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (OracleException E)
                    {
                        throw new Exception(E.Message);
                    }
                }
            }
        }

        public static DataTable GetDataTable(string strSQL)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                using (OracleCommand cmd = new OracleCommand(strSQL, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    OracleDataAdapter da = new OracleDataAdapter(cmd);

                    try
                    {
                        DataTable dt = new DataTable(); //保存记录集
                        da.Fill(dt);// '//填充数据数据集
                        return dt;
                    }
                    catch (OracleException E)
                    {
                        connection.Close();
                        throw new Exception(E.Message);
                    }
                }
            }
        }

        public static uMESPagingDataDTO GetPagingDataIns(string strSQL, int intCurrentPageNum, int intPageSize)
        {
            uMESPagingDataDTO dto = new uMESPagingDataDTO();
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                using (OracleCommand cmd = new OracleCommand("p_page.Pagination", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //分页索引。
                    OracleParameter pIndex = new OracleParameter("Pindex", OracleDbType.Int32);
                    pIndex.Direction = ParameterDirection.Input;
                    pIndex.Value = intCurrentPageNum;
                    //要查询的sql语句
                    OracleParameter pSql = new OracleParameter("Psql", OracleDbType.Varchar2);
                    pSql.Direction = ParameterDirection.Input;
                    pSql.Value = strSQL;
                    //页面显示的数据条数
                    OracleParameter pSize = new OracleParameter("Psize", OracleDbType.Int32);
                    pSize.Direction = ParameterDirection.Input;
                    pSize.Value = intPageSize;
                    //返回的分页数
                    OracleParameter pCount = new OracleParameter("Pcount", OracleDbType.Int32);
                    pCount.Direction = ParameterDirection.Output;
                    //返回的记录数
                    OracleParameter pRCount = new OracleParameter("Prcount", OracleDbType.Int32);
                    pRCount.Direction = ParameterDirection.Output;
                    //当前页数据记录集
                    OracleParameter pDataTabe = new OracleParameter("v_cur", OracleDbType.RefCursor);
                    pDataTabe.Direction = ParameterDirection.Output;
                    //添加存储过程的参数
                    cmd.Parameters.Add(pIndex);
                    cmd.Parameters.Add(pSql);
                    cmd.Parameters.Add(pSize);
                    cmd.Parameters.Add(pCount);
                    cmd.Parameters.Add(pRCount);
                    cmd.Parameters.Add(pDataTabe);
                    OracleDataAdapter da = new OracleDataAdapter(cmd);
                    try
                    {
                        DataTable dt = new DataTable(); //保存记录集
                        da.Fill(dt);// '//填充数据数据集
                        dto.PageCount = pCount.Value.ToString();// ';//获取分页数
                        dto.RowCount = pRCount.Value.ToString(); //';//获取记录数
                        dto.DBTable = dt;
                        return dto;
                    }
                    catch (OracleException E)
                    {
                        connection.Close();
                        throw new Exception(E.Message);
                    }
                }
            }
        }


        #region ExcuteDataBaseByEntity add;Wangjh 20201020
        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="excuteEntitys"></param>
        /// <returns></returns>
        public static DataTable QueryDataByEntity(ExcuteEntity excuteEntity) {

            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                
                OracleCommand cmd = new OracleCommand();
               
                string strSql = "", condition = "";
                if (excuteEntity.ExcuteType == ExcuteType.selectAll)
                {
                    strSql = string.Concat("select * from ", excuteEntity.TableName, " where 1=1");
                }
                else {
                    foreach (FieldEntity field in excuteEntity.ExcuteFileds)
                    {
                        strSql += string.Concat(",",field.FieldName);
                    }
                    if (strSql.Contains(","))
                        strSql = strSql.Substring(1);
                    strSql = string.Concat("select ",strSql," from ",excuteEntity.TableName," where 1=1");
                }
                if (excuteEntity.WhereFileds!=null)
                {
                    foreach (FieldEntity field in excuteEntity.WhereFileds)
                    {
                        if (field.Connector == "")
                            condition += string.Concat(" and ", field.FieldName, "=:", field.FieldName);
                        else
                            condition += string.Concat(" and ", field.FieldName, field.Connector, ":", field.FieldName);

                        OracleParameter cmdPara = new OracleParameter(field.FieldName, field.FieldValue);

                        if (field.FieldType == FieldType.Str)
                        {
                            cmdPara.OracleDbType = OracleDbType.Varchar2;
                        }
                        else if (field.FieldType == FieldType.Numer)
                            cmdPara.OracleDbType = OracleDbType.Int32;
                        else if (field.FieldType == FieldType.Date)
                            cmdPara.OracleDbType = OracleDbType.Date;

                        cmdPara.Direction = ParameterDirection.Input;
                        cmd.Parameters.Add(cmdPara);
                    }
                }

                if (!string.IsNullOrWhiteSpace(excuteEntity.strWhere))
                {
                    condition += excuteEntity.strWhere;
                }
               
                strSql = string.Concat(strSql,condition);

                cmd.Connection = connection;
                cmd.CommandText = strSql;
                cmd.CommandType = CommandType.Text;//cmdType;

                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    DataTable ds = new DataTable();
                    try
                    {
                        da.Fill(ds);

                        cmd.Parameters.Clear();
                    }
                    catch (OracleException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally {
                        cmd.Dispose();
                    }
                    return ds;
                }

            }
        }
      /// <summary>
      /// 生成条件
      /// </summary>
      /// <param name="field"></param>
      /// <param name="cmd"></param>
      /// <param name="thirdSql"></param>
      static void GenerateConditions(FieldEntity field, OracleCommand cmd,ref string thirdSql) {

            if (field.Connector == "")
                thirdSql += string.Concat(" and ", field.FieldName, "=:", field.FieldName);
            else
                thirdSql += string.Concat(" and ", field.FieldName, field.Connector, ":", field.FieldName);

            OracleParameter cmdPara = new OracleParameter(field.FieldName, field.FieldValue);

            if (field.FieldType == FieldType.Str)
                cmdPara.OracleDbType = OracleDbType.Varchar2;
            else if (field.FieldType == FieldType.Numer)
                cmdPara.OracleDbType = OracleDbType.Int32;
            else if (field.FieldType == FieldType.Date)
                cmdPara.OracleDbType = OracleDbType.Date;

            cmdPara.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(cmdPara);
        }
        /// <summary>
        /// 添加cmd
        /// </summary>
        /// <param name="field"></param>
        /// <param name="cmd"></param>
        static void addCmd(FieldEntity field, OracleCommand cmd) {
            OracleParameter cmdPara = new OracleParameter(field.FieldName, field.FieldValue);

            if (field.FieldType == FieldType.Str)
                cmdPara.OracleDbType = OracleDbType.Varchar2;
            else if (field.FieldType == FieldType.Numer)
                cmdPara.OracleDbType = OracleDbType.Int32;
            else if (field.FieldType == FieldType.Date)
                cmdPara.OracleDbType = OracleDbType.Date;

            cmdPara.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(cmdPara);
        }

        /// <summary>
        /// 执行数据
        /// </summary>
        /// <param name="excuteEntity"></param>
        /// <returns></returns>
        public static int ExecuteDataByEntity(ExcuteEntity excuteEntity) {

            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    try
                    {
                        connection.Open();
                        //PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        string strSql = "", sencondSql="", thirdSql = "";
                        if (excuteEntity.ExcuteType == ExcuteType.del) {
                            strSql =string.Concat("delete from ", excuteEntity.TableName);
                            if (excuteEntity.WhereFileds == null)
                                return -1;
                            if (excuteEntity.WhereFileds.Count == 0)
                                return -1;
                            //条件
                            thirdSql = " where 1=1 ";
                            foreach (FieldEntity field in excuteEntity.WhereFileds) {
                                GenerateConditions(field,cmd,ref thirdSql);
                            }
                        }
                        else if(excuteEntity.ExcuteType == ExcuteType.insert){
                            if (excuteEntity.ExcuteFileds == null)
                            {
                                return -1;
                            }
                            else {
                                if (excuteEntity.ExcuteFileds.Count == 0)
                                    return -1;
                            }
                            strSql = string.Concat("insert into ",excuteEntity.TableName);
                            sencondSql = " (";
                            string valuesSql = " values(";
                            foreach (FieldEntity field in excuteEntity.ExcuteFileds)
                            {
                                sencondSql +=string.Concat(field.FieldName,",");
                                valuesSql += string.Concat(":",field.FieldName,",");
                                addCmd(field,cmd);
                            }
                            sencondSql = sencondSql.Substring(0,sencondSql.Length-1);
                            valuesSql = valuesSql.Substring(0,valuesSql.Length-1);
                            sencondSql += string.Concat(")",valuesSql,")");
                          
                        }
                        else if (excuteEntity.ExcuteType == ExcuteType.update)
                        {
                            if (excuteEntity.ExcuteFileds == null)
                            {
                                return -1;
                            }
                            else
                            {
                                if (excuteEntity.ExcuteFileds.Count == 0)
                                    return -1;
                            }

                            strSql = string.Concat("update ",excuteEntity.TableName);
                            sencondSql = " set ";
                            foreach (FieldEntity field in excuteEntity.ExcuteFileds)
                            {
                                sencondSql += string.Concat(field.FieldName,"=:",field.FieldName,",");
                                addCmd(field, cmd);
                            }
                            sencondSql = sencondSql.Substring(0, sencondSql.Length - 1);
                            //条件
                            thirdSql = " where 1=1 ";
                            if (excuteEntity.WhereFileds != null) {
                                foreach (FieldEntity field in excuteEntity.WhereFileds)
                                {
                                    GenerateConditions(field, cmd, ref thirdSql);
                                }
                            }

                        }


                        if (!string.IsNullOrWhiteSpace(excuteEntity.strWhere))
                        {
                            thirdSql += excuteEntity.strWhere;
                        }

                        strSql = string.Concat(strSql,sencondSql,thirdSql);
                        cmd.Connection = connection;
                        cmd.CommandText = strSql;
                        cmd.CommandType = CommandType.Text;//cmdType;

                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (OracleException E)
                    {
                        throw new Exception(E.Message);
                    }
                }
            }

        }
        /// <summary>
        /// 批量执行数据
        /// </summary>
        /// <param name="excuteEntitys"></param>
        /// <returns></returns>
        public static int ExecuteDataByEntitys(List<ExcuteEntity> excuteEntitys) {
            int re = 0;
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                using (OracleTransaction trans = connection.BeginTransaction())
                {
                    OracleCommand cmd = new OracleCommand();
                    try
                    {
                        foreach (ExcuteEntity excuteEntity in excuteEntitys)
                        {
                            re = 0;
                            //PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                            string strSql = "", sencondSql = "", thirdSql = "";
                            if (excuteEntity.ExcuteType == ExcuteType.del)
                            {
                                strSql = string.Concat("delete from ", excuteEntity.TableName);
                                if (excuteEntity.WhereFileds == null)
                                {
                                    re = -1;
                                    throw new Exception("删除数据请给条件");
                                }
                                if (excuteEntity.WhereFileds.Count == 0)
                                { re = -1;
                                    throw new Exception("删除数据请给条件");
                                }
                                //条件
                                thirdSql = " where 1=1 ";
                                foreach (FieldEntity field in excuteEntity.WhereFileds)
                                {
                                    GenerateConditions(field, cmd, ref thirdSql);
                                }
                            }
                            else if (excuteEntity.ExcuteType == ExcuteType.insert)
                            {
                                if (excuteEntity.ExcuteFileds == null)
                                {
                                    re = -1;
                                    throw new Exception("插入数据请给插入字段");
                                }
                                else
                                {
                                    if (excuteEntity.ExcuteFileds.Count == 0)
                                    { re = -1;
                                        throw new Exception("插入数据请给插入字段");
                                    }
                                }
                                strSql = string.Concat("insert into ", excuteEntity.TableName);
                                sencondSql = " (";
                                string valuesSql = " values(";
                                foreach (FieldEntity field in excuteEntity.ExcuteFileds)
                                {
                                    sencondSql += string.Concat(field.FieldName, ",");
                                    valuesSql += string.Concat(":", field.FieldName, ",");
                                    addCmd(field, cmd);
                                }
                                sencondSql = sencondSql.Substring(0, sencondSql.Length - 1);
                                valuesSql = valuesSql.Substring(0, valuesSql.Length - 1);
                                sencondSql += string.Concat(")", valuesSql, ")");

                            }
                            else if (excuteEntity.ExcuteType == ExcuteType.update)
                            {
                                if (excuteEntity.ExcuteFileds == null)
                                {
                                    re = -1;
                                    throw new Exception("更新数据请给更新字段");
                                }
                                else
                                {
                                    if (excuteEntity.ExcuteFileds.Count == 0)
                                    { re = -1;
                                        throw new Exception("更新数据请给更新字段");
                                    }
                                }

                                strSql = string.Concat("update ", excuteEntity.TableName);
                                sencondSql = " set ";
                                foreach (FieldEntity field in excuteEntity.ExcuteFileds)
                                {
                                    sencondSql += string.Concat(field.FieldName, "=:", field.FieldName, ",");
                                    addCmd(field, cmd);
                                }
                                sencondSql = sencondSql.Substring(0, sencondSql.Length - 1);
                                //条件
                                thirdSql = " where 1=1 ";
                                if (excuteEntity.WhereFileds != null)
                                {
                                    foreach (FieldEntity field in excuteEntity.WhereFileds)
                                    {
                                        GenerateConditions(field, cmd, ref thirdSql);
                                    }
                                }
                            }


                            if (!string.IsNullOrWhiteSpace(excuteEntity.strWhere))
                            {
                                thirdSql += excuteEntity.strWhere;
                            }

                            //if (re == -1)
                            //{
                            //    trans.Rollback();
                            //    cmd.Dispose();
                            //    return -1;
                            //}

                            strSql = string.Concat(strSql, sencondSql, thirdSql);
                            cmd.Connection = connection;
                            cmd.CommandText = strSql;
                            cmd.CommandType = CommandType.Text;//cmdType;
                            cmd.Transaction = trans;

                            int rows = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                        //事务提交
                        trans.Commit();
                    }
                    catch (Exception  E)
                    {
                        trans.Rollback();
                        throw new Exception(E.Message);
                    }
                    finally {
                        cmd.Dispose();
                    }
                    
                }
            }
            return 1;
        }

        #endregion

    }
}