﻿/*
'Copyright ?1995-2007, Camstar Systems, Inc. All Rights Reserved.
'Description:检测项类
'Copyright (c) : 通力凯顿（北京）系统集成有限公司
'Writer:Wangjh
'create Date:2020-4-20
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.DBUtility;

namespace uMES.LeanManufacturing.ReportBusiness
{
   public class uMESCheckItemInfoBusiness
    {
        /// <summary>
        /// 保存检测项 新增，修改
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
       public ResultModel SaveCheckItemInfo(Dictionary<string,string> para) {
            ResultModel re = new ResultModel(false,"");

            string insertSql = @"insert into CheckItemInfo ci (ci.checkiteminfoid,ci.checkiteminfoname,ci.specid,ci.productid,ci.checkitem,keyspec,ci.firstcheck,ci.speccheck,ci.FactoryReCheck,ci.notes,ci.workflowid)
values('{0}','{1}','{2}','{3}','{4}',{5},{6},{7},{8},'{9}','{10}')";

            string updateSql = @"update CheckItemInfo ci set ci.checkiteminfoname='{0}',ci.specid='{1}',ci.productid='{2}',ci.checkitem='{3}',ci.keyspec={4},ci.firstcheck={5},ci.speccheck={6},ci.FactoryReCheck={7},ci.notes='{8}',
ci.workflowid='{9}' where ci.checkiteminfoid='{10}'";

            string sql = "";
            if (para["SaveType"]=="1")//插入
            {
                sql = string.Format(insertSql,para["CheckIteminfoId"], para["CheckIteminfoName"],para["SpecId"],para["ProductId"],para["CheckItem"],para["KeySpec"],para["FirstCheck"],para["SpecCheck"],para["FactoryReCheck"],
                    para["Notes"],para["WorkflowId"]);
            }
            else if (para["SaveType"] == "2")//更新
            {
                sql = string.Format(updateSql,  para["CheckIteminfoName"], para["SpecId"], para["ProductId"], para["CheckItem"], para["KeySpec"], para["FirstCheck"], para["SpecCheck"], para["FactoryReCheck"],
                   para["Notes"], para["WorkflowId"], para["CheckIteminfoId"]);
            }
            if (sql != "")
            {
                if (OracleHelper.ExecuteSql(sql) > 0)
                {
                    re.IsSuccess = true;
                }
            }


            return re;
        }

        /// <summary>
        /// 查询检测项信息
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
       public uMESPagingDataDTO GetCheckItemInfo(Dictionary<string, string> para) {

            string strSql = @"select ci.checkiteminfoid,ci.checkiteminfoname,ci.checkitem,ci.notes,ci.keyspec,ci.firstcheck,ci.speccheck,

                            p.productid,pb.productname,p.productrevision productrev,pb.productname||':'||p.productrevision productinfo,

                            w.workflowid,wb.workflowname,w.workflowrevision workflowrev,wb.workflowname||':'||w.workflowrevision workflowinfo,

                            s.specid,sb.specname,s.specrevision specrev ,ci.FactoryReCheck
                            from CheckItemInfo ci

                            left join product p on p.productid=ci.productid

                            left join productbase pb on pb.productbaseid=p.productbaseid

                            left join workflow w on w.workflowid=ci.workflowid

                            left join workflowbase wb on wb.workflowbaseid=w.workflowbaseid

                            left join spec s on s.specid=ci.specid

                            left join specbase sb on sb.specbaseid=s.specbaseid

                            where 1=1";

            if (para.ContainsKey("ProductId") &&!string.IsNullOrWhiteSpace(para["ProductId"])) {
                strSql += string.Format(" and ci.productid='{0}' ",para["ProductId"]);
            }
            if (para.ContainsKey("WorkflowId") && !string.IsNullOrWhiteSpace(para["WorkflowId"]))
            {
                strSql += string.Format(" and ci.WorkflowId='{0}' ", para["WorkflowId"]);
            }
            if (para.ContainsKey("SpecId") && !string.IsNullOrWhiteSpace(para["SpecId"]))
            {
                strSql += string.Format(" and ci.SpecId='{0}' ", para["SpecId"]);
            }


            uMESPagingDataDTO result = new uMESPagingDataDTO();

            result = OracleHelper.GetPagingDataIns(strSql, int.Parse(para["CurrentPageIndex"]), int.Parse(para["PageSize"]));
            return result;
        }

    /// <summary>
    /// 删除检测项
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
       public bool DeleteCheckItemInfo(string id) {
            string strSql = @"delete checkiteminfo ci where ci.checkiteminfoid='{0}'";

            strSql = string.Format(strSql,id);

            if (OracleHelper.ExecuteSql(strSql) > 0)
                return true;
            else
                return false;

        }
    }
}
