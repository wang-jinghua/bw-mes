﻿using System;
using System.Collections.Generic;
using System.Text;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.Common;
using System.Data;
using uMES.LeanManufacturing.DBUtility;

namespace uMES.LeanManufacturing.ReportBusiness
{
    public class uMESCommonBusiness
    {

        #region 将取消关联的子批次状态置为 2； 数量置为 0
        public int UpdateContainerState(string strContainerList)
        {
            int intResult = -1;
            StringBuilder sb = new StringBuilder();
            sb.Append("UPDATE container c SET c.qty =0,c.status = 2 WHERE c.containername IN ("+strContainerList+")");
            intResult = OracleHelper.ExecuteSql(sb.ToString());
            return intResult;

        }
        #endregion

        #region 获取数量调整原因
        public DataTable GetQtyAdjustReason()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT qar.*
                        FROM qtyadjustreason qar ");
            DataTable dt = OracleHelper.GetDataTable(sb.ToString());
            return dt;
        }
        #endregion

        #region 获取拆分后的批次号

        /// <summary>
        /// 获取批次查分等级
        /// </summary>
        /// <param name="strContainerID"></param>
        /// <returns></returns>
        public void ContainerSplitLevel(string strContainerID,ref int intLevel)
        {

      
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT c.containerid,c.containername,c.splitfromid
                        FROM container c
                        WHERE c.status = 1 AND c.containerid <> c.containername
                        AND c.splitfromid IS NOT  NULL ");
            sb.Append(" AND c.containerid ='" + strContainerID + "'");

            DataTable dt = OracleHelper.GetDataTable(sb.ToString());

            if (dt.Rows.Count > 0)
            {
                intLevel += 1;
                strContainerID = dt.Rows[0]["splitfromid"].ToString();
                ContainerSplitLevel(strContainerID,ref intLevel);
            }

        
        }

        public int ContainerSplitTime(string strContainerID)
        {

        
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT c.containerid,c.containername,c.splitfromid
                        FROM container c
                        WHERE c.status = 1 AND c.containerid <> c.containername ");
            sb.Append(" AND c.splitfromid ='" + strContainerID + "'");

            DataTable dt = OracleHelper.GetDataTable(sb.ToString());
            int intTime = dt.Rows.Count + 1;

            return intTime;

        }

        #endregion

        #region 获取质量记录的不合格产品序号
        public DataTable GetProductNoByQRIDList(DataTable dtQR)
        {
            string strQRIDList = string.Empty;

            foreach (DataRow row in dtQR.Rows)
            {
                string strQRID = row["QRID"].ToString();

                if (strQRIDList == string.Empty)
                {
                    strQRIDList = string.Format("'{0}'", strQRID);
                }
                else
                {
                    strQRIDList += string.Format(",'{0}'", strQRID);
                }
            }

            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT qrpi.containerid,qrpi.containername,qrpi.productno,qrpi.qty");
            strQuery.AppendLine("FROM qualityrecordproductnoinfo qrpi");
            strQuery.AppendLine(string.Format("WHERE qrpi.qualityrecordinfoid IN ({0})", strQRIDList));

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取报工单未做质量记录的不合格产品序号
        public DataTable GetNoQrProductNoByReportID(string strReportID)
        {
            DataTable dtProductNo = GetNonProductNoByReportID(strReportID);
            DataTable dtQRProductNo = GetQRProductNoByReportId(strReportID);

            DataTable DT = dtProductNo.Clone();

            foreach (DataRow row in dtProductNo.Rows)
            {
                string strContainerID = row["ContainerID"].ToString();

                Boolean isOK = true;
                foreach (DataRow r in dtQRProductNo.Rows)
                {
                    string strContID = r["ContainerID"].ToString();

                    if (strContID == strContainerID)
                    {
                        isOK = false;
                        break;
                    }
                }

                if (isOK == true)
                {
                    DT.Rows.Add(row.ItemArray);
                }
            }

            return DT;
        }
        #endregion

        #region 获取报工单已做质量记录的不合格产品序号
        public DataTable GetQRProductNoByReportId(string strReportID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT qrpi.containerid,qrpi.containername,qrpi.productno");
            strQuery.AppendLine("FROM qualityrecordproductnoinfo qrpi");
            strQuery.AppendLine("LEFT JOIN qualityrecordinfo qri ON qri.id = qrpi.qualityrecordinfoid");
            strQuery.AppendLine(string.Format("WHERE qri.reportinfoid = '{0}'", strReportID));

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取报工单的不合格产品序号
        public DataTable GetNonProductNoByReportID(string strReportID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT ccpi.containerid,ccpi.containername,ccpi.productno");
            strQuery.AppendLine("FROM conventioncheckproductnoinfo ccpi");
            strQuery.AppendLine("LEFT JOIN conventioncheckinfo cci ON cci.conventioncheckinfoid = ccpi.conventioncheckinfoid");
            strQuery.AppendLine("WHERE ccpi.hgorbhg = 0");
            strQuery.AppendLine(string.Format("AND cci.reportinfoid = '{0}'", strReportID));

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取ReleaseReason
        public DataTable GetReleaseReason()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT releasereasonid,releasereasonname");
            strQuery.AppendLine("FROM releasereason");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY releasereasonname ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取HoldReason
        public DataTable GetHoldReason()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT holdreasonid,holdreasonname");
            strQuery.AppendLine("FROM holdreason");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY holdreasonname ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取ChangeStatusReason
        public DataTable GetChangeStatusReason()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT changestatusreasonid,changestatusreasonname");
            strQuery.AppendLine("FROM changestatusreason");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY changestatusreasonname ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取LossReason
        public DataTable GetLossReason()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT lossreasonname,lossreasonid");
            strQuery.AppendLine("FROM lossreason");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY lossreasonname ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取RejectReason
        public DataTable GetRejectReason()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT rejectreasonname,rejectreasonid");
            strQuery.AppendLine("FROM rejectreason");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY rejectreasonname ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取ProblemLevel
        public DataTable GetProblemLevel()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT problemlevelname,problemlevelid");
            strQuery.AppendLine("FROM problemlevel");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY problemlevelname ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取ProblemType
        public DataTable GetProblemType()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT pt.problemtypename,pt.problemtypeid");
            strQuery.AppendLine("FROM problemtype pt");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY pt.problemtypename ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 获取RemoveReason
        public DataTable GetRemoveReason()
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT rr.removereasonname,rr.removereasonid");
            strQuery.AppendLine("FROM removereason rr");
            strQuery.AppendLine("WHERE 1 = 1");

            strQuery.AppendLine("ORDER BY rr.removereasonname ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 截取批次号
        public string GetProductNoFromContainerName(string strContainerName)
        {
            string[] strArray = strContainerName.Split('/');
            if (strArray.Length >= 2)
            {
                string strName = strArray[strArray.Length - 1];
                return strName.Substring(strName.Length - 3);
            }
            else
            {
                return strContainerName;
            }
        }
        #endregion

        #region 获取批次的产品序号
        public DataTable GetProductNo(string strContainerID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT c.containername,c.containerid,'' AS productno");
            strQuery.AppendLine("FROM container c");
            strQuery.AppendLine("WHERE c.status = 1");

            if (strContainerID != string.Empty)
            {
                strQuery.AppendLine(string.Format("AND c.parentcontainerid = '{0}'", strContainerID));
            }

            strQuery.AppendLine("ORDER BY c.containername ASC");

            DataTable DT = OracleHelper.GetDataTable(strQuery.ToString());

            foreach (DataRow row in DT.Rows)
            {
                string strContainerName = row["ContainerName"].ToString();
                row["ProductNo"] = GetProductNoFromContainerName(strContainerName);
            }

            return DT;
        }

        public DataTable GetProductNoStatus(string strContainerID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT c.containername,c.containerid,'' AS productno");
            strQuery.AppendLine("FROM container c");
            strQuery.AppendLine("WHERE 1 = 1");

            if (strContainerID != string.Empty)
            {
                strQuery.AppendLine(string.Format("AND c.parentcontainerid = '{0}'", strContainerID));
            }

            strQuery.AppendLine("ORDER BY c.containername ASC");

            DataTable DT = OracleHelper.GetDataTable(strQuery.ToString());

            foreach (DataRow row in DT.Rows)
            {
                string strContainerName = row["ContainerName"].ToString();
                row["ProductNo"] = GetProductNoFromContainerName(strContainerName);
            }

            return DT;
        }
        #endregion

        #region 获取Employee
        public DataTable GetEmployeeByFactory(string strFactoryID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT e.employeeid,e.employeename,e.fullname");
            strQuery.AppendLine("FROM employee e");
            strQuery.AppendLine("LEFT JOIN sessionvalues sv ON sv.sessionvaluesid = e.sessionvaluesid");
            strQuery.AppendLine("WHERE 1 = 1");

            if (strFactoryID != string.Empty)
            {
                strQuery.AppendLine(string.Format("AND sv.factoryid = '{0}'", strFactoryID));
            }

            strQuery.AppendLine("ORDER BY e.employeename ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }

        public DataTable GetEmployee(string strTeamID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT e.employeeid,e.employeename,e.fullname");
            strQuery.AppendLine("FROM employee e");
            strQuery.AppendLine("WHERE 1 = 1");

            if (strTeamID != string.Empty)
            {
                strQuery.AppendLine(string.Format("AND e.teamid = '{0}'", strTeamID));
            }

            strQuery.AppendLine("ORDER BY e.employeename ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }

        public DataTable GetEmployeeByRoles(string roles) {
            string strSql = @"select e.employeeid,e.employeename,e.fullname from employee e
left join employeerole er on er.employeeid=e.employeeid
left join roledef rd on rd.roleid=er.roleid";
            strSql += $" where rd.rolename in ({roles}) ";
            return OracleHelper.Query(strSql).Tables[0];
        }
        #endregion

        #region 获取Resource
        public DataTable GetResource(string strFactoryID, string strTeamID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT r.resourcename,r.resourceid");
            strQuery.AppendLine("FROM resourcedef r");
            strQuery.AppendLine("WHERE 1 = 1");

            if (strFactoryID != string.Empty)
            {
                strQuery.AppendLine(string.Format("AND r.factoryid = '{0}'", strFactoryID));
            }
            if (strTeamID != string.Empty)
            {
                strQuery.AppendLine(string.Format("AND r.teamid = '{0}'", strTeamID));
            }

            strQuery.AppendLine("ORDER BY r.resourcename ASC");

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 去除字符串中的空格
        public string FormatString(string str)
        {
            return str.Replace("&nbsp;", "");
        }
        #endregion

        #region 获取Team
        /// <summary>
        /// 获取Team
        /// </summary>
        public DataTable GetTeam(string strFactoryID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT t.teamid,t.teamname");
            strQuery.AppendLine("FROM team t");
            strQuery.AppendFormat("WHERE t.factoryid = '{0}'", strFactoryID);
            strQuery.AppendLine("ORDER BY t.teamname ASC");
            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 根据多个WorkflowID获取Spec列表
        /// <summary>
        /// 根据多个WorkflowID获取Spec列表
        /// </summary>
        /// <param name="strWorkflowID"></param>
        /// <returns></returns>
        public DataTable GetSpecListByWorkflowID(List<string> listWorkflowID)
        {
            string strWorkflowIDs = string.Empty;
            for (int i = 0; i < listWorkflowID.Count; i++)
            {
                strWorkflowIDs = strWorkflowIDs + string.Format("'{0}'", listWorkflowID[i]);
            }
            strWorkflowIDs = strWorkflowIDs.Replace("''", "','");

            if (strWorkflowIDs == string.Empty)
            {
                strWorkflowIDs = "''";
            }

            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT(CASE WHEN sb.specname IS NULL THEN sb1.specname ELSE sb.specname END) AS specname,");
            strQuery.AppendLine("   (CASE WHEN s.specrevision IS NULL THEN s1.specrevision ELSE s.specrevision END) AS specrevision,");
            strQuery.AppendLine("   (CASE WHEN s.specid IS NULL THEN s1.specid ELSE s.specid END) AS specid,");
            strQuery.AppendLine("   (CASE WHEN s.teamid IS NULL THEN s1.teamid ELSE s.teamid END) AS teamid,");
            strQuery.AppendLine("   (CASE WHEN s.resourcegroupid IS NULL THEN s1.resourcegroupid ELSE s.resourcegroupid END) AS resourcegroupid,");
            strQuery.AppendLine("   NVL((CASE WHEN s.unitworktime IS NULL THEN s1.unitworktime ELSE s.unitworktime END),1) AS unitworktime,");
            strQuery.AppendLine("   wfs.workflowid,wfs.sequence,wfs.workflowstepid,");
            strQuery.AppendLine("   (CASE WHEN o.worktimexs IS NULL THEN NVL(o1.worktimexs,1) ELSE NVL(o.worktimexs,1) END) AS worktimexs");
            strQuery.AppendLine("FROM workflowstep wfs");
            strQuery.AppendLine("LEFT JOIN spec s ON s.specid = wfs.specid");
            strQuery.AppendLine("LEFT JOIN specbase sb ON sb.specbaseid = s.specbaseid");
            strQuery.AppendLine("LEFT JOIN specbase sb1 ON sb1.specbaseid = wfs.specbaseid");
            strQuery.AppendLine("LEFT JOIN spec s1 ON s1.specid = sb1.revofrcdid");
            strQuery.AppendLine("LEFT JOIN operation o ON o.operationid = s.operationid");
            strQuery.AppendLine("LEFT JOIN operation o1 ON o1.operationid = s1.operationid");
            strQuery.AppendFormat("WHERE wfs.workflowid IN ({0})", strWorkflowIDs);
            strQuery.AppendLine("ORDER BY wfs.sequence ASC");

            DataTable DT = OracleHelper.GetDataTable(strQuery.ToString());
            return DT;
        }
        #endregion

        public string FormatParaStr(string strPara)
        {
            string str = "";
            str = strPara.Replace("'", "").Replace("%", "").Replace("&", "");
            return str;
        }

        #region 工序名称处理
        /// <summary>
        /// 剔除工序名称中的件号
        /// </summary>
        /// <param name="strSpecName"></param>
        /// <returns></returns>
        public string GetSpecNameWithOutProdName(string strSpecName)
        {
            string[] array = strSpecName.Split('-');
            int intLen = array.Length;

            string str = "";
            if (intLen >= 3)
            {
                str = array[intLen - 2].ToString() + "-" + array[intLen - 1].ToString();
            }
            else
            {
                str = strSpecName;
            }

            return str;
        }

        /// <summary>
        /// 从工序名称中获取工序号
        /// </summary>
        /// <param name="strSpecName"></param>
        /// <returns></returns>
        public string GetSpecNoFromSpecName(string strSpecName)
        {
            string[] array = strSpecName.Split('-');
            int intLen = array.Length;

            string str = "";
            //原取值>=3
            if (intLen >= 2)
            {
                str = array[intLen - 2].ToString();
            }
            else
            {
                str = strSpecName;
            }

            return str;
        }

        /// <summary>
        /// 从工序名称中获取真正的工序名称
        /// </summary>
        /// <param name="strSpecName"></param>
        /// <returns></returns>
        public string GetSpecNameFromSpecName(string strSpecName)
        {
            string[] array = strSpecName.Split('-');
            int intLen = array.Length;

            string str = "";
            if (intLen >= 3)
            {
                str = array[intLen - 1].ToString();
            }
            else
            {
                str = strSpecName;
            }

            return str;
        }
        #endregion

        //剔除批次名称中的件号
        public string GetContainerNameWithoutProduct(string strContainerName)
        {
            string[] strArray = strContainerName.Split('/');
            if (strArray.Length >= 2)
            {
                return strArray[strArray.Length - 1];
            }
            else
            {
                return strContainerName;
            }
        }

        public uMESPagingDataDTO PagingData(DataTable dt, int currentPage, int pageSize)
        {

            dt.Columns.Add("rowno", typeof(System.Int32));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["rowno"] = i + 1;
            }
            uMESPagingDataDTO dto = new uMESPagingDataDTO();
            dto.RowCount = dt.Rows.Count.ToString();

            if (dt.Rows.Count % pageSize != 0)
            {
                dto.PageCount = (dt.Rows.Count / pageSize + 1).ToString();
            }
            else
            {
                dto.PageCount = (dt.Rows.Count / pageSize).ToString();
            }
            int min = (currentPage - 1) * pageSize;
            int max = currentPage * pageSize;

            DataRow[] drs = dt.Select("rowno>" + min + " and rowno<=" + max, "rowno");

            DataTable tmp = dt.Clone();
            foreach (DataRow dr in drs)
            {
                tmp.ImportRow(dr);
            }

            dto.DBTable = tmp;
            return dto;
        }

        public int GetMonthDays(string strMonth)
        {
            DateTime date = Convert.ToDateTime(strMonth);
            int month = date.Month;
            int year = date.Year;


            int days = 0;
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            {
                days = 31;
            }
            else if (month == 4 || month == 6 || month == 9 || month == 11)
            {
                days = 30;
            }
            else
            { // 2月份，闰年29天、平年28天  
                if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
                {
                    days = 29;
                }
                else
                {
                    days = 28;
                }
            }

            return days;
        }

        /// <summary>
        /// 获取所有车间名称(factoryname)和描述(description)
        /// </summary>
        public DataTable GetFactoryNames()
        {
            string sql = "select f.factoryname, f.description, f.factoryid from factory f";
            DataTable dt = OracleHelper.GetDataTable(sql);
            return dt;
        }

        /// <summary>
        /// 获取车间描述
        /// </summary>
        public DataTable getFactoryName(string factoryname)
        {
            string sql = "select f.description from factory f";
            sql += string.Format(" where f.factoryname = '{0}'", factoryname);
            DataTable dt = OracleHelper.GetDataTable(sql);
            return dt;
        }

        public DataTable GetResource(string team)
        {
            string sql =
            "select rd.resourcename,  rd.resourceename\n" +
            "  from resourcedef rd\n" +
            "  left join team t on rd.teamid = t.teamid\n" +
            " where t.teamname = '" + team + "'";
            return OracleHelper.GetDataTable(sql);
        }

        #region 根据workflowid和specid获取workflowstep信息 add:Wangjh 20201026
        public DataTable GetStepInfo(string workflowid,string specid) {
            string strSql = @"select ws.* from workflow w 
left join workflowstep ws on ws.workflowid=w.workflowid
left join specbase sb on sb.specbaseid=ws.specbaseid
left join spec s on s.specid=nvl(sb.revofrcdid,ws.specid)";
            strSql += $" where w.workflowid='{workflowid}' and s.specid='{specid}' ";

            return OracleHelper.Query(strSql).Tables[0];
        }
        #endregion

        #region 系统日志存储
        public bool SaveMESAuditLog(MESAuditLog entity) {
          int i=  OracleHelper.ExecuteDataByEntity(new ExcuteEntity("MESAuditLog", ExcuteType.insert) {
                ExcuteFileds = new List<FieldEntity>() {
                    new FieldEntity("id",entity.id,FieldType.Str),
                    new FieldEntity("createemployeeid",entity.CreateEmployeeID,FieldType.Str),
                    new FieldEntity("createdate",entity.CreateDate,FieldType.Date),
                    new FieldEntity("parentid",entity.ParentID,FieldType.Str),
                     new FieldEntity("parentname",entity.ParentName,FieldType.Str),
                       new FieldEntity("BusinessName",entity.BusinessName,FieldType.Str),
                     new FieldEntity("description",entity.Description,FieldType.Str),
                      new FieldEntity("notes",entity.Notes,FieldType.Str),
                      new FieldEntity("OperationType",entity.OperationType,FieldType.Numer),
                       new FieldEntity("ContainerID",entity.ContainerID,FieldType.Str),
                      new FieldEntity("ContainerName",entity.ContainerName,FieldType.Str)
                }
            });
            bool re = false;
            if (i > 0)
                re = true;

            return re;
        }
        /// <summary>
        /// 批量存储
        /// </summary>
        /// <param name="entitys"></param>
        /// <returns></returns>
        public bool SaveMESAuditLogs(List<MESAuditLog> entitys)
        {
            List<ExcuteEntity> excuteEntitys = new List<ExcuteEntity>();
            ExcuteEntity excuteEntity = new ExcuteEntity();
            foreach (var entity in entitys) {
                excuteEntity = new ExcuteEntity("MESAuditLog", ExcuteType.insert);
                excuteEntity.ExcuteFileds = new List<FieldEntity>() {
                    new FieldEntity("id",entity.id,FieldType.Str),
                    new FieldEntity("createemployeeid",entity.CreateEmployeeID,FieldType.Str),
                    new FieldEntity("createdate",entity.CreateDate,FieldType.Date),
                    new FieldEntity("parentid",entity.ParentID,FieldType.Str),
                     new FieldEntity("parentname",entity.ParentName,FieldType.Str),
                       new FieldEntity("BusinessName",entity.BusinessName,FieldType.Str),
                     new FieldEntity("description",entity.Description,FieldType.Str),
                      new FieldEntity("notes",entity.Notes,FieldType.Str),
                      new FieldEntity("OperationType",entity.OperationType,FieldType.Numer),
                       new FieldEntity("ContainerID",entity.ContainerID,FieldType.Str),
                      new FieldEntity("ContainerName",entity.ContainerName,FieldType.Str)
                };
                excuteEntitys.Add(excuteEntity);
            }
           int i= OracleHelper.ExecuteDataByEntitys(excuteEntitys);
            bool re = false;
            if (i > 0)
                re = true;

            return re;
        }
        #endregion

        #region 根据entity操作数据库
        /// <summary>
        /// 单个执行
        /// </summary>
        /// <param name="excuteEntity"></param>
        /// <returns></returns>
        public bool ExecuteDataByEntity(ExcuteEntity excuteEntity) {
            int i = OracleHelper.ExecuteDataByEntity(excuteEntity);

            bool re = false;
            if (i > 0)
                re = true;

            return re;
        }

        /// <summary>
        /// 批量执行
        /// </summary>
        /// <param name="excuteEntitys"></param>
        /// <returns></returns>
        public bool ExecuteDataByEntitys(List<ExcuteEntity> excuteEntitys)
        {
            int i = OracleHelper.ExecuteDataByEntitys(excuteEntitys);

            bool re = false;
            if (i > 0)
                re = true;

            return re;
        }

        /// <summary>
        /// 查询数据根据entity
        /// </summary>
        /// <param name="excuteEntity"></param>
        /// <returns></returns>
        public DataTable QueryDataByEntity(ExcuteEntity excuteEntity) {
            return OracleHelper.QueryDataByEntity(excuteEntity);
        }

        #endregion

        /// <summary>
        ///     将DataTable B 合并至 DataTable A 中,A 与 B 的关系是 N:1, 
        ///     本方法将改变A表的结构,外键列名应在合并前设为相同的名称
        /// </summary>
        /// <param name="columns">两个表之间的外键</param>
        public DataTable CombineDataTale(DataTable A, DataTable B, List<string> columns)
        {
            //将B中的新列添加到A中
            for (int i = 0; i < B.Columns.Count; i++)
            {
                if (columns.Contains(B.Columns[i].ColumnName.ToLower()) == false)
                {
                    A.Columns.Add(B.Columns[i].ColumnName, Type.GetType("System.String"));
                }
            }
            //将外键对应的行从B复制到A中
            StringBuilder sb = null;
            DataRow[] drs = null;
            for (int i = 0; i < A.Rows.Count; i++)
            {
                sb = new StringBuilder();
                for (int j = 0; j < columns.Count; j++)
                {
                    sb.AppendFormat("{0} = '{1}' and ", columns[j], A.Rows[i][columns[j]]);
                }
                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 5, 5);
                }
                drs = B.Select(sb.ToString());
                if (drs.Length > 0)
                {
                    for (int k = 0; k < B.Columns.Count; k++)
                    {
                        A.Rows[i][B.Columns[k].ColumnName] = drs[0][k].ToString();
                    }
                }
            }
            return A;
        }
    }
}
