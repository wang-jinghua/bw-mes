﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.DBUtility;
using System.Data;

namespace uMES.LeanManufacturing.ReportBusiness
{
   public class BwMakeCalendarBusiness
    {
        /// <summary>
        /// 保存本次制造日历信息
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public ResultModel SaveMakeCalendar(Dictionary<string,string> para) {
            int n = int.Parse(para["ContinueDay"]);
            ArrayList strSqls = new ArrayList();
            string strSql = @"insert into bwMakeCalendar (bwMakeCalendarid,bwMakeCalendarname,Createdate,Createemployeeid,Description,Holidaydate,Holidayname)
values('{0}','{1}',sysdate,'{2}','{3}',to_date('{4}','yyyy-mm-dd hh24:mi:ss'),'{5}')";
            string deleteSql = @"delete bwMakeCalendar where Holidaydate=to_date('{0}','yyyy-mm-dd  hh24:mi:ss')";

            DateTime startDate = DateTime.Parse(para["StartDate"]);

            for (var i = 1; i <= n; i++) {
                DateTime holidayDate = startDate;
                strSqls.Add(string.Format(deleteSql,holidayDate));
                strSqls.Add(string.Format(strSql,Guid.NewGuid().ToString(),DateTime.Now.Ticks.ToString(),para["CreateEmployeeID"],para["Description"],holidayDate,para["HolidayName"]));
                startDate = startDate.AddDays(1);
            }

            OracleHelper.ExecuteSqlTran(strSqls);

            return new ResultModel(true,"保存成功");
        }

        /// <summary>
        /// 批量保存假期
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public ResultModel SaveMakeCalendars(Dictionary<string, object> para) {
            var holidays = para["HolidayDates"] as string[];
            ArrayList strSqls = new ArrayList();
            string strSql = @"insert into bwMakeCalendar (bwMakeCalendarid,bwMakeCalendarname,Createdate,Createemployeeid,Description,Holidaydate,Holidayname)
values('{0}','{1}',sysdate,'{2}','{3}',to_date('{4}','yyyy-mm-dd hh24:mi:ss'),'{5}')";
            string deleteSql = @"delete bwMakeCalendar where Holidaydate=to_date('{0}','yyyy-mm-dd  hh24:mi:ss')";

            foreach (string date in holidays) {
                strSqls.Add(string.Format(deleteSql, date));
                strSqls.Add(string.Format(strSql, Guid.NewGuid().ToString(), DateTime.Now.Ticks.ToString(), para["CreateEmployeeID"], para["Description"], date, para["HolidayName"]));
            }
            OracleHelper.ExecuteSqlTran(strSqls);

            return new ResultModel(true, "保存成功");
        }

        /// <summary>
        /// 查询日历假期信息
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public DataTable GetMakeCalendarInfo(Dictionary<string, string> para) {
            string strSql = " select t.* from bwMakeCalendar t where 1=1 ";
            if (para.ContainsKey("Year") && !string.IsNullOrWhiteSpace(para["Year"])) {
                strSql += $" and to_char(t.holidaydate,'yyyy')={para["Year"]} ";
            }
            if (para.ContainsKey("Month") && !string.IsNullOrWhiteSpace(para["Month"]))
            {
                strSql += $" and to_char(t.holidaydate,'mm')={para["Month"]} ";
            }
            if (para.ContainsKey("BwMakeCalendarName") && !string.IsNullOrWhiteSpace(para["BwMakeCalendarName"]))
            {
                strSql += $" and t.Bwmakecalendarname='{para["BwMakeCalendarName"]}' ";
            }
            if (para.ContainsKey("HolidayDate") && !string.IsNullOrWhiteSpace(para["HolidayDate"]))
            {
                strSql += $" and t.HolidayDate=to_date('{para["HolidayDate"]}','yyyy-mm-dd hh24:mi:ss') ";
            }

            return OracleHelper.Query(strSql).Tables[0];
        }

        /// <summary>
        /// 查询日历假期信息,含有持续天数
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public DataTable GetMakeCalendarByHolidayDate(Dictionary<string, string> para)
        {
            string strSql = " select t.* from bwMakeCalendar t where t.HolidayDate=to_date('{0}','yyyy-mm-dd hh24:mi:ss') ";

            if (para.ContainsKey("BwMakeCalendarName") && !string.IsNullOrWhiteSpace(para["BwMakeCalendarName"]))
            {
                strSql += $" and t.Bwmakecalendarname='{para["BwMakeCalendarName"]}' ";
            }
            
            DataTable dt = OracleHelper.Query(string.Format(strSql, para["HolidayDate"])).Tables[0];

            if (dt.Rows.Count > 0) {
                dt.Columns.Add("CONTINUEDAY");
                DateTime startDate = DateTime.Parse(para["HolidayDate"]);string holidayName =dt.Rows[0]["holidayName"].ToString();
                var date = startDate;
                int count = 0;
                do
                {
                    date = date.AddDays(1); count++;
                } while (OracleHelper.GetSingle(string.Format(strSql+ " and t.holidayName='{1}' ", date.ToString("yyyy-MM-dd"),holidayName)) != null);

                while (OracleHelper.GetSingle(string.Format(strSql + " and t.holidayName='{1}' ", startDate.AddDays(-1).ToString("yyyy-MM-dd"), holidayName)) != null) {
                    count++;
                    startDate = startDate.AddDays(-1);
                }

                dt.Rows[0]["ContinueDay"] = count;
            }

            return dt;
        }

        /// <summary>
        /// 根据参数删除假期
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public ResultModel DeleteHolidayByParams(Dictionary<string, string> para) {
            ResultModel re = new ResultModel(false,"");

            string strSql = "delete bwMakeCalendar t where 1=1 ";
            string conditionSql = "";
            if (para.ContainsKey("BwMakeCalendarName") && !string.IsNullOrWhiteSpace(para["BwMakeCalendarName"])) {
                conditionSql += $" and BwMakeCalendarName='{para["BwMakeCalendarName"]}' ";
            }
            if (para.ContainsKey("BwMakeCalendarID") && !string.IsNullOrWhiteSpace(para["BwMakeCalendarID"]))
            {
                conditionSql += $" and BwMakeCalendarID='{para["BwMakeCalendarID"]}' ";
            }
            if (para.ContainsKey("HolidayName") && !string.IsNullOrWhiteSpace(para["HolidayName"]))
            {
                conditionSql += $" and HolidayName='{para["HolidayName"]}' ";
            }
            if (para.ContainsKey("HolidayDate") && !string.IsNullOrWhiteSpace(para["HolidayDate"]))
            {
                conditionSql += $" and t.HolidayDate=to_date('{para["HolidayDate"]}','yyyy-mm-dd hh24:mi:ss') ";
            }

            if (string.IsNullOrWhiteSpace(conditionSql)) {
                return new ResultModel(true, "");
            }

            int i = OracleHelper.ExecuteSql(string.Concat(strSql,conditionSql));

            if (i > 0)
            { re.IsSuccess = true; re.Message = "删除成功"; }
            return re;
        }
    }
}
