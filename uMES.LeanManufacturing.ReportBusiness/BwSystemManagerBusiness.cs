﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.DBUtility;

namespace uMES.LeanManufacturing.ReportBusiness
{
  public  class BwSystemManagerBusiness
    {

        #region 人员登录日志查询

        public uMESPagingDataDTO GetEmployeeLoginInfo(Dictionary<string,string> para) {
            string strSql = @"select e.fullname,u.employeename,to_char(u.logindate,'yyyy-MM-dd HH24:mi:ss') logindate,u.loginip,u.userlogininfoid,
decode(u.logintype,0,'正常登陆',1,'刷卡登陆',2,'单点登陆') logintype,decode(u.isonline,0,'离线','在线') isonline from userlogininfo u
left join employee e on e.employeename=u.employeename
where 1=1";
            if (para.ContainsKey("EmployeeName") && !string.IsNullOrWhiteSpace(para["EmployeeName"])) {
                strSql += $" and u.employeename like '%{para["EmployeeName"]}%' ";
            }
            if (para.ContainsKey("StartDate") && !string.IsNullOrWhiteSpace(para["StartDate"]))
            {
                strSql += $" and u.logindate>=to_date('{para["StartDate"]}','yyyy-MM-dd HH24:mi:ss') ";
            }
            if (para.ContainsKey("EndDate") && !string.IsNullOrWhiteSpace(para["EndDate"]))
            {
                strSql += $" and u.logindate<=to_date('{para["EndDate"]}','yyyy-MM-dd HH24:mi:ss') ";
            }
            if (para.ContainsKey("IsOnLine") && !string.IsNullOrWhiteSpace(para["IsOnLine"]))
            {
                if(int.Parse(para["IsOnLine"])>-1)
                    strSql += $" and u.IsOnLine={para["IsOnLine"]}";
            }

            strSql += " order by u.logindate desc ";

            uMESPagingDataDTO result = new uMESPagingDataDTO();
            result = OracleHelper.GetPagingDataIns(strSql, int.Parse(para["CurrentPageIndex"]), int.Parse(para["PageSize"]));

            return result;
        }

        #endregion

        #region 系统操作记录查询

        public uMESPagingDataDTO GetMESAuditLogInfo(Dictionary<string, string> para)
        {
            string strSql = @"select ml.Containerid,ml.Containername,e.fullname,to_char(ml.createdate,'yyyy-MM-dd HH24:mi:ss') createdate,decode(ml.operationtype,0,'添加',1,'修改',2,'删除') operationtype,ml.businessname,ml.description,ml.notes,ml.parentid,ml.parentname from MESAuditLog ml 
left join employee e on e.employeeid=ml.createemployeeid
where 1=1";
            if (para.ContainsKey("ContainerName") && !string.IsNullOrWhiteSpace(para["ContainerName"]))
            {
                strSql += $" and ml.ContainerName like '%{para["ContainerName"]}%' ";
            }
            if (para.ContainsKey("EmployeeID") && !string.IsNullOrWhiteSpace(para["EmployeeID"]))
            {
                strSql += $" and ml.employeeid = '{para["EmployeeID"]}' ";
            }
            if (para.ContainsKey("BusinessName") && !string.IsNullOrWhiteSpace(para["BusinessName"]))
            {
                strSql += $" and ml.Businessname like '%{para["BusinessName"]}%' ";
            }
            if (para.ContainsKey("BusinessNames") && !string.IsNullOrWhiteSpace(para["BusinessNames"]))
            {
                strSql += $" and ml.Businessname in ({para["BusinessNames"]}) ";
            }
            if (para.ContainsKey("ParentName") && !string.IsNullOrWhiteSpace(para["ParentName"]))
            {
                strSql += $" and ml.ParentName = '{para["ParentName"]}' ";
            }
            if (para.ContainsKey("StartDate") && !string.IsNullOrWhiteSpace(para["StartDate"]))
            {
                strSql += $" and ml.createdate>=to_date('{para["StartDate"]}','yyyy-MM-dd HH24:mi:ss') ";
            }
            if (para.ContainsKey("EndDate") && !string.IsNullOrWhiteSpace(para["EndDate"]))
            {
                strSql += $" and ml.createdate<=to_date('{para["EndDate"]}','yyyy-MM-dd HH24:mi:ss') ";
            }
            if (para.ContainsKey("OperationType") && !string.IsNullOrWhiteSpace(para["OperationType"]))
            {
                if (int.Parse(para["OperationType"]) > -1)
                    strSql += $" and ml.OperationType={para["OperationType"]}";
            }

            strSql += " order by ml.createdate desc ";

            uMESPagingDataDTO result = new uMESPagingDataDTO();
            result = OracleHelper.GetPagingDataIns(strSql, int.Parse(para["CurrentPageIndex"]), int.Parse(para["PageSize"]));

            return result;
        }

        #endregion
    }
}
