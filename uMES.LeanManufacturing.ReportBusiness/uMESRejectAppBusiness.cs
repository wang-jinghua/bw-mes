﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using uMES.LeanManufacturing.DBUtility;
using uMES.LeanManufacturing.ParameterDTO;
using CamstarAPI;

namespace uMES.LeanManufacturing.ReportBusiness
{
    public class uMESRejectAppBusiness
    {
        uMESContainerBusiness containerBal = new uMESContainerBusiness();
        #region 获取工序协作信息
        public DataTable GetSynerAuditInfo(Dictionary<string, string> para)
        {
            DataTable dtReturn = new DataTable();

            string strConIDList = string.Empty;
            if (para.Keys.Contains("ContainerIDList"))
            {
                strConIDList = (string)para["ContainerIDList"];
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT st.containerid,st.workflowid,ws.sequence startseq,ws2.sequence endseq ,s.unitworktime,s.setupworktime 
                        FROM  synergicinfo st 
                        LEFT JOIN spec s ON s.specid=st.specid
                        LEFT JOIN specbase sb ON sb.specbaseid=s.specbaseid
                        LEFT JOIN workflowstep ws ON ws.workflowid=st.workflowid AND (ws.specbaseid=sb.specbaseid OR ws.specid=s.specid)
                        LEFT JOIN spec s2 ON s2.specid=st.returnspecid
                        LEFT JOIN specbase sb2 ON sb2.specbaseid=s2.specbaseid
                        LEFT JOIN workflowstep ws2 ON ws2.workflowid=st.workflowid AND (ws2.specbaseid=sb2.specbaseid OR ws2.specid=s2.specid)
                        WHERE st.returndate IS NOT NULL
                        AND st.containerid IN (" + strConIDList + @")");
            DataTable dt = OracleHelper.GetDataTable(sb.ToString());
            dt.Columns.Add("specid");
            dtReturn = dt.Clone();
            string strWorkFlowIDList = string.Empty;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string strFLowID = string.Empty;
                if (!string.IsNullOrEmpty(dt.Rows[i]["workflowid"].ToString()))
                {
                    strFLowID = dt.Rows[i]["workflowid"].ToString();
                }

                if (strWorkFlowIDList.Contains(",'" + strFLowID + "'") == false)
                {
                    strWorkFlowIDList += "'" + strFLowID + "',";
                }
            }

            if (strWorkFlowIDList != "")
            {
                strWorkFlowIDList = strWorkFlowIDList.TrimEnd(',');

                DataTable dtFolw = GetSpecInfo(strWorkFlowIDList);

                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string strContainerId = string.Empty;
                        if (!string.IsNullOrEmpty(dt.Rows[i]["containerid"].ToString()))
                        {
                            strContainerId = dt.Rows[i]["containerid"].ToString();
                        }

                        string strFLowID = string.Empty;
                        if (!string.IsNullOrEmpty(dt.Rows[i]["workflowid"].ToString()))
                        {
                            strFLowID = dt.Rows[i]["workflowid"].ToString();
                        }

                        string strStartSeq = string.Empty;
                        if (!string.IsNullOrEmpty(dt.Rows[i]["startseq"].ToString()))
                        {
                            strStartSeq = dt.Rows[i]["startseq"].ToString();
                        }

                        string strEndSeq = string.Empty;
                        if (!string.IsNullOrEmpty(dt.Rows[i]["endseq"].ToString()))
                        {
                            strEndSeq = dt.Rows[i]["endseq"].ToString();
                        }

                        DataRow[] dr = dtFolw.Select(string.Format("workflowid='{0}' and sequence>={1} and sequence<={2}", strFLowID, strStartSeq, strEndSeq));
                        if (dr.Length > 0)
                        {
                            for (int h = 0; h < dr.Length; h++)
                            {
                                DataRow row = dtReturn.NewRow();
                                row["containerid"] = strContainerId;
                                row["workflowid"] = strFLowID;
                                row["specid"] = dr[h]["specid"].ToString();
                                dtReturn.Rows.Add(row);
                            }
                        }
                    }


                }
            }
            return dtReturn;
        }

        public DataTable GetSpecInfo(string strWorkFlowId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT w.workflowid,ws.sequence,s.specid,sb2.specname,s.unitworktime,s.setupworktime 
                        FROM workflow w
                        LEFT JOIN workflowstep ws ON ws.workflowid=w.workflowid
                        LEFT JOIN specbase sb ON sb.specbaseid=ws.specbaseid
                        LEFT JOIN spec s ON s.specid=NVL(sb.revofrcdid,ws.specid)
                        LEFT JOIN specbase sb2 ON sb2.specbaseid=s.specbaseid
                        WHERE  1 = 1
                        AND w.workflowid IN (" + strWorkFlowId + @")");
            DataTable dt = OracleHelper.GetDataTable(sb.ToString());
            return dt;

        }
        #endregion

        #region  获取送检数量
        public string GetCheckQty(Dictionary<string, string> para)
        {
            string strCheckQty = string.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append(@"SELECT SUM(NVL(cck.eligibilityqty,0)+NVL(cck.nonsenseqty,0)) AS checkQty
                        FROM conventioncheckinfo cck
                        WHERE 1 = 1 ");
            sb.Append("AND cck.containerid ='" + para["ContainerID"] + "' ");
            sb.Append("AND cck.workflowid ='" + para["WorkflowID"] + "' ");
            sb.Append("AND cck.specid ='" + para["SpecID"] + "' ");

            DataTable dt = OracleHelper.GetDataTable(sb.ToString());
            if (dt.Rows.Count>0)
            {
                strCheckQty = dt.Rows[0]["checkQty"].ToString();
            }
            return strCheckQty;
        }
        #endregion

        #region 获取工序工时信息
        public DataTable GetSpecWorkTimeInfo(Dictionary<string,string> para)
        {
            string strSql = @" SELECT sb2.specname,s.specid,ws.sequence,s.unitworktime,s.setupworktime,o.operationname,
                                      f.factoryname,s.issynergic
                                FROM workflow w
                                LEFT JOIN workflowstep ws ON ws.workflowid = w.workflowid
                                LEFT JOIN specbase sb ON sb.specbaseid = ws.specbaseid
                                LEFT JOIN spec s ON s.specid = NVL(sb.revofrcdid, ws.specid)
                                LEFT JOIN specbase sb2 ON sb2.specbaseid = s.specbaseid
                                LEFT JOIN operation o ON s.operationid = o.operationid
                                LEFT JOIN factory f ON f.factoryid = s.factoryid
                                WHERE 1 = 1  ";

            StringBuilder sb = new StringBuilder();
            sb.Append(strSql);
            sb.Append("AND w.workflowid ='"+para["WorkflowID"] +"' ");
            sb.Append("AND s.specid ='" + para["SpecID"] + "' ");

            DataTable dt = OracleHelper.GetDataTable(sb.ToString());
            if (dt.Rows.Count>0)
            {
                sb.Append(" AND ws.sequence >='" + dt.Rows[0]["sequence"].ToString() + "' ");
                sb.Append(" ORDER BY ws.sequence");
                dt= OracleHelper.GetDataTable(sb.ToString());
            }
            return dt;
        }
        #endregion

        #region 批次关联/取消关联
        public Boolean AssociateOrDisassociate(Dictionary<string,string> para,List<string> listChildContainer, string strTxnName, out string strMessage)
        {
            string strContainerName = para["ContainerName"].ToString();

            List<ClientAPIEntity> m_DataList = new List<ClientAPIEntity>();

            ClientAPIEntity dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "Container";
            dataEntity.ClientDataValue = strContainerName;
            dataEntity.ClientOtherValue = "Lot";
            dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
            m_DataList.Add(dataEntity);

            List<ClientAPIEntity> p_ChildContainerList = new List<ClientAPIEntity>();
            foreach (string strChild in listChildContainer)
            {
                dataEntity = new ClientAPIEntity();
                dataEntity.ClientDataName = "Container";
                dataEntity.ClientDataValue = strChild;
                dataEntity.ClientOtherValue = "Lot";
                dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
                p_ChildContainerList.Add(dataEntity);
            }

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.AssociateOrDisassociate(strTxnName, m_DataList, p_ChildContainerList, ref strInfo);

            strMessage = strInfo;
            return result;
        }
        #endregion

        #region 批次继续
        public Boolean ReleaseContainer(Dictionary<string, object> para, out string strMessage)
        {
            string strContainerName = para["ContainerName"].ToString();

            List<ClientAPIEntity> m_DataList = new List<ClientAPIEntity>();

            ClientAPIEntity dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "Container";
            dataEntity.ClientDataValue = strContainerName;
            dataEntity.ClientOtherValue = "Lot";
            dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
            m_DataList.Add(dataEntity);

            dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "ReleaseReason";
            dataEntity.ClientDataValue = para["ReasonCode"].ToString();
            dataEntity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField;
            m_DataList.Add(dataEntity);

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.RunTxnService("ReleaseDoc", "Release", m_DataList, ref strInfo);

            strMessage = strInfo;
            return result;
        }
        #endregion

        #region 批次暂停
        public Boolean HoldContainer(Dictionary<string, object> para, out string strMessage)
        {
            string strContainerName = para["ContainerName"].ToString();

            List<ClientAPIEntity> m_DataList = new List<ClientAPIEntity>();

            ClientAPIEntity dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "Container";
            dataEntity.ClientDataValue = strContainerName;
            dataEntity.ClientOtherValue = "Lot";
            dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
            m_DataList.Add(dataEntity);

            dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "HoldReason";
            dataEntity.ClientDataValue = para["ReasonCode"].ToString();
            dataEntity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField;
            m_DataList.Add(dataEntity);

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.RunTxnService("HoldDoc", "Hold", m_DataList, ref strInfo);

            strMessage = strInfo;
            return result;
        }
        #endregion

        #region 批次打开
        public Boolean OpenContainer(Dictionary<string, object> para, out string strMessage)
        {
            string strContainerName = para["ContainerName"].ToString();

            List<ClientAPIEntity> m_DataList = new List<ClientAPIEntity>();

            ClientAPIEntity dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "Container";
            dataEntity.ClientDataValue = strContainerName;
            dataEntity.ClientOtherValue = "Lot";
            dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
            m_DataList.Add(dataEntity);

            dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "ChangeStatusReason";
            dataEntity.ClientDataValue = para["ReasonCode"].ToString();
            dataEntity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField;
            m_DataList.Add(dataEntity);

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.RunTxnService("OpenDoc", "Open", m_DataList, ref strInfo);

            strMessage = strInfo;
            return result;
        }
        #endregion

        #region 批次关闭
        public Boolean CloseContainer(Dictionary<string, object> para, out string strMessage)
        {
            string strContainerName = para["ContainerName"].ToString();

            List<ClientAPIEntity> m_DataList = new List<ClientAPIEntity>();

            ClientAPIEntity dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "Container";
            dataEntity.ClientDataValue = strContainerName;
            dataEntity.ClientOtherValue = "Lot";
            dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
            m_DataList.Add(dataEntity);

            dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "ChangeStatusReason";
            dataEntity.ClientDataValue = para["ReasonCode"].ToString();
            dataEntity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField;
            m_DataList.Add(dataEntity);

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.RunTxnService("CloseDoc", "Close", m_DataList, ref strInfo);

            strMessage = strInfo;
            return result;
        }
        #endregion

        #region 报废
        public Boolean ChangeQty(Dictionary<string, string> para, DataTable dtChangeQtyDetail, string strType, out string strMessage)
        {
            List<ClientAPIEntity> dataEntityList = new List<ClientAPIEntity>();
            ClientAPIEntity dataEntity = new ClientAPIEntity();

            dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
            dataEntity.ClientInputTypeEnum = InputTypeEnum.Details;
            dataEntity.ClientDataName = "Container";
            dataEntity.ClientDataValue = para["ContainerName"].ToString();
            dataEntity.ClientOtherValue = para["Level"].ToString();
            dataEntityList.Add(dataEntity);

            Dictionary<string, List<ClientAPIEntity>> p_SubentityListData = new Dictionary<string, List<ClientAPIEntity>>();

            for (int i = 0; i < dtChangeQtyDetail.Rows.Count; i++)
            {
                List<ClientAPIEntity> EntityList = new List<ClientAPIEntity>();

                dataEntity = new ClientAPIEntity();
                dataEntity.ClientDataTypeEnum = DataTypeEnum.DataField;
                dataEntity.ClientDataName = "Qty";
                dataEntity.ClientDataValue = dtChangeQtyDetail.Rows[i]["Qty"].ToString();
                EntityList.Add(dataEntity);

                dataEntity = new ClientAPIEntity();
                dataEntity.ClientDataTypeEnum = DataTypeEnum.NamedObjectField;
                dataEntity.ClientDataName = "ReasonCode";
                dataEntity.ClientDataValue = dtChangeQtyDetail.Rows[i]["ReasonCode"].ToString();
                EntityList.Add(dataEntity);

                p_SubentityListData.Add(i.ToString(), EntityList);
            }

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.ContainerChangeQty(dataEntityList, p_SubentityListData, strType, ref strInfo);

            //add:Wangjh 更改未指派的预派工数量
            if (result) {
                containerBal.UpdateTeamDispatchQty(para["ContainerName"].ToString());
            }

            strMessage = strInfo;
            return result;
        }
        #endregion

        #region 添加质疑单
        /// <summary>
        /// 添加质疑单
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public Boolean AddQuestionInfo(Dictionary<string, string> para, DataTable dtProductNo)
        {
            ArrayList SQLStringList = new ArrayList();

            //主表
            string strID = Guid.NewGuid().ToString();
            StringBuilder strSQL = new StringBuilder();
            strSQL.AppendLine("INSERT INTO questioninfo(ID,questioninfoname,rejectappinfoid,qty,uomid,opremployeeid,oprdate,notes)");
            strSQL.AppendLine("VALUES (");
            strSQL.AppendLine(string.Format("'{0}',", strID));
            strSQL.AppendLine(string.Format("'{0}',", para["QuestionInfoName"]));
            strSQL.AppendLine(string.Format("'{0}',", para["RejectAppInfoID"]));
            strSQL.AppendLine(string.Format("{0},", para["Qty"]));
            strSQL.AppendLine(string.Format("'{0}',", para["UomID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["OprEmployeeID"]));
            strSQL.AppendLine(string.Format("TO_DATE('{0}','YYYY-MM-DD HH24:MI:SS'),", para["OprDate"]));
            strSQL.AppendLine(string.Format("'{0}'", para["Notes"]));
            strSQL.AppendLine(")");

            SQLStringList.Add(strSQL.ToString());

            //产品序号
            if (dtProductNo.Rows.Count > 0)
            {
                foreach (DataRow row in dtProductNo.Rows)
                {
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("INSERT INTO questionproductnoinfo(ID,questioninfoid,containerid,containername,productno,notes)");
                    strSQL.AppendLine("VALUES(sys_guid(),");
                    strSQL.AppendLine(string.Format("'{0}',", strID));
                    strSQL.AppendLine(string.Format("'{0}',", row["ContainerID"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["ContainerName"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["ProductNo"]));
                    strSQL.AppendLine(string.Format("'{0}'", row["Notes"]));
                    strSQL.AppendLine(")");

                    SQLStringList.Add(strSQL.ToString());

                    //修改不合格品审理单中的信息
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("UPDATE rejectappproductnoinfo SET isq = 1");
                    strSQL.AppendLine(string.Format("WHERE ID = '{0}'", row["ID"]));

                    SQLStringList.Add(strSQL.ToString());
                }
            }

            OracleHelper.ExecuteSqlTran(SQLStringList);

            return true;
        }
        #endregion

        #region 添加返修单
        /// <summary>
        /// 添加返修单
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public Boolean AddRepairInfo(Dictionary<string, string> para, DataTable dtProductNo)
        {
            ArrayList SQLStringList = new ArrayList();

            //主表
            string strID = "";

            if (para.ContainsKey("RepairInfoID"))
                strID = para["RepairInfoID"];
            else
                strID= Guid.NewGuid().ToString();
            StringBuilder strSQL = new StringBuilder();
            strSQL.AppendLine("INSERT INTO repairinfo(ID,repairinfoname,rejectappinfoid,containerid,workflowid,specid,qty,");
            strSQL.AppendLine("       uomid,repaircontainerid,repaircontainername,repairworkflowid,opremployeeid,oprdate,notes)");
            strSQL.AppendLine("VALUES (");
            strSQL.AppendLine(string.Format("'{0}',", strID));
            strSQL.AppendLine(string.Format("'{0}',", para["RepairInfoName"]));
            strSQL.AppendLine(string.Format("'{0}',", para["RejectAppInfoID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["ContainerID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["WorkflowID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["SpecID"]));
            strSQL.AppendLine(string.Format("{0},", para["Qty"]));
            strSQL.AppendLine(string.Format("'{0}',", para["UomID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["RepairContainerID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["RepairContainerName"]));
            strSQL.AppendLine(string.Format("'{0}',", para["RepairWorkflowID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["OprEmployeeID"]));
            strSQL.AppendLine(string.Format("TO_DATE('{0}','YYYY-MM-DD HH24:MI:SS'),", para["OprDate"]));
            strSQL.AppendLine(string.Format("'{0}'", para["Notes"]));
            strSQL.AppendLine(")");

            SQLStringList.Add(strSQL.ToString());

            //产品序号
            if (dtProductNo.Rows.Count > 0)
            {
                foreach (DataRow row in dtProductNo.Rows)
                {
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("INSERT INTO Repairproductnoinfo(ID,repairinfoid,containerid,containername,productno,notes)");
                    strSQL.AppendLine("VALUES(sys_guid(),");
                    strSQL.AppendLine(string.Format("'{0}',", strID));
                    strSQL.AppendLine(string.Format("'{0}',", row["ContainerID"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["ContainerName"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["ProductNo"]));
                    strSQL.AppendLine(string.Format("'{0}'", row["Notes"]));
                    strSQL.AppendLine(")");

                    SQLStringList.Add(strSQL.ToString());

                    //修改不合格品审理单中的信息
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("UPDATE rejectappproductnoinfo SET status = 10");
                    strSQL.AppendLine(string.Format("WHERE ID = '{0}'", row["ID"]));

                    SQLStringList.Add(strSQL.ToString());
                }
            }

            OracleHelper.ExecuteSqlTran(SQLStringList);

            return true;
        }
        #endregion

        #region 非标准移动
        public Boolean MoveNonStd(Dictionary<string, string> para, out string strMessage)
        {
            string strContainerName = para["ContainerName"].ToString();

            string strWorkflowName = string.Empty;
            string strWorkflowRev = string.Empty;
            string strStepName = string.Empty;

            if (para.Keys.Contains("WorkflowName") == false)
            {
                string strWorkflowID = para["WorkflowID"].ToString();

                StringBuilder strQuery = new StringBuilder();
                strQuery.AppendLine("SELECT wfb.workflowname,wf.workflowrevision,wfs.workflowstepname");
                strQuery.AppendLine("FROM workflowstep wfs");
                strQuery.AppendLine("LEFT JOIN workflow wf ON wf.workflowid = wfs.workflowid");
                strQuery.AppendLine("LEFT JOIN workflowbase wfb ON wfb.workflowbaseid = wf.workflowbaseid");
                strQuery.AppendLine(string.Format("WHERE wf.workflowid = '{0}'", strWorkflowID));
                strQuery.AppendLine("ORDER BY wfs.sequence ASC");

                DataTable DT = OracleHelper.GetDataTable(strQuery.ToString());

                strWorkflowName = DT.Rows[0]["WorkflowName"].ToString();
                strWorkflowRev = DT.Rows[0]["WorkflowRevision"].ToString();
                strStepName = DT.Rows[0]["WorkflowStepName"].ToString();
            }
            else
            {
                strWorkflowName = para["WorkflowName"].ToString();
                strWorkflowRev = para["WorkflowRev"].ToString();
                strStepName = para["StepName"].ToString();
            }

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.ContainerOperByNotStandard(strContainerName, "Lot", "", ref strWorkflowName, ref strStepName, ref strWorkflowRev, ref strInfo);

            strMessage = strInfo;
            return result;
        }
        #endregion

        #region 批次拆分
        public Boolean SplitContainer(Dictionary<string, object> para, out string strMessage)
        {
            string strContainerName = para["ContainerName"].ToString();

            List<ClientAPIEntity> m_DataList = new List<ClientAPIEntity>();

            ClientAPIEntity dataEntity = new ClientAPIEntity();
            dataEntity.ClientDataName = "Container";
            dataEntity.ClientDataValue = strContainerName;
            dataEntity.ClientOtherValue = "Lot";
            dataEntity.ClientDataTypeEnum = DataTypeEnum.ContainerField;
            m_DataList.Add(dataEntity);

            Dictionary<string, List<ClientAPIEntity>> m_ChildList = new Dictionary<string, List<ClientAPIEntity>>();
            DataTable dtToContainerInfo = (DataTable)para["dtToContainerInfo"];

            int i = 0;
            foreach (DataRow row in dtToContainerInfo.Rows)
            {

                List<ClientAPIEntity> m_Child = new List<ClientAPIEntity>();

                ClientAPIEntity childEntity = new ClientAPIEntity();
                childEntity.ClientDataTypeEnum = DataTypeEnum.DataField;
                childEntity.ClientDataName = "ToContainerName";
                childEntity.ClientDataValue = row["ToContainerName"].ToString();
                m_Child.Add(childEntity);

                childEntity = new ClientAPIEntity();
                childEntity.ClientDataTypeEnum = DataTypeEnum.DataField;
                childEntity.ClientDataName = "Qty";
                childEntity.ClientDataValue = row["Qty"].ToString();
                m_Child.Add(childEntity);
                
                m_ChildList.Add(i.ToString(), m_Child);
                i++;
            }

            DataTable dtChildList = (DataTable)para["dtChildList"];

            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();
            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            Boolean result = api.Split(m_DataList, m_ChildList, dtChildList, ref strInfo);
            if (result)
            {
                //add;Wangjh 新批次的某些属性不能和源批次一样，需要置为初始 20201124
                string strSql = "update container c set c.IsAps=0 , c.State=0 where c.containername='{0}' ";
                foreach (DataRow row in dtToContainerInfo.Rows)
                {
                    OracleHelper.ExecuteSql(string.Format(strSql, row["ToContainerName"].ToString()));
                }
                 //

                    containerBal.UpdateTeamDispatchQty(strContainerName);
            }
            strMessage = strInfo;
            return result;
        }

        public Boolean ContainerMaint(Dictionary<string, object> para, ref string strMessage)
        {
            Boolean result = false ;
            string strInfo = string.Empty;
            string serverUser = para["ServerUser"].ToString();
            string serverPassword = para["ServerPassword"].ToString();

            CamstarClientAPI api = new CamstarClientAPI(serverUser, serverPassword);
            DataTable dtToContainerInfo = (DataTable)para["dtToContainerInfo"];
            foreach (DataRow row in dtToContainerInfo.Rows)
            {

                var m_DataList = new List<ClientAPIEntity>();
                ClientAPIEntity dataEntity = new ClientAPIEntity("PlannedStartDate", InputTypeEnum.Details, DataTypeEnum.DataField, row["PlannedStartDate"].ToString(), "");
                m_DataList.Add(dataEntity);


                dataEntity = new ClientAPIEntity("PlannedCompletionDate", InputTypeEnum.Details, DataTypeEnum.DataField, row["PlannedCompletionDate"].ToString(), "");
                m_DataList.Add(dataEntity);
          
                result = api.ContainerMaint(row["ToContainerName"].ToString(), "Lot", m_DataList, ref strInfo);
                strMessage = strInfo;
            }         
            return result;
        }
        #endregion
        
        #region 添加报废单
        /// <summary>
        /// 添加报废单
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public Boolean AddScrapInfo(Dictionary<string, string> para, DataTable dtProductNo)
        {
            ArrayList SQLStringList = new ArrayList();

            //主表
            string strID = "";
            if (para.ContainsKey("ScrapInfoID"))
                strID = para["ScrapInfoID"];
            else
                strID = Guid.NewGuid().ToString();

            StringBuilder strSQL = new StringBuilder();
            strSQL.AppendLine("INSERT INTO scrapinfo(ID,scrapinfoname,containerid,specid,");
            strSQL.AppendLine("       rejectappinfoid,qty,submitemployeeid,submitdate,notes,factoryid,employeeid,WorkflowID)");
            strSQL.AppendLine("VALUES (");
            strSQL.AppendLine(string.Format("'{0}',", strID));
            strSQL.AppendLine(string.Format("'{0}',", para["ScrapInfoName"]));
            strSQL.AppendLine(string.Format("'{0}',", para["ContainerID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["SpecID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["RejectAppInfoID"]));
            strSQL.AppendLine(string.Format("{0},", para["Qty"]));
            strSQL.AppendLine(string.Format("'{0}',", para["SubmitEmployeeID"]));
            strSQL.AppendLine(string.Format("TO_DATE('{0}','YYYY-MM-DD HH24:MI:SS'),", para["SubmitDate"]));
            strSQL.AppendLine(string.Format("'{0}',", para["Notes"]));
            strSQL.AppendLine(string.Format("'{0}',", para["FactoryID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["EmployeeID"]));
            strSQL.AppendLine(string.Format("'{0}'", para["WorkflowID"]));
            strSQL.AppendLine(")");

            SQLStringList.Add(strSQL.ToString());
            //OracleHelper.ExecuteSql(strSQL.ToString());

            //产品序号
            if (dtProductNo.Rows.Count > 0)
            {
                foreach (DataRow row in dtProductNo.Rows)
                {
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("INSERT INTO scrapproductnoinfo(ID,scrapinfoid,containerid,");
                    strSQL.AppendLine("     containername,productno,qty,uomid,lossreasonid,notes)");
                    strSQL.AppendLine("VALUES(sys_guid(),");
                    strSQL.AppendLine(string.Format("'{0}',", strID));
                    strSQL.AppendLine(string.Format("'{0}',",string.IsNullOrWhiteSpace(row["ContainerID"].ToString()) ? para["ContainerID"] : row["ContainerID"]));
                    strSQL.AppendLine(string.Format("'{0}',", string.IsNullOrWhiteSpace(row["ContainerName"].ToString()) ? para["ContainerName"] : row["ContainerName"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["ProductNo"]));
                    strSQL.AppendLine(string.Format("{0},", row["Qty"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["UOMID"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["LossReasonID"]));
                    strSQL.AppendLine(string.Format("'{0}'", row["Notes"]));
                    strSQL.AppendLine(")");

                    SQLStringList.Add(strSQL.ToString());
                    //OracleHelper.ExecuteSql(strSQL.ToString());

                    //修改不合格品审理单中的信息
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("UPDATE rejectappproductnoinfo SET status = 10");
                    strSQL.AppendLine(string.Format("WHERE ID = '{0}'", row["ID"]));

                    SQLStringList.Add(strSQL.ToString());
                    //OracleHelper.ExecuteSql(strSQL.ToString());
                }
            }

            OracleHelper.ExecuteSqlTran(SQLStringList);

            return true;
        }
        #endregion

        #region 获取不合格品审理单的产品序号
        public DataTable GetProductNoByRejectApp(string strRejectAppInfoID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT rapi.containerid,rapi.containername,rapi.productno,rapi.id,");
            strQuery.AppendLine("   DECODE(rapi.disposeresult,10,'让步使用',20,'返工/返修',30,'报废') AS disposeresult,");
            strQuery.AppendLine("   lr.lossreasonname,rapi.qty,rapi.status,rapi.lossreasonid,rapi.isq");
            strQuery.AppendLine("FROM rejectappproductnoinfo rapi");
            strQuery.AppendLine("LEFT JOIN lossreason lr ON lr.lossreasonid = rapi.lossreasonid");
            strQuery.AppendLine(string.Format("WHERE rapi.disposeresult is not null and  rapi.rejectappinfoid = '{0}'", strRejectAppInfoID));

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion

        #region 审核不合格品审理单

        public DataTable Getrejectapphistory(string strRejectAppinfoId)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("SELECT r.*,e.fullname");
            sb.AppendLine("FROM rejectapphistory r");
            sb.AppendLine("LEFT JOIN employee e ON e.employeeid = r.opremployeeid");
            sb.AppendLine("WHERE 1 = 1");
            sb.AppendLine("AND r.rejectappinfoid ='"+strRejectAppinfoId+"'");

            dt = OracleHelper.GetDataTable(sb.ToString());

            return dt;
        }

        public Boolean AppRejectAppInfo(Dictionary<string, string> paraH, Dictionary<string, string> paraM, DataTable dtProductNo)
        {
            ArrayList SQLStringList = new ArrayList();

            StringBuilder strSQL = new StringBuilder();
            strSQL.AppendLine("INSERT INTO rejectapphistory(ID,rejectappinfoid,oprtype,opremployeeid,");
            strSQL.AppendLine("     oprdate,notes");
            if (paraH.ContainsKey("AssessmentLevel"))
            {
                strSQL.AppendLine(",AssessmentLevel)");
            }
            else
            {
                strSQL.AppendLine(")");
            }
            strSQL.AppendLine("Values(sys_guid(),");
            strSQL.AppendLine(string.Format("'{0}',", paraH["RejectAppInfoID"]));
            strSQL.AppendLine(string.Format("{0},", paraH["OprType"]));
            strSQL.AppendLine(string.Format("'{0}',", paraH["OprEmployeeID"]));
            strSQL.AppendLine(string.Format("TO_DATE('{0}','YYYY-MM-DD HH24:MI:SS'),", paraH["OprDate"]));

            if (paraH.ContainsKey("AssessmentLevel"))
            {
                strSQL.AppendLine(string.Format("'{0}',", paraH["Notes"]));
                strSQL.AppendLine(string.Format("'{0}'", paraH["AssessmentLevel"]));
            }
            else
            {
                strSQL.AppendLine(string.Format("'{0}'", paraH["Notes"]));
            }

            strSQL.AppendLine(")");

            SQLStringList.Add(strSQL.ToString());
            //OracleHelper.ExecuteSql(strSQL.ToString());

            int intCount = 0;
            strSQL = new StringBuilder();
            strSQL.AppendLine("UPDATE rejectappinfo SET");

            if (paraM.Keys.Contains("Status"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("Status = {0}", paraM["Status"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("DesignEmployeeID"))//工艺员 Add;Wangjh
            {
                intCount++;
                strSQL.AppendLine(string.Format("DesignEmployeeID = '{0}'", paraM["DesignEmployeeID"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("DesignEmpNotes"))//设计员意见 Add;Wangjh
            {
                intCount++;
                strSQL.AppendLine(string.Format("DesignEmpNotes = '{0}'", paraM["DesignEmpNotes"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("DisposeNotes"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("DisposeNotes = '{0}'", paraM["DisposeNotes"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("DisposeNotes2"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("DisposeNotes2 = '{0}'", paraM["DisposeNotes2"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("DisposeNotes3"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("DisposeNotes3 = '{0}'", paraM["DisposeNotes3"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("DisposeNotes4"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("DisposeNotes4 = '{0}'", paraM["DisposeNotes4"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("RBQty"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("RBQty = {0}", paraM["RBQty"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("FixQty"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("FixQty = {0}", paraM["FixQty"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("ScrapQty"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("ScrapQty = {0}", paraM["ScrapQty"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            if (paraM.Keys.Contains("ResponsibleDepartment"))
            {
                intCount++;
                strSQL.AppendLine(string.Format("ResponsibleDepartment = '{0}'", paraM["ResponsibleDepartment"]));

                if (intCount < paraM.Count)
                {
                    strSQL.AppendLine(",");
                }
            }

            strSQL.AppendLine(string.Format("WHERE id = '{0}'", paraH["RejectAppInfoID"]));

            SQLStringList.Add(strSQL.ToString());
            //OracleHelper.ExecuteSql(strSQL.ToString());

            //产品序号处理结果
            if (dtProductNo != null)
            {
                foreach (DataRow row in dtProductNo.Rows)
                {
                    if (string.IsNullOrWhiteSpace(row["DisposeResult"].ToString()))
                        continue;
                    string strID = row["ID"].ToString();

                    if (strID != string.Empty)
                    {
                        strSQL = new StringBuilder();
                        strSQL.AppendLine("UPDATE rejectappproductnoinfo SET");
                        strSQL.AppendLine(string.Format("DisposeResult = {0},", row["DisposeResult"]));
                        strSQL.AppendLine(string.Format("qty = {0},", row["Qty"]));
                        strSQL.AppendLine(string.Format("LossReasonID = '{0}'", row["LossReasonID"]));
                        strSQL.AppendLine(string.Format("WHERE ID = '{0}'", strID));

                        SQLStringList.Add(strSQL.ToString());
                        //OracleHelper.ExecuteSql(strSQL.ToString());
                    }
                    else
                    {
                        strSQL = new StringBuilder();
                        strSQL.AppendLine("INSERT INTO rejectappproductnoinfo(ID,rejectappinfoid,containerid,containername,productno,");
                        strSQL.AppendLine("     qty,uomid,disposeresult,lossreasonid,disposeadviceid,disposenotes,qualitynotes,notes,status,isq)");
                        strSQL.AppendLine("VALUES(sys_guid(),");
                        strSQL.AppendLine(string.Format("'{0}',", paraH["RejectAppInfoID"]));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("{0},", row["Qty"]));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("{0},", row["DisposeResult"]));
                        strSQL.AppendLine(string.Format("'{0}',", row["LossReasonID"]));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("'{0}',", string.Empty));
                        strSQL.AppendLine(string.Format("{0},", "0"));
                        strSQL.AppendLine(string.Format("{0}", "0"));
                        strSQL.AppendLine(")");

                        SQLStringList.Add(strSQL.ToString());
                        //OracleHelper.ExecuteSql(strSQL.ToString());
                    }
                }
            }

            OracleHelper.ExecuteSqlTran(SQLStringList);

            return true;
        }
        #endregion

        #region 获取不合格品审理单列表
        #region 分页查询
        public uMESPagingDataDTO GetSourceData(Dictionary<string, string> para, int intPageIndex, int intPageSize)
        {
            string strSQL = GetSQL_D(para);

            uMESPagingDataDTO retR = OracleHelper.GetPagingDataIns(strSQL, intPageIndex, intPageSize);
            return retR;
        }
        #endregion

        #region 不分页查询，用于导出
        public DataTable GetAllDataForOutExcel(Dictionary<string, string> para)
        {
            string strSQL = GetSQL_D(para);

            DataTable DT = OracleHelper.GetDataTable(strSQL);
            return DT;
        }
        #endregion

        #region 组合查询语句
        protected string GetSQL_D(Dictionary<string, string> para)
        {
            StringBuilder strSQL = new StringBuilder();
            strSQL.AppendLine("SELECT rai.rejectappinfoname,mo.processno,mo.oprno,c.containername,");
            strSQL.AppendLine("     pb.productname,p.description,sb.specname,rai.qty,rai.qualitynotes,");
            strSQL.AppendLine("     e.fullname AS submitfullname,rai.submitdate,rai.id,rai.status,");
            strSQL.AppendLine("     rai.DisposeNotes,rai.DisposeNotes2,rai.DisposeNotes3,DisposeNotes4,rai.designempnotes,");
            strSQL.AppendLine("     rai.rbqty,rai.fixqty,rai.scrapqty,rai.specid,rai.workflowid,rai.containerid,c.productid,rai.responsibledepartment ");
            strSQL.AppendLine("FROM rejectappinfo rai");
            strSQL.AppendLine("LEFT JOIN container c ON c.containerid = rai.containerid");
            strSQL.AppendLine("LEFT JOIN mfgorder mo ON mo.mfgorderid = c.mfgorderid");
            strSQL.AppendLine("LEFT JOIN product p ON p.productid = c.productid");
            strSQL.AppendLine("LEFT JOIN productbase pb ON pb.productbaseid = p.productbaseid");
            strSQL.AppendLine("LEFT JOIN spec s ON s.specid = rai.specid");
            strSQL.AppendLine("LEFT JOIN specbase sb ON sb.specbaseid = s.specbaseid");
            strSQL.AppendLine("LEFT JOIN employee e ON e.employeeid = rai.submitemployeeid");
            strSQL.AppendLine("WHERE 1 = 1");

            if (para.Keys.Contains("Status"))
            {
                if (!string.IsNullOrEmpty(para["Status"]))
                {
                    strSQL.AppendLine(string.Format("AND rai.status IN ({0})", para["Status"]));
                }
            }
            if (para.Keys.Contains("ScanContainerName"))
            {
                if (!string.IsNullOrEmpty(para["ScanContainerName"]))
                {
                    strSQL.AppendLine(string.Format("AND c.containername = '{0}'", para["ScanContainerName"]));
                }
            }
            if (para.Keys.Contains("ProcessNo")) //工作令号
            {
                if (!string.IsNullOrEmpty(para["ProcessNo"]))
                {
                    strSQL.AppendLine(string.Format("AND LOWER(mo.processno) LIKE '%{0}%'", para["ProcessNo"].ToLower()));
                }
            }
            if (para.Keys.Contains("ContainerName")) //批次号
            {
                if (!string.IsNullOrEmpty(para["ContainerName"]))
                {
                    strSQL.AppendLine(string.Format("AND LOWER(c.containername) LIKE '%{0}%'", para["ContainerName"].ToLower()));
                }
            }
            if (para.Keys.Contains("ProductName")) //图号/名称
            {
                if (!string.IsNullOrEmpty(para["ProductName"]))
                {
                    strSQL.AppendLine(string.Format("AND (LOWER(pb.productname) LIKE '%{0}%' OR LOWER(p.description) LIKE '%{0}%')", para["ProductName"].ToLower()));
                }
            }
            if (para.Keys.Contains("SpecName")) //工序
            {
                if (!string.IsNullOrEmpty(para["SpecName"]))
                {
                    strSQL.AppendLine(string.Format("AND LOWER(sb.specname) LIKE '%{0}%'", para["SpecName"].ToLower()));
                }
            }
            if (para.Keys.Contains("StartDate")) //开始时间
            {
                if (!string.IsNullOrEmpty(para["StartDate"]))
                {
                    strSQL.AppendLine(string.Format("AND rai.submitdate >= TO_DATE('{0} 00:00:00','yyyy-MM-dd HH24:MI:SS')", para["StartDate"]));
                }
            }
            if (para.Keys.Contains("EndDate")) //结束时间
            {
                if (!string.IsNullOrEmpty(para["EndDate"]))
                {
                    strSQL.AppendLine(string.Format("AND rai.submitdate <= TO_DATE('{0} 23:59:59','yyyy-MM-dd HH24:MI:SS')", para["EndDate"]));
                }
            }

            //查询指定工艺/计划
            if (para.Keys.Contains("DesignEmployeeID"))
            {
                if (!string.IsNullOrEmpty(para["DesignEmployeeID"]))
                {
                    strSQL.AppendLine(string.Format("AND rai.designemployeeid = '{0}'", para["DesignEmployeeID"]));
                }
            }
            //查询指定设计员
            if (para.Keys.Contains("DesignEmpID"))
            {
                if (!string.IsNullOrEmpty(para["DesignEmpID"]))
                {
                    strSQL.AppendLine(string.Format("AND rai.designempid = '{0}'", para["DesignEmpID"]));
                }
            }
            strSQL.AppendLine("ORDER BY rai.submitdate desc");

            return strSQL.ToString();
        }
        #endregion
        #endregion

        #region 添加不合格品审理记录
        /// <summary>
        /// 添加不合格品审理记录
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public Boolean AddRejectAppInfo(Dictionary<string, string> para, DataTable dtQualityRecord, DataTable dtProductNo)
        {
            ArrayList SQLStringList = new ArrayList();

            //主表
            string strID = "";
            if (para.ContainsKey(""))
                strID = para["RejectappInfoID"];
            else
                strID= Guid.NewGuid().ToString();
            StringBuilder strSQL = new StringBuilder();
            strSQL.AppendLine("INSERT INTO rejectappinfo(ID,rejectappinfoname,containerid,specid,submitemployeeid,submitdate,submitnotes,");
            strSQL.AppendLine("       qty,uomid,rejectreasonid,designemployeeid,qualitynotes,notes,status,WorkflowID,responsibledepartment,DesignEmpID)");
            strSQL.AppendLine("VALUES (");
            strSQL.AppendLine(string.Format("'{0}',", strID));
            strSQL.AppendLine(string.Format("'{0}',", para["RejectAppInfoName"]));
            strSQL.AppendLine(string.Format("'{0}',", para["ContainerID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["SpecID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["SubmitEmployeeID"]));
            strSQL.AppendLine(string.Format("TO_DATE('{0}','YYYY-MM-DD HH24:MI:SS'),", para["SubmitDate"]));
            strSQL.AppendLine(string.Format("'{0}',", para["SubmitNotes"]));
            strSQL.AppendLine(string.Format("{0},", para["Qty"]));
            strSQL.AppendLine(string.Format("'{0}',", para["UOMID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["RejectReasonID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["DesignEmployeeID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["QualityNotes"]));
            strSQL.AppendLine(string.Format("'{0}',", para["Notes"]));
            strSQL.AppendLine(string.Format("{0},", para["Status"]));
            strSQL.AppendLine(string.Format("'{0}',", para["WorkflowID"]));
            strSQL.AppendLine(string.Format("'{0}',", para["ResponsiDepart"]));//add:Wangjh 增加责任部门
            strSQL.AppendFormat("'{0}'",para["DesignEmpID"]);//add：Wangjh
            strSQL.AppendLine(")");

            SQLStringList.Add(strSQL.ToString());
            //OracleHelper.ExecuteSql(strSQL.ToString());

            //质量记录
            if (dtQualityRecord != null)
            {
                foreach (DataRow row in dtQualityRecord.Rows)
                {
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("INSERT INTO rejectappqrinfo(Id,rejectappinfoid,qualityrecordinfoid)");
                    strSQL.AppendLine("VALUES(sys_guid(),");
                    strSQL.AppendLine(string.Format("'{0}',", strID));
                    strSQL.AppendLine(string.Format("'{0}'", row["QRID"]));
                    strSQL.AppendLine(")");

                    SQLStringList.Add(strSQL.ToString());
                    //OracleHelper.ExecuteSql(strSQL.ToString());

                    //修改质量记录的issubmit属性
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("UPDATE qualityrecordinfo SET issubmit = 1");
                    strSQL.AppendLine(string.Format("WHERE ID = '{0}'", row["QRID"]));

                    SQLStringList.Add(strSQL.ToString());
                }
            }

            //产品序号
            if (dtProductNo != null)
            {
                foreach (DataRow row in dtProductNo.Rows)
                {
                    strSQL = new StringBuilder();
                    strSQL.AppendLine("INSERT INTO rejectappproductnoinfo(ID,rejectappinfoid,containerid,containername,productno,");
                    strSQL.AppendLine("     qty,uomid,disposeresult,lossreasonid,disposeadviceid,disposenotes,qualitynotes,notes,status,isq)");
                    strSQL.AppendLine("VALUES(sys_guid(),");
                    strSQL.AppendLine(string.Format("'{0}',", strID));
                    strSQL.AppendLine(string.Format("'{0}',", row["ContainerID"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["ContainerName"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["ProductNo"]));
                    strSQL.AppendLine(string.Format("{0},", row["Qty"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["UOMID"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["DisposeResult"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["LossReasonID"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["DisposeAdviceID"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["DisposeNotes"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["QualityNotes"]));
                    strSQL.AppendLine(string.Format("'{0}',", row["Notes"]));
                    strSQL.AppendLine(string.Format("{0},", "0"));
                    strSQL.AppendLine(string.Format("{0}", "0"));
                    strSQL.AppendLine(")");

                    SQLStringList.Add(strSQL.ToString());
                    //OracleHelper.ExecuteSql(strSQL.ToString());
                }
            }

            OracleHelper.ExecuteSqlTran(SQLStringList);

            return true;
        }
        #endregion

        #region 获取设计/工艺人员
        public DataTable GetDesignEmployee(string strFactoryID, string strEmployeeID)
        {
            StringBuilder strQuery = new StringBuilder();
            strQuery.AppendLine("SELECT DISTINCT e.employeeid,e.fullname,rd.rolename");
            strQuery.AppendLine("FROM employeerole er");
            strQuery.AppendLine("LEFT JOIN employee e ON e.employeeid = er.employeeid");
            strQuery.AppendLine("LEFT JOIN sessionvalues sv ON sv.sessionvaluesid = e.sessionvaluesid");
            strQuery.AppendLine("LEFT JOIN roledef rd ON rd.roleid = er.roleid");
            strQuery.AppendLine("WHERE 1 = 1");
            strQuery.AppendLine("AND (rd.rolename LIKE '%设计%' OR rd.rolename LIKE '%工艺%')");
            //strQuery.AppendLine(string.Format("AND sv.factoryid = '{0}'", strFactoryID));
            strQuery.AppendLine(string.Format("AND e.employeeid <> '{0}'", strEmployeeID));

            return OracleHelper.GetDataTable(strQuery.ToString());
        }
        #endregion
    }
}
