﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MaterialAssortPopupForm.aspx.cs" Inherits="MaterialAssortPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>物料齐套</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                   工作令号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    批次号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispContainerID" runat="server" Visible="false"></asp:TextBox>
                </td> 
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    图号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispProductID" runat="server" Visible="false"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    名称：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdRight" align="left" nowrap="nowrap">
                    派工数量：
                </td>
                <td align="left" class="tdRight">
                    <asp:TextBox ID="txtDispQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispID" runat="server" Visible="False"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    工序：
                </td>
                <td align="left" class="tdRight">
                    <asp:TextBox ID="txtDispSpecName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispSpecID" runat="server" Visible="False"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    设备/工位：
                </td>
                <td align="left" class="tdRight">
                    <asp:TextBox ID="txtDispResourceName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    要求完成日期：
                </td>
                <td align="left" class="tdNoBorder">
                    <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div style="height:8px;width:100%;"></div>
        <div>
            <igtbl:UltraWebGrid ID="wgMaterialList" runat="server" Height="400px" Width="100%">
            <Bands>
                <igtbl:UltraGridBand>
                <Columns>
                <igtbl:UltraGridColumn Key="ProductName" Width="200px" BaseColumnName="ProductName" AllowGroupBy="No">
<Header Caption="图号">
<RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
</Header>

<Footer>
<RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
</Footer>
                        </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="200px" AllowGroupBy="No">
                        <header caption="名称">
                            <rowlayoutcolumninfo originx="1" />
                        </header>
                        <footer>
                            <rowlayoutcolumninfo originx="1" />
                        </footer>
                    </igtbl:UltraGridColumn>
                        <igtbl:UltraGridColumn BaseColumnName="QtyRequired" Key="QtyRequired" Width="100px" AllowGroupBy="No">
                            <header caption="单台数">
                                <rowlayoutcolumninfo originx="2" />
                            </header>
                            <footer>
                                <rowlayoutcolumninfo originx="2" />
                            </footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="RequireQty" Key="RequireQty" Width="100px">
                        <header caption="需求数量">
                            <rowlayoutcolumninfo originx="3" />
                        </header>
                        <footer>
                            <rowlayoutcolumninfo originx="3" />
                        </footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn Key="StoreQty1" Width="100px" BaseColumnName="StoreQty1">
                        <header caption="二级库库存">
                            <rowlayoutcolumninfo originx="4" />
                        </header>
                        <footer>
                            <rowlayoutcolumninfo originx="4" />
                        </footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="LackQty" Key="LackQty" Width="100px">
                        <Header Caption="缺件数量">
                            <RowLayoutColumnInfo OriginX="5" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="5" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn Key="StoreQty" Width="100px">
                        <Header Caption="ERP库存">
                            <RowLayoutColumnInfo OriginX="6" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="6" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID">
                        <Header Caption="ProductID">
                            <RowLayoutColumnInfo OriginX="7" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="7" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    </Columns>
                    <AddNewRow View="NotSet" Visible="NotSet">
                    </AddNewRow>
                </igtbl:UltraGridBand>       
            </Bands>
            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                RowHeightDefault="18px">
                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="400px" Width="100%">
                </FrameStyle>
                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                </RowAlternateStyleDefault>
                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                    StyleMode="QuickPages">
                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </PagerStyle>
                </Pager>
                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                </EditCellStyleDefault>
                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                </FooterStyleDefault>
                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    <Padding Bottom="3px" Top="2px" />
                    <Padding Top="2px" Bottom="3px"></Padding>
                </HeaderStyleDefault>
                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                    <Padding Left="3px" />
                </RowSelectorStyleDefault>
                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                    <Padding Left="3px" />
                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                </RowStyleDefault>
                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                </GroupByRowStyleDefault>
                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                </SelectedRowStyleDefault>
                <GroupByBox Hidden="True">
                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                    </BoxStyle>
                </GroupByBox>
                <AddNewBox>
                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </BoxStyle>
                </AddNewBox>
                <ActivationObject BorderColor="" BorderWidth="">
                </ActivationObject>
                <FilterOptionsDefault FilterUIType="HeaderIcons">
                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px" Height="420px" Width="200px">
                        <Padding Left="2px" />
                    </FilterDropDownStyle>
                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                    </FilterHighlightRowStyle>
                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px">
                        <Padding Left="2px" />
                    </FilterOperandDropDownStyle>
                </FilterOptionsDefault>
            </DisplayLayout>
        </igtbl:UltraWebGrid>
        </div>
    <div style="height:8px;width:100%;"></div>
    <div>
        <table style="width:100%;">
            <tr>
               <td style="text-align:left; width:100%;" colspan="2">
                    <asp:Button ID="btnMaterialApp" runat="server" Text="提交物料申请"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnMaterialApp_Click" />
                   <asp:Button ID="btnClose" runat="server" Text="关闭"
                        CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
               </td>
           </tr>
           <tr>
               <td style="font-size:12px; font-weight:bold;" nowrap="nowrap">状态信息：</td>
               <td style="text-align:left; width:100%;">
                   <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
               </td>
           </tr>
        </table>
    </div>
    </form>
</body>
</html>
