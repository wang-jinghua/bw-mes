﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="AssemblyRecordForm.aspx.cs" Inherits="AssemblyRecordForm" EnableViewState="true" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function openproduct() {
            var someValue = window.showModalDialog("uMES_ProductListForm.aspx", "", "dialogWidth:425px; dialogHeight:600px; status=no; center: Yes; resizable: NO;");
            if (someValue != "" && someValue != undefined) {
                var array = someValue.split(":");
                document.getElementById("<%=txtProductNameDisp.ClientID%>").value = array[0];
                document.getElementById("<%=txtProductName.ClientID%>").value = array[0];
                document.getElementById("<%=txtProductRevision.ClientID%>").value = array[1];
                document.getElementById("<%=txtProductID.ClientID%>").value = array[2];
                document.getElementById("<%=txtDescription.ClientID%>").value = array[3];
            }
        }

        function openproductno() {
            var someValue = window.showModalDialog("uMES_ProductNoForm.aspx", "", "dialogWidth:375px; dialogHeight:600px; status=no; center: Yes; resizable: NO;");
            if (someValue != "" && someValue != undefined) {
                var array = someValue.split(":");
                document.getElementById("<%=txtProductNo.ClientID%>").value = array[0];
                //document.getElementById("txtProductNoDisp").value = array[0];
                document.getElementById("<%=txtProductNoID.ClientID%>").value = array[1];
            }
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="9" class="tdBottom">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispContainerID" runat="server" class="stdTextBox" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtDispWorkflowID" runat="server" class="stdTextBox" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">图号：</div>
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispProductID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdBottom">
                        <div class="divLabel">名称：</div>
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">数量：</div>
                        <asp:TextBox ID="txtDispQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtChildCount" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">计划开始日期：</div>
                        <asp:TextBox ID="txtDispPlannedStartDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">计划完成日期：</div>
                        <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom" style=" display:none">
                        <div class="divLabel">作业令号：</div>
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td class="tdBottom" colspan="4">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="tdRight" align="left" nowrap="nowrap">
                        <div class="divLabel">产品序号：</div>
                        <asp:TextBox ID="txtProductNo" runat="server" class="stdTextBox" Width="200px"></asp:TextBox>
                        <input type="button" value="..." class="searchButton" onclick="openproductno()" />
                        <asp:TextBox ID="txtProductNoID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td class="tdNoBorder" colspan="7" align="left" nowrap="nowrap">
                        <div class="divLabel">工序：</div>
                        <asp:DropDownList ID="ddlSpec" runat="server" Width="155px" Height="28px" AutoPostBack="true"
                            Style="font-size: 16px;" OnSelectedIndexChanged="ddlSpec_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 650px;">
                        <div style="width: 650px;">
                            <igtbl:UltraWebGrid ID="wgItemList" runat="server" Height="300px" Width="100%" OnActiveRowChange="wgItemList_ActiveRowChange">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn Key="ProductName" Width="150px" BaseColumnName="ProductName">
                                                <Header Caption="图号">
                                                </Header>

                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px">
                                                <Header Caption="名称">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="RequireQty" Key="RequireQty" Width="80px">
                                                <Header Caption="需求数量">
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="AssemblyQty" Key="AssemblyQty" Width="90px">
                                                <Header Caption="已装配数量">
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="90px">
                                                <Header Caption="待装配数量">
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID">
                                                <Header Caption="ProductID">
                                                    <RowLayoutColumnInfo OriginX="5" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="5" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ItemID" Hidden="True" Key="ItemID">
                                                <Header Caption="ItemID">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ProductRevision" Hidden="false" Key="ProductRevision">
                                                <Header Caption="ProductRevision">
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                    RowHeightDefault="18px">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                        StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Top="2px" Bottom="3px"></Padding>
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </div>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td style="width: 100%;">
                        <div>
                            <igtbl:UltraWebGrid ID="wgAssemblyList" runat="server" Height="300px" Width="100%">
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="UltraWebGrid1"
                                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy"
                                    RowHeightDefault="18px">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                        StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                        <Padding Top="2px" Bottom="3px"></Padding>
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="FullName" Key="FullName" Width="80px">
                                                <Header Caption="操作者">
                                                </Header>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="AssemblyDate" Key="AssemblyDate" Width="150px" DataType="System.DateTime" Format="yyyy-MM-dd HH:mm">
                                                <Header Caption="操作时间">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px">
                                                <Header Caption="装配数量">
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="SerialNumber" Key="SerialNumber" Width="200px">
                                                <Header Caption="序列号">
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Notes" Key="Notes" Width="200px">
                                                <Header Caption="存在问题">
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                            </igtbl:UltraWebGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" style="width: 320px;" valign="top">
                        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                                    <div class="divLabel">物料编码：</div>
                                    <asp:TextBox ID="txtProductNameDisp" runat="server" class="stdTextBox" Width="200px"></asp:TextBox>
                                    <input type="button" value="..." class="searchButton" onclick="openproduct()" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                                    <div class="divLabel">数量：</div>
                                    <asp:TextBox ID="txtQty" runat="server" class="stdTextBox"></asp:TextBox>
                                    <asp:TextBox ID="txtAssemblyQty" runat="server" class="stdTextBox" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtRequireQty" runat="server" class="stdTextBox" Visible="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                                    <div class="divLabel">序列号：</div>
                                    <asp:TextBox ID="txtSerialNumber" runat="server" class="stdTextBox" Width="220px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                                    <div class="divLabel">存在问题：</div>
                                    <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" TextMode="MultiLine" Width="220px" Height="80px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdRight" colspan="2">
                                    <asp:Button ID="btnAdd" runat="server" Text="添加" OnClick="btnAdd_Click" />
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td colspan="2">
                                    <asp:TextBox ID="txtProductID" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtProductRevision" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td style="width: 100%;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left" nowrap="nowrap">
                                    <igtbl:UltraWebGrid ID="wgMaterialList" runat="server" Height="300px" Width="100%">
                                        <Bands>
                                            <igtbl:UltraGridBand>
                                                <Columns>
                                                    <igtbl:TemplatedColumn Width="30px" AllowGroupBy="No" Key="ckSelect">
                                                        <CellTemplate>
                                                            <asp:CheckBox ID="ckSelect" runat="server" />
                                                        </CellTemplate>
                                                        <Header Caption=""></Header>
                                                    </igtbl:TemplatedColumn>
                                                    <igtbl:UltraGridColumn Key="ProductName" Width="150px" BaseColumnName="ProductName">
                                                        <Header Caption="图号">
                                                        </Header>

                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px">
                                                        <Header Caption="名称">
                                                            <RowLayoutColumnInfo OriginX="1" />
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="1" />
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="90px">
                                                        <Header Caption="数量">
                                                            <RowLayoutColumnInfo OriginX="2" />
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="2" />
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn BaseColumnName="SerialNumber" Key="SerialNumber" Width="200px">
                                                        <Header Caption="序列号">
                                                            <RowLayoutColumnInfo OriginX="3" />
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="3" />
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn BaseColumnName="Notes" Key="Notes" Width="200px">
                                                        <Header Caption="存在问题">
                                                            <RowLayoutColumnInfo OriginX="4" />
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="4" />
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID">
                                                        <Header Caption="ProductID">
                                                            <RowLayoutColumnInfo OriginX="5" />
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="5" />
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn BaseColumnName="ItemID" Hidden="True" Key="ItemID">
                                                        <Header Caption="ItemID">
                                                            <RowLayoutColumnInfo OriginX="6" />
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="6" />
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                </Columns>
                                                <AddNewRow View="NotSet" Visible="NotSet">
                                                </AddNewRow>
                                            </igtbl:UltraGridBand>
                                        </Bands>
                                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                            RowHeightDefault="18px">
                                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                                            </FrameStyle>
                                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                            </RowAlternateStyleDefault>
                                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                                StyleMode="QuickPages">
                                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                </PagerStyle>
                                            </Pager>
                                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                            </EditCellStyleDefault>
                                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                            </FooterStyleDefault>
                                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                <Padding Bottom="3px" Top="2px" />
                                                <Padding Top="2px" Bottom="3px"></Padding>
                                            </HeaderStyleDefault>
                                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                                <Padding Left="3px" />
                                            </RowSelectorStyleDefault>
                                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                                <Padding Left="3px" />
                                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                            </RowStyleDefault>
                                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                            </GroupByRowStyleDefault>
                                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                            </SelectedRowStyleDefault>
                                            <GroupByBox Hidden="True">
                                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                                </BoxStyle>
                                            </GroupByBox>
                                            <AddNewBox>
                                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                </BoxStyle>
                                            </AddNewBox>
                                            <ActivationObject BorderColor="" BorderWidth="">
                                            </ActivationObject>
                                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                    Font-Size="11px" Height="420px" Width="200px">
                                                    <Padding Left="2px" />
                                                </FilterDropDownStyle>
                                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                                </FilterHighlightRowStyle>
                                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                    Font-Size="11px">
                                                    <Padding Left="2px" />
                                                </FilterOperandDropDownStyle>
                                            </FilterOptionsDefault>
                                        </DisplayLayout>
                                    </igtbl:UltraWebGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td valign="top">
                        <asp:Button ID="btnSelectAll" runat="server" Text="全选" OnClick="btnSelectAll_Click" /><br />
                        &nbsp;<br />
                        <asp:Button ID="btnReverseSelect" runat="server" Text="反选" OnClick="btnReverseSelect_Click" /><br />
                        &nbsp;<br />
                        <asp:Button ID="btnDel" runat="server" Text="删除" OnClick="btnDel_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;" colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="保存"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
