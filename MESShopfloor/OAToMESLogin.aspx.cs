﻿using System;
using uMES.LeanManufacturing.ReportBusiness;
using System.Collections.Generic;
using System.Data;

public partial class OAToMESLogin : System.Web.UI.Page
{
    private uMESLoginBusiness ulb = new uMESLoginBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.Count == 1)
        {
            string strUserName = Request.QueryString[0];

            DataTable DT = ulb.GetOAUserInfo(strUserName);

            if(DT.Rows.Count==0)
            {
                Response.Write("<script>alert('无效的账号')</script>");
            }
            else
            {
                int isShopfloorUser = Convert.ToInt32(DT.Rows[0]["IsShopfloorUser"].ToString());
                if (isShopfloorUser == 1)
                {
                    Response.Write("<script>window.location='/MESShopfloor/Login.aspx?userCode=" + strUserName + "'</script>");
                }
                else
                {
                    Response.Write("<script>window.location='/CamstarPortal/Default.aspx?userCode=" + strUserName + "'</script>");
                }
            }
        }
    }
}