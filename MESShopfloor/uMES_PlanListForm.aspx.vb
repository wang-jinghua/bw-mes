'Copyright ?1995-2007, Camstar Systems, Inc. All Rights Reserved.
'Description:零组件图号选择弹出界面
'Copyright (c):通力凯顿（北京）系统集成有限公司
'Writer:苗利刚
'create Date:2016.9.28
'Rewriter:
'Rewrite Date:

Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Camstar.WebClient.WebServicesProxy.InSiteWebServices
Imports System.Data
Imports uMES.LeanManufacturing.DBUtility
Imports uMES.LeanManufacturing.ReportBusiness

Partial Class uMES_PlanListForm
    Inherits System.Web.UI.Page

    Dim bll As uMESZZBusiness = New uMESZZBusiness()
    Dim intPageSize As Integer = 15

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                BindFamily()
                txtProcessNo.Text = ""
                txtPlanDate.Value = ""

                btnSearch_Click(sender, e)
            Catch ex As Exception
                tdMessage.InnerText = ex.Message
            End Try
        End If
    End Sub

#Region "绑定车型数据"
    Protected Sub BindFamily()
        Dim dt As DataTable = bll.GetFamily()
        ddlFamily.DataSource = dt
        ddlFamily.DataTextField = "ProductFamilyName"
        ddlFamily.DataValueField = "ProductFamilyID"
        ddlFamily.DataBind()
        ddlFamily.Items.Insert(0, New ListItem("", ""))
    End Sub
#End Region

    Protected Function GetTotalPage() As Integer
        ''确定总页数
        'Dim strQuery As String = GetSql.Replace("p.planno,p.id,p.plandate,p.processno,pf.productfamilyname", "COUNT(*)")
        Dim strQuery As String = GetSql()
        Dim DT As DataTable = OracleHelper.GetDataTable(strQuery)
        Dim intTotal As Integer = 0
        Dim intTotalPage As Integer = 0
        If DT.Rows.Count > 0 Then
            intTotal = CInt(DT.Rows(0)(4).ToString)
            intTotalPage = Int(intTotal / intPageSize)
        End If

        If intTotal Mod intPageSize <> 0 Then
            intTotalPage += 1
        End If

        txtTotalPage.Text = intTotalPage.ToString
        txtCurrentPage.Text = 0
        lLabel1.Text = "第  页  共 " & intTotalPage & " 页"

        GetTotalPage = intTotalPage
    End Function

    Protected Sub GetDataList(ByVal intPage As Integer)
        ddlFamily.SelectedValue = txtFamilyIDHidden.Text
        txtProcessNo.Text = txtProcessNoHidden.Text
        txtPlanDate.Value = txtPlanDateHidden.Text

        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)

        Dim strQuery As String = "SELECT * FROM" & vbCrLf
        strQuery += "(SELECT b.*, rownum AS rnum FROM (" & vbCrLf
        strQuery += GetSql() & vbCrLf
        strQuery += ") b" & vbCrLf
        strQuery += "WHERE rownum <= " & intPageSize * intPage & vbCrLf
        strQuery += ")WHERE rnum > " & intPageSize * (intPage - 1) & vbCrLf

        Dim DT As DataTable = OracleHelper.GetDataTable(strQuery)
        wglist.Clear()
        wglist.DataSource = DT
        wglist.DataBind()
        lLabel1.Text = "第 " & intPage & " 页  共 " & intTotalPage & " 页"
    End Sub

    Protected Function GetSql() As String
        Dim strFamilyID As String = ddlFamily.SelectedValue
        Dim strProcessNo As String = txtProcessNo.Text.Trim
        Dim strPlanDate As String = txtPlanDate.Value.Trim

        Dim strQuery As String = "SELECT p.planno,p.id,p.plandate,p.processno,rownum,p.qty,pf.productfamilyname,p.status,p.productfamilyid,pf.bomid" & vbCrLf & _
                                "FROM zzjlplan p" & vbCrLf & _
                                "LEFT JOIN productfamily pf ON pf.productfamilyid = p.productfamilyid" & vbCrLf & _
                                "WHERE 1 = 1" & vbCrLf & _
                                "AND (p.status = 0 OR p.status = 1) AND p.billtype = 'JLJH'" & vbCrLf

        If strFamilyID <> "" Then
            strQuery += "AND LOWER(p.productfamilyid) = '" & strFamilyID.ToLower & "'" & vbCrLf
        End If
        If strProcessNo <> "" Then
            strQuery += "AND LOWER(p.processno) LIKE '%" & strProcessNo.ToLower & "%'" & vbCrLf
        End If
        If strPlanDate <> "" Then
            strQuery += "AND p.plandate = TO_DATE('" & strPlanDate.ToLower & " 00:00:00','yyyy-MM-dd HH24:MI:SS')" & vbCrLf
        End If

        strQuery += "ORDER BY rownum desc" & vbCrLf

        GetSql = strQuery
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            txtFamilyIDHidden.Text = ddlFamily.SelectedValue
            txtProcessNoHidden.Text = txtProcessNo.Text.Trim
            txtPlanDateHidden.Text = txtPlanDate.Value.Trim

            wglist.Clear()
            txtCurrentPage.Text = 1
            If GetTotalPage() > 0 Then
                txtCurrentPage.Text = 1
                GetDataList(1)
            End If
        Catch ex As Exception
            tdMessage.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ddlFamily.SelectedValue = ""
        txtProcessNo.Text = ""
        txtPlanDate.Value = ""
        txtFamilyIDHidden.Text = ""
        txtProcessNoHidden.Text = ""
        txtPlanDateHidden.Text = ""

        lLabel1.Text = "第  页  共  页"
        wglist.Clear()
        txtPage.Text = ""
        txtTotalPage.Text = 0
        txtCurrentPage.Text = 0
        btnSearch_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strPlanID As String = ""
            Dim strPlanNo As String = ""
            Dim strStatus As String = "0"
            Dim strFamilyID As String = ""
            Dim strBomID As String = ""
            For i As Integer = 0 To wglist.Rows.Count - 1
                If wglist.Rows(i).Activated = True Then
                    strPlanID = wglist.Rows(i).Cells.FromKey("ID").Text.ToString
                    strPlanNo = wglist.Rows(i).Cells.FromKey("PlanNo").Text.ToString
                    strStatus = wglist.Rows(i).Cells.FromKey("Status").Text.ToString
                    strFamilyID = wglist.Rows(i).Cells.FromKey("ProductFamilyID").Text.ToString
                    strBomID = wglist.Rows(i).Cells.FromKey("BomID").Text.ToString
                    Exit For
                End If
            Next

            Response.Write("<script> parent.window.returnValue='" + strPlanNo + "," + strPlanID + "," + strStatus + "," + strFamilyID + ":" + strBomID + "' ; window.close()</script>")

        Catch ex As Exception
            tdMessage.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Write("<script> parent.window.returnValue='' ; window.close()</script>")
    End Sub

    Protected Sub lbtnFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnFirst.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)

        If intTotalPage > 0 Then
            txtCurrentPage.Text = 1
            GetDataList(1)
        End If
    End Sub

    Protected Sub lbtnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnPrev.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As Integer = txtCurrentPage.Text
        Dim intCurrentPage As Integer = CInt(strCurrentPage) - 1

        If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
            txtCurrentPage.Text = intCurrentPage
            GetDataList(intCurrentPage)
        End If
    End Sub

    Protected Sub lbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnNext.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As Integer = txtCurrentPage.Text
        Dim intCurrentPage As Integer = CInt(strCurrentPage) + 1

        If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
            txtCurrentPage.Text = intCurrentPage
            GetDataList(intCurrentPage)
        End If
    End Sub

    Protected Sub lbtnLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLast.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        If intTotalPage > 0 Then
            txtCurrentPage.Text = intTotalPage
            GetDataList(intTotalPage)
        End If
    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As String = txtPage.Text.Trim

        If IsNumeric(strCurrentPage) = True Then
            Dim intCurrentPage As Integer = CInt(strCurrentPage)
            If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
                txtCurrentPage.Text = intCurrentPage
                GetDataList(intCurrentPage)
            End If
            txtPage.Text = ""
        Else
            tdMessage.InnerText = "页数应为数字"
            txtPage.Focus()
            Exit Sub
        End If
    End Sub
End Class
