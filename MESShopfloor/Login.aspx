﻿<%@ page language="c#" codefile="Login.aspx.cs" autoeventwireup="true" inherits="Login" %>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <link href="themes/camstar/Images/Icons/favicon.ico" rel="SHORTCUT ICON" />
    <link href="styles/camstar.ui.login.min.css" rel="stylesheet" />
    <link href="styles/camstar.helpers.min.css" rel="stylesheet" />
    <link href="styles/camstar.gradients.min.css" rel="stylesheet" />
    <title>制造执行系统</title>
</head>
    <body>
        <form id="form1" runat="server" method="post">
            <div class="ui-login" style="position:relative">
                <div style="position:absolute;left:100px;top:200px;color:red">
                    <h1>内部</h1>
                    <p style="font-size:20px;">不得存储、传输、处理涉密信息！</p>
                    
                </div>
                <div class="ui-login-signin">
                    <div class="ui-login-title ui-rounded ui-shadow">制造执行系统</div>
                    <div class="ui-login-main-customer ui-shadow">
                        <div class="ui-login-border"></div>
                        <ul style="margin-left:20px;">
                            <li style="list-style-type:none; font-size:12px;"><asp:label id="CardLabel" runat="server">卡号</asp:label></li>
                            <li style="list-style-type:none;"><asp:textbox id="cardNumberTxtBox" runat="server" Width="200px" AutoPostBack="true" OnTextChanged="cardNumberTxtBox_TextChanged"></asp:textbox></li>
                            <li style="list-style-type:none; font-size:12px;"><asp:label id="UsernameLabel" runat="server">用户名</asp:label></li>
                            <li style="list-style-type:none;"><asp:textbox id="userNameTxtBox" runat="server" Width="200px" TabIndex="1"></asp:textbox></li>
                            <li style="list-style-type:none; font-size:12px;"><asp:label id="PasswordLabel" runat="server">密码</asp:label></li>
                            <li style="list-style-type:none;"><asp:textbox id="passwordTxtBox" runat="server" textmode="Password" Width="200px" TabIndex="2"></asp:textbox></li>
                            <li style="list-style-type:none;"><asp:button id="LoginButton" runat="server" text="登 录" cssclass="cs-button" OnClick="loginButton_Click" TabIndex="3" /></li>
                        </ul>
                        <div id="divError" runat="server" class="ui-login-error">
                            <asp:label id="ErrorLabel" runat="server"></asp:label>
                        </div>
                        <a class="ui-login-options option-link-expand"></a>
                      <div class="ui-login-border"></div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
