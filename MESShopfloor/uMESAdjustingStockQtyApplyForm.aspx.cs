﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class uMESAdjustingStockQtyApplyForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "uMESAdjustingStockQtyApplyForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESSecondaryWarehouseBusiness bll = new uMESSecondaryWarehouseBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：库存调整申请";
        master.strTitle = "库存调整申请";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }


    #region 数据查询

    #region 绑定入库原因列表
    protected void GetStockReasonInfo()
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("StockReason", "3");
        DataTable DT = bll.GetStockReasonInfo(para);

        ddlStockReason.DataTextField = "StockReasonName";
        ddlStockReason.DataValueField = "StockReasonId";
        ddlStockReason.DataSource = DT;
        ddlStockReason.DataBind();

        ddlStockReason.Items.Insert(0, "");
    }
    #endregion

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ScanContainerName", strScanContainerName);
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        //查询未出库库存信息
        result.Add("Out", "1");
        return result;
    }

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if(strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                //查询未出库库存信息
                para.Add("Out", "1");
                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch(Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    //查询主信息
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.txtCurrentPage.Text = "1";
        Dictionary<string, string> query = GetQuery();
        QueryData(query);
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();
        ClearDispData();
        uMESPagingDataDTO result = bll.GetMainSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 10);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;       
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtContainerId.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtProductId.Text = string.Empty;
        txtStockQty.Text = string.Empty;
        txtAfterQty.Text = string.Empty;
        //txtAllInQty.Text = "0";
        //txtAllOutQty.Text = "0";
        txtUomId.Text = string.Empty;
        txtQty.Text = string.Empty;

        try
        {
            ddlStockReason.SelectedIndex = 0;
        }
        catch
        {
        }

        ItemGrid.Clear();
       
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion
    
    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if(e.Row.Cells.FromKey("ProcessNo").Value!=null)
            { 
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            txtDispContainerName.Text = strContainerName;

            string strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            txtContainerId.Text = strContainerID;

            string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            txtDispProductName.Text = strProductName;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }

            txtProductId.Text = string.Empty;
            if (e.Row.Cells.FromKey("productid").Value != null)
            {
                string strProductId = e.Row.Cells.FromKey("productid").Value.ToString();
                txtProductId.Text = strProductId;
            }

            txtUomId.Text = string.Empty;
            if (e.Row.Cells.FromKey("uomid").Value != null)
            {
                string strUomId = e.Row.Cells.FromKey("uomid").Value.ToString();
                txtUomId.Text = strUomId;
            }

            txtFactoryStockID.Text = string.Empty;
            if (e.Row.Cells.FromKey("FactoryStockID").Value != null)
            {
                string strFactoryStockID = e.Row.Cells.FromKey("FactoryStockID").Value.ToString();
                txtFactoryStockID.Text = strFactoryStockID;
            }

            //批次数量
            string strQty = string.Empty;
            if (e.Row.Cells.FromKey("qty").Value != null)
            {
               strQty  = e.Row.Cells.FromKey("qty").Value.ToString();
                txtQty.Text = strQty;
            }
            //库存调整原因显示
            GetStockReasonInfo();

            ////入库数量
            //string strAllInQty ="0";
            // if (e.Row.Cells.FromKey("inqty").Value != null)
            //{
            //    strAllInQty = e.Row.Cells.FromKey("inqty").Value.ToString();
            //}

            //txtAllInQty.Text = strAllInQty;

            ////已出库数量
            //string strAllOutQty = "0";
            //if (e.Row.Cells.FromKey("outqty").Value != null)
            //{
            //    strAllOutQty = e.Row.Cells.FromKey("outqty").Value.ToString();
            //}
            //txtAllOutQty.Text = strAllOutQty;

            ////库存数量
            //txtStockQty.Text=  (Convert.ToInt32(txtAllInQty.Text) - Convert.ToInt32(txtAllOutQty.Text)).ToString();
            string strStockQty = string.Empty;
            if (e.Row.Cells.FromKey("StockQty").Value != null)
            {
                strStockQty = e.Row.Cells.FromKey("StockQty").Value.ToString();
                txtStockQty.Text = strStockQty;
            }

        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #endregion

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (CheckData() == false)
            {
                return;
            }
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;

            //以字典形式传递参数
            Dictionary<string, object> para = new Dictionary<string, object>();
            DataSet ds = new DataSet();
            //返回消息信息
            string strMessage = string.Empty;

            //主表要保存的信息
            DataTable dtMain = new DataTable();
            dtMain.Columns.Add("ID");//唯一标识
            dtMain.Columns.Add("ContainerID");//生产批次ID
            dtMain.Columns.Add("StockReasonID");//数量调整原因ID
            dtMain.Columns.Add("ProductID");//产品/图号ID
            dtMain.Columns.Add("Qty");//批次数量
            dtMain.Columns.Add("UOMID");//计量单位ID
            dtMain.Columns.Add("ContainerName");//生产批次号
            dtMain.Columns.Add("ProductName");//产品/图号
            dtMain.Columns.Add("Description");//名称
            dtMain.Columns.Add("ProcessNo");//工作令号
            dtMain.Columns.Add("OprNo");//作业令号
            dtMain.Columns.Add("StockQty");//库存数量
            dtMain.Columns.Add("AfterQty");//调整后数量
            dtMain.Columns.Add("ApplyDate");//申请时间
            dtMain.Columns.Add("ApplicantID");//申请人ID
            dtMain.Columns.Add("Applyer");//申请人
            dtMain.Columns.Add("Reviewer");//审核者
            dtMain.Columns.Add("ReviewerID");//审核者ID
            dtMain.Columns.Add("ReviewDate");//审核时间
            dtMain.Columns.Add("ReviewComments");//审核意见
            dtMain.Columns.Add("Result");//审核结果
            dtMain.Columns.Add("FactoryStockID"); //

            dtMain.Rows.Add();
            int intMainRow = dtMain.Rows.Count - 1;
            dtMain.Rows[intMainRow]["ID"] = DateTime.Now.ToString("yyyyMMddHHmmssffff") + "㊣String"; //唯一标识
            dtMain.Rows[intMainRow]["ContainerID"] = txtContainerId.Text + "㊣String";//生产批次ID
            dtMain.Rows[intMainRow]["StockReasonID"] = ddlStockReason.SelectedValue + "㊣String";//数量调整原因ID
            dtMain.Rows[intMainRow]["ProductID"] = txtProductId.Text + "㊣String";//产品 / 图号ID
            dtMain.Rows[intMainRow]["Qty"] = txtQty.Text + "㊣Integer";//批次数量
            dtMain.Rows[intMainRow]["UOMID"] = txtUomId.Text + "㊣String";//计量单位ID
            dtMain.Rows[intMainRow]["ContainerName"] = txtDispContainerName.Text + "㊣String";//生产批次号
            dtMain.Rows[intMainRow]["ProductName"] = txtDispProductName.Text + "㊣String";//产品 / 图号
            dtMain.Rows[intMainRow]["Description"] = txtDispDescription.Text + "㊣String";//名称
            dtMain.Rows[intMainRow]["ProcessNo"] = txtDispProcessNo.Text + "㊣String";//工作令号
            dtMain.Rows[intMainRow]["OprNo"] = txtDispOprNo.Text + "㊣String";//作业令号
            dtMain.Rows[intMainRow]["StockQty"] = txtStockQty.Text + "㊣Integer";//库存数量
            dtMain.Rows[intMainRow]["AfterQty"] = txtAfterQty.Text + "㊣Integer";//调整后数量
            dtMain.Rows[intMainRow]["ApplyDate"] = DateTime.Now + "㊣Date";//申请时间
            dtMain.Rows[intMainRow]["ApplicantID"] = userInfo["EmployeeID"] + "㊣String"; //申请人ID
            dtMain.Rows[intMainRow]["Applyer"] = userInfo["FullName"] + "㊣String"; //申请人
            dtMain.Rows[intMainRow]["Reviewer"] = "" + "㊣String";//审核者
            dtMain.Rows[intMainRow]["ReviewerID"] = "" + "㊣String";//审核者ID
            dtMain.Rows[intMainRow]["ReviewDate"] = "" + "㊣Date";//审核时间
            dtMain.Rows[intMainRow]["ReviewComments"] = "" + "㊣String";//审核意见
            dtMain.Rows[intMainRow]["Result"] = "0" + "㊣Integer";//审核结果
            dtMain.Rows[intMainRow]["FactoryStockID"] = txtFactoryStockID.Text + "㊣String";

            ds.Tables.Add(dtMain);
            ds.Tables[0].TableName = "InventoryAdjustmentInfo";
            para.Add("dsData", ds);

            //保存数据
            bool result = bll.SaveDataToDatabase(para, out strMessage);
            if (result == true)
            {
                ClearDispData();
                strMessage = "申请成功！";
            }

            DisplayMessage(strMessage, result);


        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 数据验证
    protected Boolean CheckData()
    {
        Boolean result = true;

        if (txtDispContainerName.Text=="")
        {
            DisplayMessage("请选择库存调整信息！", false);
            result = false;
            return result;
        }

        //if (ddlFactoryStock.SelectedIndex==0)
        //{
        //    DisplayMessage("请选择分厂二级库！", false);
        //    result = false;
        //    return result;
        //}

        if (ddlStockReason.SelectedIndex == 0)
        {
            DisplayMessage("请选择调整原因！", false);
            result = false;
            return result;
        }

         //调整后数量 
        string strAfterQty = txtAfterQty.Text;
        if (strAfterQty != "")
        {
            if (IsNumeric(strAfterQty) == true)
            {
                if (IsInt(strAfterQty) == false)
                {
                    DisplayMessage("调整后数量必须为正整数！", false);
                    result = false;
                    return result;
                }
            }
            else
            {
                DisplayMessage("调整后数量必须为数字！", false);
                result = false;
                return result;
            }
        }
        else
        {
            DisplayMessage("请输调整后数量！", false);
            result = false;
            return result;
        }

        if (Convert.ToInt32(txtAfterQty.Text)>Convert.ToInt32(txtStockQty.Text) && ddlStockReason.SelectedItem.Text.Contains("增加")==false)
        {
            DisplayMessage("调整后数量需大于库存数量，请选择相应的调整原因！", false);
            result = false;
            return result;
        }

        if (Convert.ToInt32(txtAfterQty.Text) < Convert.ToInt32(txtStockQty.Text) && ddlStockReason.SelectedItem.Text.Contains("减少") == false)
        {
            DisplayMessage("调整后数量需小于库存数量，请选择相应的调整原因！", false);
            result = false;
            return result;
        }

        return result;
    }
    #endregion

    //验证是否是数字
    public bool IsNumeric(string s)
    {
        bool bReturn = true;
        double result = 0;
        try
        {
            result = double.Parse(s);
        }
        catch
        {
            result = 0;
            bReturn = false;
        }
        return bReturn;
    }

    //验证是否是正整数
    public static bool IsInt(string inString)
    {
        Regex regex = new Regex("^[0-9]*[1-9][0-9]*$");
        return regex.IsMatch(inString.Trim());
    }
    #endregion



}