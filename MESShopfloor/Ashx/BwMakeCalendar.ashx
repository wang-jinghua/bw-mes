﻿<%@ WebHandler Language="C#" Class="BwMakeCalendar" %>

using System;
using System.Web;
using uMES.LeanManufacturing.ReportBusiness;
using System.Collections.Generic;
using System.Web.SessionState;
using uMES.LeanManufacturing.ParameterDTO;
using Newtonsoft.Json;
using System.Data;

public class BwMakeCalendar : IHttpHandler,IRequiresSessionState {

    BwMakeCalendarBusiness bwMC = new BwMakeCalendarBusiness();

    public void ProcessRequest (HttpContext context) {

        var action = context.Request["action"].ToString();

        switch (action) {
            case "SaveHolidayInfo":
                SaveHolidayInfo(context);
                break;
            case "SaveMakeCalendars":
                SaveMakeCalendars(context);
                break;
            case "GetHolidayInfo":
                GetHolidayInfo(context);
                break;
            case "GetMakeCalendarByHolidayDate":
                GetMakeCalendarByHolidayDate(context);
                break;
            case "DeleteHoliday":
                DeleteHolidayByParams(context);
                break;
            default:
                break;
        }

        //  context.Response.Write("Hello World");
    }

    public bool IsReusable {
        get {
            return false;
        }
    }
    /// <summary>
    /// 保存假期
    /// </summary>
    /// <param name="context"></param>
    public  void SaveHolidayInfo(HttpContext context)
    {
        var re =new ResultModel(false,"");
        var user = context.Session["UserInfo"] as Dictionary<string, string>;

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["StartDate"] = context.Request["startDate"].ToString();
        para["ContinueDay"] = context.Request["continueDay"].ToString();
        para["HolidayName"] = context.Request["holidayName"].ToString();
        para["CreateEmployeeID"] = user["EmployeeID"];
        para["Description"] = "";

        re = bwMC.SaveMakeCalendar(para);

        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(re));

    }

    /// <summary>
    /// 批量保存假期
    /// </summary>
    /// <param name="context"></param>
    public void SaveMakeCalendars(HttpContext context)
    {
        var re =new ResultModel(false,"");
        var user = context.Session["UserInfo"] as Dictionary<string, string>;

        Dictionary<string, object> para= new Dictionary<string, object>();
        para["HolidayName"] = context.Request["HolidayName"].ToString();
        para["CreateEmployeeID"] = user["EmployeeID"];
        para["Description"] = "";
        string  holidayDates =System.Web.HttpUtility.UrlDecode( context.Request["HolidayDates"]);
                
        para["HolidayDates"] = holidayDates.Split(',');

        re = bwMC.SaveMakeCalendars(para);

        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(re));
    }

    /// <summary>
    /// 查询假期
    /// </summary>
    /// <param name="context"></param>
    public void GetHolidayInfo(HttpContext context)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["Year"] = context.Request["Year"].ToString();
        if(context.Request["Month"]!=null)
            para["Month"] = context.Request["Month"].ToString();
        //para["HolidayDate"] = context.Request["HolidayDate"].ToString();

        DataTable dt = bwMC.GetMakeCalendarInfo(para);

        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(dt));
    }

    /// <summary>
    /// 查询假期根据日期，含有持续天数
    /// </summary>
    /// <param name="context"></param>
    public void GetMakeCalendarByHolidayDate(HttpContext context)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["HolidayDate"] = context.Request["HolidayDate"].ToString();
        if(context.Request["BwMakeCalendarName"]!=null)
            para["BwMakeCalendarName"] = context.Request["BwMakeCalendarName"].ToString();

        DataTable dt = bwMC.GetMakeCalendarByHolidayDate(para);

        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(dt));
    }

    /// <summary>
    /// 删除假期
    /// </summary>
    /// <param name="context"></param>
    public void DeleteHolidayByParams(HttpContext context)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        if(context.Request["BwMakeCalendarName"]!=null)
            para["BwMakeCalendarName"] = context.Request["BwMakeCalendarName"].ToString();
        if(context.Request["MakeCalendarID"]!=null)
            para["BwMakeCalendarID"] = context.Request["MakeCalendarID"].ToString();
        if(context.Request["HolidayName"]!=null)
            para["HolidayName"] = context.Request["HolidayName"].ToString();


        var re = bwMC.DeleteHolidayByParams(para);

        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(re));
    }

}