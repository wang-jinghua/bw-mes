﻿<%@ WebHandler Language="C#" Class="MenuTaskCount" %>

using System;
using System.Web;
using uMES.LeanManufacturing.ReportBusiness;
using System.Collections.Generic;
using System.Web.SessionState;

public class MenuTaskCount : IHttpHandler,IRequiresSessionState {
    uMESProblemBusiness problem = new uMESProblemBusiness();

    public void ProcessRequest (HttpContext context) {

        var action = context.Request["action"].ToString();

        switch (action) {
            case "GetProblemDispatchFormCount":
                GetProblemDispatchFormCount(context);
                break;
            default:
                break;
        }

        //  context.Response.Write("Hello World");
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

    public  void GetProblemDispatchFormCount(HttpContext context)
    {
        int re = 0;string status=context.Request["status"].ToString(), searchEmployee = context.Request["searchEmployee"].ToString();
        Dictionary<string, string> query = new Dictionary<string, string>();
        query["Status"] = status;
        query["SearchEmployee"] = searchEmployee;
        var temp = problem.GetSourceData(query, 1, 100000);
        re = int.Parse(temp.RowCount);
        context.Response.ContentType = "text/plain";
        context.Response.Write(re);

    }

}