﻿<%@ WebHandler Language="C#" Class="BwSystemManager" %>

using System;
using System.Web;
using uMES.LeanManufacturing.ReportBusiness;
using System.Collections.Generic;
using Newtonsoft.Json;

public class BwSystemManager : IHttpHandler {
    BwSystemManagerBusiness bll = new BwSystemManagerBusiness();

    public void ProcessRequest (HttpContext context) {

        
        var action = context.Request["action"].ToString();

        switch (action) {
            case "GetEmployeeLoginInfo":
                GetEmployeeLoginInfo(context);
                break;
             case "GetPermissionMenuRecord":
                GetPermissionMenuRecord(context);
                break;
             case "GetContainerExcuteRecord":
                GetContainerExcuteRecord(context);
                break;
            default:
                break;
        }

        
    }

    public bool IsReusable {
        get {
            return false;
        }
    }
    /// <summary>
    /// 人员登陆记录
    /// </summary>
    /// <param name="context"></param>
     public  void GetEmployeeLoginInfo(HttpContext context)
    {

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["EmployeeName"]= context.Request["EmployeeName"].ToString();
         para["StartDate"]= context.Request["StartDate"].ToString();
          para["EndDate"]= context.Request["EndDate"].ToString();
          para["IsOnLine"]= context.Request["IsOnLine"].ToString();
          para["PageSize"]= context.Request["PageSize"].ToString();
          para["CurrentPageIndex"]= context.Request["CurrentPageIndex"].ToString();

        var temp = bll.GetEmployeeLoginInfo(para);
      
        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(temp));

    }
    /// <summary>
    /// 获取权限菜单配置修改记录
    /// </summary>
    /// <param name="context"></param>
      public  void GetPermissionMenuRecord(HttpContext context)
    {

        Dictionary<string, string> para = new Dictionary<string, string>();
         para["StartDate"]= context.Request["StartDate"].ToString();
          para["EndDate"]= context.Request["EndDate"].ToString();
          para["BusinessNames"]= context.Request["BusinessNames"].ToString();
          para["PageSize"]= context.Request["PageSize"].ToString();
          para["CurrentPageIndex"]= context.Request["CurrentPageIndex"].ToString();

        var temp = bll.GetMESAuditLogInfo(para);

        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(temp));

    }

      /// <summary>
    /// 获取批次执行记录
    /// </summary>
    /// <param name="context"></param>
      public  void GetContainerExcuteRecord(HttpContext context)
    {

        Dictionary<string, string> para = new Dictionary<string, string>();
         para["StartDate"]= context.Request["StartDate"].ToString();
          para["EndDate"]= context.Request["EndDate"].ToString();
          para["BusinessName"]= context.Request["BusinessName"].ToString();
         para["ContainerName"]= context.Request["ContainerName"].ToString();
          para["PageSize"]= context.Request["PageSize"].ToString();
          para["CurrentPageIndex"]= context.Request["CurrentPageIndex"].ToString();

        var temp = bll.GetMESAuditLogInfo(para);

        context.Response.ContentType = "text/plain";
        context.Response.Write(JsonConvert.SerializeObject(temp));

    }

}