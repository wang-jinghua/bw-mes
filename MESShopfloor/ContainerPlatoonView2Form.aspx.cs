﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class ContainerPlatoonView2Form : System.Web.UI.Page
{
    const string QueryWhere = "ContainerPlatoonView2Form";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPlatoonBusiness bll = new uMESContainerPlatoonBusiness();
    int m_PageSize = 40;
    DataTable DT = new DataTable();
    DateTime myStartDate = new DateTime();
    DataTable dtRGStatus = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：预派工信息浏览";
        master.strTitle = "预派工信息浏览";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { QueryData(upageTurning.CurrentPageIndex); });
     
        if (!IsPostBack)
        {
            txtEndDate.Value = DateTime.Now.AddMonths(1).ToString("yyyy-MM-dd");
            //AddColumns();
            ClearMessage();
        }
    }
    
    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);
        hdScanCon.Value = string.Empty;
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = "";
        txtEndDate.Value = "";
        wgResult.Rows.Clear();
        wgResult.Columns.Clear();
        upageTurning.TotalRowCount = 0;
        AddColumns();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();
       
        //if (strStartDate == string.Empty)
        //{
        //    ShowStatusMessage("请选择开始日期", false);
        //    return null;
        //}

        if (strEndDate == string.Empty)
        {
            ShowStatusMessage("请选择截止日期", false);
            return null;
        }

        //DateTime dtStartDate = Convert.ToDateTime(strStartDate);
        DateTime dtStartDate = new DateTime();
        DateTime dtEndDate = Convert.ToDateTime(strEndDate);

        //if (dtStartDate > dtEndDate)
        //{
        //    ShowStatusMessage("截止日期应晚于开始日期", false);
        //    return null;
        //}

        //dtRGStatus = bll.GetResourceGroupStatus(dtStartDate, dtEndDate);

        //TimeSpan ts = dtEndDate.Subtract(dtStartDate);

        //int Days = ts.Days + 1;

        Dictionary<string, string> result = new Dictionary<string, string>();
        //result.Add("Days", Days.ToString());
        if (!string.IsNullOrEmpty(hdScanCon.Value))
        {
            result.Add("ScanContainerName", hdScanCon.Value);
            result.Add("StartDate", strStartDate);
            result.Add("EndDate", strEndDate);
        }
        else
        {
            result.Add("ProcessNo", strProcessNo);
            result.Add("ContainerName", strContainerName);
            result.Add("ProductName", strProductName);
            result.Add("SpecName", strSpecName);
            result.Add("StartDate", strStartDate);
            result.Add("EndDate", strEndDate);
        }

        return result;
    }

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";
            hdScanCon.Value = string.Empty;
            if (strScan != string.Empty)
            {
                hdScanCon.Value = strScan;
                QueryData(1);
            }
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    public void QueryData(int intIndex)
    {
        try
        {
            Dictionary<string, string> para = GetQuery();

            if (para == null)
            {
                return;
            }

            ShowStatusMessage("", true);
            uMESPagingDataDTO result = bll.GetAPSMainInfo(para, intIndex, m_PageSize);
            wgResult.Rows.Clear();
            wgResult.Columns.Clear();

            Drawing(result.DBset, intIndex);

            //给分页控件赋值，用于分页控件信息显示
            this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
            this.upageTurning.RowCountByPage = m_PageSize;
        }
        catch (Exception e)
        {
            ShowStatusMessage(e.Message, false);
        }

    }

    /// <summary>
    /// 绘制零件进度表显示界面
    /// </summary>
    /// <param name="ds"></param>
    void Drawing(DataSet ds,int intIndex)
    {
        DataTable disDt = new DataTable();
        disDt.Columns.Add("SeqNumber");//序号
        disDt.Columns.Add("ProcessNo");//工作令号
        disDt.Columns.Add("OprNo");//作业令号
        disDt.Columns.Add("ProductName");//图号
        disDt.Columns.Add("Description");//名称
        disDt.Columns.Add("ContainerName");//批次号
        disDt.Columns.Add("Qty");//批次数量
        disDt.Columns.Add("isSpec");//是否是工序
        disDt.Columns.Add("CurrentSpec");//当前工序
        disDt.Columns.Add("ContainerID");
        disDt.Columns.Add("State");

        //表头行
        //DataRow rowTitle = disDt.NewRow();
        //rowTitle["SeqNumber"] = "序号";
        //rowTitle["ProcessNo"] = "工作令号";
        //rowTitle["OprNo"] = "作业令号";
        //rowTitle["ProductName"] = "图号";
        //rowTitle["Description"] = "名称";
        //rowTitle["ContainerName"] = "批次号";
        //rowTitle["Qty"] = "数量";
        //rowTitle["isSpec"] = "是否是工序";
        //rowTitle["CurrentSpec"] = "当前工序";
        //rowTitle["ContainerID"] = "批次ID";
        //rowTitle["State"] = "";
        //disDt.Rows.Add(rowTitle);

        //创建Grid列
        AddColumns();

        //主信息
        DataTable dtMain = ds.Tables[0];

        //详细信息
        DataTable dtDetail = ds.Tables[1];
        DT = dtDetail.Copy();

        //最大列值
        int maxColum = 0;

        DateTime beginDate = DateTime.MaxValue;

        foreach (DataRow row in dtDetail.Rows)
        {
            if (Convert.ToDateTime(row["PlannedStartDate"].ToString()) < beginDate)
            {
                beginDate = Convert.ToDateTime(row["PlannedStartDate"].ToString());
            }
        }

        DateTime endDate = DateTime.MinValue;

        foreach (DataRow row in dtDetail.Rows)
        {
            if (Convert.ToDateTime(row["PlannedCompletionDate"].ToString()) > endDate)
            {
                endDate = Convert.ToDateTime(row["PlannedCompletionDate"].ToString());
            }
        }

        myStartDate = beginDate;
        TimeSpan daystmp = endDate - beginDate;
        int dayLenth = daystmp.Days + 1;
        maxColum = dayLenth;// * 24;

        //创建Grid时间列
        UltraGridColumn colum = new UltraGridColumn();

        Color headBackColor = Color.DarkGray;
        for (int i = 0; i < maxColum; i++)
        {
            //string strTmp = beginDate.AddHours(i).ToString("yyyyMMddHH");
            string strTmp = beginDate.AddDays(i).ToString("yyyyMMdd");
            colum = new UltraGridColumn(strTmp, " ", ColumnType.NotSet, "");
            colum.Header.Caption = beginDate.AddDays(i).ToString("yyyy-MM-dd");
            colum.BaseColumnName = "";
            colum.Key = strTmp;
            //colum.Width = 3;
            colum.HeaderStyle.Height = 20;
            colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            //colum.Header.Title = beginDate.AddHours(i).ToString("yyyy-MM-dd HH时");
            colum.Header.Title = beginDate.AddDays(i).ToString("yyyy-MM-dd");
            wgResult.Bands[0].Columns.Add(colum);
        }

        dtRGStatus = bll.GetResourceGroupStatus(beginDate, endDate);

        //foreach (DataRow row in DT.Rows)
        //{
        //    string strRGID = row["ResourceGroupID"].ToString();
        //    string strStart = Convert.ToDateTime(row["PlannedStartDate"].ToString()).ToString("yyyy-MM-dd");
        //    string strEnd = Convert.ToDateTime(row["PlannedCompletionDate"].ToString()).ToString("yyyy-MM-dd");

        //    StringBuilder strFilter = new StringBuilder();
        //    strFilter.Append(string.Format("RGID = '{0}' AND RGStatus = 1 AND ", strRGID));
        //    //strFilter.Append(string.Format("RGDate >= TO_DATE('{0} 00:00:00','yyyy-MM-dd HH24:MI:SS') ", strStart));
        //    //strFilter.Append(string.Format("AND RGDate <= TO_DATE('{0} 00:00:00','yyyy-MM-dd HH24:MI:SS')", strEnd));
        //    strFilter.Append(string.Format("RGDate >= '{0} 00:00:00' ", strStart));
        //    strFilter.Append(string.Format("AND RGDate <= '{0} 00:00:00'", strEnd));

        //    DataRow[] rows = dtRGStatus.Select(strFilter.ToString());

        //    if (rows.Length > 0)
        //    {
        //        row["state"] = 1;
        //    }
        //    else
        //    {
        //        row["state"] = 0;
        //    }
        //}

        //disDt赋值
        int rowindex = 0;
        int pageSize = m_PageSize;
        int pageIndex = intIndex;

        foreach (DataRow drMain in dtMain.Rows)
        {
            rowindex += 1;
            DataRow rowTmp = disDt.NewRow();
            rowTmp["SeqNumber"] = pageSize * (pageIndex - 1) + rowindex;
            rowTmp["ProcessNo"] = drMain["ProcessNo"];//工作令号
            rowTmp["OprNo"] = drMain["OprNo"];//作业令号
            rowTmp["ProductName"] = drMain["ProductName"];//图号
            rowTmp["Description"] = drMain["Description"];//名称
            rowTmp["ContainerName"] = drMain["ContainerName"];//批次号
            rowTmp["ContainerID"] = drMain["ContainerID"];
            rowTmp["State"] = drMain["State"];
            rowTmp["Qty"] = drMain["Qty"];//批次数量
            rowTmp["isSpec"] = "1";
            disDt.Rows.Add(rowTmp);
        }

        wgResult.DataSource = disDt;
        wgResult.DataBind();
    }

    protected void wgResult_DataBound(object sender, EventArgs e)
    {
        ////表头行
        //wgResult.Rows[0].Cells[0].AllowEditing = AllowEditing.No;

        //DateTime dateTitle = myStartDate;
        //for (int i = 0; i < wgResult.Columns.Count; i++)
        //{
        //    if (i == 2 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 || i == 9)
        //    {
        //        wgResult.Rows[0].Cells[i].Style.HorizontalAlign = HorizontalAlign.Center;
        //        wgResult.Rows[0].Cells[i].Style.Font.Bold = true;
        //        wgResult.Rows[0].Cells[i].Style.Font.Size = 12;
        //    }

        //    if (i % 24 == 12)
        //    {
        //        wgResult.Rows[0].Cells[i].ColSpan = 24;
        //        wgResult.Rows[0].Cells[i].Style.HorizontalAlign = HorizontalAlign.Center;
        //        wgResult.Rows[0].Cells[i].Style.Font.Size = 16;
        //        wgResult.Rows[0].Cells[i].Text = dateTitle.ToString("yyyy-MM-dd");
        //        dateTitle = dateTitle.AddDays(1);
        //    }
        //}

        for (int i = 0; i < wgResult.Rows.Count; i++)
        {
            if (!string.IsNullOrEmpty(wgResult.Rows[i].Cells.FromKey("isspec").Text))
            {
                if (wgResult.Rows[i].Cells.FromKey("isspec").Text == "1")
                {
                    string strContainerID = wgResult.Rows[i].Cells.FromKey("ContainerID").Value.ToString();

                    //任务信息
                    DataRow[] rows = DT.Select("ContainerID = '" + strContainerID + "'");

                    foreach (DataRow row in rows)
                    {
                        Color cellColor = bll.RandColor();

                        string strSpecName = row["SpecName"].ToString();
                        string strRGName = row["ResourceGroupName"].ToString();
                        string strGS = row["TotalGS"].ToString();

                        Double dblGS = Convert.ToDouble(strGS) / 60 / 24;
                        strGS = dblGS.ToString("0.00");

                        //string strStatus = row["state"].ToString();

                        DateTime startTime = Convert.ToDateTime(row["PlannedStartDate"]);
                        DateTime startTime1 = new DateTime();
                        if (startTime.Hour < 12)
                        {
                            startTime1 = Convert.ToDateTime(startTime.ToString("yyyy-MM-dd") + " 00:00:00");
                        }
                        if (startTime.Hour >= 12)
                        {
                            startTime1 = Convert.ToDateTime(startTime.AddDays(1).ToString("yyyy-MM-dd") + " 00:00:00");
                        }

                        DateTime endTime = Convert.ToDateTime(row["PlannedCompletionDate"]);
                        DateTime endTime1 = new DateTime();
                        if (endTime.Hour < 12)
                        {
                            endTime1 = Convert.ToDateTime(endTime.AddDays(-1).ToString("yyyy-MM-dd") + " 00:00:00");
                        }
                        if (endTime.Hour >= 12)
                        {
                            endTime1 = Convert.ToDateTime(endTime.ToString("yyyy-MM-dd") + " 00:00:00");
                        }
                        //int intH = bll.GetH(startTime, endTime);

                        TimeSpan daystmp = endTime1 - startTime1;
                        int intD = daystmp.Days + 1;

                        //if (daystmp.Hours > 12)
                        //{
                        //    intD = intD + 1;
                        //}

                        DateTime beginTmpTime = startTime1;

                        //int intAddHours = 0;
                        //if (startTime.Minute > 30)
                        //{
                        //    intAddHours = 1;
                        //}

                        //int intAddDays = 0;
                        //if (startTime.Hour >= 12)
                        //{
                        //    intAddDays = 1;
                        //}

                        //if (endTime.Hour < 12)
                        //{
                        //    intD = intD - 1;
                        //}

                        //string tmpDate = beginTmpTime.AddHours(intAddHours).ToString("yyyyMMddHH");
                        //string tmpDate = beginTmpTime.AddDays(intAddDays).ToString("yyyyMMdd");

                        for (int j = 0; j < intD; j++)
                        {
                            string tmpDate = beginTmpTime.AddDays(j).ToString("yyyyMMdd");
                            for (int k = 0; k < wgResult.Columns.Count; k++)
                            {
                                if (wgResult.Columns[k].Key == tmpDate)
                                {
                                    string strRGID = row["ResourceGroupID"].ToString();
                                    string strDate = beginTmpTime.AddDays(j).ToString("yyyy-MM-dd");

                                    string strF = string.Format("RGID = '{0}' AND RGDate = '{1}' AND RGStatus = 1", strRGID, strDate);
                                    DataRow[] rowsS = dtRGStatus.Select(strF);
                                    if (rowsS.Length > 0)
                                    {
                                        wgResult.Rows[i].Cells.FromKey(tmpDate).Text = "▲" + strRGName;
                                    }

                                    wgResult.Rows[i].Cells.FromKey(tmpDate).Style.BackColor = cellColor;
                                    //wgResult.Rows[i].Cells.FromKey(tmpDate).ColSpan = intH;
                                    //wgResult.Rows[i].Cells.FromKey(tmpDate).ColSpan = intD;

                                    strSpecName = common.GetSpecNameWithOutProdName(strSpecName);
                                    string strTitle = string.Format("{0},{1},{2}天\n{3}-{4}", strSpecName, strRGName, strGS, startTime.ToString("yyyy-MM-dd HH:mm"), endTime.ToString("yyyy-MM-dd HH:mm"));
                                    wgResult.Rows[i].Cells.FromKey(tmpDate).Title = strTitle;
                                    break;
                                }
                            }
                        }
                    }

                    string strState = wgResult.Rows[i].Cells.FromKey("State").Value.ToString();

                    if (strState == "1")
                    {
                        wgResult.Rows[i].Cells.FromKey("State").Style.BackColor = Color.LightGreen;
                        wgResult.Rows[i].Cells.FromKey("State").Text = "";

                        wgResult.Rows[i].Cells.FromKey("ckSelect").AllowEditing = AllowEditing.No;
                    }
                    else
                    {
                        wgResult.Rows[i].Cells.FromKey("State").Style.BackColor = Color.LightYellow;
                        wgResult.Rows[i].Cells.FromKey("State").Text = "";

                        wgResult.Rows[i].Cells.FromKey("ckSelect").AllowEditing = AllowEditing.Yes;
                    }

                    continue;
                }
            }
        }
    }

    #region 创建Grid列
    /// <summary>
    /// 创建Grid列
    /// </summary>
    void AddColumns()
    {
        UltraGridColumn colum = new UltraGridColumn("ckSelect", "", ColumnType.CheckBox, "");
        colum.Header.Caption = "选择";
        colum.Key = "ckSelect";
        colum.Width = 40;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.AllowUpdate = AllowUpdate.Yes;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("ContainerID", "ContainerID", ColumnType.NotSet, "");
        colum.BaseColumnName = "ContainerID";
        colum.Key = "ContainerID";
        colum.Width = 120;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("xuhao", "", ColumnType.NotSet, "");
        colum.Header.Caption = "序号";
        colum.BaseColumnName = "Seqnumber";
        colum.Key = "Seqnumber";
        colum.Width = 40;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("State", "", ColumnType.NotSet, "");
        colum.BaseColumnName = "State";
        colum.Key = "State";
        colum.Width = 40;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("processno", "", ColumnType.NotSet, "");
        colum.Header.Caption = "工作令号";
        colum.BaseColumnName = "processno";
        colum.Key = "processno";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("oprno", "", ColumnType.NotSet, "");
        colum.Header.Caption = "作业令号";
        colum.BaseColumnName = "oprno";
        colum.Key = "oprno";
        colum.Width = 120;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("productname", "", ColumnType.NotSet, "");
        colum.Header.Caption = "图号";
        colum.BaseColumnName = "productname";
        colum.Key = "productname";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("description", "", ColumnType.NotSet, "");
        colum.Header.Caption = "名称";
        colum.BaseColumnName = "description";
        colum.Key = "description";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("containername", "", ColumnType.NotSet, "");
        colum.Header.Caption = "批次号";
        colum.BaseColumnName = "containername";
        colum.Key = "containername";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("qty", "", ColumnType.NotSet, "");
        colum.Header.Caption = "数量";
        colum.BaseColumnName = "qty";
        colum.Key = "qty";
        colum.Width = 60;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("IsSpec", "IsSpec", ColumnType.NotSet, "");
        colum.BaseColumnName = "IsSpec";
        colum.Key = "IsSpec";
        colum.Width = 60;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("CurrentSpec", "CurrentSpec", ColumnType.NotSet, "");
        colum.BaseColumnName = "CurrentSpec";
        colum.Key = "CurrentSpec";
        colum.Width = 60;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);
    }
    #endregion

    public void ResetQuery()
    {
        ShowStatusMessage("", true);

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        wgResult.Rows.Clear();
        upageTurning.TotalRowCount = 0;
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    }
    #endregion

    #region 清除提示信息
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"" + "" + "\", 'true');</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            upageTurning.TxtCurrentPageIndex.Text = "1";
            Session["CurrenIndex"] = null;
            hdScanCon.Value = string.Empty;
            QueryData(upageTurning.CurrentPageIndex);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    protected void btnAPS_Click(object sender, EventArgs e)
    {
        try
        {
            List<string> listContainerID = new List<string>();

            for (int i = 0; i < wgResult.Rows.Count; i++)
            {
                string ck = wgResult.Rows[i].Cells.FromKey("ckSelect").Value.ToString();

                if (ck.ToUpper() == "TRUE")
                {
                    if (wgResult.Rows[i].Cells.FromKey("ContainerID").Value != null)
                    {
                        string strContainerID = wgResult.Rows[i].Cells.FromKey("ContainerID").Value.ToString();

                        listContainerID.Add(strContainerID);
                    }
                }
            }

            if (listContainerID.Count == 0)
            {
                ShowStatusMessage("请选择要下发的批次", false);
                return;
            }

          // bll.UpdateDIState(listContainerID);

            int intIndex = 1;
            if (upageTurning.TxtCurrentPageIndex.Text!=null)
            {
                intIndex = Convert.ToInt32(upageTurning.TxtCurrentPageIndex.Text);
            }
            Session["CurrenIndex"] = upageTurning.TxtCurrentPageIndex.Text;
            QueryData(intIndex);
            Session["CurrenIndex"] = null;
            ShowStatusMessage("下发完成", true);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
}