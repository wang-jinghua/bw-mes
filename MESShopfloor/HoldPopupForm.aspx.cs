﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Data;
using System.Drawing;
using Infragistics.WebUI.UltraWebGrid;

public partial class HoldPopupForm : System.Web.UI.Page
{
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESRejectAppBusiness rejectapp = new uMESRejectAppBusiness();

    string businessName = "批次管理", parentName = "Container";
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Dictionary<string, string> popupData = (Dictionary<string, string>)Session["PopupData"];
            txtDispProcessNo.Text = popupData["ProcessNo"].ToString();
            txtDispOprNo.Text = popupData["OprNo"].ToString();
            txtDispContainerID.Text = popupData["ContainerID"].ToString();
            txtDispContainerName.Text = popupData["ContainerName"].ToString();
            txtDispProductName.Text = popupData["ProductName"].ToString();
            txtDispDescription.Text = popupData["Description"].ToString();
            txtDispContainerQty.Text = popupData["Qty".ToString()].ToString();
            txtDispPlannedStartDate.Text = popupData["PlannedStartDate"].ToString();
            txtDispPlannedCompletionDate.Text = popupData["PlannedCompletionDate"].ToString();
            txtChildCount.Text = popupData["ChildCount"].ToString();
            txtDispWorkflowID.Text = popupData["WorkflowID"].ToString();

            Session["PopupData"] = null;
            Session["ContainerMessage"] = null;
            GetHoldReason();

            //txtScrapInfoName.Text = DateTime.Now.ToString("yyyyMMddHHmmssfff");
        }
    }

    #region 获取暂停原因
    /// <summary>
    /// 获取暂停原因
    /// </summary>
    protected void GetHoldReason()
    {
        DataTable DT = common.GetHoldReason();

        ddlReason.DataSource = DT;
        ddlReason.DataTextField = "HoldReasonName";
        ddlReason.DataValueField = "HoldReasonID";
        ddlReason.DataBind();

        ddlReason.Items.Insert(0, "");
    }
    #endregion

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }
    #endregion

    #region 提交按钮
    protected void btnMaterialApp_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {
            string strReasonCode = ddlReason.SelectedItem.Text;
            if (strReasonCode == string.Empty)
            {
                ShowStatusMessage("请选择暂停原因", false);
                return;
            }

            //暂停
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strOprEmployeeID = userInfo["EmployeeID"];
            string strEmployeeName = userInfo["ApiEmployeeName"];
            string strPassword = userInfo["ApiPassword"];
            string strContainerName = txtDispContainerName.Text;
            Dictionary<string, object> para1 = new Dictionary<string, object>();
            para1.Add("ContainerName", strContainerName);
            para1.Add("ServerUser", strEmployeeName);
            para1.Add("ServerPassword", strPassword);
            para1.Add("ReasonCode", strReasonCode);

            string strMessage = string.Empty;

            Boolean result = rejectapp.HoldContainer(para1, out strMessage);

            if (result == false)
            {
                ShowStatusMessage(strMessage, false);
                return;
            }
            Session["ContainerMessage"] ="批次暂停成功！";
            Response.Write("<script>parent.window.returnValue='1'; window.close()</script>");

            #region 记录日志
            var ml = new MESAuditLog();
            ml.ContainerName = txtDispContainerName.Text; ml.ContainerID = txtDispContainerID.Text;
            ml.ParentID = ml.ContainerID; ml.ParentName = parentName;
            ml.CreateEmployeeID = userInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = 1;
            ml.Description = "批次暂停:" + "暂停成功";
            common.SaveMESAuditLog(ml);
            #endregion
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
    #endregion
}