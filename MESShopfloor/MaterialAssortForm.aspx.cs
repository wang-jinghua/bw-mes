﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Web.UI;

public partial class MaterialAssortForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "MaterialAssortForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：物料齐套";
        master.strTitle = "物料齐套";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strTeamID = userInfo["TeamID"];
        result.Add("TeamID", strTeamID);
        result.Add("DispatchType", "1");
        result.Add("Status", "20"); //已接收
        result.Add("ScanContainerName", strScanContainerName);
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("SpecName", strSpecName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);
        result.Add("Sequence", "1");//add:Wangjh 0925

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        uMESPagingDataDTO result = dispatch.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 9);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;

        ClearDispData();
    }

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProcessNo").Value != null)
            {
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }
            txtDispContainerName.Text = string.Empty;
            if (e.Row.Cells.FromKey("ContainerName").Value != null)
            {
                string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
                txtDispContainerName.Text = strContainerName;
            }
            txtDispContainerID.Text = string.Empty;
            if (e.Row.Cells.FromKey("ContainerID").Value != null)
            {
                string strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
                txtDispContainerID.Text = strContainerID;
            }
            txtDispProductName.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProductName").Value != null)
            {
                string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
                txtDispProductName.Text = strProductName;
            }
            txtDispProductID.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProductID").Value != null)
            {
                string strProductID = e.Row.Cells.FromKey("ProductID").Value.ToString();
                txtDispProductID.Text = strProductID;
            }
            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }
            txtDispQty.Text = string.Empty;
            if (e.Row.Cells.FromKey("Qty").Value != null)
            {
                string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
                txtDispQty.Text = strQty;
            }
            txtDispSpecName.Text = string.Empty;
            if (e.Row.Cells.FromKey("SpecNameDisp").Value != null)
            {
                string strSpec = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
                txtDispSpecName.Text = strSpec;
            }
            txtDispWorkflowID.Text = string.Empty;
            if (e.Row.Cells.FromKey("WorkflowID").Value != null)
            {
                string strWorkflowID = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
                txtDispWorkflowID.Text = strWorkflowID;
            }
            txtDispSpecID.Text = string.Empty;
            if (e.Row.Cells.FromKey("SpecID").Value != null)
            {
                string strSpecID = e.Row.Cells.FromKey("SpecID").Value.ToString();
                txtDispSpecID.Text = strSpecID;
            }
            txtDispTeamName.Text = string.Empty;
            if (e.Row.Cells.FromKey("TeamName").Value != null)
            {
                string strTeamName = e.Row.Cells.FromKey("TeamName").Value.ToString();
                txtDispTeamName.Text = strTeamName;
            }
            txtDispTeamID.Text = string.Empty;
            if (e.Row.Cells.FromKey("TeamID").Value != null)
            {
                string strTeamID = e.Row.Cells.FromKey("TeamID").Value.ToString();
                txtDispTeamID.Text = strTeamID;
            }
            txtDispResourceName.Text = string.Empty;
            if (e.Row.Cells.FromKey("ResourceName").Value != null)
            {
                string strResourceName = e.Row.Cells.FromKey("ResourceName").Value.ToString();
                txtDispResourceName.Text = strResourceName;
            }
            txtDispResourceID.Text = string.Empty;
            if (e.Row.Cells.FromKey("ResourceID").Value != null)
            {
                string strResourceID = e.Row.Cells.FromKey("ResourceID").Value.ToString();
                txtDispResourceID.Text = strResourceID;
            }
            DataTable DT = dispatch.GetResourceDispatchInfo(txtDispResourceID.Text);

            wgDispatchList.DataSource = DT;
            wgDispatchList.DataBind();

            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Value.ToString();
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
            }
            txtDispID.Text = string.Empty;
            wgEmployee.Clear();
            wgProductNo.Clear();
            if (e.Row.Cells.FromKey("ID").Value != null)
            {
                string strID = e.Row.Cells.FromKey("ID").Value.ToString();
                txtDispID.Text = strID;
                DataTable dtEmployee = dispatch.GetEmployeeByDispatchID(strID);
                wgEmployee.DataSource = dtEmployee;
                wgEmployee.DataBind();
                DataTable dt = dispatch.GetProductNoByDispatchID(strID);
                wgProductNo.DataSource = dt;
                wgProductNo.DataBind();
            }
            txtDispParentID.Text = string.Empty;
            if (e.Row.Cells.FromKey("ParentID").Value != null)
            {
                string strParentID = e.Row.Cells.FromKey("ParentID").Value.ToString();
                txtDispParentID.Text = strParentID;
            }
            txtParentQty.Text = string.Empty;
            if (e.Row.Cells.FromKey("ParentQty").Value != null)
            {
                string strParentQty = e.Row.Cells.FromKey("ParentQty").Value.ToString();
                txtParentQty.Text = strParentQty;
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #endregion

    #region 物料齐套按钮
    protected void btnMaterialAssort_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (txtDispID.Text == string.Empty)
            {
                DisplayMessage("请选择要查看齐套信息的任务", false);
                return;
            }

            Dictionary<string, string> popupData = new Dictionary<string, string>();
            popupData.Add("ID", txtDispID.Text);
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("ProductID", txtDispProductID.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("Qty", txtDispQty.Text);
            popupData.Add("SpecName", txtDispSpecName.Text);
            popupData.Add("SpecID", txtDispSpecID.Text);
            popupData.Add("ResourceName", txtDispResourceName.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);

            Session["PopupData"] = popupData;
            
            string strScript = "<script>openmaterialapp()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispTeamName.Text = string.Empty;
        txtDispResourceName.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;

        txtDispID.Text = string.Empty;

        wgEmployee.Clear();
        wgProductNo.Clear();
        wgDispatchList.Clear();
    }
    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                string strTeamID = userInfo["TeamID"];
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("TeamID", strTeamID);
                para.Add("DispatchType", "1");
                para.Add("Status", "20"); //已接收

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void wgDispatchList_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < wgDispatchList.Rows.Count; i++)
        {
            string strSpecName = wgDispatchList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            wgDispatchList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }
}