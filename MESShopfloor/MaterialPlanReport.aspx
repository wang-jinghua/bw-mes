﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="MaterialPlanReport.aspx.cs" Inherits="MaterialPlanReport" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent"  runat="Server">
    
<table style="border-collapse: collapse; border-color: #8ec2f5" border="1"
            cellpadding="2">
            <tr>
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    车型：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlFamily" runat="server" Width="150px" AutoPostBack="false" >
                    </asp:DropDownList>
                </td> 
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    责任单位：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlFactory" runat="server" Width="150px" AutoPostBack="false" >
                    </asp:DropDownList>
                </td> 
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    班组：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlTeam" runat="server" Width="150px" AutoPostBack="false" >
                    </asp:DropDownList>
                </td>
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    令号：
                </td>
                <td align="left" style="width: 150px">
                    <asp:TextBox ID="txtProcessNo" runat="server" class="ReportTextBox" 
                        Width="150px"></asp:TextBox>
                </td>
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    配送地点：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlStation" runat="server" Width="150px" >
                    </asp:DropDownList>
                </td>                        
                <td style="text-align: left;" colspan="6">
                    <asp:Button ID="btnSearch" runat="server" Text="查询"
                        CssClass="ReportButton" EnableTheming="True" onclick="btnSearch_Click" />
                    <asp:Button ID="btnReSet" runat="server" Text="重置" 
                        CssClass="ReportButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    <asp:Button ID="btnExport" runat="server" Text="Excel导出" Width="80px" CssClass="ReportButton"
                        EnableTheming="True" onclick="btnExport_Click" />
                </td>
            </tr>
        </table>
  <div id="ItemDiv" runat="server">
        <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="430px" Width="100%">
            <Bands>
                <igtbl:UltraGridBand>
                <Columns>

                         <igtbl:UltraGridColumn Key = "FactoryName" Width = "120px" 
                        BaseColumnName = "FactoryName">  
                           <header Caption = "责任单位">
                               <RowLayoutColumnInfo OriginX = "8"/>
                           </header>
                           <footer>
                               <RowLayoutColumnInfo OriginX = "8"/>
                           </footer>
                      </igtbl:UltraGridColumn>

                        <igtbl:UltraGridColumn Key="TeamName" Width="120px" 
                        BaseColumnName="TeamName">
                            <Header Caption="班组">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                            </Header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                            </Footer>
                        </igtbl:UltraGridColumn>

                        <igtbl:UltraGridColumn Key = "WorkStationName" Width ="120px" 
                        BaseColumnName ="WorkStationName">
                            <header Caption="配送地点">
                                <RowLayoutColumnInfo OriginX = "2"></RowLayoutColumnInfo>
                            </header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX = "2"></RowLayoutColumnInfo>
                            </Footer>
                         </igtbl:UltraGridColumn>
                        <igtbl:UltraGridColumn Key="ProductFamilyName" Width="120px" 
                        BaseColumnName="ProductFamilyName">
                            <Header Caption="车型">
                                <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                            </Header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                            </Footer>
                        </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key = "ProcessNo" Width ="150px" 
                        BaseColumnName ="ProcessNo">
                            <header Caption="令号">
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key = "Sequence" Width ="100px" 
                        BaseColumnName ="Sequence">
                            <header Caption="序号">
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key = "ProductName" Width ="200px" 
                        BaseColumnName ="ProductName">
                            <header Caption="图号">
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key = "Description" Width ="200px" 
                        BaseColumnName ="Description">
                            <header Caption="名称">
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX = "3"></RowLayoutColumnInfo>
                            </Footer>
                         </igtbl:UltraGridColumn>

                         <igtbl:UltraGridColumn Key ="RequireQty" Width = "80px" 
                        BaseColumnName = "RequireQty">
                             <header Caption = "需求数量">
                               <RowLayoutColumnInfo OriginX = "4"/>
                             </header>
                             <Footer>
                             <RowLayoutColumnInfo OriginX = "4" />
                             </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key ="Qty" Width = "80px" 
                        BaseColumnName = "Qty">
                             <header Caption = "已配送数量">
                               <RowLayoutColumnInfo OriginX = "5"/>
                             </header>
                             <Footer>
                             <RowLayoutColumnInfo OriginX = "5" />
                             </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key = "QJQty" Width = "80px" 
                        BaseColumnName = "QJQty"  >
                             <header Caption = "缺件数量">
                               <RowLayoutColumnInfo OriginX = "6"/>
                             </header>
                             <Footer>
                              <RowLayoutColumnInfo OriginX = "6"/>
                             </Footer>
                        </igtbl:UltraGridColumn>

                    </Columns>
                    <AddNewRow View="NotSet" Visible="NotSet">
                    </AddNewRow>
                </igtbl:UltraGridBand>       
            </Bands>
            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AllowSortingDefault="OnClient"
                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                RowHeightDefault="18px">
                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="430px"
                    Width="100%">
                </FrameStyle>
                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                </RowAlternateStyleDefault>
                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                    StyleMode="QuickPages">
                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </PagerStyle>
                </Pager>
                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                </EditCellStyleDefault>
                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                </FooterStyleDefault>
                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Bold="True">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    <Padding Bottom="3px" Top="2px" />
                    <Padding Top="2px" Bottom="3px"></Padding>
                </HeaderStyleDefault>
                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                    <Padding Left="3px" />
                </RowSelectorStyleDefault>
                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="25px" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" CssClass="GridRowStyle">
                    <Padding Left="3px" />
                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                </RowStyleDefault>
                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                </GroupByRowStyleDefault>
                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                </SelectedRowStyleDefault>
                <GroupByBox Hidden="True">
                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                    </BoxStyle>
                </GroupByBox>
                <AddNewBox>
                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </BoxStyle>
                </AddNewBox>
                <ActivationObject BorderColor="" BorderWidth="">
                </ActivationObject>
                <FilterOptionsDefault FilterUIType="HeaderIcons">
                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px" Height="420px" Width="200px">
                        <Padding Left="2px" />
                    </FilterDropDownStyle>
                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                    </FilterHighlightRowStyle>
                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px">
                        <Padding Left="2px" />
                    </FilterOperandDropDownStyle>
                </FilterOptionsDefault>
            </DisplayLayout>
        </igtbl:UltraWebGrid>
    </div>
    <div>
       <table style="width: 100%;">
            <tr>
                <td style="text-align: right; width: 50%;">
                    <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                        Text="第  页  共  页"></asp:Label>
                    <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                    <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                    <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                    <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                        OnClick="btnGo_Click" />
                    <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                        Visible="False"></asp:TextBox>
                    <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;"
                        Visible="False"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
