﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZYDPopupForm.aspx.cs" Inherits="ZYDPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="uMESCustomControls/ProductInfo/GetWorkFlowInfo.ascx" TagName="getWorkFlowInfo"
    TagPrefix="gPI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>质疑单</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                   工作令号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                   作业令号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispWorkflowID" runat="server" Visible="false"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    批次号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispContainerID" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtDispSpecID" runat="server" Visible="false"></asp:TextBox>
                </td> 
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    图号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispProductID" runat="server" Visible="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    名称：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    数量：
                </td>
                <td align="left" class="tdRight">
                    <asp:TextBox ID="txtDispContainerQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtChildCount" runat="server" Visible="false"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    计划开始日期：
                </td>
                <td align="left" class="tdRight">
                    <asp:TextBox ID="txtDispPlannedStartDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    计划完成日期：
                </td>
                <td align="left" class="tdNoBorder">
                    <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div style="height:8px;width:100%;"></div>
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td rowspan="3" style="width:380px;">
                        <igtbl:UltraWebGrid ID="wgProductNoList" runat="server" Height="250px" Width="380px">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn AllowGroupBy="No" Key="ckSelect" Width="30px">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption="">
                                            </Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductNo" Key="ProductNo" Width="150px">
                                            <Header Caption="产品序号">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn Key="Qty" BaseColumnName="Qty" Width="70px" Hidden="True">
                                            <Header Caption="数量">
                                                <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="DisposeResult" Key="DisposeResult" Width="120px">
                                            <Header Caption="处理意见">
                                                <RowLayoutColumnInfo OriginX="3" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="3" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="LossReasonName" Key="LossReasonName" Width="120px" Hidden="True">
                                            <Header Caption="报废原因">
                                                <RowLayoutColumnInfo OriginX="4" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="4" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerName" Hidden="True" Key="ContainerName">
                                            <Header Caption="ContainerName">
                                                <RowLayoutColumnInfo OriginX="5" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="5" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                            <Header Caption="ContainerID">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ID" Hidden="True" Key="ID">
                                            <Header Caption="ID">
                                                <RowLayoutColumnInfo OriginX="7" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="7" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="LossReasonID" Hidden="True" Key="LossReasonID">
                                            <Header Caption="LossReasonID">
                                                <RowLayoutColumnInfo OriginX="8" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="8" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="250px" Width="380px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">质疑单号：
                    </td>
                    <td align="left" class="tdRightAndBottom" colspan="7">
                        <asp:TextBox ID="txtQuestionInfoName" runat="server" class="stdTextBox"></asp:TextBox>
                        <asp:TextBox ID="txtID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">数量：
                    </td>
                    <td align="left" class="tdRightAndBottom" colspan="3">
                        <asp:TextBox ID="txtQty" runat="server" CssClass="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap" width="170px">备注：
                    </td>
                    <td align="left" class="tdRight" colspan="7">
                        <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" Height="170px" TextMode="MultiLine" Width="95%"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    <div style="height:8px;width:100%;"></div>
    <div>
        <table style="width:100%;">
            <tr>
               <td style="text-align:left; width:100%;" colspan="2">
                    <asp:Button ID="btnMaterialApp" runat="server" Text="提交"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnMaterialApp_Click" />
                   <asp:Button ID="btnClose" runat="server" Text="关闭"
                        CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
               </td>
           </tr>
           <tr>
               <td style="font-size:12px; font-weight:bold;" nowrap="nowrap">状态信息：</td>
               <td style="text-align:left; width:100%;">
                   <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
               </td>
           </tr>
        </table>
    </div>
    </form>
</body>
</html>
