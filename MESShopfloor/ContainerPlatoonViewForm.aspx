﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="ContainerPlatoonViewForm.aspx.cs" Inherits="ContainerPlatoonViewForm" EnableViewState="true" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>

<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>

<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function openSpecDetailwins(DispatchinfoID) {
            window.showModalDialog("uMESSpecDetailInfo.aspx?DispatchinfoID=" + DispatchinfoID, "", "dialogWidth:1000px; dialogHeight:300px; status=no; center: Yes; resizable: NO;");
        }
        function openRGDetailwins(RGID) {
            window.showModalDialog("uMESRGDetailInfo.aspx?RGID=" + RGID, "", "dialogWidth:1440px; dialogHeight:310px; status=no; center: Yes; resizable: NO;");
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" style="margin-top:-15px">
        <div style="width:225px;float:left;margin-right:10px">
        <div>
        
        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;">
                <colgroup>
                  <col width="auto">
                  <col width="50">
                </colgroup>
                    <tr>
                        <td align="left"  class="tdRightAndBottom" colspan="1" >
                            <div class="divLabel">工作令号：</div>
                            <div style="height:28px;padding-right:2px">   
                            <asp:TextBox ID="txtTreeProcessNo" runat="server" class="stdTextBoxFull"></asp:TextBox>
                            </div>
                        </td>
                        

                        <td class="tdRightAndBottom"  align="right" valign="bottom" colspan="1">
                            <asp:Button ID="btnTreeSearch" runat="server" Text="查询"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnTreeSearch_Click"  />
                            <asp:Button ID="btnTreeReset" runat="server" Text="重置" Visible="false"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnTreeReset_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"   class="tdRightAndBottom" colspan="2" style="text-align:left">
                                    <%--<igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel2" runat="server" Width="100%">--%>
                                     <div>产品</div>
                                     <gPI:getProductInfo ID="getProductInfo" runat="server" />
                                     <%--</igmisc:WebAsyncRefreshPanel>--%>
                                </td>
                       
                    </tr>
                </table>

        </div>
        <ignav:UltraWebTree ID="treeProduct" runat="server" Height="510px" Width="220px" Style="border:1px solid #333;margin-top:3px"
            WebTreeTarget="ClassicTree" OnNodeSelectionChanged="treeProduct_NodeSelectionChanged">                
                <AutoPostBackFlags NodeChanged="True" NodeChecked="False" NodeCollapsed="False" NodeDropped="False" NodeExpanded="False" />
            </ignav:UltraWebTree>
        
    </div>


        <div style="height:100%">

        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;border-bottom:none">
                <colgroup>
                  <col width="auto">
                    <col width="auto">
                    <col width="auto">
                      <col width="auto">
                   <%-- <col width="auto">--%>
                  <col width="260">
                    <col width="60">
                </colgroup>
                <tr style="display:none">
                    <td align="left" colspan="6" class="tdLRAndBottom">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdLRAndBottom">
                        <div class="divLabel">工作令号：</div>    
                        <div style="height:28px;padding-right:2px">                     
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBoxFull"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom" style="display:none">
                        <div class="divLabel">图号/名称：</div>
                        <div style="height:28px;padding-right:2px">   
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" class="tdRightAndBottom" >
                        <div class="divLabel">工序：</div>
                        <div style="height:28px;padding-right:2px"> 
                        <asp:TextBox ID="txtSpecName" runat="server" class="stdTextBoxFull"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" valign="bottom" class="tdRightAndBottom">
                        <div class="divLabel">任务状态：</div>
                        <asp:DropDownList ID="ddlStatus" runat="server" style="height:30px; font-size:16px;width:100%">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="未审核" Value="0"></asp:ListItem>
                            <asp:ListItem Text="审核通过" Value="10"></asp:ListItem>
                            <asp:ListItem Text="审核未通过" Value="20"></asp:ListItem>
                            <asp:ListItem Text="已下发" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                     <td align="left" nowrap="nowrap" class="tdRightAndBottom">
                        <div class="divLabel">计划开始日期：</div>
                        <div style="height:28px;padding-right:2px">   
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" style="width:110px" type="text" />
                        -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" style="width:110px" type="text" />
                            </div>
                    </td>
                    <td align="left" valign="bottom" class="tdRightAndBottom" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置" Visible="false"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
                <tr>
                     <td align="left" class="tdLRAndBottom" style="display:none" >
                        <div class="divLabel">工作令号：</div>
                         <div style="height:28px;padding-right:2px">   
                        <asp:TextBox ID="txtSrTreeProcessNo" runat="server" class="stdTextBoxFull" Enabled="false"></asp:TextBox>
                            </div>
                    </td>
                     <td align="left" class="tdLRAndBottom">
                        <div class="divLabel">所选图号：</div>
                          <div style="height:28px;padding-right:2px"> 
                        <asp:TextBox ID="txtSrTreeProduct" runat="server" class="stdTextBoxFull" Enabled="false"></asp:TextBox>
                           </div>
                    </td>
                    <td class="tdRightAndBottom" nowrap="nowrap" colspan="5" valign="bottom" align="left" >
                        <asp:Button ID="btnSearchAll" runat="server" Text="查询该产品及往下所有" OnClientClick=" return ((confirm('查询往下所有零组件，数据量大效率低，确认继续？') == true) ? true :  false)"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearchAll_Click"  />
                        <asp:Button ID="Button5" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="padding-left: 5px;" colspan="6">
                        <table style="width: 400px">
                            <tr>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox17" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Style="text-align: center; vertical-align: middle;"
                                        Font-Bold="True" Width="56px">图例：</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox14" runat="server" BackColor="LightSkyBlue" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox4" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="90px">未审核</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox11" runat="server" BackColor="LightGreen" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox10" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="90px">审核通过</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox2" runat="server" BackColor="LightYellow" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>

                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox7" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="90px">审核未通过</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox13" runat="server" BackColor="LightSeaGreen" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox12" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="90px">已下发</asp:TextBox>
                                </td>

                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox9" runat="server" BackColor="Yellow" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox8" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="90px">已外协</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox1" runat="server" BackColor="Red" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>

                                </td>

                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox6" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="90px">产能冲突</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox3" runat="server" BackColor="Green" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox5" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="90px">正常</asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <igtbl:UltraWebGrid ID="wgResult" runat="server" Height="400px" Width="100%" OnDataBound="wgResult_DataBound">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" RowSelectorsDefault="No"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="15px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="400px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 8px; width: 100%;"></div>
        <div style="text-align: left; float: left;margin-left:3px">
            <asp:Button ID="btnSelect1" runat="server" Text="全选"
                            CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnSelect1_Click" />
            <asp:Button ID="btnSelect2" runat="server" Text="反选"
                            CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnSelect2_Click" />
        </div>
        <div style="text-align: right; float: right">
            <uPT:pageTurning ID="upageTurning" runat="server" />
        </div>
        <div style="height: 8px; width: 100%;clear:both"></div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;" colspan="2">
                        <asp:Button ID="btnAPS" runat="server" Text="下发"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnAPS_Click" style="height: 26px" />
                        <asp:Button ID="Button1" runat="server" Text="图纸工艺查看"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hdScanCon" runat="server" />

        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
