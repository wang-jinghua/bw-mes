﻿<%@ Page Title="" Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="ReplaceMaterialInfoForm.aspx.cs" Inherits="ReplaceMaterialInfoForm" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" Style="margin-top: -15px">
        <table border="0" cellpadding="5" cellspacing="0" width="100%" style="table-layout: fixed;">
            <colgroup>
                <col width="120">
                <col width="150">
                <col width="auto">
            </colgroup>
            <tr>
                <td class="tdRightAndBottom">
                    <div>工作令号</div>
                    <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom">
                    <div>批次</div>
                    <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" valign="bottom">
                    <asp:Button ID="btnSearch" runat="server" Text="查询"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnReSet" runat="server" Text="重置" Visible="true"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                </td>
            </tr>

        </table>

        <table border="0" cellpadding="5" cellspacing="0" width="100%" style="table-layout: fixed;">
            <colgroup>
                <col width="auto">
            </colgroup>
            <tr>
                <td class="" style="font-weight: 400">
                    <igtbl:UltraWebGrid ID="wgContainer" runat="server" Height="200px" Width="300px" OnActiveRowChange="wgContainer_ActiveRowChange" >
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProcessNo" Key="ProcessNo" Width="150px" Hidden="false">
                                        <Header Caption="工作令号"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ContainerName" Key="ContainerName" Width="150px" Hidden="false">
                                        <Header Caption="批次"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductName" Key="ProductName" Width="150px" Hidden="true">
                                        <Header Caption="图号"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductDesc" Key="ProductDesc" Width="150px">
                                        <Header Caption="产品名称"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductID" Key="ProductID" Width="150px" Hidden="true">
                                        <Header Caption="ProductID"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductRev" Key="ProductRev" Width="80px" Hidden="true">
                                        <Header Caption="产品版本"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowName" Key="WorkflowName" Width="150px">
                                        <Header Caption="工艺"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="workflowrevision" Key="WorkflowRev" Width="80px">
                                        <Header Caption="工艺版本"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowID" Key="WorkflowID" Width="150px" Hidden="true">
                                        <Header Caption="WorkflowID"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="Qty" Key="Qty" Width="80px">
                                        <Header Caption="批次数量"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="MaterialName" Key="MaterialName" Width="150px">
                                        <Header Caption="材料名称"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="RawMaterial" Key="RawMaterial" Width="150px">
                                        <Header Caption="原材料信息"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ReplaceMaterial" Key="ReplaceMaterial" Width="150px">
                                        <Header Caption="代料信息"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="replacematerialname" Key="ReplaceMaterialName" Width="150px">
                                        <Header Caption="代料名称"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="replacematerialqty" Key="ReplaceMaterialQty" Width="150px">
                                        <Header Caption="所需代料数"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="containerlevelname" Key="ContainerLevelName" Width="100px" Hidden="true">
                                        <Header Caption="containerlevelname"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductID" Key="ProductID" Width="100px" Hidden="true">
                                        <Header Caption="ProductID"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowID" Key="WorkflowID" Width="100px" Hidden="true">
                                        <Header Caption="WorkflowID"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ContainerID" Key="ContainerID" Width="100px" Hidden="true">
                                        <Header Caption="ContainerID"></Header>
                                    </igtbl:UltraGridColumn> 
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="processquota" Key="processquota" Width="100px" Hidden="true">
                                        <Header Caption="材料定额"></Header>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="false">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px" Font-Bold="false">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Font-Bold="false"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <uPT:pageTurning ID="upageTurning" runat="server" Visible="true" />
                </td>
            </tr>
        </table>

        <table border="0" cellpadding="5" cellspacing="0" width="100%" style="table-layout: fixed;">
            <colgroup>
                <col width="800">
                <col width="150">
                <col width="150">
                <col width="auto">
            </colgroup>
            <tr style="font-weight: 400">
                <td colspan="1" rowspan="3">
                    <div>erp物料信息</div>
                    <igtbl:UltraWebGrid ID="wgReplaceMaterial" runat="server" Height="200px" Width="300px">
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="model" Key="ReplaceMaterial" Width="150px" Hidden="false">
                                        <Header Caption="规格型号"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="material" Key="Material" Width="150px" Hidden="false">
                                        <Header Caption="物料编码"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="MaterialName" Key="MaterialName" Width="150px" Hidden="false">
                                        <Header Caption="物料名称"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WarehouseName" Key="WarehouseName" Width="150px" Hidden="false">
                                        <Header Caption="仓库"></Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="baseQty" Key="Qty" Width="100px" Hidden="false">
                                        <Header Caption="数量"></Header>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="100%">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="false">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px" Font-Bold="false">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Font-Bold="false"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
                </td>
                <td class="" style="font-weight: 400;" valign="top">
                    <div>Erp物料编码</div>
                    <asp:TextBox ID="txtErpMaterial" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                </td>
                <td class="" style="font-weight: 400;" valign="top">
                    <div>物料名称</div>
                    <asp:TextBox ID="txtErpMaterialName" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                </td>
                <td class="" valign="top">
                    <asp:Button ID="btnErpSearch" runat="server" Text="查询" Style="margin-top: 16px"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnErpSearch_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <div>所需代料数</div>
                    <asp:TextBox ID="txtReplaceMaterialQty" runat="server" class="stdTextBoxFull" Enabled="true" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                </td>
                <td>
                <div>材料定额</div>
                    <asp:TextBox ID="txtMaterialQuota" runat="server" class="stdTextBoxFull" Enabled="true" onkeyup="if(isNaN(value))execCommand('undo')"  onafterpaste="if(isNaN(value))execCommand('undo')"></asp:TextBox>
                </td>
            </tr>
            <tr>

                <td valign="bottom" colspan="3">
                    <div>附件</div>
                    <asp:FileUpload ID="bffSelectPath" Style="height: 26px; width: 380px;"
                        runat="server" />
                </td>
                <td></td>
            </tr>
        </table>
        <div id="showNotes" style="width: 400px; height: 100px; position: absolute; top: 46%; left: 36%; background-color: White;" visible="false" runat="server">
            <table align="center" style="border-width: 1px; border-color: black;border-style: solid;">
                <tr style=" width: 400px;">
                    <td align="left" colspan="2">信息提示：</td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="height: 30px; font-size: 10pt">是否添加附件！</td>
                </tr>
                <tr style="background-color: gainsboro; width: 400px;">
                    <td align="right" style="width: 200px">
                        <asp:Button ID="btnYes" runat="server" Text="是" DisplayInFrame="False" OnClick="btnYes_Click" Height="30px" Width="50px" />
                    </td>
                    <td align="left" style="width: 200px">
                        <asp:Button ID="btnNo" runat="server" Text="否" DisplayInFrame="False" OnClick="btnNo_Click" Height="30px" Width="50px" />
                    </td>
                </tr>
            </table>
        </div>
    </igmisc:WebAsyncRefreshPanel>
    <table border="0" cellpadding="5" cellspacing="0" width="100%">
        <tr>
            <td class="">
                <asp:Button ID="Button2" runat="server" Text="保存代料" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                    CssClass="searchButton" EnableTheming="True" OnClick="Button2_Click" />
                <asp:Button ID="Button1" runat="server" Text="图纸工艺查看" Style="margin-left: 10px"
                    CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                <asp:Button ID="btnSave" runat="server" Text="保存代料" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" Visible="false" />
                <asp:Button ID="btnSave2" runat="server" Text="保存附件" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)" Style="margin-left: 10px"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnSave2_Click" Visible="false" />
            </td>
        </tr>
    </table>
</asp:Content>

