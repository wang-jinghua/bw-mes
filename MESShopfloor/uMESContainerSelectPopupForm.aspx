﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESContainerSelectPopupForm.aspx.cs" Inherits="uMESContainerSelectPopupForm" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>批次选择</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
    <script language="javascript" type="text/javascript">
        function WinClose(result) {
            window.returnValue = result;
            window.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
                <div>
                    <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" colspan="5" class="tdBottom" style="display: none;">
                                <div class="ScanLabel">扫描：</div>
                                <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdRight">
                                <div class="divLabel">工作令号：</div>
                                <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                            </td>
                            <td align="left" class="tdRight">
                                <div class="divLabel">批次号：</div>
                                <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                            </td>
                            <td align="left" class="tdRight">
                                <div class="divLabel">图号/名称：</div>
                                <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                            </td>
                            <td align="left" nowrap="nowrap" class="tdRight">
                                <div class="divLabel">计划开始日期：</div>
                                <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                                -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                            </td>
                            <td class="tdNoBorder" style="text-align: left;" nowrap="nowrap">
                                <asp:Button ID="btnSearch" runat="server" Text="查询"
                                    CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" Style="height: 26px" />
                                <asp:Button ID="btnReSet" runat="server" Text="重置"
                                    CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                            </td>
                        </tr>
                    </table>
                </div>

                <div style="height: 8px; width: 100%;"></div>

                <div id="ItemDiv" runat="server">
                    <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="200px" Width="100%">
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                        <CellTemplate>
                                            <asp:CheckBox ID="ckSelect" runat="server" />
                                        </CellTemplate>
                                        <Header Caption=""></Header>
                                    </igtbl:TemplatedColumn>
                                    <igtbl:UltraGridColumn Key="ProcessNo" Width="130px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                        <Header Caption="工作令号">
                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                        </Header>

                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="130px" AllowGroupBy="No" Hidden="true">
                                        <Header Caption="作业令号">
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                        <Header Caption="批次号">
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="130px" AllowGroupBy="No">
                                        <Header Caption="图号">
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="130px" AllowGroupBy="No">
                                        <Header Caption="名称">
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="60px" AllowGroupBy="No">
                                        <Header Caption="数量">
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="PlannedStartDate" Key="PlannedStartDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                        <Header Caption="计划开始日期">
                                            <RowLayoutColumnInfo OriginX="7" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="7" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="PlannedCompletionDate" Key="PlannedCompletionDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                        <Header Caption="计划完成日期">
                                            <RowLayoutColumnInfo OriginX="8" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="8" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                        <Header Caption="ContainerID">
                                            <RowLayoutColumnInfo OriginX="9" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="9" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Hidden="True" Key="WorkflowID">
                                        <Header Caption="WorkflowID">
                                            <RowLayoutColumnInfo OriginX="10" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="10" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID">
                                        <Header Caption="ProductID">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="380px"
                                Width="100%">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
                    <div style="margin-right: 5px; text-align: right; float: right">
                        <uPT:pageTurning ID="upageTurning" runat="server" />
                    </div>
                </div>


                <div style="height: 8px; width: 100%;"></div>
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: left; width: 100%;" colspan="2">
                            <asp:Button ID="btnSave" runat="server" Text="确定" Font-Size="11pt"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" Width="72px" />
                            &nbsp  
                    <asp:Button ID="btnClose" runat="server" Text="关闭" Width="72px"
                        CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
                            <asp:TextBox ID="txtContainerID" runat="server" Width="200" Height="20px" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 12px; font-weight: bold;" nowrap="nowrap">状态信息：</td>
                        <td style="text-align: left; width: 100%;">
                            <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
                        </td>
                    </tr>
                </table>
            </igmisc:WebAsyncRefreshPanel>
        </div>
    </form>
</body>
</html>
