'Copyright ?1995-2007, Camstar Systems, Inc. All Rights Reserved.
'Description:产品序号选择弹出界面
'Copyright (c):通力凯顿（北京）系统集成有限公司
'Writer:苗利刚
'create Date:2016.9.28
'Rewriter:
'Rewrite Date:

Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Camstar.WebClient.WebServicesProxy.InSiteWebServices
Imports System.Data
Imports uMES.LeanManufacturing.DBUtility

Partial Class uMES_ProductNoForm
    Inherits System.Web.UI.Page

    Dim intPageSize As Integer = 1000

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                txtProductNo.Text = ""

                If IsNothing(Session("ParentContainerID")) = False Then
                    txtParentContainerID.Text = Session("ParentContainerID").ToString
                End If

                btnSearch_Click(sender, e)
            Catch ex As Exception
                tdMessage.InnerText = ex.Message
            End Try
        End If
    End Sub

    Protected Function GetTotalPage() As Integer
        ''确定总页数
        Dim strQuery As String = GetSql.Replace("c.containername,c.containerid,'' AS productno,rownum", "COUNT(*)")
        Dim DT As DataTable = OracleHelper.GetDataTable(strQuery)
        Dim intTotal As Integer = 0
        Dim intTotalPage As Integer = 0
        If DT.Rows.Count > 0 Then
            intTotal = CInt(DT.Rows(0)(0).ToString)
            intTotalPage = Int(intTotal / intPageSize)
        End If

        If intTotal Mod intPageSize <> 0 Then
            intTotalPage += 1
        End If

        txtTotalPage.Text = intTotalPage.ToString
        txtCurrentPage.Text = 0
        lLabel1.Text = "第  页  共 " & intTotalPage & " 页"

        GetTotalPage = intTotalPage
    End Function

    Protected Sub GetDataList(ByVal intPage As Integer)
        txtProductNo.Text = txtProductNoHidden.Text

        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)

        Dim strQuery As String = "SELECT * FROM" & vbCrLf
        strQuery += "(SELECT b.*, rownum AS rnum FROM (" & vbCrLf
        strQuery += GetSql() & vbCrLf
        strQuery += ") b" & vbCrLf
        strQuery += "WHERE rownum <= " & intPageSize * intPage & vbCrLf
        strQuery += ")WHERE rnum > " & intPageSize * (intPage - 1) & vbCrLf

        Dim DT As DataTable = OracleHelper.GetDataTable(strQuery)
        wglist.Clear()
        wglist.DataSource = DT
        wglist.DataBind()
        lLabel1.Text = "第 " & intPage & " 页  共 " & intTotalPage & " 页"
    End Sub

    Protected Function GetSql() As String
        Dim strProductNo As String = txtProductNo.Text.Trim
        Dim strParentContainerID As String = txtParentContainerID.Text.Trim

        Dim strQuery As String = "SELECT c.containername,c.containerid,'' AS productno,rownum" & vbCrLf &
                                "FROM container c" & vbCrLf &
                                "WHERE c.status = 1 AND c.parentcontainerid IS NOT NULL" & vbCrLf
        'If strParentContainerID <> "" Then
        strQuery += "AND c.parentcontainerid = '" & strParentContainerID & "'" & vbCrLf
        'End If

        If strProductNo <> "" Then
            strQuery += "AND LOWER(c.containername) LIKE '%" & strProductNo.ToLower & "%'" & vbCrLf
        End If

        strQuery += "ORDER BY c.containername ASC" & vbCrLf

        GetSql = strQuery
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            txtProductNoHidden.Text = txtProductNo.Text.Trim

            wglist.Clear()
            txtCurrentPage.Text = 1
            If GetTotalPage() > 0 Then
                txtCurrentPage.Text = 1
                GetDataList(1)
            End If
        Catch ex As Exception
            tdMessage.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtProductNo.Text = ""
        txtProductNoHidden.Text = ""

        lLabel1.Text = "第  页  共  页"
        wglist.Clear()
        txtPage.Text = ""
        txtTotalPage.Text = 0
        txtCurrentPage.Text = 0
        btnSearch_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strProductNoInfo As String = ""
            For i As Integer = 0 To wglist.Rows.Count - 1
                If wglist.Rows(i).Activated = True Then
                    Dim strContainerName As String = wglist.Rows(i).Cells.FromKey("ContainerName").Text.ToString
                    Dim strContainerID As String = wglist.Rows(i).Cells.FromKey("ContainerID").Text.ToString

                    strProductNoInfo = strContainerName & ":" & strContainerID

                    Exit For
                End If
            Next

            Response.Write("<script> parent.window.returnValue='" + strProductNoInfo + "' ; window.close()</script>")

        Catch ex As Exception
            tdMessage.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Write("<script> parent.window.returnValue='' ; window.close()</script>")
    End Sub

    Protected Sub lbtnFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnFirst.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)

        If intTotalPage > 0 Then
            txtCurrentPage.Text = 1
            GetDataList(1)
        End If
    End Sub

    Protected Sub lbtnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnPrev.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As Integer = txtCurrentPage.Text
        Dim intCurrentPage As Integer = CInt(strCurrentPage) - 1

        If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
            txtCurrentPage.Text = intCurrentPage
            GetDataList(intCurrentPage)
        End If
    End Sub

    Protected Sub lbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnNext.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As Integer = txtCurrentPage.Text
        Dim intCurrentPage As Integer = CInt(strCurrentPage) + 1

        If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
            txtCurrentPage.Text = intCurrentPage
            GetDataList(intCurrentPage)
        End If
    End Sub

    Protected Sub lbtnLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLast.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        If intTotalPage > 0 Then
            txtCurrentPage.Text = intTotalPage
            GetDataList(intTotalPage)
        End If
    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As String = txtPage.Text.Trim

        If IsNumeric(strCurrentPage) = True Then
            Dim intCurrentPage As Integer = CInt(strCurrentPage)
            If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
                txtCurrentPage.Text = intCurrentPage
                GetDataList(intCurrentPage)
            End If
            txtPage.Text = ""
        Else
            tdMessage.InnerText = "页数应为数字"
            txtPage.Focus()
            Exit Sub
        End If
    End Sub
End Class
