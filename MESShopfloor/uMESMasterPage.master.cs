﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
//using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using System.Xml.Linq;

public partial class uMESMasterPage : System.Web.UI.MasterPage
{
    public string strTitle = "";
    public string strNavigation = "";
    public HtmlGenericControl div;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["UserInfo"] == null)
        {
            //Response.Write("<script>window.top.location = \"/MESShopfloor\";</script>");
            string strScript = "<script>window.top.location = \"/MESShopfloor\";</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "", strScript);
            //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }

        div = this.divTit;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["UserInfo"] == null)
        //{
        //    Response.Write("<script>window.top.location = \"/MESShopfloor\";</script>");
        //}
    }

    public void ChangeFrame(Boolean boolIsReport)
    { 
        if (boolIsReport == true )
        {
            
            DivFrame.Style["width"]="98%";
            DivFrame.Style.Add("margin", "0px auto");
            DivFrame.Style.Add("margin-bottom", "5px");
            DivFrame.Style.Add("border", "1px solid #96C2F1");
            DivFrame.Style.Add("background-color", "#EFF7FF");
            DivFrame.Style.Add("vertical-align", "bottom");
            DivFrame.Style.Add("text-align", "center");

        }
        else
        {
            DivFrame.Style.Add("width","1500px");
            DivFrame.Style.Add("margin", "0px auto");
            DivFrame.Style.Add("margin-bottom", "5px");
            DivFrame.Style.Add("border", "1px solid #96C2F1");
            DivFrame.Style.Add("background-color", "#EFF7FF");
            DivFrame.Style.Add("vertical-align", "bottom");
            DivFrame.Style.Add("text-align", "center");
        }
    }
}
