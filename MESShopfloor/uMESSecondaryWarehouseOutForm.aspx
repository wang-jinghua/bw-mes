﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="uMESSecondaryWarehouseOutForm.aspx.cs" Inherits="uMESSecondaryWarehouseOutForm" EnableViewState="true" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="8" class="tdBottom">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>

                    <td align="left" class="tdRight">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">图号/名称：</div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight" style="width: 294px">
                        <div class="divLabel">入库时间：</div>
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                        <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td align="left" class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 8px; width: 100%;"></div>
        <%--    </igmisc:WebAsyncRefreshPanel>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel2" runat="server">--%>
        <div id="ItemDiv" runat="server">
            <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="320px" Width="100%" OnActiveRowChange="ItemGrid_ActiveRowChange">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                <CellTemplate>
                                    <asp:CheckBox ID="ckSelect" runat="server" />
                                </CellTemplate>
                                <Header Caption=""></Header>
                            </igtbl:TemplatedColumn>
                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                <Header Caption="工作令号">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="150px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="作业令号">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                <Header Caption="批次号">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                <Header Caption="图号">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                <Header Caption="名称">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                <Header Caption="批次数量">
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="StockQty" Hidden="false" Key="StockQty">
                                <Header Caption="库存数量">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                <Header Caption="ContainerID">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="productid" Hidden="True" Key="productid">
                                <Header Caption="productid">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="inqty" Hidden="True" Key="inqty">
                                <Header Caption="inqty">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="outqty" Hidden="True" Key="outqty">
                                <Header Caption="outqty">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="uomid" Hidden="True" Key="uomid">
                                <Header Caption="uomid">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="uomid" Hidden="True" Key="uomid">
                                <Header Caption="uomid">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="factorystockid" Hidden="True" Key="factorystockid">
                                <Header Caption="factorystockid">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>

                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="320px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
            <div style="text-align: right; float: right">
                <uPT:pageTurning ID="upageTurning" runat="server" />
            </div>
        </div>
        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div class="divLabel">工作令号：</div>
                        <div>
                            <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" class="tdRightAndBottom" style=" display:none">
                        <div class="divLabel">作业令号：</div>
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">图号：</div>
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">名称：</div>
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom" colspan="2">
                        <div class="divLabel">批次数量：</div>
                        <asp:TextBox ID="txtQty" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div class="divLabel">分厂二级库：</div>
                        <asp:DropDownList ID="ddlFactoryStock" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;" BackColor="LightGray" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div class="divLabel">出库原因：</div>
                        <asp:DropDownList ID="ddlStockReason" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;" BackColor="White">
                        </asp:DropDownList>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">出库数量：</div>
                        <asp:TextBox ID="txtOutQty" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">交接人：</div>
                        <asp:TextBox ID="txtInEmployee" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;" colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="保存"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="visibility: hidden;">
            <!--已入库总数量-->
            <asp:TextBox ID="txtAllInQty" runat="server" Visible="false"></asp:TextBox>
            <!--已出库总数量-->
            <asp:TextBox ID="txtAllOutQty" runat="server" Visible="false"></asp:TextBox>
            <!--单位ID-->
            <asp:TextBox ID="txtUomId" runat="server" Visible="false"></asp:TextBox>
            <!--批次ID-->
            <asp:TextBox ID="txtContainerId" runat="server" Visible="false"></asp:TextBox>
            <!--产品/图号ID-->
            <asp:TextBox ID="txtProductId" runat="server" Visible="false"></asp:TextBox>
            <!--交接人ID-->
            <asp:TextBox ID="txtEmployeeId" runat="server" Visible="false"></asp:TextBox>
            <!--库存数-->
            <asp:TextBox ID="txtStockQty" runat="server" Visible="false"></asp:TextBox>
            <asp:HiddenField ID="hdScanCon" runat="server" />
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
