'Copyright ?1995-2007, Camstar Systems, Inc. All Rights Reserved.
'Description:零组件图号选择弹出界面
'Copyright (c):通力凯顿（北京）系统集成有限公司
'Writer:苗利刚
'create Date:2016.9.28
'Rewriter:
'Rewrite Date:

Imports System.Drawing
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Camstar.WebClient.WebServicesProxy.InSiteWebServices
Imports System.Data
Imports uMES.LeanManufacturing.DBUtility

Partial Class uMES_ProductListForm
    Inherits System.Web.UI.Page

    Dim intPageSize As Integer = 15

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                txtProductName.Text = ""
                txtProductDescription.Text = ""
                
                btnSearch_Click(sender, e)
            Catch ex As Exception
                tdMessage.InnerText = ex.Message
            End Try
        End If
    End Sub

    Protected Function GetTotalPage() As Integer
        ''确定总页数
        Dim strQuery As String = GetSql.Replace("pb.productname,p.description,p.productrevision,rownum,p.productid," & vbCrLf & "   NVL(wfb.workflowname,'') AS workflowname,NVL(wf.workflowrevision,'') AS workflowrevision,NVL(wf.workflowid,'') AS workflowid", "COUNT(*)")
        Dim DT As DataTable = OracleHelper.GetDataTable(strQuery)
        Dim intTotal As Integer = 0
        Dim intTotalPage As Integer = 0
        If DT.Rows.Count > 0 Then
            intTotal = CInt(DT.Rows(0)(3).ToString)
            intTotalPage = Int(intTotal / intPageSize)
        End If

        If intTotal Mod intPageSize <> 0 Then
            intTotalPage += 1
        End If

        txtTotalPage.Text = intTotalPage.ToString
        txtCurrentPage.Text = 0
        lLabel1.Text = "第  页  共 " & intTotalPage & " 页"

        GetTotalPage = intTotalPage
    End Function

    Protected Sub GetDataList(ByVal intPage As Integer)
        txtProductName.Text = txtProductNameHidden.Text
        txtProductDescription.Text = txtProductDescHidden.Text

        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)

        Dim strQuery As String = "SELECT * FROM" & vbCrLf
        strQuery += "(SELECT b.*, rownum AS rnum FROM (" & vbCrLf
        strQuery += GetSql() & vbCrLf
        strQuery += ") b" & vbCrLf
        strQuery += "WHERE rownum <= " & intPageSize * intPage & vbCrLf
        strQuery += ")WHERE rnum > " & intPageSize * (intPage - 1) & vbCrLf

        Dim DT As DataTable = OracleHelper.GetDataTable(strQuery)
        wglist.Clear()
        wglist.DataSource = DT
        wglist.DataBind()
        lLabel1.Text = "第 " & intPage & " 页  共 " & intTotalPage & " 页"
    End Sub

    Protected Function GetSql() As String
        Dim strProductName As String = txtProductName.Text.Trim
        Dim strProductDesc As String = txtProductDescription.Text.Trim

        Dim strQuery As String = "select pb.productname,p.description,p.productrevision," & vbCrLf &
                                " rownum,p.productid,wfb.workflowname,wf.workflowrevision" & vbCrLf &
                                " from (select t.productid,t.productbaseid,t.description, " & vbCrLf &
                                " t.productrevision,(case" & vbCrLf &
                                " when t.workflowid = '0000000000000000' then" & vbCrLf &
                                " (select wfb.revofrcdid from workflowbase wfb" & vbCrLf &
                                " where wfb.workflowbaseid = t.workflowbaseid)" & vbCrLf &
                                " else t.workflowid end) as workflowid from product t) p" & vbCrLf &
                                " left join productbase pb on pb.productbaseid = p.productbaseid" & vbCrLf &
                                " left join workflow wf on wf.workflowid = p.workflowid" & vbCrLf &
                                " left join workflowbase wfb on wfb.workflowbaseid = wf.workflowbaseid" & vbCrLf &
                                " WHERE 1 = 1" & vbCrLf

        If strProductName <> "" Then
            strQuery += "AND LOWER(pb.productname) LIKE '%" & strProductName.ToLower & "%'" & vbCrLf
        End If
        If strProductDesc <> "" Then
            strQuery += "AND LOWER(p.description) LIKE '%" & strProductDesc.ToLower & "%'" & vbCrLf
        End If

        strQuery += "ORDER BY rownum desc" & vbCrLf

        GetSql = strQuery
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            txtProductNameHidden.Text = txtProductName.Text.Trim
            txtProductDescHidden.Text = txtProductDescription.Text.Trim

            wglist.Clear()
            txtCurrentPage.Text = 1
            If GetTotalPage() > 0 Then
                txtCurrentPage.Text = 1
                GetDataList(1)
            End If
        Catch ex As Exception
            tdMessage.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtProductName.Text = ""
        txtProductDescription.Text = ""
        txtProductNameHidden.Text = ""
        txtProductDescHidden.Text = ""

        lLabel1.Text = "第  页  共  页"
        wglist.Clear()
        txtPage.Text = ""
        txtTotalPage.Text = 0
        txtCurrentPage.Text = 0
        btnSearch_Click(sender, e)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim strProductInfo As String = ""
            For i As Integer = 0 To wglist.Rows.Count - 1
                If wglist.Rows(i).Activated = True Then
                    Dim strProductName As String = wglist.Rows(i).Cells.FromKey("ProductName").Text.ToString
                    Dim strProductRev As String = wglist.Rows(i).Cells.FromKey("ProductRevision").Text.ToString
                    Dim strProductID As String = wglist.Rows(i).Cells.FromKey("ProductID").Text.ToString

                    strProductInfo += strProductName & ":" & strProductRev & ":" & strProductID

                    Dim strDescription As String = ""
                    If IsNothing(wglist.Rows(i).Cells.FromKey("Description").Text) = False Then
                        strDescription = wglist.Rows(i).Cells.FromKey("Description").Text.Trim
                    End If

                    strProductInfo += ":" & strDescription

                    Exit For
                End If
            Next

            Response.Write("<script> parent.window.returnValue='" + strProductInfo + "' ; window.close()</script>")

        Catch ex As Exception
            tdMessage.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Write("<script> parent.window.returnValue='' ; window.close()</script>")
    End Sub

    Protected Sub lbtnFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnFirst.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)

        If intTotalPage > 0 Then
            txtCurrentPage.Text = 1
            GetDataList(1)
        End If
    End Sub

    Protected Sub lbtnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnPrev.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As Integer = txtCurrentPage.Text
        Dim intCurrentPage As Integer = CInt(strCurrentPage) - 1

        If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
            txtCurrentPage.Text = intCurrentPage
            GetDataList(intCurrentPage)
        End If
    End Sub

    Protected Sub lbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnNext.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As Integer = txtCurrentPage.Text
        Dim intCurrentPage As Integer = CInt(strCurrentPage) + 1

        If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
            txtCurrentPage.Text = intCurrentPage
            GetDataList(intCurrentPage)
        End If
    End Sub

    Protected Sub lbtnLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLast.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        If intTotalPage > 0 Then
            txtCurrentPage.Text = intTotalPage
            GetDataList(intTotalPage)
        End If
    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Dim intTotalPage As Integer = CInt(txtTotalPage.Text)
        Dim strCurrentPage As String = txtPage.Text.Trim

        If IsNumeric(strCurrentPage) = True Then
            Dim intCurrentPage As Integer = CInt(strCurrentPage)
            If intTotalPage > 0 And intCurrentPage <= intTotalPage And intCurrentPage > 0 Then
                txtCurrentPage.Text = intCurrentPage
                GetDataList(intCurrentPage)
            End If
            txtPage.Text = ""
        Else
            tdMessage.InnerText = "页数应为数字"
            txtPage.Focus()
            Exit Sub
        End If
    End Sub

    Protected Sub wglist_ActiveRowChange(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles wglist.ActiveRowChange
        Try
            txtWorkflowID.Text = e.Row.Cells.FromKey("WorkflowID").Text
        Catch ex As Exception
            tdMessage.InnerText = ex.Message
        End Try
    End Sub
End Class
