﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.Web.UI;

public partial class RejectApp3Form : ShopfloorPage, INormalReport
{
    const string QueryWhere = "RejectApp3Form";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESRejectAppBusiness rejectapp = new uMESRejectAppBusiness();
    uMESAssemblyRecordBusiness assembly = new uMESAssemblyRecordBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：领导审核";
        master.strTitle = "领导审核";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;

        ClearContainerDisp();
        ClearRejectAppDisp();
    }

    protected void ClearContainerDisp()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispProductID.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispContainerQty.Text = string.Empty;
        txtChildCount.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;
    }

    protected void ClearRejectAppDisp()
    {
        txtID.Text = string.Empty;
        txtStatus.Text = string.Empty;
        txtDispRejectAppInfoName.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSubmitFullName.Text = string.Empty;
        txtDispSubmitDate.Text = string.Empty;
        txtDispQualityNotes.Text = string.Empty;
        txtDisposeNotes.Text = string.Empty;
        txtDisposeNotes2.Text = string.Empty;
        txtDisposeNotes3.Text = string.Empty;
        txtDisposeNotes4.Text = string.Empty;
        txtRBQty.Text = string.Empty;
        txtFixQty.Text = string.Empty;
        txtScrapQty.Text = string.Empty;

        wgProductNoList.Clear();
    }
    #endregion

    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("SpecName", strSpecName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);
        result.Add("Status", "40");

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        try
        {
            uMESPagingDataDTO result = rejectapp.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 15);
            this.ItemGrid.DataSource = result.DBTable;
            this.ItemGrid.DataBind();
            this.txtTotalPage.Text = result.PageCount;
            if (result.RowCount == "0")
            {
                this.txtCurrentPage.Text = "0";
            }
            lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
            this.txtPage.Text = this.txtCurrentPage.Text;

            ClearContainerDisp();
            ClearRejectAppDisp();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion
    
    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("Status", "40");

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();
        ClearContainerDisp();
        try
        {
            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            DataTable DT = assembly.GetContainerInfo(strContainerName);

            if (DT.Rows.Count > 0)
            {
                string strProcessNo = DT.Rows[0]["ProcessNo"].ToString();
                txtDispProcessNo.Text = strProcessNo;

                string strOprNo = DT.Rows[0]["OprNo"].ToString();
                txtDispOprNo.Text = strOprNo;

                txtDispContainerName.Text = strContainerName;

                string strContainerID = DT.Rows[0]["ContainerID"].ToString();
                txtDispContainerID.Text = strContainerID;

                string strWorkflowID = DT.Rows[0]["WorkflowID"].ToString();
                txtDispWorkflowID.Text = strWorkflowID;

                string strProductName = DT.Rows[0]["ProductName"].ToString();
                txtDispProductName.Text = strProductName;
                string strProductID = DT.Rows[0]["ProductID"].ToString();
                txtDispProductID.Text = strProductID;

                string strDescription = DT.Rows[0]["Description"].ToString();
                txtDispDescription.Text = strDescription;

                string strContainerQty = DT.Rows[0]["Qty"].ToString();
                txtDispContainerQty.Text = strContainerQty;

                string strChildCount = DT.Rows[0]["ChildCount"].ToString();
                txtChildCount.Text = strChildCount;

                string strPlannedStartDate = DT.Rows[0]["PlannedStartDate"].ToString();
                if (strPlannedStartDate != string.Empty)
                {
                    txtDispPlannedStartDate.Text = Convert.ToDateTime(strPlannedStartDate).ToString("yyyy-MM-dd");
                }

                string strPlannedCompletionDate = DT.Rows[0]["PlannedCompletionDate"].ToString();
                if (strPlannedCompletionDate != string.Empty)
                {
                    txtDispPlannedCompletionDate.Text = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                }
            }

            txtID.Text = string.Empty;
            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtID.Text = strID;

            txtStatus.Text = string.Empty;
            string strStatus = e.Row.Cells.FromKey("Status").Value.ToString();
            txtStatus.Text = strStatus;

            txtDispRejectAppInfoName.Text = string.Empty;
            string strRejectAppInfoName = e.Row.Cells.FromKey("RejectAppInfoName").Value.ToString();
            txtDispRejectAppInfoName.Text = strRejectAppInfoName;

            txtDispSpecName.Text = string.Empty;
            string strSpecName = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
            txtDispSpecName.Text = strSpecName;

            txtDispQty.Text = string.Empty;
            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            txtDispSubmitFullName.Text = string.Empty;
            string strSubmitFullName = e.Row.Cells.FromKey("SubmitFullName").Value.ToString();
            txtDispSubmitFullName.Text = strSubmitFullName;

            txtDispQualityNotes.Text = string.Empty;
            if (e.Row.Cells.FromKey("QualityNotes").Value != null)
            {
                string strQualityNotes = e.Row.Cells.FromKey("QualityNotes").Value.ToString();
                txtDispQualityNotes.Text = strQualityNotes;
            }

            txtDispSubmitDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("SubmitDate").Value != null)
            {
                string strSubmitDate = e.Row.Cells.FromKey("SubmitDate").Value.ToString();
                txtDispSubmitDate.Text = Convert.ToDateTime(strSubmitDate).ToString("yyyy-MM-dd");
            }

            txtDisposeNotes.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeNotes").Value != null)
            {
                string strDisposeNotes = e.Row.Cells.FromKey("DisposeNotes").Value.ToString();
                txtDisposeNotes.Text = strDisposeNotes;
            }

            txtDisposeNotes2.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeNotes2").Value != null)
            {
                string strDisposeNotes2 = e.Row.Cells.FromKey("DisposeNotes2").Value.ToString();
                txtDisposeNotes2.Text = strDisposeNotes2;
            }

            txtDisposeNotes3.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeNotes3").Value != null)
            {
                string strDisposeNotes3 = e.Row.Cells.FromKey("DisposeNotes3").Value.ToString();
                txtDisposeNotes3.Text = strDisposeNotes3;
            }

            txtRBQty.Text = string.Empty;
            string strRBQty = e.Row.Cells.FromKey("RBQty").Value.ToString();
            txtRBQty.Text = strRBQty;

            txtFixQty.Text = string.Empty;
            string strFixQty = e.Row.Cells.FromKey("FixQty").Value.ToString();
            txtFixQty.Text = strFixQty;

            txtScrapQty.Text = string.Empty;
            string strScrapQty = e.Row.Cells.FromKey("ScrapQty").Value.ToString();
            txtScrapQty.Text = strScrapQty;

            DataTable dtProductNo = rejectapp.GetProductNoByRejectApp(strID);
            wgProductNoList.DataSource = dtProductNo;
            wgProductNoList.DataBind();

            if (Convert.ToInt32(txtChildCount.Text) > 0)
            {
                wgProductNoList.Columns.FromKey("ProductNo").Hidden = false;
                wgProductNoList.Columns.FromKey("Qty").Hidden = true;
            }
            else
            {
                wgProductNoList.Columns.FromKey("ProductNo").Hidden = true;
                wgProductNoList.Columns.FromKey("Qty").Hidden = false;
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择要审核的不合格品审理单", false);
                return;
            }

            if (CheckData() == false)
            {
                return;
            }

            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strOprEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> paraH = new Dictionary<string, string>();
            paraH.Add("RejectAppInfoID", strID);
            paraH.Add("OprType", "50");
            paraH.Add("OprEmployeeID", strOprEmployeeID);
            paraH.Add("OprDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            paraH.Add("Notes", "");

            string strDisposeNotes4 = txtDisposeNotes4.Text.Trim();
            
            Dictionary<string, string> paraM = new Dictionary<string, string>();
            paraM.Add("Status", "50");
            paraM.Add("DisposeNotes4", strDisposeNotes4);

            rejectapp.AppRejectAppInfo(paraH, paraM, null);


            QueryData(GetQuery());

            ClearContainerDisp();
            ClearRejectAppDisp();

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 数据验证
    protected Boolean CheckData()
    {
        string strDisposeNotes4 = txtDisposeNotes4.Text.Trim();
        if (strDisposeNotes4 == string.Empty)
        {
            DisplayMessage("请填写审核意见", false);
            txtDisposeNotes4.Focus();
            return false;
        }

        return true;
    }
    #endregion
    #endregion
    
    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            if (ItemGrid.Rows[i].Cells.FromKey("SpecName").Value != null)
            {
                string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
                ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Value = common.GetSpecNameWithOutProdName(strSpecName);
            }
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();

            UltraGridRow uldr = ItemGrid.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                DisplayMessage("请选择订单记录", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            DataTable poupDt = new DataTable();
            poupDt.Columns.Add("ProductID");
            poupDt.Columns.Add("WorkflowID");
            DataRow newRow = poupDt.NewRow();
            newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
            newRow["WorkflowID"] = uldr.Cells.FromKey("Workflowid").Text;
            poupDt.Rows.Add(newRow);
            Session.Add("ProcessDocument", poupDt);
            //string strScript = string.Empty;

            //strScript = "<script>window.showModalDialog('Custom/bwCommonPage/uMESDocumentViewPopupForm.aspx', '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
            var page = "Custom/bwCommonPage/uMESDocumentViewPopupForm.aspx";
            var script = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", page, 700, 600);
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, script);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {

            ClearMessage();

            UltraGridRow uldr = ItemGrid.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                DisplayMessage("请选择订单记录", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            string strContainerID = uldr.Cells.FromKey("containerid").Text;
            string strSpecID = uldr.Cells.FromKey("specid").Text;
            string strWorkflowID = uldr.Cells.FromKey("WorkflowID").Text;
            string strScript = string.Empty;
            strScript = "<script>window.showModalDialog('Custom/bwDataCollect/uMESDataCollectDetailInfoPopupForm.aspx?ContainerID=" + strContainerID + "&SpecID=" + strSpecID + "&WorkflowID=" + strWorkflowID + "', '', 'dialogWidth: 1080px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}