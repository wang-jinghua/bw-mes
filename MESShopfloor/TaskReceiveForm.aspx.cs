﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Web.UI;

public partial class TaskReceiveForm : ShopfloorPage
{
    const string QueryWhere = "TaskReceiveForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();
    uMESSecondaryWarehouseBusiness stockBal = new uMESSecondaryWarehouseBusiness();
    int m_PageSize = 9;
    string businessName = "报工管理", parentName = "dispatchinfo";
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：任务接收";
        master.strTitle = "任务接收";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        //normalCntrl.LtnFirst = lbtnFirst;
        //normalCntrl.LtnLast = lbtnLast;
        //normalCntrl.LtnNext = lbtnNext;
        //normalCntrl.LtnPrev = lbtnPrev;
        //normalCntrl.BtnReset = btnReSet;
        //normalCntrl.BtnGo = btnGo;
        //normalCntrl.BtnSearch = btnSearch;
        //normalCntrl.LabPages = lLabel1;
        //normalCntrl.TxtPage = txtPage;
        //normalCntrl.TxtTotalPage = txtTotalPage;
        //normalCntrl.TxtCurrentPage = txtCurrentPage;
        //normalCntrl.NormalOperation = this;
        //normalCntrl.QueryWhere = QueryWhere;

        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { QueryData(upageTurning.CurrentPageIndex); });
        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 数据查询
    //查询按钮
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            hdScanCon.Value = string.Empty;
            QueryData(upageTurning.CurrentPageIndex);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";
            hdScanCon.Value = string.Empty;
            if (strScan != string.Empty)
            {

                hdScanCon.Value = strScan;
                QueryData(1);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        hdScanCon.Value = string.Empty;
        ResetQuery();
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strTeamID = userInfo["TeamID"];
        result.Add("TeamID", strTeamID);
        result.Add("DispatchType", "1");
        result.Add("Status", "0");
        if (!string.IsNullOrEmpty(hdScanCon.Value))
        {
            result.Add("ScanContainerName", hdScanCon.Value);
        }
        else
        {

            result.Add("ProcessNo", strProcessNo);
            result.Add("ContainerName", strContainerName);
            result.Add("ProductName", strProductName);
            result.Add("SpecName", strSpecName);
            result.Add("StartDate", strStartDate);
            result.Add("EndDate", strEndDate);
        }

        result.Add("ReportEmployeeID", userInfo["EmployeeID"].ToString());//add:Wangjh 20201116 指定的人才能接收

        return result;
    }

    public void QueryData(int intIndex)
    {
        ClearMessage();

        ClearDispData();
        uMESPagingDataDTO result = dispatch.GetSourceData(GetQuery(), intIndex, m_PageSize);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;

     
    }

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        //this.txtTotalPage.Text = "";
        //this.txtCurrentPage.Text = "";
        //this.txtPage.Text = "";
        //lLabel1.Text = "第  页  共  页";
    }
    #endregion

   

    
    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProcessNo").Value != null)
            {
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = string.Empty;
            if (e.Row.Cells.FromKey("ContainerName").Value!=null)
            {
                strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            }
            txtDispContainerName.Text = strContainerName;

            string strContainerID = string.Empty;
            if (e.Row.Cells.FromKey("ContainerID").Value!=null)
            {
                strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            }
            txtDispContainerID.Text = strContainerID;

            string strProductName = string.Empty;
            if (e.Row.Cells.FromKey("ProductName").Value!=null)
            {
                strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            }
            txtDispProductName.Text = strProductName;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }

            string strQty = string.Empty;
            if (e.Row.Cells.FromKey("Qty").Value!=null)
            {
                strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            }
            txtDispQty.Text = strQty;

            string strSpec = string.Empty;
            if (e.Row.Cells.FromKey("SpecNameDisp").Value!=null)
            {
                strSpec = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
            }
            txtDispSpecName.Text = strSpec;

            string strWorkflowID = string.Empty;
            if (e.Row.Cells.FromKey("WorkflowID").Value!=null)
            {
                strWorkflowID = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            }
            txtDispWorkflowID.Text = strWorkflowID;

            string strSpecID = string.Empty;
            if (e.Row.Cells.FromKey("SpecID").Value!=null)
            {
                strSpecID = e.Row.Cells.FromKey("SpecID").Value.ToString();
            }
            txtDispSpecID.Text = strSpecID;

            string strTeamName = string.Empty;
            if (e.Row.Cells.FromKey("TeamName").Value !=null)
            {
            strTeamName= e.Row.Cells.FromKey("TeamName").Value.ToString();
            }
            txtDispTeamName.Text = strTeamName;

            string strTeamID = string.Empty;
            if (e.Row.Cells.FromKey("TeamID").Value !=null)
            {
                strTeamID= e.Row.Cells.FromKey("TeamID").Value.ToString();
            }
            txtDispTeamID.Text = strTeamID;

            string strResourceName = string.Empty;
            if (e.Row.Cells.FromKey("ResourceName").Value !=null)
            {
                strResourceName= e.Row.Cells.FromKey("ResourceName").Value.ToString();
            }
            txtDispResourceName.Text = strResourceName;
            string strResourceID = string.Empty;
            if (e.Row.Cells.FromKey("ResourceID").Value !=null)
            {
                strResourceID = e.Row.Cells.FromKey("ResourceID").Value.ToString();
            }
            txtDispResourceID.Text = strResourceID;

            DataTable DT = dispatch.GetResourceDispatchInfo(strResourceID);

            wgDispatchList.DataSource = DT;
            wgDispatchList.DataBind();

            string strPlannedCompletionDate = string.Empty;
            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                
                if (e.Row.Cells.FromKey("PlannedCompletionDate").Value !=null)
                {
                    strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Value.ToString();
                    if (strPlannedCompletionDate!="")
                    {
                        strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                    }
                }
 
            }
            txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
            string strID =string.Empty;
            if (e.Row.Cells.FromKey("ID").Value!=null)
            {
                strID = e.Row.Cells.FromKey("ID").Value.ToString();
            }
            txtDispID.Text = strID;
            string strParentID = string.Empty;
            if (e.Row.Cells.FromKey("ParentID").Value !=null)
            {
                strParentID = e.Row.Cells.FromKey("ParentID").Value.ToString();
            }
            txtDispParentID.Text = strParentID;

            DataTable dtEmployee = dispatch.GetEmployeeByDispatchID(strID);
            wgEmployee.DataSource = dtEmployee;
            wgEmployee.DataBind();

            wgProductNo.Clear();
            DataTable dt = dispatch.GetProductNoByDispatchID(strID);

            wgProductNo.DataSource = dt;
            wgProductNo.DataBind();

            string strParentQty = string.Empty;
            if (e.Row.Cells.FromKey("ParentQty").Value!=null)
            {
                strParentQty= e.Row.Cells.FromKey("ParentQty").Value.ToString();
            }
                
            txtParentQty.Text = strParentQty;
           
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #endregion

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            int intCount = 0;
            TemplatedColumn temCell = (TemplatedColumn)ItemGrid.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    intCount++;
                    if (int.Parse(ItemGrid.Rows[i].Cells.FromKey("Sequence").Text) > 1 && ItemGrid.Rows[i].Cells.FromKey("MoveConfirm").Text!="是")
                    {
                        DisplayMessage(ItemGrid.Rows[i].Cells.FromKey("ContainerName").Text + "移工未确认，无法接收任务", false);
                        return;
                    }
                }

               
            }

            if (intCount == 0)
            {
                DisplayMessage("请选择要接收的任务", false);
                return;
            }

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                   

                    Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                    string strReceiveEmployeeID = userInfo["EmployeeID"];

                    Dictionary<string, string> para = new Dictionary<string, string>();
                    para.Add("ReceiveDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    para.Add("ReceiveEmployeeID", strReceiveEmployeeID);
                    para.Add("Status", "20"); //已接收

                    string strID = ItemGrid.Rows[i].Cells.FromKey("ID").Value.ToString();
                    dispatch.ReceiveTask(strID, para);

                    ContainerExcuteLog(ItemGrid.Rows[i]);
                }
            }    

            QueryData(1);

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #region 批次日志记录 add:Wangjh 20210121
    void ContainerExcuteLog(UltraGridRow row)
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        var ml = new MESAuditLog();
        ml.ContainerName = row.Cells.FromKey("ContainerName").Text; ml.ContainerID = row.Cells.FromKey("ContainerID").Text;
        ml.ParentID = row.Cells.FromKey("ID").Text; ; ml.ParentName = parentName;
        ml.CreateEmployeeID = userInfo["EmployeeID"];
        ml.BusinessName = businessName; ml.OperationType = 1;
        ml.Description = "任务接收:" + row.Cells.FromKey("SpecNameDisp").Text + ",被"+ userInfo["FullName"] + "接收";

        common.SaveMESAuditLog(ml);
    }
    #endregion

    protected void ClearDispData()
    {
        ClearMessage();
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispTeamName.Text = string.Empty;
        txtDispResourceName.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;
        txtDispID.Text = string.Empty;
        //hdScanCon.Value = string.Empty;
        wgEmployee.Clear();
        wgProductNo.Clear();
        wgDispatchList.Clear();
    }
    #endregion

    
    protected void wgDispatchList_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < wgDispatchList.Rows.Count; i++)
        {
            string strSpecName = wgDispatchList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            wgDispatchList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }



    protected void ItemGrid_InitializeRow(object sender, RowEventArgs e)
    {
        try {
            //查询显示线边库信息 add:Wangjh 20200921
            var para = new Dictionary<string, string>();
            //if (!string.IsNullOrEmpty(e.Row.Cells.FromKey("splitfromid").Text))
            //{
            //    para["ContainerID"] = e.Row.Cells.FromKey("splitfromid").Value.ToString();
            //}
            //else
            //{
                para["ContainerID"] = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            //}
            para["WorkflowID"] = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            para["SpecID"] = e.Row.Cells.FromKey("SpecID").Value.ToString(); ;
            DataTable tempDt = stockBal.GetSubmittostockinfo(para);
            if (tempDt.Rows.Count == 0)
            {
                e.Row.Cells.FromKey("MoveConfirm").Value = "否";
            }
            else
            {
                // e.Row.Cells.FromKey("FactoryStockName").Value = tempDt.Rows[0]["factorystockname"].ToString(); 
                e.Row.Cells.FromKey("MoveConfirm").Value = "是";
            }
            //
        }
        catch (Exception ex) {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();

            UltraGridRow uldr = ItemGrid.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                DisplayMessage("请选择订单记录", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            DataTable poupDt = new DataTable();
            poupDt.Columns.Add("ProductID");
            poupDt.Columns.Add("WorkflowID");
            DataRow newRow = poupDt.NewRow();
            newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
            newRow["WorkflowID"] = uldr.Cells.FromKey("Workflowid").Text;
            poupDt.Columns.Add("ContainerID"); poupDt.Columns.Add("ContainerName");
            newRow["ContainerID"] = uldr.Cells.FromKey("ContainerID").Text;
            newRow["ContainerName"] = uldr.Cells.FromKey("ContainerName").Text;
            poupDt.Rows.Add(newRow);
            Session.Add("ProcessDocument", poupDt);
            string strScript = string.Empty;

            strScript = "<script>window.showModalDialog('Custom/bwCommonPage/uMESDocumentViewPopupForm.aspx', '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}