
var over_Lay = null;
var over_Layed = null;
var ajaxloader = null;
var timer = 5000;
var timpop = 0;
function settimer(val) {
    timer = val;
}

xMouse = 0; yMouse = 0;      // globals to hold coordinates
//document.onmousemove=getMouse; // start event listener
function getMouse(e) {
    e = e || window.event;
    if (e.pageX || e.pageY) {
        xMouse = e.pageX; yMouse = e.pageY;
    } else {
        de = document.documentElement;
        b = document.body;
        xMouse = e.clientX + (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        yMouse = e.clientY + (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    //window.status = xMouse+"   " + yMouse;
}

function showMsg(divid, contenttxt, closeme) {
    displayAjaxImage(divid);
    if (closeme == 'false')
        displayAjaxContent(divid, contenttxt, 'false', 'false');
    else
        displayAjaxContent(divid, contenttxt, 'false', 'true');
}

function displayAjaxImage(ajaxload, imgpathdot) {
    ajaxloader = ajaxload;
    var loadstr = "<table width='100%'>";
    loadstr += "<tr>";
    loadstr += "<td align='center' heigth='50'>";
    loadstr += "<img src='" + imgpathdot + "images/loadingbar.gif'>";
    loadstr += "</td>";
    loadstr += "</tr>";
    loadstr += "<tr>";
    loadstr += "<td align='center' heigth='50'>";
    loadstr += "正在加载数据，请稍后...";
    loadstr += "</td>";
    loadstr += "</tr>";
    loadstr += "</table>";
    over_Lay = document.createElement("div");
    over_Lay.innerHTML = loadstr;
    over_Lay.style.position = "absolute";
    var divloader = document.getElementById(ajaxloader);
    divloader.appendChild(over_Lay);
    divloader.style.position = "absolute";
    divloader.style.zIndex = 1001;
    divloader.style.width = "300px";
    divloader.style.height = "100px";
    divloader.style.top = (document.documentElement.scrollTop + (document.documentElement.clientHeight - divloader.offsetHeight) / 2) + "px";
    divloader.style.left = (document.documentElement.scrollLeft + (document.documentElement.clientWidth - divloader.offsetWidth) / 2) + "px";
    //timpop1= setTimeout("displayAjaxContent('"+ajaxloader+"','Added Successfully','true','true')",timer);

    var displayDiv = document.getElementById("divDisable");
    if (displayDiv != null && displayDiv != 'undefined') {
        var popsize = getPageSize();
        displayDiv.style.display = "block";
        displayDiv.style.height = popsize[0];
        displayDiv.style.width = popsize[1];
    }
}

function displayAjaxContent(div, msg, typeError, closebutton) {
  
        if (over_Lay != null) {
            document.getElementById(div).removeChild(over_Lay);
        }
        var displayDiv = document.getElementById("divDisable");
        if (displayDiv != null) displayDiv.style.display = "none";

        if (msg == "")
            return;
        over_Lay = document.createElement("div");
        over_Lay.style.position = "absolute";
        //over_Lay.style.width = "100%";
        over_Lay.style.height = 20 + "px";
        over_Lay.style.right = 90 + "px";
        de = document.documentElement; b = document.body;
        yPos = (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
        if (yPos == 0)
            over_Lay.style.top = 110 + "px";
        else
            over_Lay.style.top = yPos + 110 + "px";
        if (closebutton == 'true') {
            if (typeError == 'false') {
                over_Lay.innerHTML = "<table align='right' class='responseNew'><tr><td align='center' class='responseNew'>&nbsp;&nbsp;" + msg + "</td><td align='right' width='40px'><a href='#' title='close'><img src='images/close.png' border='0'></a></td></tr></table>";
            } else {
                over_Lay.innerHTML = "<table align='right' class='error'><tr><td align='center'class='error'>&nbsp;&nbsp;" + msg + "</td><td align='right' width='40px'><a href='#' title='close'><img src='images/close.png' border='0'></a></td></tr></table>";
            }
        }
        else {
            if (typeError == 'false') {
                over_Lay.innerHTML = "<table align='right' class='responseNew'><tr><td align='center' class='responseNew'>&nbsp;&nbsp;" + msg + "</td></tr></table>";
            } else {
                over_Lay.innerHTML = "<table align='right' class='error'><tr><td align='center' class='error'>&nbsp;&nbsp;" + msg + "</td></tr></table>";
            }
        }
        if (typeError == 'false') {
            timpop = setTimeout("removedisplayAjaxContent(" + ajaxloader + ")", timer);
            over_Lay.onclick = function() {
                removedisplayAjaxContent(ajaxloader, timpop);
            }
        }
        else {
            over_Lay.onclick = function() {
                removedisplayAjaxContent(ajaxloader);
            }
        }
        document.getElementById(ajaxloader).appendChild(over_Lay);
        
    
}

function removedisplayAjaxContent(mydiv, clearID) {
    if (clearID != undefined)
        clearTimeout(clearID);
    document.getElementById(mydiv).removeChild(over_Lay);
}

/* check width and height */
var myWidth = 0, myHeight = 0;

var version_str = "";
var version_arr = "";
var major_ver = "";
var minor_ver = "";
var revision_ver = "";

function getPageSize() {
    var body = document.documentElement;
    var bodyOffsetWidth = 0;
    var bodyOffsetHeight = 0;
    var bodyScrollWidth = 0;
    var bodyScrollHeight = 0;
    var pageDimensions = [0, 0];

    pageDimensions[0] = document.body.clientHeight;
    pageDimensions[1] = document.body.clientWidth;

    bodyOffsetWidth = document.body.offsetWidth;
    bodyOffsetHeight = document.body.offsetHeight;
    bodyScrollWidth = document.body.scrollWidth;
    bodyScrollHeight = document.body.scrollHeight;

    if (bodyOffsetHeight > pageDimensions[0]) {
        pageDimensions[0] = bodyOffsetHeight;
    }

    if (bodyOffsetWidth > pageDimensions[1]) {
        pageDimensions[1] = bodyOffsetWidth;
    }

    if (bodyScrollHeight > pageDimensions[0]) {
        pageDimensions[0] = bodyScrollHeight;
    }

    if (bodyScrollWidth > pageDimensions[1]) {
        pageDimensions[1] = bodyScrollWidth;
    }

    pageDimensions[1] = bodyScrollWidth;
    return pageDimensions;
}

function ieresize() {
    try {
        var displayDiv = document.getElementById("divDisable");
        if (displayDiv != null && displayDiv != 'undefined' && displayDiv.style.display == "block") {
            var popsize = getPageSize();
            displayDiv.style.height = popsize[0];
            displayDiv.style.width = popsize[1];
        }
    }
    catch (e)
    { }
}