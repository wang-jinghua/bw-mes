﻿var treeMachine;
function loadTreeData(firstLoad) {
    var obj = 23;//document.getElementById("selDept");
    if (obj == null || obj == 'undefined') return;
    var detpID ="56cfc386-e29a-4208-95a4-fb746d9cd0e4";// obj.value;
    var deptName =23;// obj.options[obj.selectedIndex].text;
    //定义树使用了命名空间
    treeMachine = new produceTree.DivTree(deptName);
    //这2个变量是临时使用的，名字无所谓
    var node1, node2;

    //定义节点也要使用命名空间，使用深度遍历方法逐个定义
    var dt = new Ajax.Web.DataTable();
    
    dt = MasterPage.GetMachineGroup(detpID);
    if (dt.value != null && dt.value.Rows.length > 0) {
        for (var i = 0; i < dt.value.Rows.length; i++) {
            if (dt.value.Rows[i].MATOLGRPNAME != null && dt.value.Rows[i].MATOLGRPNAME.toString() != "") {
                node1 = new produceTree.Node(dt.value.Rows[i].MATOLGRPNAME);
                node1.id = dt.value.Rows[i].MATOLCODE;
                //treeMachine.add(node1);把此代码移到添加机床节点代码,如果判断没有机床节点,则此节点也不添加到树.

                if (dt.value.Rows[i].MATOLGRPID != null && dt.value.Rows[i].MATOLGRPID.toString != "") {
                    var mdt = new Ajax.Web.DataTable();
                    mdt = MasterPage.GetMachine(dt.value.Rows[i].MATOLGRPID);
                    if (mdt.value != null && mdt.value.Rows.length > 0) {
                        treeMachine.add(node1);
                        for (var j = 0; j < mdt.value.Rows.length; j++) 
                        {
                            if (mdt.value.Rows[j].MATOLNAME != null && mdt.value.Rows[j].MATOLNAME.toString != ""
                            && mdt.value.Rows[j].MATOLID != null && mdt.value.Rows[j].MATOLID.toString != "")
                            {
                                node2 = new produceTree.Node(mdt.value.Rows[j].MATOLNAME);
                                node2.id = mdt.value.Rows[j].MATOLCODE;
                                node1.add(node2);
                            }
                        }
                        mdt = null;
                    }
                }
            }
        }
        dt = null;
    }
    var showTreeDiv = document.getElementById('showMachineInDept');
    if(showTreeDiv.hasChildNodes())
    {
        for(var n=0;n < showTreeDiv.childNodes.length;n++)
            showTreeDiv.removeChild(showTreeDiv.childNodes[n]);
    }
    showTreeDiv.appendChild(treeMachine.div);
    treeMachine.init(null, null);

    //选择部门后加载机床组机床树自动进行统计
//    if (firstLoad == 1) {
//        if (window.confirm('是否选择该部门下的机床进行统计？')) {
//            return;
//        } else {
//            loadSelectedTabMaster();
//        }
//    }
}
var treeMachine1;
function loadProduceTreeData(firstLoad) {
    
    var obj = 23;//document.getElementById("selDept");
    if (obj == null || obj == 'undefined') return;
    var detpID ="9331fbc8-7089-449f-a95e-1272f1ebfcth";// obj.value;
    var deptName =23;// obj.options[obj.selectedIndex].text;
    //定义树使用了命名空间
    treeMachine1 = new produceTree.DivTree(deptName);
    //这2个变量是临时使用的，名字无所谓
    var node1, node2, node3;

    //定义节点也要使用命名空间，使用深度遍历方法逐个定义
    var dt = new Ajax.Web.DataTable();
    
    dt = MasterPage.GetTaskInfo(detpID,'',-1);
    if (dt.value != null && dt.value.Rows.length > 0)
    {
        for (var i = 0; i < dt.value.Rows.length; i++) 
        {
            if (dt.value.Rows[i].BOMNAME != null && dt.value.Rows[i].BOMNAME.toString() != "") 
            {
                node1 = new produceTree.Node(dt.value.Rows[i].BOMNAME);
                node1.id = dt.value.Rows[i].BOMPARTSONID; 
                //treeMachine.add(node1);把此代码移到添加机床节点代码,如果判断没有机床节点,则此节点也不添加到树.

                if (dt.value.Rows[i].BOMPARTSONID != null && dt.value.Rows[i].BOMPARTSONID.toString != "")
                 {
                    var mdt = new Ajax.Web.DataTable();
                    mdt = MasterPage.GetTaskInfo(detpID,dt.value.Rows[i].BOMPARTSONID,1);   
                    if (mdt.value != null && mdt.value.Rows.length > 0)
                     {
                       
                        for (var j = 0; j < mdt.value.Rows.length; j++) 
                        {
                          
                            if (mdt.value.Rows[j].BOMNAME != null && mdt.value.Rows[j].BOMNAME.toString != ""
                            && mdt.value.Rows[j].BOMPARTID != null && mdt.value.Rows[j].BOMPARTID.toString != "")
                            { 
                                treeMachine1.add(node1);
                                node2 = new produceTree.Node(mdt.value.Rows[j].BOMNAME);
                                node2.id = mdt.value.Rows[j].BOMPARTID;
                                node1.add(node2);
                                var mdt2 = new Ajax.Web.DataTable();
                                mdt2 = MasterPage.GetTaskInfo(detpID,dt.value.Rows[i].BOMPARTSONID,3);  
                                if (mdt2.value != null && mdt2.value.Rows.length > 0) 
                                {
                                  
                                    for (var j = 0; j < mdt2.value.Rows.length; j++) 
                                    {
                                         node3 = new produceTree.Node(mdt2.value.Rows[j].BOMNAME);
                                         node3.id = mdt2.value.Rows[j].BOMPARTID;
                                         node2.add(node3);
                                    }
                                    
                                } 
                              
                            }
                        }
                     
                        mdt = null;
                    }
                }
            }
           
        }
        dt = null;
    }
    var showTreeDiv = document.getElementById('showMachineInDept1');
    if(showTreeDiv.hasChildNodes())
    {
        for(var n=0;n < showTreeDiv.childNodes.length;n++)
            showTreeDiv.removeChild(showTreeDiv.childNodes[n]);
    }
    showTreeDiv.appendChild(treeMachine1.div);
    treeMachine1.init(null, null);

    //选择部门后加载机床组机床树自动进行统计
//    if (firstLoad == 1) {
//        if (window.confirm('是否选择该部门下的机床进行统计？')) {
//            return;
//        } else {
//            loadSelectedTabMaster();
//        }
//    }
}
function window.confirm(str) {
    execScript("n   =   (msgbox('" + str + "',vbYesNo,   '信息提示')=vbYes)", "vbscript");
    return (n);
} 




var treeMachine3;
function loadTreeData3(firstLoad) 
{   
    var rootname='车间';
    //定义树使用了命名空间
    treeMachine3 = new produceTree.DivTree(rootname);   
    var node1;
    //定义节点也要使用命名空间，使用深度遍历方法逐个定义
    var dt = new Ajax.Web.DataTable();    
    dt = MasterPage.bindWorkPlace(); 
    if (dt.value != null && dt.value.Rows.length > 0) 
    {
        for (var i = 0; i < dt.value.Rows.length; i++) 
        {
                if (dt.value.Rows[i].DEPTNAME != null && dt.value.Rows[i].RECORDID.toString() != "")
                {
                    node1 = new produceTree.Node(dt.value.Rows[i].DEPTNAME);
                    node1.id = dt.value.Rows[i].RECORDID;              
                    treeMachine3.add(node1);                          
                }
            
        }
        dt = null;
        
    }
   
    var showTreeDiv = document.getElementById('showMachineInDept2');
    if(showTreeDiv.hasChildNodes())
    {
        for(var n=0;n < showTreeDiv.childNodes.length;n++)
            showTreeDiv.removeChild(showTreeDiv.childNodes[n]);
    }
    showTreeDiv.appendChild(treeMachine3.div);
    treeMachine3.init(null, null);
    
}

var treeMachine4;
function loadTreeData4(firstLoad) {
    
    var obj = 23;//document.getElementById("selDept");
    if (obj == null || obj == 'undefined') return;
    var detpID ="9331fbc8-7089-449f-a95e-1272f1ebfcth";// obj.value;
    var deptName =23;// obj.options[obj.selectedIndex].text;
    //定义树使用了命名空间
    treeMachine1 = new produceTree.DivTree(deptName);
    //这2个变量是临时使用的，名字无所谓
    var node1, node2, node3;

    //定义节点也要使用命名空间，使用深度遍历方法逐个定义
    var dt = new Ajax.Web.DataTable();
    
    dt = MasterPage.GetTaskInfo(detpID,'',-1);
    if (dt.value != null && dt.value.Rows.length > 0)
    {
        for (var i = 0; i < dt.value.Rows.length; i++) 
        {
            if (dt.value.Rows[i].BOMNAME != null && dt.value.Rows[i].BOMNAME.toString() != "") 
            {
                node1 = new produceTree.Node(dt.value.Rows[i].BOMNAME);
                node1.id = dt.value.Rows[i].BOMPARTSONID; 
                //treeMachine.add(node1);把此代码移到添加机床节点代码,如果判断没有机床节点,则此节点也不添加到树.

                if (dt.value.Rows[i].BOMPARTSONID != null && dt.value.Rows[i].BOMPARTSONID.toString != "")
                 {
                    var mdt = new Ajax.Web.DataTable();
                    mdt = MasterPage.GetTaskInfo(detpID,dt.value.Rows[i].BOMPARTSONID,1);   
                    if (mdt.value != null && mdt.value.Rows.length > 0)
                     {
                       
                        for (var j = 0; j < mdt.value.Rows.length; j++) 
                        {
                          
                            if (mdt.value.Rows[j].BOMNAME != null && mdt.value.Rows[j].BOMNAME.toString != ""
                            && mdt.value.Rows[j].BOMPARTID != null && mdt.value.Rows[j].BOMPARTID.toString != "")
                            { 
                                treeMachine1.add(node1);
                                node2 = new produceTree.Node(mdt.value.Rows[j].BOMNAME);
                                node2.id = mdt.value.Rows[j].BOMPARTID;
                                node1.add(node2);
                                var mdt2 = new Ajax.Web.DataTable();
                                mdt2 = MasterPage.GetTaskInfo(detpID,dt.value.Rows[i].BOMPARTSONID,3);  
                                if (mdt2.value != null && mdt2.value.Rows.length > 0) 
                                {
                                  
                                    for (var j = 0; j < mdt2.value.Rows.length; j++) 
                                    {
                                         node3 = new produceTree.Node(mdt2.value.Rows[j].BOMNAME);
                                         node3.id = mdt2.value.Rows[j].BOMPARTID;
                                         node2.add(node3);
                                    }
                                    
                                } 
                              
                            }
                        }
                     
                        mdt = null;
                    }
                }
            }
           
        }
        dt = null;
    }
    var showTreeDiv = document.getElementById('showMachineInDept1');
    if(showTreeDiv.hasChildNodes())
    {
        for(var n=0;n < showTreeDiv.childNodes.length;n++)
            showTreeDiv.removeChild(showTreeDiv.childNodes[n]);
    }
    showTreeDiv.appendChild(treeMachine1.div);
    treeMachine1.init(null, null);

    //选择部门后加载机床组机床树自动进行统计
//    if (firstLoad == 1) {
//        if (window.confirm('是否选择该部门下的机床进行统计？')) {
//            return;
//        } else {
//            loadSelectedTabMaster();
//        }
//    }
}
 