// ---------------------------------------------------------------------------
// Example of howto: use OutlookBar
// ---------------------------------------------------------------------------

//create OutlookBar
var o = new createOutlookBar('Bar',0,0,screenSize.width-17,screenSize.height,'#eeeeee','white','blue','red','#99ccff','black');//'#000099') // OutlookBar
var p;

//create first panel
p = new createPanel('a0','下属企业查询');
p.addButton('../images/gov/ico_query.gif','企业查询','parent.mainfrm.location="EnterpriseQuery/EntQuery.aspx"');	
o.addPanel(p);

p = new createPanel('a4','工程报监业务');
p.addButton('../images/gov/ico_censor.gif','报监审批','parent.mainfrm.location="ProjectWatch/WatchQuery.aspx"');
p.addButton('../images/gov/ico_assess.gif','工程验收','parent.mainfrm.location="ProjectWatch/Assess/ProQuery.aspx"');
p.addButton('../images/gov/ico_query.gif','业务查询','parent.mainfrm.location="ProjectWatch/Query/WatchQuery.aspx"');
o.addPanel(p);

p = new createPanel('a5','文明工地评选');
p.addButton('../images/gov/ico_censor.gif','评选审批','parent.mainfrm.location="CiviSpot/SpotQuery.aspx"');
o.addPanel(p);
	
p = new createPanel('a6','事故处理业务');
p.addButton('../images/gov/ico_censor.gif','事故审批','parent.mainfrm.location="Accident/AccidentQuery.aspx"');
p.addButton('../images/gov/ico_query.gif','事故查询','parent.mainfrm.location="Accident/Query/AccidentQuery.aspx"');	
o.addPanel(p); 

//draw the OutlookBar
o.draw();  
