﻿function onsetcolor(obj) {
     obj.style.backgroundColor='#E7EEF7';
}

function outsetcolor(obj) {
     obj.style.removeAttribute("backgroundColor");
}

function bs(obj)
{
	obj.disabled=true;
	obj.style.width=100;
	obj.value='提交中，请等待';
}

function b1bs(obj)
{
	if(B1IsValid())
	{
		obj.disabled=true;
		obj.style.width=100;
		obj.value='提交中，请等待';
		return true;
	}
	else
	{
		return false;
	}
}

function b2bs(obj)
{
	if(B2IsValid())
	{
		obj.disabled=true;
		obj.style.width=100;
		obj.value='提交中，请等待';
		return true;
	}
}
function localShow()
{
	var uploadFilePath=document.forms[0].HtmlInputFile1.value.trim();
	fileTypecheck();
	if(document.forms[0].HtmlInputFile1.value.trim()!="")
	{
	ImageFrame.ImageChecker.src=document.forms[0].HtmlInputFile1.value;
	var imageWidth=ImageFrame.ImageChecker.width;
	var imageHeight=ImageFrame.ImageChecker.height;
	
	if(imageWidth>85 || imageHeight>110) {ImageFrame.location.reload(); alert("请上传适当尺寸图片文件 (85*110 以内)！"); return;}
	ImageFrame.location.reload();
	}
}
String.prototype.endsWith=function(str)
{
	return(this.substring(this.length-str.length)==str);
}

String.prototype.trim=function()
{
	return this.replace(/(\s*$)|(^\s*)/g, "");
}

function fileTypecheck()
{
	var uploadFilePath=document.forms[0].HtmlInputFile1.value.trim();
	if(uploadFilePath=="") 
	{
		document.all.btnUpload.disabled=true;
		return false;
	}
	if(!uploadFilePath.toLowerCase().endsWith("jpg"))
	{
	msg.innerHTML="请上传文件类型为jpg的文件！";
	document.all.btnUpload.disabled=true;
	return false;
	}
	else
	{
	msg.innerHTML="";
	document.all.btnUpload.disabled=false;
	}
}
function ifnull(s,s2)
{
    if (s.value == "")
    {
	  alert("有重要数据项没有填写,请填写后在进行保存！");
      s.focus();
      return false;
    }
    if(s2.value=="")
    {
       alert("有重要数据项没有填写,请填写后在进行保存！");
       s2.focus();
       return false;
    }
}
function trim(str)
{
    if(typeof(str)!="string") return "";
    var rexp=/\s*$/;
    var lexp=/^\s*/;
    return str.replace(rexp,"").replace(lexp,"");
}

function isInt(s)  //整数输入判断
{
   if (s.value == "") return true;
   var chk=parseInt(s.value,10);
   if(chk!=s.value || chk<0)
     {
       alert("该字段应是正整数！");
       s.focus();
       return false;
      }
   return true;
}
function isZH(bb)
{
	var num=0;
	var aa=bb.value.trim();
	if(aa=="") return true;
	/////////////
	var chk=parseInt(bb.value,10);
    if(chk!=bb.value || chk<0)
     {
       alert("该字段应是正整数！");
       bb.focus();
       return false;
      }
	/////////////
	for(var i=0; i<aa.length;i++)
	{
		if(aa.charCodeAt(i)>255)
		{
			num=num+2;
		}
		else
		{
			num=num+1;
		}
	}
	if(num==18||num==15)
	{
	
		return true;
	}
	else
	{	alert("身份证证号码不正确，应为15或18位！")
		bb.focus();
		return false;
		
	}
}
function isAge(s)  //年龄输入判断!
{
   if (s.value == "") return true;
   var chk=parseInt(s.value,10);
   if(chk!=s.value || chk<0 || chk>100)
     {
       alert("该字段应是正整数！");
       s.focus();
       return false;
      }
   return true;
}

function isFloat(s)  //浮点数输入判断
{
  if (s.value == "") return true;
   var chk=parseFloat(s.value);
   if(chk!=s.value)
     {
	  alert("该字段应是整数或小数！");
      s.focus();
      return false;
      }
   return true;
}

function isDate(obj) //时间输入判断
{
	var m,str,exp,d;
	str=trim(obj.value);
	if(str=="") return true;
	str=str.replace(/-0/g,"-");
	exp=/\d{4}-\d{1,2}-\d{1,2}/;
	if(str.match(exp)!=null)
    {
        m=str.split("-");
		d=new Date(Date.parse(m[1]+'-'+m[2]+'-'+m[0]));
		if(d>=new Date(1800,1,1) && d<=new Date(2070,6,6) && d.getFullYear()==parseInt(m[0]) && (d.getMonth()+1)==parseInt(m[1]) && d.getDate()==parseInt(m[2]))
		{
            return true;
		}
    }
	alert("该字段应是正确的日期值：YYYY-MM-DD！");
	obj.focus();
    return false;
}

function disableRightClick(e)
{
  var message = "Right click disabled";
  
  if(!document.rightClickDisabled) // initialize
  {
    if(document.layers) 
    {
      document.captureEvents(Event.MOUSEDOWN);
      document.onmousedown = disableRightClick;
    }
    else document.oncontextmenu = disableRightClick;
    return document.rightClickDisabled = true;
  }
  if(document.layers || (document.getElementById && !document.all))
  {
    if (e.which==2||e.which==3)
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}
//disableRightClick();


//日历控件
 function atCalendarControl(){ 
  var calendar=this; 
  this.calendarPad=null; 
  this.prevMonth=null; 
  this.nextMonth=null; 
  this.prevYear=null; 
  this.nextYear=null; 
  this.goToday=null; 
  this.calendarClose=null; 
  this.calendarAbout=null; 
  this.head=null; 
  this.body=null; 
  this.today=[]; 
  this.currentDate=[]; 
  this.sltDate; 
  this.target; 
  this.source; 

  /************** 加入日历底板及阴影 *********************/ 
  this.addCalendarPad=function(){ 
   document.write("<div id='divCalendarpad' style='position:absolute;top:100;left:0;width:255;height:167;display:none;'>");
   document.write("<iframe frameborder=0 height=168 width=255></iframe>");
   document.write("<div style='position:absolute;top:4;left:4;width:248;height:164;background-color:#336699;'></div>");
   document.write("</div>"); 
   calendar.calendarPad=document.all.divCalendarpad; 
  } 
  /************** 加入日历面板 *********************/ 
  this.addCalendarBoard=function(){ 
   var BOARD=this; 
   var divBoard=document.createElement("div"); 
   calendar.calendarPad.insertAdjacentElement("beforeEnd",divBoard); 
   divBoard.style.cssText="position:absolute;top:0;left:0;width:250;height:166;border:1 outset;background-color:buttonface;"; 

var tbBoard=document.createElement("table"); 
   divBoard.insertAdjacentElement("beforeEnd",tbBoard); 
   tbBoard.style.cssText="position:absolute;top:0;left:0;width:100%;height:10;font-size:9pt;"; 
   tbBoard.cellPadding=0; 
   tbBoard.cellSpacing=1; 
   tbBoard.bgColor="#333333"; 

  /************** 设置各功能按钮的功能 *********************/ 
   /*********** Calendar About Button ***************/ 
   trRow = tbBoard.insertRow(0); 
   calendar.calendarAbout=calendar.insertTbCell(trRow,0,"-","center"); 
   calendar.calendarAbout.onclick=function(){calendar.about();} 
   /*********** Calendar Head ***************/ 
   tbCell=trRow.insertCell(1); 
   tbCell.colSpan=5; 
   tbCell.bgColor="#99CCFF"; 
   tbCell.align="center"; 
   tbCell.style.cssText = "cursor:default"; 
   calendar.head=tbCell; 
   /*********** Calendar Close Button ***************/ 
   tbCell=trRow.insertCell(2); 
   calendar.calendarClose = calendar.insertTbCell(trRow,2,"x","center"); 
   calendar.calendarClose.title="关闭"; 
   calendar.calendarClose.onclick=function(){calendar.hide();} 

   /*********** Calendar PrevYear Button ***************/ 
   trRow = tbBoard.insertRow(1); 
   calendar.prevYear = calendar.insertTbCell(trRow,0,"&lt;&lt;","center"); 
   calendar.prevYear.title="上一年"; 
   calendar.prevYear.onmousedown=function(){ 
    calendar.currentDate[0]--; 
    calendar.show(calendar.target,calendar.currentDate[0]+"-"+calendar.currentDate[1]+"-"+calendar.currentDate[2],calendar.source); 
   } 
   /*********** Calendar PrevMonth Button ***************/ 
   calendar.prevMonth = calendar.insertTbCell(trRow,1,"&lt;","center"); 
   calendar.prevMonth.title="上一月"; 
   calendar.prevMonth.onmousedown=function(){ 

calendar.currentDate[1]--; 
    if(calendar.currentDate[1]==0){ 
     calendar.currentDate[1]=12; 
     calendar.currentDate[0]--; 
    } 
    calendar.show(calendar.target,calendar.currentDate[0]+"-"+calendar.currentDate[1]+"-"+calendar.currentDate[2],calendar.source); 
   } 
   /*********** Calendar Today Button ***************/ 
   calendar.goToday = calendar.insertTbCell(trRow,2,"今天","center",3); 
   calendar.goToday.title="选择今天"; 
   calendar.goToday.onclick=function(){ 
    calendar.sltDate=calendar.currentDate[0]+"-"+calendar.currentDate[1]+"-"+calendar.currentDate[2]; 
    calendar.target.value=calendar.sltDate; 
    calendar.hide(); 
    //calendar.show(calendar.target,calendar.today[0]+"-"+calendar.today[1]+"-"+calendar.today[2],calendar.source); 
   } 
   /*********** Calendar NextMonth Button ***************/ 
   calendar.nextMonth = calendar.insertTbCell(trRow,3,"&gt;","center"); 
   calendar.nextMonth.title="下一"; 
   calendar.nextMonth.onmousedown=function(){ 
    calendar.currentDate[1]++; 
    if(calendar.currentDate[1]==13){ 
     calendar.currentDate[1]=1; 
     calendar.currentDate[0]++; 
    } 
    calendar.show(calendar.target,calendar.currentDate[0]+"-"+calendar.currentDate[1]+"-"+calendar.currentDate[2],calendar.source); 
   } 
   /*********** Calendar NextYear Button ***************/ 
   calendar.nextYear = calendar.insertTbCell(trRow,4,"&gt;&gt;","center"); 
   calendar.nextYear.title="下一年"; 
   calendar.nextYear.onmousedown=function(){ 
    calendar.currentDate[0]++; 
    calendar.show(calendar.target,calendar.currentDate[0]+"-"+calendar.currentDate[1]+"-"+calendar.currentDate[2],calendar.source); 
} 

   trRow = tbBoard.insertRow(2); 
   var cnDateName = new Array("周日","周一","周二","周三","周四","周五","周六"); 
   for (var i = 0; i < 7; i++) { 
    tbCell=trRow.insertCell(i) 
    tbCell.innerText=cnDateName[i]; 
    tbCell.align="center"; 
    tbCell.width=35; 
    tbCell.style.cssText="cursor:default;border:1 solid #99CCCC;background-color:#99CCCC;"; 
   } 

   /*********** Calendar Body ***************/ 
   trRow = tbBoard.insertRow(3); 
   tbCell=trRow.insertCell(0); 
   tbCell.colSpan=7; 
   tbCell.height=97; 
   tbCell.vAlign="top"; 
   tbCell.bgColor="#F0F0F0"; 
   var tbBody=document.createElement("table"); 
   tbCell.insertAdjacentElement("beforeEnd",tbBody); 
   tbBody.style.cssText="position:relative;top:0;left:0;width:245;height:103;font-size:9pt;" 
   tbBody.cellPadding=0; 
   tbBody.cellSpacing=1; 
   calendar.body=tbBody; 
  } 
  /************** 加入功能按钮公共样式 *********************/ 
  this.insertTbCell=function(trRow,cellIndex,TXT,trAlign,tbColSpan){ 
   var tbCell=trRow.insertCell(cellIndex); 
   if(tbColSpan!=undefined) tbCell.colSpan=tbColSpan; 

   var btnCell=document.createElement("button"); 
   tbCell.insertAdjacentElement("beforeEnd",btnCell); 
   btnCell.value=TXT; 
   btnCell.style.cssText="width:100%;border:1 outset;background-color:buttonface;"; 
   btnCell.onmouseover=function(){ 
    btnCell.style.cssText="width:100%;border:1 outset;background-color:#F0F0F0;"; 
 } 
   btnCell.onmouseout=function(){ 
    btnCell.style.cssText="width:100%;border:1 outset;background-color:buttonface;"; 
   } 
  // btnCell.onmousedown=function(){ 
  //  btnCell.style.cssText="width:100%;border:1 inset;background-color:#F0F0F0;"; 
  // } 
   btnCell.onmouseup=function(){ 
    btnCell.style.cssText="width:100%;border:1 outset;background-color:#F0F0F0;"; 
   } 
   btnCell.onclick=function(){ 
    btnCell.blur(); 
   } 
   return btnCell; 
  } 
  this.setDefaultDate=function(){ 
   var dftDate=new Date(); 
   calendar.today[0]=dftDate.getYear(); 
   calendar.today[1]=dftDate.getMonth()+1; 
   calendar.today[2]=dftDate.getDate(); 
  } 

  /****************** Show Calendar *********************/ 
  this.show=function(targetObject,defaultDate,sourceObject){ 
   if(targetObject==undefined) { 
    alert("未设置目标对像. \n方法: ATCALENDAR.show(obj 目标对像,string 默认日期,obj 点击对像);\n\n目标对像:接受日期返回值的对像.\n默认日期:格式为\"yyyy-mm-dd\",缺省为当日日期.\n点击对像:点击这个对像弹出calendar,默认为目标对像.\n"); 
    return false; 
   } 
   else    calendar.target=targetObject; 
   if(sourceObject==undefined) calendar.source=calendar.target; 
   else calendar.source=sourceObject; 

   var firstDay; 
   var Cells=new Array(); 
 if(defaultDate==undefined || defaultDate==""){ 
    var theDate=new Array(); 
    calendar.head.innerText = calendar.today[0]+"-"+calendar.today[1]+"-"+calendar.today[2]; 
    theDate[0]=calendar.today[0]; theDate[1]=calendar.today[1]; theDate[2]=calendar.today[2]; 
   } 
   else{ 
    var reg=/^\d{4}-\d{1,2}-\d{1,2}$/ 
    if(!defaultDate.match(reg)){ 
     alert("默认日期的格式不正确\n\n默认日期可接受格式为:'yyyy-mm-dd'"); 
     return; 
    } 
    var theDate=defaultDate.split("-"); 
    calendar.head.innerText = defaultDate; 
   } 
   calendar.currentDate[0]=theDate[0]; 
   calendar.currentDate[1]=theDate[1]; 
   calendar.currentDate[2]=theDate[2]; 
   theFirstDay=calendar.getFirstDay(theDate[0],theDate[1]); 
   theMonthLen=theFirstDay+calendar.getMonthLen(theDate[0],theDate[1]); 
   //calendar.setEventKey(); 

   calendar.calendarPad.style.display=""; 
   var theRows = Math.ceil((theMonthLen)/7); 
   //清除旧的日历; 
   while (calendar.body.rows.length > 0) { 
    calendar.body.deleteRow(0) 
   } 
   //建立新的日历; 
   var n=0;day=0; 
   for(i=0;i<theRows;i++){
    theRow=calendar.body.insertRow(i);
    for(j=0;j<7;j++){
     n++;
     if(n>theFirstDay && n<=theMonthLen){ 
      day=n-theFirstDay; 
      calendar.insertBodyCell(theRow,j,day); 
     } 

else{ 
      var theCell=theRow.insertCell(j); 
      theCell.style.cssText="background-color:#F0F0F0;cursor:default;"; 
     } 
    } 
   } 

   //****************调整日历位置**************// 
   var offsetPos=calendar.getAbsolutePos(calendar.source);//计算对像的位置; 
   if((document.body.offsetHeight-(offsetPos.y+calendar.source.offsetHeight-document.body.scrollTop))
       <calendar.calendarPad.style.pixelHeight)
        {
			var calTop=offsetPos.y-calendar.calendarPad.style.pixelHeight;
		}
   else
		{
			var calTop=offsetPos.y+calendar.source.offsetHeight;
		}
   if((document.body.offsetWidth-(offsetPos.x+calendar.source.offsetWidth-document.body.scrollLeft))
		>calendar.calendarPad.style.pixelWidth)
		{ 
			var calLeft=offsetPos.x; 
		} 
   else
		{ 
			var calLeft=offsetPos.x-calendar.calendarPad.style.pixelWidth+calendar.source.offsetHeight; 
		} 
   //alert(offsetPos.x); 
   calendar.calendarPad.style.pixelLeft=calLeft; 
   calendar.calendarPad.style.pixelTop=calTop; 
  } 
  /****************** 计算对像的位置 *************************/ 
  this.getAbsolutePos = function(el) { 
   var r = { x: el.offsetLeft, y: el.offsetTop }; 
   if (el.offsetParent) { 
    var tmp = calendar.getAbsolutePos(el.offsetParent); 
    r.x += tmp.x; 
    r.y += tmp.y; 
   } 
   return r; 
  }; 

//************* 插入日期单元格 **************/ 
  this.insertBodyCell=function(theRow,j,day,targetObject){ 
   var theCell=theRow.insertCell(j); 
   if(j==0) var theBgColor="#FF9999"; 
   else var theBgColor="#FFFFFF"; 
   if(day==calendar.currentDate[2]) var theBgColor="#CCCCCC"; 
   if(day==calendar.today[2]) var theBgColor="#99FFCC"; 
   theCell.bgColor=theBgColor; 
   theCell.innerText=day; 
   theCell.align="center"; 
   theCell.width=35; 
   theCell.style.cssText="border:1 solid #CCCCCC;cursor:hand;"; 
   theCell.onmouseover=function(){  
    theCell.bgColor="#FFFFCC";  
    theCell.style.cssText="border:1 outset;cursor:hand;"; 
   } 
   theCell.onmouseout=function(){  
    theCell.bgColor=theBgColor;  
    theCell.style.cssText="border:1 solid #CCCCCC;cursor:hand;"; 
   } 
   theCell.onmousedown=function(){  
    theCell.bgColor="#FFFFCC";  
    theCell.style.cssText="border:1 inset;cursor:hand;"; 
   } 
   theCell.onclick=function(){ 
    if(calendar.currentDate[1].length<2) calendar.currentDate[1]="0"+calendar.currentDate[1]; 
    if(day.toString().length<2) day="0"+day; 
    calendar.sltDate=calendar.currentDate[0]+"-"+calendar.currentDate[1]+"-"+day; 
    calendar.target.value=calendar.sltDate; 
    calendar.hide(); 
   } 
  } 
  /************** 取得月份的第一天为星期几 *********************/ 
  this.getFirstDay=function(theYear, theMonth){ 
   var firstDate = new Date(theYear,theMonth-1,1); 
   return firstDate.getDay(); 
  } 
  /************** 取得月份共有几天 *********************/ 

this.getMonthLen=function(theYear, theMonth) { 
   theMonth--; 
   var oneDay = 1000 * 60 * 60 * 24; 
   var thisMonth = new Date(theYear, theMonth, 1); 
   var nextMonth = new Date(theYear, theMonth + 1, 1); 
   var len = Math.ceil((nextMonth.getTime() - thisMonth.getTime())/oneDay); 
   return len; 
  } 
  /************** 隐藏日历 *********************/ 
  this.hide=function(){ 
   //calendar.clearEventKey(); 
   calendar.calendarPad.style.display="none"; 
  } 
  /************** 从这里开始 *********************/ 
  this.setup=function(defaultDate){ 
   calendar.addCalendarPad(); 
   calendar.addCalendarBoard(); 
   calendar.setDefaultDate(); 
  } 
  /************** 关于AgetimeCalendar *********************/ 
  this.about=function(){ 
   /*//alert("Agetime Calendar V1.0\n\nwww.agetime.com\n"); 
   popLeft = calendar.calendarPad.style.pixelLeft+4; 
   popTop = calendar.calendarPad.style.pixelTop+25; 
   var popup = window.createPopup(); 
   var popupBody = popup.document.body; 
   popupBody.style.cssText="border:solid 2 outset;font-size:9pt;background-color:#F0F0F0;"; 
   var popHtml = "<span style='color:#336699;font-size:12pt;'><U>关于 AgetimeCalendar</U></span><BR><BR>"; 
   //popHtml+="版本: v1.0<BR>日期: 2004-03-13"; 
   popupBody.innerHTML=popHtml; 
   popup.show(popLeft,popTop,240,136,document.body); */ 
   var strAbout = "About AgetimeCalendar\n\n"; 
   strAbout+="-\t: 关于\n"; 
   strAbout+="x\t: 隐藏\n"; 
   strAbout+="<<\t: 上一年\n"; 
   strAbout+="<\t: 上一月\n"; 
strAbout+="今日\t: 返回当天日期\n"; 
   strAbout+=">\t: 下一月\n"; 
   strAbout+="<<\t: 下一年\n"; 
   alert(strAbout); 
  } 

  calendar.setup(); 
 } 

var CalendarWebControl = new atCalendarControl();
function createMenu(){}

function CheckSFZHM(bb)
{
	var num=0;
	var aa=bb.value.trim();
	if(aa=="") return true;
	for(var i=0; i<aa.length;i++)
	{
		if(aa.charCodeAt(i)>255)
		{
			num=num+2;
		}
		else
		{
			num=num+1;
		}
	}
	if(num!=15 && num!=18)
	{
		alert("身份证证号码不正确，应为15位或18位！")
		bb.focus();
		return false;
	}
	else
	{
		return true;
	}
}

function B1IsValid()
{
	var managertotal=0;//项目经理总数
	var firstmanager=0;//一级项目经理人数
	var secondmanager=0;//二级项目经理人数
	var thirdmanager=0;//三级项目经理人数
	//检查项目经理人数是否合法
	if(document.forms[0].FManagerTotal.value.trim()!="")
	{
		managertotal=parseInt(document.forms[0].FManagerTotal.value.valueOf());
	}
	if(document.forms[0].FFirstManagerSum.value.trim()!="")
	{
		firstmanager=parseInt(document.forms[0].FFirstManagerSum.value.valueOf());
	}
	if(document.forms[0].FSecondManagerSum.value.trim()!="")
	{
		secondmanager=parseInt(document.forms[0].FSecondManagerSum.value.valueOf());
	}
	if(document.forms[0].FThirdManagerSum.value.trim()!="")
	{
		thirdmanager=parseInt(document.forms[0].FThirdManagerSum.value.valueOf());
	}
	if(managertotal<firstmanager)
	{
		alert('一级项目经理人数不能多于项目经理总数');
		return false;
	}
	else if(managertotal<secondmanager)
	{
		alert('二级项目经理人数不能多于项目经理总数');
		return false;
	}
	else if(managertotal<thirdmanager)
	{
		alert('三级项目经理人数不能多于项目经理总数');
		return false;
	}
	else if(managertotal<(firstmanager+secondmanager+thirdmanager))
	{
		alert('一二三级项目经理总人数不能多于项目经理总数');
		return false;
	}
	//检验持证上岗人员
	if(parseInt(document.forms[0].FConstructSum.value)<parseInt(document.forms[0].FConstructC.value))
	{
		alert('持证上岗施工员人数不能多于施工员总数');
		return false;
	}
		if(parseInt(document.forms[0].FQualitySum.value)<parseInt(document.forms[0].FQualityC.value))
	{
		alert('持证上岗质检员人数不能多于质检员总数');
		return false;
	}
		if(parseInt(document.forms[0].FBudgetSum.value)<parseInt(document.forms[0].FBudgetC.value))
	{
		alert('持证上岗预算员人数不能多于预算员总数');
		return false;
	}
		if(parseInt(document.forms[0].FSafetySum.value)<parseInt(document.forms[0].FSafetyC.value))
	{
		alert('持证上岗安全员人数不能多于安全员总数');
		return false;
	}
	return true;
}

function B2IsValid()
{
	//检验净利润
	var profit=0;
	var tax=0;
	var net=0;
	if(document.forms[0].FProfitTotal.value.trim()!="")
	{
	profit=document.forms[0].FProfitTotal.value;
	}
	if(document.forms[0].FIncomeTax.value.trim()!="")
	{
	tax=document.forms[0].FIncomeTax.value;
	}
	if(document.forms[0].FNetProfit.value.trim()!="")
	{
	net=document.forms[0].FNetProfit.value;
	}
	if((parseFloat(profit)-parseFloat(tax))!=parseFloat(net))
	{
		alert(profit+','+tax+','+net+'利润总额、所得税、净利润的填写有误');
		return false;
	}
	return true;
}
function B3IsValid(s)
{
  ////////////口令验证
  var vpwd="";
  var vspwd="";
  if(document.forms[0].Tpwdvalid.value.trim()!="")
  {
      vspwd=document.forms[0].Tpwdvalid.value.trim();
      vpwd=document.forms[0].Tbpwd.value.trim();
      if(vspwd!=vpwd)
      {
         alert('验证口令输入错误,请重新输入');
         s.value="";
         document.forms[0].Tpwdvalid.value="";
         s.focus();
          return;
      }
  }
}
function B4IsValid(s)
{
  ////////////二次口令验证
  var vpwd="";
  var vspwd="";
  if(document.forms[0].Tbpwd.value.trim()!="")
  {
      vspwd=document.forms[0].Tpwdvalid.value.trim();
      vpwd=document.forms[0].Tbpwd.value.trim();
      if(vspwd!=vpwd)
      {
         alert('输入的口令与验证口令不同,请重新输入');
         s.value="";
         document.forms[0].Tbpwd.value="";
          s.focus();
          return;
      }
  }
}

function sum(s1,s2,s3)
{
   if (s1.value == "") return true;
   var chk=parseFloat(s1.value);
   if(chk!=s1.value)
     {
	  alert("该字段应是整数或小数！");
      s1.focus();
      return false;
      }
	var price=0;
	var count=0;
	var sum=0;
	if(s2.value.trim()!="")
	{
		count=parseInt(s2.value.valueOf());
	}
	else
	{
	      alert('进货数量没有填写,请先填写进货数量后在进行单价的填写,系统可以自动计算出总货款');
	      s2.focus();
          return;
	}
	if(s1.value.trim()!="")
	{
		price=parseFloat(s1.value.valueOf());
	}
	if(count>0)
	{
			var sum=count*price;
			s3.value=sum;
	}
}
function datasum(s)
{
    if (s.value == "") return true;
   var chk=parseFloat(document.forms[0].Tbprice.value);
   if(chk!=document.forms[0].Tbprice.value)
     {
	  alert("该字段应是整数或小数！");
      document.forms[0].Tbprice.focus();
      return false;
      }
	var price=0;
	var count=parseInt(s.value.valueOf());
	var sum=0;
	sum=parseInt(document.forms[0].Tbsum.value.valueOf());
	if(count>sum)
	alert("借出数量已经大于库存数量，请重新填写借出数量！");
	s.values="";
	s.focus();
	return;
}
function judenull(s)
{
   if(s.value == "") 
   {
    // altert("有重要数据没有填写，请检查");
      s.focus();
      return;
   }
}
			

