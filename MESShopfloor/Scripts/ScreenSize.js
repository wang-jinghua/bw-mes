﻿var screenW = 640, screenH = 480;
var screenWidth = 1024, screenHeight = 678;

function getUpdatedScreenSize() {
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        screenWidth = window.innerWidth;
        screenHeight = window.innerHeight;
    }
    else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        screenWidth = document.documentElement.clientWidth;
        screenHeight = document.documentElement.clientHeight;
    }
    else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        screenWidth = document.body.clientWidth;
        screenHeight = document.body.clientHeight;
    }
    //window.alert( 'Width XXXX= ' + screenWidth );
    //window.alert( 'Height XXXX= ' + screenHeight );	
}

//window.onresize= onWindowResizing();

function onWindowResizing() {
    getUpdatedScreenSize();
    //alert("The window has been resized!");
    setScreenSize();
}

function setScreenSize() {
    xmlHttp = GetXmlHttpObject()
    if (xmlHttp == null) {
        alert("Browser does not support HTTP Request")
        return
    }

    var url = "/ScreenDetails.do?ScreenWidth=" + screenWidth + "&ScreenHeight=" + screenHeight;
    url = url + "&sid=" + Math.random()
    //alert(" THE URL TO BE SENT IS >>>>>>>>>>>>>>>>>>  "+ url);
    xmlHttp.onreadystatechange = processReqChange
    xmlHttp.open("GET", url, true)
    xmlHttp.send(null)
} 
