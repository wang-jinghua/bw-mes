﻿//扩展表单获取值的方法为json对象
$.fn.parseForm = function () {
    var serializeObj = {};
    var array = this.serializeArray();
    var str = this.serialize();
    $(array).each(function () {
        if (serializeObj[this.name]) {
            if ($.isArray(serializeObj[this.name])) {
                serializeObj[this.name].push(this.value);
            } else {
                serializeObj[this.name] = [serializeObj[this.name], this.value];
            }
        } else {
            serializeObj[this.name] = this.value;
        }
    });
    return serializeObj;
};
//提示消息
function DisplayMessage(istrue, msg) {
    try {
        top.ShowMessage(msg, istrue);
    } catch (err) {
        if (istrue) {
            $.messager.show({
                title: '',
                width: '800',
                height: '50',
                msg: msg,
                timeout: 1000,
                showType: 'slide',
                style: {
                }
            });
        } else {
            $.messager.alert('错误', msg, 'error');
        }
    }
}
//判断字符串是否全部由空格或换行符组成
function isNullOrWhiteSpaceStr(data) {
    var reg = /^\s*$/;
    //返回值为true表示是空字符串
    return !(data != null && data != undefined && !reg.test(data));
}
//判断某个json数组里是否含有某个值，指定某个属性
function judgeValueOfJsons(jsons, field, val) {
    var re = false;
    $.each(jsons, function (i, v) {
        //if (val == "2021-02-18T00:00:00")
        //{
        //    prompt("", v[field] + " " + val);
        //    alert(v[field] == val);
        //}
        if (v[field] == val) {
            re = true;
            return true;
        }
    });
    return re;
}