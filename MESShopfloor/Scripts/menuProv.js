// ---------------------------------------------------------------------------
// Example of howto: use OutlookBar
// ---------------------------------------------------------------------------

//create OutlookBar
var o = new createOutlookBar('Bar',0,0,screenSize.width-17,screenSize.height,'#eeeeee','white','blue','red','#99ccff','black');//'#000099') // OutlookBar
var p;

//create first panel
p = new createPanel('a0','下属企业查询');
p.addButton('../images/gov/ico_query.gif','企业查询','parent.mainfrm.location="EnterpriseQuery/EntQuery.aspx"');	
o.addPanel(p);

p = new createPanel('a1','安全生产许可证');
p.addButton('../images/gov/ico_censor.gif','业务审批','parent.mainfrm.location="Certificate/CertQuery.aspx"');
p.addButton('../images/gov/ico_certificate.gif','证书生成','parent.mainfrm.location="Certificate/CertificateQuery.aspx"');		
p.addButton('../images/gov/ico_query.gif','业务查询','parent.mainfrm.location="Certificate/Query/CertQuery.aspx"');	
o.addPanel(p);
	
//p = new createPanel('a2','三类人员证书考核');
//p.addButton('../images/gov/ico_sflevel.gif','安全评级','parent.mainfrm.location="Assessment/EnterpriseQuery.aspx"');	
//o.addPanel(p);
	
p = new createPanel('a3','工程报监业务');
p.addButton('../images/gov/ico_censor.gif','报监审批','parent.mainfrm.location="ProjectWatch/WatchQuery.aspx"');
p.addButton('../images/gov/ico_assess.gif','工程验收','parent.mainfrm.location="ProjectWatch/Assess/ProQuery.aspx"');
p.addButton('../images/gov/ico_query.gif','业务查询','parent.mainfrm.location="ProjectWatch/Query/WatchQuery.aspx"');
o.addPanel(p);

p = new createPanel('a4','文明工地评选');
p.addButton('../images/gov/ico_censor.gif','评选审批','parent.mainfrm.location="CiviSpot/SpotQuery.aspx"');
o.addPanel(p);
	
p = new createPanel('a5','事故处理业务');
p.addButton('../images/gov/ico_censor.gif','事故审批','parent.mainfrm.location="Accident/AccidentQuery.aspx"');
p.addButton('../images/gov/ico_query.gif','事故查询','parent.mainfrm.location="Accident/Query/AccidentQuery.aspx"');	
o.addPanel(p); 

p = new createPanel('a6','企业安全评级');
p.addButton('../images/gov/ico_sflevel.gif','安全评级','parent.mainfrm.location="Assessment/EnterpriseQuery.aspx"');	
o.addPanel(p);

p = new createPanel('a7','管理部门安全评估');
p.addButton('../images/gov/ico_mnglevel.gif','管理评估','parent.mainfrm.location="GovAssess/DeptQuery.aspx"');	
o.addPanel(p);

//draw the OutlookBar
o.draw();  
	

