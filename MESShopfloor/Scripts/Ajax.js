﻿//所有留言的列表
function getallmsg() {
    $("loadingflag").innerHTML = "正在加载留言信息.........";
    Request.sendGET
('ajax.aspx?type=0&rnd=' + Math.random(1000), function getallok(req) { $("allmsg").innerHTML = req.responseText; }, null, true, callback2);
}
//查看某条留言的详细信息
function myget(id) {
    Request.sendGET
('ajax.aspx?type=1&id=' + id, callback, null, true, callback2);
}
//发出要求回复留言的请求，显示留言回复界面
function reply(id, title) {
    Request.sendGET
('ajax.aspx?type=6&id=' + id + '&rnd=' + Math.random(1000) + '&title=' + escape(title), callback, null, true, callback2);
}
//提交回复留言的数据
function rq() {
    var a = $('tbAuthor0').value;
    var b = $('tbtitle').value;
    var c = $('tbcontent').value;
    var d = $('Hidden1').value;
    var data = 'author=' + escape(a) + '&title=' + escape(b) + '&content=' + escape(c) + '&id=' + d;
    Request.sendGET('ajax.aspx?type=7&' + data, callback3, null, false, callback2);
}
//发出要求添加留言的请求，显示留言添加界面
function add() {
    Request.sendGET
('ajax.aspx?type=4', callback, null, true, callback2);
}
//提交添加的留言的数据
function whypost() {
    if ($("tbTitle") == "" || $("tbContent").value == "") {
        alert("留言标题和流言内容均不得为空。");
        return;
    }
    var whydata = 'type=5&author=' + escape(document.getElementById('tbauthor').value) + '&title=' + escape(document.getElementById('tbtitle').value) + '&content=' + escape(document.getElementById('tbcontent').value);
    Request.sendPOST("ajax.aspx", whydata, callback3, true, callback2)
}
//要求删除留言，返回要求输入删除密码的界面
function del(id) {
    Request.sendGET
('ajax.aspx?type=2&del=1&id=' + id, callback, null, true, callback2);
}
//删除留言
function del2(id) {
    Request.sendGET
('ajax.aspx?type=3&id=' + id + '&rnd=' + Math.random(1000) + "&pass=" + $("delpass").value, delok, null, true, callback2);
}
function delok(req) {
    req.responseText == "" ? $("loading").innerHTML = '操作成功。' : $("loading").innerHTML = req.responseText;
    getallmsg();
}
//服务器端操作后的回调函数
//服务器端返回界面代码时，负责显示这些代码
function callback(req, data) {
    $("loading").innerHTML = req.responseText;
}
//成功执行回复或添加留言后，显示提示，重新加载留言列表
function callback3(req, data) {
    $("loading").innerHTML = '操作成功。'; getallmsg();
}
//服务器端返回错误时，显示提示
function callback2(req, data) {
    $("loading").innerHTML = "出现错误，请重试！";
}

