/**
* @author vinodvv
*/
//打开导出来的数据
function exportExcel(fileName) {
    window.open("TempeFiles/" + fileName);
}
//*******Start 帮助框*******//
//打开帮助框
function showHelpDiv(imgobj, divId) {
    var divobj = document.getElementById(divId);
    var x = imgobj.offsetLeft + imgobj.offsetWidth - divobj.style.pixelWidth;
    var y = imgobj.offsetTop + imgobj.offsetHeight;
    var parent = imgobj;
    while (parent.offsetParent) {
        parent = parent.offsetParent;
        x += parent.offsetLeft;
        y += parent.offsetTop;
    }
    divobj.style.left = x + "px";
    divobj.style.top = y + "px";
    divobj.style.display = "inline";


}
//关闭帮助框
function hiddenHelpDiv(divId) {
    var divobj = document.getElementById(divId);
    divobj.style.display = 'none';
}
//*******End 帮助框*******//
function openURLinNewWindow(url, wdt, htt) {
    win_name = "";
    win_props = "width=" + wdt + ",height=" + htt + ",left=50,top=50,scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
    custwindow = window.open(url, win_name, win_props);
    custwindow.opener = window;

    navapp = navigator.appVersion;
    if (navapp.indexOf("MSIE") == -1) {
        custwindow.focus();
    }
}


function openAlarmInfoNewWindow(stime, etime, matoname,matolid,periodtype,alarmcount)
{
    //var url="MachineDetail/AlarmDetail.aspx?stime=" +stime +"&etime=" + etime + "&matoname=" +matoname +"&matolid=" + matolid + "&periodtype=" + periodType + "&alarmcount="+alarmcount; 
    var url="MachineDetail/AlarmDetail.aspx";//?stime=" +stime +"&etime=" + etime + "&matoname=" +matoname +"&matolid=" + matolid + "&periodtype=" + periodType + "&alarmcount="+alarmcount; 
    win_name = "";
    win_props = "width=800,height=630,left=50,top=50,scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no";
    custwindow = window.open(url, win_name, win_props);
    custwindow.opener = window;
    navapp = navigator.appVersion;
    if (navapp.indexOf("MSIE") == -1) {
        custwindow.focus();
    }
}


function openMaxNewWindow(url) {
    win_name = "";
    var wdt = screen.availWidth;
    var htt = screen.availHeight;
    win_props = "width=" + wdt + ",height=" + htt + ",top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes";
    custwindow = window.open(url, win_name, win_props);
    custwindow.opener = window;

    navapp = navigator.appVersion;
    if (navapp.indexOf("MSIE") == -1) {
        custwindow.focus();
    }
}

function openMaxNewWindowWithScroll(url) {
    win_name = "";
    var wdt = screen.availWidth;
    var htt = screen.availHeight - 10;
    win_props = "width=" + wdt + ",height=" + htt + ",top=0,left=0,scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes";
    custwindow = window.open(url, win_name, win_props);
    custwindow.opener = window;

    navapp = navigator.appVersion;
    if (navapp.indexOf("MSIE") == -1) {
        custwindow.focus();
    }
}

//打开幻灯滚动显示
function openLanternSlide() {
    var wdt = screen.availWidth;
    var htt = screen.availHeight;
    var welcomeinfo = document.getElementById("txtWelcomeInfo").value;
    var speed = document.getElementById("txtSpeed").value;
    var machineId = document.getElementById("selectedMachineID").value;
    var periodType = document.getElementById("HiddenPeriodType").value;
    if (machineId == "" || periodType == "") {
//        alert("请选择机床和时间范围");
    }
    else {
        var url = "LanternSlide/LanternShow.aspx?speed=" + speed + "&machineid=" + machineId + "&periodType=" + periodType + "&welcome=" + escape(welcomeinfo);
        var win_props = "width=" + wdt + ",height=" + htt + ",fullscreen=1,top=0,left=0,scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes";
        var custwindow = window.open(url, "", win_props);
        custwindow.opener = window;

        navapp = navigator.appVersion;
        if (navapp.indexOf("MSIE") == -1) {
            custwindow.focus();
        }
    }
}

function hoverbg(val) {
    for (i = 1; i <= 2; i++) {
        document.getElementById("A" + i).className = 'UnselectedMonitorBG';
        document.getElementById("B" + i).className = 'UnselectedMonitorInner';
        document.getElementById("C" + i).className = 'UnselectedMonitorMiddle';
        document.getElementById("D" + i).className = 'UnselectedMonitorMiddle';
        document.getElementById("E" + i).className = 'UnselectedMonitor';
    }
    document.getElementById("A" + val).className = 'selectedMonitorBG';
    document.getElementById("B" + val).className = 'selectedMonitorInner';
    document.getElementById("C" + val).className = 'selectedMonitorMiddle';
    document.getElementById("D" + val).className = 'selectedMonitorMiddle';
    document.getElementById("E" + val).className = 'selectedMonitor';
}

function tabs(val, stat) {
    for (i = 1; i <= 4; i++) {
        document.getElementById("ma" + i).className = 'xtop';
        document.getElementById("mb" + i).className = 'xb1';
        document.getElementById("mc" + i).className = 'xb2';
        document.getElementById("md" + i).className = 'xb3';
        document.getElementById("me" + i).className = 'xb4';
        document.getElementById("mf" + i).className = 'menuboxcontent';
        //document.getElementById("mg"+i).className = stat;

    }
    document.getElementById("ma" + val).className = 'sxtop';
    document.getElementById("mb" + val).className = 'sxb1';
    document.getElementById("mc" + val).className = 'sxb2';
    document.getElementById("md" + val).className = 'sxb3';
    document.getElementById("me" + val).className = 'sxb4';
    document.getElementById("mf" + val).className = 'smenuboxcontent';
    //document.getElementById("mg"+val).className = 'UnselectedMonitorBG';
}

function ADtabs(val, stat) {
    for (i = 1; i <= 3; i++) {
        document.getElementById("ADma" + i).className = 'xtop';
        document.getElementById("ADmb" + i).className = 'xb1';
        document.getElementById("ADmc" + i).className = 'xb2';
        document.getElementById("ADmd" + i).className = 'xb3';
        document.getElementById("ADme" + i).className = 'xb4';
        document.getElementById("ADmf" + i).className = 'menuboxcontent';
        //document.getElementById("ADmg"+i).className = stat;

    }
    document.getElementById("ADma" + val).className = 'sxtop';
    document.getElementById("ADmb" + val).className = 'sxb1';
    document.getElementById("ADmc" + val).className = 'sxb2';
    document.getElementById("ADmd" + val).className = 'sxb3';
    document.getElementById("ADme" + val).className = 'sxb4';
    document.getElementById("ADmf" + val).className = 'smenuboxcontent';
    //document.getElementById("ADmg"+val).className = 'UnselectedMonitorBG';
}


function hoverbgnew(val, status) {
    for (i = 1; i <= 2; i++) {
        document.getElementById("AA" + i).className = 'UnselectedMonitorBG';
        document.getElementById("BB" + i).className = 'UnselectedMonitorInner';
        document.getElementById("CC" + i).className = 'UnselectedMonitorMiddle';
        document.getElementById("DD" + i).className = 'UnselectedMonitorMiddle';
        document.getElementById("EE" + i).className = 'UnselectedMonitor';
    }
    document.getElementById("AA" + val).className = 'selectedMonitorBG';
    document.getElementById("BB" + val).className = 'selectedMonitorInner';
    document.getElementById("CC" + val).className = 'selectedMonitorMiddle';
    document.getElementById("DD" + val).className = 'selectedMonitorMiddle';
    document.getElementById("EE" + val).className = 'selectedMonitor';
}

function menuHover(whichMenu) {

    for (k = 1; k <= 6; k++) {
        var txtObj = document.getElementById('M' + k);
        var txtStyle = txtObj.className = 'navigation_td';
    }

    var txtObj = document.getElementById('M' + currentMenu);
    var txtStyle = txtObj.className = 'navigation_td_active';
}

function setCurrentMenu(val) {
    currentMenu = val;
    var txtObj = document.getElementById('M' + currentMenu);
    var txtStyle = txtObj.className = 'navigation_td_active';
}

function ChangeTabStyleNow2(selTab) {
    var tabTable = document.getElementById("TabsTable");
    var aRows = tabTable.rows;
    var aCells = aRows[0].cells;
    var showTab = selTab + "Tab";
    for (i = 0; i < aCells.length; i++) {
        if (aCells[i].id == showTab) {
            aCells[i].className = "tabMenu_selected_Bg";
        }
        else {
            aCells[i].className = "tabMenu_unselected_Bg";
        }
    }
    previousSelIs = selTab;
}

function showCalendarOption(tab) {
    var ipslaHomeTab = document.getElementById("ipslaHome");
    if (ipslaHomeTab == null) {
        return;
    }

    var ipslaHome = ipslaHomeTab.style.display;
    if (ipslaHome == "block" && tab == "Network") {
        return;
    }
    var menu1 = document.getElementById("calendarOptions");
    if (menu1 == null) {
        return;
    }
    if (menu1.style.display == "none") {
        menu1.style.display = 'block';
    }
}

function LoadSelectedTabView(column, leftcontent, machineId, periodType,procedureid,deptid) {
    //SetTabStyle(column, leftcontent);
    //    if (column == "Network") {
    //        loadCookieData();
    //    }
    //    else {
    //        reloadSelectedTab(machineId, periodType, column,'');
    //    }
    reloadSelectedTab(machineId, periodType, column, '',procedureid,deptid);
    //createCookie("ApplicationSelectedTab", column, 30);
}

function LoadSelectedPage(column, leftcontent) {
    //SetTabStyle(column, leftcontent);
    SetSelectedView(column + "Page");
    showCalendarOption(column);
    //createCookie('default' + leftcontent, column, 30);
}

function SetSelectedView(column) {
    var divList = document.getElementsByTagName('DIV');
    var lengthh = divList.length;
    for (i = 0; i < lengthh; i++) {
        masterDiv = divList.item(i);
        pageId = masterDiv.id;
        if (pageId.indexOf("Page") != -1) {
            if (pageId == column) {
                eval("masterDiv.style.display = 'block';");
            }
            else {
                eval("masterDiv.style.display = 'none';");
            }

            if (pageId == "NetworkPage" && pageId == column) {
                var ipslaHomeTab = document.getElementById("ipslaHome");
                if (ipslaHomeTab != null) {
                    var wanMonitorHome = ipslaHomeTab.style.display;
                    if (wanMonitorHome == "block") {
                        hideElement("calendarOptions");
                    }
                }
            }
        }
    }
}

function SetTabStyle(column, leftcontent) {

    var masterTable = document.getElementById(leftcontent);
    tableList = masterTable.getElementsByTagName('TABLE');

    var lengthh = tableList.length;
    //alert('The table length '+ lengthh );
    for (i = 0; i < lengthh; i++) {

        oTable = tableList.item(i);

        // Retrieve the rows collection for the table.
        var aRows = oTable.rows;
        //alert('The table rows length '+ aRows.length);

        // Retrieve the cells collection for the first row.
        var aCells = aRows[0].cells;
        //alert('The table cells length '+ aCells.length);

        //alert('The table name '+ oTable.id);
        var columnTab = column + "Tab";
        var anchorTag = document.getElementById("A_" + oTable.id);
        //alert('THE ANCHORTAG >>>>>> '+ anchorTag.className);

        if (oTable.id == columnTab) {
            //alert('The selected table name '+ oTable.id);
            aCells[0].className = "tbSelected_Left";
            aCells[1].className = "tbSelected_Middle";
            aCells[2].className = "tbSelected_Right";
            anchorTag.className = "tabLinkActive";
        }
        else {
            aCells[0].className = "tbUnselected_Left";
            aCells[1].className = "tbUnselected_Middle";
            aCells[2].className = "tbUnselected_Right";
            anchorTag.className = "tabLink";
        }
    }
}

function SetBGTabStyle(column, leftcontent) {

    var masterTable = document.getElementById(leftcontent);
    tableList = masterTable.getElementsByTagName('TABLE');

    var lengthh = tableList.length;
    //alert('The TABLE LENGTH SETBGTABSTYLE '+ lengthh );
    for (i = 0; i < lengthh; i++) {

        oTable = tableList.item(i);

        // Retrieve the rows collection for the table.
        var aRows = oTable.rows;
        //alert('The table rows length '+ aRows.length);

        // Retrieve the cells collection for the first row.
        var aCells = aRows[0].cells;
        //alert('The table cells length '+ aCells.length);

        //alert('The table name '+ oTable.id);
        var columnTab = column + "Tab";
        var anchorTag = document.getElementById("A_" + oTable.id);
        //alert('THE ANCHORTAG >>>>>> '+ anchorTag.className);

        if (oTable.id == columnTab) {
            //alert('The selected table name '+ oTable.id);
            aCells[0].className = "tbSelectedMon_Left";
            aCells[1].className = "tbSelectedMon_Middle";
            aCells[2].className = "tbSelectedMon_Right";
            anchorTag.className = "tabLinkActive";
        }
        else {
            aCells[0].className = "tbUnselected_Left";
            aCells[1].className = "tbUnselected_Middle";
            aCells[2].className = "tbUnselected_Right";
            anchorTag.className = "tabLink";
        }
    }
}
function formatUrl(formEle) {
    var strSubmit = '';
    var strLastElemName = '';
    strSubmit += 'ipslaMonitor=';
    for (i = 0; i < formEle.elements.length; i++) {
        formElem = formEle.elements[i];
        switch (formElem.type) {
            case 'checkbox':
                if (formElem.name == "childNode" && formElem.checked) {
                    strSubmit += escape(formElem.value) + ','
                }
                break;
        }
    }
    return strSubmit;
}
function showHide(show, hide) {
    var opt1 = document.getElementById(show);
    var opt2 = document.getElementById(hide);
    opt1.style.display = 'block';
    opt2.style.display = 'none';
}
function toggleMenu(toggle, option) {
    var menu1 = document.getElementById(toggle);
    if (menu1 == null) {
        return;
    }
    if (option == "none") {
        menu1.style.display = 'none';
    }
    else {
        menu1.style.display = '';
    }
}
function showIpsla() {
    var divEle = document.getElementById('ipslaHome');
    eval("divEle.style.display = 'block';");
    divEle = document.getElementById('toggleNetwork');
    eval("divEle.style.display = 'none';");
}

function showElement(option) {
    var id = document.getElementById(option);
    if (id != null) {
        eval("id.style.display = 'block'");
    }
}
function hideElement(option) {
    var id = document.getElementById(option);
    if (id != null) {
        eval("id.style.display = 'none'");
    }
}

function setCookie(c_name,value,expiredays)
{
    var exdate=new Date()
    exdate.setDate(exdate.getDate()+expiredays)
    document.cookie.length=9999;
    document.cookie=c_name+ "=" +escape(value)+
    ((expiredays==null) ? "" : ";expires="+exdate.toGMTString())
}


function setprocedureCookie(c_name1,value1,expiredays1)//changed by zhaolei
{
    var exdate1=new Date()
    exdate1.setDate(exdate1.getDate()+expiredays1)
    document.cookie.length=9999;
    document.cookie=c_name1+ "=" +escape(value1)+
    ((expiredays1==null) ? "" : ";expires1="+exdate1.toGMTString())
}

function getCookie(c_name)
{
if(document.cookie.length>0)
{
    c_start=document.cookie.indexOf(c_name + "=")
    if(c_start!=-1){ 
       c_start=c_start + c_name.length+1 
       c_end=document.cookie.indexOf(";",c_start)
       if(c_end==-1) c_end=document.cookie.length
       return unescape(document.cookie.substring(c_start,c_end))
    }
}
return ""
}



//statType参数在选中第二个选项卡时才有用
function reloadSelectedTab(machineId, period, selTab, statType,procedureid,deptid) {
    var xhttp = GetXmlHttpObject();
    var overviewType = "";     

    setprocedureCookie("procedureid",procedureid,999);
    //alert(getCookie("procedureid"));
   
    setCookie("machineId",machineId,888);
    //+"&procedureid="+procedureid        &machineId=" + machineId + "   
    var url = "Communication/Handler.ashx?requestType=AJAX&period=" + period + "&requestId=" + selTab + "&overviewtype=" + overviewType +"&deptid="+deptid+ "&sid=" + Math.random();
    if (selTab == 'Network') {
        statType = document.getElementById("hdNetworkSelType").value;  //getCookieData();
    }
    if (selTab == 'Uptrend') {
        statType = document.getElementById("hdUptrendSelType").value;  //getUptrendCookieData();
    }
    if (selTab == 'NCProgramStat') {
        statType = getProgramCookieData();
    }
    if(selTab=='ProgramRunInfo')
    {
       statType=document.getElementById("hdProgramRunInfo").value;
    }
    if(selTab=='AlarmRunInfo')
    {
       statType=document.getElementById("hdAlarmRunInfo").value;
    }
    url += "&selectedTab=" + statType;

    displayAjaxImage('ajaxloader', '');
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                var msg = xhttp.responseText;
                //if (msg == '') document.location.reload();
                //var tabToShow= document.getElementById("Main");
                var tabToShow = document.getElementById("divContentDetails");
                tabToShow.innerHTML = msg;
                //loadSelectedHomeTab();
                selectedHomeTab = selTab;
                availPeriod = period;
                displayAjaxContent(ajaxloader, '', 'false', 'false');
                ipslaCode(selTab);
                if (selTab == 'Network') SetOrderAndStyle(statType);
                
                if (selTab == 'AlarmRunInfo') setAlarmOrder();
                
                if (selTab == 'ProgramRunInfo') setProgramRunOrder();
                
                if (selTab == 'Uptrend') {
                    setUptrendXMLData(msg); //注册XML
                    swapTab(statType);
                }
                
                if (selTab == 'NCProgramStat') NCProgramSetOrderAndStyle(statType);
                if (selTab == 'MachinePara') SetMachineParaOrder();
                if (selTab == 'NCProgramList') SetRunExtentInfoOrder();
             
                 
            }
            else {
                displayAjaxContent(ajaxloader, '', 'false', 'false');
                //alert("Problem retrieving the XML data: " + xhttp.statusText);
            }
        }
    }
    try {
        xhttp.send(null);
    }
    catch (e)
    { }

}

function ipslaCode(selTab) {
    if (selTab == "Network") {
        var ipslaHomeTab = document.getElementById("ipslaHome");
        if (ipslaHomeTab != null) {
            var wanMonitorHome = ipslaHomeTab.style.display;
            if (wanMonitorHome == "block") {
                hideElement("calendarOptions");
            }
        }
    }
    else {
        eval("document.getElementById('calendarOptions').style.display='block';");
    }
}


function trimAll(str) {
    /*************************************************************
    Input Parameter :str
    Purpose         : remove all white spaces in front and back of string
    Return          : str without white spaces
    ***************************************************************/

    //check for all spaces
    var objRegExp = /^(\s*)$/;
    if (objRegExp.test(str)) {
        str = str.replace(objRegExp, '');
        if (str.length == 0)
            return str;
    }

    // check for leading and trailling spaces
    objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
    if (objRegExp.test(str)) {
        str = str.replace(objRegExp, '$2');
    }
    return str;
}

function clearSearch() {
    document.SearchForm.searchTerm.value = "";
    document.SearchForm.searchTerm.focus();
}


/************   row Glow script    **************/

function glowThisRow(rowId, glowVal, stc, endc) {
    rowId.style.backgroundColor = (glowVal) ? stc : endc;
}

function glowThisRowFont(rowId, glowVal, stc, endc, Fstc, Fendc) {
    rowId.style.backgroundColor = (glowVal) ? stc : endc;
    rowId.style.color = (glowVal) ? Fstc : Fendc;
}

function glowThisRowTD(rowId, glowVal, stc, endc) {
    for (i = 3; i <= 6; i++) {
        rowId.getElementsByTagName('td')[i].style.backgroundColor = (glowVal) ? stc : endc;
    }
}

function AddCategory() {
    delayMapMenu(0);
    //openGraphWindow('/admin/CategoryOperation.do?operation=showAdd','Add Category',550,425) ;
    javascript: showPopUp('/admin/CategoryOperation.do?operation=showAdd', '', 600, 470, false, false)
}

function findPosX(obj) {
    var curleft = 0;
    if (document.getElementById || document.all) {
        while (obj.offsetParent) {
            curleft += obj.offsetLeft
            obj = obj.offsetParent;
        }
    } else if (document.layers) {
        curleft += obj.x;
    }
    return curleft;
}

function findPosY(obj) {
    var curtop = 0;
    if (document.getElementById || document.all) {
        while (obj.offsetParent) {
            curtop += obj.offsetTop
            obj = obj.offsetParent;
        }
    } else if (document.layers) {
        curtop += obj.y;
    }
    return curtop;
}



var xmlHttp

function GetXmlHttpObject() {
    var objXMLHttp = null
    if (window.XMLHttpRequest) {
        objXMLHttp = new XMLHttpRequest()
        if (objXMLHttp.overrideMimeType) {
            // set type accordingly to anticipated content type
            objXMLHttp.overrideMimeType('text/html');
        }
    }
    else if (window.ActiveXObject) {
        try {
            objXMLHttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                objXMLHttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) { }
        }
    }
    return objXMLHttp
}

function fninvsh(Lay, menuId) {
    if (menuId == 'Maps') {
        delayMapMenu(200);
    }
}

var selectedMapMenu = "";

function fnvshobj3(obj, Lay, selMapView) {
    //alert("Getting called");
    selectedMapMenu = selMapView;
    var tagName = document.getElementById(Lay);
    var leftSide = findPosX(obj);
    var topSide = findPosY(obj);
    var maxW = tagName.style.width;
    var widthM = maxW.substring(0, maxW.length - 2);
    var getVal = eval(leftSide) + eval(widthM);
    if (getVal > document.body.clientWidth) {
        leftSide = eval(leftSide) - eval(widthM);
        tagName.style.left = leftSide - 34 + 'px';
    }
    else {
        tagName.style.left = leftSide - 13 + 'px';
    }
    tagName.style.top = topSide + 15 + 'px';
    loadMapMenus('Maps');
}

function fnvshobj2(obj, Lay, selMapView) {
    //alert("Getting called");
    selectedMapMenu = selMapView;
    var tagName = document.getElementById(Lay);
    var leftSide = findPosX(obj);
    var topSide = findPosY(obj);
    var maxW = tagName.style.width;
    var widthM = maxW.substring(0, maxW.length - 2);
    var getVal = eval(leftSide) + eval(widthM);
    if (getVal > document.body.clientWidth) {
        leftSide = eval(leftSide) - eval(widthM);
        tagName.style.left = leftSide + 300 + 'px';
    }
    else {
        tagName.style.left = leftSide + 300 + 'px';
    }
    tagName.style.top = topSide + 130 + 'px';
    loadMapMenus('Maps');
}

function fnvshobj(obj, Lay, menuId) {
    //alert("Getting called");
    if (menuId == 'Maps') {
        selectedMapMenu = "";
        var tagName = document.getElementById(Lay);
        var leftSide = findPosX(obj);
        var topSide = findPosY(obj);
        var maxW = tagName.style.width;
        var widthM = maxW.substring(0, maxW.length - 2);
        var getVal = eval(leftSide) + eval(widthM);
        if (getVal > document.body.clientWidth) {
            leftSide = eval(leftSide) - eval(widthM);
            tagName.style.left = leftSide + 150 + 'px';
        }
        else {
            tagName.style.left = leftSide - 48 + 'px';
        }
        tagName.style.top = topSide + 20 + 'px';
        tagName.style.display = 'block';
        tagName.style.visibility = 'visible';
        //loadMapMenus(menuId);

        //showMapMenus(menuId);
        //tagName.style.display = 'block';
        //tagName.style.visibility = "visible";
    }
    //alert("Getting called @@@@@@");
}

function hideMapMenu() {
    var tagName = document.getElementById('menuPop');
    tagName.style.visibility = 'hidden';
    tagName.style.display = 'none';
      var tagName1 = document.getElementById('menuPop1');
    tagName1.style.visibility = 'hidden';
    tagName1.style.display = 'none';
     var tagName2 = document.getElementById('menuPop2');
    tagName2.style.visibility = 'hidden';
    tagName2.style.display = 'none';
}

function delayMapMenu(msec) {
    //alert('Getting inside delayMapMenu function ');
    clearMapMenu();
    delayMapMenu1 = setTimeout("hideMapMenu()", msec)
}

function clearMapMenu() {
    if (typeof delayMapMenu1 != "undefined")
        clearTimeout(delayMapMenu1)
}

function clearMapMenu2() {
    if (typeof delayMapMenu1 != "undefined")
        clearTimeout(delayMapMenu1)
}

function loadMapMenus(menuId) {
    if (menuId == 'Maps') {
        clearMapMenu2();
        delayMapMenu1 = setTimeout("showMapMenus()", 200);
    }
}


function showMapMenus() {
    resetPopupMenu();
    var divID = document.getElementById("menuPop");
    eval("divID.style.display = 'block';");
    eval("divID.style.visibility = 'visible';");
    if (selectedMapMenu != '') {
        showSelectedMapMenuss(selectedMapMenu);
    }
}

function showSelectedMapMenuss(selTab) {
    //window.alert('Selected Menus '+ selTab);
    var selectedMenu;
    if (selTab == 'Infrastructure View') {
        selectedMenu = 'InfraDivView';
    }
    else if (selTab == 'Business View') {
        selectedMenu = 'BusinessDivView';
    }
    else if (selTab == 'Networks View') {
        selectedMenu = 'NetworkDivView';
    }
    if (selTab != '') {
        expandSelectedTab(selectedMenu, selectedMenu + 'Complete', selectedMenu + 'Default');
        hideBackMenu(selectedMenu + 'BackMenu');
    }
}

function hideBackMenu(id) {
    var hide = document.getElementById(id);
    eval("hide.style.display = 'none'");
}

function showBackMenu(id) {
    var hide = document.getElementById(id);
    eval("hide.style.display = 'block'");
}

function expandSelectedTab(selTab, id1, id2) {
    var tabsAvailable = new Array("InfraDivView", "BusinessDivView", "NetworkDivView");
    var tabsLen = tabsAvailable.length;
    var selectedTab = document.getElementById(selTab);

    for (var i = 0; i < tabsLen; i++) {
        var idToHide = document.getElementById(tabsAvailable[i]);
        if (idToHide.style.display == 'block') {
            eval("idToHide.style.display = 'none';");
        }
    }
    eval("selectedTab.style.display = 'block';");
    toggleDiv(id1, id2);

}

function resetPopupMenu() {
    var tabsAvailable = new Array("InfraDivView", "BusinessDivView", "NetworkDivView");
    var tabsLen = tabsAvailable.length;
    for (var i = 0; i < tabsLen; i++) {
        var idToHide = document.getElementById(tabsAvailable[i]);
        eval("idToHide.style.display = 'block';");
    }
    toggleDiv('InfraDivViewDefault', 'InfraDivViewComplete');
    toggleDiv('BusinessDivViewDefault', 'BusinessDivViewComplete');
    toggleDiv('NetworkDivViewDefault', 'NetworkDivViewComplete');
}

function toggleDiv(id1, id2) {
    var disp = document.getElementById(id1);
    var hide = document.getElementById(id2);
    if (disp.style.display == 'none') {
        eval("disp.style.display = 'block'");
    }
    if (hide.style.display == 'block') {
        eval("hide.style.display = 'none'");
    }
    //expandSelectedTab('InfraDivView', id1, id2);
}



// Universal Script for dropdown list

var hideDiv = "";

function hideDropDiv(e) {

    document.getElementById(hideDiv).style.display = "none";

}

function delayhideDropDiv(msec, whereto) {
    hideDiv = whereto;
    clearhideDropDiv()
    delayDropDiv = setTimeout("hideDropDiv()", msec)
}


function clearhideDropDiv() {
    if (typeof delayDropDiv != "undefined")
        clearTimeout(delayDropDiv)
}

function deleteCategory(viewId1) {
    if (confirm("<fmt:message key='webclient.map.category.delete.message'/>")) {
        var url = "/map/MapView.do?requestedOperation=deleteCategory&viewId=" + viewId1;
        location.href = url;
    }
}

function changeView(viewName, mapName, sortBy) {
    var url = "/map/SaveMapSettings.do?viewType=" + viewName + "&sortByOption=" + sortBy + "&mapName=" + mapName;
    location.href = url;
}

function changeSort(sortType, mapName, selectedView) {
    var url = "/map/SaveMapSettings.do?viewType=" + selectedView + "&sortByOption=" + sortType + "&mapName=" + mapName;
    location.href = url;
}

function importDevices(catName) {
}

function saveView(selectedView, sortType, viewId) {
    var url = "/map/SaveMapSettings.do?viewType=" + selectedView + "&sortByOption=" + sortType + "&mapName=" + viewId + "&saveSettings=true";
    location.href = url;
}




/*   jump to men script starts here  */

function swapJumpDiv() {
    var hideJump = "";
    hideJump = document.getElementById('jumptoContent').style;
    if (hideJump.display == 'none' || hideJump.display == '') {
        clearhideDropDiv();
        eval("hideJump.display = 'block'");
    }
    else {
        //eval("hideJump.display = 'none'");
        delayhideDropDiv(0, 'jumptoContent');
    }
}

function showDropDiv() {
    var tagName1 = document.getElementById('jumptoContent');
    tagName1.style.display = 'block';
}

function delayShowJumpMenu() {
    clearJumptoMenu();
    delayJumptoMenu = setTimeout("showDropDiv()", 800)
}

function clearJumptoMenu() {
    if (typeof delayJumptoMenu != "undefined")
        clearTimeout(delayJumptoMenu)
}

function confirmAndDeleteNetwork(str, str1) {
    if (confirm(str1)) {
        var networkIp = str.substring(0, str.lastIndexOf('.'));
        var del_url = "/topo/addNetwork.do?operation=delete&isFromNView=yes&networks=" + networkIp;
        location.href = del_url;
        return true;
    }
    else {
        return false;
    }
}
// Universal Script for dropdown list with two submenu


function hideDropDiv1(e) {
    document.getElementById('floaterDiv').style.display = "none";
    //delayhideDropDiv(500,'MenuActions_1');

}


function delayhideDropDiv1(msec) {
    clearhideDropDiv1();
    delayDropDiv1 = setTimeout("hideDropDiv1()", msec)
}


function clearhideDropDiv1() {
    if (typeof delayDropDiv1 != "undefined")
        clearTimeout(delayDropDiv1)
}

function showSelectedReportBySummary(selTab) {
    var machineId = document.getElementById("selectedMachineID").value;
//    if (machineId == "") {
//        alert("请选择机床");
//        return;
//    }
    var periodType = document.getElementById("HiddenPeriodType").value;
    //SetTabStyle('Network', 'HomeTab');
    showSelectedReport('Network', selTab);
}
//***********************
//对主界面第二个选项卡操作
//************************
var previousSel = "";

function getCookieData() {
    var repID = ''; //readCookie('SelectedReportTab')
    if (repID != null && repID != '') {
        return repID;
    }
    else {
        return 'RunEfficiency';
    }
}



function SetRunProgramRunInfo(statType) {
   // ChangeTabStyle(statType);   
    switch (statType) {
        case "ProgramRunInfo": //加工停息
            var tdMTID = tdIsExist("tableProgramRunInfo", "idMtID");
            var tdMTUser = tdIsExist("tableProgramRunInfo", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableProgramRunInfo");
            SetOrder("idMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idRunDuring", col + 1, { Attri: "_order", DataType: "int" });
            SetOrder("idCountDring", col + 2, { Attri: "_order", DataType: "int" });
            SetOrder("idDuringPersent", col + 3, { Attri: "_order", DataType: "float" });
            break;        
        default:
            break;
    }
}


function SetOrderAndStyle(statType) {
//    ChangeTabStyle(statType);
    //createCookie('SelectedReportTab', statType, 30);
    switch (statType) {
        case "RunEfficiency": //机床运行效率
            var tdMTID = tdIsExist("tableRunEfficiency", "idMtID");
            var tdMTUser = tdIsExist("tableRunEfficiency", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableRunEfficiency");
            SetOrder("idMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idRunDuring", col + 1, { Attri: "_order", DataType: "int" });
            SetOrder("idCountDring", col + 2, { Attri: "_order", DataType: "int" });
            SetOrder("idDuringPersent", col + 3, { Attri: "_order", DataType: "float" });
            break;
        case "UsingRate": //机床利用率
            var tdMTID = tdIsExist("tableUsingRate", "idMtID");
            var tdMTUser = tdIsExist("tableUsingRate", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableUsingRate");
            SetOrder("idURMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idURRunDuring", col + 1, { Attri: "_order", DataType: "float" });
            SetOrder("idURPlaneTime", col + 2, { Attri: "_order", DataType: "float" });
            SetOrder("idUsingRate", col + 3, { Attri: "_order", DataType: "float" });
            break;
        case "Status": //机床状态统计
            var tdMTID = tdIsExist("tableUsingRate", "idMtID");
            var tdMTUser = tdIsExist("tableUsingRate", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;

            var showAlarmColumn = false;
            var i = 0, j = 0;
            if ($("idSTAlarm") == null && $("idPerAlarm") == null) {
                i = 1;
                j = 2;
            } else
                showAlarmColumn = true;

            newTableObj("tableUsingRate");
            SetOrder("idSTMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", 2);
            SetOrder("idSTRunDuring", col + 1, { Attri: "_order", DataType: "float" });
            SetOrder("idSTIdle", col + 2, { Attri: "_order", DataType: "float" });
            if (showAlarmColumn)
                SetOrder("idSTAlarm", col + 3, { Attri: "_order", DataType: "float" });
            SetOrder("idSTOffline", col + 4 - i, { Attri: "_order", DataType: "float" });
            SetOrder("idPerRun", col + 5 - i, { Attri: "_order", DataType: "float" });
            SetOrder("idPerIdle", col + 6 - i, { Attri: "_order", DataType: "float" });
            if (showAlarmColumn)
                SetOrder("idPerAlarm", col + 7, { Attri: "_order", DataType: "float" });
            SetOrder("idPerOffline", col + 8 - j, { Attri: "_order", DataType: "float" });
            break;
        case "StartingUpRate": //机床开机率
            var tdMTID = tdIsExist("tableStartingUpRate", "idMtID");
            var tdMTUser = tdIsExist("tableStartingUpRate", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableStartingUpRate");
            SetOrder("idSUMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idSUCountDring", col + 1, { Attri: "_order", DataType: "float" });
            SetOrder("idSUPlanTime", col + 2, { Attri: "_order", DataType: "float" });
            SetOrder("idStartingUpRate", col + 3, { Attri: "_order", DataType: "float" });
            break;
        case "TotalStartingUpRate": //机床总开机率
            var tdMTID = tdIsExist("tableTotalStartingUpRate", "idMtID");
            var tdMTUser = tdIsExist("tableTotalStartingUpRate", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableTotalStartingUpRate");
            SetOrder("idSUMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idSUCountDring", col + 1, { Attri: "_order", DataType: "float" });
            SetOrder("idSUPlanTime", col + 2, { Attri: "_order", DataType: "float" });
            SetOrder("idStartingUpRate", col + 3, { Attri: "_order", DataType: "float" });
            break;
        case "DayRunningRate": //数控设备主轴日运转效率
            newTableObj("tableDayRunningRate");
            SetOrder("idDRRMatoName", 0);
            SetOrder("idDRRCountDring", 1, { Attri: "_order", DataType: "int" });
            SetOrder("idDayRunningRate", 3, { Attri: "_order", DataType: "float" });
            break;
        case "ScanMachiningInfo":
            newTableObj("tableScanMachiningInfo");
            SetOrder("idMatoName", 0);
            SetOrder("idMachining", 1, { Attri: "_order", DataType: "float" });
            SetOrder("idArmor", 2, { Attri: "_order", DataType: "float" });
            SetOrder("idFix", 3, { Attri: "_order", DataType: "float" });
            SetOrder("idCrane", 4, { Attri: "_order", DataType: "float" });
            SetOrder("idOther", 5, { Attri: "_order", DataType: "float" });
            break;
        case "MachineCuttingEffectRate": //程序有效切削率
            var tdMTID = tdIsExist("tableCuttingEfficiency", "idMtID");
            var tdMTUser = tdIsExist("tableCuttingEfficiency", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableCuttingEfficiency");
            SetOrder("idMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idRunDuring", col + 1, { Attri: "_order", DataType: "int" });
            SetOrder("idCountDring", col + 2, { Attri: "_order", DataType: "int" });
            SetOrder("idDuringPersent", col + 3, { Attri: "_order", DataType: "float" });
            break;
        case "ActiveCuttingEffect": //有效切削率
            var tdMTID = tdIsExist("tableActiveCutRate", "idMtID");
            var tdMTUser = tdIsExist("tableActiveCutRate", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableActiveCutRate");
            SetOrder("idMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idRunDuring", col + 1, { Attri: "_order", DataType: "int" });
            SetOrder("idCountDring", col + 2, { Attri: "_order", DataType: "int" });
            SetOrder("idDuringPersent", col + 3, { Attri: "_order", DataType: "float" });
            break;
        case "DeviceEffect": //设备利用率
            var tdMTID = tdIsExist("tableDeviceEfficiency", "idMtID");
            var tdMTUser = tdIsExist("tableDeviceEfficiency", "idMtUser");
            var col = 0;
            if (tdMTID && tdMTUser) col = 2;
            else if (tdMTID || tdMTUser) col = 1;
            newTableObj("tableDeviceEfficiency");
            SetOrder("idMatoName", 0);
            if (tdMTID) SetOrder("idMtID", 1);
            if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
            SetOrder("idRunDuring", col + 1, { Attri: "_order", DataType: "int" });
            SetOrder("idCountDring", col + 2, { Attri: "_order", DataType: "int" });
            SetOrder("idDuringPersent", col + 3, { Attri: "_order", DataType: "float" });
            break;
        default:
            break;
    }
}

//判断某列在表中是否存在
function tdIsExist(table, td) {
//    var tableObj = document.getElementById(table);
//    for (var i = 0; i < tableObj.rows(0).cells.length; i++) {
//        var colLink = tableObj.rows(0).cells(i).getElementsByTagName("A");
//        if (colLink.length > 0 && colLink[0].id == td) {
//            //break;
//            return true;
//        }
//    }
//    return false;
}

function showSelectedReport(selTab, selType) {
    document.getElementById("hdNetworkSelType").value = selType;
    //alert(selTab+'---------'+selType);
    var machineId = document.getElementById("selectedMachineID").value;
    if (machineId == "") {
        alert("请选择机床");
        return;
    }
    var periodType = document.getElementById("HiddenPeriodType").value;
    var procedureid = document.getElementById("selectedProcedureID").value;
    var deptid = document.getElementById("selectdeptID").value;
    var xhttp = GetXmlHttpObject();
    //var url = "Communication/Handler.ashx?requestType=AJAX&machineId=" + machineId + "&procedureid=" + procedureid + "&deptid=" + deptid + "&period=" + periodType + "&requestId=" + selTab + "&selectedTab=" + selType + "&sid=" + Math.random();
    var url = "Communication/Handler.ashx?requestType=AJAX&machineId=" + machineId + "&deptid=" + deptid + "&period=" + periodType + "&requestId=" + selTab + "&selectedTab=" + selType + "&sid=" + Math.random();
    
    displayAjaxImage('ajaxloader', '');
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                var msg = xhttp.responseText;
                if (msg == '')
                    msg = "No Data";
                //document.location.reload();
                var tabToShow = document.getElementById("divContentDetails");
                tabToShow.innerHTML = msg;
                selectedHomeTab = selTab;
                availPeriod = periodType;
                displayAjaxContent(ajaxloader, '', 'false', 'false');
                ipslaCode(selTab);
                if (selTab == 'Network') SetOrderAndStyle(selType);
                if (selTab == 'Uptrend') {
                    setUptrendXMLData(msg);
                    swapTab(selType);
                }
                if (selTab == 'NCProgramStat') NCProgramSetOrderAndStyle(selType);
            }
            else {
                displayAjaxContent(ajaxloader, '', 'false', 'false');
                alert("Problem retrieving the XML data: " + xhttp.statusText);
            }
        }
    }
    xhttp.send(null);
}

//初始化分页
function initSplitPage() {
    page = new Page(15, "tableStartingUpOfflineTime", "tbodyStartingUpOfflineTime", "tablePage", "divPage", "spanRowCount", "spanCurrentPage");
}

//报警信息排序
function setAlarmOrder() 
{
    var tdMTID = tdIsExist("tableAlarmRunInfo", "idAlarmMtID");
    var tdMTUser = tdIsExist("tableAlarmRunInfo", "idAlarmMtUser");
    var col = 0;
    if (tdMTID && tdMTUser) col = 2;
    else if (tdMTID || tdMTUser) col = 1;
    newTableObj("tableAlarmRunInfo");      
    SetOrder("idAlarmMatoName", 0);        
    if (tdMTID) SetOrder("idAlarmMtID", 1);
    if (tdMTUser) SetOrder("idAlarmMtUser", col == 2 ? 2 : 1);
    SetOrder("idAlarmDuring", col + 1, { Attri: "_order", DataType: "int" });
}

function ChangeTabStyle(column) {

    var divName = document.getElementById('leftcontent');
    tableList = divName.getElementsByTagName('TABLE');

    var lengthh = tableList.length;
    //alert('The table length '+ lengthh );
    for (i = 0; i < lengthh; i++) {

        oTable = tableList.item(i);

        // Retrieve the rows collection for the table.
        var aRows = oTable.rows;
        //alert('The table rows length '+ aRows.length);

        // Retrieve the cells collection for the first row.
        var aCells = aRows[0].cells;
        //alert('The table cells length '+ aCells.length);

        //alert('The table name '+ oTable.id);
        var columnTab = column + "LeftTab";
        var hideName = oTable.id;

        hideName = hideName.substring(0, hideName.length - 7);
        //alert('The table  name '+ hideName);
        var divHideLine = document.getElementById(hideName + "HideLine");
        var divTabExt = document.getElementById(hideName + "TabExt");
        //alert('THE TABLE NAME DISPLAYX '+ divHideLine);

        if (oTable.id == columnTab) {
            //alert('The selected table name '+ oTable.id);
            eval("divTabExt.style.display = 'block';");
            aCells[0].className = "lines2";
            aCells[1].className = "lines2";
            aCells[2].className = "lines2";
            //aCells[3].className = "lines2";
            //aCells[4].className = "lines2";
            eval("divHideLine.style.display = 'block';");
        }
        else {
            eval("divTabExt.style.display = 'none';");
            aCells[0].className = "";
            aCells[1].className = "unselMonitor";
            aCells[2].className = "unselMonitor";
            //aCells[3].className = "unselMonitor";
            //aCells[4].className = "unselMonitor";
            eval("divHideLine.style.display = 'none';");
        }

    }
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var ca = document.cookie.split(';');
    var nameEQ = name + "=";
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

//***********************
//结束
//***********************

//***********************
//走势图选项卡操作*******
function swapTab(nn) {
    var tabName;
    var contntName;
    var active;
    var inactive;
    for (i = 1; i <= 4; i++) {
        tabName = "introTab" + i;
        //contntName = "innerContnt" + i;
        active = "introTabsBg introTabActive" + i;
        inactive = "introTabsBg introTabInActive" + i;
        document.getElementById(tabName).className = (i == nn) ? active : inactive;
        //document.getElementById(contntName).style.display = (i == nn) ? "block" : "none";
    }
    //createCookie('SelectedUptrendTab', nn, 30);
    document.getElementById("hdUptrendSelType").value = nn;
}

function getUptrendCookieData() {
    var repID = ''; //readCookie('SelectedUptrendTab')
    if (repID != null && repID != '') {
        return repID;
    }
    else {
        return 1;
    }
}
function setUptrendXMLData(msg) {
    var sprnstr = "<!--starttrend-->";
    var eprnstr = "<!--endtrend-->";
    var prnhtml = msg.substr(msg.indexOf(sprnstr) + 17);
    prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));
    var chart_UptrendChart = new FusionCharts("ChartData/ScrollCombi2D.swf", "UptrendChart", "650", "300", "0", "0", "", "noScale", "EN");
    chart_UptrendChart.setDataXML(prnhtml);
    chart_UptrendChart.render("UptrendChartDiv");
}
//走势图选项卡操作结束********
//****************************

//加工信息 排序
function setProgramRunOrder()
{

        var tdMTID = tdIsExist("tableRunEfficiency", "idMtID");
        var tdMTUser = tdIsExist("tableRunEfficiency", "idMtUser");
        var col = 0;
        if (tdMTID && tdMTUser) col = 2;
        else if (tdMTID || tdMTUser) col = 1;
        newTableObj("tableRunEfficiency");      
        SetOrder("idMatoName", 0);        
        if (tdMTID) SetOrder("idMtID", 1);
        if (tdMTUser) SetOrder("idMtUser", col == 2 ? 2 : 1);
        SetOrder("idRunDuring", col + 1, { Attri: "_order", DataType: "int" });
      
    
}


 
//人员添加程序和下发程序选项卡开始********
//****************************************
function getProgramCookieData() {
    var repID = ''; //readCookie('SelectedProgramTab')
    if (repID != null && repID != '') {
        return repID;
    }
    else {
        return 'AddProgramStat';
    }
}
function NCProgramSetOrderAndStyle(statType) {
//    ChangeTabStyle(statType);
    //createCookie('SelectedProgramTab', statType, 30);
    if (statType == "AddProgramStat") {
        newTableObj("tableAddProgramStat");
        SetOrder("idDept", 0);
        SetOrder("idUserName", 1);
        SetOrder("idQuantity", 2, { Attri: "_order", DataType: "int" });
        SetOrder("idPersent", 3, { Attri: "_order", DataType: "float" });
    }
    if (statType == "SendProgramStat") {
        newTableObj("tableSendProgramStat");
        SetOrder("idDept", 0);
        SetOrder("idUserName", 1);
        SetOrder("idQuantity", 2, { Attri: "_order", DataType: "int" });
        SetOrder("idPersent", 3, { Attri: "_order", DataType: "float" });
    }
}
//人员添加程序和下发程序选项卡结束********
//****************************************

//机床参数选项卡开始********
//****************************************
function SetMachineParaOrder() {
    newTableObj("tableMachinePara");
    SetOrder("idMatoName", 0);
    SetOrder("idFValueNum", 1, { Attri: "_order", DataType: "int" });
    SetOrder("idUSValueNum", 2, { Attri: "_order", DataType: "int" });
}
//机床参数选项卡结束********
//****************************************

//扫描加工信息统计
//根据选择的产品号加载工序列表
function getSeqNOByItemCode(itemCode) {
    var obj = document.getElementById("ScanSeqNOList");
    if (obj == 'undefined' || obj == null) return;

    var dt = new Ajax.Web.DataTable();
    dt = MasterPage.GetSeqNO(itemCode);
    if (dt.value != null && dt.value.Rows.length > 0) {
        while (obj.options.length) {
            obj.options.remove(0);
        }
        for (var i = 0; i < dt.value.Rows.length; i++) {
            var oOption = document.createElement("option");
            oOption.text = dt.value.Rows[i].SEQ_NO;
            oOption.value = dt.value.Rows[i].SEQ_NO;
            obj.options.add(oOption);
        }
    }
}
function getScanInfoStatChart() {
    var machineId = document.getElementById("selectedMachineID").value;
    if (machineId == "") {
        alert("请选择机床");
        return;
    }

    machineId = machineId.replace(/\'/g, '');
    var periodType = document.getElementById("HiddenPeriodType").value;
    var itemCode = document.getElementById("ScanItemCodeList");
    itemCode = itemCode.options[itemCode.selectedIndex].text;
    var seqNO = document.getElementById("ScanSeqNOList");
    seqNO = seqNO.options[seqNO.selectedIndex].text;

    var xhttp = GetXmlHttpObject();
    var url = "Communication/GetHtmlData.ashx?requestType=AJAX&requestId=ScanMachiningInfo&machineId=" + machineId + "&periodType=" + periodType + "&itemCode=" + itemCode + "&seqNO=" + seqNO + "&sid=" + Math.random();

    displayAjaxImage('ajaxloader', '');
    xhttp.open("GET", url, true);
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                var msg = xhttp.responseText;
                var tabToShow = document.getElementById("showScanStatChart");
                tabToShow.innerHTML = msg;
                availPeriod = periodType;
                displayAjaxContent(ajaxloader, '', 'false', 'false');
            }
            else {
                displayAjaxContent(ajaxloader, '', 'false', 'false');
                alert("Problem retrieving the XML data: " + xhttp.statusText);
            }
        }
    }
    xhttp.send(null);
}
///扫描加工信息统计选项卡结束********
//****************************************

//北重特殊需求********
//****************************************
function SetRunExtentInfoOrder() {
    //    newTableObj("tableExtentInfo");
    //    SetOrder("idMatoName", 0);
    //    SetOrder("idProgramName", 1);
    //    SetOrder("idTime", 2);
    //    SetOrder("idSetSpeed", 3, { Attri: "_order", DataType: "int" });
    //    SetOrder("idActualSpeed", 4, { Attri: "_order", DataType: "int" });
    //    SetOrder("idSetValue", 5, { Attri: "_order", DataType: "int" });
    //    SetOrder("idActualValue", 6, { Attri: "_order", DataType: "int" });
    //    SetOrder("idSPINDLETORQUE", 7, { Attri: "_order", DataType: "int" });
    //    SetOrder("idFEED_RATE", 8, { Attri: "_order", DataType: "int" });
    try {
        page = new Page(20, "tableExtentInfo", "tbodyExtentInfo", "tablePage", "divPage", "spanRowCount", "spanCurrentPage");
    }
    catch (e)
    { }
}
//北重特殊需求********
//****************************************

var win;
function openly(machineName) {
    try {
        if (win && win.setBackColorMachine) {
            win.setBackColorMachine = machineName;
            win.focus();
        } else {
            justOpen(machineName);
        }
    } catch (Error) {
        justOpen(machineName);
    }
}
function justOpen(machineName) {
    win_name = "openShowLayout";
    var wdt = screen.availWidth;
    var htt = screen.availHeight;
    win_props = "width=" + wdt + ",height=" + htt + ",top=0,left=0,scrollbars=yes,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes";
    win = window.open("Config/ShowLayoutMap.aspx?setMachine=" + machineName, win_name, win_props);
    win.opener = window;

    navapp = navigator.appVersion;
    if (navapp.indexOf("MSIE") == -1) {
        win.focus();
    }
    win.focus();
}
