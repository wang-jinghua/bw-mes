﻿/*JavaScript操作Date对象函数库,其中功能有返回本月,上一月,返回本周,上一周,返回本季度,上季度,本年,上一年等.*/

function updateDate(typeName, fromName, toName) {
    var today = new Date();
    var ty = document.getElementById(typeName).value;
    var ds = [];
    switch (ty) {
        case '':
            ds[0] = '';
            ds[1] = '';
            break;
        case 'today':
            ds[0] = today.parseString();
            ds[1] = today.parseString();
            break;
        case 'theWeek':
            ds = today.theWeek();
            break;
        case 'lastWeek':
            ds = today.lastWeek();
            break;
        case 'theMonth':
            ds = today.theMonth();
            break;
        case 'lastMonth':
            ds = today.lastMonth();
            break;
        case 'theQuarter':
            ds = today.theQuarter();
            break;
        case 'lastQuarter':
            ds = today.lastQuarter();
            break;
        case 'theYear':
            ds = today.theYear();
            break;
        case 'lastYear':
            ds = today.lastYear();
            break;
        case 'last7Day':
            ds = today.lastDays(7);
            break;
        case 'last30Day':
            ds = today.lastDays(30);
            break;
        case 'custom':
            ds[0] = '';
            ds[1] = '';
            break;
    }
    if (ds != null) {
        document.getElementById(fromName).value = ds[0];
        document.getElementById(toName).value = ds[1];
    }
}

function updateType(typeName) {
    document.getElementById(typeName).selectedIndex = 12;
}

//返
Date.prototype.getDaysOfMonth = function() {
    return (new Date(this.getFullYear(), this.getMonth() + 1, 0)).getDate();
}

//上
Date.prototype.lastQuarter = function() {
    var arr = [];
    var m = this.getMonth();
    var q = Math.floor(m / 3) + 1; //本

    var f; //开
    var l; //结
    switch (q) {
        case 1:
            f = new Date(this.getFullYear() - 1, 9, 1);
            l = new Date(this.getFullYear() - 1, 11, 31);
            break;
        case 2:
            f = new Date(this.getFullYear(), 0, 1);
            l = new Date(this.getFullYear(), 2, 31);
            break;
        case 3:
            f = new Date(this.getFullYear(), 3, 1);
            l = new Date(this.getFullYear(), 5, 30);
            break;
        case 4:
            f = new Date(this.getFullYear(), 6, 1);
            l = new Date(this.getFullYear(), 8, 30);
            break;
    }

    arr[0] = f.parseString();
    arr[1] = l.parseString();
    return arr;
}
//本
Date.prototype.theQuarter = function() {
    var arr = [];
    var m = this.getMonth();
    var q = Math.floor(m / 3) + 1; //季

    var t = new Date(this.getFullYear(), 3 * q - 2 - 1, 1);
    arr[0] = t.parseString();
    arr[1] = this.parseString();
    return arr;
}



//去
Date.prototype.lastYear = function() {
    var arr = [];
    var t = new Date(this.getFullYear() - 1, 0, 1); //去
    var l = new Date(this.getFullYear() - 1, 11, 31); //去

    arr[0] = t.parseString();
    arr[1] = l.parseString();
    return arr;
}

//今
Date.prototype.theYear = function() {
    var arr = [];
    var t = new Date(this.getFullYear(), 0, 1);
    arr[0] = t.parseString();
    arr[1] = this.parseString();
    return arr;
}
//最
Date.prototype.lastDays = function(num) {
    var arr = [];
    arr[0] = this.dateAfter(-num).parseString();
    arr[1] = this.parseString();
    return arr;
}

//上
Date.prototype.lastMonth = function() {
    var arr = [];
    var t = new Date(this.getFullYear(), this.getMonth(), 1); //本
    var l = t.dateAfter(-1); //上
    var f = new Date(l.getFullYear(), l.getMonth(), 1); //上

    arr[0] = f.parseString();
    arr[1] = l.parseString(); //上
    return arr;
}


//本
Date.prototype.theMonth = function() {
    var arr = [];
    var d = this.getDate();
    arr[0] = this.dateAfter(-d + 1).parseString();
    arr[1] = this.parseString();
    return arr;
}

//本
Date.prototype.theWeek = function() {
    var arr = [];
    var w = this.getDay();
    if (w == 0) w = 7;
    arr[0] = this.dateAfter(-w).parseString();
    arr[1] = this.parseString();
    return arr;
}

//上
Date.prototype.lastWeek = function() {
    var arr = [];
    var w = this.getDay();
    if (w == 0) w = 7;
    arr[0] = this.dateAfter(-w - 7).parseString();
    arr[1] = this.dateAfter(-w).parseString();
    return arr;
}

/*将如 9 to 09*/
String.prototype.fmtWithZero = function(isFmtWithZero) {
    if (isFmtWithZero)
        return (this < 10 ? "0" + this : this);
    else return this;
}
String.prototype.fmtWithZeroD = function(isFmtWithZero) {
    if (isFmtWithZero)
        return (this < 10 ? "00" + this : (this < 100 ? "0" + this : this));
    else return this;
}
/*
* 功根
* 参dateFmt:字由
* yy:长YY:短m:数MM:英dd:日hh:时
* mi:分ss秒ms:毫we:汉WE:英
* isFmtWithZero : 是进true or false
*/
Date.prototype.parseString = function(dateFmt, isFmtWithZero) {
    dateFmt = (dateFmt == null ? "yy-mm-dd" : dateFmt);
    isFmtWithZero = (isFmtWithZero == null ? true : isFmtWithZero);
    if (typeof (dateFmt) != "string")
        throw (new Error(-1, 'parseString()方'));
    var weekArr = [["星", "星", "星", "星", "星", "星", "星"],
                   ["SUN", "MON", "TUR", "WED", "THU", "FRI", "SAT"]];
    var monthArr = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    var str = dateFmt;
    str = str.replace(/yy/g, this.getFullYear());
    str = str.replace(/YY/g, this.getYear());
    str = str.replace(/mm/g, (this.getMonth() + 1).toString().fmtWithZero(isFmtWithZero));
    str = str.replace(/MM/g, monthArr[this.getMonth()]);
    str = str.replace(/dd/g, this.getDate().toString().fmtWithZero(isFmtWithZero));
    str = str.replace(/hh/g, this.getHours().toString().fmtWithZero(isFmtWithZero));
    str = str.replace(/mi/g, this.getMinutes().toString().fmtWithZero(isFmtWithZero));
    str = str.replace(/ss/g, this.getSeconds().toString().fmtWithZero(isFmtWithZero));
    str = str.replace(/ms/g, this.getMilliseconds().toString().fmtWithZeroD(isFmtWithZero));
    str = str.replace(/we/g, weekArr[0][this.getDay()]);
    str = str.replace(/WE/g, weekArr[1][this.getDay()]);
    return str;
}

//格式化yyyy-MM-dd HH:mm:ss
Date.prototype.formatString = function() {
    var year = this.getFullYear()
    var month = this.getMonth() + 1;
    var date = this.getDate();
    var hour = this.getHours();
    var minute = this.getMinutes();
    var second = this.getSeconds();

    month = month.toString().fmtWithZero(true);
    date = date.toString().fmtWithZero(true);
    hour = hour.toString().fmtWithZero(true);
    minute = minute.toString().fmtWithZero(true);
    second = second.toString().fmtWithZero(true);
    var s = year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
    return s;
}

//格式化yyyy-MM-dd
Date.prototype.formatShortString = function() {
    var year = this.getFullYear()
    var month = this.getMonth() + 1;
    var date = this.getDate();

    month = month.toString().fmtWithZero(true);
    date = date.toString().fmtWithZero(true);
    
    var s = year + "-" + month + "-" + date;
    return s;
}

Date.prototype.minusDays = function(days) {
    var interTimes = days * 24 * 60 * 60 * 1000;
    return new Date(Date.parse(this) - interTimes);
}


/* 功: 返天N个4小的
* 参: num number类可默;
* type 0(秒 or 1(天,默
* 返: 新owerDate类
*/
Date.prototype.dateAfter = function(num, type) {
    num = (num == null ? 1 : num);
    if (typeof (num) != "number") throw new Error(-1, "dateAfterDays(num,type)的um参");
    type = (type == null ? 1 : type);
    var arr = [1000, 86400000];
    var dd = this.valueOf();
    dd += num * arr[type];
    return new Date(dd);
}

//增加天
Date.prototype.addDays = function(days) {
    var newtimems = this.getTime() + (days * 24 * 60 * 60 * 1000);
    return new Date(newtimems);
}

//增加小时
Date.prototype.addHours = function(hours) {
    var newtimems = this.getTime() + (hours * 60 * 60 * 1000);
    return new Date(newtimems);
}

//增加分钟
Date.prototype.addMinutes = function(minutes) {
    var newtimems = this.getTime() + (minutes * 60 * 1000);
    return new Date(newtimems);
}

//增加秒
Date.prototype.addSeconds = function(seconds) {
    var newtimems = this.getTime() + (seconds * 1000);
    return new Date(newtimems);
}