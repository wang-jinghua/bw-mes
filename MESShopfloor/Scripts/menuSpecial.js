// ---------------------------------------------------------------------------
// Example of howto: use OutlookBar
// ---------------------------------------------------------------------------

//create OutlookBar
var o = new createOutlookBar('Bar',0,0,screenSize.width-17,screenSize.height,'#eeeeee','white','blue','red','#99ccff','black');//'#000099') // OutlookBar
var p;

//create first panel
p = new createPanel('a1','安全生产许可证');
p.addButton('../images/gov/ico_censor.gif','业务审批','parent.mainfrm.location="Certificate/CertQuery.aspx"');	
o.addPanel(p);
	
p = new createPanel('a4','工程报监业务');
p.addButton('../images/gov/ico_censor.gif','报监审批','parent.mainfrm.location="ProjectWatch/WatchQuery.aspx"');
o.addPanel(p);
	
p = new createPanel('a5','事故处理业务');
p.addButton('../images/gov/ico_censor.gif','事故审批','parent.mainfrm.location="Accident/AccidentQuery.aspx"');	
o.addPanel(p); 

//draw the OutlookBar
o.draw();  
	


