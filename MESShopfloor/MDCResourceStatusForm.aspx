﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="MDCResourceStatusForm.aspx.cs" Inherits="MDCResourceStatusForm" EnableViewState="true" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function IsDel() {
            if (confirm("确定要删除该派工记录吗？")) {
                return true;
            }
            else {
                return false;
            }
        }

        //二维码
        function openQRCodeForm() {
            window.showModalDialog("QRCodeForm.aspx", "", "dialogWidth:500px; dialogHeight:400px; status=no; center: Yes; sc resizable: NO;");
        }

    </script>
    <%--<igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">--%>
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td class="ScanTD" align="left" nowrap="nowrap" colspan="3">导入设备状态信息：
                    </td>
                    <td align="left" colspan="7" class="tdBottom">
                        <asp:FileUpload ID="fileUpload" runat="server" Width="360px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnImport" runat="server" Text="导入"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnImport_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="tdRight" align="left" nowrap="nowrap">设备编号：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtResourceName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">车间：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:DropDownList ID="ddlFactory" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;" AutoPostBack="True" OnSelectedIndexChanged="ddlFactory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">班组：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:DropDownList ID="ddlTeam" runat="server" Width="115px" Height="25px"></asp:DropDownList>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">采集日期：
                    </td>
                    <td align="left" nowrap="nowrap" class="tdRight">
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div id="ItemDiv" runat="server">
            <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="420px" Width="100%">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:UltraGridColumn Key="ResourceNo" Width="150px" BaseColumnName="ResourceNo" AllowGroupBy="No">
                                <Header Caption="设备编号">
                                </Header>

                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="FactoryName" Key="FactoryName" Width="150px" AllowGroupBy="No">
                                <Header Caption="车间">
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="TeamName" Key="TeamName" Width="150px" AllowGroupBy="No">
                                <Header Caption="班组">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ResourceStatus" Key="ResourceStatus" Width="150px" AllowGroupBy="No">
                                <Header Caption="状态">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="CollectDate" Key="CollectDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd HH:mm">
                                <Header Caption="采集时间">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ID" Key="ID" Hidden="True">
                                <Header Caption="ID">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="420px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: right;">
                        <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                            OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                        Text="第  页  共  页"></asp:Label>
                        <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                        <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                        <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                        <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                            OnClick="btnGo_Click" />
                        <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                            Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;" Text="1"
                            Visible="False"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    <%--</igmisc:WebAsyncRefreshPanel>--%>

</asp:Content>
