﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RejectAppSubmitPopupForm.aspx.cs" validaterequest="false" Inherits="RejectAppSubmitPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>

<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>提交不合格品审理</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                   工作令号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap" style=" display:none">
                   作业令号：
                </td>
                <td align="left" class="tdRightAndBottom" style=" display:none">
                    <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    批次号：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispContainerID" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtDispSpecID" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="TextBox1" runat="server" Visible="false"></asp:TextBox>
                </td> 
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    图号：
                </td>
                <td align="left" class="tdRightAndBottom" colspan="3">
                    <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    <asp:TextBox ID="txtDispProductID" runat="server" Visible="false"></asp:TextBox> 
                    <asp:TextBox ID="txtWorkflowID" runat="server" Visible="false"></asp:TextBox>
                     <asp:TextBox ID="txtSpecName" runat="server" Visible="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    名称：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    数量：
                </td>
                <td align="left" class="tdRight">
                    <asp:TextBox ID="txtDispContainerQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    计划开始日期：
                </td>
                <td align="left" class="tdRight">
                    <asp:TextBox ID="txtDispPlannedStartDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRight" align="left" nowrap="nowrap">
                    计划完成日期：
                </td>
                <td align="left" class="tdNoBorder">
                    <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div style="height:8px;width:100%;"></div>
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                   不合格品审理单号：
                </td>
                <td align="left" class="tdRightAndBottom" colspan="1">
                    <asp:TextBox ID="txtRejectAppInfoName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                   责任部门：
                </td>
                <td align="left" class="tdRightAndBottom" colspan="7">
                     <asp:DropDownList ID="ddlResponsibleDepartment" runat="server" Width="150px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                   不合格品数量：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:TextBox ID="txtQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    不合格原因：
                </td>
                <td align="left" class="tdRightAndBottom">
                    <asp:DropDownList ID="ddlRejectReason" runat="server" Width="155px" Height="28px"
                        style="font-size:16px;" AutoPostBack="True"></asp:DropDownList>
                </td> 
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    工艺员：
                </td>
                <td align="left" class="tdRightAndBottom" colspan="3">
                    <asp:DropDownList ID="ddlDesignEmployee" runat="server" Width="155px" Height="28px"
                        style="font-size:16px;" AutoPostBack="True"></asp:DropDownList>
                </td>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    设计员：
                </td>
                <td align="left" class="tdRightAndBottom" colspan="1">
                    <asp:DropDownList ID="ddlDesignEmp2" runat="server" Width="155px" Height="28px"
                        style="font-size:16px;" AutoPostBack="True"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                    不合格信息描述：
                </td>
                <td align="left" class="tdRight" colspan="9">
                    <%--<asp:TextBox ID="txtQualityNotes" runat="server" class="stdTextBox" Height="200px" TextMode="MultiLine" Width="700px"></asp:TextBox>--%>
                     <FTB:FreeTextBox ID="txtQualityNotes" runat="Server" ToolbarLayout="..." EnableToolbars="False"
                                                    Height="160px" Width="500px" EnableHtmlMode="False" />
                </td>
            </tr>
        </table>
        </div>
    <div style="height:8px;width:100%;"></div>
    <div>
        <table style="width:100%;">
            <tr>
               <td style="text-align:left; width:100%;" colspan="2">
                    <asp:Button ID="btnMaterialApp" runat="server" Text="提交"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnMaterialApp_Click" />
                   <asp:Button ID="btnClose" runat="server" Text="关闭"
                        CssClass="searchButton" EnableTheming="True" OnClientClick="parent.window.returnValue='true';window.close()" />
               </td>
           </tr>
           <tr>
               <td style="font-size:12px; font-weight:bold;" nowrap="nowrap">状态信息：</td>
               <td style="text-align:left; width:100%;">
                   <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
               </td>
           </tr>
        </table>
    </div>
    </form>
</body>
</html>
