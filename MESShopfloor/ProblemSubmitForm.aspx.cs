﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.Web.UI;

public partial class ProblemSubmitForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "ProblemSubmitForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESProblemBusiness problem = new uMESProblemBusiness();
    uMESAssemblyRecordBusiness assembly = new uMESAssemblyRecordBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：问题上报";
        master.strTitle = "问题上报";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
            GetProblemType();
            GetProblemLevel();
            GetFactory();
        }
        
        //获得回发参数
     
    }

    #region 获取ProblemType
    protected void GetProblemType()
    {
        DataTable DT = common.GetProblemType();

        ddlProblemType.DataTextField = "ProblemTypeName";
        ddlProblemType.DataValueField = "ProblemTypeID";
        ddlProblemType.DataSource = DT;
        ddlProblemType.DataBind();

        ddlProblemType.Items.Insert(0, string.Empty);

        ddlSearchType.DataTextField = "ProblemTypeName";
        ddlSearchType.DataValueField = "ProblemTypeID";
        ddlSearchType.DataSource = DT;
        ddlSearchType.DataBind();

        ddlSearchType.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 获取ProblemLevel
    protected void GetProblemLevel()
    {
        DataTable DT = common.GetProblemLevel();

        ddlProblemLevel.DataTextField = "ProblemLevelName";
        ddlProblemLevel.DataValueField = "ProblemLevelID";
        ddlProblemLevel.DataSource = DT;
        ddlProblemLevel.DataBind();

        ddlProblemLevel.Items.Insert(0, string.Empty);


        ddlSearchLevel.DataTextField = "ProblemLevelName";
        ddlSearchLevel.DataValueField = "ProblemLevelID";
        ddlSearchLevel.DataSource = DT;
        ddlSearchLevel.DataBind();

        ddlSearchLevel.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 获取Factory
    protected void GetFactory()
    {
        DataTable DT = common.GetFactoryNames();

        ddlToFactory.DataTextField = "FactoryName";
        ddlToFactory.DataValueField = "FactoryID";
        ddlToFactory.DataSource = DT;
        ddlToFactory.DataBind();

        ddlToFactory.Items.Insert(0, string.Empty);

        ddlSearchFactory.DataTextField = "FactoryName";
        ddlSearchFactory.DataValueField = "FactoryID";
        ddlSearchFactory.DataSource = DT;
        ddlSearchFactory.DataBind();

        ddlSearchFactory.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        txtTitle.Text = string.Empty;
        ddlSearchEmployee.SelectedValue = string.Empty;
        ddlSearchFactory.SelectedValue = string.Empty;
        ddlSearchLevel.SelectedValue = string.Empty;
        ddlSearchType.SelectedValue = string.Empty;
        wgProblem.Clear();
        ClearContainerDisp();
        ClearProblemDisp();
    }

    protected void ClearContainerDisp()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispProductID.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtChildCount.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;
        ddlSpec.Items.Clear();
    }

    protected void ClearProblemDisp()
    {
        txtProblemTitle.Text = string.Empty;
        txtID.Text = string.Empty;
        txtStatus.Text = string.Empty;
        ddlProblemType.SelectedValue = string.Empty;
        ddlProblemLevel.SelectedValue = string.Empty;
        txtPlannedDisposeDate.Value = string.Empty;
        ddlToFactory.SelectedValue = string.Empty;
        ddlToEmployee.SelectedValue = string.Empty;
        txtDisposeEmployee.Text = string.Empty;
        txtProblemDetails.Text = string.Empty;
        txtProblemResult.Text = string.Empty;
    }
    #endregion



    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        string strProblemType = ddlSearchType.SelectedValue;
        string strProblemLevel = ddlSearchLevel.SelectedValue;
        string strToFactory = ddlSearchFactory.SelectedValue;
        string strToEmployee = ddlSearchEmployee.SelectedValue;

        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strSubmitEmployeeID = userInfo["EmployeeID"];

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);
        result.Add("ProblemTypeID", strProblemType);
        result.Add("ProblemLevelID", strProblemLevel);
        result.Add("ToFactoryID", strToFactory);
        result.Add("ToEmployeeID", strToEmployee);
        result.Add("Status", "0,10");
        result.Add("ProblemTitle", txtTitle.Text);
        result.Add("SubmitEmployeeID", strSubmitEmployeeID);
        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();
        ClearProblemDisp();
        ClearContainerDisp();
        try
        {
            uMESPagingDataDTO result = problem.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 15);
            this.wgProblem.DataSource = result.DBTable;
            this.wgProblem.DataBind();
            this.txtTotalPage.Text = result.PageCount;
            if (result.RowCount == "0")
            {
                this.txtCurrentPage.Text = "0";
            }
            lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
            this.txtPage.Text = this.txtCurrentPage.Text;
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
       

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion
    
    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void BindContainerInfo(string strContainerID)
    {
        ClearMessage();

        try
        {
            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ContainerID", strContainerID);
            DataTable DT = bll.GetAllDataForOutExcel(para);
            txtDispProcessNo.Text = string.Empty;
            txtDispOprNo.Text = string.Empty;
            txtDispContainerName.Text = string.Empty;
            txtDispContainerID.Text = string.Empty;
            txtDispWorkflowID.Text = string.Empty;
            txtDispProductName.Text = string.Empty;
            txtDispProductID.Text = string.Empty;
            txtDispDescription.Text = string.Empty;
            txtDispQty.Text = string.Empty;
            txtDispPlannedCompletionDate.Text = string.Empty;
            txtDispPlannedStartDate.Text = string.Empty;

            if (DT.Rows.Count>0)
            {
                if (!string.IsNullOrEmpty(DT.Rows[0]["ProcessNo"].ToString()))
                {
                    string strProcessNo = DT.Rows[0]["ProcessNo"].ToString();
                    txtDispProcessNo.Text = strProcessNo;
                }

                if (!string.IsNullOrEmpty(DT.Rows[0]["OprNo"].ToString()))
                {
                    string strOprNo = DT.Rows[0]["OprNo"].ToString();
                    txtDispOprNo.Text = strOprNo;
                }

                if (!string.IsNullOrEmpty(DT.Rows[0]["OprNo"].ToString()))
                {
                    txtDispContainerName.Text =DT.Rows[0]["OprNo"].ToString();
                }

                txtDispContainerID.Text = strContainerID;

                string strWorkflowID = DT.Rows[0]["WorkflowID"].ToString();
                txtDispWorkflowID.Text = strWorkflowID;
                Session["ParentContainerID"] = strContainerID;

                string strProductName = DT.Rows[0]["ProductName"].ToString();
                txtDispProductName.Text = strProductName;

                string strProductID = DT.Rows[0]["ProductID"].ToString();
                txtDispProductID.Text = strProductID;

                if (!string.IsNullOrEmpty(DT.Rows[0]["Description"].ToString()))
                {
                    string strDescription = DT.Rows[0]["Description"].ToString();
                    txtDispDescription.Text = strDescription;
                }

                string strQty = string.Empty;
                if (!string.IsNullOrEmpty(DT.Rows[0]["qty"].ToString()))
                {
                    strQty = DT.Rows[0]["qty"].ToString();

                }
                txtDispQty.Text = strQty;
                if (!string.IsNullOrEmpty(DT.Rows[0]["PlannedStartDate"].ToString()))
                {
                    string strPlannedStartDate = DT.Rows[0]["PlannedStartDate"].ToString();
                    txtDispPlannedStartDate.Text = strPlannedStartDate;
                }


                if (!string.IsNullOrEmpty(DT.Rows[0]["PlannedCompletionDate"].ToString()))
                {
                    string strPlannedCompletionDate = DT.Rows[0]["PlannedCompletionDate"].ToString();
                    txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
                }
                GetSpec(strWorkflowID);
                ////获取问题列表
                //Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                //string strSubmitEmployeeID = userInfo["EmployeeID"];

                //Dictionary<string, string> para = new Dictionary<string, string>();
                //para.Add("ContainerID", strContainerID);
                //para.Add("SubmitEmployeeID", strSubmitEmployeeID);
                //para.Add("Status", "0,10");
                //DataTable DT = problem.GetAllDataForOutExcel(para);

                //wgProblem.DataSource = DT;
                //wgProblem.DataBind();
                ClearProblemDisp();
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 绑定工序列表
    protected void GetSpec(string strWorkflowID)
    {
        List<string> listWorkflowID = new List<string>();
        listWorkflowID.Add(strWorkflowID);

        DataTable DT = common.GetSpecListByWorkflowID(listWorkflowID);
        DT.Columns.Add("SpecNameDisp");

        foreach (DataRow row in DT.Rows)
        {
            string strSpecName = row["SpecName"].ToString();
            strSpecName = common.GetSpecNameWithOutProdName(strSpecName);
            row["SpecNameDisp"] = strSpecName;
        }

        ddlSpec.DataTextField = "SpecNameDisp";
        ddlSpec.DataValueField = "SpecID";
        ddlSpec.DataSource = DT;
        ddlSpec.DataBind();

        ddlSpec.Items.Insert(0, "");
    }
    #endregion

    protected void wgProblem_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            if (e.Row.Cells.FromKey("containerid").Value !=null)
            {
                BindContainerInfo(e.Row.Cells.FromKey("containerid").Value.ToString());
            }
        

            txtID.Text = string.Empty;
            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtID.Text = strID;

            txtStatus.Text = string.Empty;
            string strStatus = e.Row.Cells.FromKey("Status").Value.ToString();
            txtStatus.Text = strStatus;

            txtProblemTitle.Text = string.Empty;
            string strProblemTitle = e.Row.Cells.FromKey("ProblemTitle").Value.ToString();
            txtProblemTitle.Text = strProblemTitle;

            ddlProblemType.SelectedValue = string.Empty;
            string strProblemTypeID = e.Row.Cells.FromKey("ProblemTypeID").Value.ToString();
            ddlProblemType.SelectedValue = strProblemTypeID;

            ddlProblemLevel.SelectedValue = string.Empty;
            string strProblemLevelID = e.Row.Cells.FromKey("ProblemLevelID").Value.ToString();
            ddlProblemLevel.SelectedValue = strProblemLevelID;

            ddlToFactory.SelectedValue = string.Empty;
            string strToFactoryID = e.Row.Cells.FromKey("ToFactoryID").Value.ToString();
            ddlToFactory.SelectedValue = strToFactoryID;

            ddlToFactory_SelectedIndexChanged(null, null);

            ddlToEmployee.SelectedValue = string.Empty;
            if (e.Row.Cells.FromKey("ToEmployeeID").Value != null)
            {
                string strToEmployeeID = e.Row.Cells.FromKey("ToEmployeeID").Value.ToString();
                ddlToEmployee.SelectedValue = strToEmployeeID;
            }

            txtProblemDetails.Text = string.Empty;
            string strProblemDetails = e.Row.Cells.FromKey("ProblemDetails").Value.ToString();
            txtProblemDetails.Text = strProblemDetails;

            txtDisposeEmployee.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeFullName").Value != null)
            {
                string strDisposeFullName = e.Row.Cells.FromKey("DisposeFullName").Value.ToString();
                txtDisposeEmployee.Text = strDisposeFullName;
            }

            txtProblemResult.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProblemResult").Value != null)
            {
                string strProblemResult = e.Row.Cells.FromKey("ProblemResult").Value.ToString();
                txtProblemResult.Text = strProblemResult;
            }

            txtPlannedDisposeDate.Value = string.Empty;
            if (e.Row.Cells.FromKey("PlannedDisposeDate").Value != null)
            {
                string strPlannedDisposeDate = e.Row.Cells.FromKey("PlannedDisposeDate").Value.ToString();
                txtPlannedDisposeDate.Value = Convert.ToDateTime(strPlannedDisposeDate).ToString("yyyy-MM-dd");
            }
            if (e.Row.Cells.FromKey("specid").Value != null)
            {
                ddlSpec.SelectedValue= e.Row.Cells.FromKey("specid").Value.ToString();
             
            }

            //ClearContainerDisp();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (CheckData() == false)
            {
                return;
            }

            string strProblemTitle = txtProblemTitle.Text.Trim();
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strSubmitEmployeeID = userInfo["EmployeeID"];
            string strFactoryID = userInfo["FactoryID"];
            string strContainerID = txtDispContainerID.Text;
            string strProductID = txtDispProductID.Text;
            string strSpecID = ddlSpec.SelectedValue;
            string strProblemTypeID = ddlProblemType.SelectedValue;
            string strToFactoryID = ddlToFactory.SelectedValue;
            string strToEmployeeID = ddlToEmployee.SelectedValue;
            string strProblemLevelID = ddlProblemLevel.SelectedValue;
            string strProblemDetails = txtProblemDetails.Text.Trim();
            string strPlannedDisposeDate = txtPlannedDisposeDate.Value;

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ProblemTitle", strProblemTitle);
            para.Add("ContainerID", strContainerID);
            para.Add("ProductID", strProductID);
            para.Add("SpecID", strSpecID);
            para.Add("ResourceID", "");
            para.Add("ProblemTypeID", strProblemTypeID);
            para.Add("FactoryID", strFactoryID);
            para.Add("ToFactoryID", strToFactoryID);
            para.Add("ToEmployeeID", strToEmployeeID);
            para.Add("ProblemLevelID", strProblemLevelID);
            para.Add("ProblemDetails", strProblemDetails);
            para.Add("Status", "0");
            para.Add("Notes", "");
            para.Add("PlannedDisposeDate", strPlannedDisposeDate);

            string strID = txtID.Text;
            if (strID == string.Empty)
            {
                strID = Guid.NewGuid().ToString();
                para.Add("ID", strID);
                para.Add("SubmitEmployeeID", strSubmitEmployeeID);
                para.Add("SubmitDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                problem.AddProblemInfo(para);

                txtID.Text = strID;
            }
            else
            {
                problem.UpdateProblemInfo(strID, para);
            }
            ////获取问题列表
            //para = new Dictionary<string, string>();
            //para.Add("ContainerID", strContainerID);
            //para.Add("SubmitEmployeeID", strSubmitEmployeeID);
            //para.Add("Status", "0,10");
            //DataTable DT = problem.GetAllDataForOutExcel(para);

            //wgProblem.DataSource = DT;
            //wgProblem.DataBind();

            //ClearProblemDisp();
     
            QueryData(GetQuery());
            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 数据验证
    protected Boolean CheckData()
    {
        string strStatus = txtStatus.Text;
        if (strStatus == "5")
        {
            DisplayMessage("问题已指派，不允许修改", false);
            return false;
        }
        if (strStatus == "10")
        {
            DisplayMessage("问题已处理，不允许修改", false);
            return false;
        }
        if (strStatus == "20")
        {
            DisplayMessage("问题已关闭，不允许修改", false);
            return false;
        }

        string strProblemTitle = txtProblemTitle.Text.Trim();
        if (strProblemTitle == string.Empty)
        {
            DisplayMessage("请输入标题", false);
            txtProblemTitle.Focus();
            return false;
        }

        string strProblemTypeID = ddlProblemType.SelectedValue;
        if (strProblemTypeID == string.Empty)
        {
            DisplayMessage("请选择问题类型", false);
            ddlProblemType.Focus();
            return false;
        }

        string strProblemLevelID = ddlProblemLevel.SelectedValue;
        if (strProblemLevelID == string.Empty)
        {
            DisplayMessage("请选择问题级别", false);
            ddlProblemLevel.Focus();
            return false;
        }

        string strToFactoryID = ddlToFactory.SelectedValue;
        if (strToFactoryID == string.Empty)
        {
            DisplayMessage("请选择要求处理部门", false);
            ddlToFactory.Focus();
            return false;
        }

        string strPlannedDisposeDate = txtPlannedDisposeDate.Value;
        if (strPlannedDisposeDate == string.Empty)
        {
            DisplayMessage("请指定要求解决时间", false);
            txtPlannedDisposeDate.Focus();
            return false;
        }

        string strProblemDetails = txtProblemDetails.Text.Trim();
        if (strProblemDetails == string.Empty)
        {
            DisplayMessage("请输入问题描述", false);
            txtProblemDetails.Focus();
            return false;
        }

        return true;
    }
    #endregion
    #endregion

    protected void btnNew_Click(object sender, EventArgs e)
    {
        ClearMessage();
        ClearContainerDisp();
        ClearProblemDisp();
        //txtID.Text = string.Empty;
    }

    #region 关闭按钮
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择要关闭的问题记录", false);
                return;
            }

            string strStatus = txtStatus.Text;
            if (strStatus != "10")
            {
                DisplayMessage("该问题未处理，不允许关闭", false);
                return;
            }

            problem.ChangeProblemStatus(strID, 20);

            QueryData(GetQuery());
            DisplayMessage("保存成功", true);

            ////获取问题列表
            //Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            //string strSubmitEmployeeID = userInfo["EmployeeID"];
            //string strContainerID = txtDispContainerID.Text;
            //Dictionary<string, string> para = new Dictionary<string, string>();
            //para.Add("ContainerID", strContainerID);
            //para.Add("SubmitEmployeeID", strSubmitEmployeeID);
            //para.Add("Status", "0,10");
            //DataTable DT = problem.GetAllDataForOutExcel(para);

            //wgProblem.DataSource = DT;
            //wgProblem.DataBind();

            //ClearProblemDisp();
        
        }
        catch(Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择要删除的问题记录", false);
                return;
            }

            string strStatus = txtStatus.Text;
            if (strStatus != "0")
            {
                DisplayMessage("该问题已指派或已处理，不允许删除", false);
                return;
            }

            problem.DeleteProblemInfo(strID);

          

            ////获取问题列表
            //Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            //string strSubmitEmployeeID = userInfo["EmployeeID"];
            //string strContainerID = txtDispContainerID.Text;
            //Dictionary<string, string> para = new Dictionary<string, string>();
            //para.Add("ContainerID", strContainerID);
            //para.Add("SubmitEmployeeID", strSubmitEmployeeID);
            //para.Add("Status", "0,10");
            //DataTable DT = problem.GetAllDataForOutExcel(para);

            //wgProblem.DataSource = DT;
            //wgProblem.DataBind();

            //ClearProblemDisp();
            QueryData(GetQuery());
            DisplayMessage("删除成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ddlToFactory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();

        ddlToEmployee.Items.Clear();
        ddlToEmployee.ClearSelection();
        try
        {
            string strFactoryID = ddlToFactory.SelectedValue;
            if (strFactoryID != string.Empty)
            {
                DataTable DT = common.GetEmployeeByFactory(strFactoryID);

                ddlToEmployee.DataTextField = "FullName";
                ddlToEmployee.DataValueField = "EmployeeID";
                ddlToEmployee.DataSource = DT;
                ddlToEmployee.DataBind();

                ddlToEmployee.Items.Insert(0, string.Empty);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ddlSearchFactory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();

        ddlSearchEmployee.Items.Clear();

        try
        {
            string strFactoryID = ddlSearchFactory.SelectedValue;
            if (strFactoryID != string.Empty)
            {
                DataTable DT = common.GetEmployeeByFactory(strFactoryID);

                ddlSearchEmployee.DataTextField = "FullName";
                ddlSearchEmployee.DataValueField = "EmployeeID";
                ddlSearchEmployee.DataSource = DT;
                ddlSearchEmployee.DataBind();

                ddlSearchEmployee.Items.Insert(0, string.Empty);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        try
        {
            BindContainerInfo(ipContainer.Value);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}