﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="MfgSetUpForm.aspx.cs" Inherits="MfgSetUpForm" EnableViewState="true" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function IsSetUp() {
            if (confirm("确定要强制准备完成选中的任务吗？")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="10" class="tdBottom">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">图号/名称：</div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工序：</div>
                        <asp:TextBox ID="txtSpecName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" nowrap="nowrap" class="tdRight">
                        <div class="divLabel">要求完成日期：</div>
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div id="ItemDiv" runat="server">
            <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="300px" Width="100%" OnActiveRowChange="ItemGrid_ActiveRowChange" OnDataBound="ItemGrid_DataBound">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect">
                                <CellTemplate>
                                    <asp:CheckBox ID="ckSelect" runat="server" />
                                </CellTemplate>
                                <Header Caption=""></Header>
                            </igtbl:TemplatedColumn>
                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                <Header Caption="工作令号">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="150px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="作业令号">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                <Header Caption="批次号">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                <Header Caption="图号">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                <Header Caption="名称">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                <Header Caption="派工数量">
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="SpecNameDisp" AllowGroupBy="No" BaseColumnName="SpecNameDisp" Width="80px">
                                <Header Caption="派工工序">
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="ResourceName" BaseColumnName="ResourceName" Width="150px">
                                <Header Caption="设备/工位">
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="TeamName" BaseColumnName="TeamName" Width="150px" Hidden="True">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="9"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="PlannedCompletionDate" AllowGroupBy="No" BaseColumnName="PlannedCompletionDate" Width="120px" DataType="System.DateTime" Format="yyyy-MM-dd">
                                <Header Caption="班组">
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ID" Key="ID" Hidden="True">
                                <Header Caption="要求完成日期">
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SpecName" Hidden="True" Key="SpecName">
                                <Header Caption="ID">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerQty" Hidden="True" Key="ContainerQty">
                                <Header Caption="SpecName">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerStartDate" Hidden="True" Key="ContainerStartDate">
                                <Header Caption="ContainerQty">
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerCompletionDate" Hidden="True" Key="ContainerCompletionDate">
                                <Header Caption="ContainerStartDate">
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="TeamID" Hidden="True" Key="TeamID">

                                <Header Caption="ContainerCompletionDate">
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                <Header Caption="TeamID">
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Hidden="True" Key="WorkflowID">
                                <Header Caption="ContainerID">
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SpecID" Hidden="True" Key="SpecID">
                                <Header Caption="WorkflowID">
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ParentID" Hidden="True" Key="ParentID">
                                <Header Caption="SpecID">
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                                <Header Caption="ParentID">
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ResourceID" Hidden="True" Key="ResourceID">
                                <Header Caption="ResourceID">
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ParentQty" Hidden="True" Key="ParentQty">
                                <Header Caption="ParentQty">
                                    <RowLayoutColumnInfo OriginX="22" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="22" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Documents" Hidden="True" Key="Documents">
                                <Header Caption="Documents">
                                    <RowLayoutColumnInfo OriginX="23" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="23" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Tools" Hidden="True" Key="Tools">
                                <Header Caption="Tools">
                                    <RowLayoutColumnInfo OriginX="24" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="24" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="CuttingTools" Hidden="True" Key="CuttingTools">
                                <Header Caption="CuttingTools">
                                    <RowLayoutColumnInfo OriginX="25" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="25" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="NCProgram" Hidden="True" Key="NCProgram">
                                <Header Caption="NCProgram">
                                    <RowLayoutColumnInfo OriginX="26" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="26" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Resources" Hidden="True" Key="Resources">
                                <Header Caption="Resources">
                                    <RowLayoutColumnInfo OriginX="27" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="27" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Material" Hidden="True" Key="Material">
                                <Header Caption="Material">
                                    <RowLayoutColumnInfo OriginX="28" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="28" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SetUpID" Hidden="True" Key="SetUpID">
                                <Header Caption="SetUpID">
                                    <RowLayoutColumnInfo OriginX="29" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="29" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px" AllowRowNumberingDefault="Continuous">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: right;">
                        <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                            OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                        Text="第  页  共  页"></asp:Label>
                        <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                        <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                        <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                        <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                            OnClick="btnGo_Click" />
                        <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                            Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;"
                            Visible="False"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom" style=" display:none">
                        <div class="divLabel">作业令号：</div>
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispContainerID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">图号：</div>
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdBottom" colspan="2">
                        <div class="divLabel">名称：</div>
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRight">
                        <div class="divLabel">派工数量：</div>
                        <asp:TextBox ID="txtDispQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工序：</div>
                        <asp:TextBox ID="txtDispSpecName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispWorkflowID" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtDispSpecID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">班组：</div>
                        <asp:TextBox ID="txtDispTeamName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispTeamID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">设备/工位：</div>
                        <asp:TextBox ID="txtDispResourceName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispResourceID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdNoBorder">
                        <div class="divLabel">要求完成日期：</div>
                        <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispID" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtDispParentID" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtParentQty" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtDispatchedQty" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtDispSetUpID" runat="server" Visible="False"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <igtbl:UltraWebGrid ID="wgEmployee" runat="server" Height="300px" Width="170px" OnDataBound="wgDispatchList_DataBound">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn Width="30px" AllowGroupBy="No" Key="ckSelect" Hidden="True">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption=""></Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn Key="FullName" Width="100px" BaseColumnName="FullName" AllowGroupBy="No">
                                            <Header Caption="人员">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="EmployeeName" Key="EmployeeName" Hidden="True">
                                            <Header Caption="EmployeeID">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="EmployeeID" Key="EmployeeID" Hidden="True">
                                            <Header Caption="EmployeeName">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="170px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td>
                        <igtbl:UltraWebGrid ID="wgProductNo" runat="server" Height="300px" Width="170px" OnDataBound="wgDispatchList_DataBound">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn Width="30px" AllowGroupBy="No" Key="ckSelect" Hidden="True">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption=""></Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn Key="ProductNo" Width="100px" BaseColumnName="ProductNo" AllowGroupBy="No">
                                            <Header Caption="产品序号">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Hidden="True">
                                            <Header Caption="ContainerName">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerID" Key="ContainerID" Hidden="True">
                                            <Header Caption="ContainerID">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="170px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td style="width: 100%;">
                        <div>
                            <igtbl:UltraWebGrid ID="wgDispatchList" runat="server" Height="300px" Width="100%" OnDataBound="wgDispatchList_DataBound">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                                <Header Caption="工作令号">
                                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                </Header>

                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                                <Header Caption="批次号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                                <Header Caption="图号">
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                                <Header Caption="名称">
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                                <Header Caption="数量">
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="SpecNameDisp" AllowGroupBy="No" BaseColumnName="SpecNameDisp" Width="80px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                                </Header>

                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="PlannedCompletionDate" Key="PlannedCompletionDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                                <Header Caption="要求完成日期">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="SpecName" Hidden="True" Key="SpecName">
                                                <Header Caption="SpecName">
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                    RowHeightDefault="18px">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                        StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Top="2px" Bottom="3px"></Padding>
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td class="tdRight" align="left" nowrap="nowrap">
                        <asp:CheckBox ID="cbDocuments" runat="server" Text="工艺图纸准备" />
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">
                        <asp:CheckBox ID="cbTools" runat="server" Text="工装工具准备" />
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">
                        <asp:CheckBox ID="cbCuttingTools" runat="server" Text="刀具准备" />
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">
                        <asp:CheckBox ID="cbNCProgram" runat="server" Text="NC程序准备" />
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">
                        <asp:CheckBox ID="cbResources" runat="server" Text="设备状态" />
                    </td>
                    <td class="tdNoBorder" colspan="2">
                        <asp:CheckBox ID="cbMaterial" runat="server" Text="物料准备" />
                    </td>
                </tr>
            </table>
        </div>


        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;" colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="保存"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
                        <asp:Button ID="btnSetUp" runat="server" Text="强制准备完成"
                            CssClass="searchButton" EnableTheming="True" OnClientClick="return IsSetUp();" OnClick="btnSetUp_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
