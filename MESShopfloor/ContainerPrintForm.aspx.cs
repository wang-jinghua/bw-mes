﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.Configuration;
using System.Web.UI;
using uMES.LeanManufacturing.DBUtility;
using uMES.LeanManufacturing.DBUtility;

public partial class ContainerPrintForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "ContainerPrintForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();

    public object Databasehelper { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：过程检验流程卡打印";
        master.strTitle = "过程检验流程卡打印";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        
    }
    #endregion

    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        try
        {
            uMESPagingDataDTO result = bll.GetContainerPrintData(query, Convert.ToInt32(this.txtCurrentPage.Text), 15);
            this.ItemGrid.DataSource = result.DBTable;
            this.ItemGrid.DataBind();
            this.txtTotalPage.Text = result.PageCount;
            if (result.RowCount == "0")
            {
                this.txtCurrentPage.Text = "0";
            }
            lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
            this.txtPage.Text = this.txtCurrentPage.Text;
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 打印按钮
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        ClearMessage();
       var userInfo = Session["UserInfo"] as Dictionary<string, string>;
        try
        {
            int intCount = 0;
            List<string> listWorkflowID = new List<string>();

            TemplatedColumn temCell = (TemplatedColumn)ItemGrid.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    string strWorkflowID = ItemGrid.Rows[i].Cells.FromKey("WorkflowID").Value.ToString();
                    listWorkflowID.Add(strWorkflowID);
                    intCount++;
                }
            }

            if (intCount == 0)
            {
                DisplayMessage("请选择要打印的批次", false);
                return;
            }

            //获取工序列表
            DataTable DT = common.GetSpecListByWorkflowID(listWorkflowID);

            //创建PDF文档
            Document document = new Document(PageSize.A4, 40, 40, 50, 50);
            string strPath = Server.MapPath(Request.ApplicationPath) + ConfigurationManager.AppSettings["PDFFilePath"];
            string strFileName = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff") + ".pdf";
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(strPath + strFileName, FileMode.Create));
            document.Open();

            BaseFont baseFont = BaseFont.CreateFont("C:\\WINDOWS\\FONTS\\simfang.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            
            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    int intPageNumber = 1;

                    document.NewPage();
                    
                    document.Add(pdfMainTable(baseFont, i));

                    #region 批次号二维码
                    string strContainerName = ItemGrid.Rows[i].Cells.FromKey("ContainerName").Value.ToString();
                    string strImgName = codeContainerName(strContainerName);

                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(strImgName);
                    img.SetAbsolutePosition(95, 690);
                    writer.DirectContent.AddImage(img);

                    PdfContentByte cb = writer.DirectContent;
                    iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 16);
                    Phrase txt = new Phrase("过程检验流程卡", font);
                    ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 75, 775, 0);
                    #endregion

                    PdfPTable table = pdfSpecListTable(baseFont);
                    
                    string strWorkflowID = ItemGrid.Rows[i].Cells.FromKey("WorkflowID").Value.ToString();
                    DataRow[] rowsSpecList = DT.Select(string.Format("WorkflowID = '{0}'", strWorkflowID), "Sequence ASC");

                    for (int j = 0; j < rowsSpecList.Length; j++)
                    {
                        string strSpecNo = common.GetSpecNoFromSpecName(rowsSpecList[j]["SpecName"].ToString());
                        string strSpecName = common.GetSpecNameFromSpecName(rowsSpecList[j]["SpecName"].ToString());

                        if (j < 10)
                        {
                            PdfPCell cell = new PdfPCell(new Phrase(strSpecNo, font));
                            cell.MinimumHeight = 40;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            table.AddCell(cell);
                            
                            cell = new PdfPCell(new Phrase(strSpecName, font));
                            cell.MinimumHeight = 40;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            table.AddCell(cell);

                            for (int k = 1; k <= 6; k++)
                            {
                                cell = new PdfPCell(new Phrase("", font));
                                cell.MinimumHeight = 40;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                table.AddCell(cell);
                            }
                        }
                        else
                        {
                            if ((j - 10) % 17 == 0)
                            {
                                document.Add(table);

                                //页脚显示的位置 
                                cb = writer.DirectContent;
                                txt = new Phrase(string.Format("第{0}页", intPageNumber), font);
                                ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER, txt, 200, 30, 0);

                                document.NewPage();

                                table = pdfSpecListTable(baseFont);
                            }

                            PdfPCell cell = new PdfPCell(new Phrase(strSpecNo, font));
                            cell.MinimumHeight = 40;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            table.AddCell(cell);

                            cell = new PdfPCell(new Phrase(strSpecName, font));
                            cell.MinimumHeight = 40;
                            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            table.AddCell(cell);

                            for (int k = 1; k <= 6; k++)
                            {
                                cell = new PdfPCell(new Phrase("", font));
                                cell.MinimumHeight = 40;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                table.AddCell(cell);
                            }
                        }
                    }

                    if (rowsSpecList.Length <= 10)
                    {
                        for (int j = 1; j <= 10 - rowsSpecList.Length; j++)
                        {
                            for (int k = 1; k <= 8; k++)
                            {
                                PdfPCell cell = new PdfPCell(new Phrase("", font));
                                cell.MinimumHeight = 40;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                table.AddCell(cell);
                            }
                        }

                        //页脚显示的位置 
                        cb = writer.DirectContent;
                        txt = new Phrase(string.Format("第{0}页", intPageNumber), font);
                        ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER, txt, 200, 30, 0);

                        document.Add(table);
                    }
                    else
                    {
                        for (int j = 1; j <= 17 - (rowsSpecList.Length - 10) % 17; j++)
                        {
                            for (int k = 1; k <= 8; k++)
                            {
                                PdfPCell cell = new PdfPCell(new Phrase("", font));
                                cell.MinimumHeight = 40;
                                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                table.AddCell(cell);
                            }
                        }
                        //页脚显示的位置 
                        cb = writer.DirectContent;
                        txt = new Phrase(string.Format("第{0}页", intPageNumber +  1), font);
                        ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER, txt, 200, 30, 0);

                        document.Add(table);
                        intPageNumber++;
                    }
                    #region 单数页补齐
                    //如果是单数页，则新增一页，防止双面打印时，不同的批次信息打印到一张纸上
                    //if (intPageNumber % 2 == 1)
                    //{
                    //    document.NewPage();

                    //    table = pdfSpecListTable(baseFont);

                    //    for (int j = 1; j <= 17; j++)
                    //    {
                    //        for (int k = 1; k <= 8; k++)
                    //        {
                    //            PdfPCell cell = new PdfPCell(new Phrase("", font));
                    //            cell.MinimumHeight = 40;
                    //            cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    //            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    //            table.AddCell(cell);
                    //        }
                    //    }
                    //    //页脚显示的位置 
                    //    cb = writer.DirectContent;
                    //    txt = new Phrase(string.Format("第{0}页", intPageNumber + 1), font);
                    //    ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER, txt, 200, 30, 0);

                    //    document.Add(table);
                    //}
                    #endregion
                    //记录点击次数
                    SaveClickPrintNum(ItemGrid.Rows[i].Cells.FromKey("ContainerID").Text, ItemGrid.Rows[i].Cells.FromKey("ContainerName").Text,userInfo);
                }
            }

            document.Close();
            writer.Close();

            string strScript = "<script>window.open('ExportFile/" + strFileName + "');</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
            QueryData(GetQuery());
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 生成批次号二维码
    /// <summary>
    /// 生成批次号二维码
    /// </summary>
    /// <param name="strContainerName"></param>
    /// <returns></returns>
    protected string codeContainerName(string strContainerName)
    {
        string strCode = strContainerName;
        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        QRCodeData qrCodeData = qrGenerator.CreateQrCode(strCode, QRCodeGenerator.ECCLevel.Q);
        QRCode qrcode = new QRCode(qrCodeData);

        Bitmap qrCodeImage = qrcode.GetGraphic(3, Color.Black, Color.White, null, 5, 1, false);
        MemoryStream ms = new MemoryStream();
        string strPath = ConfigurationManager.AppSettings["FileTemp"];
        string strImgName = strPath + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff") + ".jpg";
        qrCodeImage.Save(strImgName);

        return strImgName;
    }
    #endregion

    #region 工序列表表头
    /// <summary>
    /// 工序列表表头
    /// </summary>
    /// <param name="baseFont"></param>
    /// <returns></returns>
    protected PdfPTable pdfSpecListTable(BaseFont baseFont)
    {
        PdfPTable table = new PdfPTable(8);
        table.TotalWidth = 500;
        table.LockedWidth = true;

        iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 12);
        PdfPCell cell = new PdfPCell(new Phrase("序号", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("工序", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("自检", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("互检", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("首检", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("专检", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("合格数量", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("日期", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        return table;
    }
    #endregion

    #region 过程检验流程卡主表格
    /// <summary>
    /// 过程检验流程卡主表格
    /// </summary>
    /// <param name="baseFont"></param>
    /// <returns></returns>
    protected PdfPTable pdfMainTable(BaseFont baseFont, int intRowIndex)
    {
        PdfPTable table = new PdfPTable(new float[] { 70, 96, 70, 96, 70, 98 });
        table.TotalWidth = 500;
        table.LockedWidth = true;

        PdfPCell cell;
        string strContainerName = ItemGrid.Rows[intRowIndex].Cells.FromKey("ContainerName").Value.ToString();
        iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 12);
        cell = new PdfPCell(new Phrase(strContainerName, font));
        cell.Colspan = 2;
        cell.Rowspan = 3;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_BOTTOM;
        table.AddCell(cell);

        font = new iTextSharp.text.Font(baseFont, 12);
        cell = new PdfPCell(new Phrase("工作令号", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        string strProcessNo = string.Empty;
        try
        {
            strProcessNo = ItemGrid.Rows[intRowIndex].Cells.FromKey("ProcessNo").Value.ToString();
        }
        catch { }
        cell = new PdfPCell(new Phrase(strProcessNo, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        cell = new PdfPCell(new Phrase("作业令号", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        string strOprNo = string.Empty;
        try
        {
            strOprNo = ItemGrid.Rows[intRowIndex].Cells.FromKey("OprNo").Value.ToString();
        }
        catch { }
        cell = new PdfPCell(new Phrase(strOprNo, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        font = new iTextSharp.text.Font(baseFont, 12);
        cell = new PdfPCell(new Phrase("零件名称", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        string strProductDesc = string.Empty;
        try
        {
            strProductDesc = ItemGrid.Rows[intRowIndex].Cells.FromKey("Description").Value.ToString();
        }
        catch { }
        cell = new PdfPCell(new Phrase(strProductDesc, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("零件图号", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        
        string strProductName = ItemGrid.Rows[intRowIndex].Cells.FromKey("ProductName").Value.ToString();
        cell = new PdfPCell(new Phrase(strProductName, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        var compRe = GetComponentProduct(strProductName,strProcessNo);
        string componentName = "", componentDesc = "";
        componentName = compRe.Item1;
        componentDesc = compRe.Item2;

        font = new iTextSharp.text.Font(baseFont, 12);
        cell = new PdfPCell(new Phrase("整件名称", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase(componentDesc, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("整件图号", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase(componentName, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        font = new iTextSharp.text.Font(baseFont, 12);
        cell = new PdfPCell(new Phrase("材料名称", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        string strMaterialName = string.Empty;
        try
        {
            strMaterialName = ItemGrid.Rows[intRowIndex].Cells.FromKey("MaterialName").Value.ToString();
        }
        catch { }
        cell = new PdfPCell(new Phrase(strMaterialName, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("材料牌号", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        string strMaterialPaiHao = string.Empty;
        try
        {
            strMaterialPaiHao = ItemGrid.Rows[intRowIndex].Cells.FromKey("MaterialPaiHao").Value.ToString();
        }
        catch { }
        cell = new PdfPCell(new Phrase(strMaterialPaiHao, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("材料规格", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        string strMaterialGuiGe = string.Empty;
        try
        {
            strMaterialGuiGe = ItemGrid.Rows[intRowIndex].Cells.FromKey("MaterialGuiGe").Value.ToString();
        }
        catch { }
        cell = new PdfPCell(new Phrase(strMaterialGuiGe, font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        cell = new PdfPCell(new Phrase("原材料检验", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("", font));
        cell.Colspan = 2;
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("日期", font));
        cell.Colspan = 2;
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        string sizeofblank = string.Empty;
        if (ItemGrid.Rows[intRowIndex].Cells.FromKey("sizeofblank").Value!=null)
        {
            sizeofblank = ItemGrid.Rows[intRowIndex].Cells.FromKey("sizeofblank").Value.ToString();
        }
        cell = new PdfPCell(new Phrase("规格尺寸", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase(sizeofblank, font)); 
        cell.Colspan = 2;
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("批次数量", font));
        cell.Colspan = 2;
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase(ItemGrid.Rows[intRowIndex].Cells.FromKey("Qty").Text , font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        cell = new PdfPCell(new Phrase("下料检验", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("", font));
        cell.Colspan = 2;
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("日期", font));
        cell.Colspan = 2;
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);
        cell = new PdfPCell(new Phrase("", font));
        cell.MinimumHeight = 40;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        return table;
    }
    #endregion

    #endregion

    #region 保存点击按钮次数
    void SaveClickPrintNum(string containerid,string containername, Dictionary<string, string> userinfo) {
        common.ExecuteDataByEntity(new ExcuteEntity("ContainerPrintFrequency", ExcuteType.insert) {ExcuteFileds=new List<FieldEntity>() {
               new FieldEntity("CONTAINERPRINTFREQUENCYID",Guid.NewGuid().ToString() , FieldType.Str),
            new FieldEntity("CONTAINERID",containerid , FieldType.Str),new FieldEntity("containername",containername , FieldType.Str),new FieldEntity("OPERATEEMP",userinfo["EmployeeID"] , FieldType.Str)
            , new FieldEntity("OPERATEDATE",DateTime.Now , FieldType.Date)
        } });
    }
    #endregion

    #region"获取整件图号和名称"
    Tuple<string, string> GetComponentProduct(string productname,string processno)
    {
        string strSql =string.Format( @"select p.productid,pb2.productname,p.description from mfgorder m
left join productbase pb on pb.productbaseid=m.productbaseid
left join product p on p.productid=nvl(pb.revofrcdid,m.productid)
left join productbase pb2 on pb2.productbaseid=p.productbaseid
where m.processno='{0}'
order by pb2.productname asc", processno);
        var dt= OracleHelper.Query(strSql).Tables[0];
        DataRow dr = dt.Rows[0];

        return Tuple.Create<string,string>(dr["productname"].ToString (),dr["description"].ToString ());
    }

    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        if (btnSelectAll.Text == "全选")
        {
            btnSelectAll.Text="全不选";
           uMESCommonFunction.ResetGridCheckStatus2(ItemGrid, "ckSelect",0);
        }
        else if (btnSelectAll.Text == "全不选")
        {
            btnSelectAll.Text = "全选";
            uMESCommonFunction.ResetGridCheckStatus2(ItemGrid, "ckSelect", 2);
        }

    }

    protected void btnInRevert_Click(object sender, EventArgs e)
    {
        uMESCommonFunction.ResetGridCheckStatus2(ItemGrid, "ckSelect",1);
    }
}