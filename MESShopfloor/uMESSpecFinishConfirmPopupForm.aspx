﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESSpecFinishConfirmPopupForm.aspx.cs"
    ValidateRequest="false" Inherits="uMESSpecFinishConfirmPopupForm" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="uMESCustomControls/ToleranceInput/wucToleranceInput.ascx" TagName="wucToleranceInput" TagPrefix="uc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>完工确认</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
     <script type="text/javascript" src="/MESShopfloor/Scripts/Customer.js"></script>
</head>
<body scroll="no">
    <form id="form1" runat="server">

        <%--        <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" RefreshTargetIDs="WebAsyncRefreshPanel2" Width="1064px">--%>
        <div style="height: 700px; overflow: scroll;">
            <table border="0" style="padding-top: 220px;">

                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label3" runat="server" Text=" 工作令号" Font-Bold="true" Font-Size="13pt" ForeColor="Black"></asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" class="tdRightAndBottom" style=" display:none">
                        <div style="width: 100%;">
                            <asp:Label ID="Label7" runat="server" Text="作业令号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label8" runat="server" Text="批次号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label9" runat="server" Text="图号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label10" runat="server" Text="名称" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label11" runat="server" Text="批次数量" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtConQty" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"
                            Font-Size="Medium" Width="80px"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label12" runat="server" Text="计划开始时间" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtStartTime" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label1" runat="server" Text="计划结束时间" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtEndTime" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                    </td>
                </tr>
                <tr style="height: 1px;">
                    <td style="width: 100%" colspan="8">派工信息</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <igtbl:UltraWebGrid ID="wgDispatch" runat="server" Height="120px" Width="100%">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn Key="SpecNameDisp" Width="150px" BaseColumnName="SpecNameDisp" AllowGroupBy="No">
                                            <Header Caption="派工工序">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="unitworktime" Key="unitworktime" Width="80px">
                                            <Header Caption="单件工时">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="totalworktime" Key="totalworktime" Width="80px">
                                            <Header Caption="工时总和">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="qty" Key="qty" Width="150px" AllowGroupBy="No" Hidden="false">
                                            <Header Caption="派工数量">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="teamname" Key="teamname" Width="120px">
                                            <Header Caption="班组">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="resourcename" Key="resourcename">
                                            <Header Caption="工位/设备">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="deName" Key="deName">
                                            <Header Caption="加工人员">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>

                                        <igtbl:UltraGridColumn BaseColumnName="plannedcompletiondate" Hidden="false" Key="plannedcompletiondate"
                                            DataType="Date" Format="yyyy-MM-dd" Width="120px">
                                            <Header Caption="要求完成时间">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                            <Header Caption="派工人">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="dispatchdate" Hidden="false" Key="dispatchdate"
                                            DataType="System.DateTime" Format="yyyy-MM-dd HH:mm" Width="130px">
                                            <Header Caption="派工时间">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="DispType" Key="DispType" Width="120px">
                                            <Header Caption="派工类型">
                                                <RowLayoutColumnInfo OriginX="12" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="12" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="DispatchType" Hidden="true" Key="DispatchType" Width="120px">
                                            <Header Caption="DispatchType">
                                                <RowLayoutColumnInfo OriginX="13" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="13" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="100%">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="120px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle" Wrap="true">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
                <tr style="height: 1px;">
                    <td style="width: 100%" colspan="8">报工信息</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <igtbl:UltraWebGrid ID="wgReport" runat="server" Height="80px">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn Key="SpecNameDisp" Width="150px" BaseColumnName="SpecNameDisp" AllowGroupBy="No">
                                            <Header Caption="报工工序">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="unitworktime" Key="unitworktime" Width="80px">
                                            <Header Caption="单件工时">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="totalworktime" Key="totalworktime" Width="80px">
                                            <Header Caption="工时总和">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="qty" Key="qty" Width="150px" AllowGroupBy="No" Hidden="false">
                                            <Header Caption="报工数量">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="teamname" Hidden="false" Key="teamname" Width="120px">
                                            <Header Caption="班组">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="resourcename" Hidden="false" Key="resourcename">
                                            <Header Caption="工位/设备">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname">
                                            <Header Caption="加工人员">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>

                                        <igtbl:UltraGridColumn BaseColumnName="ReportDate" Key="ReportDate"
                                            DataType="System.DateTime" Format="yyyy-MM-dd HH:mm" Width="130px">
                                            <Header Caption="报工时间">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>

                                        <igtbl:UltraGridColumn BaseColumnName="rType" Hidden="false" Key="rType" Width="120px">
                                            <Header Caption="报工类型">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="100%">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="120px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle" Wrap="true">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
                <tr style="height: 1px;">
                    <td style="width: 100%" colspan="8">检验信息</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <igtbl:UltraWebGrid ID="wgCheck" runat="server" Height="120px" Width="100%">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn Key="SpecNameDisp" Width="150px" BaseColumnName="SpecNameDisp" AllowGroupBy="No">
                                            <Header Caption="工序">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="unitworktime" Key="unitworktime" Width="80px">
                                            <Header Caption="单件工时">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="totalworktime" Key="totalworktime" Width="80px">
                                            <Header Caption="工时总和">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="reportQty" Key="reportQty" Width="80px" AllowGroupBy="No" Hidden="false">
                                            <Header Caption="报工数量">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="reporter" Hidden="false" Key="reporter" Width="100px">
                                            <Header Caption="报工人">
                                                <RowLayoutColumnInfo OriginX="3" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="3" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="reportdate" Hidden="false" Key="reportdate" Width="130px" DataType="System.DateTime" Format="yyyy-MM-dd HH:mm">
                                            <Header Caption="报工时间">
                                                <RowLayoutColumnInfo OriginX="4" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="4" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="EligibilityQty" Hidden="False" Key="EligibilityQty">
                                            <Header Caption="合格数量">
                                                <RowLayoutColumnInfo OriginX="5" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="5" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="NonsenseQty" Hidden="false" Key="NonsenseQty" Width="100px">
                                            <Header Caption="不合格数量">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="checktype" Hidden="false" Key="checktype" Width="100px">
                                            <Header Caption="检验类型">
                                                <RowLayoutColumnInfo OriginX="7" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="7" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="checker" Hidden="false" Key="checker" Width="120px">
                                            <Header Caption="检验人">
                                                <RowLayoutColumnInfo OriginX="8" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="8" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="checkdate" Hidden="false" Key="checkdate"
                                            DataType="System.DateTime" Format="yyyy-MM-dd HH:mm" Width="130px">
                                            <Header Caption="检验时间">
                                                <RowLayoutColumnInfo OriginX="9" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="9" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ScrapQty" Hidden="True" Key="ScrapQty" Width="120px">
                                            <Header Caption="报废数">
                                                <RowLayoutColumnInfo OriginX="10" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="10" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="100%">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="120px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle" Wrap="true">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
                <tr style="height: 1px;">
                    <td style="width: 100%" colspan="8">质量记录</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <igtbl:UltraWebGrid ID="wgQual" runat="server" Height="80px">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn Key="qualityrecordinfoname" Width="150px" BaseColumnName="qualityrecordinfoname" AllowGroupBy="No">
                                            <Header Caption="质量记录单号">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SpecDisName" Key="SpecDisName" Width="150px" AllowGroupBy="No" Hidden="false">
                                            <Header Caption="工序">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="unitworktime" Key="unitworktime" Width="80px">
                                            <Header Caption="单件工时">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="totalworktime" Key="totalworktime" Width="80px">
                                            <Header Caption="工时总和">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                            <Header Caption="不合格数量">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="disissubmit" Hidden="false" Key="disissubmit" Width="170px">
                                            <Header Caption="是否提交不合格审理">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>

                                        <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                            <Header Caption="检验人">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SubmitDate" Hidden="false" Key="SubmitDate"
                                            DataType="System.DateTime" Format="yyyy-MM-dd HH:mm" Width="130px">
                                            <Header Caption="记录时间">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>


                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="100%">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="120px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle" Wrap="true">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
                <tr style="height: 1px;">
                    <td style="width: 100%" colspan="8">不合格品审理单</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <igtbl:UltraWebGrid ID="wgUnQualified" runat="server" Height="80px">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn Key="rejectappinfoname" Width="150px" BaseColumnName="rejectappinfoname" AllowGroupBy="No">
                                            <Header Caption="不合格品审理单号">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SpecNameDisp" Key="SpecNameDisp" Width="150px" AllowGroupBy="No" Hidden="false">
                                            <Header Caption="工序">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="unitworktime" Key="unitworktime" Width="80px">
                                            <Header Caption="单件工时">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="totalworktime" Key="totalworktime" Width="80px">
                                            <Header Caption="工时总和">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="true" Key="qty" Width="100px">
                                            <Header Caption="不合格数量">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>

                                        <igtbl:UltraGridColumn BaseColumnName="rbqty" Hidden="true" Key="rbqty">
                                            <Header Caption="让步使用">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="fixqty" Hidden="false" Key="fixqty" Width="120px">
                                            <Header Caption="返工返修数量">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="scrapqty" Hidden="false" Key="scrapqty" Width="100px">
                                            <Header Caption="报废数量">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SubmitDate" Hidden="false" Key="SubmitDate"
                                            DataType="System.DateTime" Format="yyyy-MM-dd HH:mm" Width="120px">
                                            <Header Caption="检验时间">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>

                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="100%">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="120px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle" Wrap="true">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
                <tr style="height: 1px;">
                    <td style="width: 100%" colspan="8">报废信息</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <igtbl:UltraWebGrid ID="wgScrapInfo" runat="server" Height="80px">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn Key="ScrapInfoName" Width="150px" BaseColumnName="ScrapInfoName" AllowGroupBy="No">
                                            <Header Caption="报废单号">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SpecNameDisp" Key="SpecNameDisp" Width="150px" AllowGroupBy="No" Hidden="false">
                                            <Header Caption="工序">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="unitworktime" Key="unitworktime" Width="80px">
                                            <Header Caption="单件工时">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="totalworktime" Key="totalworktime" Width="80px">
                                            <Header Caption="工时总和">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                            <Header Caption="报废数量">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="lossreasonname" Hidden="false" Key="lossreasonname" Width="150px">
                                            <Header Caption="报废原因">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="QualityRecordInfoID" Hidden="true" Key="QualityRecordInfoID">
                                            <Header Caption="不合格审理单号">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                            <Header Caption="记录人">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SubmitDate" Hidden="false" Key="SubmitDate"
                                            DataType="System.DateTime" Format="yyyy-MM-dd HH:mm" Width="120px">
                                            <Header Caption="记录时间">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>


                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="100%">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="120px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle" Wrap="true">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
            </table>

        </div>
        <div style="position: absolute; top: 700px;">
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: left; width: 100%;">
                                    <asp:Button ID="btnConfirm" runat="server" Text="确定" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                                        CssClass="searchButton" EnableTheming="True" OnClick="btnConfirm_Click" />

                                </td>

                                <td style="padding-left: 500px;">
                                    <asp:Button ID="btnClose" runat="server" Text="关闭"
                                        CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="font-size: 12px; font-weight: bold;" nowrap="nowrap">状态信息：</td>
                                <td style="text-align: left; width: 100%;">
                                    <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="display: none; visibility: hidden">
            <!--跟踪卡ID-->
            <asp:TextBox ID="txtContainerID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--工序ID-->
            <asp:TextBox ID="txtSpecID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--计量单位ID-->
            <asp:TextBox ID="txtUomID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--工艺规程编号 -->
            <asp:TextBox ID="txtWorkflowID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--生产订单 -->
            <asp:TextBox ID="txtMfgOrderName" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--分厂ID -->
            <asp:TextBox ID="txtFactoryID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--报工数 -->
            <asp:TextBox ID="txtReportQty" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--派工数 -->
            <asp:TextBox ID="txtDispatchQty" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--工序名称 -->
            <asp:TextBox ID="txtSpecName" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>

            <div>
                <asp:Label ID="Label14" runat="server" Text="备注" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
            </div>
            <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" Height="200px" Width="120px"></asp:TextBox>
        </div>
        <%--     </igmisc:WebAsyncRefreshPanel>--%>
    </form>
</body>
</html>
