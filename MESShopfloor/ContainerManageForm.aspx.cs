﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;

public partial class ContainerManageForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "ContainerManageForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();
    uMESPartReportingBusiness partReport = new uMESPartReportingBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：批次管理";
        master.strTitle = "批次管理";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ScanContainerName", strScanContainerName);
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        Session[QueryWhere] = result;

        return result;
    }

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        uMESPagingDataDTO result = bll.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 10);
        result.DBTable.Columns.Add("constatus");
        for (int i=0;i<result.DBTable.Rows.Count;i++)
        {
            result.DBTable.Rows[i]["constatus"] = "正常";

            if (result.DBTable.Rows[i]["holdreasonid"].ToString() !="")
            {
                result.DBTable.Rows[i]["constatus"] = "暂停";
            }
            
            if (result.DBTable.Rows[i]["status"].ToString() =="2")
            {
                result.DBTable.Rows[i]["constatus"] = "关闭";
            }
        }

        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;

        ClearDispData();

        if (Session["ContainerMessage"] !=null)
        {
            DisplayMessage(Session["ContainerMessage"].ToString(),true);
            Session["ContainerMessage"] = null;
        }
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;
        
        txtTeamID.Text = string.Empty;
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if(e.Row.Cells.FromKey("ProcessNo").Value!=null)
            { 
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            txtDispContainerName.Text = strContainerName;

            string strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            txtDispContainerID.Text = strContainerID;

            string strWorkflowID = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            txtDispWorkflowID.Text = strWorkflowID;

            string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            txtDispProductName.Text = strProductName;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }

            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            txtDispPlannedStartDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("PlannedStartDate").Value != null)
            {
                string strPlannedStartDate = e.Row.Cells.FromKey("PlannedStartDate").Value.ToString();
                txtDispPlannedStartDate.Text = strPlannedStartDate;
            }

            txtDispPlannedCompletionDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Value.ToString();
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
            }

            txtDispChildCount.Text = e.Row.Cells.FromKey("ChildCount").Value.ToString();
            txtDispWorkflowName.Text = e.Row.Cells.FromKey("WorkflowName").Value.ToString();
            txtDispWorkflowRev.Text = e.Row.Cells.FromKey("WorkflowRev").Value.ToString();
            txtDispStepName.Text = e.Row.Cells.FromKey("StepName").Value.ToString();
            txtDispProductRev.Text = e.Row.Cells.FromKey("ProductRevision").Value.ToString();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion

    protected void btnSplit_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (txtDispContainerID.Text == string.Empty)
            {
                DisplayMessage("请选择要拆分的批次", false);
                return;
            }

            UltraGridRow activeRow = ItemGrid.DisplayLayout.ActiveRow;

            if (activeRow.Cells.FromKey("constatus").Text=="关闭")
            {
                DisplayMessage("批次已关闭！", false);
                return;
            }

            if (activeRow.Cells.FromKey("constatus").Text == "暂停")
            {
                DisplayMessage("批次已暂停！", false);
                return;
            }

         

            string strSpecID = ItemGrid.DisplayLayout.ActiveRow.Cells.FromKey("specid").Text;
            Dictionary<string, string> popupData = new Dictionary<string, string>();
            //批次当前工序是否已进行指派
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("SpecID", strSpecID);
            popupData.Add("IsAdd", "1");

            DataTable dt = partReport.GetSpecDispatchInfo(popupData);
            if (dt.Rows.Count>0)
            {
                DataRow[] dr = dt.Select("status='10' OR status='15' ");
                if (dr.Length>0)
                {
                    DisplayMessage("已进行任务指派，无法进行拆分！", false);
                    return;
                }
            

                DataRow[] dr1 = dt.Select("status='20'");
                if (dr1.Length>0)
                {
                    DisplayMessage("当前工序已接收，无法进行拆分！", false);
                    return;
                }

            }

            dt = new DataTable();
            dt = partReport.GetSpecReportInfo(popupData);
            if (dt.Rows.Count > 0)
            {
                DisplayMessage("当前工序已报工，无法进行拆分！", false);
                return;
            }

            dt = new DataTable();
            dt = partReport.GetSpecCheckInfo(popupData);
            if (dt.Rows.Count > 0)
            {
                DisplayMessage("当前工序已检验，无法进行拆分！", false);
                return;
            }

            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
           
            popupData.Add("ContainerName", txtDispContainerName.Text);
            //popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("Qty", txtDispQty.Text);
            popupData.Add("PlannedStartDate", txtDispPlannedStartDate.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);
            popupData.Add("ChildCount", txtDispChildCount.Text);

     

            Session["PopupData"] = popupData;
            
            string strScript = "<script>opensplit()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (txtDispContainerID.Text == string.Empty)
            {
                DisplayMessage("请选择要关闭的批次", false);
                return;
            }


            UltraGridRow activeRow = ItemGrid.DisplayLayout.ActiveRow;

            if (activeRow.Cells.FromKey("constatus").Text == "关闭")
            {
                DisplayMessage("批次已关闭！", false);
                return;
            }

            //if (activeRow.Cells.FromKey("constatus").Text == "暂停")
            //{
            //    DisplayMessage("批次已暂停，无法进行拆分！", false);
            //    return;
            //}

            Dictionary<string, string> popupData = new Dictionary<string, string>();
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("Qty", txtDispQty.Text);
            popupData.Add("PlannedStartDate", txtDispPlannedStartDate.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);
            popupData.Add("ChildCount", txtDispChildCount.Text);

            Session["PopupData"] = popupData;
            
            string strScript = "<script>openclose()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnOpen_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (txtDispContainerID.Text == string.Empty)
            {
                DisplayMessage("请选择要打开的批次", false);
                return;
            }

            Dictionary<string, string> popupData = new Dictionary<string, string>();
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("Qty", txtDispQty.Text);
            popupData.Add("PlannedStartDate", txtDispPlannedStartDate.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);
            popupData.Add("ChildCount", txtDispChildCount.Text);

            Session["PopupData"] = popupData;
            
            string strScript = "<script>openopen()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnHold_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (txtDispContainerID.Text == string.Empty)
            {
                DisplayMessage("请选择要暂停的批次", false);
                return;
            }
            UltraGridRow activeRow = ItemGrid.DisplayLayout.ActiveRow;

            if (activeRow.Cells.FromKey("constatus").Text == "关闭")
            {
                DisplayMessage("批次已关闭！", false);
                return;
            }

            if (activeRow.Cells.FromKey("constatus").Text == "暂停")
            {
                DisplayMessage("批次已暂停！", false);
                return;
            }

            Dictionary<string, string> popupData = new Dictionary<string, string>();
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("Qty", txtDispQty.Text);
            popupData.Add("PlannedStartDate", txtDispPlannedStartDate.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);
            popupData.Add("ChildCount", txtDispChildCount.Text);

            Session["PopupData"] = popupData;
            
            string strScript = "<script>openhold()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnRelease_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (txtDispContainerID.Text == string.Empty)
            {
                DisplayMessage("请选择要继续的批次", false);
                return;
            }

            UltraGridRow activeRow = ItemGrid.DisplayLayout.ActiveRow;

            if (activeRow.Cells.FromKey("constatus").Text == "关闭")
            {
                DisplayMessage("批次已关闭！", false);
                return;
            }

            Dictionary<string, string> popupData = new Dictionary<string, string>();
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("ProductRev", txtDispProductRev.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("Qty", txtDispQty.Text);
            popupData.Add("PlannedStartDate", txtDispPlannedStartDate.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);
            popupData.Add("ChildCount", txtDispChildCount.Text);

            Session["PopupData"] = popupData;
            
            string strScript = "<script>openrelease()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnQtyAdjust_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (txtDispContainerID.Text == string.Empty)
            {
                DisplayMessage("请选择要调整数量的批次", false);
                return;
            }

            UltraGridRow activeRow = ItemGrid.DisplayLayout.ActiveRow;

            if (activeRow.Cells.FromKey("constatus").Text == "关闭")
            {
                DisplayMessage("批次已关闭！", false);
                return;
            }

            if (activeRow.Cells.FromKey("constatus").Text == "暂停")
            {
                DisplayMessage("批次已暂停！", false);
                return;
            }

            Dictionary<string, string> popupData = new Dictionary<string, string>();
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("Qty", txtDispQty.Text);
            popupData.Add("PlannedStartDate", txtDispPlannedStartDate.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);
            popupData.Add("ChildCount", txtDispChildCount.Text);
            popupData.Add("WorkflowName", txtDispWorkflowName.Text);
            popupData.Add("WorkflowRev", txtDispWorkflowRev.Text);
            popupData.Add("StepName", txtDispStepName.Text);
            popupData.Add("ProductRev", txtDispProductRev.Text);

            //已指派不能调整
            popupData.Add("SpecID", activeRow.Cells.FromKey("specid").Text);
            var dt = OracleHelper.QueryDataByEntity(new ExcuteEntity("dispatchinfo", ExcuteType.selectAll) {
                WhereFileds=new List<FieldEntity>() { new FieldEntity("ContainerID", popupData["ContainerID"],FieldType.Str),new FieldEntity("WorkflowID",popupData["WorkflowID"],FieldType.Str),
                new FieldEntity("SpecID",popupData["SpecID"],FieldType.Str)},
                strWhere= " and parentid is not null "
            });
            if (dt.Rows.Count > 0) {
                DisplayMessage("当前序已经指派，不能调整数量", false);
                return;
            }

            Session["PopupData"] = popupData;
            
            string strScript = "<script>openadjust()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();

            UltraGridRow uldr = ItemGrid.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                DisplayMessage("请选择订单记录", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            DataTable poupDt = new DataTable();
            poupDt.Columns.Add("ProductID");
            poupDt.Columns.Add("WorkflowID"); poupDt.Columns.Add("ContainerID"); poupDt.Columns.Add("ContainerName");
            DataRow newRow = poupDt.NewRow();
            newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
            newRow["WorkflowID"] = uldr.Cells.FromKey("Workflowid").Text; newRow["ContainerID"] = uldr.Cells.FromKey("ContainerID").Text;
            newRow["ContainerName"] = uldr.Cells.FromKey("ContainerName").Text;
            poupDt.Rows.Add(newRow);
            Session.Add("ProcessDocument", poupDt);
            string strScript = string.Empty;

            strScript = "<script>window.showModalDialog('Custom/bwCommonPage/uMESDocumentViewPopupForm.aspx?v='+new Date().getTime(), '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}