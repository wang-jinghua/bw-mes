﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Data;
using System.Drawing;
using Infragistics.WebUI.UltraWebGrid;

public partial class FXPopupForm : System.Web.UI.Page
{
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerBusiness containerBal  = new uMESContainerBusiness();
    uMESRejectAppBusiness rejectapp = new uMESRejectAppBusiness();

    string businessName = "质量管理", parentName = "repairinfo";
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Dictionary<string, object> popupData = (Dictionary<string, object>)Session["PopupData"];
            txtDispContainerID.Text = popupData["ContainerID"].ToString();
            txtDispProcessNo.Text = popupData["ProcessNo"].ToString();
            txtDispOprNo.Text = popupData["OprNo"].ToString();
            txtDispContainerName.Text = popupData["ContainerName"].ToString();
            txtDispProductName.Text = popupData["ProductName"].ToString();
            txtDispDescription.Text = popupData["Description"].ToString();
            txtDispContainerQty.Text = popupData["ContainerQty".ToString()].ToString();
            txtDispSpecID.Text = popupData["SpecID"].ToString();
            txtDispPlannedStartDate.Text = popupData["PlannedStartDate"].ToString();
            txtDispPlannedCompletionDate.Text = popupData["PlannedCompletionDate"].ToString();
            txtID.Text = popupData["ID"].ToString();
            txtChildCount.Text = popupData["ChildCount"].ToString();
            txtDispWorkflowID.Text = popupData["WorkflowID"].ToString();
            txtFXQty.Text = popupData["RepairQty"].ToString();//add:Wangjh 20201023

            DataTable dtProductNo = (DataTable)(popupData["dtProductNo"]);
            wgProductNoList.DataSource = dtProductNo;
            wgProductNoList.DataBind();

            Session["PopupData"] = null;
            
            getWorkFlowInfo.SearchWorkFlow(new Dictionary<string, string>() {
                { "ProductID",popupData["ProductID"].ToString()}, { "ProductName",""}
            },1); getWorkFlowInfo.GetBtnSearch.Visible = false;

            //txtScrapInfoName.Text = DateTime.Now.ToString("yyyyMMddHHmmssfff");
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }
    #endregion

    #region 提交按钮
    protected void btnMaterialApp_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {
            //数据验证 add:Wangjh
            if (string.IsNullOrWhiteSpace(getWorkFlowInfo.WorkFlowValue)) {
                ShowStatusMessage("请选择工艺",false);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtFXContainerName.Text)) {
                ShowStatusMessage("请输入返修批次号", false);
                return;
            }

            //数量和产品序号
            int intQty = 0;
            DataTable dtProductNo = new DataTable();
            dtProductNo.Columns.Add("ID");
            dtProductNo.Columns.Add("ContainerID");
            dtProductNo.Columns.Add("ContainerName");
            dtProductNo.Columns.Add("ProductNo");
            dtProductNo.Columns.Add("Notes");

            TemplatedColumn temCell1 = (TemplatedColumn)wgProductNoList.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell1.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem1 = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell1.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem1.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    intQty += Convert.ToInt32(wgProductNoList.Rows[i].Cells.FromKey("Qty").Value.ToString());

                    DataRow row = dtProductNo.NewRow();

                    row["ID"] = wgProductNoList.Rows[i].Cells.FromKey("ID").Value.ToString();

                    if (wgProductNoList.Rows[i].Cells.FromKey("ContainerID").Value != null)
                    {
                        row["ContainerID"] = wgProductNoList.Rows[i].Cells.FromKey("ContainerID").Value.ToString();
                    }
                    else
                    {
                        row["ContainerID"] = string.Empty;
                    }

                    if (wgProductNoList.Rows[i].Cells.FromKey("ContainerName").Value != null)
                    {
                        row["ContainerName"] = wgProductNoList.Rows[i].Cells.FromKey("ContainerName").Value.ToString();
                    }
                    else
                    {
                        row["ContainerName"] = string.Empty;
                    }

                    if (wgProductNoList.Rows[i].Cells.FromKey("ProductNo").Value != null)
                    {
                        row["ProductNo"] = wgProductNoList.Rows[i].Cells.FromKey("ProductNo").Value.ToString();
                    }
                    else
                    {
                        row["ProductNo"] = string.Empty;
                    }

                    row["Notes"] = string.Empty;

                    dtProductNo.Rows.Add(row);
                }
            }

            //拆批
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string apiEmployeeName = userInfo["ApiEmployeeName"], apiPassword =userInfo["ApiPassword"];//add:Wangjh 20201023
            string strOprEmployeeID = userInfo["EmployeeID"];
            string strEmployeeName = userInfo["EmployeeName"];
            string strPassword = userInfo["Password"];
            string strWorkflowID = txtDispWorkflowID.Text;
            string strContainerName = txtDispContainerName.Text;
            string strFXContainerName = txtDispProductName.Text+"/"+ txtFXContainerName.Text.Trim();
            Dictionary<string, object> para1 = new Dictionary<string, object>();
            para1.Add("ContainerName", strContainerName);
            para1.Add("ServerUser", apiEmployeeName);
            para1.Add("ServerPassword", apiPassword);

            DataTable dtToContainerInfo = new DataTable();
            dtToContainerInfo.Columns.Add("ToContainerName");
            dtToContainerInfo.Columns.Add("Qty");

            DataRow rowToContainer = dtToContainerInfo.NewRow();
            rowToContainer["ToContainerName"] = strFXContainerName;
            rowToContainer["Qty"] = txtFXQty.Text;  //intQty.ToString();
            dtToContainerInfo.Rows.Add(rowToContainer);

            para1.Add("dtToContainerInfo", dtToContainerInfo);

            DataTable dtChildList = new DataTable();
            dtChildList.Columns.Add("ContainerName");
            dtChildList.Columns.Add("Level");

            foreach (DataRow r in dtProductNo.Rows)
            {
                string strChild = r["ContainerName"].ToString();

                if (strChild != string.Empty)
                {
                    DataRow rr = dtChildList.NewRow();
                    rr["ContainerName"] = strChild;
                    rr["Level"] = "Lot";
                    dtChildList.Rows.Add(rr);
                }
            }

            para1.Add("dtChildList", dtChildList);

            string strMessage = string.Empty;

            Boolean result = rejectapp.SplitContainer(para1, out strMessage);

            if (result == false)
            {
                ShowStatusMessage(strMessage, false);
                return;
            }

            //非标准移动
            string strFXWorkflowID = getWorkFlowInfo.WorkFlowValue;
            Dictionary<string, string> para2 = new Dictionary<string, string>();
            para2.Add("ContainerName", strFXContainerName);
            para2.Add("WorkflowID", strFXWorkflowID);
            para2.Add("ServerUser", apiEmployeeName);
            para2.Add("ServerPassword", apiPassword);

            result = rejectapp.MoveNonStd(para2, out strMessage);

            if (result == false)
            {
                ShowStatusMessage(strMessage, false);
                return;
            }

            //添加返修单记录
            string strRepairInfoName = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string strContainerID = txtDispContainerID.Text;
            string strSpecID = txtDispSpecID.Text;
            string strRejectAppInfoID = txtID.Text;

            Dictionary<string, string> para3 = new Dictionary<string, string>();
            para3.Add("RepairInfoName", strRepairInfoName);
            para3.Add("RejectAppInfoID", strRejectAppInfoID);
            para3.Add("ContainerID", strContainerID);
            para3.Add("WorkflowID", strWorkflowID);
            para3.Add("SpecID", strSpecID);
            para3.Add("Qty", intQty.ToString());
            para3.Add("UomID", "");
            para3.Add("RepairContainerID", "");
            para3.Add("RepairContainerName", strFXContainerName);
            para3.Add("RepairWorkflowID", strFXWorkflowID);
            para3.Add("OprEmployeeID", strOprEmployeeID);
            para3.Add("OprDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            para3.Add("Notes", txtNotes.Text.Trim());
            para3.Add("RepairInfoID", Guid.NewGuid().ToString());

            rejectapp.AddRepairInfo(para3, dtProductNo);
            //更改不合格审理单状态

            #region 记录日志
            var ml = new MESAuditLog();
            ml.ContainerName = txtDispContainerName.Text; ml.ContainerID = txtDispContainerID.Text;
            ml.ParentID = para3["RepairInfoID"]; ml.ParentName = parentName;
            ml.CreateEmployeeID = userInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = 0;
            ml.Description =string.Format( "返修:提交返修批次为:{0},数量:{1}",para3["RepairContainerName"],para3["Qty"]);
            common.SaveMESAuditLog(ml);
            #endregion

            Response.Write("<script>parent.window.returnValue='1'; window.close()</script>");
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
    #endregion
}