﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.Web.UI;

public partial class AssemblyRemoveForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "AssemblyRemoveForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESAssemblyRecordBusiness bll = new uMESAssemblyRecordBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：装配拆除";
        master.strTitle = "装配拆除";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
            GetRemoveReason();
        }
    }

    #region 获取拆除原因
    protected void GetRemoveReason()
    {
        DataTable DT = common.GetRemoveReason();

        ddlRemoveReason.DataTextField = "RemoveReasonName";
        ddlRemoveReason.DataValueField = "RemoveReasonID";
        ddlRemoveReason.DataSource = DT;
        ddlRemoveReason.DataBind();

        ddlRemoveReason.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        Dictionary<string, string> result = new Dictionary<string, string>();

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        try
        {
            //
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
    }
    #endregion
    
    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strAssemblyRecordID = txtAssemblyID.Text;
            if (strAssemblyRecordID == string.Empty)
            {
                DisplayMessage("请选择要拆除的装配记录", false);
                return;
            }

            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strEmployeeID = userInfo["EmployeeID"];
            string strRemoveDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string strRemoveReasonID = ddlRemoveReason.SelectedValue;
            string strNotes = txtNotes.Text.Trim();

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("AssemblyRecordInfoID", strAssemblyRecordID);
            para.Add("EmployeeID", strEmployeeID);
            para.Add("RemoveDate", strRemoveDate);
            para.Add("RemoveReasonID", strRemoveReasonID);
            para.Add("Notes", strNotes);

            bll.AddAssemblyRemoveInfo(para);

            AfterSave();

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion

    #region AfterSave
    protected void AfterSave()
    {
        txtAssemblyID.Text = string.Empty;
        txtDispMaterialName.Text = string.Empty;
        txtDispMaterialDesc.Text = string.Empty;
        txtDispMaterialQty.Text = string.Empty;
        txtDispMaterialSN.Text = string.Empty;
        txtDispMaterialFN.Text = string.Empty;
        txtDispMaterialDate.Text = string.Empty;

        ddlSpec_SelectedIndexChanged(null, null);

        wgAssemblyList.Clear();
        ddlRemoveReason.SelectedValue = string.Empty;
        txtNotes.Text = string.Empty;

        //Dictionary<string, string> para = (Dictionary<string, string>)Session["Para_ARemove"];
        //string strContainerID = para["ContainerID"];
        //string strSpecID = para["SpecID"];
        //string strProductNoID = para["ProductNoID"];
        //string strProductID = para["ProductID"];

        //DataTable DT = bll.GetAssemblyList(strContainerID, strSpecID, strProductNoID, strProductID);

        //wgAssemblyList.DataSource = DT;
        //wgAssemblyList.DataBind();
    }
    #endregion

    #region 查询批次信息
    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();
        ClearDispData();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                DataTable DT = bll.GetContainerInfo(strScan);

                if (DT.Rows.Count == 0)
                {
                    DisplayMessage("无效的批次号", false);
                    return;
                }

                string strProcessNo = DT.Rows[0]["ProcessNo"].ToString();
                txtDispProcessNo.Text = strProcessNo;
                string strOprNo = DT.Rows[0]["OprNo"].ToString();
                txtDispOprNo.Text = strOprNo;
                string strContainerID = DT.Rows[0]["ContainerID"].ToString();
                txtDispContainerID.Text = strContainerID;
                Session["ParentContainerID"] = strContainerID;
                string strContainerName = DT.Rows[0]["ContainerName"].ToString();
                txtDispContainerName.Text = strContainerName;
                string strWorkflowID = DT.Rows[0]["WorkflowID"].ToString();
                txtDispWorkflowID.Text = strWorkflowID;
                string strProductID = DT.Rows[0]["ProductID"].ToString();
                txtDispProductID.Text = strProductID;
                string strProductName = DT.Rows[0]["ProductName"].ToString();
                txtDispProductName.Text = strProductName;
                string strDescription = DT.Rows[0]["Description"].ToString();
                txtDispDescription.Text = strDescription;
                string strQty = DT.Rows[0]["Qty"].ToString();
                txtDispQty.Text = strQty;
                string strChildCount = DT.Rows[0]["ChildCount"].ToString();
                txtChildCount.Text = strChildCount;

                string strPlannedStartDate = DT.Rows[0]["PlannedStartDate"].ToString();
                if (strPlannedStartDate != string.Empty)
                {
                    strPlannedStartDate = Convert.ToDateTime(strPlannedStartDate).ToString("yyyy-MM-dd");
                }
                txtDispPlannedStartDate.Text = strPlannedStartDate;

                string strPlannedCompletionDate = DT.Rows[0]["PlannedCompletionDate"].ToString();
                if (strPlannedCompletionDate != string.Empty)
                {
                    strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                }
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;

                GetSpec(strWorkflowID);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion

    #region 绑定工序列表
    protected void GetSpec(string strWorkflowID)
    {
        List<string> listWorkflowID = new List<string>();
        listWorkflowID.Add(strWorkflowID);

        DataTable DT = common.GetSpecListByWorkflowID(listWorkflowID);
        DT.Columns.Add("SpecNameDisp");

        foreach (DataRow row in DT.Rows)
        {
            string strSpecName = row["SpecName"].ToString();
            strSpecName = common.GetSpecNameWithOutProdName(strSpecName);
            row["SpecNameDisp"] = strSpecName;
        }

        ddlSpec.DataTextField = "SpecNameDisp";
        ddlSpec.DataValueField = "SpecID";
        ddlSpec.DataSource = DT;
        ddlSpec.DataBind();

        ddlSpec.Items.Insert(0, "");
    }
    #endregion

    #region 清空显示的批次信息
    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;

        ddlSpec.Items.Clear();
        txtProductNo.Text = string.Empty;

        wgItemList.Clear();
        wgAssemblyList.Clear();
        
        txtNotes.Text = string.Empty;
    }
    #endregion

    #region 选择工序
    protected void ddlSpec_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();
        wgItemList.Clear();
        wgAssemblyList.Clear();

        ClearAssemblyInfo();

        try
        {
            int intChildCount = Convert.ToInt32(txtChildCount.Text);
            string strProductNo = txtProductNo.Text;

            if (intChildCount > 0 && strProductNo == string.Empty)
            {
                DisplayMessage("请选择产品序号", true);
                return;
            }

            string strContainerID = txtDispContainerID.Text;
            string strProductID = txtDispProductID.Text;
            string strSpecID = ddlSpec.SelectedValue;

            string strProductNoID = string.Empty;
            int intQty = Convert.ToInt32(txtDispQty.Text);
            if (intChildCount > 0)
            {
                strProductNoID = txtProductNoID.Text;
                intQty = 1;
            }

            if (strProductID != string.Empty && strSpecID != string.Empty)
            {
                DataTable DT = bll.GetMaterialListByProductID(strProductID, strSpecID);
                DataTable dtAssembly = bll.GetAssemblyQty(strContainerID, strSpecID, strProductNoID);

                DT.Columns.Add("RequireQty");
                DT.Columns.Add("AssemblyQty");
                DT.Columns.Add("Qty");

                foreach (DataRow row in DT.Rows)
                {
                    int intQtyRequired = Convert.ToInt32(row["QtyRequired"].ToString());

                    string strProdID = row["ProductID"].ToString();
                    int intAssemblyQty = 0;
                    DataRow[] rows = dtAssembly.Select(string.Format("ProductID = '{0}'", strProdID));

                    if (rows.Length > 0)
                    {
                        intAssemblyQty = Convert.ToInt32(rows[0]["Qty"].ToString());
                    }

                    row["RequireQty"] = intQtyRequired * intQty;
                    row["AssemblyQty"] = intAssemblyQty;

                    if (intQtyRequired * intQty - intAssemblyQty >= 0)
                    {
                        row["Qty"] = intQtyRequired * intQty - intAssemblyQty;
                    }
                    else
                    {
                        row["Qty"] = "0";
                    }
                }

                wgItemList.DataSource = DT;
                wgItemList.DataBind();
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion

    protected void wgItemList_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            string strContainerID = txtDispContainerID.Text;
            string strSpecID = ddlSpec.SelectedValue;
            string strProductNoID = txtProductNoID.Text;
            string strProductID = e.Row.Cells.FromKey("ProductID").Value.ToString();

            DataTable DT = bll.GetAssemblyList(strContainerID, strSpecID, strProductNoID, strProductID);

            wgAssemblyList.DataSource = DT;
            wgAssemblyList.DataBind();

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ContainerID", strContainerID);
            para.Add("SpecID", strSpecID);
            para.Add("ProductNoID", strProductNoID);
            para.Add("ProductID", strProductID);

            Session["Para_ARemove"] = para;
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void wgAssemblyList_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            txtDispMaterialName.Text = strProductName;

            string strDescription = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
            }
            txtDispMaterialDesc.Text = strDescription;

            string strQty = string.Empty;
            if (e.Row.Cells.FromKey("Qty").Value != null)
            {
                strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            }
            txtDispMaterialQty.Text = strQty;

            string strSerialNumber = string.Empty;
            if (e.Row.Cells.FromKey("SerialNumber").Value != null)
            {
                strSerialNumber = e.Row.Cells.FromKey("SerialNumber").Value.ToString();
            }
            txtDispMaterialSN.Text = strSerialNumber;

            string strFullName = string.Empty;
            if (e.Row.Cells.FromKey("FullName").Value != null)
            {
                strFullName = e.Row.Cells.FromKey("FullName").Value.ToString();
            }
            txtDispMaterialFN.Text = strFullName;

            string strAssemblyDate = string.Empty;
            if (e.Row.Cells.FromKey("AssemblyDate").Value != null)
            {
                strAssemblyDate = e.Row.Cells.FromKey("AssemblyDate").Value.ToString();
            }
            txtDispMaterialDate.Text = strAssemblyDate;

            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtAssemblyID.Text = strID;
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ClearAssemblyInfo()
    {
        txtDispMaterialName.Text = string.Empty;
        txtDispMaterialDesc.Text = string.Empty;
        txtDispMaterialQty.Text = string.Empty;
        txtDispMaterialSN.Text = string.Empty;
        txtDispMaterialFN.Text = string.Empty;
        txtDispMaterialDate.Text = string.Empty;
        txtAssemblyID.Text = string.Empty;

        ddlRemoveReason.SelectedValue = string.Empty;
        txtNotes.Text = string.Empty;
    }
}