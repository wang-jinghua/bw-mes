﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.UI;
using System.Text.RegularExpressions;

public partial class TestForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "TestForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();
    uMESRejectAppBusiness reject = new uMESRejectAppBusiness();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：测试";
        master.strTitle = "测试";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        ClearMessage();

        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strTeamID = userInfo["TeamID"];
        result.Add("TeamID", strTeamID);
        result.Add("DispatchType", "1");
        result.Add("Status", "20"); //已接收
        result.Add("ScanContainerName", strScanContainerName);
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("SpecName", strSpecName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        uMESPagingDataDTO result = dispatch.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 9);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;

        ClearDispData();
    }

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 提示信息
    ///// <summary>
    ///// 提示信息
    ///// </summary>
    ///// <param name="strMessage"></param>
    ///// <param name="boolResult"></param>
    //protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    //{
    //    string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
    //    Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    //}

    ///// <summary>
    ///// 清除提示信息
    ///// </summary>
    //protected void ClearMessage()
    //{
    //    string strScript = "<script>ShowMessage(\"\", true);</script>";
    //    LiteralControl child = new LiteralControl(strScript);
    //    WebAsyncRefreshPanel1.Controls.Add(child);
    //}
    #endregion

    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProcessNo").Value != null)
            {
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            txtDispContainerName.Text = strContainerName;
            string strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            txtDispContainerID.Text = strContainerID;

            string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            txtDispProductName.Text = strProductName;
            string strProductID = e.Row.Cells.FromKey("ProductID").Value.ToString();
            txtDispProductID.Text = strProductID;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }

            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            string strSpec = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
            txtDispSpecName.Text = strSpec;
            string strWorkflowID = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            txtDispWorkflowID.Text = strWorkflowID;
            string strSpecID = e.Row.Cells.FromKey("SpecID").Value.ToString();
            txtDispSpecID.Text = strSpecID;

            string strTeamName = e.Row.Cells.FromKey("TeamName").Value.ToString();
            txtDispTeamName.Text = strTeamName;
            string strTeamID = e.Row.Cells.FromKey("TeamID").Value.ToString();
            txtDispTeamID.Text = strTeamID;

            string strResourceName = e.Row.Cells.FromKey("ResourceName").Value.ToString();
            txtDispResourceName.Text = strResourceName;
            string strResourceID = e.Row.Cells.FromKey("ResourceID").Value.ToString();
            txtDispResourceID.Text = strResourceID;

            DataTable DT = dispatch.GetResourceDispatchInfo(strResourceID);

            wgDispatchList.DataSource = DT;
            wgDispatchList.DataBind();

            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Value.ToString();
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
            }

            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtDispID.Text = strID;
            string strParentID = e.Row.Cells.FromKey("ParentID").Value.ToString();
            txtDispParentID.Text = strParentID;

            DataTable dtEmployee = dispatch.GetEmployeeByDispatchID(strID);
            wgEmployee.DataSource = dtEmployee;
            wgEmployee.DataBind();

            wgProductNo.Clear();
            DataTable dt = dispatch.GetProductNoByDispatchID(strID);

            wgProductNo.DataSource = dt;
            wgProductNo.DataBind();

            string strParentQty = e.Row.Cells.FromKey("ParentQty").Value.ToString();
            txtParentQty.Text = strParentQty;
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #endregion

    #region 物料齐套按钮
    protected void btnMaterialAssort_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            Dictionary<string, object> popupData = new Dictionary<string, object>();
            popupData.Add("ProcessNo", "ProcessNo");
            popupData.Add("OprNo", "OprNo");
            popupData.Add("ContainerName", "Product001/2001");
            popupData.Add("ProductName", "Product001");
            popupData.Add("Description", "测试产品001");
            popupData.Add("ContainerQty", "10");
            popupData.Add("PlannedStartDate", "PlannedStartDate");
            popupData.Add("PlannedCompletionDate", "PlannedCompletionDate");
            popupData.Add("ContainerID", "0004108000000066");
            popupData.Add("SpecID", "00071c8000000002");

            DataTable dtQualityRecord = new DataTable();
            dtQualityRecord.Columns.Add("QRID");
            dtQualityRecord.Columns.Add("Qty");

            DataRow row = dtQualityRecord.NewRow();
            row["QRID"] = "001";
            row["Qty"] = "1";
            dtQualityRecord.Rows.Add(row);

            row = dtQualityRecord.NewRow();
            row["QRID"] = "002";
            row["Qty"] = "2";
            dtQualityRecord.Rows.Add(row);

            row = dtQualityRecord.NewRow();
            row["QRID"] = "003";
            row["Qty"] = "3";
            dtQualityRecord.Rows.Add(row);

            if (dtQualityRecord.Rows.Count == 0)
            {
                DisplayMessage("请选择质量记录", false);
                return;
            }

            popupData.Add("dtQualityRecord", dtQualityRecord);

            //产品序号
            DataTable dtProductNo = new DataTable();
            dtProductNo.Columns.Add("ContainerID");
            dtProductNo.Columns.Add("ContainerName");
            dtProductNo.Columns.Add("ProductNo");
            dtProductNo.Columns.Add("Qty");
            dtProductNo.Columns.Add("UOMID");
            dtProductNo.Columns.Add("DisposeResult");
            dtProductNo.Columns.Add("LossReasonID");
            dtProductNo.Columns.Add("DisposeAdviceID");
            dtProductNo.Columns.Add("DisposeNotes");
            dtProductNo.Columns.Add("QualityNotes");
            dtProductNo.Columns.Add("Notes");

            row = dtProductNo.NewRow();
            row["ContainerID"] = "0004108000000067";
            row["ContainerName"] = "Product001/2001001";
            row["ProductNo"] = "001";
            row["Qty"] = "1";
            row["DisposeResult"] = "0";
            dtProductNo.Rows.Add(row);

            row = dtProductNo.NewRow();
            row["ContainerID"] = "0004108000000068";
            row["ContainerName"] = "Product001/2001002";
            row["ProductNo"] = "002";
            row["Qty"] = "1";
            row["DisposeResult"] = "0";
            dtProductNo.Rows.Add(row);

            row = dtProductNo.NewRow();
            row["ContainerID"] = "0004108000000069";
            row["ContainerName"] = "Product001/2001003";
            row["ProductNo"] = "003";
            row["Qty"] = "1";
            row["DisposeResult"] = "0";
            dtProductNo.Rows.Add(row);

            row = dtProductNo.NewRow();
            row["ContainerID"] = "000410800000006a";
            row["ContainerName"] = "Product001/2001004";
            row["ProductNo"] = "004";
            row["Qty"] = "1";
            row["DisposeResult"] = "0";
            dtProductNo.Rows.Add(row);

            row = dtProductNo.NewRow();
            row["ContainerID"] = "000410800000006b";
            row["ContainerName"] = "Product001/2001005";
            row["ProductNo"] = "005";
            row["Qty"] = "1";
            row["DisposeResult"] = "0";
            dtProductNo.Rows.Add(row);

            row = dtProductNo.NewRow();
            row["ContainerID"] = "000410800000006c";
            row["ContainerName"] = "Product001/2001006";
            row["ProductNo"] = "006";
            row["Qty"] = "1";
            row["DisposeResult"] = "0";
            dtProductNo.Rows.Add(row);

            popupData.Add("dtProductNo", dtProductNo);

            Session["PopupData"] = popupData;

            ClientScript.RegisterStartupScript(GetType(), "", "<script>openrejectappsubmit()</script>");
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispTeamName.Text = string.Empty;
        txtDispResourceName.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;

        txtDispID.Text = string.Empty;

        wgEmployee.Clear();
        wgProductNo.Clear();
        wgDispatchList.Clear();
    }
    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void wgDispatchList_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < wgDispatchList.Rows.Count; i++)
        {
            string strSpecName = wgDispatchList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            wgDispatchList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    protected void btnTest_Click(object sender, EventArgs e)
    {
        Dictionary<string, object> para = new Dictionary<string, object>();
        para.Add("D1", "Data001");
        para.Add("D2", "Data002");
        para.Add("D3", "Data003");
        para.Add("D4", "Data004");
        para.Add("D5", "Data005");

        List<Dictionary<string, object>> pD6 = new List<Dictionary<string, object>>();

        for (int i = 0; i < 6; i++)
        {
            Dictionary<string, object> item = new Dictionary<string, object>();
            item.Add("qty", (i + 1) * 10);

            Dictionary<string, string> p = new Dictionary<string, string>();
            p.Add("Name", "Prod00" + i.ToString());
            item.Add("material", p);

            pD6.Add(item);
        }

        para.Add("Data", pD6);

        string pJson = JsonConvert.SerializeObject(para);

        txtScan.Text = pJson;
    }

    protected void btnLoss_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strEmployeeName = userInfo["EmployeeName"].ToString();
            string strPassword = userInfo["Password"].ToString();

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ContainerName", "");
            para.Add("Level", "");
            para.Add("ServerUser", strEmployeeName);
            para.Add("ServerPassword", strPassword);

            DataTable dtChangeQtyDetails = new DataTable();
            dtChangeQtyDetails.Columns.Add("Qty");
            dtChangeQtyDetails.Columns.Add("ReasonCode");

            string strInfo = string.Empty;
            //Boolean result = reject.ChangeQty(para, dtChangeQtyDetails,out strInfo);

            DisplayMessage(strInfo, true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        DisplayMessage("嘉时代缴费卡士大夫看见爱上", false);
        //ExecuteScript("<script>alert('asdfasdf');</script>");

    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        ClearMessage();
        //DisplayMessage("", true);
        //DisplayMessage("成功", true);
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        string pattern1 = @"(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,10}";
        string result1 = new Regex(pattern1).Match("abcd.3347").Value;

        DisplayMessage(result1, true);
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        //DateTime start1 = Convert.ToDateTime("2020-08-10 08:00:00");
        //DateTime end1 = Convert.ToDateTime("2020-08-10 12:00:00");
        //DateTime start2 = Convert.ToDateTime("2020-08-10 13:00:00");
        //DateTime end2 = Convert.ToDateTime("2020-08-10 17:00:00");

        DataTable DT = new DataTable();
        DT.Columns.Add("start");
        DT.Columns.Add("end");

        DataRow row = DT.NewRow();
        row["start"] = "08:00:00";
        row["end"] = "12:00:00";
        DT.Rows.Add(row);

        row = DT.NewRow();
        row["start"] = "13:00:00";
        row["end"] = "17:00:00";
        DT.Rows.Add(row);

        row = DT.NewRow();
        row["start"] = "18:00:00";
        row["end"] = "22:00:00";
        DT.Rows.Add(row);

        //DataTable dt = GetDaySJD(DateTime.Now, DT);

        DateTime startTime = Convert.ToDateTime("2020-08-13 07:00:00");
        Double dblGS = 37.5 * 60;
        DateTime endTime = DateTime.Now;
        DateTime startTimeTemp = DateTime.Now;
        GetEndDateTime(startTime, out startTime, out endTime, dblGS, DT, 0, startTimeTemp, true);

        DisplayMessage(startTime.ToString("yyyy-MM-dd HH:mm:ss") + " - " + endTime.ToString("yyyy-MM-dd HH:mm:ss"), true);

        //DateTime date = Convert.ToDateTime("2020-08-15 07:00:00");

        //if (date.DayOfWeek == DayOfWeek.Saturday)
        //{
        //    DisplayMessage("周六", true);
        //}

        //DisplayMessage(date.DayOfWeek.ToString(), true);

        //DateTime startTime = Convert.ToDateTime("2020-08-10 09:20:00");

        //if (startTime.Hour.ToString() == "0" && startTime.Minute.ToString() == "0" && startTime.Second.ToString() == "0")
        //{
        //    startTime = startTime.AddHours(8);
        //}

        //Double dblGS = 5.5 * 60; //以分钟计算

        //if (startTime < end1)
        //{
        //    Double intM = GetM(startTime, end1);

        //    if (intM >= dblGS)
        //    {
        //        DisplayMessage(startTime.AddMinutes(dblGS).ToString("yyyy-MM-dd HH:mm:ss"), true);
        //    }
        //    else
        //    {
        //        dblGS = dblGS - intM;


        //    }
        //}

        //DisplayMessage(GetM(startTime,end1).ToString(), true);

        //DisplayMessage(startTime.AddHours(dblGS).ToString("yyyy-MM-dd HH:mm:ss"), true);
    }

    #region 根据给定的开始时间、工时、每日工作时间段计算结束时间
    /// <summary>
    /// 根据给定的开始时间、工时、每日工作时间段计算结束时间
    /// </summary>
    /// <param name="startDateTime">给定的开始时间</param>
    /// <param name="startTime">计算出的开始时间，只需给定任意初始值</param>
    /// <param name="endTime">计算出的结束时间</param>
    /// <param name="dblGS">工时（单位：分钟）</param>
    /// <param name="DT">每日工作时间段</param>
    /// <param name="intS">内部参数：固定为 0 </param>
    /// <param name="startTimeTemp">内部参数，只需给定任意初始值</param>
    /// <param name="boolSS">是否包含周末（周六和周日）</param>
    protected void GetEndDateTime(DateTime startDateTime, out DateTime startTime, out DateTime endTime, 
        Double dblGS, DataTable DT, int intS, DateTime startTimeTemp, Boolean boolIncludeSS)
    {
        startDateTime = GetStartDateTime(startDateTime, boolIncludeSS);

        if (intS == 0) //第一次执行时，输出的开始时间设定为给定的开始时间
        {
            startTime = startDateTime;
        }
        else //递归时记录输出的开始时间
        {
            startTime = startTimeTemp;
        }

        endTime = DateTime.Now;
        
        DataTable dt = GetDaySJD(startDateTime, DT); //获取给定日期的工作时间段

        if (intS == 1) //递归时，开始时间为最早工作时间段的开始时间
        {
            startDateTime = Convert.ToDateTime(dt.Rows[0]["StartTime"]);
        }

        //获取开始时间所在及之后的工作时间段
        DataRow[] rows = dt.Select(string.Format("(StartTime <= #{0}# AND EndTime > #{0}#) OR (StartTime >= #{0}#)", startDateTime), "StartTime ASC");

        //按顺序将工时排入工作时间段，同时处理开始时间和结束时间
        for (int i = 0; i < rows.Length; i++)
        {
            DateTime sTime = Convert.ToDateTime(rows[i]["StartTime"]);
            if (i == 0 && intS == 0)
            {
                if (startDateTime >= sTime)
                {
                    sTime = startDateTime;
                }
                else
                {
                    startTime = sTime;
                    startTimeTemp = startTime;
                }
            }
            
            DateTime eTime = Convert.ToDateTime(rows[i]["EndTime"]);
            Double T1 = GetM(sTime, eTime);

            if (T1 >= dblGS)
            {
                endTime = sTime.AddMinutes(dblGS);
                dblGS = 0;
                break; 
            }
            else
            {
                dblGS = dblGS - T1;
            }
        }

        //当天全部时间段全部拍完后，如果工时仍大于零，则向第二天继续排
        if (dblGS > 0)
        {
            startDateTime = startDateTime.AddDays(1);
            intS = 1;

            GetEndDateTime(startDateTime, out startTime, out endTime, dblGS, DT, 1, startTimeTemp, boolIncludeSS);
        }
    }

    protected DateTime GetStartDateTime(DateTime date, Boolean boolIncludeSS)
    {
        DateTime result = date;

        if (boolIncludeSS == false)
        {
            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                result = date.AddDays(2);
            }

            if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                result = date.AddDays(1);
            }
        }

        return result;
    }

    /// <summary>
    /// 获取给定日期的工作时间段
    /// </summary>
    /// <param name="date"></param>
    /// <param name="DT"></param>
    /// <returns></returns>
    protected DataTable GetDaySJD(DateTime date, DataTable DT)
    {
        DataTable dt = DT.Copy();

        dt.Columns.Add("StartTime", System.Type.GetType("System.DateTime"));
        dt.Columns.Add("EndTime", System.Type.GetType("System.DateTime"));

        foreach (DataRow row in dt.Rows)
        {
            string strDate = date.ToString("yyyy-MM-dd");
            string strStartTime = row["start"].ToString();
            string strEndTime = row["end"].ToString();

            row["StartTime"] = Convert.ToDateTime(string.Format("{0} {1}", strDate, strStartTime));
            row["EndTime"] = Convert.ToDateTime(string.Format("{0} {1}", strDate, strEndTime));
        }

        return dt;
    }

    /// <summary>
    /// 计算两个时间点之间的时间差（单位：分钟）
    /// </summary>
    /// <param name="dt1"></param>
    /// <param name="dt2"></param>
    /// <returns></returns>
    protected Double GetM(DateTime dt1, DateTime dt2)
    {
        TimeSpan ts = dt1.Subtract(dt2);

        Double intD = ts.Days;
        Double intH = ts.Hours;
        Double intM = ts.Minutes;

        return Math.Abs(intD * 24 * 60 + intH * 60 + intM);
    }
    #endregion

    protected void Button5_Click(object sender, EventArgs e)
    {
        try
        {
            //DateTime dt1 = Convert.ToDateTime("2020-08-10 00:00:00");
            //DateTime dt2 = Convert.ToDateTime("2020-08-10 24:00:00");

            //TimeSpan ts = dt1.Subtract(dt2);

            Double dblgs = 56.4536512561345;
            int intgs = Convert.ToInt32(dblgs);

            //Random exm = new Random();
            //int result = exm.Next(1, 10);

            DisplayMessage(intgs.ToString(), true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}