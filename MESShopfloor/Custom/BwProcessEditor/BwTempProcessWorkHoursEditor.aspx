﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/uMESMasterPage.master" CodeFile="BwTempProcessWorkHoursEditor.aspx.cs" Inherits="Custom_BwProcessEditor_BwTempProcessWorkHoursEditor" %>
<%@ Register Assembly="Infragistics2.Web.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.ListControls" TagPrefix="ig" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetWorkFlowInfo.ascx" TagName="getWorkFlowInfo"
    TagPrefix="gPI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" style="margin-top:-15px">
        <table border="0" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;">
                 <colgroup>
                  <col width="200">
                  <col width="200">
                  <col width="auto">
                </colgroup>
                <tr>
                    <td class="tdRightAndBottom">
                         <div>产品</div>
                        <asp:TextBox ID="txtProduct" runat="server" class="stdTextBoxFull" Enabled="true" ></asp:TextBox>       
                    </td>
                    <td class="tdRightAndBottom">
                       <div>工艺路线</div>
                        <asp:TextBox ID="txtWorkflow" runat="server" class="stdTextBoxFull" Enabled="true" ></asp:TextBox>     
                    </td>
                    <td class="tdRightAndBottom" valign="bottom">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置" Visible="true"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            
            </table>
          <table border="0" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;">
                <colgroup>
                  <col width="600">
                  <col width="auto">
                </colgroup>
                 <tr>
                     <td class="tdNoBorder" style="font-weight:400">
                          <div>工艺信息</div>
                        <igtbl:UltraWebGrid ID="wgWorkflow" runat="server" Height="200px" Width="300px" OnActiveRowChange="wgWorkflow_ActiveRowChange">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductName" Key="ProductName" Width="150px">
                                             <Header Caption="图号"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductDesc" Key="ProductDesc" Width="150px">
                                             <Header Caption="产品名称"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductRev" Key="ProductRev" Width="80px" Hidden="true">
                                             <Header Caption="产品版本"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowName" Key="WorkflowName" Width="150px">
                                             <Header Caption="工艺"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowRev" Key="WorkflowRev" Width="80px">
                                             <Header Caption="工艺版本"></Header>
                                        </igtbl:UltraGridColumn>
                                         
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductID" Key="ProductID" Width="100px" Hidden="true">
                                             <Header Caption="ProductID"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowID" Key="WorkflowID" Width="100px" Hidden="true">
                                             <Header Caption="WorkflowID"></Header>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="false">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px" Font-Bold="false">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Font-Bold="false"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                     </td>
                     <td class="tdNoBorder" style="font-weight:400">
                          <div>工序信息</div>
                        <igtbl:UltraWebGrid ID="wgSpec" runat="server" Height="200px" Width="300px" OnInitializeRow="wgSpec_InitializeRow">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:UltraGridColumn Key="ckSelect" Type="CheckBox" Width="40px" AllowUpdate="Yes">
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Key="WorkflowStepName" Width="150px">
                                             <Header Caption="工序"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="TeamName" Key="TeamName" Width="100px">
                                             <Header Caption="班组"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="FactoryName" Key="FactoryName" Width="100px">
                                             <Header Caption="分厂"></Header>
                                        </igtbl:UltraGridColumn>
                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="" Key="EditUnitWorkTime" Width="100px" AllowUpdate="Yes">
                                             <Header Caption="单件工时"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="" Key="EditSetupWorkTime" Width="100px"  AllowUpdate="Yes">
                                             <Header Caption="准备工时"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="unitworktime" Key="UnitWorkTime" Width="100px" Hidden="true">
                                             <Header Caption="单件工时"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="setupworktime" Key="SetupWorkTime" Width="100px" Hidden="true">
                                             <Header Caption="准备工时"></Header>
                                        </igtbl:UltraGridColumn>
                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SynergicName" Key="SynergicName" Width="100px" Hidden="false">
                                             <Header Caption="是否外协"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="IsModifyWorkHours" Key="IsModifyWorkHours" Width="100px" Hidden="false">
                                             <Header Caption="是否修改"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="OperationName" Key="OperationName" Width="100px" Hidden="true">
                                             <Header Caption="operation"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="operationid" Key="OperationID" Width="100px" Hidden="true">
                                             <Header Caption="operationid"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="issynergic" Key="IsSynergic" Width="100px" Hidden="true">
                                             <Header Caption="issynergic"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SpecNo" Key="SpecNo" Width="100px" Hidden="true">
                                             <Header Caption="SpecNo"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="specrevision" Key="SpecRev" Width="100px" Hidden="true">
                                             <Header Caption="specrevision"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailopname" Key="DetailOpName" Width="100px" Hidden="true">
                                             <Header Caption="detailopname"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailsetupworktime" Key="DetailSetupWorktime" Width="100px" Hidden="true">
                                             <Header Caption="detailsetupworktime"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailunitworktime" Key="DetailUnitWorktime" Width="100px" Hidden="true">
                                             <Header Caption="detailunitworktime"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="specid" Key="specid" Width="100px" Hidden="true">
                                             <Header Caption="specid"></Header>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="690px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="false">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px" Font-Bold="false">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Font-Bold="false"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                     </td>
                 </tr>
            </table>
        <table border="0" cellpadding="5" cellspacing="0" width="100%" >
                 <tr>
                     <td class="tdNoBorder">
                          <asp:Button ID="btnSave" runat="server" Text="保存"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click"  />
                         <asp:Button ID="btnSubmit" runat="server" Text="提交" style="margin-left:20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSubmit_Click"   />
                         <asp:Button ID="Button1" runat="server" Text="图纸工艺查看" style="margin-left:20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                     </td>
                 </tr>
            </table>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
