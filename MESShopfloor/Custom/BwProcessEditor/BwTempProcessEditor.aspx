﻿<%@ Page Title="" Language="C#" MasterPageFile="~/uMESMasterPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="BwTempProcessEditor.aspx.cs" Inherits="Custom_BwProcessEditor_BwTempProcessEditor" %>

<%@ Register Assembly="Infragistics2.Web.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.Web.UI.ListControls" TagPrefix="ig" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetWorkFlowInfo.ascx" TagName="getWorkFlowInfo"
    TagPrefix="gPI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server"  ></asp:ScriptManager>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
        <div>
            
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" style="margin-top:-15px">
            <table border="0" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;">
                 <colgroup>
                  <col width="220">
                  <col width="300">
                  <col width="auto">
                </colgroup>
                <tr>
                    <td class="tdRightAndBottom">
                        <div>产品</div>
                        <gPI:getProductInfo ID="getProductInfo" runat="server" />
                    </td>
                    <td class="tdRightAndBottom">
                       <div>工艺路线</div>
                       <gPI:getWorkFlowInfo ID="getWorkFlowInfo" runat="server" />
                    </td>
                    <td class="tdRightAndBottom">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置" Visible="true"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            
            </table>
            <table border="0" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;">
                 <colgroup>
                  <col width="130">
                  <col width="130">
                  <col width="190">
                  <col width="130">
                  <col width="130">
                  <col width="110">
                  <col width="110">
                  <col width="120">
                  <col width="auto" />
                </colgroup>
                <tr>
                    <td colspan="9" class="tdNoBorder">
                         <table border="0" cellpadding="5" cellspacing="0"  >
                            
                              <tr>
                                      <td class="tdNoBorder" colspan="1" style="font-weight:400;" width="650">
                                          <div>
                                              源工艺信息</div>
                                          <igtbl:UltraWebGrid ID="wgSourceWorkflow" runat="server" Height="200px" Width="300px">
                                              <Bands>
                                                  <igtbl:UltraGridBand>
                                                      <Columns>
                                                          <igtbl:UltraGridColumn AllowUpdate="Yes" Key="ckSelect" Type="CheckBox" Width="40px">
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Key="WorkflowStepName" Width="150px">
                                                              <Header Caption="工序">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="TeamName" Key="TeamName" Width="100px">
                                                              <Header Caption="班组">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="FactoryName" Key="FactoryName" Width="100px">
                                                              <Header Caption="分厂">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="unitworktime" Key="UnitWorkTime" Width="100px">
                                                              <Header Caption="单件工时">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="setupworktime" Key="SetupWorkTime" Width="100px">
                                                              <Header Caption="准备工时">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SynergicName" Hidden="false" Key="SynergicName" Width="100px">
                                                              <Header Caption="是否外协">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="OperationName" Hidden="true" Key="OperationName" Width="100px">
                                                              <Header Caption="operation">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="operationid" Hidden="true" Key="OperationID" Width="100px">
                                                              <Header Caption="operationid">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="issynergic" Hidden="true" Key="IsSynergic" Width="100px">
                                                              <Header Caption="issynergic">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SpecNo" Hidden="true" Key="SpecNo" Width="100px">
                                                              <Header Caption="SpecNo">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailopname" Hidden="true" Key="DetailOpName" Width="100px">
                                                              <Header Caption="detailopname">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailsetupworktime" Hidden="true" Key="DetailSetupWorktime" Width="100px">
                                                              <Header Caption="detailsetupworktime">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailunitworktime" Hidden="true" Key="DetailUnitWorktime" Width="100px">
                                                              <Header Caption="detailunitworktime">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                      </Columns>
                                                      <AddNewRow View="NotSet" Visible="NotSet">
                                                      </AddNewRow>
                                                  </igtbl:UltraGridBand>
                                              </Bands>
                                              <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                                  <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="650px">
                                                  </FrameStyle>
                                                  <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                                  </RowAlternateStyleDefault>
                                                  <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                                      <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                      <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                      </PagerStyle>
                                                  </Pager>
                                                  <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                                  </EditCellStyleDefault>
                                                  <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                                      <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                  </FooterStyleDefault>
                                                  <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                                      <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                      <Padding Bottom="3px" Top="2px" />
                                                      <Padding Bottom="3px" Top="2px" />
                                                  </HeaderStyleDefault>
                                                  <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Font-Bold="false" Height="25px">
                                                      <Padding Left="3px" />
                                                  </RowSelectorStyleDefault>
                                                  <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Bold="false" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                                      <Padding Left="3px" />
                                                      <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                                  </RowStyleDefault>
                                                  <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                                  </GroupByRowStyleDefault>
                                                  <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                                  </SelectedRowStyleDefault>
                                                  <GroupByBox Hidden="True">
                                                      <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                                      </BoxStyle>
                                                  </GroupByBox>
                                                  <AddNewBox>
                                                      <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                                          <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                      </BoxStyle>
                                                  </AddNewBox>
                                                  <ActivationObject BorderColor="" BorderWidth="">
                                                  </ActivationObject>
                                                  <FilterOptionsDefault FilterUIType="HeaderIcons">
                                                      <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                                          <Padding Left="2px" />
                                                      </FilterDropDownStyle>
                                                      <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                                      </FilterHighlightRowStyle>
                                                      <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                                          <Padding Left="2px" />
                                                      </FilterOperandDropDownStyle>
                                                  </FilterOptionsDefault>
                                              </DisplayLayout>
                                          </igtbl:UltraWebGrid>
                                      </td>
                                      <td class="tdNoBorder" style="width:80px" align="center">
                                          <div style="margin-left:0px">
                                              <asp:Button ID="btnMove" runat="server" CssClass="searchButton" EnableTheming="True" OnClick="btnMove_Click" Text="=&gt;" Width="60px" ToolTip="往右添加" />
                                              <asp:Button ID="btnDelRow" runat="server" CssClass="searchButton" EnableTheming="True" OnClick="btnDelRow_Click" Style="margin-top:10px" Text="-" Width="60px" ToolTip="删除" />
                                          </div>
                                      </td>
                                      <td class="tdNoBorder" colspan="1" style="font-weight:400;width:650px">
                                          <div>
                                              临时艺信息</div>
                                          <igtbl:UltraWebGrid ID="wgTargetWorkflow" runat="server" Height="200px" OnActiveRowChange="wgTargetWorkflow_ActiveRowChange" Width="300px">
                                              <Bands>
                                                  <igtbl:UltraGridBand>
                                                      <Columns>
                                                          <igtbl:UltraGridColumn AllowUpdate="Yes" Hidden="true" Key="ckSelect" Type="CheckBox" Width="40px">
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Key="WorkflowStepName" Width="150px">
                                                              <Header Caption="工序">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="TeamName" Key="TeamName" Width="100px">
                                                              <Header Caption="班组">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="FactoryName" Key="FactoryName" Width="100px">
                                                              <Header Caption="分厂">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="unitworktime" Key="UnitWorkTime" Width="100px">
                                                              <Header Caption="单件工时">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="setupworktime" Key="SetupWorkTime" Width="100px">
                                                              <Header Caption="准备工时">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SynergicName" Hidden="false" Key="SynergicName" Width="100px">
                                                              <Header Caption="是否外协">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="OperationName" Hidden="true" Key="OperationName" Width="100px">
                                                              <Header Caption="operation">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="operationid" Hidden="true" Key="OperationID" Width="100px">
                                                              <Header Caption="operationid">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="issynergic" Hidden="true" Key="IsSynergic" Width="100px">
                                                              <Header Caption="issynergic">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SpecNo" Hidden="true" Key="SpecNo" Width="100px">
                                                              <Header Caption="SpecNo">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailopname" Hidden="true" Key="DetailOpName" Width="100px">
                                                              <Header Caption="detailopname">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailsetupworktime" Hidden="true" Key="DetailSetupWorktime" Width="100px">
                                                              <Header Caption="detailsetupworktime">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="detailunitworktime" Hidden="true" Key="DetailUnitWorktime" Width="100px">
                                                              <Header Caption="detailunitworktime">
                                                              </Header>
                                                          </igtbl:UltraGridColumn>
                                                      </Columns>
                                                      <AddNewRow View="NotSet" Visible="NotSet">
                                                      </AddNewRow>
                                                  </igtbl:UltraGridBand>
                                              </Bands>
                                              <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                                  <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="650px">
                                                  </FrameStyle>
                                                  <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                                  </RowAlternateStyleDefault>
                                                  <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                                      <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                      <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                      </PagerStyle>
                                                  </Pager>
                                                  <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                                  </EditCellStyleDefault>
                                                  <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                                      <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                  </FooterStyleDefault>
                                                  <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                                      <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                      <Padding Bottom="3px" Top="2px" />
                                                      <Padding Bottom="3px" Top="2px" />
                                                  </HeaderStyleDefault>
                                                  <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                                      <Padding Left="3px" />
                                                  </RowSelectorStyleDefault>
                                                  <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                                      <Padding Left="3px" />
                                                      <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                                  </RowStyleDefault>
                                                  <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                                  </GroupByRowStyleDefault>
                                                  <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                                  </SelectedRowStyleDefault>
                                                  <GroupByBox Hidden="True">
                                                      <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                                      </BoxStyle>
                                                  </GroupByBox>
                                                  <AddNewBox>
                                                      <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                                          <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                      </BoxStyle>
                                                  </AddNewBox>
                                                  <ActivationObject BorderColor="" BorderWidth="">
                                                  </ActivationObject>
                                                  <FilterOptionsDefault FilterUIType="HeaderIcons">
                                                      <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                                          <Padding Left="2px" />
                                                      </FilterDropDownStyle>
                                                      <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                                      </FilterHighlightRowStyle>
                                                      <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                                          <Padding Left="2px" />
                                                      </FilterOperandDropDownStyle>
                                                  </FilterOptionsDefault>
                                              </DisplayLayout>
                                          </igtbl:UltraWebGrid>
                                      </td>
                                  </tr>
                         </table>
                    </td>
                </tr>
                <tr>
                    <td class="tdNoBorder" colspan="9">
                        <asp:Button ID="btnSelectAll" runat="server" Text="全选"
                            CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnSelectAll_Click"  />
                        <asp:Button ID="btnInRevert" runat="server" Text="反选"
                                    CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnInRevert_Click" />
                  
                    </td>
                </tr>
                <tr style="display:none">
                    <td class="tdNoBorder" colspan="9" style="font-weight:400">
                        <div>工艺详细信息</div> 
                    </td>
                </tr>
                <tr style="display:none">
                     <td class="tdNoBorder" colspan="2">
                        <div>工艺名称</div>
                         <asp:TextBox ID="txtWorkflowDesc" runat="server" class="stdTextBoxFull"></asp:TextBox>
                          
                    </td>
                    <td class="tdNoBorder" colspan="7">

                    </td>
                </tr>
                <tr>
                    <td class="tdNoBorder" colspan="9" style="font-weight:400">
                        <div>工序详细信息</div> 
                    </td>
                </tr>
                <tr>
                     <td class="tdNoBorder" style="position:relative">                           
                           <div>临时工艺版本</div>
                        <asp:TextBox ID="txtWorklfowRev" runat="server" class="stdTextBoxFull" Width="100px"  Enabled="False" style="float:left"></asp:TextBox>      
                                <asp:Button ID="btnGenerateRev" runat="server" Text="…" Visible="true" style="float:right;margin:0"
                            CssClass="searchButton" EnableTheming="True"  ToolTip="生成临时版本号" OnClick="btnGenerateRev_Click" />                                      
                    </td>
                    <td class="tdNoBorder">
                         <div>工序号</div>
                         <asp:TextBox ID="txtSpecNo" runat="server" class="stdTextBoxFull" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                               
                    </td>
                    <td class="tdNoBorder">                           
                           <div>工序名称</div>
                         <asp:DropDownList ID="ddlOperation" runat="server" Width="180px" Height="28px" Visible="true" Style="font-size: 16px" AutoPostBack="True" OnSelectedIndexChanged="ddlOperation_SelectedIndexChanged"></asp:DropDownList>     
                   </td>
                    <td class="tdNoBorder">                           
                           <div>班组</div>
                        <asp:TextBox ID="txtTeam" runat="server" class="stdTextBoxFull" Enabled="False" ></asp:TextBox>                                                   
                    </td>
                     <td class="tdNoBorder">                           
                           <div>分厂</div>
                        <asp:TextBox ID="txtFactory" runat="server" class="stdTextBoxFull"  Enabled="False" ></asp:TextBox>                                                   
                    </td>
                     <td class="tdNoBorder" style="display:none">
                         <div>单件工时</div>
                         <asp:TextBox ID="txtUnitWorkTime" runat="server" class="stdTextBoxFull" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                               
                    </td>
                     <td class="tdNoBorder" style="display:none">
                         <div>准备工时</div>
                         <asp:TextBox ID="txtSetupWorkTime" runat="server" class="stdTextBoxFull" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                               
                    </td>
                    <td class="tdNoBorder" valign="bottom" style="display:none">
                         <input id="ckSynergic" type="checkbox" runat="server" />
                        <asp:Label runat="server">是否外协</asp:Label>   
                    </td>
                    <td class="tdNoBorder" valign="bottom" colspan="4">
                         <asp:Button ID="Button1" runat="server" Text="添加"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                        <asp:Button ID="btnAdjust" runat="server" Text="调整" Visible="true" style="margin-left:20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnAdjust_Click" />
                    </td>
                </tr>
                <tr>
                    <td  class="tdNoBorder" colspan="9">
                        <div>附件</div> 
                     <asp:FileUpload ID="bffSelectPath" Style="height: 26px; width: 380px;"
                                runat="server" />
                                                   
                        

                    </td>
                </tr>
            </table>
        
    </igmisc:WebAsyncRefreshPanel>
             <table border="0" cellpadding="5" cellspacing="0" width="100%" >
                 <tr>
                     <td class="tdNoBorder">
                          <asp:Button ID="btnSave" runat="server" Text="保存工艺" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click"  />
                         <asp:Button ID="btnImport" runat="server" Text="工艺挂附件" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)" style="margin-left:20px"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnImport_Click" />
                        <asp:Button ID="btnDel" runat="server" Text="删除工艺" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)" Visible="true" style="margin-left:20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnDel_Click" />
                     </td>
                 </tr>
            </table>
        </div>
       <%-- </ContentTemplate>
         <Triggers>
            <asp:PostBackTrigger ControlID="btnImport" />
        </Triggers>
    </asp:UpdatePanel>--%>

</asp:Content>

