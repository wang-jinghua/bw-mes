﻿/*
'Copyright ?1995-2007, Camstar Systems, Inc. All Rights Reserved.
'Description:工艺编制
'Copyright (c) : 通力凯顿（北京）系统集成有限公司
'Writer:Wangjh
'create Date:2020-11-22
*/
using Infragistics.WebUI.UltraWebGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.DBUtility;
using Newtonsoft.Json;
using System.IO;
using System.Configuration;

public partial class Custom_BwProcessEditor_BwTempProcessEditor : BaseForm
{
    uMESWorkflowEditorBusiness workflowBal=new uMESWorkflowEditorBusiness();
    uMESCommonBusiness commonBal = new uMESCommonBusiness();
    string businessName = "基础数据", parentName = "workflow";
    protected void Page_Load(object sender, EventArgs e)
    {
     //   WebPanel = WebAsyncRefreshPanel1;
        getProductInfo.GetProductData += new GetProductInfo_ascx.GetProductDataEventHandler(LoadProductData);
        getWorkFlowInfo.GetWorkFlowData += new GetWorkFlowInfo_ascx.GetWorkFlowDataEventHandler(LoadWorkflowData);
        getProductInfo.DDlProductDataChanged += new GetProductInfo_ascx.DDlProductDataChangedEventHandler(DDlProductDataChanged);
        getWorkFlowInfo.DDlWorkFlowDataChanged += new GetWorkFlowInfo_ascx.DDlWorkFlowDataChangedEventHandler(DDlWorkflowDataChanged);
       DisplayMessage("",true);
        if (IsPostBack == false)
        {
            getProductInfo.GetProductControlDiv.Style["Width"] = "190px";
            getWorkFlowInfo.GetTextBox.Enabled = false;
            getWorkFlowInfo.GetBtnSearch.Visible = false;
            //初始化operation
            var dt = OracleHelper.QueryDataByEntity(new ExcuteEntity("Operation",ExcuteType.selectAll));
            ddlOperation.DataTextField = "operationname";
            ddlOperation.DataValueField = "operationid";
            ddlOperation.DataSource = dt;
            ddlOperation.DataBind();
            ddlOperation.Items.Insert(0, "");
        }
    }

    #region 方法
    void DDlProductDataChanged()
    {
        getWorkFlowInfo.InitControl();
        ResetData(false, true, true);
        string productName = "";

        string productRev = "";

        GetProductInfo(ref productName, ref productRev);

        if (productName == "")
        {
            return;
        }

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["ProductName"] = productName;
        para["ProductRev"] = productRev;
        para["AllStatus"] = "1";

        getWorkFlowInfo.SearchWorkFlow(para, 1);

    }

    void DDlWorkflowDataChanged() {
        ResetData(false, true, true);
       
    }

    void LoadProductData()
    {
        if (getProductInfo.Message.Length > 0)
        {
            DisplayMessage( getProductInfo.Message, false);
        }
    }

    void LoadWorkflowData()
    {
        if (getWorkFlowInfo.Message.Length > 0)
        {
            DisplayMessage(getWorkFlowInfo.Message, false);
        }
    }

    //获取产品名及版本
    void GetProductInfo(ref string productName, ref string productRev)
    {
        string productInfo = getProductInfo.ProducText;

        if (productInfo == "")
        {
            return;
        }

        productName = productInfo.Substring(0, productInfo.IndexOf(":"));
        productRev = productInfo.Substring(productInfo.IndexOf(":") + 1, productInfo.IndexOf("(", productName.Length) - productInfo.IndexOf(":") - 1);

    }

    //获取选择的工艺名及版本
    void GetWorkflowInfo(ref string workflowName, ref string workflowRev)
    {
        string strWrokflow = getWorkFlowInfo.WorkFlowText;
        GetWorkflowInfo(strWrokflow,ref workflowName,ref workflowRev);

    }
    /// <summary>
    /// 获取分割后的工艺名和版本
    /// </summary>
    /// <param name="strWorklfow"></param>
    /// <param name="workflowName"></param>
    /// <param name="workflowRev"></param>
    void GetWorkflowInfo(string strWorklfow,ref string workflowName, ref string workflowRev) {
        if (strWorklfow!= "请选择工艺路线" && strWorklfow != ""&& strWorklfow != string.Empty)
        {
            string[] array = strWorklfow.Split(':');

            workflowName = array[0];

            workflowRev = array[1];
        }
    }

    /// <summary>
    /// 清除数据
    /// </summary>
    /// <param name="condition"></param>
    /// <param name="grid"></param>
    /// <param name="detail"></param>
    void ResetData(bool condition,bool grid,bool detail) {
        if (condition)
        {
            getProductInfo.InitControl();
            getWorkFlowInfo.InitControl();
        }
        if (grid) {
            wgSourceWorkflow.Rows.Clear(); wgTargetWorkflow.Rows.Clear();
            txtWorklfowRev.Text = "";
        }
        if (detail) {
            txtSpecNo.Text = "";
            ddlOperation.SelectedValue = "";
            txtTeam.Text = "";
            txtFactory.Text = "";
            txtUnitWorkTime.Text = "";
            txtSetupWorkTime.Text = "";
            txtWorkflowDesc.Text = "";
        }
    }

    /// <summary>
    /// 网右添加工序信息
    /// </summary>
    ResultModel addRightStep2() {
        RowsCollection allRows = wgSourceWorkflow.Rows;
        var selectRows = from UltraGridRow r in allRows
                         where Convert.ToBoolean(r.Cells.FromKey("ckSelect").Text)== true
                         select r;
        if (selectRows.Count() == 0) {
            return new ResultModel(false,"请选择记录");
        }
        var targetSteps = wgTargetWorkflow.Rows;
        
        //检查不能含有相同工序;
        var commonSteps = from UltraGridRow r in selectRows
                          join UltraGridRow r2 in targetSteps on r.Cells.FromKey("WorkflowStepName").Text equals r2.Cells.FromKey("WorkflowStepName").Text
                        select r;
        if (commonSteps.Count() > 0) {
            return new ResultModel(false, "所选工序中含有工序已添加到右边列表中");
        }

        //开始追加到目标列表中
        foreach (UltraGridRow r in selectRows) {
            targetSteps.Add();
            var lastRow = targetSteps[targetSteps.Count-1];
            lastRow.Cells.FromKey("WorkflowStepName").Value = r.Cells.FromKey("WorkflowStepName").Value;
            lastRow.Cells.FromKey("TeamName").Value = r.Cells.FromKey("TeamName").Value;
            lastRow.Cells.FromKey("FactoryName").Value = r.Cells.FromKey("FactoryName").Value;
            lastRow.Cells.FromKey("UnitWorkTime").Value = r.Cells.FromKey("UnitWorkTime").Value;
            lastRow.Cells.FromKey("SetupWorkTime").Value = r.Cells.FromKey("SetupWorkTime").Value;
            lastRow.Cells.FromKey("OperationName").Value = r.Cells.FromKey("OperationName").Value;
            lastRow.Cells.FromKey("SpecNo").Value = r.Cells.FromKey("SpecNo").Value;
        }


        //重新排序
         var rr =from UltraGridRow r in targetSteps
                           orderby int.Parse(r.Cells.FromKey("SpecNo").Text) ascending
                           select r;

        wgTargetWorkflow.Rows.Clear();

        wgTargetWorkflow.DataSource = rr;


        //wgTargetWorkflow.DataBind();



        return new ResultModel(true,"");

    }

    /// <summary>
    /// 网右添加工序信息
    /// </summary>
    ResultModel addRightStep()
    {
        RowsCollection allRows = wgSourceWorkflow.Rows;
        var selectRows = from UltraGridRow r in allRows
                         where Convert.ToBoolean(r.Cells.FromKey("ckSelect").Text) == true
                         select r;
        if (selectRows.Count() == 0)
        {
            return new ResultModel(false, "请选择记录");
        }
        var targetSteps = uMESCommonFunction.GetGridData(wgTargetWorkflow);

        //检查不能含有相同工序;
        var commonSteps = from UltraGridRow r in selectRows
                          join DataRow r2 in targetSteps.Rows on r.Cells.FromKey("WorkflowStepName").Text equals  r2["WorkflowStepName"].ToString()
                          select r;
        if (commonSteps.Count() > 0)
        {
            return new ResultModel(false, "所选工序中含有工序已添加到右边列表中");
        }

        //开始追加到目标列表中
        foreach (UltraGridRow r in selectRows)
        {
            DataRow newRow= targetSteps.NewRow();

            newRow["WorkflowStepName"] = r.Cells.FromKey("WorkflowStepName").Value;
            newRow["TeamName"] = r.Cells.FromKey("TeamName").Value;
            newRow["FactoryName"] = r.Cells.FromKey("FactoryName").Value;
            newRow["UnitWorkTime"] = r.Cells.FromKey("UnitWorkTime").Value;
            newRow["SetupWorkTime"] = r.Cells.FromKey("SetupWorkTime").Value;
            newRow["OperationName"] = r.Cells.FromKey("OperationName").Value;
            newRow["OperationID"] = r.Cells.FromKey("OperationID").Value;
            newRow["SpecNo"] = r.Cells.FromKey("SpecNo").Value;
            newRow["DetailOpName"] = r.Cells.FromKey("DetailOpName").Value;
            newRow["DetailSetupWorktime"] = r.Cells.FromKey("DetailSetupWorktime").Value;
            newRow["DetailUnitWorktime"] = r.Cells.FromKey("DetailUnitWorktime").Value;
            newRow["SynergicName"] = r.Cells.FromKey("SynergicName").Value;
            newRow["IsSynergic"] = r.Cells.FromKey("IsSynergic").Value;

            targetSteps.Rows.Add(newRow);
        }

        //重新排序
        var rr = from DataRow r in targetSteps.Rows
                 orderby int.Parse(r["SpecNo"].ToString()) ascending
                 select r;

        wgTargetWorkflow.Rows.Clear();

        wgTargetWorkflow.DataSource = rr.CopyToDataTable();
        wgTargetWorkflow.DataBind();

        return new ResultModel(true, "");

    }

    /// <summary>
    /// 添加一行工序
    /// </summary>
    /// <returns></returns>
    ResultModel AddRowStep() {
        ResultModel re = new ResultModel(false, "");
        if (string.IsNullOrWhiteSpace(txtWorklfowRev.Text)) {
            return new ResultModel(false,"请先选择工艺");
        }
        if (string.IsNullOrWhiteSpace(txtSpecNo.Text))
        {
            return new ResultModel(false, "请输入工序号");
        }
        else {
            //if (ckSynergic.Checked == false && (string.IsNullOrWhiteSpace(txtUnitWorkTime.Text)|| string.IsNullOrWhiteSpace(txtSetupWorkTime.Text))) {
            //    return new ResultModel(false, "非外协工序请输入工时");
            //}
        }
        if (ckSynergic.Checked == false&&string.IsNullOrWhiteSpace(txtTeam.Text))
        {
            return new ResultModel(false, "无班组信息");
        }
        var targetSteps = uMESCommonFunction.GetGridData(wgTargetWorkflow);//获取目标工序信息
        var newRow = targetSteps.NewRow();

        string productName="", proRev="";
        GetProductInfo(ref productName,ref proRev);

        newRow["SpecNo"] = txtSpecNo.Text; newRow["WorkflowStepName"] = productName+"-"+txtSpecNo.Text+"-"+ddlOperation.SelectedItem.Text; newRow["TeamName"] = txtTeam.Text; newRow["FactoryName"] = txtFactory.Text;
        newRow["UnitWorkTime"] = txtUnitWorkTime.Text; newRow["SetupWorkTime"] = txtSetupWorkTime.Text; newRow["OperationName"] = ddlOperation.SelectedItem.Text;
        newRow["OperationID"] = ddlOperation.SelectedValue;
        if (ckSynergic.Checked)
        {
            newRow["SynergicName"] = "是";
            newRow["IsSynergic"] = 1;
        }
        else {
            newRow["SynergicName"] = "否";
            newRow["IsSynergic"] = 0;
        }
        

        if (targetSteps.Select("WorkflowStepName='"+ newRow["WorkflowStepName"].ToString()+"'").Length>0) {
            return new ResultModel(false, "已含有此工序");
        }
        if (targetSteps.Select("SpecNo='" + newRow["SpecNo"].ToString() + "'").Length > 0)
        {
            return new ResultModel(false, "已含有此工序号");
        }

        targetSteps.Rows.Add(newRow);

        //重新排序
        var temp = from DataRow r in targetSteps.Rows
                   orderby int.Parse(r["specno"].ToString()) ascending
                   select r;
        targetSteps = temp.CopyToDataTable();

        wgTargetWorkflow.DataSource = targetSteps;
        wgTargetWorkflow.DataBind();

        return new ResultModel(true,"");

    }

    /// <summary>
    /// 修改一工序
    /// </summary>
    /// <returns></returns>
    ResultModel AdjustRowStep()
    {
        ResultModel re = new ResultModel(false, "");
        UltraGridRow activeRow = wgTargetWorkflow.DisplayLayout.ActiveRow;
        if (activeRow == null) {
            return new ResultModel(false, "请先选择工序行");
        }
        if (string.IsNullOrWhiteSpace(txtWorklfowRev.Text))
        {
            return new ResultModel(false, "请先选择工艺");
        }
        if (string.IsNullOrWhiteSpace(txtSpecNo.Text))
        {
            return new ResultModel(false, "请输入工序号");
        }
        else
        {
            //if (ckSynergic.Checked == false && (string.IsNullOrWhiteSpace(txtUnitWorkTime.Text) || string.IsNullOrWhiteSpace(txtSetupWorkTime.Text)))
            //{
            //    return new ResultModel(false, "非外协工序请输入工时");
            //}
        }
        if (ckSynergic.Checked == false && string.IsNullOrWhiteSpace(txtTeam.Text))
        {
            return new ResultModel(false, "无班组信息");
        }
        if (activeRow.Cells.FromKey("SpecNo").Text != txtSpecNo.Text) {
            return new ResultModel(false, "无法调整工序号");
        }

        string productName = "", proRev = "";
        GetProductInfo(ref productName, ref proRev);

        activeRow.Cells.FromKey("SpecNo").Value = txtSpecNo.Text;
        activeRow.Cells.FromKey("WorkflowStepName").Value = productName + "-" + txtSpecNo.Text + "-" + ddlOperation.SelectedItem.Text;
        activeRow.Cells.FromKey("TeamName").Value = txtTeam.Text;
        activeRow.Cells.FromKey("FactoryName").Value = txtFactory.Text;
        activeRow.Cells.FromKey("UnitWorkTime").Value = txtUnitWorkTime.Text;
        activeRow.Cells.FromKey("SetupWorkTime").Value = txtSetupWorkTime.Text;
        activeRow.Cells.FromKey("OperationName").Value = ddlOperation.SelectedItem.Text;
        activeRow.Cells.FromKey("OperationID").Value = ddlOperation.SelectedValue;
        if (ckSynergic.Checked)
        {
            activeRow.Cells.FromKey("SynergicName").Value = "是";
            activeRow.Cells.FromKey("IsSynergic").Value = 1;
        }
        else
        {
            activeRow.Cells.FromKey("SynergicName").Value = "否";
            activeRow.Cells.FromKey("IsSynergic").Value = 0;
        }

        return new ResultModel(true, "");

    }

    /// <summary>
    /// 点击行
    /// </summary>
    void ActiveTargetRow() {
        var activeRow = wgTargetWorkflow.DisplayLayout.ActiveRow;
        if (activeRow == null)
            return;
        ResetData(false,false,true);
        txtSpecNo.Text = activeRow.Cells.FromKey("SpecNo").Text;
        ddlOperation.SelectedValue = activeRow.Cells.FromKey("OperationID").Text;
        txtTeam.Text= activeRow.Cells.FromKey("TeamName").Text;
        txtFactory.Text= activeRow.Cells.FromKey("FactoryName").Text;
        txtUnitWorkTime.Text= activeRow.Cells.FromKey("UnitWorkTime").Text;
        txtSetupWorkTime.Text= activeRow.Cells.FromKey("SetupWorkTime").Text;
        ckSynergic.Value= activeRow.Cells.FromKey("IsSynergic").Text;
    }
    /// <summary>
    /// 生成临时工艺版本号
    /// </summary>
    void GenerateRev() {
        int maxLsRevNo = 0;
        foreach (ListItem w in getWorkFlowInfo.GetWFDDL.Items)
        {
            if (w.Text == "请选择工艺路线")
                continue;
            string name = "", rev = "";
            GetWorkflowInfo(w.Text,ref name, ref rev);
            if (!rev.Contains("LS"))
            {
                continue;
            }
            int no = int.Parse(rev.Replace("LS", ""));
            if (no > maxLsRevNo)
                maxLsRevNo = no;

        }
        txtWorklfowRev.Text = "LS" + (++maxLsRevNo).ToString().PadLeft(2, '0');
    }

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <returns></returns>
    ResultModel SaveData() {
        ResultModel re = new ResultModel(false,"");
        re.Data = 0;

        DataTable dsworkflow = new DataTable();
        dsworkflow.Columns.AddRange(new [] { new DataColumn("WorkflowName"), new DataColumn("workFlowRevision"), new DataColumn("Factory") ,new DataColumn("Status"),
        new DataColumn("Description")});
        string workflowName = "", oldWorkflowRev = "";
        string workflowRev = txtWorklfowRev.Text;
        GetWorkflowInfo(ref workflowName,ref oldWorkflowRev);
        //判断是否能够更改
        if (oldWorkflowRev == workflowRev)
        {
            DataTable workflow = workflowBal.ValidateWorkflow(workflowName, workflowRev);
            if (workflow.Rows.Count > 0) { 
                if (workflow.Rows[0]["isdispatch"].ToString() != "0" || workflow.Rows[0]["containerNum"].ToString() != "0")
                {
                    return new ResultModel(false, "此工艺已经使用，不能做更改");
                }
                else {
                    re = workflowBal.DeleteWorkflow(UserInfo["ApiEmployeeName"], UserInfo["ApiPassword"], workflowName, workflowRev);
                    if (!re.IsSuccess)
                    { return re; }
                    else {
                        //将老版本更改为默认版本
                        GetWorkflowInfo(getWorkFlowInfo.GetWFDDL.Items[1].Text, ref workflowName, ref oldWorkflowRev);
                    }
                }
            }
            //给结果集增加下次刷新时默认选择的工艺索引
            re.Data = getWorkFlowInfo.GetWFDDL.SelectedIndex;
        }
        //
        DataRow dr = dsworkflow.NewRow();

        dr["WorkflowName"] = workflowName;
        dr["workFlowRevision"] = workflowRev;
        dr["Factory"] = UserInfo["FactoryName"];
        dr["Description"] = txtWorkflowDesc.Text ;dr["Status"] = 2;

        dsworkflow.Rows.Add(dr);

        //工序
        DataTable dtSpec = uMESCommonFunction.GetGridData(wgTargetWorkflow);
        dtSpec.Columns.AddRange(new[] {
            new DataColumn("SpecRev"),new DataColumn("ObjType"),new DataColumn("ObjName"),new DataColumn("ObjRev"),new DataColumn("Description"),
            new DataColumn("StepName")
        });
        foreach (DataRow drSpec in dtSpec.Rows) {
            drSpec["SpecRev"] = workflowRev;
            drSpec["ObjType"] = "Spec";
            drSpec["ObjName"] = drSpec["WorkflowStepName"];
            drSpec["ObjType"] = "Spec";
            drSpec["ObjRev"] = drSpec["SpecRev"];
            drSpec["StepName"]= drSpec["WorkflowStepName"];
        }

        dtSpec.AcceptChanges();

        var re2= workflowBal.CreateNewWorkflow(UserInfo["ApiEmployeeName"],UserInfo["ApiPassword"],dsworkflow,dtSpec, oldWorkflowRev,false, UserInfo["EmployeeName"]);

        re.IsSuccess = re2.IsSuccess;
        re.Message = re2.Message;
        
        if (re.IsSuccess) {
            if (int.Parse(re.Data.ToString()) == 0) {
                re.Data = getWorkFlowInfo.GetWFDDL.Items.Count;
            }
            //继承源工艺的附件信息
            if (oldWorkflowRev != workflowRev) {
                workflowBal.InheritAttachmentInfo(workflowName, oldWorkflowRev, workflowRev);
            }

           
        }

        return re;
        

    }

    /// <summary>
    /// 删除工艺
    /// </summary>
    /// <returns></returns>
    ResultModel DeleteData() {
        ResultModel re = new ResultModel(false, "");

        string workflowName = "", workflowRev = "";
        GetWorkflowInfo(ref workflowName, ref workflowRev);

        if (!workflowRev.Contains("LS")) {
            return new ResultModel(false, "只能删除临时工艺");
        }

        DataTable workflow = workflowBal.ValidateWorkflow(workflowName, workflowRev);
        if (workflow.Rows.Count > 0)
        {
            if (workflow.Rows[0]["isdispatch"].ToString() != "0" || workflow.Rows[0]["containerNum"].ToString() != "0")
            {
                return new ResultModel(false, "此工艺已经使用，不能做更改");
            }
            else
            {
                re = workflowBal.DeleteWorkflow(UserInfo["ApiEmployeeName"], UserInfo["ApiPassword"], workflowName, workflowRev);
                if (!re.IsSuccess)
                { return re; }

                MESAuditLog ml = new MESAuditLog();
                #region 日志赋值
                ml.ParentID = getWorkFlowInfo.WorkFlowValue; ml.ParentName = parentName;
                ml.CreateEmployeeID = UserInfo["EmployeeID"];
                ml.BusinessName = businessName; ml.OperationType = 2;
                ml.Description = "临时工艺:工艺删除" + getWorkFlowInfo.WorkFlowText;
                commonBal.SaveMESAuditLog(ml);
                #endregion
            }
        }

        re.IsSuccess = true;
        return re;
    }

    /// <summary>
    /// 保存到服务器
    /// </summary>
    /// <param name="strpath"></param>
    /// <returns></returns>
    string SaveFileToServer(ref string strpath,string productName,string workflowRev)
    {
        
        var fileName = strpath.Substring(strpath.LastIndexOf(@"\") + 1);
        
       // File.Delete(strpath);

        string newFielName = productName+"-"+ workflowRev+"-"+"改"+"-"+ fileName;

        strpath = ConfigurationManager.AppSettings["AccessoriesHTTPPath"] + newFielName;
        bffSelectPath.SaveAs(strpath);

        bffSelectPath.Dispose();

        return newFielName;
    }
    /// <summary>
    /// 保存附件
    /// </summary>
    /// <returns></returns>
    ResultModel SaveWorkflowDoc() {
        ResultModel re = new ResultModel(false,"");

        var strPath = bffSelectPath.FileName;

        if (strPath == string.Empty)
        {
            return new ResultModel(false, "请选择附件");
        }


        string productName = "";
        string productRev = "";
        GetProductInfo(ref productName, ref productRev);


        string workflowName = "", oldWorkflowRev = "";
        string workflowRev = txtWorklfowRev.Text;
        GetWorkflowInfo(ref workflowName, ref oldWorkflowRev);

        if (!oldWorkflowRev.Contains("LS")) {
            return new ResultModel(false, "只能给临时工艺挂附件");
        }

        string newFileName = SaveFileToServer(ref strPath, productName, oldWorkflowRev);
        
        string workflowId = getWorkFlowInfo.WorkFlowValue;

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["ckattachmentname"] = newFileName;
        para["ckattachtype"] = "其他";
        para["ckurl"] = strPath;
        para["workflowid"] = workflowId;

        workflowBal.SaveObjectDoc(para);


        re.IsSuccess = true;
        return re;
    }

    #endregion

    #region 提示防范重写
    
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    //protected override  void DisplayMessage(string strMessage, Boolean boolResult)
    //{
    //    //strMessage = strMessage.Trim(new[] { '\r', '\n' });
    //    //处理特殊字符 add:Wangjh
    //    strMessage = strMessage.Replace("\"", "");
    //    strMessage = strMessage.Replace("\r", "");
    //    strMessage = strMessage.Replace("\n", "");

    //    string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
    //    ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(),Guid.NewGuid().ToString(), strScript, true);
    //}

    #endregion

    #region 事件

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try {
            ResetData(false,true,true);

            DataTable re = workflowBal.GetWorkflowStepInfoByWorkflowId(getWorkFlowInfo.WorkFlowValue);
            wgSourceWorkflow.DataSource = re;
            wgSourceWorkflow.DataBind();
            //若有临时工艺，直接显示所选临时工艺的版本号
            string strWorkflow = getWorkFlowInfo.WorkFlowText;
            string name = "", rev = "";
            GetWorkflowInfo(strWorkflow, ref name, ref rev);
            if (rev.Contains("LS"))
            {
                txtWorklfowRev.Text = rev;
            }
            else
            {
                GenerateRev();
            }
        } catch (Exception ex) {
            DisplayMessage(ex.Message,false);
        }
    }
    
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        try
        {
            ResetData(true,true,true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    //往右添加工序信息
    protected void btnMove_Click(object sender, EventArgs e)
    {
        try
        {
            ResetData(false, false, true);
         var re=   addRightStep();
            if (!re.IsSuccess)
                DisplayMessage(re.Message, re.IsSuccess);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnDelRow_Click(object sender, EventArgs e)
    {
        try
        {
            var index = wgTargetWorkflow.DisplayLayout.ActiveRow;
            if (index == null)
            {
                DisplayMessage("请先选择记录",false);
                return;
            }
            wgTargetWorkflow.Rows.Remove(index);
            DisplayMessage("",true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    
    protected void ddlOperation_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            txtTeam.Text = "";txtFactory.Text = "";
            DataTable dt = workflowBal.GetOperationInfoById(ddlOperation.SelectedValue);

            if (dt.Rows.Count == 0)
                return;
            txtTeam.Text = dt.Rows[0]["TeamName"].ToString();
            
            txtFactory.Text = dt.Rows[0]["FactoryName"].ToString();

            //如果有外协厂则标记为外协序
            if (!dt.Rows[0].IsNull("synergiccompanyid"))
                ckSynergic.Checked = true;
            else
                ckSynergic.Checked = false;
           // DisplayMessage("", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try {
          var re=  AddRowStep();
            if (re.IsSuccess == false) {
                DisplayMessage(re.Message,false);
            }
        } catch (Exception ex) {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void wgTargetWorkflow_ActiveRowChange(object sender, RowEventArgs e)
    {
        try
        {
            ActiveTargetRow();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnAdjust_Click(object sender, EventArgs e)
    {
        try
        {
            var re = AdjustRowStep();
            if (re.IsSuccess == false)
            {
                DisplayMessage(re.Message, false);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    protected void btnGenerateRev_Click(object sender, EventArgs e)
    {
        try
        {
            GenerateRev();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            var re = SaveData();
            if (!re.IsSuccess)
            {
                DisplayMessage(re.Message,re.IsSuccess);
                return;
            }
            DDlProductDataChanged();
            getWorkFlowInfo.GetWFDDL.SelectedIndex = int.Parse(re.Data.ToString());//默认选择新创的工艺
            btnSearch_Click(null,null);
            DisplayMessage("工艺保存成功", re.IsSuccess);

            MESAuditLog ml = new MESAuditLog();
            #region 日志赋值
            ml.ParentID = getWorkFlowInfo.WorkFlowValue; ml.ParentName = parentName;
            ml.CreateEmployeeID = UserInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = 0;
            ml.Description = "临时工艺:创建" + getWorkFlowInfo.WorkFlowText ;
            commonBal.SaveMESAuditLog(ml);
            #endregion

        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnDel_Click(object sender, EventArgs e)
    {
        try
        {
            var re = DeleteData();
            if (!re.IsSuccess)
            {
                DisplayMessage(re.Message, re.IsSuccess);
                return;
            }
            DDlProductDataChanged();
            //btnSearch_Click(null,null);
            DisplayMessage("工艺删除成功", re.IsSuccess);
                       
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        if (btnSelectAll.Text == "全选")
        {
            btnSelectAll.Text = "全不选";
            uMESCommonFunction.ResetGridCheckStatus(wgSourceWorkflow, "ckSelect", 0);
        }
        else if (btnSelectAll.Text == "全不选")
        {
            btnSelectAll.Text = "全选";
            uMESCommonFunction.ResetGridCheckStatus(wgSourceWorkflow, "ckSelect", 2);
        }

    }

    protected void btnInRevert_Click(object sender, EventArgs e)
    {
        uMESCommonFunction.ResetGridCheckStatus(wgSourceWorkflow, "ckSelect", 1);
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        // ClearMessage();

        try
        {
            ResultModel result = SaveWorkflowDoc();
            if (result.IsSuccess)
            {
                DisplayMessage("附件保存成功",true);

                MESAuditLog ml = new MESAuditLog();
                #region 日志赋值
                ml.ParentID = getWorkFlowInfo.WorkFlowValue; ml.ParentName = parentName;
                ml.CreateEmployeeID = UserInfo["EmployeeID"];
                ml.BusinessName = businessName; ml.OperationType = 1;
                ml.Description = "临时工艺:挂附件" + getWorkFlowInfo.WorkFlowText;
                commonBal.SaveMESAuditLog(ml);
                #endregion
            }


        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }

    }
    #endregion


























}