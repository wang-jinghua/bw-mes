﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.ReportBusiness;

public partial class Custom_BwMaterialApp_BwMaterialAppSucceedForm : BaseForm
{
    uMESMaterialBusiness material = new uMESMaterialBusiness();
    int m_PageSize = 20;
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：ERP接收成功的物料信息";
        master.strTitle = "ERP接收成功的物料信息";
        master.ChangeFrame(true);
        WebPanel = WebAsyncRefreshPanel1;
        ClearMessage();
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { SearchData(upageTurning.CurrentPageIndex); });

    }
    #region"数据查询"
    void SearchData(int index)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        if (!string.IsNullOrWhiteSpace(txtProcessNo.Text)) {
            para.Add("ProcessNo", txtProcessNo.Text);
        }
        if (!string.IsNullOrWhiteSpace(txtContainerName.Text))
        {
            para.Add("ContainerName", txtContainerName.Text);
        }
        if (!string.IsNullOrWhiteSpace(txtStartDate.Value))
        {
            para.Add("StartDate", txtStartDate.Value);
        }
        if (!string.IsNullOrWhiteSpace(txtEndDate.Value))
        {
            para.Add("EndDate", txtEndDate.Value);
        }
        para["CurrentPageIndex"]= index.ToString();
        para["PageSize"] = m_PageSize.ToString();

        uMESPagingDataDTO result = material.GetErpSucceedRecevieData(para);

        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();

        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try { SearchData(1); } catch (Exception ex) { DisplayMessage(ex.Message, false); }
    }
    #endregion


    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ExcuteScript("<script>location.reload();</script>");
    }
}