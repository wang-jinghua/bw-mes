﻿using Infragistics.WebUI.UltraWebGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.ReportBusiness;

public partial class Custom_BwCheckManage_BwContainerCheckForm : BaseForm
{
    int m_PageSize = 30;
    uMESContainerCheckBusiness containerCheckBal = new uMESContainerCheckBusiness();
    uMESWorkReportBusiness workreport = new uMESWorkReportBusiness();
    uMESContainerBusiness containerBal = new uMESContainerBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：检验平台";
        master.strTitle = "检验平台";
        master.ChangeFrame(true);

        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { SearchData(upageTurning.CurrentPageIndex); });
        
        if (!IsPostBack)
        {
            InitControl();
            ClearMessage();
        }
    }

    #region 事件
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ClearMessage();

        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strReportEmp = txtReportEmp.Text.Trim();

        string strScanContainerName = string.Empty;
        if (!string.IsNullOrWhiteSpace(txtScan.Text.Trim()))
        {
            strScanContainerName = txtScan.Text.Trim();
        }

        if (strProcessNo == string.Empty && strContainerName == string.Empty && strProductName == string.Empty && strReportEmp == string.Empty && strScanContainerName == string.Empty && ddlCheckType.SelectedItem.Text == string.Empty)
        {
            ShowStatusMessage("请输入查询条件", false);
            return;
        }

        try { SearchData(1); } catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }
    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        SearchData(1);
    }
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        //Response.Redirect(Request.Url.AbsoluteUri);


         txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtScan.Text = string.Empty;


        if (ddlCheckType.Items.Count>0)
        {
             ddlCheckType.SelectedIndex=0;
        }

        gdContainer.Clear();
        ShowStatusMessage("", true);
    }
    protected void popupClosedButton_Click(object sender, EventArgs e)
    {
        ClearMessage();


        if (PopupData == null)
        {
            return;
        }

        Dictionary<string, string> result = PopupData as Dictionary<string, string>;

        if (result.ContainsKey("Page")==true)
        {
            if (result["Page"] == "BwContainerCheckPopupForm.aspx")
            {
                SearchData(1);
                ShowStatusMessage(result["Message"], true);
            }
        }
    }
    protected void btnCheck_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        var activeRow = gdContainer.DisplayLayout.ActiveRow;

        if(activeRow == null){
            ShowStatusMessage("请选择记录", false);
            return;
        }

        DataTable dt = ActiveRowToDt(activeRow);

        //判断是否已检验
        string strIsChecked = dt.Rows[0]["IsChecked"].ToString();

        if (strIsChecked == "1")
        {
            ShowStatusMessage("该报工记录已检验完毕，不允许重复检验", false);
            return;
        }
        //数据库验证是否检验
        if (containerBal.GetTableInfo("conventioncheckinfo", "reportinfoid", dt.Rows[0]["WorkReportInfoID"].ToString()).Rows.Count>0) {
            ShowStatusMessage("该报工记录已检验完毕，不允许重复检验", false);
            return;
        }


        PopupData = dt;
        var page = "BwContainerCheckPopupForm.aspx";
        var script = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", page, 1200, 300);

        //ClientScript.RegisterStartupScript(this.GetType(), btnCheck.Text, script);

        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, script);

        ShowStatusMessage("", true);
    }
    #endregion

    #region 方法
    /// <summary>
    /// 查询数据
    /// </summary>
    /// <param name="index"></param>
    void SearchData(int index)
    {
        ShowStatusMessage("", true);
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["CurrentPageIndex"] = index.ToString();
        para["PageSize"] = m_PageSize.ToString();

        para["ProcessNo"] = txtProcessNo.Text.Trim();
        para["ContainerName"] = txtContainerName.Text.Trim();
        para["ProductName"] = txtProductName.Text.Trim();
        para["ReportEmp"] = txtReportEmp.Text.Trim();

        if (!string.IsNullOrWhiteSpace(txtScan.Text.Trim()))
        {
            para["ScanContainerName"] = txtScan.Text.Trim();
        }

        if (ddlCheckType.SelectedValue != "-1") {
            para["ReportType"] = ddlCheckType.SelectedValue;
        }

        uMESPagingDataDTO result = containerCheckBal.GetCheckData(para);

        this.gdContainer.DataSource = result.DBTable;
        this.gdContainer.DataBind();

        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;
    }
    /// <summary>
    /// 初始化控件
    /// </summary>
    void InitControl() {
        ddlCheckType.Items.AddRange(new ListItem[] { new ListItem("全部", "-1"), new ListItem("首检", "0"),new ListItem("工序检验", "3"),new ListItem("入厂检验", "4") });
    }

    /// <summary>
    /// 选择的行转化为dt
    /// </summary>
    /// <param name="activeRow"></param>
    /// <returns></returns>
    DataTable ActiveRowToDt(UltraGridRow activeRow) {
        DataTable dt = new DataTable();
        
        foreach (UltraGridColumn col in gdContainer.Columns)
        {
            if (dt.Columns.Contains(col.Key))
                continue;

            if (col.Key == "ReportDate")
            {
                dt.Columns.Add(col.Key,System.Type.GetType("System.DateTime"));
            }
            else
            {
                dt.Columns.Add(col.Key);
            }
        }

        var dr = dt.NewRow();
        foreach (UltraGridColumn col in gdContainer.Columns)
        {
            if (col.Key == "ReportDate")
            {
                dr[col.Key] = (DateTime)activeRow.Cells.FromKey(col.Key).Value;
            }
            else
            {
                dr[col.Key] = activeRow.Cells.FromKey(col.Key).Value;
            }
        }
        dt.Rows.Add(dr);

        return dt;
    }

    #endregion


    /// <summary>
    /// 质量记录
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnQR_Click(object sender, EventArgs e)
    {
        try
        {
  
            UltraGridRow uldr = gdContainer.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                ShowStatusMessage("请选择批次记录!", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }
            Session["PopupData"] = null;

            Dictionary<string, string> popupData = new Dictionary<string, string>();

            //不合格数量
            string strNonsenseQty = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("NonsenseQty").Text))
            {
                strNonsenseQty = uldr.Cells.FromKey("NonsenseQty").Text;
            }
            popupData.Add("NonsenseQty", strNonsenseQty);

            if (strNonsenseQty=="0")
            {
                ShowStatusMessage("没有不合格数量，无法进行质量记录填写!", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            //工作令号
            string strProcessNo = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("ProcessNo").Text))
            {
                strProcessNo = uldr.Cells.FromKey("ProcessNo").Text;
            }
            popupData.Add("ProcessNo", strProcessNo);

            //作业令号
            string strOprNo = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("OprNo").Text))
            {
                strOprNo = uldr.Cells.FromKey("OprNo").Text;
            }
            popupData.Add("OprNo", strOprNo);

            //批次号
            string strContainerName = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("ContainerName").Text))
            {
                strContainerName = uldr.Cells.FromKey("ContainerName").Text;
            }
            popupData.Add("ContainerName", strContainerName);

            //图号
            string strProductName = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("ProductName").Text))
            {
                strProductName = uldr.Cells.FromKey("ProductName").Text;
            }
            popupData.Add("ProductName", strProductName);

            //名称
            string strProductDesc = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("ProductDesc").Text))
            {
                strProductDesc = uldr.Cells.FromKey("ProductDesc").Text;
            }
            popupData.Add("Description", strProductDesc);

            //批次数量
            string strConQty = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("ConQty").Text))
            {
                strConQty = uldr.Cells.FromKey("ConQty").Text;
            }
            popupData.Add("ConQty", strConQty);

            //计划开始时间
            string strPlannedStartDate = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("PlannedStartDate").Text))
            {
                strPlannedStartDate =Convert.ToDateTime(uldr.Cells.FromKey("PlannedStartDate").Text).ToString("yyyy-MM-dd");
            }
            popupData.Add("StartTime", strPlannedStartDate);

            //计划结束时间
            string strPlannedCompletionDate = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("PlannedCompletionDate").Text))
            {
                strPlannedCompletionDate = Convert.ToDateTime(uldr.Cells.FromKey("PlannedCompletionDate").Text).ToString("yyyy-MM-dd"); 
            }
            popupData.Add("EndTime", strPlannedCompletionDate);

            //工序
            string strSpecNameDisp = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("SpecNameDisp").Text))
            {
                strSpecNameDisp = uldr.Cells.FromKey("SpecNameDisp").Text;
            }
            popupData.Add("SpecDisName", strSpecNameDisp);

            //跟踪卡ID
            string strContainerID = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("ContainerID").Text))
            {
                strContainerID = uldr.Cells.FromKey("ContainerID").Text;
            }
            popupData.Add("ContainerID", strContainerID);

            //工艺规程ID
            string strWorkflowID = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("WorkflowID").Text))
            {
                strWorkflowID = uldr.Cells.FromKey("WorkflowID").Text;
            }
            popupData.Add("WorkflowID", strWorkflowID);

            //报工单ID
            string strWorkreportInfoID = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("workreportinfoid").Text))
            {
                strWorkreportInfoID = uldr.Cells.FromKey("workreportinfoid").Text;
            }
            popupData.Add("ReportInfoID", strWorkreportInfoID);

            //工序ID
            string strSpecID = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("SpecID").Text))
            {
                strSpecID = uldr.Cells.FromKey("SpecID").Text;
            }
            popupData.Add("SpecID", strSpecID);

            //计量单位ID
            string strUomID = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("uomid").Text))
            {
                strUomID = uldr.Cells.FromKey("uomid").Text;
            }
            popupData.Add("UomID", strUomID);

            //含图号的工序名称 
            string strSpecName = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("SpecName").Text))
            {
                strSpecName = uldr.Cells.FromKey("SpecName").Text;
            }
            popupData.Add("SpecName", strSpecName);



            //报工数量
            string strReportQty = string.Empty;
            if (!string.IsNullOrEmpty(uldr.Cells.FromKey("qty").Text))
            {
                strReportQty = uldr.Cells.FromKey("qty").Text;
            }
            popupData.Add("ReportQty", strReportQty);

            Session["PopupData"] = popupData;

            var page = "../../uMESQualityRecordInfoPopupForm.aspx";
            var script = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", page, 980, 660);

            //   ClientScript.RegisterStartupScript(this.GetType(), btnQR.Text, script);

            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, script);

            ShowStatusMessage("", true);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    }

    /// <summary>
    /// 清除提示信息
    /// </summary>
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"\", true);</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }
    #endregion

    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            var activeRow = gdContainer.DisplayLayout.ActiveRow;

            if (activeRow == null)
            {
                ShowStatusMessage("请选择记录", false);
                return;
            }

            DataTable dt = ActiveRowToDt(activeRow);

            string strWorkReportID = dt.Rows[0]["WorkReportInfoID"].ToString();
            string strPlannedSatartDate = dt.Rows[0]["PlannedStartDate"].ToString();
            if (strPlannedSatartDate != string.Empty)
            {
                strPlannedSatartDate = Convert.ToDateTime(strPlannedSatartDate).ToString("yyyy-MM-dd");
            }
            string strPlannedCompletionDate = dt.Rows[0]["PlannedCompletionDate"].ToString();
            if (strPlannedCompletionDate != string.Empty)
            {
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
            }

            Dictionary<string, object> popupData = new Dictionary<string, object>();
            popupData.Add("ProcessNo", dt.Rows[0]["ProcessNo"].ToString());
            popupData.Add("OprNo", dt.Rows[0]["OprNo"].ToString());
            popupData.Add("ContainerName", dt.Rows[0]["ContainerName"].ToString());
            popupData.Add("ProductName", dt.Rows[0]["ProductName"].ToString());
            popupData.Add("Description", dt.Rows[0]["ProductDesc"].ToString());
            popupData.Add("ContainerQty", dt.Rows[0]["ContainerQty"].ToString());
            popupData.Add("PlannedStartDate", strPlannedSatartDate);
            popupData.Add("PlannedCompletionDate", strPlannedCompletionDate);
            popupData.Add("ContainerID", dt.Rows[0]["ContainerID"].ToString());
            popupData.Add("SpecID", dt.Rows[0]["SpecID"].ToString());
            popupData.Add("ID", "");
            popupData.Add("WorkReportID", strWorkReportID);
            popupData.Add("WorkflowID", dt.Rows[0]["WorkflowID"].ToString());
            popupData.Add("PageName", "ContainerCheck");
            popupData.Add("ChildCount", dt.Rows[0]["ChildCount"].ToString());
            popupData.Add("Qty", dt.Rows[0]["Qty"].ToString());

            DataTable DT = new DataTable();
            DT.Columns.Add("ID");
            DT.Columns.Add("ContainerID");
            DT.Columns.Add("ContainerName");
            DT.Columns.Add("ProductNo");
            DT.Columns.Add("Qty");
            DT.Columns.Add("DisposeResult");
            DT.Columns.Add("LossReasonID");
            DT.Columns.Add("LossReasonName");

            //获取报工单有效产品序号
            DataTable dtPN = workreport.GetWorkReportProductNo(strWorkReportID);

            foreach (DataRow row in dtPN.Rows)
            {
                DataRow r = DT.NewRow();
                r["ID"] = string.Empty;
                r["ContainerID"] = row["ContainerID"].ToString();
                r["ContainerName"] = row["ContainerName"].ToString();
                r["ProductNo"] = row["ProductNo"].ToString();
                r["Qty"] = "1";
                r["DisposeResult"] = string.Empty;
                r["LossReasonID"] = string.Empty;
                r["LossReasonName"] = string.Empty;

                DT.Rows.Add(r);
            }

            popupData.Add("dtProductNo", DT);

            Session["PopupData"] = popupData;
            
            string strScript = "<script>openscrapsubmit()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    /// <summary>
    /// 工艺文档查看
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
        ClearMessage();



        UltraGridRow uldr = gdContainer.DisplayLayout.ActiveRow;
        if (uldr == null)
        {
            ShowStatusMessage("请选择批次记录", false);
            //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
            return;
        }

        DataTable poupDt = new DataTable();
        poupDt.Columns.Add("ProductID");
        poupDt.Columns.Add("WorkflowID");
        DataRow newRow = poupDt.NewRow();
        newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
        newRow["WorkflowID"] = uldr.Cells.FromKey("currentFlowid").Text;
        poupDt.Columns.Add("ContainerID"); poupDt.Columns.Add("ContainerName");
        newRow["ContainerID"] = uldr.Cells.FromKey("ContainerID").Text;
        newRow["ContainerName"] = uldr.Cells.FromKey("ContainerName").Text;
        poupDt.Rows.Add(newRow);
        Session.Add("ProcessDocument", poupDt);
        string strScript = string.Empty;

        //strScript = "<script>window.showModalDialog('../bwCommonPage/uMESDocumentViewPopupForm.aspx?v='+'', '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
        //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);

        var page = "../bwCommonPage/uMESDocumentViewPopupForm.aspx";
        var script = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", page, 700, 600);

        //ClientScript.RegisterStartupScript(this.GetType(), btnCheck.Text, script);

        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, script);
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            var activeRow = gdContainer.DisplayLayout.ActiveRow;

            if (activeRow == null)
            {
                ShowStatusMessage("请选择记录", false);
                return;
            }

            DataTable dt = ActiveRowToDt(activeRow);

            string strPlannedSatartDate = dt.Rows[0]["PlannedStartDate"].ToString();
            if (strPlannedSatartDate != string.Empty)
            {
                strPlannedSatartDate = Convert.ToDateTime(strPlannedSatartDate).ToString("yyyy-MM-dd");
            }
            string strPlannedCompletionDate = dt.Rows[0]["PlannedCompletionDate"].ToString();
            if (strPlannedCompletionDate != string.Empty)
            {
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
            }

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ProcessNo", dt.Rows[0]["ProcessNo"].ToString());
            para.Add("OprNo", dt.Rows[0]["OprNo"].ToString());
            para.Add("ContainerName", dt.Rows[0]["ContainerName"].ToString());
            para.Add("ProductName", dt.Rows[0]["ProductName"].ToString());
            para.Add("Description", dt.Rows[0]["ProductDesc"].ToString());
            para.Add("ConQty", dt.Rows[0]["ContainerQty"].ToString());
            para.Add("StartTime", strPlannedSatartDate);
            para.Add("EndTime", strPlannedCompletionDate);
            para.Add("ContainerID", dt.Rows[0]["ContainerID"].ToString());
            para.Add("WorkflowID", dt.Rows[0]["WorkflowID"].ToString());
            para.Add("SpecID", dt.Rows[0]["SpecID"].ToString());
            para.Add("UomID", dt.Rows[0]["UomID"].ToString());
            para.Add("SpecName", dt.Rows[0]["SpecName"].ToString());
            para.Add("MfgOrderName", dt.Rows[0]["MfgOrderName"].ToString());
            para.Add("FactoryID", dt.Rows[0]["FactoryID"].ToString());

            Session["PopupData"] = para;

            string strScript = "<script>opencontainerspecfinish()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }


    /// <summary>
    /// 数据采集
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDataCol_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            var activeRow = gdContainer.DisplayLayout.ActiveRow;
            if (activeRow == null)
            {
                ShowStatusMessage("请选择记录", false);
                return;
            }


            Dictionary<string, string> popupData = new Dictionary<string, string>();
            //工作令号
            string strProcessNo = string.Empty;
            if (activeRow.Cells.FromKey("ProcessNo").Value!=null)
            {
                strProcessNo = activeRow.Cells.FromKey("ProcessNo").Value.ToString();
            }
            popupData.Add("ProcessNo", strProcessNo);

            //作业令号
            string strOprNo = string.Empty;
            if (activeRow.Cells.FromKey("OprNo").Value != null)
            {
                strOprNo = activeRow.Cells.FromKey("OprNo").Value.ToString();
            }
            popupData.Add("OprNo", strOprNo);

            //批次号
            string strContainerName = string.Empty;
            if (activeRow.Cells.FromKey("ContainerName").Value != null)
            {
                strContainerName = activeRow.Cells.FromKey("ContainerName").Value.ToString();
            }
            popupData.Add("ContainerName", strContainerName);

            //生产任务
            string strMfgOrderName = string.Empty;
            if (activeRow.Cells.FromKey("MfgOrderName").Value != null)
            {
                strMfgOrderName = activeRow.Cells.FromKey("MfgOrderName").Value.ToString();
            }
            popupData.Add("MfgorderName", strMfgOrderName);

            //图号
            string strProductName = string.Empty;
            if (activeRow.Cells.FromKey("ProductName").Value != null)
            {
                strProductName = activeRow.Cells.FromKey("ProductName").Value.ToString();
            }
            popupData.Add("ProductName", strProductName);

            //名称
            string strProductDesc = string.Empty;
            if (activeRow.Cells.FromKey("ProductDesc").Value != null)
            {
                strProductDesc = activeRow.Cells.FromKey("ProductDesc").Value.ToString();
            }
            popupData.Add("Description", strProductDesc);

            //批次数量
            string strConQty = string.Empty;
            if (activeRow.Cells.FromKey("ConQty").Value != null)
            {
                strConQty = activeRow.Cells.FromKey("ConQty").Value.ToString();
            }
            popupData.Add("ConQty", strConQty);

            //计划开始时间 
            string strPlannedStartDate = string.Empty;
            if (activeRow.Cells.FromKey("PlannedStartDate").Value != null)
            {
                strPlannedStartDate = activeRow.Cells.FromKey("PlannedStartDate").Value.ToString();
            }
            popupData.Add("StartTime", strPlannedStartDate);

            //计划结束时间
            string strPlannedCompletionDate = string.Empty;
            if (activeRow.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                strPlannedCompletionDate = activeRow.Cells.FromKey("PlannedCompletionDate").Value.ToString();
            }
            popupData.Add("EndTime", strPlannedCompletionDate);

            //工序
            string strSpecNameDisp = string.Empty;
            if (activeRow.Cells.FromKey("SpecNameDisp").Value != null)
            {
                strSpecNameDisp = activeRow.Cells.FromKey("SpecNameDisp").Value.ToString();
            }
            popupData.Add("SpecDisName", strSpecNameDisp);

            //跟踪卡ID
            string strContainerID = string.Empty;
            if (activeRow.Cells.FromKey("ContainerID").Value != null)
            {
                strContainerID = activeRow.Cells.FromKey("ContainerID").Value.ToString();
            }
            popupData.Add("ContainerID", strContainerID);

            //图号ID
            string strProductID = string.Empty;
            if (activeRow.Cells.FromKey("productid").Value != null)
            {
                strProductID = activeRow.Cells.FromKey("productid").Value.ToString();
            }
            popupData.Add("ProductID", strProductID);

            //工艺规程ID
            string strWorkflowID = string.Empty;
            if (!string.IsNullOrEmpty(activeRow.Cells.FromKey("WorkflowID").Text))
            {
                strWorkflowID = activeRow.Cells.FromKey("WorkflowID").Text;
            }
            popupData.Add("WorkflowID", strWorkflowID);

            //工序ID
            string strSpecID = string.Empty;
            if (activeRow.Cells.FromKey("SpecID").Value != null)
            {
                strSpecID = activeRow.Cells.FromKey("SpecID").Value.ToString();
            }
            popupData.Add("SpecID", strSpecID);

            popupData.Add("Type", "Check"); 

            popupData.Add("CheckCollectType", activeRow.Cells.FromKey("ReportTypeName").Text);

            Session["PopupData"] = popupData;

            string strScript = "<script>openPopUpForm()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }
}