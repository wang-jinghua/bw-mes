﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BwContainerCheckPopupForm.aspx.cs" Inherits="Custom_BwCheckManage_BwContainerCheckPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>检验</title>
     <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
            <div id="ItemDiv" runat="server" style="margin: 5px;">
                <igtbl:UltraWebGrid ID="gdContainer" runat="server" Height="60px" Width="100%">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                    <CellTemplate>
                                        <asp:CheckBox ID="ckSelect" runat="server" />
                                    </CellTemplate>
                                    <Header Caption=""></Header>
                                </igtbl:TemplatedColumn>
                                <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                    <Header Caption="工作令号">
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="150px" AllowGroupBy="No" Hidden="true">
                                    <Header Caption="作业令号">
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                    <Header Caption="批次">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                    <Header Caption="图号">
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductDesc" Key="ProductDesc" Width="150px" AllowGroupBy="No">
                                    <Header Caption="名称">
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                    <Header Caption="报工数量">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ConQty" Key="ConQty" Width="80px" AllowGroupBy="No" Hidden="true">
                                    <Header Caption="批次数量">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="SpecNameDisp" BaseColumnName="SpecNameDisp" Width="150px" Hidden="true">
                                    <Header Caption="工序">
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="SpecName" BaseColumnName="SpecName" Width="150px" Hidden="true">
                                    <Header Caption="工序">
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="ReportDate" BaseColumnName="ReportDate" Width="130px" DataType="System.DateTime" Format="yyy-MM-dd HH:mm">
                                    <Header Caption="报工时间">
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="ReportType" BaseColumnName="ReportType" Width="100px" Hidden="True">
                                    <Header Caption="检验类型">
                                        <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ReportTypeName" Key="ReportTypeName" Width="100px">
                                    <Header Caption="检验类型">
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ContainerID" Key="ContainerID" Hidden="True">
                                    <Header Caption="ContainerID">
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Key="WorkflowID" Hidden="True">
                                    <Header Caption="报工WorkflowID">
                                        <RowLayoutColumnInfo OriginX="10" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="10" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="workreportinfoid" Hidden="True" Key="WorkReportInfoID">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="14" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="14" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="SpecID" Hidden="True" Key="SpecID">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="13" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="13" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="PlannedStartDate" Hidden="True" Key="PlannedStartDate">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="15" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="15" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="PlannedCompletionDate" Hidden="True" Key="PlannedCompletionDate">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="uomid" Hidden="True" Key="uomid">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Footer>
                                </igtbl:UltraGridColumn> 
                                <igtbl:UltraGridColumn BaseColumnName="productid" Hidden="True" Key="productid">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="currentFlowid" Hidden="True" Key="currentFlowid">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="16" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="nonsenseqty" Hidden="True" Key="nonsenseqty">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="25" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="25" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="60px" Width="1100px">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
            </div>

        <table border="0" cellpadding="5" cellspacing="0" width="1100">
            <tr>
                <td valign="top">
                    <table class="stdTable" cellpadding="5" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" class="tdRightAndBottom" colspan="">
                                <div>合格数</div>
                                <asp:TextBox ID="txtPassQty" runat="server" Width="120" Height="20px" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                            </td>
                            <td align="left" class="tdRightAndBottom" colspan="">
                                <div>不合格数</div>
                                <asp:TextBox ID="txtLossQty" runat="server" Width="120" Height="20px" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                                <asp:TextBox ID="ConventionCheckConfirm" runat="server" Text="0" Visible="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdRight" colspan="">
                                <div>备注</div>
                                <asp:TextBox ID="txtNotes" runat="server" Width="260" Height="50px" TextMode="MultiLine"></asp:TextBox>
                            </td>
                              <td class="tdRight" valign="bottom">
                                    <asp:Button ID="btnSubmit" runat="server" Text="确定" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                                CssClass="searchButton" EnableTheming="True" Style="width:100px;margin-right: 20px" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnQR" runat="server" Text="质量记录" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                                CssClass="searchButton" EnableTheming="True" Style="width:100px;margin-right: 20px" OnClick="btnQR_Click" Enabled="false" />
                                <asp:Button ID="CloseButton" runat="server" Text="关闭" style="width:100px"
                                CssClass="searchButton" EnableTheming="True" OnClick="CloseButton_Click" />
                            </td >
                        </tr>
                        <tr>
                          
                        </tr>
                    </table>
                </td>
                <td style="display:none">
                    <div>合格子批次</div>
                    <igtbl:UltraWebGrid ID="gdChildContainer" runat="server" Height="200px" Width="270px">
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:UltraGridColumn Key="ckSelect" Width="40px" AllowGroupBy="No" AllowUpdate="Yes" Type="CheckBox">
                                        <Header Caption="">
                                        </Header>

                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ContainerName" Key="ContainerName" Width="150px" Hidden="True">
                                        <Header Caption="ContainerName">
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                        <Header Caption="ContainerID">
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ProductNo" Key="ProductNo" Width="150px">
                                        <Header Caption=" 产品序号">
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="NotSet"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="NotSet" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="270px">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
                    <div style="margin-top: 20px">
                        <asp:Button ID="btnAll" runat="server" Text="全选" Width="80px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnAll_Click" />
                        <asp:Button ID="btnNotAll" runat="server" Text="全不选" Width="80px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnNotAll_Click" />
                    </div>
                </td>
            </tr>
            
        </table>

            <div style="padding: 5px">
               
               
               
            </div>

            <div style="clear: both"></div>

            <div style="margin: 5px;">
                <asp:Label runat="server" Style="font-size: 12px; font-weight: bold;">状态信息：</asp:Label>
                <asp:Label ID="lStatusMessage" runat="server"   Font-Size="Small" Width="100%" Height="35px" ></asp:Label>
            </div>
    </form>
</body>
</html>
