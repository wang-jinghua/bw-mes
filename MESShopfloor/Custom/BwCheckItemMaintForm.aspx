﻿<%@ Page Title="" Language="C#" MasterPageFile="~/uMESMasterPage.master" ValidateRequest="false" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="BwCheckItemMaintForm.aspx.cs" Inherits="Custom_BwCheckItemMaintForm" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetWorkFlowInfo.ascx" TagName="getWorkFlowInfo"
    TagPrefix="gPI" %>
<%@ Register Src="~/uMESCustomControls/ToleranceInput/wucToleranceInput.ascx" TagName="wuctoleranceinput"
    TagPrefix="uc1" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdRight">
                            <div class="divLabel">件号：</div>
                            <gPI:getProductInfo ID="getProductInfo" runat="server" />
                        </td>
                        <td align="left" class="tdRight">
                            <div class="divLabel">工艺：</div>
                            <gPI:getWorkFlowInfo ID="getWorkFlowInfo" runat="server" />
                        </td>
                        <td align="left" class="tdRight">
                            <div class="divLabel">工序：</div>
                            <asp:DropDownList ID="ddlStep" runat="server" Width="200px" Height="28px" Style="font-size: 16px"></asp:DropDownList>
                        </td>

                        <td class="tdNoBorder" nowrap="nowrap">
                            <asp:Button ID="btnSearch" runat="server" Text="查询"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnReSet" runat="server" Text="重置"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="ItemDiv" runat="server">
                <igtbl:UltraWebGrid ID="gdContainer" runat="server" Height="250px" Width="100%" OnActiveRowChange="gdContainer_ActiveRowChange">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                    <CellTemplate>
                                        <asp:CheckBox ID="ckSelect" runat="server" />
                                    </CellTemplate>
                                    <Header Caption=""></Header>
                                </igtbl:TemplatedColumn>
                                <igtbl:UltraGridColumn Key="ProductInfo" Width="150px" BaseColumnName="ProductInfo">
                                    <Header Caption="件号">
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="WorkflowInfo" Key="WorkflowInfo" Width="200px">
                                    <Header Caption="工艺">
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="SpecName" Key="SpecName" Width="150px">
                                    <Header Caption="工序">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="CheckItemInfoName" Key="CheckItemInfoName" Width="150px" AllowGroupBy="No">
                                    <Header Caption="检测项名称">
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="CheckitemDis" Key="CheckitemDis" Width="200px">
                                    <Header Caption="检测项内容">
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="KeySpec" Key="KeySpec" Width="80px" AllowGroupBy="No">
                                    <Header Caption="关键工序">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="FirstCheck" BaseColumnName="FirstCheck" AllowGroupBy="No" Width="80px">
                                    <Header Caption="首件检验">
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="SpecCheck" BaseColumnName="SpecCheck" AllowGroupBy="No" Width="80px">
                                    <Header Caption="工序检验">
                                        <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="factoryrecheck" BaseColumnName="factoryrecheck" AllowGroupBy="No" Width="80px">
                                    <Header Caption="入厂检验">
                                        <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="Notes" Key="Notes" AllowGroupBy="No" Width="200px" Hidden="True">
                                    <Header Caption="备注">
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductId" Hidden="True" Key="ProductId">
                                    <Header Caption="ProductId">
                                        <RowLayoutColumnInfo OriginX="10" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="10" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="WorkflowId" Hidden="True" Key="WorkflowId">
                                    <Header Caption="WorkflowID">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="SpecId" Key="SpecId" Hidden="True">
                                    <Header>
                                        <RowLayoutColumnInfo OriginX="12" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="12" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="CheckIteminfoId" Hidden="True" Key="CheckIteminfoId">
                                    <Header Caption="CheckIteminfoId">
                                        <RowLayoutColumnInfo OriginX="13" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="13" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="CheckItem" Hidden="True" Key="CheckItem" Width="150px">
                                    <Header Caption="CheckItem">
                                        <RowLayoutColumnInfo OriginX="14" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="14" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px" AllowRowNumberingDefault="Continuous">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="250px" Width="100%">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
                <div style="margin-right: 5px; text-align: right; float: right">
                    <uPT:pageTurning ID="upageTurning" runat="server" />
                </div>

                <div style="clear: both"></div>
            </div>

            <div style="margin: 5px;">
                <div style="float: left; margin-right: 20px;">
                    <div>检测项名称</div>
                    <asp:TextBox ID="txtCheckName" runat="server" Width="120" Height="20px"></asp:TextBox>
                </div>
                <div style="float: left; margin-right: 20px; padding-top: 20px">
                    <input id="ckKeySpec" type="checkbox" runat="server" />
                    <asp:Label runat="server">关键工序</asp:Label>
                </div>
                <div style="float: left; margin-right: 20px; padding-top: 20px">
                    <input id="ckFirstCheck" type="checkbox" runat="server" />
                    <asp:Label runat="server">首件检验</asp:Label>
                </div>
                <div style="float: left; margin-right: 20px; padding-top: 20px">
                    <input id="ckSpecCheck" type="checkbox" runat="server" />
                    <asp:Label runat="server">工序检验</asp:Label>
                </div>
                <div style="float: left; margin-right: 20px; padding-top: 20px">
                    <input id="ckFactoryReCheck" type="checkbox" runat="server" />
                    <asp:Label runat="server">入厂检验</asp:Label>
                    <div style="clear: both"></div>


                </div>
            <div style="margin-top: 10px;">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: left; width: 100%;" colspan="2">
                            <uc1:wuctoleranceinput ID="wucToleranceInput1" runat="server" height="160" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="margin-top: 5px; margin-left: 3px">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: left; width: 100%;" colspan="2">
                            <asp:Button ID="btnAdd" runat="server" Text="添加新项"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnSave" runat="server" Text="保存项" Style="margin-left: 10px"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
                            <asp:Button ID="btnDel" runat="server" Text="删除项" Style="margin-left: 10px"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnDel_Click" />
                            <asp:Button ID="Button1" runat="server" Text="删除项" Style="margin-left: 10px"
                                CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" Visible="false" />
                        </td>
                    </tr>
                </table>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">--%>

    <%--    </igmisc:WebAsyncRefreshPanel>--%>
</asp:Content>

