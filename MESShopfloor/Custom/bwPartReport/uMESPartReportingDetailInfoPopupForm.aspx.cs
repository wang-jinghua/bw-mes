﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class uMESSpecFinishConfirmPopupForm : System.Web.UI.Page
{
    uMESCommonBusiness common = new uMESCommonBusiness();
    string webRootDir = HttpRuntime.AppDomainAppPath;
    string webRootUrl = "~";
    uMESPartReportingBusiness bll = new uMESPartReportingBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Dictionary<string, string> para = new Dictionary<string, string>();
            if (Request.QueryString["ContainerID"] != null)
            {
                para.Add("ContainerID", Request.QueryString["ContainerID"]);
            }
            if (Request.QueryString["SpecID"] != null)
            {
                para.Add("SpecID", Request.QueryString["SpecID"]);
            }
            if (Request.QueryString["WorkflowID"] != null)
            {
                para.Add("WorkflowID", Request.QueryString["WorkflowID"]);
            }

            para.Add("IsAdd", "1");

            GetSpecDispatchInfo(para);
            GetSpecReportInfo(para);
            GetSpecCheckInfo(para);
            GetQualityRecordInfo(para);
            GetSpecUnqualifiedProductsInfo(para);
            GetScrapInfo(para);
            ShowDataCollectInfo(para);
        }


    }

    /// <summary>
    /// 获取工序派工信息
    /// </summary>
    /// <param name="para"></param>
    protected void GetSpecDispatchInfo(Dictionary<string,string> para)
    {
        try
        {
           
            DataTable dt = bll.GetSpecDispatchInfo(para);
            wgDispatch.DataSource = dt;
            wgDispatch.DataBind();
        }
        catch(Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 获取工序报工信息
    /// </summary>
    /// <param name="para"></param>
    protected void GetSpecReportInfo(Dictionary<string, string> para)
    {
        try
        {
            para.Add("ReportType", "'0','3'");
            DataTable dt = bll.GetSpecReportInfo(para);
            wgReport.DataSource = dt;
            wgReport.DataBind();
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 获取检验信息
    /// </summary>
    /// <param name="para"></param>
    protected void GetSpecCheckInfo(Dictionary<string, string> para)
    {
        try
        {
            DataTable dt = bll.GetSpecCheckInfo(para);
            wgCheck.DataSource = dt;
            wgCheck.DataBind();
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 获取质量记录信息
    /// </summary>
    /// <param name="para"></param>
    protected void GetQualityRecordInfo(Dictionary<string, string> para)
    {
        try
        {
            DataTable dt = bll.GetSavedQualInfo(para);
            wgQual.DataSource = dt;
            wgQual.DataBind(); 
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 获取不合格品审理信息
    /// </summary>
    /// <param name="para"></param>
    protected void GetSpecUnqualifiedProductsInfo(Dictionary<string, string> para)
    {
        try
        {
            DataTable dt = bll.GetRejectAppInfo(para);
            wgUnQualified.DataSource = dt;
            wgUnQualified.DataBind();
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 获取报废信息
    /// </summary>
    /// <param name="para"></param>
    protected void GetScrapInfo(Dictionary<string, string> para)
    {
        try
        {
            DataTable dt = bll.GetSpecScrapInfo(para);
            wgScrapInfo.DataSource = dt;
            wgScrapInfo.DataBind();
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }
    /// <summary>
    /// 获取数据采集信息
    /// </summary>
    /// <param name="para"></param>
    void ShowDataCollectInfo(Dictionary<string, string> para)
    {
        try
        {
            DataTable dtCol = bll.GetDataCollectInfo(para);
            dtCol.Columns.Add("checktype");
            for (int i = 0; i < dtCol.Rows.Count; i++)
            {
                string strCheckitem = dtCol.Rows[i]["checkitem"].ToString();
                string strCheckitemDis = string.Empty;
                //ParseCode(strCheckitem, ref dtNew, ref strCheckitemDis);
                ParseCode(strCheckitem, ref strCheckitemDis);
                dtCol.Rows[i]["checkitem"] = strCheckitemDis;

                strCheckitemDis = string.Empty;
                ParseCode(dtCol.Rows[i]["checkvalue"].ToString(), ref strCheckitemDis);
                dtCol.Rows[i]["checkvalue"] = strCheckitemDis;

                strCheckitemDis = string.Empty;
                ParseCode(dtCol.Rows[i]["checkvalue1"].ToString(), ref strCheckitemDis);
                dtCol.Rows[i]["checkvalue1"] = strCheckitemDis;

                if (!string.IsNullOrEmpty(dtCol.Rows[i]["collecttype"].ToString()))
                {
                    if (dtCol.Rows[i]["collecttype"].ToString()=="1")
                    {
                        dtCol.Rows[i]["checktype"] = "关键工序";
                    }
                    if (dtCol.Rows[i]["collecttype"].ToString() == "2")
                    {
                        dtCol.Rows[i]["checktype"] = "首件检验";
                    }
                    if (dtCol.Rows[i]["collecttype"].ToString() == "3")
                    {
                        dtCol.Rows[i]["checktype"] = "工序检验";
                    }
                    if (dtCol.Rows[i]["collecttype"].ToString() == "4")
                    {
                        dtCol.Rows[i]["checktype"] = "入厂检验";
                    }
                }

            }
            wgDataCollect.DataSource = dtCol;
            wgDataCollect.DataBind();
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    public void ParseCode(String strPreviewCode, ref string strPreviewCodeDis)
    {
        try
        {
            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath = webRootDir + ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    strPreviewCodeDis += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir + ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (Session["dtImage"] == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            Session["dtImage"] = dtImage;
                        }
                        else
                        {
                            dtImage = (DataTable)Session["dtImage"];
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        Session["dtImage"] = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        strPreviewCodeDis += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                strPreviewCodeDis += strPreviewCode;
            }
        }
        catch (Exception myError)
        {

        }

    }

    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
       // lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            //lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            //lStatusMessage.ForeColor = Color.Red;
        }
    }

}