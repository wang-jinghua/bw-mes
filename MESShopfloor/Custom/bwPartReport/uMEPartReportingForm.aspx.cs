﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class uMEPartReporting : System.Web.UI.Page
{
    const string QueryWhere = "uMEPartReporting";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESPartReportingBusiness bll = new uMESPartReportingBusiness();
    int m_PageSize = 9;
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：零件进度表";
        master.strTitle = "零件进度表";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        //normalCntrl.LtnFirst = lbtnFirst;
        //normalCntrl.LtnLast = lbtnLast;
        //normalCntrl.LtnNext = lbtnNext;
        //normalCntrl.LtnPrev = lbtnPrev;
        //normalCntrl.BtnReset = btnReSet;
        //normalCntrl.BtnGo = btnGo;
        //normalCntrl.BtnSearch = btnSearch;
        //normalCntrl.LabPages = lLabel1;
        //normalCntrl.TxtPage = txtPage;
        //normalCntrl.TxtTotalPage = txtTotalPage;
        //normalCntrl.TxtCurrentPage = txtCurrentPage;
        //normalCntrl.NormalOperation = this;
        //normalCntrl.QueryWhere = QueryWhere;
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { QueryData(upageTurning.CurrentPageIndex); });

        if (!IsPostBack)
        {
            AddColumns();
            ClearMessage();
        }
    }


    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        upageTurning.TxtCurrentPageIndex.Text = "1";
        ShowStatusMessage("", true);
        hdScanCon.Value = string.Empty;
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = "";
        txtEndDate.Value = "";
        txtSpecName.Text = string.Empty;
        wgResult.Rows.Clear();
        wgResult.Columns.Clear();
        upageTurning.TotalRowCount = 0;
        AddColumns();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();
        string strSpecName = txtSpecName.Text.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        if (!string.IsNullOrEmpty(hdScanCon.Value))
        {
            result.Add("ScanContainerName", hdScanCon.Value);
        }
        else
        {
            result.Add("ProcessNo", strProcessNo);
            result.Add("ContainerName", strContainerName);
            result.Add("ProductName", strProductName);
            result.Add("StartDate", strStartDate);
            result.Add("EndDate", strEndDate);
            result.Add("SpecName", strSpecName);
        }
        return result;
    }

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {
            upageTurning.TxtCurrentPageIndex.Text = "1";
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";
            hdScanCon.Value = string.Empty;
            if (strScan != string.Empty)
            {
                hdScanCon.Value = strScan;
                QueryData(1);
            }
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    public void QueryData(int intIndex)
    {
        try
        {
            ShowStatusMessage("", true);
            uMESPagingDataDTO result = bll.GetPartReportingMainInfo(GetQuery(), intIndex, m_PageSize);
            wgResult.Rows.Clear();
            wgResult.Columns.Clear();
            Drawing(result.DBset);
            //给分页控件赋值，用于分页控件信息显示
            this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
            this.upageTurning.RowCountByPage = m_PageSize;

        }
        catch (Exception e)
        {
            ShowStatusMessage(e.Message, false);
        }

    }

    /// <summary>
    /// 绘制零件进度表显示界面
    /// </summary>
    /// <param name="ds"></param>
    void Drawing(DataSet ds)
    {
        DataTable disDt = new DataTable();
        disDt.Columns.Add("Seqnumber");//序号
        disDt.Columns.Add("processno");//工作令号
        disDt.Columns.Add("oprno");//作业令号
        disDt.Columns.Add("productname");//图号
        disDt.Columns.Add("description");//名称
        disDt.Columns.Add("containername");//批次号
        disDt.Columns.Add("conqty");//批次数量
        disDt.Columns.Add("isSpec");//是否是工序
        disDt.Columns.Add("CurrentSpec");//当前工序工序
        disDt.Columns.Add("ProductID");
        disDt.Columns.Add("WorkflowID");

        //创建Grid列
        AddColumns();

        //主信息
        DataTable dtMain = ds.Tables[0];

        //详细信息
        DataTable dt = ds.Tables[1];
        DataView dv = dt.DefaultView;
        dv.RowFilter = "isskiped = '' OR isskiped IS NULL";
        dv.Sort = "originalstartdate,ShowNo";
        DataTable dtDetail = dv.ToTable();

        //最大列值
        int maxColum = 0;

        if (dtDetail.Rows.Count > 0)
        {
            foreach (DataRow dr in dtMain.Rows)
            {
                string strFilter = String.Format("containername='{0}' ", dr["containername"].ToString().Trim());
                DataRow[] rows = dtDetail.Select(strFilter);
                if (maxColum < rows.Length)
                {
                    maxColum = rows.Length;
                }
            }
        }

        //创建工序列
        for (int i = 1; i <= maxColum; i++)
        {
            disDt.Columns.Add("A" + i.ToString().Trim());
        }

        //创建Grid工序列
        UltraGridColumn colum = new UltraGridColumn();
        for (int i = 1; i <= maxColum; i++)
        {
            colum = new UltraGridColumn("A" + i.ToString(), " ", ColumnType.NotSet, "");
            colum.BaseColumnName = "A" + i.ToString();
            colum.Key = "A" + i.ToString();
            colum.Width = 80;
            colum.HeaderStyle.Height = 20;
            colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            wgResult.Bands[0].Columns.Add(colum);
        }

        //disDt赋值
        int rowindex = 0;
        int pageSize = m_PageSize;
        int pageIndex = upageTurning.CurrentPageIndex;
        //if (!string.IsNullOrEmpty(upageTurning.CurrentPageIndex.ToString))
        //{
        //    pageIndex = Convert.ToInt32(txtCurrentPage.Text);
        //}

        foreach (DataRow drMain in dtMain.Rows)
        {

            rowindex += 1;
            DataRow[] drInsrt;
            DataRow rowTmp = disDt.NewRow();
            rowTmp["Seqnumber"] = pageSize * (pageIndex - 1) + rowindex;
            rowTmp["processno"] = drMain["processno"];//工作令号
            rowTmp["oprno"] = drMain["oprno"];//作业令号
            rowTmp["productname"] = drMain["productname"];//图号
            rowTmp["description"] = drMain["description"];//名称
            rowTmp["containername"] = drMain["containername"];//批次号
            rowTmp["conqty"] = drMain["conqty"];//批次数量
            rowTmp["ProductID"] = drMain["ProductID"];
            rowTmp["WorkflowID"] = drMain["WorkflowID"];
            //rowTmp["isSpec"] = "1";//批次数量
            disDt.Rows.Add(rowTmp);
            rowTmp = disDt.NewRow();
            rowTmp["Seqnumber"] = "";
            rowTmp["processno"] = "";//工作令号
            rowTmp["oprno"] = "";//作业令号
            rowTmp["productname"] = "";//图号
            rowTmp["description"] = "";//名称
            rowTmp["containername"] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;正常";//批次号

            if (drMain["holdreasonid"].ToString() != "")
            {
                rowTmp["containername"] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;暂停";
            }

            if (drMain["status"].ToString() == "2")
            {
                rowTmp["containername"] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关闭";
            }


            rowTmp["conqty"] = "";//批次数量
            rowTmp["ProductID"] = drMain["ProductID"];
            rowTmp["WorkflowID"] = drMain["WorkflowID"];
            //rowTmp["isSpec"] = "";
            disDt.Rows.Add(rowTmp);
            string strConName = drMain["containername"].ToString();
            string strConID = drMain["containerid"].ToString();
            drInsrt = dtDetail.Select("containername='" + strConName + "'", "showno,sequenceno");
            for (int h = 0; h < drInsrt.Length; h++)
            {
                string strWorkflowid = drInsrt[h]["workflowid"].ToString();
                string strSpecID = drInsrt[h]["specID"].ToString();
                string strSpecName = drInsrt[h]["specNameNew"].ToString();

                string strMainConId = string.Empty;
                if (!string.IsNullOrEmpty(drInsrt[h]["maincontainerid"].ToString()))
                {
                    strMainConId = drInsrt[h]["maincontainerid"].ToString();
                }
                string strUrl = "<a href='#' onclick =openSpecDetailwins('" + strConID + "','" + strSpecID + "','" + strWorkflowid + "')  style='font-size: small;height:10px;text-align: center;color:Blue;'>" + strSpecName + "</a>";

                if (strMainConId != "" && strMainConId != null)
                {
                    strUrl = "<a href='#' onclick =openSpecDetailwins('" + strConID + "','" + strSpecID + "','" + strWorkflowid + "')  style='font-size: small;height:10px;text-align: center;color:Blue;'>" + strSpecName + "</a>";

                }
                //工序加工状态
                string strState = "-1";
                string strTmp = Color.Gainsboro.ToArgb().ToString();
                if (!string.IsNullOrEmpty(drInsrt[h]["state"].ToString()))
                {
                    strState = drInsrt[h]["state"].ToString();
                }
                switch (strState)
                {
                    case "1"://已派工
                        strTmp = Color.Yellow.ToArgb().ToString();
                        break;
                    case "5"://已接收
                        strTmp = Color.Gold.ToArgb().ToString();
                        break;
                    case "2"://已报工
                        strTmp = Color.YellowGreen.ToArgb().ToString();
                        break;
                    case "3": //已检验
                        strTmp = Color.DeepSkyBlue.ToArgb().ToString();
                        break;
                    case "4": //已完工
                        strTmp  = Color.Green.ToArgb().ToString();
                        break;

                }
                //当前工序
                string strCurrentSpec = drInsrt[h]["iscurrentspec"].ToString();
                //是否有质量问题
                if (!string.IsNullOrEmpty(drInsrt[h]["IsQuality"].ToString()))
                {
                    strTmp = strTmp + ",1";
                }

                if (!string.IsNullOrEmpty(drInsrt[h]["synerspec"].ToString()))
                {
                    strTmp = strTmp + ",外协";
                }

                //是否协作工序

                if (disDt.Rows.Count > 2)
                {
                    if (strCurrentSpec == "1")
                    {
                        disDt.Rows[disDt.Rows.Count - 2]["CurrentSpec"] = strUrl + "A" + (h + 1).ToString();
                    }
                    disDt.Rows[disDt.Rows.Count - 2]["IsSpec"] = "1";
                    disDt.Rows[disDt.Rows.Count - 2]["A" + (h + 1).ToString()] = strUrl;
                }
                else
                {
                    if (strCurrentSpec == "1")
                    {
                        disDt.Rows[0]["CurrentSpec"] = strUrl + "A" + (h + 1).ToString();
                    }

                    disDt.Rows[0]["IsSpec"] = "1";
                    disDt.Rows[0]["A" + (h + 1).ToString()] = strUrl;
                }

                disDt.Rows[disDt.Rows.Count - 1]["A" + (h + 1).ToString()] = strTmp;

            }
        }
        wgResult.DataSource = disDt;
        wgResult.DataBind();

    }
    void DrawingOld(DataSet ds)
    {
        DataTable disDt = new DataTable();
        disDt.Columns.Add("Seqnumber");//序号
        disDt.Columns.Add("processno");//工作令号
        disDt.Columns.Add("oprno");//作业令号
        disDt.Columns.Add("productname");//图号
        disDt.Columns.Add("description");//名称
        disDt.Columns.Add("containername");//批次号
        disDt.Columns.Add("conqty");//批次数量
        disDt.Columns.Add("isSpec");//是否是工序
        disDt.Columns.Add("CurrentSpec");//当前工序工序

        //创建Grid列
        AddColumns();

        //主信息
        DataTable dtMain = ds.Tables[0];

        //详细信息
        DataTable dt = ds.Tables[1];
        DataView dv = dt.DefaultView;
        dv.Sort = "originalstartdate,ShowNo";
        DataTable dtDetail = dv.ToTable(true, "containerid", "containername", "workflowid", "ShowNo");

        //最大列值
        int maxColum = 0;

        if (dtDetail.Rows.Count > 0)
        {
            foreach (DataRow dr in dtDetail.Rows)
            {
                string strFilter = String.Format("containername='{0}' AND workflowid='{1}'", dr["containername"].ToString().Trim(), dr["workflowid"].ToString().Trim());
                DataRow[] rows = dt.Select(strFilter);
                if (maxColum < rows.Length)
                {
                    maxColum = rows.Length;
                }
            }

            //创建工序列
            for (int i = 1; i <= maxColum; i++)
            {
                disDt.Columns.Add("A" + i.ToString().Trim());
            }

            //创建Grid工序列
            UltraGridColumn colum = new UltraGridColumn();
            for (int i = 1; i <= maxColum; i++)
            {
                colum = new UltraGridColumn("A" + i.ToString(), " ", ColumnType.NotSet, "");
                colum.BaseColumnName = "A" + i.ToString();
                colum.Key = "A" + i.ToString();
                colum.Width = 80;
                colum.HeaderStyle.Height = 20;
                colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
                wgResult.Bands[0].Columns.Add(colum);
            }

            //disDt赋值
            int rowindex = 0;
            int pageSize = m_PageSize;
            int pageIndex = upageTurning.CurrentPageIndex;
            //if (!string.IsNullOrEmpty(txtCurrentPage.Text))
            //{
            //    pageIndex = Convert.ToInt32(txtCurrentPage.Text);
            //}

            foreach (DataRow drMain in dtMain.Rows)
            {

                rowindex += 1;
                DataRow[] drInsrt;
                DataRow rowTmp = disDt.NewRow();
                rowTmp["Seqnumber"] = pageSize * (pageIndex - 1) + rowindex;
                rowTmp["processno"] = drMain["processno"];//工作令号
                rowTmp["oprno"] = drMain["oprno"];//作业令号
                rowTmp["productname"] = drMain["productname"];//图号
                rowTmp["description"] = drMain["description"];//名称
                rowTmp["containername"] = drMain["containername"];//批次号
                rowTmp["conqty"] = drMain["conqty"];//批次数量
                //rowTmp["isSpec"] = "1";//批次数量

                string strConName = drMain["containername"].ToString();
                string strConID = drMain["containerid"].ToString();
                DataRow[] drDetail = dtDetail.Select("containername='" + strConName + "'");

                IList list = new List<string>();
                if (drDetail.Length > 0)
                {
                    for (int i = 0; i < drDetail.Length; i++)
                    {
                        string strWorkflowid = drDetail[i]["WORKFLOWID"].ToString().Trim();
                        string strShowNo = drDetail[i]["ShowNo"].ToString().Trim();
                        if (list.Contains(strWorkflowid + "," + strShowNo) == false)
                        {
                            list.Add(strWorkflowid + "," + strShowNo);
                        }
                    }

                    for (int l = 0; l < list.Count; l++)
                    {
                        string[] strWorkflowidList = list[l].ToString().Trim().Split(',');
                        string strWorkflowid = strWorkflowidList[0];
                        string strShowNo = strWorkflowidList[1];
                        string strFilterc = string.Format("containername='{0}' AND WORKFLOWID='{1}' AND ShowNo ='{2}'", strConName, strWorkflowid, strShowNo);
                        drInsrt = dt.Select(strFilterc, "SequenceNo");
                        if (l != 0)
                        {
                            rowTmp = disDt.NewRow();
                            rowTmp["Seqnumber"] = "";
                            rowTmp["processno"] = "";//工作令号
                            rowTmp["oprno"] = "";//作业令号
                            rowTmp["productname"] = "";//图号
                            rowTmp["description"] = "";//名称
                            rowTmp["containername"] = "";//批次号
                            rowTmp["conqty"] = "";//批次数量
                            //rowTmp["isSpec"] = "";

                        }
                        disDt.Rows.Add(rowTmp);

                        //工序加工进度颜色显示
                        rowTmp = disDt.NewRow();
                        rowTmp["Seqnumber"] = "";
                        rowTmp["processno"] = "";//工作令号
                        rowTmp["oprno"] = "";//作业令号
                        rowTmp["productname"] = "";//图号
                        rowTmp["description"] = "";//名称
                        rowTmp["containername"] = "";//批次号
                        rowTmp["conqty"] = "";//批次数量
                        //rowTmp["isSpec"] = "";
                        disDt.Rows.Add(rowTmp);
                        //工序列赋值
                        for (int h = 0; h < drInsrt.Length; h++)
                        {
                            string strSpecID = drInsrt[h]["specID"].ToString();
                            string strSpecName = drInsrt[h]["specNameNew"].ToString();
                            string strUrl = "<a href='#' onclick =openSpecDetailwins('" + strConID + "','" + strSpecID + "','" + strWorkflowid + "')  style='font-size: small;height:10px;text-align: center;color:Blue;'>" + strSpecName + "</a>";

                            //工序加工状态
                            string strState = "-1";
                            string strTmp = Color.Gainsboro.ToArgb().ToString();
                            if (!string.IsNullOrEmpty(drInsrt[h]["state"].ToString()))
                            {
                                strState = drInsrt[h]["state"].ToString();
                            }
                            switch (strState)
                            {
                                case "1"://已派工
                                    strTmp = Color.Yellow.ToArgb().ToString();
                                    break;
                                case "5"://已接收
                                    strTmp = Color.Gold.ToArgb().ToString();
                                    break;
                                case "2"://已报工
                                    strTmp = Color.YellowGreen.ToArgb().ToString();
                                    break;
                                case "3": //已检验
                                    strTmp = Color.DeepSkyBlue.ToArgb().ToString();
                                    break;
                                case "4": //已完工
                                    strTmp = strTmp = Color.Green.ToArgb().ToString();
                                    break;

                            }
                            //当前工序
                            string strCurrentSpec = drInsrt[h]["iscurrentspec"].ToString();
                            //是否有质量问题
                            if (!string.IsNullOrEmpty(drInsrt[h]["IsQuality"].ToString()))
                            {
                                strTmp = strTmp + ",1";
                            }
                            if (disDt.Rows.Count > 2)
                            {
                                if (strCurrentSpec == "1")
                                {
                                    disDt.Rows[disDt.Rows.Count - 2]["CurrentSpec"] = strUrl + "A" + (h + 1).ToString();
                                }
                                disDt.Rows[disDt.Rows.Count - 2]["IsSpec"] = "1";
                                disDt.Rows[disDt.Rows.Count - 2]["A" + (h + 1).ToString()] = strUrl;
                            }
                            else
                            {
                                if (strCurrentSpec == "1")
                                {
                                    disDt.Rows[0]["CurrentSpec"] = strUrl + "A" + (h + 1).ToString();
                                }

                                disDt.Rows[0]["IsSpec"] = "1";
                                disDt.Rows[0]["A" + (h + 1).ToString()] = strUrl;
                            }

                            disDt.Rows[disDt.Rows.Count - 1]["A" + (h + 1).ToString()] = strTmp;

                        }
                    }
                }
            }
        }
        wgResult.DataSource = disDt;
        wgResult.DataBind();

    }
    protected void wgResult_DataBound(object sender, EventArgs e)
    {
        {
            for (int i = 0; i < wgResult.Rows.Count; i++)
            {
                if (!string.IsNullOrEmpty(wgResult.Rows[i].Cells.FromKey("isspec").Text))
                {
                    if (wgResult.Rows[i].Cells.FromKey("isspec").Text == "1")
                    {
                        string strCSpecName = wgResult.Rows[i].Cells.FromKey("CurrentSpec").Text;
                        for (int j = 0; j < wgResult.Columns.Count; j++)
                        {
                            string strColName = wgResult.Columns[j].BaseColumnName.ToString();
                            if (wgResult.Columns[j].BaseColumnName.Contains("A"))
                            {
                                if (!string.IsNullOrEmpty(wgResult.Rows[i].Cells[j].Text))
                                {
                                    if ((wgResult.Rows[i].Cells[j].Text + strColName) == strCSpecName)
                                    {
                                        wgResult.Rows[i].Cells[j].Style.BackColor = Color.Pink;
                                    }

                                }
                            }
                        }
                        continue;
                    }
                }

                for (int j = 0; j < wgResult.Columns.Count; j++)
                {
                    if (wgResult.Columns[j].BaseColumnName.Contains("A"))
                    {
                        if (!string.IsNullOrEmpty(wgResult.Rows[i].Cells[j].Text))
                        {
                            string strTmp = wgResult.Rows[i].Cells[j].Text;
                            string[] strArray = wgResult.Rows[i].Cells[j].Text.Split(',');
                            if (strArray.Length >= 2)
                            {
                                wgResult.Rows[i].Cells[j].Style.BackColor = Color.FromArgb(Convert.ToInt32(strArray[0]));
                                if (strArray[strArray.Length - 1].ToString() == "1")
                                {
                                    wgResult.Rows[i].Cells[j].Text = "&nbsp;&nbsp;&nbsp;▲";
                                }
                                else
                                {
                                    wgResult.Rows[i].Cells[j].Text = "&nbsp;&nbsp;&nbsp;外协";
                                }
                               
                            }
                            else
                            {
                                wgResult.Rows[i].Cells[j].Style.BackColor = Color.FromArgb(Convert.ToInt32(wgResult.Rows[i].Cells[j].Text));
                                wgResult.Rows[i].Cells[j].Text = "";
                            }
                        }
                    }
                }
            }
        }
    }
    /// <summary>
    /// 创建Grid列
    /// </summary>
    void AddColumns()
    {

        UltraGridColumn colum = new UltraGridColumn("xuhao", "序号", ColumnType.NotSet, "");
        colum.BaseColumnName = "Seqnumber";
        colum.Key = "Seqnumber";
        colum.Width = 40;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("processno", "工作令号", ColumnType.NotSet, "");
        colum.BaseColumnName = "processno";
        colum.Key = "processno";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("oprno", "作业令号", ColumnType.NotSet, "");
        colum.BaseColumnName = "oprno";
        colum.Key = "oprno";
        colum.Width = 120;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("productname", "图号", ColumnType.NotSet, "");
        colum.BaseColumnName = "productname";
        colum.Key = "productname";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("description", "名称", ColumnType.NotSet, "");
        colum.BaseColumnName = "description";
        colum.Key = "description";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("containername", "批次号", ColumnType.NotSet, "");
        colum.BaseColumnName = "containername";
        colum.Key = "containername";
        colum.Width = 120;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("conqty", "数量", ColumnType.NotSet, "");
        colum.BaseColumnName = "conqty";
        colum.Key = "conqty";
        colum.Width = 60;
        colum.Hidden = false;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("IsSpec", "IsSpec", ColumnType.NotSet, "");
        colum.BaseColumnName = "IsSpec";
        colum.Key = "IsSpec";
        colum.Width = 60;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("CurrentSpec", "CurrentSpec", ColumnType.NotSet, "");
        colum.BaseColumnName = "CurrentSpec";
        colum.Key = "CurrentSpec";
        colum.Width = 60;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("WorkflowID", "WorkflowID", ColumnType.NotSet, "");
        colum.BaseColumnName = "WorkflowID";
        colum.Key = "WorkflowID";
        colum.Width = 60;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);

        colum = new UltraGridColumn("ProductID", "ProductID", ColumnType.NotSet, "");
        colum.BaseColumnName = "ProductID";
        colum.Key = "ProductID";
        colum.Width = 60;
        colum.Hidden = true;
        colum.HeaderStyle.Height = 20;
        colum.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
        colum.Header.Fixed = true;
        wgResult.Bands[0].Columns.Add(colum);


    }
    public void ResetQuery()
    {
        ShowStatusMessage("", true);

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        txtSpecName.Text = string.Empty;
        wgResult.Rows.Clear();
        upageTurning.TotalRowCount = 0;
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        // ClientScript.RegisterStartupScript(GetType(), "", strScript);
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    }
    #endregion

    #region 清除提示信息
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"" + "" + "\", 'true');</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            try
            {
                upageTurning.TxtCurrentPageIndex.Text = "1";
                hdScanCon.Value = string.Empty;
                QueryData(upageTurning.CurrentPageIndex);

            }
            catch (Exception ex)
            {
                ShowStatusMessage(ex.Message, false);
            }
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();

            UltraGridRow uldr = wgResult.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                ShowStatusMessage("请选择订单记录", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            DataTable poupDt = new DataTable();
            poupDt.Columns.Add("ProductID");
            poupDt.Columns.Add("WorkflowID");
            DataRow newRow = poupDt.NewRow();
            newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
            newRow["WorkflowID"] = uldr.Cells.FromKey("Workflowid").Text;
            poupDt.Rows.Add(newRow);
            Session.Add("ProcessDocument", poupDt);
            string strScript = string.Empty;

            strScript = "<script>window.showModalDialog('../bwCommonPage/uMESDocumentViewPopupForm.aspx', '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
}