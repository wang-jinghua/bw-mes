﻿using Infragistics.WebUI.UltraWebNavigator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;

public partial class Custom_bwPartReport_BwProductTreeViewForm : ShopfloorPage
{
    uMESProductTreeViewBusiness productTreeBal = new uMESProductTreeViewBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：零组件树形查看";
        master.strTitle = "零组件树形查看";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        DisplayMessage("", true);
        if (!IsPostBack)
        {
            examepleTree();
        }
        
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try {
            tvTree.Nodes.Clear();

            string productName="", productRev = "";
            GetProductInfo(ref productName,ref productRev);
            if (string.IsNullOrWhiteSpace(productName)) {
                DisplayMessage("请选择产品", false);
                return;
            }
            if (string.IsNullOrWhiteSpace(txtProcessNo.Text))
            {
                DisplayMessage("请选择工作令号", false);
                return;
            }
            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ProductName", productName); para.Add("ProductRev", productRev);
            para.Add("ProcessNo",txtProcessNo.Text);//工作令号
            DataTable dt = productTreeBal.GetSubProductInfo(para);

            if (dt.Rows.Count == 0)
            {
                DisplayMessage("此产品没有子级零组件",false);
                return;
            }
            //TreeView
            /*
            TreeNode productTree = GetProductTree(productName, productRev, dt);
            DataRow[] drs = dt.Select(string.Format("productname='{0}' and productrev='{1}'", productName, productRev));
            productTree.Text = drs[0]["productname"].ToString() + ":" + drs[0]["productrev"].ToString() + " | " + drs[0]["productStatus"].ToString();
            productTree.Value = drs[0]["productid"].ToString();            
            tvTree.Nodes.Add(productTree);
            tvTree.Nodes[0].Expand();
            */
            //UltraWebTree
            Node productTree = GetProductTree2(productName, productRev, dt);
            DataRow[] drs = dt.Select(string.Format("productname='{0}' and productrev='{1}'", productName, productRev));
            productTree.Text = drs[0]["productname"].ToString() + ":" + drs[0]["productrev"].ToString() + " | " + drs[0]["productStatus"].ToString();
            //productTree.Value = drs[0]["productid"].ToString();
            treeProduct.Nodes.Add(productTree);
            treeProduct.Nodes[0].Expand(false);


        } catch (Exception ex) {
            DisplayMessage(ex.Message,false);
        }
    }
    //TreeView
    TreeNode GetProductTree(string productName,string productRev, DataTable dt) {
        TreeNode productTree = new TreeNode();

        DataRow[] drs = dt.Select(string.Format("productname='{0}' and productrev='{1}'",productName,productRev));
      

        for (int i = 0; i < drs.Length; i++) {
            TreeNode productSubTree = GetProductTree(drs[i]["subproductname"].ToString(), drs[i]["subproductrev"].ToString(),dt);

            productSubTree.Text = drs[i]["subproductname"].ToString() + ":" + drs[i]["subproductrev"].ToString() + " | " + drs[i]["subproductstatus"].ToString();
            productSubTree.Value = drs[i]["subproductid"].ToString();
            productTree.ChildNodes.Add(productSubTree);
        }

        return productTree;

    }
    //UltraWebTree
    Node GetProductTree2(string productName, string productRev, DataTable dt)
    {
        Node productTree = new Node();

        DataRow[] drs = dt.Select(string.Format("productname='{0}' and productrev='{1}'", productName, productRev));


        for (int i = 0; i < drs.Length; i++)
        {
            Node productSubTree = GetProductTree2(drs[i]["subproductname"].ToString(), drs[i]["subproductrev"].ToString(), dt);

            productSubTree.Text = drs[i]["subproductname"].ToString() + ":" + drs[i]["subproductrev"].ToString() + " | " + drs[i]["subproductstatus"].ToString();
            //productSubTree.Value = drs[i]["subproductid"].ToString();
            productTree.Nodes.Add(productSubTree);
        }

        return productTree;

    }
   
    //获取产品名及版本
    void GetProductInfo(ref string productName, ref string productRev)
    {
        string productInfo = getProductInfo.ProducText;

        if (productInfo == "")
        {
            return;
        }

        productName = productInfo.Substring(0, productInfo.IndexOf(":"));
        productRev = productInfo.Substring(productInfo.IndexOf(":") + 1, productInfo.IndexOf("(", productName.Length) - productInfo.IndexOf(":") - 1);

    }

    protected void tvTree_SelectedNodeChanged(object sender, EventArgs e)
    {
        DisplayMessage(DateTime.Now.Second.ToString(),true);
    }

    protected override void DisplayMessage(string strMessage, Boolean boolResult)
    {
        strMessage = strMessage.Trim(new[] { '\r', '\n' });//add:Wangjh 防止消息本身有转义符
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), strScript);
    }

    void examepleTree() {
        TreeNode productTree = new TreeNode();
        productTree.Text = "组件";

        for (int i = 0; i < 6; i++)
        {
            TreeNode productSubTree = new TreeNode();
            productSubTree.Text = "零件" + i.ToString() + "|未完成";
            productSubTree.Value = "Value" + i.ToString();

            for (int j = 0; j < 3; j++)
            {
                TreeNode productSubTree2 = new TreeNode();
                productSubTree2.Text = "零件" + j.ToString() + "|未完成";
                productSubTree2.Value = "Value" + j.ToString();

                for (int k = 0; k < 2; k++)
                {
                    TreeNode productSubTree3 = new TreeNode();
                    productSubTree3.Text = "零件" + k.ToString() + "|未完成";
                    productSubTree3.Value = "Value" + k.ToString();

                    productSubTree2.ChildNodes.Add(productSubTree3);
                }

                productSubTree.ChildNodes.Add(productSubTree2);
            }

            productTree.ChildNodes.Add(productSubTree);
        }

        tvTree.Nodes.Add(productTree);
        tvTree.CollapseAll();
    }

    protected void treeProduct_NodeChanged(object sender, WebTreeNodeEventArgs e)
    {
      //  DisplayMessage(DateTime.Now.Second.ToString(), true);
    }


    protected void treeProduct_NodeSelectionChanged(object sender, WebTreeNodeEventArgs e)
    {
        DisplayMessage(DateTime.Now.Second.ToString(), true);
    }

    protected void treeProduct_NodeDropped(object sender, WebTreeNodeDroppedEventArgs e)
    {
        //DisplayMessage(DateTime.Now.Second.ToString(), true);
    }
}