﻿<%@ Page Language="C#"  MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="BwProductTreeViewForm.aspx.cs" Inherits="Custom_bwPartReport_BwProductTreeViewForm" %>

<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>

<%--<link href="~/style/css.css" rel="stylesheet" type="text/css" disabled="true"/>--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    
<%--    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">--%>

    <div>
        
        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left"  width="300" class="tdRight">
                            <div class="divLabel">工作令号：</div>
                            <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                        </td>
                        <td align="left"  width="300" class="tdRight">
                            <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel2" runat="server">
                             <div>产品</div>
                             <gPI:getProductInfo ID="getProductInfo" runat="server" />
                             </igmisc:WebAsyncRefreshPanel>
                        </td>

                        <td class="tdRight"  nowrap="nowrap" style="text-align:left">
                            <asp:Button ID="btnSearch" runat="server" Text="查询"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnReSet" runat="server" Text="重置"
                                CssClass="searchButton" EnableTheming="True"/>
                        </td>
                    </tr>
                </table>

    </div>
     
    <div style="margin-top:5px">
        <div id="dvTree" style="float:left;margin-top:-18px">
            <asp:TreeView ID="tvTree" runat="server" Height="420px" Width="300px" ShowLines="true" 
                        BorderWidth="1px" Style="overflow:auto ;left: 0px; position: relative; top: 0px;text-align:left;
                        " NodeWrap="True" BorderStyle="Notset" OnSelectedNodeChanged="tvTree_SelectedNodeChanged" Visible="False"    >
                    </asp:TreeView> 
            
            <ignav:UltraWebTree ID="treeProduct" runat="server" OnNodeChanged="treeProduct_NodeChanged" OnNodeDropped="treeProduct_NodeDropped" OnNodeSelectionChanged="treeProduct_NodeSelectionChanged" WebTreeTarget="ClassicTree">
                <AutoPostBackFlags NodeChanged="True" NodeChecked="False" NodeCollapsed="False" NodeDropped="False" NodeExpanded="False" />
            </ignav:UltraWebTree>
        </div>
        <div style="float:left;margin-left:10px">
            <igtbl:UltraWebGrid ID="wgContainerInfo" runat="server" Height="400px" Width="100%" >
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                                <Header Caption="工作令号">
                                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                </Header>

                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                                <Header Caption="批次号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                                <Header Caption="图号">
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                                <Header Caption="名称">
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                                <Header Caption="数量">
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="ContainerStatus" AllowGroupBy="No" BaseColumnName="ContainerStatus" Width="80px">
                                                <Header Caption="状态">
                                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                                </Header>

                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                    RowHeightDefault="18px">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="400px" Width="100%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                        StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Top="2px" Bottom="3px"></Padding>
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
             </igtbl:UltraWebGrid>
        </div>
    </div>
        
    <script type="text/javascript">
       
            $("#dvTree a").css({"font-size": "16px" });
     
    </script>
           
   <%-- </igmisc:WebAsyncRefreshPanel>--%>

</asp:Content>
