﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintTaskTableForm.aspx.cs" Inherits="Custom_bwPartReport_PrintTaskTableForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>生产计划简表</title>
    <style>
        .tbd {
            border-width: 1px;
            border-style: solid;
            border-color: black;
            border: none;
        }

        .bd {
            border-width: 1px;
            border-style: solid;
            border-color: black;
            border-top: none;
            border-right: none;
        }

        .bd1 {
            border-width: 1px;
            border-color: black;
            border: none;
        }

        .bd2 {
            border-width: 1px;
            border-style: solid;
            border-color: black;
            border-top: none;
            border-left: none;
            border-right: none;
        }

        .bd3 {
            border-width: 1px;
            border-style: solid;
            border-color: black;
            border-top: none;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">
        <div>

            <%
                int intCount = 0;
                for (int i = 0; i < dtMain.Rows.Count; i++)
                {
                    if (i == dtMain.Rows.Count - 1)
                    {
            %>
            <div style="page-break-after: always">
                <%
                    }
                    if (i % intPageSize == 0)//表头
                    {
                        intCount = 0;
                %>
                <table class="tbd" cellpadding="5" cellspacing="0">
                    <tr>
                        <td class="bd1" style="border-bottom: none; font-size: 25pt; height: 42px;" align="center" colspan="6">生产计划简表</td>
                    </tr>
                    <tr>
                        <td class="bd2" style="font-size: 14pt; height: 20px;" colspan="6">工作令号：<%=dtMain.Rows[0]["ProcessNo"].ToString() %></td>
                    </tr>
                    <tr align="center" style="font-size: 13pt; font-weight: bold; height: 25px;">
                        <td class="bd">序号</td>
                        <td class="bd">批次号</td>
                        <td class="bd">图号</td>
                        <td class="bd">名称</td>
                        <td class="bd">数量</td>
                        <td class="bd3">备注</td>
                    </tr>
                    <%
                        }
                    %>
                    <tr>
                        <td class="bd" style="width: 60px; height: 25px;"><%=i+1 %></td>
                        <td class="bd" style="width: 150px;"><%=dtMain.Rows[i]["ContainerName"].ToString() %></td>
                        <td class="bd" style="width: 100px;"><%=dtMain.Rows[i]["ProductName"].ToString() %></td>
                        <td class="bd" style="width: 280px;"><%=dtMain.Rows[i]["Description"].ToString() %></td>
                        <td class="bd" style="width: 60px;"><%=dtMain.Rows[i]["ConQty"].ToString() %></td>
                        <td class="bd3" style="width: 250px;">&nbsp;</td>
                    </tr>
                    <%
                        if (i % intPageSize == intPageSize - 1)//表尾
                        {
                    %>
                </table>
            </div>
            <%
                    }
                    intCount += 1;
                }

                for (int i = 0; i < intPageSize - intCount; i++)
                {
            %>
            <tr>
                <td class="bd" style="width: 60px; height: 25px;">&nbsp;</td>
                <td class="bd" style="width: 150px;">&nbsp;</td>
                <td class="bd" style="width: 100px;">&nbsp;</td>
                <td class="bd" style="width: 280px;">&nbsp;</td>
                <td class="bd" style="width: 60px;">&nbsp;</td>
                <td class="bd3" style="width: 250px;">&nbsp;</td>
            </tr>
            <%
                }

                if (intCount != intPageSize)
                {
            %>
        </table>
        </div>
        <%
            }
        %>
       
    </div>
    </form>
</body>
</html>
