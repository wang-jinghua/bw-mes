﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESPartReportingDetailInfoPopupForm.aspx.cs"
    ValidateRequest="false" Inherits="uMESSpecFinishConfirmPopupForm" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>工序加工详细信息</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="ufcSectionStyle">
            <span class="ufcSectionSpanStyle">信息展示</span>
        </div>
        <div id="DivNormalWS" runat="server" style="width: 99%; height:580px;">
            <igtab:UltraWebTab ID="wtInfo" runat="server" Width="100%" Height="560px" BorderColor="#949878"
                BorderStyle="Solid" BorderWidth="1px" Font-Bold="False"
                Font-Italic="False" Font-Overline="False"
                Font-Strikeout="False" Font-Underline="False">
                <DefaultTabStyle BackColor="#FEFCFD" Font-Names="Microsoft Sans Serif" Font-Size="12pt"
                    ForeColor="Black" Height="21px">
                    <Padding Bottom="0px" Top="1px" />
                    <Padding Top="1px" Bottom="0px"></Padding>
                </DefaultTabStyle>
                <SelectedTabStyle>
                    <Padding Bottom="1px" Top="0px" />
                    <Padding Top="0px" Bottom="1px"></Padding>
                </SelectedTabStyle>
                <Tabs>
                    <igtab:Tab Text="派工信息" Key="DispatchAndWrokReportRecord" AccessKey="1">
                        <ContentTemplate>
                            <div style="width: 100%; margin: 3px 0 3px 3px">
                                <igtbl:UltraWebGrid ID="wgDispatch" runat="server">
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn Key="SpecNameDisp" Width="120px" BaseColumnName="SpecNameDisp" AllowGroupBy="No">
                                                    <Header Caption="派工工序">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="qty" Key="qty" Width="80px" AllowGroupBy="No" Hidden="false">
                                                    <Header Caption="派工数量">
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="teamname" Key="teamname" Width="150px">
                                                    <Header Caption="班组">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="resourcename" Key="resourcename" Width="150px" Hidden="true">
                                                    <Header Caption="工位/设备">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="deName" Key="deName" Hidden="true">
                                                    <Header Caption="加工人员">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>

                                                <igtbl:UltraGridColumn BaseColumnName="plannedcompletiondate" Hidden="false" Key="plannedcompletiondate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="125px">
                                                    <Header Caption="要求完成时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                    <Header Caption="派工人">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="dispatchdate" Hidden="false" Key="dispatchdate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="120px">
                                                    <Header Caption="派工时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="disptype" Hidden="false" Key="disptype" Width="100px">
                                                    <Header Caption="派工类型">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="operator" Hidden="false" Key="operator" Width="120px">
                                                    <Header Caption="操作工">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" BorderCollapseDefault="Separate"
                                        Name="wgWorkInfo" RowHeightDefault="100%" SelectTypeRowDefault="Single" StationaryMargins="Header"
                                        StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        AllowRowNumberingDefault="ByDataIsland" CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy">
                                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                                            Font-Size="8pt" Height="550px" Width="100%">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                                            <Padding Left="0px"></Padding>
                                        </RowAlternateStyleDefault>
                                        <Pager PageSize="100">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                                            Height="100%" Wrap="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault>
                                            <Padding Left="0px"></Padding>
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px" Wrap="true"
                                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="12pt">
                                            <Padding Left="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8pt" Height="200px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                </igtbl:UltraWebGrid>
                            </div>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="报工信息" Key="ShipRecord" AccessKey="2">
                        <ContentTemplate>
                            <div style="width: 100%; margin: 3px 0 3px 3px">
                                <igtbl:UltraWebGrid ID="wgReport" runat="server" Height="80px">
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn Key="SpecNameDisp" Width="150px" BaseColumnName="SpecNameDisp" AllowGroupBy="No">
                                                    <Header Caption="报工工序">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="qty" Key="qty" Width="150px" AllowGroupBy="No" Hidden="false">
                                                    <Header Caption="报工数量">
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="teamname" Hidden="false" Key="teamname" Width="120px">
                                                    <Header Caption="班组">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="resourcename" Hidden="false" Key="resourcename">
                                                    <Header Caption="工位/设备">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname">
                                                    <Header Caption="加工人员">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="operator" Hidden="false" Key="operator">
                                                    <Header Caption="报工人">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>

                                                <igtbl:UltraGridColumn BaseColumnName="ReportDate" Key="ReportDate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="120px">
                                                    <Header Caption="报工时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>

                                                <igtbl:UltraGridColumn BaseColumnName="rType" Hidden="false" Key="rType" Width="120px">
                                                    <Header Caption="报工类型">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" BorderCollapseDefault="Separate"
                                        Name="wgShipInfo" RowHeightDefault="100%" SelectTypeRowDefault="Single" StationaryMargins="Header"
                                        StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        AllowRowNumberingDefault="ByDataIsland" CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy">
                                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                                            Font-Size="8pt" Height="550px" Width="100%">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                                            <Padding Left="0px"></Padding>
                                        </RowAlternateStyleDefault>
                                        <Pager PageSize="100">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                                            Height="100%" Wrap="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault>
                                            <Padding Left="0px"></Padding>
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px" Wrap="true"
                                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="12pt">
                                            <Padding Left="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8pt" Height="200px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                </igtbl:UltraWebGrid>
                            </div>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="检验信息" Key="QualityRecord" AccessKey="4">
                        <ContentTemplate>
                            <div style="width: 100%; margin: 3px 0 3px 3px">
                                <igtbl:UltraWebGrid ID="wgCheck" runat="server">
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn Key="SpecNameDisp" Width="150px" BaseColumnName="SpecNameDisp" AllowGroupBy="No">
                                                    <Header Caption="工序">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="reportQty" Key="reportQty" Width="80px" AllowGroupBy="No" Hidden="false">
                                                    <Header Caption="报工数量">
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="reporter" Hidden="false" Key="reporter" Width="100px">
                                                    <Header Caption="报工人">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="reportdate" Hidden="false" Key="reportdate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="100px">
                                                    <Header Caption="报工时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="EligibilityQty" Hidden="true" Key="EligibilityQty">
                                                    <Header Caption="合格数量">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="NonsenseQty" Hidden="false" Key="NonsenseQty" Width="100px">
                                                    <Header Caption="不合格数量">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="checktype" Hidden="false" Key="checktype" Width="100px">
                                                    <Header Caption="检验类型">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="checker" Hidden="false" Key="checker" Width="120px">
                                                    <Header Caption="检验人">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="checkdate" Hidden="false" Key="checkdate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="120px">
                                                    <Header Caption="检验时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="ScrapQty" Hidden="True" Key="ScrapQty" Width="120px">
                                                    <Header Caption="报废数">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" BorderCollapseDefault="Separate"
                                        Name="wgWorkInfo" RowHeightDefault="24px" SelectTypeRowDefault="Single" StationaryMargins="Header"
                                        StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        AllowRowNumberingDefault="ByDataIsland" CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy">
                                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                                            Font-Size="8pt" Height="550px" Width="100%">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                                            <Padding Left="0px"></Padding>
                                        </RowAlternateStyleDefault>
                                        <Pager PageSize="100">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                                            Height="100%" Wrap="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault>
                                            <Padding Left="0px"></Padding>
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="12pt">
                                            <Padding Left="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8pt" Height="200px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                </igtbl:UltraWebGrid>
                            </div>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="质量记录" Key="QualityRecord" AccessKey="5">
                        <ContentTemplate>
                            <div style="margin: 3px 0 3px 3px; width: 100%">
                                <igtbl:UltraWebGrid ID="wgQual" runat="server">
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn Key="qualityrecordinfoname" Width="150px" BaseColumnName="qualityrecordinfoname" AllowGroupBy="No">
                                                    <Header Caption="质量记录单号">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="SpecDisName" Key="SpecDisName" Width="150px" AllowGroupBy="No" Hidden="false">
                                                    <Header Caption="工序">
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                                    <Header Caption="不合格数量">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="disissubmit" Hidden="false" Key="disissubmit" Width="120px">
                                                    <Header Caption="是否提交不合格审理">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>

                                                <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                    <Header Caption="检验人">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="SubmitDate" Hidden="false" Key="SubmitDate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="120px">
                                                    <Header Caption="记录时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" BorderCollapseDefault="Separate"
                                        Name="wgWorkInfo" RowHeightDefault="24px" SelectTypeRowDefault="Single" StationaryMargins="Header"
                                        StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        AllowRowNumberingDefault="ByDataIsland" CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy">
                                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                                            Font-Size="8pt" Height="550px" Width="100%">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                                            <Padding Left="0px"></Padding>
                                        </RowAlternateStyleDefault>
                                        <Pager PageSize="100">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                                            Height="100%" Wrap="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault>
                                            <Padding Left="0px"></Padding>
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="12pt">
                                            <Padding Left="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8pt" Height="200px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                </igtbl:UltraWebGrid>
                            </div>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="不合格品审理单" Key="QualityRecord" AccessKey="5">
                        <ContentTemplate>
                            <div style="margin: 3px 0 3px 3px; width: 100%">
                                <igtbl:UltraWebGrid ID="wgUnQualified" runat="server" Height="80px">
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn Key="rejectappinfoname" Width="150px" BaseColumnName="rejectappinfoname" AllowGroupBy="No">
                                                    <Header Caption="不合格品审理单号">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="SpecNameDisp" Key="SpecNameDisp" Width="150px" AllowGroupBy="No" Hidden="false">
                                                    <Header Caption="工序">
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="true" Key="qty" Width="100px">
                                                    <Header Caption="不合格数量">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>

                                                <igtbl:UltraGridColumn BaseColumnName="rbqty" Hidden="true" Key="rbqty">
                                                    <Header Caption="让步使用">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="fixqty" Hidden="false" Key="fixqty" Width="110px">
                                                    <Header Caption="返工返修数量">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="scrapqty" Hidden="false" Key="scrapqty" Width="100px">
                                                    <Header Caption="报废数量">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="SubmitDate" Hidden="false" Key="SubmitDate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="120px">
                                                    <Header Caption="检验时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>

                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" BorderCollapseDefault="Separate"
                                        Name="wgCheckPoint" RowHeightDefault="24px" SelectTypeRowDefault="Single" StationaryMargins="Header"
                                        StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        AllowRowNumberingDefault="ByDataIsland" CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy">
                                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                                            Font-Size="8pt" Height="550px" Width="100%">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                                            <Padding Left="0px"></Padding>
                                        </RowAlternateStyleDefault>
                                        <Pager PageSize="100">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                                            Height="100%" Wrap="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault>
                                            <Padding Left="0px"></Padding>
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="12pt">
                                            <Padding Left="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8pt" Height="200px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                </igtbl:UltraWebGrid>
                            </div>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="报废信息" Key="QualityRecord" AccessKey="5">
                        <ContentTemplate>
                            <div style="margin: 3px 0 3px 3px; width: 100%">
                                <igtbl:UltraWebGrid ID="wgScrapInfo" runat="server" Height="80px">
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn Key="ScrapInfoName" Width="150px" BaseColumnName="ScrapInfoName" AllowGroupBy="No">
                                                    <Header Caption="报废单号">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="SpecNameDisp" Key="SpecNameDisp" Width="150px" AllowGroupBy="No" Hidden="false">
                                                    <Header Caption="工序">
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="6" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                                    <Header Caption="报废数量">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="lossreasonname" Hidden="false" Key="lossreasonname">
                                                    <Header Caption="报废原因">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="QualityRecordInfoID" Hidden="true" Key="QualityRecordInfoID">
                                                    <Header Caption="不合格审理单号">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                    <Header Caption="记录人">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn BaseColumnName="SubmitDate" Hidden="false" Key="SubmitDate"
                                                    DataType="Date" Format="yyyy-MM-dd" Width="120px">
                                                    <Header Caption="记录时间">
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="11" />
                                                    </Footer>
                                                </igtbl:UltraGridColumn>


                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" BorderCollapseDefault="Separate"
                                        Name="wgCheckPoint" RowHeightDefault="24px" SelectTypeRowDefault="Single" StationaryMargins="Header"
                                        StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        AllowRowNumberingDefault="ByDataIsland" CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy">
                                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                                            Font-Size="8pt" Height="550px" Width="100%">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                                            <Padding Left="0px"></Padding>
                                        </RowAlternateStyleDefault>
                                        <Pager PageSize="100">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                                            Height="100%" Wrap="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault>
                                            <Padding Left="0px"></Padding>
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="12pt">
                                            <Padding Left="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8pt" Height="200px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                </igtbl:UltraWebGrid>
                            </div>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="数据采集" Key="ProblemRecord" AccessKey="6">
                        <ContentTemplate>
                            <div style="width: 100%; margin: 3px 0 3px 3px">
                                <igtbl:UltraWebGrid ID="wgDataCollect" runat="server">
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn Key="checkiteminfoname" Width="100px" BaseColumnName="checkiteminfoname" AllowGroupBy="No">
                                                    <Header Caption="检测项名称">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn Key="checkitem" Width="450px" BaseColumnName="checkitem" Hidden="false">
                                                    <Header Caption="检测项内容">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn Key="checktype" Width="80px" BaseColumnName="checktype" Hidden="false">
                                                    <Header Caption="检查类型">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn Key="productno" Width="80px" BaseColumnName="productno" AllowGroupBy="No">
                                                    <Header Caption="产品序号">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn Key="checkvalue" Width="150px" BaseColumnName="checkvalue" Hidden="false" AllowUpdate="Yes">
                                                    <Header Caption="实测值(工人)">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                                <igtbl:UltraGridColumn Key="checkvalue1" Width="150px" BaseColumnName="checkvalue1" Hidden="false" AllowUpdate="Yes">
                                                    <Header Caption="实测值(检验)">
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Header>
                                                    <Footer>
                                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                    </Footer>
                                                </igtbl:UltraGridColumn>
                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" BorderCollapseDefault="Separate"
                                        Name="wgProblem" RowHeightDefault="24px" SelectTypeRowDefault="Single" StationaryMargins="Header"
                                        StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        AllowRowNumberingDefault="ByDataIsland" CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy">
                                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                                            Font-Size="8pt" Height="550px" Width="100%">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                                            <Padding Left="0px"></Padding>
                                        </RowAlternateStyleDefault>
                                        <Pager PageSize="100">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                                            Height="100%" Wrap="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault>
                                            <Padding Left="0px"></Padding>
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="12pt">
                                            <Padding Left="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8pt" Height="200px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="8px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                </igtbl:UltraWebGrid>
                            </div>
                        </ContentTemplate>
                    </igtab:Tab>
                </Tabs>
                <RoundedImage FillStyle="LeftMergedWithCenter" HoverImage="[ig_tab_winXP2.gif]" LeftSideWidth="7"
                    NormalImage="[ig_tab_winXP3.gif]" RightSideWidth="6" SelectedImage="[ig_tab_winXP1.gif]"
                    ShiftOfImages="2" />
                <RoundedImage NormalImage="[ig_tab_winXP3.gif]" HoverImage="[ig_tab_winXP2.gif]"
                    SelectedImage="[ig_tab_winXP1.gif]" LeftSideWidth="7" RightSideWidth="6" ShiftOfImages="2"
                    FillStyle="LeftMergedWithCenter"></RoundedImage>
            </igtab:UltraWebTab>
        </div>
    </form>
</body>
</html>
