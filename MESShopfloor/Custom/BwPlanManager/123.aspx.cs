﻿using CamstarAPI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.DBUtility;
using Newtonsoft.Json;

public partial class Custom_BwPlanManager_123 : BaseForm
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void CloseButton_Click(object sender, EventArgs e)
    {
        //ClientScript.RegisterStartupScript(this.GetType(), "NotifyParent", "<script>NotifyParentOfPopupClosed(); OnPopupWindowClosed();  window.close();</script> ");
        // ClientScript.RegisterStartupScript(this.GetType(), "NotifyParent", "<script>$(window.dialogArguments[0].document.getElementById('dvPopupClosedButton')).find(':submit').click();window.returnValue = true;  window.close();</script> ");


        //ClientScript.RegisterStartupScript(this.GetType(), "NotifyParent", "<script>window.returnValue = true; window.close();</script> ");

        var api = new CamstarClientAPI("Administrator", "123.abc");
        string strInfo = "";
        var m_DataList = new List<ClientAPIEntity>();
        var dataEntity = new ClientAPIEntity();

        dataEntity = new ClientAPIEntity("PlannedStartDate", InputTypeEnum.Details, DataTypeEnum.DataField, new DateTime(2020, 9, 10, 14, 15, 10).ToString("yyyy-MM-dd HH:mm:ss"), "");
        m_DataList.Add(dataEntity);

        dataEntity = new ClientAPIEntity("PlannedCompletionDate", InputTypeEnum.Details, DataTypeEnum.DataField, new DateTime(2020, 9, 24, 20, 24, 50).ToString("yyyy-MM-dd HH:mm:ss"), "");
        m_DataList.Add(dataEntity);


        var result = api.ContainerMaint("p001/2002", "Lot", m_DataList, ref strInfo);
        popIn.Value = strInfo;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        txtNotes.Text = string.Empty;
    }

    protected void btnQuery_Click(object sender, EventArgs e)
    {
        try {
            ExcuteEntity excuteEntity = new ExcuteEntity("productsubproduct", ExcuteType.select);

            excuteEntity.ExcuteFileds = new List<FieldEntity>() { new FieldEntity("productname"),new FieldEntity("subproductname"),new FieldEntity("lastupdatedate") ,
            new FieldEntity("to_char(createdate,'yyyy-mm-dd') createdate")};//查询的字段

            excuteEntity.WhereFileds = new List<FieldEntity>() { new FieldEntity("productname", "JE1.904.000", FieldType.Str),
           new FieldEntity("createdate", DateTime.Parse("2020/9/22 14:50:05"), FieldType.Date,">=") };//条件

            excuteEntity.strWhere = " or id in ('5','6')";//例如：a in (c,d)适用于复杂条件

            DataTable dt = OracleHelper.QueryDataByEntity(excuteEntity);

            string s = JsonConvert.SerializeObject(dt);

            txtNotes.Text = s;
        } catch (Exception ex) {
            txtNotes.Text = ex.Message;
        }      
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        try {
            ExcuteEntity excuteEntity = new ExcuteEntity("productsubproduct", ExcuteType.insert);

            excuteEntity.ExcuteFileds = new List<FieldEntity>() { new FieldEntity("productname","test001",FieldType.Str),new FieldEntity("ID","5",FieldType.Str),
                new FieldEntity("subproductname","subtest001",FieldType.Str),new FieldEntity("lastupdatedate",DateTime.Parse("2020/9/22 14:50:05"),FieldType.Date)};

            
            int r = OracleHelper.ExecuteDataByEntity(excuteEntity);

            txtNotes.Text = r.ToString();
        } catch (Exception ex) {
            txtNotes.Text = ex.Message;
        }
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        try
        {
            ExcuteEntity excuteEntity = new ExcuteEntity("productsubproduct", ExcuteType.update);

            excuteEntity.ExcuteFileds = new List<FieldEntity>() { new FieldEntity("productname","test002",FieldType.Str),
                new FieldEntity("subproductname","subtest001",FieldType.Str),new FieldEntity("lastupdatedate",DateTime.Parse("2020/10/22 21:48:23"),FieldType.Date)};//更新的字段

            excuteEntity.WhereFileds = new List<FieldEntity>() { new FieldEntity("ID", "5", FieldType.Str), new FieldEntity("createdate", DateTime.Parse("2021/9/22 14:50:05"), FieldType.Date) };//条件

            int r = OracleHelper.ExecuteDataByEntity(excuteEntity);

            txtNotes.Text = r.ToString();
        }
        catch (Exception ex)
        {
            txtNotes.Text = ex.Message;
        }
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        try
        {
            ExcuteEntity excuteEntity = new ExcuteEntity("productsubproduct", ExcuteType.del);
            
           // excuteEntity.WhereFileds = new List<FieldEntity>() { new FieldEntity("ID", "5", FieldType.Str), new FieldEntity("createdate", DateTime.Parse("2020/9/22 14:50:05"), FieldType.Date) };

            int r = OracleHelper.ExecuteDataByEntity(excuteEntity);

            txtNotes.Text = r.ToString();
        }
        catch (Exception ex)
        {
            txtNotes.Text = ex.Message;
        }
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        try
        {
            List<ExcuteEntity> lists = new List<ExcuteEntity>();
            //插入
            ExcuteEntity excuteEntity = new ExcuteEntity("productsubproduct", ExcuteType.insert);
            excuteEntity.ExcuteFileds = new List<FieldEntity>() { new FieldEntity("productname","test001",FieldType.Str),new FieldEntity("ID","5",FieldType.Str),
                new FieldEntity("subproductname","subtest001",FieldType.Str),new FieldEntity("lastupdatedate",DateTime.Parse("2020/9/22 14:50:05"),FieldType.Date),
                new FieldEntity("createdate", DateTime.Parse("2020/9/22 14:50:05"),FieldType.Date)};

            lists.Add(excuteEntity);
            //更新
            excuteEntity = new ExcuteEntity("productsubproduct", ExcuteType.update);

            excuteEntity.ExcuteFileds = new List<FieldEntity>() { new FieldEntity("productname","test0010",FieldType.Str),
                new FieldEntity("subproductname","subtest001",FieldType.Str),new FieldEntity("lastupdatedate",DateTime.Parse("2025/1/1 18:40:08"),FieldType.Date)};

            excuteEntity.WhereFileds = new List<FieldEntity>() { new FieldEntity("ID", "5", FieldType.Str), new FieldEntity("createdate", DateTime.Parse("2020/9/22 14:50:05"), FieldType.Date) };

            lists.Add(excuteEntity);
            //删除
             excuteEntity = new ExcuteEntity("productsubproduct", ExcuteType.del);
             excuteEntity.WhereFileds = new List<FieldEntity>() { new FieldEntity("ID", "6", FieldType.Str), new FieldEntity("createdate", DateTime.Parse("2020/9/22 14:50:05"), FieldType.Date) };
            lists.Add(excuteEntity);
            
            int r = OracleHelper.ExecuteDataByEntitys(lists);
            txtNotes.Text = r.ToString();
        }
        catch (Exception ex)
        {
            txtNotes.Text = ex.Message;
        }
    }
}