﻿//Description:订单导入
//Copyright (c) : 通力凯顿（北京）系统集成有限公司
//Writer:Wangjh
//create Date:2020-4-16
//Rewriter:
//Rewrite Date:
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using System.Data;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.IO;

public partial class Custom_BwPlanManager_BwMfgorderImportForm : ShopfloorPage
{
    Dictionary<string, string> userInfo;
    int m_PageSize = 30;
    BwMfgorderImportBusiness mfgOrderImportBal = new BwMfgorderImportBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：订单导入";
        master.strTitle = "订单导入";
        master.ChangeFrame(true);

        //WebPanel = WebAsyncRefreshPanel1;

        userInfo = Session["UserInfo"] as Dictionary<string, string>;
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(()=> { SearchData(upageTurning.CurrentPageIndex); });


        if (!IsPostBack)
        {
            //ClearMessage();
            ShowStatusMessage("",true);
        }
    }

    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        string strScript = "ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");";
        //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        //ClientScript.RegisterStartupScript(this.GetType(), "", strScript);
        ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "Message", strScript, true);

    }
    #region 事件
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //ClearMessage();
        ShowStatusMessage("", true);
        try { SearchData(1); } catch (Exception ex) { ShowStatusMessage (ex.Message,false); }
       
    }
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        // ClearMessage();
        ShowStatusMessage("", true);
        ResetData();
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        // ClearMessage();

        try
        {
            ResultModel result = new ResultModel(false, "");

            var strPath = bffSelectPath.FileName;

            if (strPath == string.Empty)
            {
                ShowStatusMessage("请选择Excel文件", false);
                return;
            }

            SaveFileToServer(ref strPath);

            DataTable dt = new DataTable();
            
            result = mfgOrderImportBal.ImportData(strPath, ref dt, userInfo["EmployeeName"], userInfo["Password"]);

            if (result.IsSuccess)
            {
                gdMfgorder.DataSource = dt;
                gdMfgorder.DataBind();
            }

            string strMessage = result.Message.Trim().Replace("\"","");
            ShowStatusMessage (strMessage, result.IsSuccess);
            ShowStatusMessage("", true);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
       
    }
    #endregion

    #region 方法
    /// <summary>
    /// 查询订单
    /// </summary>
    /// <param name="index"></param>
    void SearchData(int index) {
        gdMfgorder.Rows.Clear();

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["MfgManagerID"] = userInfo["EmployeeID"];
        para["ProcessNo"] = txtProcessNo.Text.Trim();
        para["OprNo"] = txtOprNo.Text.Trim();
        para["ProductName"] = txtProductName.Text.Trim();
        para["PlannedStartDate1"] = txtPlanStartDate1.Value.Trim();
        para["PlannedStartDate2"] = txtPlanStartDate2.Value.Trim();                                                                                     

        para["CurrentPageIndex"] = index.ToString();
        para["PageSize"]= m_PageSize.ToString();


        uMESPagingDataDTO result = mfgOrderImportBal.GetSearchData(para);

        this.gdMfgorder.DataSource = result.DBTable;
        this.gdMfgorder.DataBind();

        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount =int.Parse( result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;
    }

   /// <summary>
   /// 保存到服务器
   /// </summary>
   /// <param name="strpath"></param>
   /// <returns></returns>
     bool SaveFileToServer(ref string strpath ) {

        bool result = false;

        var fileName = strpath.Substring(strpath.LastIndexOf(@"\") + 1);

        File.Delete(strpath);

            strpath = @"C:\订单导入备份\" + fileName;
            bffSelectPath.SaveAs(strpath);

        bffSelectPath.Dispose();
        result = true;
        return result;
     }

    void ResetData() {
        //Response.Redirect(Request.Url.AbsoluteUri);  
        txtProcessNo.Text = string.Empty;
        txtOprNo.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtPlanStartDate1.Value = string.Empty;
        txtPlanStartDate2.Value= string.Empty;
        gdMfgorder.Clear();

    }

    #endregion





   
}