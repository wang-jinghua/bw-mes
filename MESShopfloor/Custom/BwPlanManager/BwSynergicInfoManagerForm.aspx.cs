﻿using Infragistics.WebUI.UltraWebGrid;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.ReportBusiness;

public partial class Custom_BwPlanManager_BwSynergicInfoManagerForm : BaseForm
{
    int m_PageSize = 30;
    uMESSynergicInfoBusiness synergicBal = new uMESSynergicInfoBusiness();
    uMESContainerBusiness containerBal = new uMESContainerBusiness();
    uMESCommonBusiness common = new uMESCommonBusiness();
    static string businessName = "外协管理";
    static string parentName = "Synergicinfo";
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：外协转出";
        master.strTitle = "外协转出";
        master.ChangeFrame(true);

        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { SearchData(upageTurning.CurrentPageIndex); });

        if (!IsPostBack)
        {
            InitControl();
            ClearMessage();
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    }

    /// <summary>
    /// 清除提示信息
    /// </summary>
    protected override void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"\", true);</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }
    #endregion

    #region 事件
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ClearMessage();
        try { ClearData(false, true, true); SearchData(1); } catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }

    protected void gdContainer_ActiveRowChange(object sender, Infragistics.WebUI.UltraWebGrid.RowEventArgs e)
    {
        ClearMessage();
        LoadSpecInfo(e.Row);
    }

    protected void btnReSet_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ClearMessage();
        try
        {
            ResultModel re = SaveData();
            if (re.IsSuccess)
            {
                ClearData(false, false, true);
                SearchData(1);
                if (re.IsSuccess)
                    ShowStatusMessage("保存成功", true);
            }
            else
            {
                ShowStatusMessage(re.Message, re.IsSuccess);
            }

        }
        catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        SearchData(1);
    }
    #endregion

    #region 方法
    /// <summary>
    /// 查询数据
    /// </summary>
    /// <param name="index"></param>
    void SearchData(int index)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["CurrentPageIndex"] = index.ToString();
        para["PageSize"] = m_PageSize.ToString();

        para["ProcessNo"] = txtProcessNo.Text.Trim();
        para["ContainerName"] = txtContainerName.Text.Trim();
        para["ProductName"] = txtProductName.Text.Trim();
        para["PlannedStartDate1"] = txtPlanStartDate1.Value.Trim();
        para["PlannedStartDate2"] = txtPlanStartDate2.Value.Trim();

        if (!string.IsNullOrWhiteSpace(txtScan.Text.Trim()))
        {
            para["ScanContainerName"] = txtScan.Text.Trim();
        }
        uMESPagingDataDTO result = synergicBal.GetSynergicOutData(para);
        this.gdContainer.DataSource = result.DBTable;
        this.gdContainer.DataBind();
        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;
    }
    /// <summary>
    /// 加载工序信息
    /// </summary>
    void LoadSpecInfo(UltraGridRow row)
    {
        ShowStatusMessage("", true);

        string synergicinfoid = row.Cells.FromKey("SynergicinfoId").Text;
        if (string.IsNullOrWhiteSpace(synergicinfoid))
        {
            gdSpec.Columns.FromKey("ckSelect").Hidden = false;

            DataTable dt = synergicBal.GetSynergicSpec(row.Cells.FromKey("WorkflowID").Text);
            var rows = dt.Select("sequence=" + row.Cells.FromKey("sequence").Text);
            gdSpec.DataSource = rows.CopyToDataTable();
            gdSpec.DataBind();

            ddlSynergicCompany.Enabled = true;
        }
        else
        {
            gdSpec.Columns.FromKey("ckSelect").Hidden = true;

            DataTable dt = synergicBal.GetSynergicApplySpec(synergicinfoid);
            gdSpec.DataSource = dt;
            gdSpec.DataBind();

            //外协单位
            ddlSynergicCompany.SelectedValue = row.Cells.FromKey("TofactoryId").Text;
            ddlSynergicCompany.Enabled = false;
        }
    }

    protected void gdSpec_DataBound(object sender, EventArgs e)
    {
        UltraGridRow activeRow = gdContainer.DisplayLayout.ActiveRow;
        int intQty = 0;
        if (activeRow != null)
        {
            if (activeRow.Cells.FromKey("Qty").Value != null)
            {
                intQty = Convert.ToInt32(activeRow.Cells.FromKey("Qty").Value.ToString());
            }
        }
        for (int i = 0; i < gdSpec.Rows.Count; i++)
        {
            string strSpecName = gdSpec.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            gdSpec.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);

            if (gdSpec.Rows[i].Cells.FromKey("unitworktime").Value != null)
            {

                gdSpec.Rows[i].Cells.FromKey("totalworktime").Text = (intQty * Convert.ToDouble(gdSpec.Rows[i].Cells.FromKey("unitworktime").Text)).ToString();
            }
        }
    }

    /// <summary>
    /// 提交数据
    /// </summary>
    /// <returns></returns>
    ResultModel SaveData()
    {
        ResultModel re = new ResultModel(false, "");

        DataTable containerDt = uMESCommonFunction.GetGridChooseInfo2(gdContainer, null, "ckSelect");

        if (containerDt.Rows.Count == 0)
        {
            return new ResultModel(false, "请先选择批次记录");
        }

        //if (containerDt.Rows.Count > 1) {
        //    return new ResultModel(false, "转出只能选择单个");
        //}


        //UltraGridRow activeRow = gdContainer.DisplayLayout.ActiveRow;

        //必填项验证
        if (string.IsNullOrWhiteSpace(txtCompletedate.Value) || string.IsNullOrWhiteSpace(txtOutHandoverEmp.Text))
        {
            re.Message = "请填入要求完成时间或交接人";
            return re;
        }
        DateTime comDate;
        if (DateTime.TryParse(txtCompletedate.Value, out comDate) == false)
        {
            re.Message = "要求完成时间格式错误";
            return re;
        }

        //验证
        foreach (DataRow dr in containerDt.Rows)
        {
            if (dr["MoveConfirm"].ToString() == "否"&& int.Parse(dr["sequence"].ToString())>1)
            {
                re.Message = "有批次移工未确认";
                return re;
            }
        }

        foreach (DataRow dr in containerDt.Rows)
        {
            re = SaveSingleData(dr);
            if (re.IsSuccess)
                ContainerExcuteLog(dr);
        }

        if (re.IsSuccess)
        { 
            re.Message = "保存成功";
        }
        return re;
    }

    ResultModel SaveData2(string synergicinfoId)
    {
        ResultModel re = new ResultModel(false, "");
        Dictionary<string, Object> conditionPara = new Dictionary<string, Object>();
        conditionPara.Add("SynergicinfoId", "'" + synergicinfoId + "'");
        Dictionary<string, Object> updatePara = new Dictionary<string, Object>();
        updatePara.Add("SubmitEmployeeId", "'" + UserInfo["EmployeeID"] + "'");
        updatePara.Add("SubmitDate", "sysdate");
        updatePara["SubmitNotes"] = "'" + txtSubmitNotes.Text + "'";
        updatePara["PlannedComplitionDate"] = DateTime.Parse(txtCompletedate.Value);
        updatePara["OuthandoverEmp"] = "'" + txtOutHandoverEmp.Text + "'";
        re.IsSuccess = containerBal.UpdateTableByField("Synergicinfo", updatePara, conditionPara);
        if (re.IsSuccess)
        {            
            re.Message = "保存成功";
        }
        return re;
    }

    /// <summary>
    /// 获取转回工序
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    string GetReturnSpecID(DataRow dr)
    {
        string synergicinfoid = dr["SynergicinfoId"].ToString();
        DataTable dt = new DataTable();
        if (string.IsNullOrWhiteSpace(synergicinfoid))
        {
            dt = synergicBal.GetSynergicSpec(dr["WorkflowID"].ToString());
            var rows = dt.Select("sequence>=" + dr["sequence"].ToString());
            dt = rows.CopyToDataTable();
        }
        else
        {
            dt = synergicBal.GetSynergicApplySpec(synergicinfoid);
        }

        if (dt.Rows.Count == 0)
            return "";

        return dt.Rows[dt.Rows.Count - 1]["SpecID"].ToString();
    }
    /// <summary>
    /// 获取申请的工序信息
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    DataTable GetApplySpecInfo(DataRow dr)
    {
        string synergicinfoid = dr["SynergicinfoId"].ToString();
        DataTable dt = new DataTable();
        if (string.IsNullOrWhiteSpace(synergicinfoid))
        {
            dt = synergicBal.GetSynergicSpec(dr["WorkflowID"].ToString());
            var rows = dt.Select("sequence=" + dr["sequence"].ToString());

            dt = rows.CopyToDataTable();
        }
        else
        {
            dt = synergicBal.GetSynergicApplySpec(synergicinfoid);
        }
        return dt;
    }

    /// <summary>
    /// 保存单个批次的转出
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    private ResultModel SaveSingleData(DataRow dr)
    {
        ResultModel re = new ResultModel(false, "");
        string synergicinfoId = dr["SynergicinfoId"].ToString();
        if (!string.IsNullOrWhiteSpace(synergicinfoId))
        {
            //手动申请的情况下是另一种逻辑
            re = SaveData2(synergicinfoId);
            return re;
        }
        DataTable dt = uMESCommonFunction.GetGridChooseInfo(gdSpec, null, "ckSelect");
        if (dt.Rows.Count == 0)
        {
            re.Message = "请选择工序";
            return re;
        }
        //判断是否连续
        if (dt.Rows.Count > 1)
        {
            for (int i = 1; i < dt.Rows.Count; i++)
            {
                int curSeq = int.Parse(dt.Rows[i]["sequence"].ToString());
                int preSeq = int.Parse(dt.Rows[i - 1]["sequence"].ToString());
                if (preSeq + 1 != curSeq)
                {
                    re.Message = "请选择连续的工序";
                    return re;
                }
            }
        }
        //存入数据
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["ContainerId"] = dr["ContainerID"].ToString();
        para["FromFactoryId"] = UserInfo["FactoryID"];
        para["SpecId"] = dr["SpecId"].ToString();
        para["TofactryId"] = ddlSynergicCompany.SelectedValue;
        para["PlannedComplitionDate"] = txtCompletedate.Value;
        para["Qty"] = dr["Qty"].ToString();
        para["SubmitEmployeeId"] = UserInfo["EmployeeID"];
        para["SubmitNotes"] = txtSubmitNotes.Text;
        para["UomId"] = dr["UomID"].ToString();
        para["WorkflowId"] = dr["WorkflowID"].ToString();
        para["Status"] = "''";
        para["OuthandoverEmp"] = txtOutHandoverEmp.Text;
        para["ReturnSpecID"] = GetReturnSpecID(dr);
        re = synergicBal.SaveSynergicInfoOut(para);
        
        return re;
    }

    /// <summary>
    /// 初始化控件信息
    /// </summary>
    void InitControl()
    {
        DataTable dt = containerBal.GetTableInfo("synergiccompany", null, "");
        ddlSynergicCompany.DataTextField = "synergiccompanyname";
        ddlSynergicCompany.DataValueField = "synergiccompanyid";
        ddlSynergicCompany.DataSource = dt;
        ddlSynergicCompany.DataBind();
    }

    ///清除数据
    void ClearData(bool isCondition, bool isGrid, bool isDetail)
    {
        if (isCondition)
        {
            txtScan.Text = ""; txtContainerName.Text = ""; txtPlanStartDate1.Value = ""; txtPlanStartDate2.Value = "";
            txtProcessNo.Text = ""; txtProductName.Text = "";
        }
        if (isGrid)
        {
            gdContainer.Rows.Clear();
        }
        if (isDetail)
        {
            gdSpec.Rows.Clear();
            txtCompletedate.Value = "";
            txtOutHandoverEmp.Text = "";
            txtSubmitNotes.Text = "";
        }
    }

    /// <summary>
    /// 打印转储单
    /// </summary>
    /// <returns></returns>
    ResultModel PrintContainer()
    {
        ResultModel re = new ResultModel(false, "");
        DataTable dt = uMESCommonFunction.GetGridChooseInfo2(gdContainer, null, "ckSelect");

        var tempView = dt.DefaultView;

        if (tempView.ToTable(true, "synergiccompanyname").Rows.Count > 1)
        {
            return new ResultModel(false, "批量打印只能选择相同外协单位的");
        }

        if (dt.Rows.Count == 0)
        {
            return new ResultModel(false, "请选择记录");
        }

        //创建PDF文档
        Document document = new Document(PageSize.A4, 40, 40, 40, 40);
        string strPath = Server.MapPath(Request.ApplicationPath) + ConfigurationManager.AppSettings["PDFFilePath"];
        string strFileName = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff") + ".pdf";
        PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(strPath + strFileName, FileMode.Create));
        document.Open();

        BaseFont baseFont = BaseFont.CreateFont("C:\\WINDOWS\\FONTS\\simfang.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

        document.NewPage();

        document.Add(pdfTitleTable(baseFont));

        var table = pdfMainTable(baseFont);
        iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 10);

        int num = 0;
        foreach (DataRow dr in dt.Rows)
        {

            ContainerExcuteLog(dr, true);

            DataTable specDt = GetApplySpecInfo(dr);
            foreach (DataRow specDr in specDt.Rows)
            {
                string specName = specDr["SpecName"].ToString();
                string operationName = "";//工序信息
                if (specName.Contains("-"))
                    operationName = specName.Substring(specName.LastIndexOf('-') + 1);
                else
                    operationName = specName;
                table.AddCell(SetCell(dr["ProcessNo"].ToString(), font, 30));
                table.AddCell(SetCell(dr["ProductName"].ToString(), font, 30));
                table.AddCell(SetCell(dr["ContainerName"].ToString().Replace(dr["ProductName"].ToString() + "/", ""), font, 30));
                table.AddCell(SetCell(dr["ProductDesc"].ToString(), font, 30));
                table.AddCell(SetCell(operationName, font, 30));//工序
                table.AddCell(SetCell(dr["Qty"].ToString(), font, 30));
                table.AddCell(SetCell(dr["unitworktime"].ToString(), font, 30));
                num++;
            }

        }
        //if (num < 17) {
        //    for (int i = num; i < 17; i++) {
        //        table.AddCell(SetCell("", font, 30));
        //        table.AddCell(SetCell("", font, 30));
        //        table.AddCell(SetCell("", font, 30));
        //        table.AddCell(SetCell("", font, 30));
        //        table.AddCell(SetCell("", font, 30));
        //        table.AddCell(SetCell("", font, 30));
        //        table.AddCell(SetCell("", font, 30));
        //    }
        //}
        table.AddCell(SetCell("承制单位", new iTextSharp.text.Font(baseFont, 12), 30, 1));
        table.AddCell(SetCell(dt.Rows[0]["synergiccompanyname"].ToString(), font, 30, 6));
        document.Add(table);

        table = pdfEmployeeTable(baseFont);
        table.AddCell(SetCell("制单人", new iTextSharp.text.Font(baseFont, 12))); table.AddCell(SetCell(UserInfo["FullName"], font));
        table.AddCell(SetCell("部门", new iTextSharp.text.Font(baseFont, 12)));
        table.AddCell(SetCell(UserInfo["FactoryName"], font));
        table.AddCell(SetCell("制单时间", new iTextSharp.text.Font(baseFont, 12))); table.AddCell(SetCell(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), font));
        document.Add(table);

        var cb = writer.DirectContent;
        ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER, new Phrase(""), 200, 30, 0);
        document.Close();
        writer.Close();

        string strScript = "<script>window.open('../../ExportFile/" + strFileName + "');</script>";
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);


        re.IsSuccess = true;
        return re;
    }

    PdfPTable pdfTitleTable(BaseFont baseFont)
    {
        PdfPTable table = new PdfPTable(6);
        table.TotalWidth = 500;
        table.LockedWidth = true;

        iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 20);

        PdfPCell cell = new PdfPCell(new Phrase("外协转出单", font));
        cell.Colspan = 4;
        cell.MinimumHeight = 50;
        cell.BorderWidth = 0;
        cell.PaddingRight = 50;
        cell.HorizontalAlignment = Element.ALIGN_RIGHT;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        table.AddCell(cell);

        font = new iTextSharp.text.Font(baseFont, 12);
        cell = new PdfPCell(new Phrase("编号：WX" + DateTime.Now.ToString("yyyyMMddHHmmss"), font));
        cell.Colspan = 2;
        cell.MinimumHeight = 50;
        cell.BorderWidth = 0;
        cell.PaddingBottom = 10;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_BOTTOM;
        table.AddCell(cell);
        return table;
    }

    protected PdfPTable pdfMainTable(BaseFont baseFont)
    {
        PdfPTable table = new PdfPTable(7);
        table.SetWidths(new float[] { 80, 80, 50, 120, 80, 40, 40 });
        table.TotalWidth = 500;
        table.LockedWidth = true;
        PdfPCell cell; List<PdfPCell> listCells = new List<PdfPCell>();



        iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 12);
        cell = new PdfPCell(new Phrase("工作令号", font));
        cell.MinimumHeight = 30;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;

        table.AddCell(cell);

        table.AddCell(SetCell("图号", font, 30));
        table.AddCell(SetCell("批次号", font, 30));
        table.AddCell(SetCell("产品名称", font, 30));
        table.AddCell(SetCell("工序名称", font, 30));
        table.AddCell(SetCell("数量", font, 30));
        table.AddCell(SetCell("定额", font, 30));

        //listCells.Add(cell);
        //listCells.Add(SetCell("图号",font,40));
        //listCells.Add(SetCell("批次号", font, 40));
        //listCells.Add(SetCell("产品名称", font, 40));
        //listCells.Add(SetCell("数量", font, 40));
        //listCells.Add(SetCell("定额", font, 40));

        //row = new PdfPRow(listCells.ToArray());

        // table.Rows.Add(row);


        return table;
    }

    PdfPTable pdfEmployeeTable(BaseFont baseFont)
    {
        PdfPTable table = new PdfPTable(6);
        table.SetWidths(new float[] { 80, 80, 80, 80, 80, 100 });
        table.TotalWidth = 500;
        table.LockedWidth = true;
        List<PdfPCell> listCells = new List<PdfPCell>();
        iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 12);
        table.AddCell(SetCell("移交人", font)); table.AddCell(SetCell("", font));
        table.AddCell(SetCell("接收人", font)); table.AddCell(SetCell("", font));
        table.AddCell(SetCell("转出时间", font)); table.AddCell(SetCell("", font));
        return table;
    }

    PdfPCell SetCell(string text, Font font, int minHeigth = 30, int colSpan = 1, int rowSpan = 1)
    {
        PdfPCell cell = new PdfPCell(new Phrase(text, font));
        cell.Colspan = colSpan; cell.Rowspan = rowSpan;
        cell.MinimumHeight = minHeigth;
        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        return cell;
    }

    #region 批次日志记录 add:Wangjh 20210120
    void ContainerExcuteLog(DataRow dr,bool isPrint=false)
    {
        MESAuditLog ml = new MESAuditLog();
        ml.ContainerName =dr["ContainerName"].ToString(); ml.ContainerID = dr["ContainerID"].ToString();
        ml.ParentID = dr["SynergicinfoId"].ToString(); ml.ParentName = parentName;
        ml.CreateEmployeeID = UserInfo["EmployeeID"];
        ml.BusinessName = businessName; ml.OperationType = 1;
        if (!isPrint)
            ml.Description = "外协转出:" + "转出到:" + dr["synergiccompanyname"].ToString();
        else
        { ml.OperationType = -1; ml.Description = "外协转出:" + "转出单打印"; }
        common.SaveMESAuditLog(ml);

    }

    #endregion

    #endregion

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();
            UltraGridRow uldr = gdContainer.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                ShowStatusMessage("请选择订单记录", false);
                return;
            }
            DataTable poupDt = new DataTable();
            poupDt.Columns.Add("ProductID");
            poupDt.Columns.Add("WorkflowID");
            DataRow newRow = poupDt.NewRow();
            newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
            newRow["WorkflowID"] = uldr.Cells.FromKey("Workflowid").Text;
            poupDt.Columns.Add("ContainerID"); poupDt.Columns.Add("ContainerName");
            newRow["ContainerID"] = uldr.Cells.FromKey("ContainerID").Text;
            newRow["ContainerName"] = uldr.Cells.FromKey("ContainerName").Text;
            poupDt.Rows.Add(newRow);
            Session.Add("ProcessDocument", poupDt);
            string strScript = string.Empty;
            strScript = "<script>window.showModalDialog('../bwCommonPage/uMESDocumentViewPopupForm.aspx', '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();
            var re = PrintContainer();
            if (re.IsSuccess == false)
            {
                DisplayMessage(re.Message, re.IsSuccess);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}