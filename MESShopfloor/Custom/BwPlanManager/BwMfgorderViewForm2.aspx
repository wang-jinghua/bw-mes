﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" EnableEventValidation="true" AutoEventWireup="true" CodeFile="BwMfgorderViewForm2.aspx.cs" Inherits="Custom_BwPlanManager_BwMfgorderViewForm2" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>

<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">    
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
    <div style="width:225px;float:left;margin-right:10px">
        <div>
        
        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;">
                <colgroup>
                  <col width="auto">
                  <col width="50">
                </colgroup>
                    <tr>
                        <td align="left"  class="tdRightAndBottom" colspan="1" >
                            <div class="divLabel">工作令号：</div>
                            <div style="height:28px;padding-right:2px">   
                            <asp:TextBox ID="txtTreeProcessNo" runat="server" class="stdTextBoxFull"></asp:TextBox>
                            </div>
                        </td>
                        

                        <td class="tdRightAndBottom"  align="right" valign="bottom" colspan="1">
                            <asp:Button ID="btnTreeSearch" runat="server" Text="查询"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnTreeSearch_Click"  />
                            <asp:Button ID="btnTreeReset" runat="server" Text="重置" Visible="false"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnTreeReset_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"   class="tdRightAndBottom" colspan="2" style="text-align:left">
                                    <%--<igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel2" runat="server" Width="100%">--%>
                                     <div>产品</div>
                                     <gPI:getProductInfo ID="getProductInfo" runat="server" />
                                     <%--</igmisc:WebAsyncRefreshPanel>--%>
                                </td>
                       
                    </tr>
                </table>

        </div>
        <asp:TreeView ID="tvTree" runat="server" Height="450px" Width="300px" ShowLines="true"  Visible="false"
                        BorderWidth="1px" Style="overflow:auto ;left: 0px; position: relative; top: 0px;text-align:left;
                        " NodeWrap="True" BorderStyle="Notset" OnSelectedNodeChanged="tvTree_SelectedNodeChanged"    >
                    </asp:TreeView>
        <ignav:UltraWebTree ID="treeProduct" runat="server" Height="475px" Width="220px" Style="border:1px solid #333;margin-top:3px"
            WebTreeTarget="ClassicTree" OnNodeSelectionChanged="treeProduct_NodeSelectionChanged">    
             <SelectedNodeStyle BackColor="#66CCFF" />          
                <AutoPostBackFlags NodeChanged="True" NodeChecked="False" NodeCollapsed="False" NodeDropped="False" NodeExpanded="False" />
            </ignav:UltraWebTree>
        
    </div>
<%--    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" Style="float:left">--%>
        <div style="height:100%">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%" style="table-layout:fixed;">
                <colgroup>
                  <col width="auto">
                    <col width="auto">
                  <col width="270">
                    <col width="auto">
                </colgroup>
                <tr>
                    <td align="left" class="tdLRAndBottom">
                        <div class="divLabel">工作令号：</div>    
                        <div style="height:28px;padding-right:2px">                     
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" class="tdRightAndBottom" style="display:none">
                        <div class="divLabel">作业令号：</div>    
                        <div style="height:28px;padding-right:2px">                         
                            <asp:TextBox ID="txtOprNo" runat="server" class="stdTextBoxFull"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">产品图号：</div>
                        <div style="height:28px;padding-right:2px">   
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" nowrap="nowrap" class="tdRightAndBottom">
                        <div class="divLabel">计划开始日期：</div>
                        <div style="height:28px;padding-right:2px">   
                        <input id="txtPlanStartDate1" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" style="width:110px" type="text" />
                        -
                    <input id="txtPlanStartDate2" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" style="width:110px" type="text" />
                            </div>
                    </td>
                    <td class="tdRightAndBottom" nowrap="nowrap" align="left" valign="bottom">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置" Visible="false"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
                <tr>
                     <td align="left" class="tdLRAndBottom" style="display:none" >
                        <div class="divLabel">工作令号：</div>
                         <div style="height:28px;padding-right:2px">   
                        <asp:TextBox ID="txtSrTreeProcessNo" runat="server" class="stdTextBoxFull" Enabled="false"></asp:TextBox>
                            </div>
                    </td>
                     <td align="left" class="tdLRAndBottom">
                        <div class="divLabel">所选产品图号：</div>
                          <div style="height:28px;padding-right:2px"> 
                        <asp:TextBox ID="txtSrTreeProduct" runat="server" class="stdTextBoxFull" Enabled="false"></asp:TextBox>
                           </div>
                    </td>
                    <td class="tdRightAndBottom" nowrap="nowrap" colspan="3" valign="bottom" align="left" >
                        <asp:Button ID="btnSearchAll" runat="server" Text="查询该产品及往下所有"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearchAll_Click"  />
                        <asp:Button ID="Button2" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <div id="ItemDiv" runat="server" style="">
            <igtbl:UltraWebGrid ID="gdMfgorder" runat="server" Height="400px" Width="100%">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect">
                                <CellTemplate>
                                    <asp:CheckBox ID="ckSelect" runat="server" />
                                </CellTemplate>
                                <Header Caption=""></Header>
                            </igtbl:TemplatedColumn>
                            <igtbl:UltraGridColumn Key="MfgorderName" Width="200px" BaseColumnName="MfgorderName" AllowGroupBy="No">
                                <Header Caption="订单号">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProcessNo" Key="ProcessNo" Width="120px" AllowGroupBy="No">
                                <Header Caption="工作令号">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="120px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="作业令号">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="120px" AllowGroupBy="No">
                                <Header Caption="图号">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductRev" Key="ProductRev" Width="120px" AllowGroupBy="No">
                                <Header Caption="版本">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductDesc" Key="ProductDesc" Width="120px" AllowGroupBy="No">
                                <Header Caption="名称">
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="Qty" AllowGroupBy="No" BaseColumnName="Qty" Width="60px">
                                <Header Caption="数量">
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="UomName" BaseColumnName="UomName" Width="60px" AllowGroupBy="No">
                                <Header Caption="单位">
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="PlannedStartDate" Key="PlannedStartDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                <Header Caption="计划开始日期">
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="PlannedCompletionDate" Key="PlannedCompletionDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                <Header Caption="计划完成日期">
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="PriorityName" Key="PriorityName" Width="100px" AllowGroupBy="No">
                                <Header Caption="优先级">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OrdertypeName" Key="OrdertypeName" Width="100px" AllowGroupBy="No">
                                <Header Caption="订单类型">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="FinishedToFactoryName" Key="FinishedToFactoryName" Width="120px" AllowGroupBy="No">
                                <Header Caption="最后交往">
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ERPOrderID" Key="ERPOrderID" Width="120px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="ERP内码">
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Notes" Key="Notes" Width="200px" AllowGroupBy="No">
                                <Header Caption="备注">
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID" Width="10px">
                                <Header Caption="产品ID">
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowName" Hidden="True" Key="WorkflowName" Width="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowRev" Hidden="True" Key="WorkflowRev">
                                <Header Caption="工艺版本">
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Hidden="True" Key="WorkflowID">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="MfgorderID" Hidden="True" Key="MfgorderID">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                              <igtbl:UltraGridColumn BaseColumnName="ReplaceMaterial" Hidden="true" Key="ReplaceMaterial" Width ="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ReplaceMaterialName" Hidden="true" Key="ReplaceMaterialName" Width ="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                              <igtbl:UltraGridColumn BaseColumnName="ReplaceMaterialQty" Hidden="true" Key="ReplaceMaterialQty" Width ="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px" AllowRowNumberingDefault="Continuous">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="450px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
        </div>
        <div style="">
            <table width="100%">
                <tr>
                    <td colspan="2" align="left">
                         <asp:Button ID="btnSelectAll" runat="server" Text="全选"
                            CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnSelectAll_Click"  />
                        <asp:Button ID="btnInRevert" runat="server" Text="反选"
                                    CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnInRevert_Click" />

                    </td>
                    <td align="right">
                        <uPT:pageTurning ID="upageTurning" runat="server" Visible="true" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; " colspan="3">
                        <asp:Button ID="btnStart" runat="server" Text="批次创建"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnStart_Click" />
                        <asp:Button ID="btnStarts" runat="server" Text="批量创建" Visible="true" Style="margin-left: 20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnStarts_Click" />
                        <asp:Button ID="Button1" runat="server" Text="图纸工艺查看" style="margin-left:20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                    </td>
                    
                </tr>
            </table>
        </div>

        <div style="margin-top: 20px">
            <table style="width: 100%;">
            </table>
        </div>
        <div id="dvPopupClosedButton">
            <asp:Button ID="popupClosedButton" Visible="true" runat="server" Width="0px" Text="" Style="position: absolute; top: -500px" OnClick="popupClosedButton_Click" />
        </div>

        <div id="BwContainerStartPopup"></div>
        </div>
<%--    </igmisc:WebAsyncRefreshPanel>--%>
    </igmisc:WebAsyncRefreshPanel>
    <script type="text/javascript">

        function openform() {

            var returnValue = window.showModalDialog("BwContainerStartPopupForm.aspx?", "详细信息", "dialogHeight:280px; dialogWidth:550px; dialogTop:280px; dialogLeft:550px; edge:Raised; center: No; resizable: NO; status: No;");


        }
        function showPopup() {
            $('#BwContainerStartPopup').window({
                content: '<iframe scrolling="auto" frameborder="0" src="' + "BwContainerStartPopupForm.aspx" + '" style="width:100%;height:100%;padding:0;margin:0;display:block;" onLoad=""></iframe>',
                title: "批次创建",
                width: 1000,
                height: 500,
                modal: true
            });
        }
    </script>
</asp:Content>
