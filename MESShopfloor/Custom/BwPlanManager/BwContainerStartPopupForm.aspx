﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="BwContainerStartPopupForm.aspx.cs" Inherits="Custom_BwPlanManager_BwContainerStartPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetWorkFlowInfo.ascx" TagName="getWorkFlowInfo"
    TagPrefix="gPI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>批次创建</title>
    <base target="_self" />
    <script type="text/javascript" src="/MESShopfloor/Scripts/DateControl.js"></script>
</head>
<body>
    <form runat="server">

        <div id="ItemDiv" runat="server" style="margin: 5px;">
            <igtbl:UltraWebGrid ID="gdMfgorder" runat="server" Height="60px" Width="98%">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:TemplatedColumn Width="20px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                <CellTemplate>
                                    <asp:CheckBox ID="ckSelect" runat="server" />
                                </CellTemplate>
                                <Header Caption=""></Header>
                            </igtbl:TemplatedColumn>
                            <igtbl:UltraGridColumn Key="MfgorderName" Width="120px" BaseColumnName="MfgorderName" AllowGroupBy="No">
                                <Header Caption="订单号">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProcessNo" Key="ProcessNo" Width="120px" AllowGroupBy="No">
                                <Header Caption="工作令号">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="120px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="作业令号">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="120px" AllowGroupBy="No">
                                <Header Caption="图号">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductRev" Key="ProductRev" Width="60px" AllowGroupBy="No">
                                <Header Caption="版本">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductDesc" Key="ProductDesc" Width="120px" AllowGroupBy="No">
                                <Header Caption="名称">
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="MfgQty" AllowGroupBy="No" BaseColumnName="MfgQty" Width="100px">
                                <Header Caption="订单数量">
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="Qty" BaseColumnName="Qty" Width="80px" AllowGroupBy="No">
                                <Header Caption="可投数">
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="UomName" Key="UomName" Width="60px" AllowGroupBy="No">
                                <Header Caption="单位">
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="PlannedStartDate" Key="PlannedStartDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                <Header Caption="计划开始日期">
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="PlannedCompletionDate" Key="PlannedCompletionDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                <Header Caption="计划完成日期">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="PriorityName" Key="PriorityName" Width="100px" AllowGroupBy="No" Hidden="True">
                                <Header Caption="优先级">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OrdertypeName" Key="OrdertypeName" Width="100px" AllowGroupBy="No" Hidden="True">
                                <Header Caption="订单类型">
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="FinishedToFactoryName" Key="FinishedToFactoryName" Width="120px" AllowGroupBy="No" Hidden="True">
                                <Header Caption="最后交往">
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ERPOrderID" Key="ERPOrderID" Width="120px" AllowGroupBy="No" Hidden="True">
                                <Header Caption="ERP内码">
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Notes" Hidden="True" Key="Notes" Width="200px" AllowGroupBy="No">
                                <Header Caption="备注">
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID" Width="10px">
                                <Header Caption="产品ID">
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowName" Hidden="True" Key="WorkflowName" Width="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowRev" Hidden="True" Key="WorkflowRev">
                                <Header Caption="工艺版本">
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Hidden="True" Key="WorkflowID">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="MfgorderID" Hidden="True" Key="MfgorderID">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                             <igtbl:UltraGridColumn BaseColumnName="ReplaceMaterial" Hidden="true" Key="ReplaceMaterial" Width ="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ReplaceMaterialName" Hidden="true" Key="ReplaceMaterialName" Width ="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ReplaceMaterialQty" Hidden="true" Key="ReplaceMaterialQty" Width ="120px">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="60px" Width="98%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
        </div>
        <div id="dvContainerInfo" style="margin: 5px; font-size: 16px">
            <table border="0" cellpadding="5" cellspacing="0" width="98%">
                <tr>
                    <td valign="top">
                        <table class="stdTable" cellpadding="5" cellspacing="0" width="98%">
                            <tr style="display: none;">
                                <td align="left" class="tdRightAndBottom">
                                    <div>批次层级</div>
                                    <asp:DropDownList ID="ddlContainerLevel" runat="server" Width="120px" Height="28px" Style="font-size: 16px"></asp:DropDownList>
                                </td>
                                <td align="left" class="tdRightAndBottom">
                                    <div>开始原因</div>
                                    <asp:DropDownList ID="ddlStartReason" runat="server" Width="120px" Height="28px" Style="font-size: 16px"></asp:DropDownList>
                                </td>
                                <td align="left" class="tdRightAndBottom">
                                    <div>所有者</div>
                                    <asp:DropDownList ID="ddlOwner" runat="server" Width="120px" Height="28px" Style="font-size: 16px"></asp:DropDownList>
                                </td>
                                <td align="left" class="tdRightAndBottom">
                                    
                                </td>
                                <td align="left" class="tdRightAndBottom">
                                    
                                </td>
                                <td align="left" class="tdRightAndBottom"></td>
                            </tr>
                            <tr>
                                <td align="left" class="tdRightAndBottom">
                                    <div>批次号</div>
                                    <asp:TextBox ID="txtContainerYearNo" runat="server" Width="50" Enabled="false" Height="20px" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>

                                    <asp:TextBox ID="txtContainerNo" runat="server" Width="50" Height="20px" Style="margin-right: 8px" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                                </td>
                                <td align="left" class="tdRightAndBottom">&nbsp;</td>
                                <td align="left" class="tdRightAndBottom">
                                    <div>产品</div>
                                    <gPI:getProductInfo ID="getProductInfo" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="tdRightAndBottom">
                                    <div>数量</div>
                                    <asp:TextBox ID="txtQty" runat="server" Width="120" Height="20px" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                                </td>
                                <td align="left" class="tdRightAndBottom">
                                    <div>单位</div>
                                    <asp:DropDownList ID="ddlUom" runat="server" Width="120px" Height="28px" Style="font-size: 16px"></asp:DropDownList>
                                </td>
                                <td align="left" class="tdRightAndBottom" colspan="2">
                                    <div>工艺路线</div>
                                    <gPI:getWorkFlowInfo ID="getWorkFlowInfo" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="tdRight">
                                    <div>批次计划开始时间</div>
                                    <input id="txtPlannedstartdate" runat="server" onclick="this.value = ''; setday(this);" style="width: 120px" class="" type="text" />
                                </td>
                                <td align="left" class="tdRight">
                                    <div>批次计划结束时间</div>
                                    <input id="txtPlannedcompletiondate" runat="server" onclick="this.value = ''; setday(this);" style="width: 120px" class="" type="text" />
                                </td>
                                <td align="left" class="tdRight">
                                    <div>备注</div>
                                    <asp:TextBox ID="txtNotes" runat="server" Width="250" Height="50px" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="display:none">
                        <table class="stdTable" cellpadding="5" cellspacing="0" width="98%">
                            <tr>
                                <td align="left" class="tdRight">
                                    <div>起始序号</div>
                                    <asp:TextBox ID="txtStartSubNo" runat="server" Width="120" Height="20px" onkeypress="if (event.keyCode<48 || event.keyCode>57) event.returnValue=false;"></asp:TextBox>
                                    <div>子批次序号</div>
                                    <igtbl:UltraWebGrid ID="gdSubContainerNo" runat="server" Height="200px" Width="300px">
                                        <Bands>
                                            <igtbl:UltraGridBand>
                                                <Columns>
                                                    <igtbl:UltraGridColumn Key="ckSelect" Type="CheckBox" Width="40px" AllowUpdate="Yes">
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SubContainerNo" Key="SubContainerNo" Width="200px">
                                                        <Header>
                                                            <RowLayoutColumnInfo OriginX="1" />
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="1" />
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                </Columns>
                                                <AddNewRow View="NotSet" Visible="NotSet">
                                                </AddNewRow>
                                            </igtbl:UltraGridBand>
                                        </Bands>
                                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                            RowHeightDefault="18px">
                                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="300px">
                                            </FrameStyle>
                                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                            </RowAlternateStyleDefault>
                                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                                StyleMode="QuickPages">
                                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                </PagerStyle>
                                            </Pager>
                                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                            </EditCellStyleDefault>
                                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                            </FooterStyleDefault>
                                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                <Padding Bottom="3px" Top="2px" />
                                                <Padding Top="2px" Bottom="3px"></Padding>
                                            </HeaderStyleDefault>
                                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                                <Padding Left="3px" />
                                            </RowSelectorStyleDefault>
                                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                                <Padding Left="3px" />
                                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                            </RowStyleDefault>
                                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                            </GroupByRowStyleDefault>
                                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                            </SelectedRowStyleDefault>
                                            <GroupByBox Hidden="True">
                                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                                </BoxStyle>
                                            </GroupByBox>
                                            <AddNewBox>
                                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                </BoxStyle>
                                            </AddNewBox>
                                            <ActivationObject BorderColor="" BorderWidth="">
                                            </ActivationObject>
                                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                    Font-Size="11px" Height="420px" Width="200px">
                                                    <Padding Left="2px" />
                                                </FilterDropDownStyle>
                                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                                </FilterHighlightRowStyle>
                                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                    Font-Size="11px">
                                                    <Padding Left="2px" />
                                                </FilterOperandDropDownStyle>
                                            </FilterOptionsDefault>
                                        </DisplayLayout>
                                    </igtbl:UltraWebGrid>
                                </td>
                                <td class="tdRight" align="left">
                                    <div style="float: left; width: 99px;">
                                        <asp:Button ID="btnAdd" runat="server" Text="添加" Width="80px" Style="margin-top: 20px"
                                            CssClass="searchButton" EnableTheming="True" OnClick="btnAdd_Click" />
                                        <asp:Button ID="btnAll" runat="server" Text="全选" Width="80px" Style="margin-top: 20px"
                                            CssClass="searchButton" EnableTheming="True" OnClick="btnAll_Click" />
                                        <asp:Button ID="btnNotAll" runat="server" Text="全不选" Width="80px" Style="margin-top: 20px"
                                            CssClass="searchButton" EnableTheming="True" OnClick="btnNotAll_Click" />
                                        <asp:Button ID="btnDel" runat="server" Text="删除" Width="80px" Style="margin-top: 20px"
                                            CssClass="searchButton" EnableTheming="True" OnClick="btnDel_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>



        <div style="margin: 5px;">
            <asp:Button ID="btnStart" runat="server" Text="批次创建" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                CssClass="searchButton" EnableTheming="True" Style="margin-right: 20px" OnClick="btnStart_Click" />
            <asp:Button ID="CloseButton" runat="server" Text="关闭"
                CssClass="searchButton" EnableTheming="True" OnClick="CloseButton_Click" />
        </div>

        <div style="margin: 5px;">
            <asp:Label runat="server" Style="font-size: 12px; font-weight: bold;">状态信息：</asp:Label>
            <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
        </div>

        <script type="text/javascript">
            // $.ajaxSetup({ cache: false });

        </script>

    </form>
</body>
</html>
