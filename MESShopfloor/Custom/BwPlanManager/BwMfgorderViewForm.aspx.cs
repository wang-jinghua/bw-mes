﻿//Description:订单导入
//Copyright (c) : 通力凯顿（北京）系统集成有限公司
//Writer:Wangjh
//create Date:2020-4-16
//Rewriter:
//Rewrite Date:
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using System.Data;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.IO;
using Infragistics.WebUI.UltraWebGrid;

public partial class Custom_BwPlanManager_BwMfgorderViewForm : BaseForm
{
    // Dictionary<string, string> userInfo;
    int m_PageSize = 30;
    BwMfgorderImportBusiness mfgOrderImportBal = new BwMfgorderImportBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：批次创建";
        master.strTitle = "批次创建";
        master.ChangeFrame(true);

        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { SearchData(upageTurning.CurrentPageIndex); });

        if (!IsPostBack)
        {
            ClearMessage();
        }
    }

    #region 事件
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try { SearchData(1); } catch (Exception ex) { ShowStatusMessage(ex.Message, false); }

    }
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearMessage();

        //$("#dvPopupClosedButton").find(':submit').click();


        ResetData();

    }

    protected void btnStart_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);


        DataTable dt = uMESCommonFunction.GetGridChooseInfo(gdMfgorder, null, "ckSelect");

        if (dt.Rows.Count != 1)
        {
            ShowStatusMessage("请选择单条记录", false);
            return;
        }

        PopupData = dt;

        var page = "BwContainerStartPopupForm.aspx";
        var strScript = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", page, 1260, 450);
        
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);

        // ShowPopupPage("123.aspx", 1000, 500, true);


        //var pop = PopupData;
        //if (pop == null)
        //    DisplayMessage(lStatusMessage, "qwerty", false);
        //else
        //{ DisplayMessage(lStatusMessage, pop as string, false);
        //    SearchData(1);
        //}

    }
    protected void btnStarts_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("",true);
        DataTable dt = uMESCommonFunction.GetGridChooseInfo(gdMfgorder, null, "ckSelect");

        if (dt.Rows.Count < 2)
        {
            //DisplayMessage(lStatusMessage, "请选择多条记录", false);
            //return;
        }

        PopupData = dt;

        var page = "BwContainerBatchStartPopupForm.aspx";
        var strScript = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", page, 1280, 550);

        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    }


    protected void popupClosedButton_Click(object sender, EventArgs e)
    {

        if (PopupData == null)
        {
            return;
        }

        Dictionary<string, string> result = PopupData as Dictionary<string, string>;

        if (result["Page"] == "BwContainerBatchStartPopupForm.aspx")
        {
            SearchData(1);
            //ShowStatusMessage(result["Message"], true);
        }

    }

    #endregion

    #region 方法
    /// <summary>
    /// 查询订单
    /// </summary>
    /// <param name="index"></param>
    void SearchData(int index)
    {
        gdMfgorder.Rows.Clear();

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["MfgManagerID"] = UserInfo["EmployeeID"];
        para["ProcessNo"] = txtProcessNo.Text.Trim();
        para["OprNo"] = txtOprNo.Text.Trim();
        para["ProductName"] = txtProductName.Text.Trim();
        para["PlannedStartDate1"] = txtPlanStartDate1.Value.Trim();
        para["PlannedStartDate2"] = txtPlanStartDate2.Value.Trim();

        para["CurrentPageIndex"] = index.ToString();
        para["PageSize"] = m_PageSize.ToString();


        uMESPagingDataDTO result = mfgOrderImportBal.GetSearchData(para);

        this.gdMfgorder.DataSource = result.DBTable;
        this.gdMfgorder.DataBind();

        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;
    }

    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    }

    /// <summary>
    /// 清除提示信息
    /// </summary>
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"\", true);</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }

    void ResetData()
    {
        // Response.Redirect(Request.Url.AbsoluteUri);
        txtProcessNo.Text = string.Empty;
        txtOprNo.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtPlanStartDate1.Value = string.Empty;
        txtPlanStartDate2.Value = string.Empty;
        gdMfgorder.Clear();
        ShowStatusMessage("", true);
    }

    #endregion

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();

            UltraGridRow uldr = gdMfgorder.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                ShowStatusMessage("请选择订单记录", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            DataTable poupDt = new DataTable();
            poupDt.Columns.Add("ProductID");
            poupDt.Columns.Add("WorkflowID");
            DataRow newRow = poupDt.NewRow();
            newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
            newRow["WorkflowID"] = uldr.Cells.FromKey("Workflowid").Text;
            poupDt.Rows.Add(newRow);
            Session.Add("ProcessDocument", poupDt);
            string strScript = string.Empty;

            strScript = "<script>window.showModalDialog('../bwCommonPage/uMESDocumentViewPopupForm.aspx', '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}