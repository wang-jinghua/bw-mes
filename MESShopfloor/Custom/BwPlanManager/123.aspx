﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="123.aspx.cs" Inherits="Custom_BwPlanManager_123" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <base target="_self" />
     <script type="text/javascript" src="/MESShopfloor/Scripts/jquery.min.js"></script>

    <style type="text/css">
        #popIn {
            width: 669px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
   <input id="popIn" runat="server" />
         <asp:Button ID="CloseButton" runat="server" Text="关闭"
                        CssClass="searchButton" EnableTheming="True"  OnClick="CloseButton_Click"  />
        <asp:Button ID="btnQuery" runat="server" Text="查询测试"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnQuery_Click"   />
         <asp:Button ID="Button2" runat="server" Text="插入测试"
                        CssClass="searchButton" EnableTheming="True" OnClick="Button2_Click" />
        <asp:Button ID="Button3" runat="server" Text="更新测试"
                        CssClass="searchButton" EnableTheming="True" OnClick="Button3_Click"  />
        <asp:Button ID="Button4" runat="server" Text="删除测试"
                        CssClass="searchButton" EnableTheming="True" OnClick="Button4_Click"  />
        <asp:Button ID="Button5" runat="server" Text="批量sql执行"
                        CssClass="searchButton" EnableTheming="True" OnClick="Button5_Click" />
         <asp:Button ID="Button1" runat="server" Text="清除"
                        CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click"  />
    </div>
        <asp:TextBox ID="txtNotes" runat="server" Width="500px" Height="100px" TextMode="MultiLine"></asp:TextBox>
    </form>
</body>
</html>
<script type="text/javascript">
    $.ajaxSetup({ cache: false });
</script>
