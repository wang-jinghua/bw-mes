﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using  System.Data;

public partial class forms_shopfloor_WLWorkReport_uMESWLCharInputForm : System.Web.UI.Page
{
    //ParseCodeInfo pCode = new ParseCodeInfo();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            string value = Request.QueryString["sendValue"];
    
            if (Session["dtImage"] != null)
            {
                Session["dtImage1"] =Session["dtImage"];
            }


            MyToleranceInput.ParseCode(value.TrimStart("&nbsp;".ToArray()));

            //if (dtImage.Rows.Count>0)
            //{
       
            //}
          

        }
    }


    protected void Page_UnLoad(object sender, EventArgs e)
    {

        DataTable dtImage = new DataTable();
        dtImage.Columns.Add("FileName");
        dtImage.Columns.Add("HtmlCode");
 
        if (Session["dtImage1"] != null)
        {
            DataTable dt = (DataTable)Session["dtImage1"];
            DataView dv = dt.DefaultView;

            DataTable dt1 = dv.ToTable(true, "FileName", "HtmlCode");
            foreach (DataRow dr1 in dt1.Rows)
            {
                dtImage.Rows.Add();
                int intAddRow = dtImage.Rows.Count - 1;
                dtImage.Rows[intAddRow]["FileName"] = dr1["FileName"];
                dtImage.Rows[intAddRow]["HtmlCode"] = dr1["HtmlCode"];
            }
        }

        if (Session["dtImage"] != null)
        {
            DataTable dt = (DataTable)Session["dtImage"];
            DataView dv = dt.DefaultView;

            DataTable dt1 = dv.ToTable(true, "FileName", "HtmlCode");
            foreach (DataRow dr1 in dt1.Rows)
            {
                dtImage.Rows.Add();
                int intAddRow = dtImage.Rows.Count - 1;
                dtImage.Rows[intAddRow]["FileName"] = dr1["FileName"];
                dtImage.Rows[intAddRow]["HtmlCode"] = dr1["HtmlCode"];
            }
        }
        Session["dtImage"] = dtImage;

    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
         string strCode = MyToleranceInput.GetCode();
        //if (strCode.Trim().Contains("p") == true)
        //{
        //    int first = strCode.Trim().IndexOf("p") + 2;
        //    int last = strCode.Trim().LastIndexOf("p") - 5;
        //    code.Value = strCode.Substring(first, last);
        //}
        //else
        //{
            code.Value = strCode.Trim();
        //}

        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "", "WinClose('1')", true);
        //        Me.CloseWindow()
        Session["ReturnValue"] = code.Value;
        ClientScript.RegisterStartupScript(ClientScript.GetType(), "ReturnValue", "<script>ReturnValue();</script>");
    }

    public String GetCode(string strPreviewCodeDis)
    {
        String strHtmlCode, strHtml, strHtmlTemp;
        try
        {
            DataTable dtImage = new DataTable();
            int i, intStartFlag, intEndFlag;
            dtImage = (DataTable)Session["dtImage"];

            strHtml = strPreviewCodeDis;
            strHtmlCode = strHtml;

            intStartFlag = strHtmlCode.IndexOf("<img");
            if (intStartFlag == -1)
            {
                intStartFlag = strHtmlCode.IndexOf("<IMG");
            }
            if (intStartFlag > -1)
            {
                intEndFlag = strHtmlCode.IndexOf(">", intStartFlag);
                ;
            }
            else
            {
                intEndFlag = -1;
            }

            if (dtImage != null)
            {
                if (dtImage.Rows.Count > 0)
                {
                    while (intStartFlag > -1)
                    {
                        strHtmlTemp = strHtmlCode.Substring(intStartFlag, intEndFlag - intStartFlag + 1);
                        for (i = 0; i <= dtImage.Rows.Count - 1; i++)
                        {
                            if (strHtmlTemp.IndexOf(@"/" + dtImage.Rows[i][0]) > -1)
                            {
                                strHtmlCode = strHtmlCode.Replace(strHtmlTemp, (String)dtImage.Rows[i][1]);
                                break;
                            }

                        }

                        intStartFlag = strHtmlCode.IndexOf("<img", intStartFlag + 1);
                        if (intStartFlag == -1)
                        {
                            intStartFlag = strHtmlCode.IndexOf("<IMG", intStartFlag + 1);
                        }
                        if (intStartFlag > -1)
                        {
                            intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                            ;
                        }
                        else
                        {
                            intEndFlag = -1;
                        }
                        //intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                    }

                }

            }

            return strHtmlCode;
        }
        catch (Exception myError)
        {
            return "";
        }
    }

}
