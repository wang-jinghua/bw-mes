﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class uMESDataCollectPopupForm : System.Web.UI.Page
{
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESSecondaryWarehouseBusiness bll = new uMESSecondaryWarehouseBusiness();
    string webRootDir = HttpRuntime.AppDomainAppPath;
    string webRootUrl = "~";
    public FreeTextBoxControls.FreeTextBox oHtml;
    string businessName = "数据采集", parentName = "DataCollectInfo";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Session["PopupData"] !=null)
            {
                Session["FinalSaveInfo"] = null;
                wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("productno").Hidden = true;
                Dictionary<string, string> para = (Dictionary<string, string>)Session["PopupData"];
                ///控件赋值
                BindDataToControls(para);

                if (ddlCollectType.Items.Count>0)
                {
                    if (para.ContainsKey("CollectType"))
                    {
                        para.Remove("CollectType");
                    }
                    para.Add("CollectType", ddlCollectType.SelectedValue);
                }


                if (para.ContainsKey("CheckCollectType"))
                {
                    if (para["CheckCollectType"].Contains("首检"))
                    {
                        ddlCollectType.SelectedValue = "2";
                    }

                    if (para["CheckCollectType"].Contains("工序检验"))
                    {
                        ddlCollectType.SelectedValue = "3";
                    }

                    if (para["CheckCollectType"].Contains("入厂检验"))
                    {
                        ddlCollectType.SelectedValue = "4";
                    }
                    if (para.ContainsKey("CollectType"))
                    {
                        para.Remove("CollectType");
                    }
                    para.Add("CollectType", ddlCollectType.SelectedValue);
                }

                //获取检测项信息
                BindCollectItemInfo(para);
                if (wgCollectCon.Rows.Count>0)
                {
                    wgCollectCon.DisplayLayout.ActiveRow = wgCollectCon.Rows[0];
                    wgCollectCon.Rows[0].Selected = true;
                    wgCollectCon.Rows[0].Activated = true;
                    activeRow(wgCollectCon.Rows[0]);
                }

                DataTable dtSave = bll.GetDataCollectInfo(para);
                if (dtSave.Rows.Count>0)
                {
                    if (!string.IsNullOrEmpty(dtSave.Rows[0]["Notes"].ToString()))
                    {
                        txtNotes.Text = dtSave.Rows[0]["Notes"].ToString();
                    }


                    //if (!string.IsNullOrEmpty(dtSave.Rows[0]["DocumentName"].ToString()))
                    //{
                    //    //fileUpload.FileName(dtSave.Rows[0]["DocumentName"].ToString()) ;
                    //    //fileUpload.
                    //}

                }

                //绑定产品序号
                if (txtContainerID.Text !="")
                {
                    if (GetProductNo(txtContainerID.Text).Rows.Count > 0)
                    {
                        wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("productno").Hidden = false;
                    }
                }
            }
        }

    }

    protected Dictionary<string, string> SearchCondition()
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        //跟踪卡ID
        if (!string.IsNullOrEmpty(txtContainerID.Text))
        {
            para.Add("ContainerID", txtContainerID.Text);
        }

        //工序ID
        if (!string.IsNullOrEmpty(txtSpecID.Text))
        {
            para.Add("SpecID", txtSpecID.Text);
        }

        //图号ID
        if (!string.IsNullOrEmpty(txtProductID.Text))
        {
            para.Add("ProductID", txtProductID.Text);
        }

        //工艺规程ID
        if (!string.IsNullOrEmpty(txtWorkflowID.Text))
        {
            para.Add("WorkflowID", txtWorkflowID.Text);
        }

        //采集类型
        if (ddlCollectType.Items.Count > 0)
        {
            para.Add("CollectType", ddlCollectType.SelectedValue);
        }

        if (!string.IsNullOrEmpty(txtCheckItemInfoID.Text))
        {
            para.Add("CheckItemInfoID", txtCheckItemInfoID.Text);
        }


        return para;
    }

    /// <summary>
    /// 界面控件赋值
    /// </summary>
    protected void BindDataToControls(Dictionary<string, string> para)
    {
        try
        {
            //工作令号
            if (para.ContainsKey("ProcessNo"))
            {
                txtDispProcessNo.Text =para["ProcessNo"];
            }

            //作业令号
            if (para.ContainsKey("OprNo"))
            {
                txtDispOprNo.Text = para["OprNo"];
            }

            //批次号
            if (para.ContainsKey("ContainerName"))
            {
                txtDispContainerName.Text = para["ContainerName"];
            }

            //生产任务
            if (para.ContainsKey("MfgorderName"))
            {
                txtMfgorderName.Text = para["MfgorderName"];
            }

            //图号
            if (para.ContainsKey("ProductName"))
            {
                txtDispProductName.Text  = para["ProductName"];
            }

            //名称
            if (para.ContainsKey("Description"))
            {
                txtDispDescription.Text = para["Description"];
            }

            //批次数量
            if (para.ContainsKey("ConQty"))
            {
                txtConQty.Text  = para["ConQty"];
            }

            //计划开始时间
            if (para.ContainsKey("StartTime"))
            {
                txtStartTime.Text  = para["StartTime"];
            }

            //计划结束时间
            if (para.ContainsKey("EndTime"))
            {
                txtEndTime.Text = para["EndTime"];
            }

            //工序
            if (para.ContainsKey("SpecDisName"))
            {
                txtSpecDisName.Text = para["SpecDisName"];
            }

 

            //跟踪卡ID
            if (para.ContainsKey("ContainerID"))
            {
                txtContainerID.Text = para["ContainerID"];
            }

            //图号ID
            if (para.ContainsKey("ProductID"))
            {
                txtProductID.Text = para["ProductID"];
            }

            //工艺规程ID
            if (para.ContainsKey("WorkflowID"))
            {
                txtWorkflowID.Text = para["WorkflowID"];
            }


            //工序ID
            if (para.ContainsKey("SpecID"))
            {
                txtSpecID.Text = para["SpecID"];
            }

            // 操作类型 工作/检验
            if (para.ContainsKey("Type"))
            {
                hdType.Value = para["Type"];
            }

            //
        
            wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("checkvalue").Hidden = false;
            wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("checkvalue1").Hidden = false;
            wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("checkvalue").AllowUpdate = AllowUpdate.No;
            wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("checkvalue1").AllowUpdate = AllowUpdate.No;
            if (hdType.Value == "Check")
            {
                wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("checkvalue1").AllowUpdate = AllowUpdate.Yes;
            }
            else
            {
                wgRealValue.DisplayLayout.Bands[0].Columns.FromKey("checkvalue").AllowUpdate = AllowUpdate.Yes;
            }
            BindCollectType();
        }
        catch (Exception Ex)
        {

            ShowStatusMessage(Ex.Message, false);
        }
    }

    /// <summary>
    /// 绑定检测项信息
    /// </summary>
    /// <param name="para"></param>
    protected void BindCollectItemInfo(Dictionary<string, string> para)
    {
        try {
            DataTable dt = bll.GetCheckitemInfo(para);
            dt.Columns.Add("CheckitemDis");
            DataTable dtNew = new DataTable();
            if (dt.Rows.Count>0)
            {
                for (int i=0;i<dt.Rows.Count;i++)
                {
                    string strCheckitem = dt.Rows[i]["checkitem"].ToString();
                    string strCheckitemDis = string.Empty;
                    //ParseCode(strCheckitem, ref dtNew, ref strCheckitemDis);
                    ParseCode(strCheckitem, ref strCheckitemDis);
                    dt.Rows[i]["CheckitemDis"] = strCheckitemDis;
                }
            }
            wgCollectCon.DataSource = dt;
            wgCollectCon.DataBind();
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message,false);
        }
    }

    /// <summary>
    /// 绑定采集类型
    /// </summary>
    protected void BindCollectType()
    {

        ListItem item = new ListItem();
        item =new ListItem("关键工序" , "1");
        ddlCollectType.Items.Add(item);

        item= new ListItem("首件检验", "2");
        ddlCollectType.Items.Add(item);

        item  = new ListItem("工序检验", "3");
        ddlCollectType.Items.Add(item);

        item = new ListItem("入厂检验", "4");
        ddlCollectType.Items.Add(item);


    }

    /// <summary>
    /// 根据采集类型获取检测项信息
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCollectType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //保存Grid界面信息
            SaveGridValue();
            Dictionary<string, string> para = SearchCondition();
            BindCollectItemInfo(para);
            if (wgCollectCon.Rows.Count > 0)
            {
                //wgCollectCon.DisplayLayout.ActiveRow = wgCollectCon.Rows[0];
                //wgCollectCon.Rows[0].Selected = true;
                //wgCollectCon.Rows[0].Activated = true;
                //activeRow(wgCollectCon.Rows[0]);
            }
            wgRealValue.Rows.Clear();
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    /// <summary>
    /// 绑定产品序号
    /// </summary>
    /// <param name="strContainerID"></param>
    protected DataTable GetProductNo(string  strContainerID)
    {
    
        DataTable dtProductNo = common.GetProductNo(strContainerID);
        return dtProductNo;
    }

    /// <summary>
    /// 选中已保存的质量记录信息
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void wgCollectCon_ActiveRowChange(object sender, RowEventArgs e)
    {
        try
        {

            activeRow(e.Row);
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 行点击事件
    /// </summary>
    /// <param name="row"></param>
    void activeRow(UltraGridRow row)
    {
        //将Grid界面的数据保存至Session中
        SaveGridValue();

        //检测项表ID
        txtCheckItemInfoID.Text = string.Empty;
        if (row.Cells.FromKey("checkiteminfoid").Value!=null)
        {
            txtCheckItemInfoID.Text = row.Cells.FromKey("checkiteminfoid").Value.ToString();
        }
        txtCollectType.Text = string.Empty;
        if (row.Cells.FromKey("CollectType").Value != null)
        {
            txtCollectType.Text = row.Cells.FromKey("CollectType").Value.ToString();
        }
        //获取已保存的数据采集信息
        GetDataCollectInfo();


    }

    //将Grid界面的数据保存至Session中
    void SaveGridValue()
    {
        DataTable dtSave = new DataTable();
        dtSave.Columns.Add("ID");//唯一标识
        dtSave.Columns.Add("DataCollectInfoID");//数据采集主信息表ID
        dtSave.Columns.Add("CheckItemID");//检测项ID
        dtSave.Columns.Add("ProductNo");//产品序号
        dtSave.Columns.Add("CollectType");//采集类型
        dtSave.Columns.Add("CollectQty");//采集数量
        dtSave.Columns.Add("CheckValue");//实测值
        dtSave.Columns.Add("CheckValue1");//实测值(检验)
        dtSave.Columns.Add("Notes");//备注
        dtSave.Columns.Add("SequenceNum");//顺序号
        dtSave.Columns.Add("CheckItemIDandType");
        if (wgRealValue.Rows.Count>0)
        {
            for (int i=0;i<wgRealValue.Rows.Count;i++)
            {
                if (hdType.Value == "Check")
                {
                    if (!string.IsNullOrEmpty(wgRealValue.Rows[i].Cells.FromKey("CheckValue1").Text.ToString()))
                    {
                        dtSave.Rows.Add();
                        int intAddRow = dtSave.Rows.Count - 1;
                        dtSave.Rows[intAddRow]["ID"] = "";
                        dtSave.Rows[intAddRow]["DataCollectInfoID"] = "";
                        dtSave.Rows[intAddRow]["CheckItemID"] = txtCheckItemInfoID.Text;
                        dtSave.Rows[intAddRow]["ProductNo"] = wgRealValue.Rows[i].Cells.FromKey("ProductNo").Value;
                        dtSave.Rows[intAddRow]["CollectType"] = txtCollectType.Text;
                        dtSave.Rows[intAddRow]["CollectQty"] = wgRealValue.Rows[i].Cells.FromKey("CollectQty").Value;
                        dtSave.Rows[intAddRow]["CheckValue"] = wgRealValue.Rows[i].Cells.FromKey("CheckValue").Value;
                        dtSave.Rows[intAddRow]["CheckValue1"] = wgRealValue.Rows[i].Cells.FromKey("CheckValue1").Value;
                        dtSave.Rows[intAddRow]["Notes"] = "";
                        dtSave.Rows[intAddRow]["CheckItemIDandType"] = txtCheckItemInfoID.Text+txtCollectType.Text;
                        dtSave.Rows[intAddRow]["SequenceNum"] = i + 1;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(wgRealValue.Rows[i].Cells.FromKey("CheckValue").Text.ToString()))
                    {
                        dtSave.Rows.Add();
                        int intAddRow = dtSave.Rows.Count - 1;
                        dtSave.Rows[intAddRow]["ID"] = "";
                        dtSave.Rows[intAddRow]["DataCollectInfoID"] = "";
                        dtSave.Rows[intAddRow]["CheckItemID"] = txtCheckItemInfoID.Text;
                        dtSave.Rows[intAddRow]["ProductNo"] = wgRealValue.Rows[i].Cells.FromKey("ProductNo").Value;
                        dtSave.Rows[intAddRow]["CollectType"] = txtCollectType.Text;
                        dtSave.Rows[intAddRow]["CollectQty"] = wgRealValue.Rows[i].Cells.FromKey("CollectQty").Value;
                        dtSave.Rows[intAddRow]["CheckValue"] = wgRealValue.Rows[i].Cells.FromKey("CheckValue").Value;
                        dtSave.Rows[intAddRow]["CheckValue1"] = wgRealValue.Rows[i].Cells.FromKey("CheckValue1").Value;
                        dtSave.Rows[intAddRow]["Notes"] = "";
                        dtSave.Rows[intAddRow]["CheckItemIDandType"] = txtCheckItemInfoID.Text + txtCollectType.Text;
                        dtSave.Rows[intAddRow]["SequenceNum"] = i + 1;
                    }
                }

             

            }
        }

        if (Session["FinalSaveInfo"] != null)
        {
            DataTable dt = (DataTable)Session["FinalSaveInfo"];

            DataView dv = dt.DefaultView; //CheckItemIDandType
            string strFilter = txtCheckItemInfoID.Text + txtCollectType.Text;
            dv.RowFilter = "CheckItemIDandType <> '"+strFilter+"'";
            DataTable newTb = dv.ToTable();


            if (dtSave.Rows.Count>0)
            {
                for (int i = 0; i < dtSave.Rows.Count; i++)
                {
                    newTb.Rows.Add();
                    int intAddRow = newTb.Rows.Count - 1;
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["ID"].ToString()))
                    {
                        newTb.Rows[intAddRow]["ID"] = dtSave.Rows[i]["ID"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["DataCollectInfoID"].ToString()))
                    {
                        newTb.Rows[intAddRow]["DataCollectInfoID"] = dtSave.Rows[i]["DataCollectInfoID"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["CheckItemID"].ToString()))
                    {
                        newTb.Rows[intAddRow]["CheckItemID"] = dtSave.Rows[i]["CheckItemID"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["ProductNo"].ToString()))
                    {
                        newTb.Rows[intAddRow]["ProductNo"] = dtSave.Rows[i]["ProductNo"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["CollectType"].ToString()))
                    {
                        newTb.Rows[intAddRow]["CollectType"] = dtSave.Rows[i]["CollectType"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["CollectQty"].ToString()))
                    {
                        newTb.Rows[intAddRow]["CollectQty"] = dtSave.Rows[i]["CollectQty"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["CheckValue"].ToString()))
                    {
                        newTb.Rows[intAddRow]["CheckValue"] = dtSave.Rows[i]["CheckValue"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["CheckValue1"].ToString()))
                    {
                        newTb.Rows[intAddRow]["CheckValue1"] = dtSave.Rows[i]["CheckValue1"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["Notes"].ToString()))
                    {
                        newTb.Rows[intAddRow]["Notes"] = dtSave.Rows[i]["Notes"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["CheckItemIDandType"].ToString()))
                    {
                        newTb.Rows[intAddRow]["CheckItemIDandType"] = dtSave.Rows[i]["CheckItemIDandType"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dtSave.Rows[i]["SequenceNum"].ToString()))
                    {
                        newTb.Rows[intAddRow]["SequenceNum"] = dtSave.Rows[i]["SequenceNum"].ToString();
                    }

                }

            }
            Session["FinalSaveInfo"] = null;
            if (newTb.Rows.Count>0)
            {
                Session["FinalSaveInfo"] = newTb;
            }
    
        }
        else
        {
            if (dtSave.Rows.Count>0)
            {
                Session["FinalSaveInfo"] = dtSave;
            }
         
        }
    }

    //获取已保存的数据采集信息
    protected void GetDataCollectInfo()
    {
        try
        {
            wgRealValue.Rows.Clear();
            DataTable dtShow = new DataTable();
            dtShow.Columns.Add("productno");
            dtShow.Columns.Add("collectqty");
            dtShow.Columns.Add("checkvalue");
            dtShow.Columns.Add("checkvalue1");

            if (Session["FinalSaveInfo"] != null)
            {
                DataTable dtMain = (DataTable)Session["FinalSaveInfo"];
                DataTable newTb = dtMain.Clone();
                for (int i = 0; i <dtMain.Rows.Count; i++)
                {
                    string strCheckItemID = string.Empty;
                    if (!string.IsNullOrEmpty(dtMain.Rows[i]["CheckItemID"].ToString()))
                    {
                        strCheckItemID = dtMain.Rows[i]["CheckItemID"].ToString();
                    }

                    string strCollectType = string.Empty;
                    if (!string.IsNullOrEmpty(dtMain.Rows[i]["CollectType"].ToString()))
                    {
                        strCollectType = dtMain.Rows[i]["CollectType"].ToString();
                    }
                    if (strCheckItemID == txtCheckItemInfoID.Text & strCollectType==txtCollectType.Text)
                    {
                        newTb.Rows.Add();
                        int intAddRow = newTb.Rows.Count - 1;
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["ID"].ToString()))
                        {
                            newTb.Rows[intAddRow]["ID"] = dtMain.Rows[i]["ID"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["DataCollectInfoID"].ToString()))
                        {
                            newTb.Rows[intAddRow]["DataCollectInfoID"] = dtMain.Rows[i]["DataCollectInfoID"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["CheckItemID"].ToString()))
                        {
                            newTb.Rows[intAddRow]["CheckItemID"] = dtMain.Rows[i]["CheckItemID"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["ProductNo"].ToString()))
                        {
                            newTb.Rows[intAddRow]["ProductNo"] = dtMain.Rows[i]["ProductNo"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["CollectType"].ToString()))
                        {
                            newTb.Rows[intAddRow]["CollectType"] = dtMain.Rows[i]["CollectType"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["CollectQty"].ToString()))
                        {
                            newTb.Rows[intAddRow]["CollectQty"] = dtMain.Rows[i]["CollectQty"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["CheckValue"].ToString()))
                        {
                            newTb.Rows[intAddRow]["CheckValue"] = dtMain.Rows[i]["CheckValue"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["CheckValue1"].ToString()))
                        {
                            newTb.Rows[intAddRow]["CheckValue1"] = dtMain.Rows[i]["CheckValue1"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["Notes"].ToString()))
                        {
                            newTb.Rows[intAddRow]["Notes"] = dtMain.Rows[i]["Notes"].ToString();
                        }
                        if (!string.IsNullOrEmpty(dtMain.Rows[i]["SequenceNum"].ToString()))
                        {
                            newTb.Rows[intAddRow]["SequenceNum"] = dtMain.Rows[i]["SequenceNum"].ToString();
                        }
                        
                    }
                }
                //dt.AcceptChanges();
                //DataRow[] foundRow;
                //foundRow = dt.Select("CheckItemID = '" + txtCheckItemInfoID.Text + "'", "");
                //DataTable newTb = dt.Clone();
                //foreach (DataRow row in foundRow)
                //{
                //    newTb.Rows.Remove(row);
                //}

                foreach (DataRow row in newTb.Rows)
                {
                    DataRow dr = dtShow.NewRow();
                    dr["productno"] = row["productno"];
                    dr["collectqty"] = row["collectqty"];
                    dr["checkvalue"] = row["checkvalue"];
                    dr["checkvalue1"] = row["checkvalue1"];
                    dtShow.Rows.Add(dr);
                }
            }
            if (dtShow.Rows.Count==0)
            {
                Dictionary<string, string> para = SearchCondition();
                DataTable dtV = new DataTable();
                DataTable dtCol = bll.GetDataCollectInfo(para);
                if (dtCol.Rows.Count > 0)
                {
                    foreach (DataRow row in dtCol.Rows)
                    {
                        DataRow dr = dtShow.NewRow();

                        dr["productno"] = row["productno"];
                        dr["collectqty"] = row["collectqty"];
                        string strCheckitemDis = string.Empty;
                        //ParseCode(strCheckitem, ref dtNew, ref strCheckitemDis);
                        ParseCode(row["checkvalue"].ToString(), ref strCheckitemDis);
                        dr["checkvalue"] = strCheckitemDis;

                        string strCheckitemDis1 = string.Empty;
                        //ParseCode(strCheckitem, ref dtNew, ref strCheckitemDis);
                        ParseCode(row["checkvalue1"].ToString(), ref strCheckitemDis1);
                        dr["checkvalue1"] = strCheckitemDis1;
                        dtShow.Rows.Add(dr);
                    }

                }
                else
                {

                    //查询跟踪卡产品序号
                    DataTable dtProductNo = GetProductNo(txtContainerID.Text);
                    if (dtProductNo.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtProductNo.Rows)
                        {
                            DataRow dr = dtShow.NewRow();

                            dr["productno"] = row["productno"];
                            dr["collectqty"] = "1";
                            dr["checkvalue"] = "";
                            dr["checkvalue1"] = "";
                            dtShow.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataRow dr = dtShow.NewRow();

                        dr["productno"] = "";
                        dr["collectqty"] = txtConQty.Text; ;
                        dr["checkvalue"] = "";
                        dr["checkvalue1"] = "";
                        dtShow.Rows.Add(dr);
                    }
                }
            }
            wgRealValue.DataSource = dtShow;
            wgRealValue.DataBind();
        }
        catch(Exception e)
        {
            ShowStatusMessage(e.Message,false);
        }
    }

    /// <summary>
    /// 确定按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            //保存Grid界面信息
            SaveGridValue();
            if (Session["FinalSaveInfo"] == null)
            {
                ShowStatusMessage("没有要保存的信息！", true);
                return;
            }

            //判断是否已经保存数据
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            Dictionary<string, string> para1 = SearchCondition();
            para1.Remove("CheckItemInfoID");
            para1.Remove("CollectType");
            DataTable dtSave = bll.GetDataCollectInfo(para1);
            //日志对象 add;Wangjh 20210121
            MESAuditLog ml = new MESAuditLog();
            //主表ID
            Dictionary<string, object> para = new Dictionary<string, object>();
            DataSet ds = new DataSet();
            string strMainID = string.Empty;
            string strMessage = string.Empty;
            if (dtSave.Rows.Count > 0)
            {
                ml.OperationType = 1; ml.Description = "数据采集实测值:" + txtSpecDisName.Text + ",实测值修改成功";
                //旧的文件名称
                string strOldDocumentName = string.Empty;
                strMainID = dtSave.Rows[0]["id"].ToString(); 
                  DataTable dtUpdate = new DataTable();
                dtUpdate.Columns.Add("TableName");
                dtUpdate.Columns.Add("ID");
                if (hdType.Value == "Check")
                {
                    strOldDocumentName = dtSave.Rows[0]["CheckDocument"].ToString();
                    dtUpdate.Columns.Add("updateCheckerID");//更新人员ID
                    dtUpdate.Columns.Add("updateCheckTime");//更新时间
                }
                else
                {
                 strOldDocumentName=   dtSave.Rows[0]["DocumentName"].ToString();
                    dtUpdate.Columns.Add("updateEmployeeID");//更新人员ID
                    dtUpdate.Columns.Add("updateTime");//更新时间
                }

                dtUpdate.Columns.Add("Notes");//备注
               

                dtUpdate.Rows.Add();
                int intUpdateRow = dtUpdate.Rows.Count - 1;
                dtUpdate.Rows[intUpdateRow]["TableName"] = "DataCollectInfo";
                dtUpdate.Rows[intUpdateRow]["ID"] =strMainID + "㊣String";//
                if (hdType.Value == "Check")
                {
                    dtUpdate.Rows[intUpdateRow]["updateCheckerID"] = userInfo["EmployeeID"] + "㊣String";
                    dtUpdate.Rows[intUpdateRow]["updateCheckTime"] = System.DateTime.Now + "㊣Date";
                }
                else
                {
                    dtUpdate.Rows[intUpdateRow]["updateEmployeeID"] = userInfo["EmployeeID"] + "㊣String";
                    dtUpdate.Rows[intUpdateRow]["updateTime"] = System.DateTime.Now + "㊣Date";
                }

                dtUpdate.Rows[intUpdateRow]["Notes"] = txtNotes.Text + "㊣String";
                if (!string.IsNullOrEmpty(fileUpload.FileName))
                {
                    if (hdType.Value == "Check")
                    {
                        dtUpdate.Columns.Add("CheckDocument");//文档名称
                        dtUpdate.Rows[intUpdateRow]["CheckDocument"] = fileUpload.FileName + "㊣String";
                    }
                    else
                    {
                        dtUpdate.Columns.Add("DocumentName");//文档名称
                        dtUpdate.Rows[intUpdateRow]["DocumentName"] = fileUpload.FileName + "㊣String";
                    }
           
                }
             

                int iResult = bll.UpdateDataToDatabase(dtUpdate, out strMessage);
                if (iResult == -1)
                {
                    ShowStatusMessage(strMessage, false);
                    return;
                }
                //删除旧文档
                string strFilePath = strOldDocumentName.Trim();
                String strDocPath = Server.MapPath(Request.ApplicationPath); // ConfigurationManager.AppSettings("ImportExcelPatch")

                String strFileName  = strFilePath.Substring(strFilePath.LastIndexOf("\\") + 1);

                string strFileType = strFileName.Substring(strFileName.LastIndexOf(".") + 1);

                string strSavePath = string.Empty;
                if (!string.IsNullOrEmpty(strFilePath))
                {
                    //strSavePath = strDocPath + "\\DataCollectDoc\\" + strFileName;

                    string strFullName = strDocPath + "\\DataCollectDoc\\" + strFileName;
                    strSavePath = strDocPath + "\\DataCollectDoc\\";

                    DirectoryInfo dir = new DirectoryInfo(strSavePath);
                    FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
                    foreach (FileSystemInfo i in fileinfo)
                    {
                        //if (i is DirectoryInfo)            //判断是否文件夹
                        //{
                        //    DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                        //    subdir.Delete(true);          //删除子目录和文件
                        //}
                        //else
                        //{
                        //    //如果 使用了 streamreader 在删除前 必须先关闭流 ，否则无法删除 sr.close();
                        //    File.Delete(i.FullName);      //删除指定文件
                        //}

                        if (i.FullName== strFullName)
                        {
                            File.Delete(i.FullName);
                        }
                    }
                }
             
            }
            else
            {
                ml.OperationType = 0; ml.Description = "数据采集实测值:" + txtSpecDisName.Text + ",实测值添加成功";

                DataTable dtMain = new DataTable();
                dtMain.Columns.Add("ID");//唯一标识
                dtMain.Columns.Add("ContainerID");//生产批次号
                dtMain.Columns.Add("SpecID");//工序ID
                dtMain.Columns.Add("EmployeeID");//操作者ID
                dtMain.Columns.Add("CollectDate");//采集时间
                dtMain.Columns.Add("Notes");//备注
                dtMain.Columns.Add("ProductID");//产品/图号ID
                dtMain.Columns.Add("WorkflowID");//工艺路线ID
                dtMain.Columns.Add("ContainerName");//生产批次号
                dtMain.Columns.Add("ProductName");//产品/图号
                dtMain.Columns.Add("Description");//名称
                dtMain.Columns.Add("ProcessNo");//工作令号
                dtMain.Columns.Add("OprNo");//作业令号
                dtMain.Columns.Add("MfgorderName");//生产任务
                dtMain.Columns.Add("PlanStartDate");//计划开始时间
                dtMain.Columns.Add("PlanEndDate");//计划结束时间
                dtMain.Columns.Add("DocumentName");//文档名称
                dtMain.Columns.Add("CheckDocument");//检验关联文档名称
                dtMain.Columns.Add("updateEmployeeID");//更新人员ID
                dtMain.Columns.Add("updateTime");//更新时间

                dtMain.Columns.Add("updateCheckerID");//更新人员ID(检验)
                dtMain.Columns.Add("updateCheckTime");//更新时间(检验)
                strMainID = Guid.NewGuid().ToString();
                dtMain.Rows.Add();
                int intAddRow = dtMain.Rows.Count - 1;
                dtMain.Rows[intAddRow]["ID"] =strMainID + "㊣String";
                dtMain.Rows[intAddRow]["ContainerID"] =txtContainerID.Text + "㊣String";
                dtMain.Rows[intAddRow]["SpecID"]=txtSpecID.Text + "㊣String";
                dtMain.Rows[intAddRow]["EmployeeID"]= userInfo["EmployeeID"] + "㊣String";
                dtMain.Rows[intAddRow]["CollectDate"]=System.DateTime.Now + "㊣Date";
                dtMain.Rows[intAddRow]["Notes"]=txtNotes.Text + "㊣String";
                dtMain.Rows[intAddRow]["ProductID"]=txtProductID.Text + "㊣String";
                dtMain.Rows[intAddRow]["WorkflowID"]=txtWorkflowID.Text + "㊣String";
                dtMain.Rows[intAddRow]["ContainerName"]=txtDispContainerName.Text + "㊣String";
                dtMain.Rows[intAddRow]["ProductName"]=txtDispProductName.Text + "㊣String";
                dtMain.Rows[intAddRow]["Description"]=txtDispDescription.Text + "㊣String";
                dtMain.Rows[intAddRow]["ProcessNo"]=txtDispProcessNo.Text + "㊣String";
                dtMain.Rows[intAddRow]["OprNo"]=txtDispOprNo.Text + "㊣String";
                dtMain.Rows[intAddRow]["MfgorderName"]=txtMfgorderName.Text + "㊣String";
                dtMain.Rows[intAddRow]["PlanStartDate"] = txtStartTime.Text + "㊣Date";
                dtMain.Rows[intAddRow]["PlanEndDate"]= txtEndTime.Text + "㊣Date";
                if (hdType.Value == "Check")
                {
                    dtMain.Rows[intAddRow]["CheckDocument"] = fileUpload.FileName + "㊣String";
                    dtMain.Rows[intAddRow]["DocumentName"] = ""+ "㊣String";
                }
                else
                {
                    dtMain.Rows[intAddRow]["CheckDocument"] = "" + "㊣String";
                    dtMain.Rows[intAddRow]["DocumentName"] = fileUpload.FileName + "㊣String";
                }
  
                dtMain.Rows[intAddRow]["updateEmployeeID"] ="" + "㊣String";
                dtMain.Rows[intAddRow]["updateTime"]="" + "㊣Date";
                dtMain.Rows[intAddRow]["updateCheckerID"] = "" + "㊣String";
                dtMain.Rows[intAddRow]["updateCheckTime"] = "" + "㊣Date";

                dtMain.Rows.Add();
                dtMain.Rows[dtMain.Rows.Count - 1]["id"] = "" + "㊣String";
                ds.Tables.Add(dtMain);
                ds.Tables[ds.Tables.Count - 1].TableName = "DataCollectInfo" + "㊣id";
            }
            string strCheckItemidList = string.Empty;
            DataTable dtMine = new DataTable();
            dtMine.Columns.Add("ID");//唯一标识
            dtMine.Columns.Add("DataCollectInfoID");//数据采集主信息表ID
            dtMine.Columns.Add("CheckItemID");//检测项ID
            dtMine.Columns.Add("ProductNo");//产品序号
            dtMine.Columns.Add("CollectType");//采集类型
            dtMine.Columns.Add("CollectQty");//采集数量
            dtMine.Columns.Add("CheckValue");//实测值
            dtMine.Columns.Add("CheckValue1");//实测值
            dtMine.Columns.Add("Notes");//备注 
            dtMine.Columns.Add("SequenceNum"); 
            if (Session["FinalSaveInfo"] != null)
            {
                DataTable dt = (DataTable)Session["FinalSaveInfo"];
                if (dt.Rows.Count>0)
                {
                    for (int i=0;i<dt.Rows.Count;i++)
                    {
                        dtMine.Rows.Add();
                        int intAddRow = dtMine.Rows.Count - 1;
                        dtMine.Rows[intAddRow]["ID"] = Guid.NewGuid().ToString() + "㊣String";
                        dtMine.Rows[intAddRow]["DataCollectInfoID"] = strMainID + "㊣String";
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CheckItemIDandType"].ToString()))
                        {
                            if (strCheckItemidList.Contains("'"+ dt.Rows[intAddRow]["CheckItemIDandType"] +"'")==false)
                            {
                                strCheckItemidList += "'" + dt.Rows[intAddRow]["CheckItemIDandType"] + "',";
                            }
                            dtMine.Rows[intAddRow]["CheckItemID"] = dt.Rows[i]["CheckItemID"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["ProductNo"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["ProductNo"] = dt.Rows[i]["ProductNo"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CollectType"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CollectType"] = dt.Rows[i]["CollectType"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CollectQty"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CollectQty"] = dt.Rows[i]["CollectQty"].ToString() + "㊣Integer";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CheckValue"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CheckValue"] = GetCode(dt.Rows[i]["CheckValue"].ToString()) + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CheckValue1"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CheckValue1"] = GetCode(dt.Rows[i]["CheckValue1"].ToString()) + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["Notes"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["Notes"] = dt.Rows[i]["Notes"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["SequenceNum"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["SequenceNum"] = dt.Rows[i]["SequenceNum"].ToString() + "㊣Integer";
                        }
                    }
                }
            }

            if (dtSave.Rows.Count>0 & strCheckItemidList!="")
            {
                strCheckItemidList = strCheckItemidList.TrimEnd(',');
                DataView dv = dtSave.DefaultView;
                dv.RowFilter = "CheckItemIDandType NOT IN (" + strCheckItemidList + ")";
                DataTable dt = dv.ToTable();
                if (dt.Rows.Count>0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        dtMine.Rows.Add();
                        int intAddRow = dtMine.Rows.Count - 1;
                        dtMine.Rows[intAddRow]["ID"] = Guid.NewGuid().ToString()  + "㊣String";
                        dtMine.Rows[intAddRow]["DataCollectInfoID"] = strMainID + "㊣String";
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CheckItemID"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CheckItemID"] = dt.Rows[i]["CheckItemID"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["ProductNo"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["ProductNo"] = dt.Rows[i]["ProductNo"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CollectType"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CollectType"] = dt.Rows[i]["CollectType"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CollectQty"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CollectQty"] = dt.Rows[i]["CollectQty"].ToString() + "㊣Integer";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CheckValue"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CheckValue"] = GetCode(dt.Rows[i]["CheckValue"].ToString()) + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["CheckValue1"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["CheckValue1"] = GetCode(dt.Rows[i]["CheckValue1"].ToString()) + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["Notes"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["Notes"] = dt.Rows[i]["Notes"].ToString() + "㊣String";
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[i]["SequenceNum"].ToString()))
                        {
                            dtMine.Rows[intAddRow]["SequenceNum"] = dt.Rows[i]["SequenceNum"].ToString() + "㊣Integer";
                        }
                    }
                }
            }
            
            dtMine.Rows.Add();
            dtMine.Rows[dtMine.Rows.Count - 1]["DataCollectInfoID"] = strMainID + "㊣String";

            ds.Tables.Add(dtMine);
            ds.Tables[ds.Tables.Count - 1].TableName = "DataCollectDetailInfo" + "㊣DataCollectInfoID";

            //保存数据
            para.Add("dsData", ds);

            bool result = bll.SaveDataToDatabaseNew(para, out strMessage);
            if (result)
            {
                //保存文档
                string strFilePath = (fileUpload.FileName).Trim();
                String strDocPath = Server.MapPath(Request.ApplicationPath); // ConfigurationManager.AppSettings("ImportExcelPatch")

                String strFileName = strFilePath.Substring(strFilePath.LastIndexOf("\\") + 1);

                string strFileType = strFileName.Substring(strFileName.LastIndexOf(".") + 1);

                string strSavePath = string.Empty;
                if (!string.IsNullOrEmpty(strFilePath))
                {
                    strSavePath = strDocPath + "\\DataCollectDoc\\" + strFileName;
                    if (File.Exists(strSavePath)==false)
                    {
                        if (Request.Files[0].ContentLength > 0)
                        {
                            Request.Files[0].SaveAs(strSavePath);
                        }
                        else
                        {
                            ShowStatusMessage("文档保存失败", result);
                            return;
                        }
                                
          
                    }
                }

                //保存日志
                ml.ParentID = strMainID; 
                ContainerExcuteLog(ml);
            }
            Session["FinalSaveInfo"] = null;
            ShowStatusMessage(strMessage, result);
            
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }
    }

    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }

    /// <summary>
    /// 验证是否是正整数
    /// </summary>
    /// <param name="inString"></param>
    /// <returns></returns>
    public static bool IsInt(string inString)
    {
        Regex regex = new Regex("^[0-9]*[1-9][0-9]*$");
        return regex.IsMatch(inString.Trim());
    }
 
    protected void btnClose_Click(object sender, EventArgs e)
    {
        int intRow = Convert.ToInt32(hdSelRowIndex.Value);
        int intCol = Convert.ToInt32(hdSelColIndex.Value);
        if (Session["ReturnValue"]!=null)
        {
            DataTable dt = new DataTable();
            string strCodeDis = "";
            //ParseCode((string)Session["ReturnValue"], ref dt, ref strCodeDis);
            ParseCode((string)Session["ReturnValue"],  ref strCodeDis);
            wgRealValue.Rows[intRow].Cells[intCol].Value = strCodeDis;
            Session["ReturnValue"] = null;
        }
         
    }

    //[System.Web.Services.WebMethod()]    public  string StrParseCode(string  str1)
    //{
    //    //    Dim pCode As New ParseCodeInfo

    //    return  ParseChildrenAttribute(str1.Trim());
    //}

    public void ParseCodeNew(String strPreviewCode, ref DataTable dtImageAllInfo, ref string strPreviewCodeDis)
    {
        try
        {
            dtImageAllInfo = new DataTable();
            dtImageAllInfo.Columns.Add("FileName");
            dtImageAllInfo.Columns.Add("HtmlCode");

            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath = webRootDir + ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    strPreviewCodeDis += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir + ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (dtImageAllInfo == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            dtImageAllInfo = dtImage;
                        }
                        else
                        {
                            dtImage = dtImageAllInfo;
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        dtImageAllInfo = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        strPreviewCodeDis += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                strPreviewCodeDis += strPreviewCode;
            }
        }
        catch (Exception myError)
        {
            ShowStatusMessage(myError.Message, false);
        }

    }

    public String GetCode(string strPreviewCodeDis)
    {
        String strHtmlCode, strHtml, strHtmlTemp;
        try
        {
            DataTable dtImage = new DataTable();
            int i, intStartFlag, intEndFlag;
            dtImage = (DataTable)Session["dtImage"];

            strHtml = strPreviewCodeDis;
            strHtmlCode = strHtml;

            intStartFlag = strHtmlCode.IndexOf("<img");
            if (intStartFlag == -1)
            {
                intStartFlag = strHtmlCode.IndexOf("<IMG");
            }
            if (intStartFlag > -1)
            {
                intEndFlag = strHtmlCode.IndexOf(">", intStartFlag);
                ;
            }
            else
            {
                intEndFlag = -1;
            }

            if (dtImage != null)
            {
                if (dtImage.Rows.Count > 0)
                {
                    while (intStartFlag > -1)
                    {
                        strHtmlTemp = strHtmlCode.Substring(intStartFlag, intEndFlag - intStartFlag + 1);
                        for (i = 0; i <= dtImage.Rows.Count - 1; i++)
                        {
                            if (strHtmlTemp.IndexOf(@"/" + dtImage.Rows[i][0]) > -1)
                            {
                                strHtmlCode = strHtmlCode.Replace(strHtmlTemp, (String)dtImage.Rows[i][1]);
                                break;
                            }

                        }

                        intStartFlag = strHtmlCode.IndexOf("<img", intStartFlag + 1);
                        if (intStartFlag == -1)
                        {
                            intStartFlag = strHtmlCode.IndexOf("<IMG", intStartFlag + 1);
                        }
                        if (intStartFlag > -1)
                        {
                            intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                            ;
                        }
                        else
                        {
                            intEndFlag = -1;
                        }
                        //intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                    }

                }

            }

            return strHtmlCode;
        }
        catch (Exception myError)
        {

            ShowStatusMessage(myError.Message, false);
            return "";
        }
    }

    public void ParseCode(String strPreviewCode, ref string strPreviewCodeDis)
    {
        try
        {
            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath = webRootDir + ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    strPreviewCodeDis += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir + ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (Session["dtImage"] == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            Session["dtImage"] = dtImage;
                        }
                        else
                        {
                            dtImage = (DataTable)Session["dtImage"];
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        Session["dtImage"] = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        strPreviewCodeDis += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                strPreviewCodeDis += strPreviewCode;
            }
        }
        catch (Exception myError)
        {

        }

    }


    #region 批次日志记录 add:Wangjh 20210121

    void ContainerExcuteLog(MESAuditLog ml)
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        ml.ContainerID = txtContainerID.Text;ml.ContainerName = txtDispContainerName.Text;
        ml.ParentName = parentName; ml.BusinessName = businessName;
        ml.CreateEmployeeID = userInfo["EmployeeID"];

        common.SaveMESAuditLog(ml);
    }

    #endregion
}