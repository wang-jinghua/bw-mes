﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESDataCollectPopupForm.aspx.cs"
  ValidateRequest="false" Inherits="uMESDataCollectPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>数据采集</title>
    <base target="_self" />
    <link href="../../styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
    <script src="../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.8.2.js" type="text/javascript"></script>
    <script type="text/javascript">

        function AfterChangeShow() {
            var CurrentGrid1 = igtbl_getGridById("wgRealValue");

            var CurrentCell = null;
            var sendValue = "";

            if (typeof (CurrentGrid1) != 'undefined') {
                CurrentCell = CurrentGrid1.getActiveCell();
            }
            if (typeof (CurrentCell) == 'undefined' || CurrentCell == null) {
                return false;
            }
            var CurrentColIndex = CurrentCell.Index;
            var CurrentRow = CurrentCell.Row;
            var CurRowIndex = CurrentRow.getIndex();
            document.getElementById("hdSelRowIndex").value = CurRowIndex;
            document.getElementById("hdSelColIndex").value = CurrentColIndex;
            sendValue = CurrentGrid1.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).getElement().innerHTML;
            var url = "uMESWLCharInputForm.aspx?sendValue=" + encodeURIComponent(sendValue) + "";
            var returnValue = window.showModalDialog(url, 'window', "dialogHeight:400px; dialogWidth:850px;center:Yes; resizable:NO; status:No;");
                        
            if (typeof (returnValue) != 'undefined') {
                document.getElementById("Button1").click();
                document.getElementById("txtDispProcessNo").value = returnValue;
                //这里请求的目的？且未找到对应的ashx add;Wangjh 20201117 增加return
                return;
                $.ajax({
                    type: "GET",
                    url: "ashx/DataCollect.ashx",
                    data: {
                        factoryid: returnValue
                    },
                    dataType: "text",
                    cashe: false,
                    success: function (data) {
                        debugger;
                        CurrentGrid1.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).setValue(data.d);
                    },
                    error: function (err) {
                        alert("Error:" + err);
                    }
                });
                //$.ajax({
                //    type: "Get",
                //    url: "uMESDataCollectPopupForm.aspx/StrParseCode",
                //    data: "{'str1':'" + returnValue + "'}",
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    success: function (data) {
                //        if (returnValue != "" && typeof (returnValue) != 'undefined') {

                //            alert(returnValue);
                //            //if (typeof (CurrentGrid1) != 'undefined' && btnValue == "1" && selectDg == "1") {
                //                CurrentGrid1.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).setValue(data.d);
                //            //}
                //            //if (typeof (CurrentGrid2) != 'undefined' && btnValue == "2" && selectDg == "2") {
                //            //    CurrentGrid2.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).setValue(data.d);
                //            //}
                //            //if (typeof (CurrentGrid3) != 'undefined' && btnValue == "3" && selectDg == "3") {
                //            //    CurrentGrid3.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).setValue(data.d);
                //            //}
                //            //if (typeof (CurrentGrid4) != 'undefined' && btnValue == "4" && selectDg == "4") {
                //            //    CurrentGrid4.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).setValue(data.d);
                //            //}
                //            //if (typeof (CurrentGrid5) != 'undefined' && btnValue == "5" && selectDg == "5") {
                //            //    CurrentGrid5.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).setValue(data.d);
                //            //}
                //            //if (typeof (CurrentGrid6) != 'undefined' && selectDg == "6") {
                //            //    CurrentGrid6.Rows.getRow(CurRowIndex).getCell(CurrentColIndex).setValue(data.d);
                //            //}
                //            //document.getElementById("Button1").click();

                //        }
                //        else {
                //            return false;
                //        }
                //    },
                //    error: function (err) {
                //        alert("Error:" + err);
                //    },
                //    complete: function (XMLHttpRequest, textStatus) {

                //        //                        var xml = window.XMLHttpRequest ? new window.XMLHttpRequest() : new ActiveXObject("Microsoft XMLHttp");
                //        //                        xml.abort();
                //        XMLHttpRequest = null;

                //    }

                //});


            }
        }
    </script>
</head>
<body Scroll = "no">
    <form id="form1" runat="server">
        
<%--        <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" RefreshTargetIDs="WebAsyncRefreshPanel2" Width="1064px">--%>
            <div>
                <table>
                    <tr style="height:1px;">
                        <td></td>
                    </tr>
                    <tr>
                        <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                            <div style="width: 100%">
                                <asp:Label ID="Label3" runat="server" Text=" 工作令号" Font-Bold="true" Font-Size="13pt" ForeColor="Black"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtDispProcessNo"  runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                            </div>
                        </td>
                        <td align="left" class="tdRightAndBottom" style=" display:none">
                            <div style="width: 100%;">
                                <asp:Label ID="Label7" runat="server" Text="作业令号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </td>
                        <td align="left" class="tdRightAndBottom">
                            <div style="width: 100%;">
                                <asp:Label ID="Label8" runat="server" Text="批次号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>         
                        </td> 
                        <td align="left" class="tdRightAndBottom">
                            <div style="width: 100%;">
                                <asp:Label ID="Label9" runat="server" Text="图号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </td>
                        <td align="left" class="tdRightAndBottom" colspan="2">
                            <div style="width: 100%;">
                                <asp:Label ID="Label10" runat="server" Text="名称" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="left" class="tdRightAndBottom">
                            <div style="width: 100%;">
                                <asp:Label ID="Label11" runat="server" Text="批次数量" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtConQty" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </td>
                        <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                            <div style="width: 100%">
                                <asp:Label ID="Label12" runat="server" Text="计划开始时间" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtStartTime" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </td>
                        <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                            <div style="width: 100%">
                                <asp:Label ID="Label1" runat="server" Text="计划结束时间" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtEndTime" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </td>
                        <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                            <div style="width: 100%">
                                <asp:Label ID="Label4" runat="server" Text="工序" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                            </div>
                            <asp:TextBox ID="txtSpecDisName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false" Font-Size="Medium"></asp:TextBox>
                        </td>
                        <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                            <div style="width: 100%">
                                <asp:Label ID="Label2" runat="server" Text="采集类型" Font-Bold="true" Font-Size="12pt"></asp:Label>
                            </div>
                                <asp:DropDownList ID="ddlCollectType" runat="server" Width="180px" Height="28px" AutoPostBack="true"
                                    style="font-size:16px;"  BackColor="White" OnSelectedIndexChanged="ddlCollectType_SelectedIndexChanged"></asp:DropDownList>                    
                        </td>

                    </tr>
                </table>
            </div>
            <div>
                <div  style="margin:5px;float:left">
                    <igtbl:UltraWebGrid ID="wgCollectCon" runat="server"  Height="229px" Width="170px" OnActiveRowChange="wgCollectCon_ActiveRowChange"  >
                        <Bands>
                            <igtbl:UltraGridBand>
                            <Columns>
                            <igtbl:UltraGridColumn Key="checkiteminfoname" Width="100px" BaseColumnName="checkiteminfoname" AllowGroupBy="No">
                                <Header Caption="检测项名称">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn> 
                           <igtbl:UltraGridColumn Key="CheckitemDis" Width="450px" BaseColumnName="CheckitemDis" AllowUpdate="Yes">
                                <Header Caption="检测项内容">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checkitem" Width="450px" BaseColumnName="checkitem" Hidden="true" >
                                <Header Caption="检测项内容">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checkiteminfoid" Width="450px" BaseColumnName="checkiteminfoid" Hidden="true">
                                <Header Caption="checkiteminfoid">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="CollectType" Width="450px" BaseColumnName="CollectType" Hidden="true">
                                <Header Caption="CollectType">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>       
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="Yes"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"   
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                            RowHeightDefault="100%">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="229px" Width="600px">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
                </div>
                <div style="margin:5px;float:left">
                    <igtbl:UltraWebGrid ID="wgRealValue" runat="server"  Height="250px" Width="300px">
                        <Bands>
                            <igtbl:UltraGridBand>
                            <Columns>
                            <igtbl:UltraGridColumn Key="productno" Width="80px" BaseColumnName="productno" AllowGroupBy="No">
                                <Header Caption="产品序号">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checkvalue" Width="140px" BaseColumnName="checkvalue" Hidden="false" AllowUpdate="Yes">
                                <Header Caption="实测值(工人)">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checkvalue1" Width="140px" BaseColumnName="checkvalue1" Hidden="false" AllowUpdate="Yes">
                                <Header Caption="实测值(检验)">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="collectqty" Width="150px" BaseColumnName="collectqty" Hidden="true" AllowGroupBy="No">
                                <Header Caption="数量">
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>       
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="None"
                            CellClickActionDefault="Edit" ViewType="OutlookGroupBy" ScrollBarView="both"  
                            RowHeightDefault="100%">

                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="250px" Width="300px">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
                   <div>
                        <input type="button" id="btnCharInput" runat="server" value="特殊字符" onclick="return AfterChangeShow();"
                                                                            /> 
                    </div>
                </div>
  
                <div style="clear:both"></div>
            </div>
            <div style="margin:5px;">
                <div style=" padding-top:5px; margin:5px;float:left">
                    <div>
                        <asp:Label ID="Label5" runat="server" Text="附件" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                    </div>
                    <div >
                        <asp:FileUpload ID="fileUpload" runat="server" Width="600px"  Height="20px" />                    
                    </div>
                </div>
                <div style="margin:5px;">
                    <div>
                        <asp:Label ID="Label14" runat="server" Text="备注" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" Height="30px" Width="300px" ></asp:TextBox>
                    </div>
                </div>

 
                <div style=" clear:both;"></div>
            </div>
    
            <div style="margin:5px;">
                <asp:Button ID="btnConfirm" runat="server" Text="确定" style="margin-right:20px" 
                            CssClass="searchButton" EnableTheming="True" OnClick="btnConfirm_Click"  />
                <asp:Button ID="btnClose" runat="server" Text="关闭" 
                            CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
            </div>
            <div  style="margin:5px;">
                <div style="font-size:12px; font-weight:bold;float:left; padding-top:5px;">状态信息：</div>
   
                <div>
                    <asp:Label ID="lStatusMessage" runat="server" style="font-size:12px;" Width="90%"></asp:Label>
                </div>
            </div>
            <div style=" display:none; visibility:hidden">
                <!--跟踪卡ID-->
                <asp:TextBox ID="txtContainerID" runat="server" class="stdTextBox" Height="100px" Width="680px" ></asp:TextBox>
                <!--工序ID-->
                <asp:TextBox ID="txtSpecID" runat="server" class="stdTextBox" Height="100px" Width="680px" ></asp:TextBox>
                <!--工艺规程编号 -->
                <asp:TextBox ID="txtWorkflowID" runat="server" class="stdTextBox" Height="100px" Width="680px" ></asp:TextBox>
                <!--图号ID-->
                <asp:TextBox ID="txtProductID" runat="server" class="stdTextBox" Height="100px" Width="680px" ></asp:TextBox>
                <!--生产任务-->
                <asp:TextBox ID="txtMfgorderName" runat="server" Visible="false"></asp:TextBox>
                <!--检测项表ID--> 
                  <asp:TextBox ID="txtCollectType" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtCheckItemInfoID" runat="server" Visible="false"></asp:TextBox>
                <asp:HiddenField ID="hdSelRowIndex" runat="server" />
                <asp:HiddenField ID="hdSelColIndex" runat="server" />
                <asp:Button ID="Button1" runat="server" Text="关闭" CssClass="searchButton" EnableTheming="True"  OnClick="btnClose_Click" />
                <asp:HiddenField ID="hdType" runat="server" />
            </div>
       <%-- </igmisc:WebAsyncRefreshPanel>--%>
    </form>
</body>
</html>
