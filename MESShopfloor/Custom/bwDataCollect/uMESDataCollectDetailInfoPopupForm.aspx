﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESDataCollectDetailInfoPopupForm.aspx.cs"
    ValidateRequest="false" Inherits="uMESDataCollectDetailInfoPopupForm" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>数据采集</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="DivNormalWS">
            <igtbl:UltraWebGrid ID="wgDataCollect" runat="server">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:UltraGridColumn Key="checkiteminfoname" Width="100px" BaseColumnName="checkiteminfoname" AllowGroupBy="No">
                                <Header Caption="检测项名称">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checkitem" Width="450px" BaseColumnName="checkitem" Hidden="false">
                                <Header Caption="检测项内容">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checktype" Width="80px" BaseColumnName="checktype" Hidden="false">
                                <Header Caption="检查类型">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="productno" Width="80px" BaseColumnName="productno" AllowGroupBy="No">
                                <Header Caption="产品序号">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checkvalue" Width="150px" BaseColumnName="checkvalue" Hidden="false" AllowUpdate="Yes">
                                <Header Caption="实测值(工人)">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="checkvalue1" Width="150px" BaseColumnName="checkvalue1" Hidden="false" AllowUpdate="Yes">
                                <Header Caption="实测值(检验)">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px" AllowRowNumberingDefault="Continuous">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="600px"
                        Width="1080px">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
        </div>
    </form>
</body>
</html>
