﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" CodeFile="uMESWLCharInputForm.aspx.cs" Inherits="forms_shopfloor_WLWorkReport_uMESWLCharInputForm" %>

<%@ Register Src="~/uMESCustomControls/ToleranceInput/wucToleranceInput.ascx" TagName="wucToleranceInput" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>特殊字符录入</title>
		<base target="_self" />
            <script language="javascript" type="text/javascript">
        function WinClose(result) {
            window.returnValue = result;
            window.close()
        }
    </script>

        <script type="text/javascript">
            function ReturnValue() {
                var returnvalue  = document.getElementById("code").value;

               //returnvalue= $("#code").val();
                window.returnValue = returnvalue;
                window.close();
            }
            
          
        </script>
        <script src="../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
<%--        <script type="text/javascript">
            $().ready(function () {
//                $("#CloseButton").click(function () {
//                    var returnvalue = $("#code").val();
//                    parent.window.returnValue = returnvalue;
//                    window.close();
//                });


            });
        
        </script>--%>
	</head>
    <!-- Caution: modifying the id and runat attributes of the body will affect the integrity of the application. -->
    <body class="FormBody" id="bodyControl" runat="server">
    <!-- Caution: modifying the id and runat attributes of the form will affect the integrity of the application. -->    
        <form id="formControl" method="post" runat="server">

		<div style="margin-top:8px;margin-left:6px;">	
			<table>
               <tr>
                    <td>
                    <uc1:wucToleranceInput ID="MyToleranceInput" runat="server" />
                    </td>
               </tr>
            </table>
        </div>       
            <asp:button id="btnOK" style="Z-INDEX: 102; LEFT: 416px; POSITION: absolute; TOP: 299px; left:7px;"
				runat="server" Width="72px" Text="确定" DisplayInFrame="False" 
            onclick="btnOK_Click" ></asp:button><!--OnClientClick="return ReturnValue('value');"-->
            <asp:button id="CloseButton" style="Z-INDEX: 102; LEFT: 416px; POSITION: absolute; TOP: 300px;left:120px; height: 21px;"
				runat="server" Width="72px" Text="返回" DisplayInFrame="False" OnClientClick="javascript:window.close();"
             ></asp:button><!-- -->
            
		<%--	<asp:Message id="StatusMessage" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 330px" runat="server"
				Width="480px" Height="16px"></asp:Message>--%>
                <div style="display:none">
                 <input type="text" runat="server" id="code" />
                </div>
		</form>
	</body>
</html>
