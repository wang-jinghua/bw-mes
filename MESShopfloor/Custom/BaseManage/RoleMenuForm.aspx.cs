﻿using Infragistics.WebUI.UltraWebGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.ReportBusiness;

public partial class Custom_BaseManage_RoleMenuForm : BaseForm
{
    int m_PageSize = 6;
    uMESRoleMenuBusiness RoleMenuBusiness = new uMESRoleMenuBusiness ();
    uMESCommonBusiness common = new uMESCommonBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：角色菜单管理";
        master.strTitle = "角色菜单管理";
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { SearchData(upageTurning.CurrentPageIndex); });
        WebPanel = WebAsyncRefreshPanel1;
        if (!IsPostBack)
        {
            GetRole();
            //GetMenu();

            //ShowStatusMessage("", true);
            ClearMessage();
        }
    }
    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        DisplayMessage(strMessage,boolResult);
    }
    #endregion

    #region 清除提示信息
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"" + "" + "\", 'true');</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }
    #endregion

    #region 获取角色列表
    protected void GetRole()
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        DataTable DT = RoleMenuBusiness.GetRole();

        ddlRole.DataTextField = "rolename";
        ddlRole.DataValueField = "roleid";
        ddlRole.DataSource = DT;
        ddlRole.DataBind();
        ddlRole.Items.Insert(0, "");
    }
    #endregion

    #region 获取菜单列表
    protected void GetMenu()
    {
        DataTable DT = RoleMenuBusiness.GetMenu();
        gdMenuList.DataSource = DT;
        gdMenuList.DataBind();
    }
    #endregion

    #region  查询数据
    void SearchData(int index)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["CurrentPageIndex"] = index.ToString();
        para["PageSize"] = m_PageSize.ToString();

        string strMenuName = txtSearchMenuName .Text.Trim();
        string strRoleName = txtSearchRoleName.Text.Trim();
        if (strMenuName != "")
        {
            para["menuname"] = strMenuName;
        }

        if (strRoleName != "")
        {
            para["RoleName"] = strRoleName;
        }
        uMESPagingDataDTO result = RoleMenuBusiness.GetRoleMenuData(para);

        this.gdRole.DataSource = result.DBTable;
        this.gdRole.DataBind();

        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;

    }

    protected void gdRole_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gdRole.Rows.Count; i++)
        {
            string strisDeleted = "";
            if (gdRole.Rows[i].Cells.FromKey("isDeleted").Value != null)
            {
                strisDeleted = gdRole.Rows[i].Cells.FromKey("isDeleted").Value.ToString();
                if (strisDeleted == "1")
                {
                    gdRole.Rows[i].Cells.FromKey("rolename").Style.BackColor = System.Drawing.Color.Red;
                }

            }
        }
    }
    #endregion

    #region 查询按钮
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //ClearData(false, true, true);
            ShowStatusMessage("", true);
            SearchData(1);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
    #endregion

    #region 重置按钮
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        //Response.Redirect(Request.Url.AbsoluteUri);
        ShowStatusMessage("", true);
        ClearData(true, true, true);
    }
    #endregion

    #region 选中菜单角色表中行
    protected void gdRole_ActiveRowChange(object sender, RowEventArgs e)
    {
        ddlRole.Enabled = false;
        gdMenuList.Clear();
        string strid = e.Row.Cells.FromKey("id").Text;
        string strRoleID = e.Row.Cells.FromKey("roleid").Text;

        string strisDeleted = "";
        if (e.Row.Cells.FromKey("isDeleted").Value != null)
        {
            strisDeleted = e.Row.Cells.FromKey("isDeleted").Value.ToString();
        }

        if (string.IsNullOrWhiteSpace(strid) == false)
        {
            txtID.Text = strid;
            ddlRole.SelectedIndex = 0;
            //角色未删除时绑定角色信息
            if (strisDeleted == "0")
            {
                ddlRole.SelectedValue = strRoleID;
                DataTable dt = RoleMenuBusiness.GetRoleMenu(strRoleID);
                //Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuList.Columns.FromKey("ckSelect");
                //for (int j = 0; j <= dt.Rows.Count - 1; j++)
                //{
                //    string strRoleMenuid = dt.Rows[j]["menuid"].ToString();
                //    for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
                //    {
                //        string strMenuid = gdMenuList.Rows[i].Cells.FromKey("id").Text;
                //        Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                //        CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                //        if (strRoleMenuid == strMenuid)
                //        {
                //            ckSelect.Checked = true;
                //        }
                //    }
                //}
                gdMenuList.DataSource = dt;
                gdMenuList.DataBind();
            }
        }
        ShowStatusMessage("", true);
    }
    #endregion 

    #region 全选、全不选
    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuList.Columns.FromKey("ckSelect");
        for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
            ckSelect.Checked = true;
        }
        ShowStatusMessage("", true);
    }

    protected void btnSelectNotAll_Click(object sender, EventArgs e)
    {
        Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuList.Columns.FromKey("ckSelect");
        for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
            ckSelect.Checked = false;
        }
        ShowStatusMessage("", true);
    }
    #endregion

    #region 清除数据
    void ClearData(bool isCondition, bool isGrid, bool isDetail)
    {
        ddlRole.Enabled = true;
        if (isGrid)
        {
            gdRole.Rows.Clear();
        }
        if (isDetail)
        {
            GetRole();
            //GetMenu(); 
            txtSearchMenuName .Text = "";
            txtSearchRoleName.Text = "";
            txtID.Text = "";
            gdMenuList.Clear();
            txtName.Text = "";
            gdSelectList.Clear();
        }
    }
    #endregion

    #region   保存菜单
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("", true);
            ResultModel re = new ResultModel(false, "");
            MESAuditLog ml = new MESAuditLog();
            //必填项验证
            if (ddlRole.SelectedValue == "")
            {
                ShowStatusMessage("请选择角色", false);
                return;
            }
            int inttemp = 0;
            Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuList .Columns.FromKey("ckSelect");
            for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    inttemp += 1;
                } 
            }

            if (inttemp == 0)
            {
                ShowStatusMessage("请选择菜单", false);
                return;
            }
            string strID = txtID.Text;
            if (!string.IsNullOrWhiteSpace(strID))
            {
                re = UpdateData(strID);    //更新 
                #region 日志赋值
                ml.ParentID = ddlRole.SelectedValue; ml.ParentName = "roledef";
                ml.CreateEmployeeID = UserInfo["EmployeeID"];
                ml.BusinessName = "系统配置";ml.OperationType = 1;
                ml.Description = ddlRole.SelectedItem.Text+"角色的菜单修改" ;
                #endregion
            }
            else
            {
                DataTable dt = RoleMenuBusiness.GetRoleMenu(ddlRole .SelectedValue );
                if (dt.Rows.Count >0)
                {
                    ShowStatusMessage("该角色已经有菜单，请选择该角色进行修改", false);
                    return;
                }
                re = SaveData();   //保存
                #region 日志赋值
                ml.ParentID = ddlRole.SelectedValue; 
                ml.CreateEmployeeID = UserInfo["EmployeeID"]; ml.ParentName = "roledef";
                ml.BusinessName = "系统配置"; ml.OperationType = 0;
                ml.Description = ddlRole.SelectedItem.Text + "角色的菜单添加";
                #endregion
            }
            if (re.IsSuccess)
            {
                ClearData(false, false, true);
                SearchData(1);
                //存储日志
                common.SaveMESAuditLog(ml);
            }
            ShowStatusMessage(re.Message, re.IsSuccess);
        }
        catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }


    /// <summary>
    /// 提交数据
    /// </summary>
    /// <returns></returns>
    ResultModel SaveData()
    {
        ResultModel re = new ResultModel(false, "");
        //存入数据
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strEmployeeID = userInfo["EmployeeID"];

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["rolename"] = ddlRole .SelectedItem .Text  ;
        para["roleid"] = ddlRole.SelectedValue;
        para["CreateEmployeeID"] = strEmployeeID;
        para["CreateDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
         
        DataTable dt = new DataTable();
        dt.Columns.Add("menuname");
        dt.Columns.Add("menuid");
        Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuList .Columns.FromKey("ckSelect");
        for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
            if (ckSelect.Checked == true)
            {
                DataRow row = dt.NewRow();
                row["menuname"] = gdMenuList.Rows[i].Cells.FromKey("menuname").Value.ToString();
                row["menuid"] = gdMenuList.Rows[i].Cells.FromKey("menuid").Value.ToString();
                dt.Rows.Add(row);
            }
        } 
       
        re = RoleMenuBusiness  .SaveRoleMenuInfo(para, dt); 
        if (re.IsSuccess)
            re.Message = "保存成功";
        return re;

    }

    ResultModel UpdateData(string strid)
    {
        ResultModel re = new ResultModel(false, "");
        UltraGridRow activeRow = gdRole .DisplayLayout.ActiveRow;
        if (activeRow != null)
        {
            string strRoleid = activeRow.Cells.FromKey("roleid").Text;
            //存入数据
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> para = new Dictionary<string, string>(); 
            para["roleid"] = strRoleid;
            para["rolename"] =ddlRole .SelectedItem.Text; 
            para["CreateEmployeeID"] = strEmployeeID;
            para["CreateDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            //子菜单
            DataTable dt = new DataTable();
            dt.Columns.Add("menuname");
            dt.Columns.Add("menuid");
            Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuList.Columns.FromKey("ckSelect");
            for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    DataRow row = dt.NewRow();
                    row["menuname"] = gdMenuList.Rows[i].Cells.FromKey("menuname").Value.ToString();
                    row["menuid"] = gdMenuList.Rows[i].Cells.FromKey("menuid").Value.ToString();
                    dt.Rows.Add(row);
                }
            } 
            re = RoleMenuBusiness.UpdateRoleMenu  (para, dt);

            //if (re.IsSuccess)
            //{
            //    SearchData(1);
            //    ClearData(false, false, true);
            //}
        }

        ShowStatusMessage(re.Message, re.IsSuccess);

        if (re.IsSuccess)
            re.Message = "保存成功";
        return re;
    }
    #endregion

    #region 删除人员菜单
    protected void btnDel_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("", true);
            ResultModel re = DeleteData();
            if (re.IsSuccess)
            {
                SearchData(1);
                ClearData(false, false, true);
            }
            ShowStatusMessage(re.Message, re.IsSuccess);
           

        }
        catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }

    /// <summary>
    /// 删除数据
    /// </summary>
    /// <returns></returns>
    ResultModel DeleteData()
    {
        ResultModel re = new ResultModel(false, "");
        UltraGridRow activeRow = gdRole.DisplayLayout.ActiveRow;
        if (activeRow == null)
        {
            re.Message = "请选择需要删除的角色菜单";
            return re;
        }

        string strisDeleted = "";
        if (activeRow.Cells.FromKey("isDeleted").Value != null)
        {
            strisDeleted = activeRow.Cells.FromKey("isDeleted").Value.ToString();
        }       

        //判断角色是否已经删除，如果未删除则逐条信息进行删除，如果已删除则该角色关联的所有人员信息一次性删除
        if (strisDeleted == "0")
        {
            string strID = activeRow.Cells.FromKey("id").Text;
            re.IsSuccess = RoleMenuBusiness.DelRoleMenu(strID);

            #region 日志赋值
            MESAuditLog ml = new MESAuditLog();
            ml.ParentID = ddlRole.SelectedValue; ml.ParentName = "roledef";
            ml.CreateEmployeeID = UserInfo["EmployeeID"];
            ml.BusinessName = "系统配置"; ml.OperationType = 2;
            ml.Description = ddlRole.SelectedItem.Text + "角色的菜单删除";
            common.SaveMESAuditLog(ml);
            #endregion
        }
        else
        {
            string strRoleID = activeRow.Cells.FromKey("roleid").Text;
            re.IsSuccess = RoleMenuBusiness.DelRoleMenuByRoleID(strRoleID);
        }
         

        if (re.IsSuccess)
            re.Message = "删除成功";
        return re;
    }
    #endregion 

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("",true);
            ClearData(true, true, true);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    /// <summary>
    /// 菜单信息右移
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnMovrRight_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("", true);
            int inttemp = 0;
            Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuList.Columns.FromKey("ckSelect");
            for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    inttemp += 1;
                }
            }

            if (inttemp == 0)
            {
                ShowStatusMessage("请选择菜单信息！", false);
                return;
            }
            Boolean blInsert = false;
            for (int i = temCell.CellItems.Count - 1; i >= 0; i--)
            {
                blInsert = false;
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    string strMenuName = string.Empty;
                    if (gdMenuList.Rows[i].Cells.FromKey("menuname").Value != null)
                    {
                        strMenuName = gdMenuList.Rows[i].Cells.FromKey("menuname").Value.ToString();
                    }
                    string strID = string.Empty;
                    if (gdMenuList.Rows[i].Cells.FromKey("menuid").Value != null)
                    {
                        strID = gdMenuList.Rows[i].Cells.FromKey("menuid").Value.ToString();
                    }
                    if (gdSelectList.Rows.Count > 0)
                    {
                        for (int j = 0; j < gdSelectList.Rows.Count; j++)
                        {
                            string strComID = string.Empty;
                            if (gdSelectList.Rows[j].Cells.FromKey("id").Value != null)
                            {
                                strComID = gdSelectList.Rows[j].Cells.FromKey("id").Value.ToString();
                            }
                            if (strID == strComID)
                            {
                                blInsert = false;
                                break;
                            }
                            blInsert = true;
                        }
                    }
                    else
                    {
                        blInsert = true;
                    }

                    if (blInsert == true)
                    {
                        gdSelectList.Rows.Add();
                        gdSelectList.Rows[gdSelectList.Rows.Count - 1].Cells.FromKey("menuname").Value = strMenuName;
                        gdSelectList.Rows[gdSelectList.Rows.Count - 1].Cells.FromKey("id").Value = strID;

                    }
                    gdMenuList.Rows[i].Delete();

                }
            }

        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("", true);
            int inttemp = 0;
            for (int i = 0; i <= gdSelectList.Rows.Count - 1; i++)
            {
                if (gdSelectList.Rows[i].Cells.FromKey("select").Value.ToString().ToLower() == "true")
                {
                    inttemp += 1;
                }
            }

            if (inttemp == 0)
            {
                ShowStatusMessage("请选择菜单信息！", false);
                return;
            }
            Boolean blInsert = false;
            for (int i = gdSelectList.Rows.Count - 1; i >= 0; i--)
            {
                blInsert = false;

                if (gdSelectList.Rows[i].Cells.FromKey("select").Value.ToString().ToLower() == "true")
                {
                    string strID = string.Empty;
                    if (gdSelectList.Rows[i].Cells.FromKey("id").Value != null)
                    {
                        strID = gdSelectList.Rows[i].Cells.FromKey("id").Value.ToString();
                    }
                    string strMenuName = string.Empty;
                    if (gdSelectList.Rows[i].Cells.FromKey("menuname").Value != null)
                    {
                        strMenuName = gdSelectList.Rows[i].Cells.FromKey("menuname").Value.ToString();
                    }
 
                    if (gdMenuList.Rows.Count > 0)
                    {
                        for (int j = 0; j < gdMenuList.Rows.Count; j++)
                        {
                            string strComID = string.Empty;
                            if (gdMenuList.Rows[j].Cells.FromKey("menuid").Value != null)
                            {
                                strComID = gdMenuList.Rows[j].Cells.FromKey("menuid").Value.ToString();
                            }
                            if (strID == strComID)
                            {
                                blInsert = false;
                                break;
                            }
                            blInsert = true;
                        }
                    }
                    else
                    {
                        blInsert = true;
                    }

                    if (blInsert == true)
                    {
                        gdMenuList.Rows.Add();
                        gdMenuList.Rows[gdMenuList.Rows.Count - 1].Cells.FromKey("menuname").Value = strMenuName;
                        gdMenuList.Rows[gdMenuList.Rows.Count - 1].Cells.FromKey("menuid").Value = strID;
                
                    }

                }
            }

        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    protected void btnNameSearch_Click1(object sender, EventArgs e)
    {
        try
        {
            
            gdSelectList.Clear();
            if (txtName.Text == "")
            {
                ShowStatusMessage("请输入查询条件！", false);
                return;
            }

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("MenuName", txtName.Text);
            DataTable dt = RoleMenuBusiness.GetMenuInfo(para);
            gdSelectList.DataSource = dt;
            gdSelectList.DataBind();
            ShowStatusMessage("", true);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    protected void gdMenuList_ClickCellButton(object sender, CellEventArgs e)
    {
        int i = e.Cell.Row.Index;
        ResultModel re = new ResultModel();
        if (e.Cell.Key == "MoveUp")
        {
           re= GridRowMove(gdMenuList, i, true);
        }
        else if (e.Cell.Key == "MoveDown") {
            re=GridRowMove(gdMenuList, i, false);
        }
        if (re.IsSuccess == false)
            ShowStatusMessage(re.Message,false);
    }

    /// <summary>
    /// 移动行
    /// </summary>
    /// <param name="gd"></param>
    /// <param name="index"></param>
    /// <param name="isUp"></param>
    /// <returns></returns>
    ResultModel GridRowMove(UltraWebGrid gd,int index,bool isUp) {
        ResultModel re = new ResultModel(false,"");
        int intAfterIndex = 0;
        if (isUp)
        {
            if (index == 0)
            {
                re.Message = "当前选中为第一行，无法进行上移！！";
                return re;
            }
            intAfterIndex = index - 1;
        }
        else {
            if (index == gd.Rows.Count - 1)
            {
                re.Message = "当前选中为最后一行，无法进行下移！！";
                return re;
            }
            intAfterIndex = index + 1;
        }

        
        for (int j = 0; j < gd.Columns.Count; j++)
        {
            string strValue = string.Empty;
            if (!string.IsNullOrEmpty(gd.Rows[index].Cells[j].Text))
            {
                strValue = gd.Rows[index].Cells[j].Text;
            }

            if (!string.IsNullOrEmpty(gd.Rows[intAfterIndex].Cells[j].Text))
            {
                gd.Rows[index].Cells[j].Text = gd.Rows[intAfterIndex].Cells[j].Text;
            }

            gd.Rows[intAfterIndex].Cells[j].Text = strValue;
        }

        gd.DisplayLayout.ActiveRow = gd.Rows[intAfterIndex];
        gd.Rows[intAfterIndex].Selected = true;
        gd.Rows[intAfterIndex].Activated = true;

        re.IsSuccess = true;
        return re;
    }
}