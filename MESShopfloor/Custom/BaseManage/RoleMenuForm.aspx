﻿<%@ Page Title="" Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="RoleMenuForm.aspx.cs" Inherits="Custom_BaseManage_RoleMenuForm" %>

<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdRight" style="width: 10%">
                        <div class="divLabel">角色名称：</div>
                        <asp:TextBox ID="txtSearchRoleName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>

                    <td align="left" class="tdRight" style="width: 10%">
                        <div class="divLabel">菜单名称：</div>
                        <asp:TextBox ID="txtSearchMenuName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>

                    <td valign="bottom" class="tdNoBorder" nowrap="nowrap" style="width: 80%">
                        <asp:Button ID="btnSearch" runat="server" Text="查询" Width="60px" Font-Size="12pt"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置" Width="60px" Font-Size="12pt"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="ItemDiv" runat="server">
            <igtbl:UltraWebGrid ID="gdRole" runat="server" Height="308px" Width="100%" OnActiveRowChange="gdRole_ActiveRowChange" OnDataBound="gdRole_DataBound">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>

                            <igtbl:UltraGridColumn Key="rolename" Width="200px" BaseColumnName="rolename" AllowGroupBy="No">
                                <Header Caption="角色名称">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="menuname" Key="menuname" Width="200px" AllowGroupBy="No">
                                <Header Caption="菜单名称">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="fullname" Key="fullname" Width="180px" AllowGroupBy="No">
                                <Header Caption="创建人">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="createdate" BaseColumnName="createdate" Width="180px" Format="yyyy-MM-dd">
                                <Header Caption="创建时间">
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="menuid" BaseColumnName="menuid" Width="60px" Hidden="True">
                                <Header Caption="menuid">
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>

                            <igtbl:UltraGridColumn BaseColumnName="id" Hidden="True" Key="id">
                                <Header Caption="id">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="roleid" Hidden="True" Key="roleid">
                                <Header Caption="roleid">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="isDeleted" Hidden="True" Key="isDeleted">
                                <Header Caption="isDeleted">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="208px" Width="50%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
            <div style="margin-left: 28%; text-align: right; float: left">
                <uPT:pageTurning ID="upageTurning" runat="server" />
            </div>

            <div style="clear: both"></div>
        </div>
        <div>
            <div style="float:left;">
                <div style="width: 373px; font-size: 11pt">角色</div>
                <asp:DropDownList ID="ddlRole" runat="server" Width="150px" Height="28px" Style="font-size: 16px"></asp:DropDownList>
            </div>
            <%--<div style="float: left; padding-top: 20px; padding-left: 10px">
                <asp:ImageButton ID="btnEmployee" runat="server" DisplayInFrame="false" Text="" Style="width: 16px;"
                    ImageUrl="~/images/search.gif" />
            </div>--%>
            <div style="float: left; margin-left: 220px;width:220px;text-align:left;position:relative;">
                <div style="font-size: 11pt">菜单名称：</div>
                <div>
                    
                <asp:TextBox ID="txtName" runat="server" class="stdTextBox"></asp:TextBox>
                <asp:Button ID="btnNameSearch" runat="server" Text="查询" Width="60px" Font-Size="12pt"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnNameSearch_Click1"  />
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div>
            <div style="float:left;">
                    <igtbl:UltraWebGrid ID="gdMenuList" runat="server" Height="294px" Width="520px" OnClickCellButton="gdMenuList_ClickCellButton">
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:TemplatedColumn Width="60px" AllowGroupBy="No" Key="ckSelect">
                                        <CellTemplate>
                                            <asp:CheckBox ID="ckSelect" runat="server" />
                                        </CellTemplate>
                                        <Header Caption="选择"></Header>
                                    </igtbl:TemplatedColumn>
                                    <igtbl:UltraGridColumn Key="menuname" Width="250px" BaseColumnName="menuname" AllowGroupBy="No">
                                        <Header Caption="菜单名称">
                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                        </Header>

                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="menuid" Key="menuid" Hidden="true" Width="150px" AllowGroupBy="No">
                                        <Header Caption="id">
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn Key="MoveUp" Type="Button" Width="60px" NullText="上移" CellButtonDisplay="Always">
                                        <CellButtonStyle Cursor="Hand" ForeColor="Blue">
                                        </CellButtonStyle>
                                        <Header Caption="">
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                     <igtbl:UltraGridColumn Key="MoveDown" Type="Button" Width="60px" NullText="下移" CellButtonDisplay="Always">
                                        <CellButtonStyle Cursor="Hand" ForeColor="Blue">
                                        </CellButtonStyle>
                                        <Header Caption="">
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="294px" Width="520px">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
            </div>
           <div style="float: left; padding-top: 120px">
                <asp:Button ID="btnMovrRight" runat="server" Text=">>"
                    CssClass="searchButton" EnableTheming="True" Width="68px" OnClick="btnMovrRight_Click" />
                <br />
                <br />
                     <asp:Button ID="btnMoveLeft" runat="server" Text="<<"
                    CssClass="searchButton" EnableTheming="True" Width="68px" OnClick="btnMoveLeft_Click" />
               
                <br />
                <br />
                <asp:Button ID="btnSelectAll" runat="server" Text="全选"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnSelectAll_Click" Width="68px" />
                <br />
                <br />
                <asp:Button ID="btnSelectNotAll" runat="server" Text="全不选"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnSelectNotAll_Click" />
            </div>
            <div>
                    <igtbl:UltraWebGrid ID="gdSelectList" runat="server" Height="294px" Width="101%">
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                <igtbl:UltraGridColumn Key="select" Width="40px" BaseColumnName="select" AllowGroupBy="No" AllowUpdate="Yes" DataType="System.Boolean" Type="CheckBox">
                                    <Header Caption="选择">
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn Key="menuname" Width="250px" BaseColumnName="menuname" AllowGroupBy="No">
                                        <Header Caption="菜单名称">
                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                        </Header>

                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="id" Key="id" Hidden="true" Width="150px" AllowGroupBy="No">
                                        <Header Caption="id">
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="294px" Width="400px">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                <Padding Bottom="3px" Top="2px" />
                                <Padding Top="2px" Bottom="3px"></Padding>
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                    </igtbl:UltraWebGrid>
            </div>
        </div>
        <table>
            <tr>
                <td style="width: 368px" colspan="2">

                </td>
            </tr>
            <tr>
                <td style="width: 368px">

                </td>
                <td valign="bottom">

                </td>
            </tr>
        </table>

        <div style="clear: both"></div>

        <div style="margin: 10px 5px">
            <asp:Button ID="btnSubmit" runat="server" Text="保存" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                CssClass="searchButton" EnableTheming="True" Style="margin-right: 0px" OnClick="btnSubmit_Click" />
            <asp:Button ID="btnInsert" runat="server" Text="新增" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                CssClass="searchButton" EnableTheming="True" Style="margin-left: 20px" OnClick="btnInsert_Click" />
            <asp:Button ID="btnDel" runat="server" Text="删除" UseSubmitBehavior="true" OnClientClick=" return ((confirm('确认删除？') == true) ? true :  false) "
                CssClass="searchButton" EnableTheming="True" Style="margin-left: 20px" OnClick="btnDel_Click" />
        </div>
        <div>
            <asp:TextBox ID="txtID" runat="server" Width="200" Height="20px" Visible="false"></asp:TextBox>
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>

