﻿using Infragistics.WebUI.UltraWebGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.ReportBusiness;

public partial class Custom_BaseManage_RoleEmployeeForm : BaseForm
{
    int m_PageSize = 6;string businessName = "系统配置", parentName = "roledef";
    uMESRoleEmployeeBusiness RoleEmployeeBusiness = new uMESRoleEmployeeBusiness();
    uMESCommonBusiness common = new uMESCommonBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：人员角色管理";
        master.strTitle = "人员角色管理";
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { SearchData(upageTurning.CurrentPageIndex); });
        WebPanel = WebAsyncRefreshPanel1;
        if (!IsPostBack)
        {
            GetRole();
            //GetEmployee();

            //ShowStatusMessage("", true);
            ClearMessage();
        }
    }

    #region 获取角色列表
    protected void GetRole()
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        DataTable DT = RoleEmployeeBusiness.GetRole();

        ddlRole.DataTextField = "rolename";
        ddlRole.DataValueField = "roleid";
        ddlRole.DataSource = DT;
        ddlRole.DataBind();
        ddlRole.Items.Insert(0, "");
    }
    #endregion

    #region 获取人员列表
    protected void GetEmployee()
    {
        DataTable DT = common.GetEmployee("");
        gdEmployeeList.DataSource = DT;
        gdEmployeeList.DataBind();
    }
    #endregion

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        DisplayMessage(strMessage,boolResult);
    }
    #endregion

    #region 清除提示信息
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"" + "" + "\", 'true');</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }
    #endregion

    #region  查询数据
    void SearchData(int index)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["CurrentPageIndex"] = index.ToString();
        para["PageSize"] = m_PageSize.ToString();

        string strFullName = txtSearchFullName.Text.Trim();
        string strRoleName = txtSearchRoleName.Text.Trim();
        if (strFullName != "")
        {
            para["FullName"] = strFullName;
        }

        if (strRoleName != "")
        {
            para["RoleName"] = strRoleName;
        }
        uMESPagingDataDTO result = RoleEmployeeBusiness.GetRoleEmployeeData(para);

        this.gdRole.DataSource = result.DBTable;
        this.gdRole.DataBind();

        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;

    }

    protected void gdRole_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gdRole.Rows.Count; i++)
        {
            string strisDeleted = "";
            if (gdRole.Rows[i].Cells.FromKey("isDeleted").Value != null)
            {
                strisDeleted = gdRole.Rows[i].Cells.FromKey("isDeleted").Value.ToString();
                if (strisDeleted=="1")
                {
                    gdRole.Rows[i].Cells.FromKey("rolename").Style.BackColor = System.Drawing.Color.Red;
                }
       
            }
        }
    }
    #endregion

    #region 查询按钮
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //ClearData(false, true, true);
            SearchData(1);
        } catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }
    #endregion

    #region 重置按钮
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        //Response.Redirect(Request.Url.AbsoluteUri);
        ShowStatusMessage("",true);
        ClearData(true, true, true);
    }
    #endregion

    #region 选中人员角色表中行
    protected void gdgdMenu_ActiveRowChange(object sender, RowEventArgs e)
    {
        ddlRole.Enabled = false;
        string strid = e.Row.Cells.FromKey("id").Text;
        string strRoleID = e.Row.Cells.FromKey("roleid").Text;
        string strisDeleted = ""; 
        if (e.Row.Cells.FromKey("isDeleted").Value !=null)
        {
            strisDeleted= e.Row.Cells.FromKey("isDeleted").Value.ToString();
        }
        gdEmployeeList.Clear();
        if (string.IsNullOrWhiteSpace(strid) == false)
        {
            txtID.Text = strid;
            ddlRole.SelectedIndex = 0;
            //角色未删除时绑定角色信息
            if (strisDeleted=="0")
            {
                ddlRole.SelectedValue = strRoleID;
                DataTable dt = RoleEmployeeBusiness.GetRoleEmployee(strRoleID);
                //Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdEmployeeList.Columns.FromKey("ckSelect");
                //for (int j = 0; j <= dt.Rows.Count - 1; j++)
                //{
                //    string strRoleEmployeeid = dt.Rows[j]["employeeid"].ToString();
                //    for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
                //    {
                //        string strEmployeeid = gdEmployeeList.Rows[i].Cells.FromKey("employeeid").Text;
                //        Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                //        CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                //        if (strRoleEmployeeid == strEmployeeid)
                //        {
                //            ckSelect.Checked = true;
                //        }
                //    }
                //}
                gdEmployeeList.DataSource = dt;
                gdEmployeeList.DataBind();
            }

        }
        ShowStatusMessage("", true);
    }
    #endregion 

    #region 全选、全不选
    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("",true);
        Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdEmployeeList.Columns.FromKey("ckSelect");
        for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
            ckSelect.Checked = true;
        }
    }

    protected void btnSelectNotAll_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);
        Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdEmployeeList.Columns.FromKey("ckSelect");
        for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
            ckSelect.Checked = false;
        }
    }
    #endregion

    #region 清除数据
    void ClearData(bool isCondition, bool isGrid, bool isDetail)
    {
        ddlRole.Enabled = true;
        upageTurning.TotalRowCount= 0;
        if (isCondition) {
            txtSearchFullName.Text = "";
            txtSearchRoleName.Text = "";
        }
        if (isGrid)
        {
            gdRole.Rows.Clear();
        }
        if (isDetail)
        {
            GetRole();
            //GetEmployee(); 
           
            txtID.Text = "";
            txtName.Text = "";
            gdEmployeeList.Clear();
            gdEmployeeInfo.Clear();
        }
    }
    #endregion

    #region   保存菜单
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            ResultModel re = new ResultModel(false, "");

            //必填项验证
            if (ddlRole.SelectedValue == "")
            {
                ShowStatusMessage("请选择角色", false);
                return;
            }
            int inttemp = 0;
            Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdEmployeeList.Columns.FromKey("ckSelect");
            for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    inttemp += 1;
                } 
            }

            if (inttemp == 0)
            {
                ShowStatusMessage("请选择人员", false);
                return;
            }
            string strID = txtID.Text;

            #region 日志赋值
            MESAuditLog ml = new MESAuditLog();
            ml.ParentID = ddlRole.SelectedValue; ml.ParentName = parentName;
            ml.CreateEmployeeID = UserInfo["EmployeeID"];
            ml.BusinessName = businessName; 
            #endregion

            if (!string.IsNullOrWhiteSpace(strID))
            {
                re = UpdateData(strID);    //更新 

                ml.OperationType = 1;
                ml.Description = ddlRole.SelectedItem.Text + "角色的人员修改";
            }
            else
            {
                DataTable dt = RoleEmployeeBusiness .GetRoleEmployee (ddlRole.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    ShowStatusMessage("该角色已经有人员，请选择该角色进行修改", false);
                    return;
                }
                re = SaveData();   //保存

                ml.OperationType = 0;//添加
                ml.Description = ddlRole.SelectedItem.Text + "角色的人员添加";
            }
            if (re.IsSuccess)
            {
                ClearData(false, false, true);
                SearchData(1);
                common.SaveMESAuditLog(ml);
            }

            ShowStatusMessage(re.Message, re.IsSuccess);
        }
        catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }


    /// <summary>
    /// 提交数据
    /// </summary>
    /// <returns></returns>
    ResultModel SaveData()
    {
        ResultModel re = new ResultModel(false, "");
        //存入数据
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strEmployeeID = userInfo["EmployeeID"];

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["rolename"] = ddlRole .SelectedItem .Text  ;
        para["roleid"] = ddlRole.SelectedValue;
        para["CreateEmployeeID"] = strEmployeeID;
        para["CreateDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");


        //子菜单
        DataTable dt = new DataTable();
        dt.Columns.Add("employeeid");

        Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdEmployeeList.Columns.FromKey("ckSelect");
        for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
            if (ckSelect.Checked == true)
            {
                DataRow row = dt.NewRow();
                row["employeeid"] = gdEmployeeList.Rows[i].Cells.FromKey("employeeid").Value.ToString(); 
                dt.Rows.Add(row);
            }
        } 
       
        re = RoleEmployeeBusiness .SaveRoleEmployeeInfo (para, dt); 
        if (re.IsSuccess)
            re.Message = "保存成功";
        return re;

    }

    ResultModel UpdateData(string strid)
    {
        ResultModel re = new ResultModel(false, "");
        UltraGridRow activeRow = gdRole .DisplayLayout.ActiveRow;
        if (activeRow != null)
        {
            string strRoleid = activeRow.Cells.FromKey("roleid").Text;
            //存入数据
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> para = new Dictionary<string, string>(); 
            para["roleid"] = strRoleid;
            para["rolename"] =ddlRole  .SelectedItem.Text; 
            para["CreateEmployeeID"] = strEmployeeID;
            para["CreateDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            //子菜单
            DataTable dt = new DataTable();
            dt.Columns.Add("employeeid");
            Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdEmployeeList.Columns.FromKey("ckSelect");
            for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    DataRow row = dt.NewRow();
                    row["employeeid"] = gdEmployeeList.Rows[i].Cells.FromKey("employeeid").Value.ToString();
                    dt.Rows.Add(row);
                }
            } 
            re = RoleEmployeeBusiness .UpdateRoleEmployee (para, dt);

            //if (re.IsSuccess)
            //{
            //    SearchData(1);
            //    ClearData(false, false, true);
            //}
        }

        ShowStatusMessage(re.Message, re.IsSuccess);

        if (re.IsSuccess)
            re.Message = "保存成功";
        return re;
    }
    #endregion

    #region 删除人员角色
    protected void btnDel_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("", true);
            ResultModel re = DeleteData();
            if (re.IsSuccess)
            {
                #region 日志赋值
                MESAuditLog ml = new MESAuditLog();
                ml.ParentID = ddlRole.SelectedValue; ml.ParentName = parentName;
                ml.CreateEmployeeID = UserInfo["EmployeeID"];
                ml.BusinessName = businessName;
                ml.OperationType = 2;
                ml.Description = ddlRole.SelectedItem.Text + "角色的人员删除";
                common.SaveMESAuditLog(ml);
                #endregion

                SearchData(1);
                ClearData(false, false, true);
               
            }
            ShowStatusMessage(re.Message, re.IsSuccess);

        }
        catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }

    /// <summary>
    /// 删除数据
    /// </summary>
    /// <returns></returns>
    ResultModel DeleteData()
    {
        ResultModel re = new ResultModel(false, "");
        UltraGridRow activeRow = gdRole.DisplayLayout.ActiveRow;
        if (activeRow == null)
        {
            re.Message = "请选择需要删除的角色人员";
            return re;
        }

        string strisDeleted = "";
        if (activeRow.Cells.FromKey("isDeleted").Value != null)
        {
            strisDeleted = activeRow.Cells.FromKey("isDeleted").Value.ToString();
        }

        //判断角色是否已经删除，如果未删除则逐条信息进行删除，如果已删除则该角色关联的所有人员信息一次性删除
        if (strisDeleted == "0")
        {
            string strID = activeRow.Cells.FromKey("id").Text;
            re.IsSuccess = RoleEmployeeBusiness.DelRoleEmployee(strID);
        }
        else
        {
            string strRoleID = activeRow.Cells.FromKey("roleid").Text;
            re.IsSuccess = RoleEmployeeBusiness.DelRoleEmployeeByRoleID(strRoleID);
        }
       

        if (re.IsSuccess)
            re.Message = "删除成功";
        return re;
    }
    #endregion 

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("", true);
            ClearData(true,true,true);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    protected void btnNameSearch_Click(object sender, EventArgs e)
    {

        try
        {
            ShowStatusMessage("", true);
            gdEmployeeInfo.Clear();
            if (txtName.Text=="")
            {
                ShowStatusMessage("请输入查询条件！", false);
                return;
            }

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("EmployeeName", txtName.Text);
            DataTable dt = RoleEmployeeBusiness.GetEmployeeInfo(para);
            gdEmployeeInfo.DataSource = dt;
            gdEmployeeInfo.DataBind();
           
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    /// <summary>
    /// 人员信息右移
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnMovrRight_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("",true);
            int inttemp = 0;
            Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdEmployeeList.Columns.FromKey("ckSelect");
            for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    inttemp += 1;
                }
            }

            if (inttemp == 0)
            {
                ShowStatusMessage("请选择人员信息！", false);
                return;
            }
            Boolean blInsert = false;
            for (int i = temCell.CellItems.Count - 1; i >=0; i--)
            {
                blInsert = false;
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    string strFullName = string.Empty;
                    if (gdEmployeeList.Rows[i].Cells.FromKey("fullname").Value !=null)
                    {
                        strFullName = gdEmployeeList.Rows[i].Cells.FromKey("fullname").Value.ToString();
                    }
                    string strFactoryName = string.Empty;
                    if (gdEmployeeList.Rows[i].Cells.FromKey("factoryname").Value != null)
                    {
                        strFactoryName = gdEmployeeList.Rows[i].Cells.FromKey("factoryname").Value.ToString();
                    }
                    string strTeamName = string.Empty;
                    if (gdEmployeeList.Rows[i].Cells.FromKey("teamname").Value != null)
                    {
                        strTeamName = gdEmployeeList.Rows[i].Cells.FromKey("teamname").Value.ToString();
                    }
                    string strEmployeeName = string.Empty;
                    if (gdEmployeeList.Rows[i].Cells.FromKey("employeename").Value != null)
                    {
                        strEmployeeName = gdEmployeeList.Rows[i].Cells.FromKey("employeename").Value.ToString();
                    }
                    string strEmployeeId = string.Empty;
                    if (gdEmployeeList.Rows[i].Cells.FromKey("employeeid").Value != null)
                    {
                        strEmployeeId = gdEmployeeList.Rows[i].Cells.FromKey("employeeid").Value.ToString();
                    }
                    if (gdEmployeeInfo.Rows.Count > 0)
                    {
                        for (int j = 0; j < gdEmployeeInfo.Rows.Count; j++)
                        {
                            string strComName = string.Empty;
                            if (gdEmployeeInfo.Rows[j].Cells.FromKey("employeename").Value != null)
                            {
                                strComName = gdEmployeeInfo.Rows[j].Cells.FromKey("employeename").Value.ToString();
                            }
                            if (strComName == strEmployeeName)
                            {
                                blInsert = false;
                                break;
                            }
                            blInsert = true;
                        }
                    }
                    else
                    {
                        blInsert = true;
                    }

                    if (blInsert==true)
                    {
                        gdEmployeeInfo.Rows.Add();
                        gdEmployeeInfo.Rows[gdEmployeeInfo.Rows.Count - 1].Cells.FromKey("FullName").Value = strFullName;
                        gdEmployeeInfo.Rows[gdEmployeeInfo.Rows.Count - 1].Cells.FromKey("FactoryName").Value = strFactoryName;
                        gdEmployeeInfo.Rows[gdEmployeeInfo.Rows.Count - 1].Cells.FromKey("TeamName").Value = strTeamName;
                        gdEmployeeInfo.Rows[gdEmployeeInfo.Rows.Count - 1].Cells.FromKey("EmployeeName").Value = strEmployeeName;
                        gdEmployeeInfo.Rows[gdEmployeeInfo.Rows.Count - 1].Cells.FromKey("EmployeeId").Value = strEmployeeId;
                    }
                    gdEmployeeList.Rows[i].Delete();

                }
            }

        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    protected void btnMoveLeft_Click(object sender, EventArgs e)
    {
        try
        {
            ShowStatusMessage("", true);
            int inttemp = 0;
            for (int i = 0; i <= gdEmployeeInfo.Rows.Count - 1; i++)
            {
                if (gdEmployeeInfo.Rows[i].Cells.FromKey("select").Value.ToString().ToLower() == "true")
                {
                    inttemp += 1;
                }
            }

            if (inttemp == 0)
            {
                ShowStatusMessage("请选择人员信息！", false);
                return;
            }
            Boolean blInsert = false;
            for (int i = gdEmployeeInfo.Rows.Count - 1; i >= 0; i--)
            {
                blInsert = false;
    
                if (gdEmployeeInfo.Rows[i].Cells.FromKey("select").Value.ToString().ToLower() == "true")
                {
                    string strFullName = string.Empty;
                    if (gdEmployeeInfo.Rows[i].Cells.FromKey("fullname").Value != null)
                    {
                        strFullName = gdEmployeeInfo.Rows[i].Cells.FromKey("fullname").Value.ToString();
                    }
                    string strFactoryName = string.Empty;
                    if (gdEmployeeInfo.Rows[i].Cells.FromKey("factoryname").Value != null)
                    {
                        strFactoryName = gdEmployeeInfo.Rows[i].Cells.FromKey("factoryname").Value.ToString();
                    }
                    string strTeamName = string.Empty;
                    if (gdEmployeeInfo.Rows[i].Cells.FromKey("teamname").Value != null)
                    {
                        strTeamName = gdEmployeeInfo.Rows[i].Cells.FromKey("teamname").Value.ToString();
                    }
                    string strEmployeeName = string.Empty;
                    if (gdEmployeeInfo.Rows[i].Cells.FromKey("employeename").Value != null)
                    {
                        strEmployeeName = gdEmployeeInfo.Rows[i].Cells.FromKey("employeename").Value.ToString();
                    }
                    string strEmployeeId = string.Empty;
                    if (gdEmployeeInfo.Rows[i].Cells.FromKey("employeeid").Value != null)
                    {
                        strEmployeeId = gdEmployeeInfo.Rows[i].Cells.FromKey("employeeid").Value.ToString();
                    }
                    if (gdEmployeeList.Rows.Count > 0)
                    {
                        for (int j = 0; j < gdEmployeeList.Rows.Count; j++)
                        {
                            string strComName = string.Empty;
                            if (gdEmployeeList.Rows[j].Cells.FromKey("employeename").Value != null)
                            {
                                strComName = gdEmployeeList.Rows[j].Cells.FromKey("employeename").Value.ToString();
                            }
                            if (strComName == strEmployeeName)
                            {
                                blInsert = false;
                                break;
                            }
                            blInsert = true;
                        }
                    }
                    else
                    {
                        blInsert = true;
                    }

                    if (blInsert == true)
                    {
                        gdEmployeeList.Rows.Add();
                        gdEmployeeList.Rows[gdEmployeeList.Rows.Count - 1].Cells.FromKey("FullName").Value = strFullName;
                        gdEmployeeList.Rows[gdEmployeeList.Rows.Count - 1].Cells.FromKey("FactoryName").Value = strFactoryName;
                        gdEmployeeList.Rows[gdEmployeeList.Rows.Count - 1].Cells.FromKey("TeamName").Value = strTeamName;
                        gdEmployeeList.Rows[gdEmployeeList.Rows.Count - 1].Cells.FromKey("EmployeeName").Value = strEmployeeName;
                        gdEmployeeList.Rows[gdEmployeeList.Rows.Count - 1].Cells.FromKey("EmployeeId").Value = strEmployeeId;
                    }

                }
            }

        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
}