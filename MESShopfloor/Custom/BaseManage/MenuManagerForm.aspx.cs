﻿using Infragistics.WebUI.UltraWebGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ParameterDTO;
using uMES.LeanManufacturing.ReportBusiness;

public partial class Custom_BaseManage_MenuManagerForm : BaseForm
{
    int m_PageSize = 6;
    uMESMenuBusiness MenuBusiness = new uMESMenuBusiness();
    uMESCommonBusiness commonBal = new uMESCommonBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：菜单管理";
        master.strTitle = "菜单管理"; 
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { SearchData(upageTurning.CurrentPageIndex); });
        
        if (!IsPostBack)
        {
            //ShowStatusMessage("", true);
            ClearMessage();
        } 
        if (Session["FileData"] != null)
        {
            txtFile.Text = Session["FileData"].ToString(); 
            Session["FileData"] = null;
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        // ClientScript.RegisterStartupScript(GetType(), "", strScript);
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
    }
    #endregion

    #region 清除提示信息
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"" + "" + "\", 'true');</script>";
        LiteralControl child = new LiteralControl(strScript);
        WebAsyncRefreshPanel1.Controls.Add(child);
    }
    #endregion
    #region  查询数据
    void SearchData(int index)
    {
        
        Session["FileData"] = null;
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["CurrentPageIndex"] = index.ToString();
        para["PageSize"] = m_PageSize.ToString();

        string strMenuName = txtSearchMenuName.Text.Trim();
        string strMenuChildName = txtSearchMenuChidName.Text.Trim();
        if (strMenuName != null)
        {
            para["MenuName"] = strMenuName;
        }

        if (strMenuChildName != null)
        {
            para["MenuChidName"] = strMenuChildName;
        }
        uMESPagingDataDTO result = MenuBusiness.GetMenuData(para);

        this.gdMenu.DataSource = result.DBTable;
        this.gdMenu.DataBind();

        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;

    }
    #endregion

    #region 查询按钮
    protected void btnSearch_Click(object sender, EventArgs e)
    { 
        try
        { 
            //ClearData(false, true, true);
            SearchData(1);
        }
        catch (Exception ex)
        { ShowStatusMessage(ex.Message, false); }
    }
    #endregion

    #region 重置按钮
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        //Response.Redirect(Request.Url.AbsoluteUri);
        ShowStatusMessage("", true);
        ClearData(true, true, true);
    }
    #endregion

    #region 选择文件按钮
    protected void btnFile_Click(object sender, EventArgs e)
    { 
        //ScriptManager.RegisterStartupScript(UpdatePanel3, this.Page.GetType(), "", "FileData();", true);
    }
    #endregion

    #region 选中菜单表中行
    protected void gdgdMenu_ActiveRowChange(object sender, RowEventArgs e)
    {
        string strid = e.Row.Cells.FromKey("id").Text;
        string strName = e.Row.Cells.FromKey("menuname").Text;

        if (string.IsNullOrWhiteSpace(strid) == false)
        {
            txtMenuName.Text = strName;
            txtMainID.Text = strid;
            DataTable dt = MenuBusiness.GetMenuChid(strid);
            gdMenuChid.DataSource = dt;
            gdMenuChid.DataBind();
        }

        ShowStatusMessage("", true);
    } 

    #endregion

    #region 选择子菜单列表一行
    protected void gdMenuChid_ActiveRowChange(object sender, RowEventArgs e)
    {
        string strid = e.Row.Cells.FromKey("id").Text;
        string strName = e.Row.Cells.FromKey("menuchildname").Text;
        string strSequence = e.Row.Cells.FromKey("Sequence").Text;
        string strUrl = e.Row.Cells.FromKey("url").Text;
        // e.Row.Cells.FromKey("select").Value = true;
        txtFile.Text = string.Empty;
        txtID.Text = strid;
        txtMenuChilName.Text = strName;
        txtSequence.Text = strSequence;
        ShowStatusMessage("",true);
    }
    #endregion

    #region 全选、全不选
    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        //Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuChid.Columns.FromKey("ckSelect");
        //for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        //{
        //    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
        //    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
        //    ckSelect.Checked = true;
        //}

        for (int i=0;i<gdMenuChid.Rows.Count;i++)
        {
            gdMenuChid.Rows[i].Cells.FromKey("select").Value = true;
        }
    }

    protected void btnSelectNotAll_Click(object sender, EventArgs e)
    {
        //Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuChid.Columns.FromKey("ckSelect");
        //for (int i = 0; i <= temCell.CellItems.Count - 1; i++)
        //{
        //    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
        //    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
        //    ckSelect.Checked = false;
        //}
        for (int i = 0; i < gdMenuChid.Rows.Count ; i++)
        {
            gdMenuChid.Rows[i].Cells.FromKey("select").Value = false;
        }
    }
    #endregion

    #region 子菜单添加
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(txtMenuChilName.Text))
        {
            ShowStatusMessage("请输入子菜单名称", false);
            return;
        }
        //if (string.IsNullOrWhiteSpace(txtSequence.Text))
        //{
        //    ShowStatusMessage("请输入子菜单顺序号", false);
        //    return;
        //}
        //else
        //{
        //    string strSequene = txtSequence.Text.Trim();
        //    if (txtSequence.Text.Contains("."))
        //    {
        //        ShowStatusMessage("数量应为正数", false);
        //        return;
        //    }
        //    else
        //    {
        //        try
        //        {
        //            int intSequence = Convert.ToInt32(strSequene);

        //            if (intSequence <= 0)
        //            {
        //                ShowStatusMessage("数量应为正整数", false);
        //                return;
        //            }
        //        }
        //        catch
        //        {
        //            ShowStatusMessage("数量应为数字", false);
        //            return;
        //        }
        //    }
        //}
        if (string.IsNullOrWhiteSpace(txtFile .Text ))
        {
            ShowStatusMessage("请选择子菜单路径", false);
            return;
        }


        int i = txtFile.Text.LastIndexOf("MESShopfloor\\");
        string strUrl = "";
        if (txtFile.Text.Contains("http"))
        {
            strUrl = txtFile.Text;
        }
        else {
             strUrl = txtFile.Text.Substring(i + 13);
        }

        string strMessage = string.Empty;
        for (int h = 0; h < gdMenuChid.Rows.Count; h++)
        {
            strMessage = "";
            string strMenuChildName = string.Empty;
            if (!string.IsNullOrEmpty(gdMenuChid.Rows[h].Cells.FromKey("menuchildname").Text))
            {
                strMenuChildName = gdMenuChid.Rows[h].Cells.FromKey("menuchildname").Text;
            }
            string strPath = string.Empty;
            if (!string.IsNullOrEmpty(gdMenuChid.Rows[h].Cells.FromKey("url").Text))
            {
                strPath = gdMenuChid.Rows[h].Cells.FromKey("url").Text;
            }
            if (strMenuChildName == txtMenuChilName.Text && strPath == strUrl)
            {
                strMessage = "已存在名为" + txtMenuChilName.Text + "的菜单！";
                ShowStatusMessage(strMessage, false);
                return;
            }
        }

        UltraGridRow activeRow = gdMenuChid.DisplayLayout.ActiveRow;

        if (activeRow == null)
        {
            gdMenuChid.Rows.Add();
            gdMenuChid.Rows[gdMenuChid.Rows.Count - 1].Cells.FromKey("menuchildname").Text = txtMenuChilName.Text;
            gdMenuChid.Rows[gdMenuChid.Rows.Count - 1].Cells.FromKey("url").Text = strUrl;
            gdMenuChid.Rows[gdMenuChid.Rows.Count - 1].Cells.FromKey("Sequence").Text = txtSequence.Text;
            gdMenuChid.Rows[gdMenuChid.Rows.Count - 1].Cells.FromKey("id").Text = "";


            gdMenuChid.DisplayLayout.ActiveRow = gdMenuChid.Rows[gdMenuChid.Rows.Count - 1];
            gdMenuChid.Rows[gdMenuChid.Rows.Count - 1].Selected = true;
            gdMenuChid.Rows[gdMenuChid.Rows.Count - 1].Activated = true;
            gdMenuChid.DisplayLayout.ScrollBar = ScrollBar.Auto;
            gdMenuChid.DisplayLayout.ScrollTop =(gdMenuChid.Rows.Count)*20;

        }
        else
        {
            //DataTable dt1 = new DataTable();
            //dt1.Columns.Add("menuchildname");
            //dt1.Columns.Add("url");
            //dt1.Columns.Add("Sequence");
            //dt1.Columns.Add("id");
            //dt1.Columns.Add("mainid");
            int intSelectRow = activeRow.Index;

            //for (int j=0;j<gdMenuChid.Rows.Count;i++)
            //{
            //    dt1.Rows.Add();
            //    if (!string.IsNullOrEmpty(gdMenuChid.Rows[j].Cells.FromKey("menuchildname").Text))
            //    {
            //        dt1.Rows[dt1.Rows.Count - 1]["menuchildname"] = gdMenuChid.Rows[j].Cells.FromKey("menuchildname").Text;
            //    }
            //    if (!string.IsNullOrEmpty(gdMenuChid.Rows[j].Cells.FromKey("url").Text))
            //    {
            //        dt1.Rows[dt1.Rows.Count - 1]["url"] = gdMenuChid.Rows[j].Cells.FromKey("url").Text;
            //    }
            //    if (!string.IsNullOrEmpty(gdMenuChid.Rows[j].Cells.FromKey("Sequence").Text))
            //    {
            //        dt1.Rows[dt1.Rows.Count - 1]["Sequence"] = gdMenuChid.Rows[j].Cells.FromKey("Sequence").Text;
            //    }
            //    if (!string.IsNullOrEmpty(gdMenuChid.Rows[j].Cells.FromKey("id").Text))
            //    {
            //        dt1.Rows[dt1.Rows.Count - 1]["id"] = gdMenuChid.Rows[j].Cells.FromKey("id").Text;
            //    }
            //    if (!string.IsNullOrEmpty(gdMenuChid.Rows[j].Cells.FromKey("mainid").Text))
            //    {
            //        dt1.Rows[dt1.Rows.Count - 1]["mainid"] = gdMenuChid.Rows[j].Cells.FromKey("mainid").Text;
            //    }


            //    if (j==intSelectRow)
            //    {
            //        dt1.Rows.Add();
            //        dt1.Rows[dt1.Rows.Count - 1]["menuchildname"] = txtMenuChilName.Text;
            //        dt1.Rows[dt1.Rows.Count - 1]["url"] = strUrl;
            //        dt1.Rows[dt1.Rows.Count - 1]["Sequence"] = "";
            //        dt1.Rows[dt1.Rows.Count - 1]["id"] = "";
            //        dt1.Rows[dt1.Rows.Count - 1]["mainid"] = "";

            //    }
            //}

            //gdMenuChid.Clear();

            //gdMenuChid.DataSource = dt1;
            //gdMenuChid.DataBind();
            UltraGridRow GridRow = new UltraGridRow();
            gdMenuChid.Rows.Insert(intSelectRow + 1, GridRow, false);

            gdMenuChid.Rows[intSelectRow + 1].Cells.FromKey("menuchildname").Text = txtMenuChilName.Text;
            gdMenuChid.Rows[intSelectRow + 1].Cells.FromKey("url").Text = strUrl;

            gdMenuChid.DisplayLayout.ActiveRow = gdMenuChid.Rows[intSelectRow+1];
            gdMenuChid.Rows[intSelectRow+1].Selected = true;
            gdMenuChid.Rows[intSelectRow+1].Activated = true;
            gdMenuChid.DisplayLayout.ScrollBar = ScrollBar.Always;

        }
        txtMenuChilName.Text = "";
        txtSequence.Text = "";
        txtID.Text = "";
        txtFile.Text = "";
        ShowStatusMessage("", true);
    }

    #endregion
    #region 子菜单删除
    protected void btnChildDel_Click(object sender, EventArgs e)
    {
        //int intTemp = 0;
        ////Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuChid.Columns.FromKey("ckSelect");
        ////for (int i = gdMenuChid.Rows.Count - 1; i >= 0; i--)
        ////{
        ////    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
        ////    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
        ////    if (ckSelect.Checked == true)
        ////    {
        ////        intTemp += 1;
        ////    }
        ////}
        //for (int i = 0; i < gdMenuChid.Rows.Count; i++)
        //{
        //    if (gdMenuChid.Rows[i].Cells.FromKey("select").Value.ToString()=="true")
        //    {
        //        intTemp += 1;
        //    }
        //}
        //if (intTemp == 0)
        //{
        //    ShowStatusMessage("请选择需要删除的子菜单", false);
        //    return;
        //}

        //for (int i = gdMenuChid.Rows.Count - 1; i >= 0; i--)
        //{
        //    //Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
        //    //CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
        //    //if (ckSelect.Checked == true)
        //    //{
        //    //    gdMenuChid.Rows.Remove(gdMenuChid.Rows[i]);
        //    //}

        //    if (gdMenuChid.Rows[i].Cells.FromKey("select").Value.ToString() == "true")
        //    {
        //        gdMenuChid.Rows.Remove(gdMenuChid.Rows[i]);
        //    }
        //}

        UltraGridRow activeRow = gdMenuChid.DisplayLayout.ActiveRow;

        if (activeRow == null)
        {
            ShowStatusMessage("请选择需要删除的子菜单", false);
            return;
        }

        gdMenuChid.Rows.Remove(activeRow);
        ShowStatusMessage("",true);
    }
    #endregion

    #region 清除数据
    void ClearData(bool isCondition, bool isGrid, bool isDetail)
    {
        ShowStatusMessage("", true);
        if (isGrid)
        {
            gdMenu.Rows.Clear();
        }
        if (isDetail)
        {
            gdMenuChid.Rows.Clear();
            txtMainID.Text = "";
            txtID.Text = "";
            txtMenuChilName.Text = "";
            txtMenuName.Text = "";
            txtSequence.Text = "";
            txtSearchMenuName.Text = "";
            txtSearchMenuChidName.Text = "";
            txtFile.Text ="";
        }
    }
    #endregion

    #region   保存菜单
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            ResultModel re = new ResultModel(false, "");
            MESAuditLog ml = new MESAuditLog();
            //必填项验证
            if (gdMenuChid.Rows.Count == 0)
            {
                ShowStatusMessage("请输入子菜单", false);
                return;
            }
            string strID = txtMainID.Text;
            if (!string.IsNullOrWhiteSpace(strID))
            {
                re = UpdateData(strID);    //更新 

                #region 日志赋值
                ml.ParentID = strID;ml.ParentName= "menu";
                ml.CreateEmployeeID = UserInfo["EmployeeID"];
                ml.BusinessName = "系统配置";ml.OperationType = 1;
                ml.Description = "菜单修改:"+ txtMenuName.Text;
                #endregion
            }
            else
            {
                //保存前验证主菜单是否已经存在
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("MenuName1", txtMenuName.Text);
                para.Add("CurrentPageIndex", "1");
                para.Add("PageSize", "50");
                uMESPagingDataDTO result = MenuBusiness.GetMenuData(para);
                if (result.DBTable.Rows.Count>0)
                {
                    ShowStatusMessage("名为" + txtMenuName.Text + "的主菜单已存在！", false);
                    return;
                }
                re = SaveData();   //保存

                #region 日志赋值
                ml.ParentID = re.Data.ToString(); ml.ParentName = "menu";
                ml.CreateEmployeeID = UserInfo["EmployeeID"];
                ml.BusinessName = "系统配置"; ml.OperationType = 0;
                ml.Description = "菜单添加:" + txtMenuName.Text;
                #endregion
            }

            if (re.IsSuccess)
            {
                ClearData(false, false, true);
                SearchData(1);
                //存储日志
                commonBal.SaveMESAuditLog(ml);
            }
            ShowStatusMessage(re.Message, re.IsSuccess);
        }
        catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }


    /// <summary>
    /// 提交数据
    /// </summary>
    /// <returns></returns>
    ResultModel SaveData()
    {
        ResultModel re = new ResultModel(false, "");
        //存入数据
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strEmployeeID = userInfo["EmployeeID"];

        Dictionary<string, string> para = new Dictionary<string, string>();
        para["menuname"] = txtMenuName.Text;
        para["CreateEmployeeID"] = strEmployeeID;
        para["CreateDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");


        //子菜单
        DataTable dtMenuchild = new DataTable();
        dtMenuchild.Columns.Add("menuchildname");
        dtMenuchild.Columns.Add("url");
        dtMenuchild.Columns.Add("sequence");
        for (int i = 0; i < gdMenuChid.Rows.Count; i++)
        {
            DataRow row = dtMenuchild.NewRow();
            row["menuchildname"] = gdMenuChid.Rows[i].Cells.FromKey("menuchildname").Value.ToString();
            row["url"] = gdMenuChid.Rows[i].Cells.FromKey("url").Value.ToString();
            row["sequence"] =(i+1).ToString();
            dtMenuchild.Rows.Add(row);
        }

        dtMenuchild = dtMenuchild.Rows.Cast<DataRow>().OrderBy(t => t["Sequence"]).CopyToDataTable();
        re = MenuBusiness.SaveMenuInfo(para, dtMenuchild);

        if (re.IsSuccess)
            re.Message = "保存成功";
        return re;

    }

    ResultModel UpdateData(string strid)
    {
        ResultModel re = new ResultModel(false, "");
        UltraGridRow activeRow = gdMenu.DisplayLayout.ActiveRow;
        if (activeRow != null)
        {
            string strID = activeRow.Cells.FromKey("id").Text;
            //存入数据
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> para = new Dictionary<string, string>();
            para["menuname"] = txtMenuName.Text;
            para["mainid"] = strID;

            //子菜单
            DataTable dtMenuchild = new DataTable();
            dtMenuchild.Columns.Add("menuchildname");
            dtMenuchild.Columns.Add("url");
            dtMenuchild.Columns.Add("sequence");
            dtMenuchild.Columns.Add("id");
            for (int i = 0; i < gdMenuChid.Rows.Count; i++)
            {
                DataRow row = dtMenuchild.NewRow();
                row["menuchildname"] = gdMenuChid.Rows[i].Cells.FromKey("menuchildname").Text.ToString();
                row["url"] = gdMenuChid.Rows[i].Cells.FromKey("url").Text.ToString();
                row["sequence"] = (i + 1).ToString();
                if (gdMenuChid.Rows[i].Cells.FromKey("id").Value !=null)
                {
                    row["id"] = gdMenuChid.Rows[i].Cells.FromKey("id").Text.ToString();
                }
                else
                {
                    row["id"] = "";
                }
               
                dtMenuchild.Rows.Add(row);
            }
            dtMenuchild = dtMenuchild.Rows.Cast<DataRow>().OrderBy(t => t["Sequence"]).CopyToDataTable();
            re = MenuBusiness.UpdateMenu(para, dtMenuchild);

            //if (re.IsSuccess)
            //{ 
            //    SearchData(1);
            //    ClearData(false, false, true);
            //}
        }

        ShowStatusMessage(re.Message, re.IsSuccess);

        if (re.IsSuccess)
            re.Message = "保存成功";
        return re;
    }
    #endregion

    #region 删除菜单 
    protected void btnDel_Click(object sender, EventArgs e)
    {
        try
        {
            ResultModel re = DeleteData();
            if (re.IsSuccess)
            {
                SearchData(1);
                ClearData(false, false, true);
            }
            ShowStatusMessage(re.Message, re.IsSuccess);

        }
        catch (Exception ex) { ShowStatusMessage(ex.Message, false); }
    }

    /// <summary>
    /// 删除数据
    /// </summary>
    /// <returns></returns>
    ResultModel DeleteData()
    {
        ResultModel re = new ResultModel(false, "");
        UltraGridRow activeRow = gdMenu.DisplayLayout.ActiveRow;
        if (activeRow == null)
        {
            re.Message = "请选择需要删除的菜单";
            return re;
        }
        string strID = activeRow.Cells.FromKey("id").Text;
        DataTable dt = MenuBusiness.GetMenuRole(strID);
        if (dt.Rows.Count > 0)
        {
            re.Message = "该菜单已经与角色关联不能删除！";
            return re;
        }

        re.IsSuccess = MenuBusiness.DelMenu(strID);

        if (re.IsSuccess)
        {
            re.Message = "删除成功";

            MESAuditLog ml = new MESAuditLog();
            #region 日志赋值
            ml.ParentID = strID;
            ml.CreateEmployeeID = UserInfo["EmployeeID"]; ml.ParentName = "menu";
            ml.BusinessName = "系统配置"; ml.OperationType = 2;
            ml.Description = "菜单删除:" + txtMenuName.Text;
            commonBal.SaveMESAuditLog(ml);
            #endregion
        }
        return re;
    }
    #endregion 

    /// <summary>
    /// 菜单行上移
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnMoveUp_Click(object sender, EventArgs e)
    {
        try
        {     
            string strMessage = string.Empty;
            GridRowMove("Up",out strMessage);

            if (strMessage !=null)
            {
                ShowStatusMessage(strMessage,false);
            }
            //ShowStatusMessage("", false);
        }
        catch( Exception ex)
        {
            ShowStatusMessage(ex.Message,false);
        }
    }

    /// <summary>
    /// 菜单行下移
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnMoveDown_Click(object sender, EventArgs e)
    {
        try
        {
            string strMessage = string.Empty;
            GridRowMove("Down", out strMessage);

            if (strMessage != null)
            {
                ShowStatusMessage(strMessage, false);
            }
            //ShowStatusMessage("", false);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    void GridRowMove(string strType,out string strMessage)
    {
        strMessage = string.Empty;
       // int intTemp = 0;
        int intSelectRow =-1;
        ////Infragistics.WebUI.UltraWebGrid.TemplatedColumn temCell = (TemplatedColumn)gdMenuChid.Columns.FromKey("ckSelect");
        //for (int i = 0; i < gdMenuChid.Rows.Count; i++)
        //{
        //    //Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
        //    //CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
        //    //if (ckSelect.Checked == true)
        //    //{
        //    //    intTemp += 1;
        //    //    if (intSelectRow == -1)
        //    //    {
        //    //        intSelectRow = i;
        //    //    }
        //    //}
        //    //string str1 = string.Empty;
        //    //str1 = gdMenuChid.Rows[i].Cells.FromKey("select").Value.ToString();
        //    if (gdMenuChid.Rows[i].Cells.FromKey("select").Value.ToString() == "true")
        //    {
        //        intTemp += 1;
        //        if (intSelectRow == -1)
        //        {
        //            intSelectRow = i;
        //        }
        //    }

        //}
        //if (intTemp == 0)
        //{
        //    strMessage = "请选择要移动的行！";
        //    return;
        //}

        //if (intTemp >1)
        //{
        //    strMessage = "请选择一行进行移动！";
        //    return;
        //}

        UltraGridRow activeRow = gdMenuChid.DisplayLayout.ActiveRow;
        intSelectRow = activeRow.Index;
        if (activeRow == null)
        {
            strMessage = "请选择要移动的行！";
            return;
        }


        int intAfterIndex =0;
        if (strType =="Up")
        {
            if (intSelectRow ==0)
            {
                strMessage = "当前选中为第一行，无法进行上移！！";
                return;
            }

            intAfterIndex = intSelectRow - 1;
        }
        else if(strType == "Down")
        {
            if (intSelectRow == gdMenuChid.Rows.Count-1)
            {
                strMessage = "当前选中为最后一行行，无法进行下移！！";
                return;
            }

            intAfterIndex = intSelectRow + 1;
        }

        for (int j=0;j<gdMenuChid.Columns.Count-1;j++)
        {
            string strValue = string.Empty;
            if (!string.IsNullOrEmpty(gdMenuChid.Rows[intSelectRow].Cells[j].Text))
            {
                strValue = gdMenuChid.Rows[intSelectRow].Cells[j].Text;
            }

            if (!string.IsNullOrEmpty(gdMenuChid.Rows[intAfterIndex].Cells[j].Text))
            {
                gdMenuChid.Rows[intSelectRow].Cells[j].Text = gdMenuChid.Rows[intAfterIndex].Cells[j].Text;
            }

            gdMenuChid.Rows[intAfterIndex].Cells[j].Text = strValue;
        }



        //Infragistics.WebUI.UltraWebGrid.CellItem cellItem1 = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[intAfterIndex];
        //CheckBox ckSelect1 = (CheckBox)cellItem1.FindControl("ckSelect");
        //ckSelect1.Checked = true;

        //Infragistics.WebUI.UltraWebGrid.CellItem cellItem2 = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[intSelectRow];
        //CheckBox ckSelect2 = (CheckBox)cellItem2.FindControl("ckSelect");
        //ckSelect2.Checked = false;

        //gdMenuChid.Rows[intAfterIndex].Cells.FromKey("select").Value = true;
        //gdMenuChid.Rows[intSelectRow].Cells.FromKey("select").Value = false;
        gdMenuChid.DisplayLayout.ActiveRow = gdMenuChid.Rows[intAfterIndex];
        gdMenuChid.Rows[intAfterIndex].Selected = true;
        gdMenuChid.Rows[intAfterIndex].Activated = true;



    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        try
        {
            ClearData(true,true,true);
        }
        catch(Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
}