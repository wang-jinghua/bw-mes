﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileDataPopupForm.aspx.cs" Inherits="FileDataPopupForm" %>

<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>页面信息</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
    <script>
        function closeDialog() {
            window.returnValue = true;
            window.close();
        }
        window.onbeforeunload = function () {
            var n = window.event.screenX - window.screenLeft;
            var b = n > document.documentElement.scrollWidth - 20;
            if (b && window.event.clientY < 0 || window.event.altKey) {
                window.returnValue = false;
                window.close();
            }
        }
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 57%;
        }

        .auto-style2 {
            z-index: 200;
        }

        .auto-style3 {
            height: 64px;
            width: 100%;
            margin-top: 428px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="height: 8px; width: 100%;">
                    <table class="auto-style1" cellpadding="5" cellspacing="0">
                        <tr>
                            <td style="width: 500px;">
                                <igtbl:UltraWebGrid ID="gdMenu" runat="server" Height="368px" Width="1011px" OnActiveRowChange="gdMenu_ActiveRowChange1">
                                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdMenu"
                                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy"
                                        RowHeightDefault="18px">
                                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="368px" Width="1011px">
                                        </FrameStyle>
                                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                        </RowAlternateStyleDefault>
                                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                            StyleMode="QuickPages">
                                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                            </PagerStyle>
                                        </Pager>
                                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                        </EditCellStyleDefault>
                                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </FooterStyleDefault>
                                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                            <Padding Top="2px" Bottom="3px"></Padding>
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </HeaderStyleDefault>
                                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                            <Padding Left="3px" />
                                        </RowSelectorStyleDefault>
                                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                            <Padding Left="3px" />
                                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                        </RowStyleDefault>
                                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                        </GroupByRowStyleDefault>
                                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                        </SelectedRowStyleDefault>
                                        <GroupByBox Hidden="True">
                                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                            </BoxStyle>
                                        </GroupByBox>
                                        <AddNewBox>
                                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                            </BoxStyle>
                                        </AddNewBox>
                                        <ActivationObject BorderColor="" BorderWidth="">
                                        </ActivationObject>
                                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="11px" Height="420px" Width="200px">
                                                <Padding Left="2px" />
                                            </FilterDropDownStyle>
                                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                            </FilterHighlightRowStyle>
                                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                Font-Size="11px">
                                                <Padding Left="2px" />
                                            </FilterOperandDropDownStyle>
                                        </FilterOptionsDefault>
                                    </DisplayLayout>
                                    <Bands>
                                        <igtbl:UltraGridBand>
                                            <Columns>
                                                <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="Url" Key="Url" Width="950px">
                                                    <Header Caption="路径">
                                                    </Header>
                                                </igtbl:UltraGridColumn>
                                            </Columns>
                                            <AddNewRow View="NotSet" Visible="NotSet">
                                            </AddNewRow>
                                        </igtbl:UltraGridBand>
                                    </Bands>
                                </igtbl:UltraWebGrid>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td style="text-align: right; width: 50%;">
                                            <asp:LinkButton ID="lbtnFirst8" runat="server" Style="z-index: 200; font-size: 13px;"
                                                OnClick="lbtnFirst8_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnPrev8" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnPrev8_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnNext8" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnNext8_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnLast8" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnLast8_Click">尾页</asp:LinkButton>&nbsp;
                            <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                                Text="第  页  共  页"></asp:Label>
                                            <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                                            <asp:TextBox ID="txtPage8" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                                            <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                                            <asp:Button ID="btnGo8" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                                                OnClick="btnGo8_Click" />
                                            <asp:Label ID="Label1" runat="server" Style="z-index: 200; font-size: 13px;" Text="当前页显示"></asp:Label>
                                            <asp:TextBox ID="txtPageSize8" AutoPostBack="true" Enabled="false" Text="10" runat="server" Style="z-index: 200; width: 30px;"></asp:TextBox>
                                            <asp:Label ID="Label2" runat="server" Style="z-index: 200; font-size: 13px;" Text="条"></asp:Label>
                                            <asp:TextBox ID="txtTotalPage8" runat="server" Style="z-index: 200; width: 30px;"
                                                Visible="False"></asp:TextBox>
                                            <asp:TextBox ID="txtCurrentPage8" runat="server"
                                                Visible="False" CssClass="auto-style2" Height="25px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <table style="width: 100%;">
                        <tr>
                            <td style="text-align: left; width: 100%;" colspan="2">
                                <asp:Button ID="btnSave" runat="server" Text="提交" Font-Size="11pt"
                                    CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" Width="72px" />
                                &nbsp  
                   <asp:Button ID="btnClose" runat="server" Text="关闭" Font-Size="11pt"
                       CssClass="searchButton" EnableTheming="True" OnClientClick="closeDialog()" Width="72px" OnClick="btnClose_Click" />
                                <asp:TextBox ID="txtUrl" runat="server" Width="200" Height="20px" Visible="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 12px; font-weight: bold;" nowrap="nowrap">状态信息：</td>
                            <td style="text-align: left; width: 100%;">
                                <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
                            </td>
                        </tr>
                    </table>
                            </td>
                        </tr>
                    </table>
                </div>

                <div style="height: 8px; width: 100%;">
                   
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
