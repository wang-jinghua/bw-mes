﻿<%@ Page Title="" Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="MenuManagerForm.aspx.cs" Inherits="Custom_BaseManage_MenuManagerForm" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>
<asp:Content ContentPlaceHolderID="HeaderContent"  runat="Server">
        
    <script type="text/javascript">
        function FileData() {
            var url = "FileDataPopupForm.aspx";
            url = encodeURI(url)
            window.showModalDialog(url, window, "dialogWidth=1090px;dialogHeight=530px");
        }
        function IsDel()
        {
            Debug;
            if(confirm("确定要删除该菜单吗？"))
            {
              
                return true;
            }
            else
            {
                document.getElementById("btnDel").click();
                return false;
            }
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" style="width:98%">   
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdRight" style="width:10%">
                        <div class="divLabel">菜单名称：</div>
                        <asp:TextBox ID="txtSearchMenuName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" style="display:none;" class="tdRight">
                        <asp:TextBox ID="txtSearchMenuChidName" runat="server" class="stdTextBox" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdNoBorder" style="width:90%">
                        <asp:Button ID="btnSearch" runat="server" Text="查询" Width ="60px"  font-size ="12pt"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"  Width ="60px"  font-size ="12pt"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>

                </tr>
            </table>
        </div>
        <div id="ItemDiv" runat="server" style="width:700px">
            <igtbl:UltraWebGrid ID="gdMenu" runat="server" Height="208px" Width="60%" OnActiveRowChange="gdgdMenu_ActiveRowChange">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>

                            <igtbl:UltraGridColumn Key="menuname" Width="300px" BaseColumnName="menuname" AllowGroupBy="No">
                                <Header Caption="主菜单名称">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="menuchildname" Key="menuchildname" Width="200px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="子菜单名称">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="url" Key="url" Width="550px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="地址">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="fullname" Key="fullname" Width="150px" AllowGroupBy="No">
                                <Header Caption="创建人">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="createdate" BaseColumnName="createdate" Width="180px" Format="yyyy-MM-dd HH:mm:ss">
                                <Header Caption="创建时间">
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="seuqnece" BaseColumnName="seuqnece" Width="60px" Hidden="True">
                                <Header Caption="顺序号">
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>

                            <igtbl:UltraGridColumn BaseColumnName="id" Hidden="True" Key="id">
                                <Header Caption="子菜单id">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="mainid" Hidden="True" Key="mainid">
                                <Header Caption="主菜单id">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault ="ByDataIsland"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="150px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
            <div style="float:right">
                <uPT:pageTurning ID="upageTurning" runat="server" />
            </div>
            <div style="clear: both"></div>
        </div> 

        <div >
            <div style="width: 653px; float: left">
                <igtbl:UltraWebGrid ID="gdMenuChid" runat="server" Height="207px" Width="98%" OnActiveRowChange="gdMenuChid_ActiveRowChange">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
<%--                                <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect">
                                    <CellTemplate>
                                        <asp:CheckBox ID="ckSelect" runat="server" />
                                    </CellTemplate>
                                    <Header Caption="选择"></Header>
                                </igtbl:TemplatedColumn>--%>
                                <igtbl:UltraGridColumn BaseColumnName="select" Key="select" Width="40px" Type="CheckBox" AllowUpdate="Yes" Hidden="true">
                                    <Header Caption="选择">
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="menuchildname" Key="menuchildname" Width="120px">
                                    <Header Caption="子菜单名称">
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="url" Key="url" Width="450px">
                                    <Header Caption="路径">
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="Sequence" Key="Sequence"  Width="60px" Hidden="true">
                                    <Header Caption="顺序号">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                                <igtbl:UltraGridColumn BaseColumnName="id" Key="id" Hidden="True">
                                    <Header Caption="id">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                                <igtbl:UltraGridColumn BaseColumnName="mainid" Key="mainid" Hidden="True">
                                    <Header Caption="mainid">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" RowSelectorsDefault="Yes"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"  AllowRowNumberingDefault="ByDataIsland"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both" AddNewRowDefault-View="Bottom"
                        RowHeightDefault="15px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="207px" Width="650px">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
                <div style="margin-right: 5px; text-align: right; float: left; width: 687px; margin-left: 10px; display:none;">
                    <asp:Button ID="btnSelectAll" runat="server" Text="全选"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnSelectAll_Click"  />
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <asp:Button ID="btnSelectNotAll" runat="server" Text="全不选"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSelectNotAll_Click"  />
                </div>
            
            </div>

            <div>
                <div style="padding-top:100px; "> 
                    <asp:Button ID="btnChildDel" runat="server" Text="删除" Width="71px" Height="27px" CssClass="searchButton" EnableTheming="True" OnClick="btnChildDel_Click"  />
                </div>
                <div style="padding-top:20px; "> 
                    <asp:Button ID="btnMoveUp" runat="server" Text="上移"  Width="71px" Height="27px"  CssClass="searchButton" EnableTheming="True" OnClick="btnMoveUp_Click" />
                </div>
                <div style=" padding-top:20px; "> 
                    <asp:Button ID="btnMoveDown" runat="server" Text="下移" Width="71px" Height="27px"  CssClass="searchButton" EnableTheming="True" OnClick="btnMoveDown_Click" />
                </div>
            </div>
            <div style="clear: both"  ></div>
            <div style="width: 100%;margin:5px" >
                <div style="float:left; ">
                    <div style ="font-size :11pt">主菜单名称</div>
                    <asp:TextBox ID="txtMenuName" runat="server" Width="200px" Height="20px"></asp:TextBox>
                </div>
                <div style="float:left; padding-left:10px;" >
                    <div style ="font-size :11pt">子菜单名称</div>
                    <asp:TextBox ID="txtMenuChilName" runat="server" Width="100px" Height="20px"></asp:TextBox>
                </div>
                <div style="float:left; padding-left:10px; ">
                    <div style ="font-size :11pt">路径</div>
                    <asp:TextBox ID="txtFile" runat="server" Width="173px" Height="20px" TextMode="Number"></asp:TextBox> 
                </div>
                <div style="float:left; padding-top:20px; padding-left:3px;">
                    <asp:Button ID="btnFile" runat="server" CssClass="ReportButton" UseSubmitBehavior="false" EnableTheming="True"
                        OnClick="btnFile_Click"  OnClientClick="FileData()" Text="选择" Width="60px" Height="27px" />
                </div>
                <div  style="float:left; padding-top:20px;  padding-left:10px; ">
                    <asp:Button ID="btnAdd" runat="server" Text="添加" CssClass="ReportButton" EnableTheming="True" Width="60px" Height="27px" OnClick="btnAdd_Click" />
                </div>
   <%--             <div style="float:left; padding-top:20px;  padding-left:10px; ">
                    <asp:Button ID="btnChildDel" runat="server" Text="删除" Width="60px" Height="27px" CssClass="ReportButton" EnableTheming="True" OnClick="btnChildDel_Click"  />
                </div>
                <div style="float:left; padding-top:20px;  padding-left:10px; ">
                    <asp:Button ID="btnMoveUp" runat="server" Text="上移"  Width="60px" Height="27px"  CssClass="ReportButton" EnableTheming="True" OnClick="btnMoveUp_Click" />
                </div>
                <div style="float:left; padding-top:20px;  padding-left:10px; ">
                    <asp:Button ID="btnMoveDown" runat="server" Text="下移" Width="60px" Height="27px"  CssClass="ReportButton" EnableTheming="True" OnClick="btnMoveDown_Click" />
                </div>--%>
            </div>

            <div style="clear: both"></div>
            <div style=" display:none;">
                <asp:TextBox ID="txtID" runat="server" Width="200" Height="20px" Visible ="false" ></asp:TextBox>
                <asp:TextBox ID="txtMainID" runat="server" Width="200" Height="20px" Visible ="false" ></asp:TextBox>
                <div style ="font-size :11pt">顺序号</div>
                <asp:TextBox ID="txtSequence" runat="server" Width="250px" Height="20px" TextMode="Number"></asp:TextBox> 
            </div>
            <div style="margin: 10px 5px">
                <asp:Button ID="btnSubmit" runat="server" Text="保存" 
                    CssClass="searchButton" EnableTheming="True" Style="margin-right: 0px" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnInsert" runat="server" Text="新增" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)"
                CssClass="searchButton" EnableTheming="True" Style="margin-left: 20px" OnClick="btnInsert_Click" />
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                  <asp:Button ID="btnDel" runat="server" Text="删除" UseSubmitBehavior="true"  OnClientClick=" return ((confirm('确认删除？') == true) ? true :  false) "
                      CssClass="searchButton" EnableTheming="True" Style="margin-left: 20px" OnClick="btnDel_Click" />
            </div>
      
        </div> 
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>

