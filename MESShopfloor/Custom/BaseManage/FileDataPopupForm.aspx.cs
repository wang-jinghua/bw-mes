﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Data;
using System.Drawing;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;

public partial class FileDataPopupForm : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
            //Session.Remove("FileData");
            DataTable dt = new DataTable();
            GetFileInfo(ref dt);
            Session["Data"]= dt; 
            getPageCount(dt.Rows.Count, 10);
            lbtnFirst8_Click(sender, e); 
        }
    }

    #region 选择文件 

    //获取所有的文件
    void GetFileInfo(ref DataTable dt)
    {
        dt.Columns.Add("Url");
        findRsmFile(Request.PhysicalApplicationPath, ref dt);
    }

    private void findRsmFile(string file, ref DataTable dt)
    {

        DirectoryInfo drInfo = new DirectoryInfo(file);

        //获取当前目录下所有以*.aspx结尾的文件

        FileInfo[] fi = drInfo.GetFiles();

        foreach (FileInfo f in fi)
        {

            if (f.Extension == ".aspx"||f.Extension==".html")
            {
                DataRow row = dt.NewRow();
                row["Url"] = f.FullName;
                dt.Rows.Add(row);
            }

        }

        //获取当前目录下所有子文件夹
        DirectoryInfo[] subDr = drInfo.GetDirectories();

        //遍历所有子文件夹
        foreach (DirectoryInfo subDir in drInfo.GetDirectories())
        {
            string subFile = subDir.FullName + @"\";

            //递归
            findRsmFile(subFile, ref dt);
        }
    }


    #endregion

    #region 选中事件
    protected void gdMenu_ActiveRowChange1(object sender, RowEventArgs e)
    {
        txtUrl.Text = e.Row.Cells.FromKey("Url").Text;
    }
    #endregion

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }
    #endregion

    #region 提交按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);
        try
        {
            if (txtUrl.Text == "")
            {
                ShowStatusMessage("请选择一个页面！", false);
                return;
            }
            Session["FileData"] = txtUrl.Text;
            ScriptManager.RegisterStartupScript(UpdatePanel3, this.Page.GetType(), "", "closeDialog();", true);
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }

    }
    #endregion

    #region 翻页

    //首页
    protected void lbtnFirst8_Click(object sender, EventArgs e)
    {
        DataTable dt = Session["Data"] as DataTable; 
        txtCurrentPage8.Text = Convert.ToString(FirstPage());
        GetPagedTable(dt, Convert.ToInt32(txtCurrentPage8.Text), 10);
    }
    //上一页
    protected void lbtnPrev8_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCurrentPage8.Text))
        {
            return;
        }
        string stringinfo = "";
        DataTable dt = Session["Data"] as DataTable;
        txtCurrentPage8.Text = Convert.ToString(PrevPage(Convert.ToInt32(txtCurrentPage8.Text), Convert.ToInt32(txtTotalPage8.Text), ref stringinfo));
        GetPagedTable(dt, Convert.ToInt32(txtCurrentPage8.Text), 10);
    }
    //下一页
    protected void lbtnNext8_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCurrentPage8.Text))
        {
            return;
        }
        string stringinfo = "";
        DataTable dt = Session["Data"] as DataTable;
        txtCurrentPage8.Text = Convert.ToString(NextPage(Convert.ToInt32(txtCurrentPage8.Text), Convert.ToInt32(txtTotalPage8.Text), ref stringinfo));
        GetPagedTable(dt, Convert.ToInt32(txtCurrentPage8.Text), 10);
    }
    //末页
    protected void lbtnLast8_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCurrentPage8.Text))
        {
            return;
        }
        string stringinfo = "";
        DataTable dt = Session["Data"] as DataTable;
        txtCurrentPage8.Text = Convert.ToString(LastPage(Convert.ToInt32(txtTotalPage8.Text), ref stringinfo));
        GetPagedTable(dt, Convert.ToInt32(txtCurrentPage8.Text), 10);
    }
    //转到第几页
    protected void btnGo8_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCurrentPage8.Text))
        {
            return;
        }
        if (txtPage8.Text.Trim() == "")
        {
            return;
        }
        try
        {
            Convert.ToInt32(txtPage8.Text.Trim());
        }
        catch (Exception)
        {
            txtPage8.Text = "";
            return;
        }
        string stringinfo = "";
        DataTable dt = Session["Data"] as DataTable;
        txtCurrentPage8.Text = Convert.ToString(GoPage(Convert.ToInt32(txtPage8.Text), Convert.ToInt32(txtTotalPage8.Text), ref stringinfo));
        GetPagedTable(dt, Convert.ToInt32(txtPage8.Text), 10);

    }

    //上一页
    public static int PrevPage(int CurrentPage, int TotalPage, ref string strInfo)
    {
        if (TotalPage < 1)
        {
            strInfo = "总页数小于1";
            return -1;
        }
        CurrentPage--;
        if (CurrentPage < 1)
        {
            return 1;
        }
        return CurrentPage;
    }

    //下一页
    public static int NextPage(int CurrentPage, int TotalPage, ref string strInfo)
    {
        if (TotalPage < 1)
        {
            strInfo = "总页数小于1";
            return -1;
        }

        if (CurrentPage < 1)
        {
            strInfo = "当前页数小于1";
            return -1;
        }
        CurrentPage++;
        if (CurrentPage > TotalPage)
        {
            return TotalPage;
        }
        return CurrentPage;
    }
    //首页
    public static int FirstPage()
    {
        return 1;
    }

    //尾页
    public static int LastPage(int TotalPage, ref string strInfo)
    {
        if (TotalPage < 1)
        {
            strInfo = "总页数小于1";
            return -1;
        }
        return TotalPage;
    }

    //跳转到某一页
    public static int GoPage(int CurrentPage, int TotalPage, ref string strInfo)
    {
        if (TotalPage < 1)
        {
            strInfo = "总页数小于1";
            return -1;
        }
        if (CurrentPage < 1)
        {
            strInfo = "当前页数小于1";
            return -1;
        }
        if (CurrentPage > TotalPage)
        {
            strInfo = "跳转页大于总页数";
            return -1;
        }
        return CurrentPage;
    }

    //分页主方法
    /// <summary>  
    /// 对DataTable进行分页,起始页为1  
    /// </summary>  
    /// <param name="dtData"></param>  
    /// <param name="PageIndex"></param>  
    /// <param name="PageSize"></param>  
    /// <returns></returns>  
    public void GetPagedTable(DataTable dtData, int PageIndex, int PageSize)
    {
        if (PageIndex == 0)
            return;
        DataTable newdt = dtData.Copy();
        newdt.Clear();

        int rowbegin = (PageIndex - 1) * PageSize;
        int rowend = PageIndex * PageSize;

        if (rowbegin >= dtData.Rows.Count)
            return;

        if (rowend > dtData.Rows.Count)
            rowend = dtData.Rows.Count;
        for (int i = rowbegin; i <= rowend - 1; i++)
        {
            DataRow newdr = newdt.NewRow();
            DataRow dr = dtData.Rows[i];
            foreach (DataColumn column in dtData.Columns)
            {
                newdr[column.ColumnName] = dr[column.ColumnName];
            }
            newdt.Rows.Add(newdr);
        }

        gdMenu.DataSource = newdt;
        gdMenu.DataBind();
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage8.Text, this.txtTotalPage8.Text);
        this.txtPage8.Text = this.txtCurrentPage8.Text;
    }

    /// <summary>  
    /// 获取总页数  
    /// </summary>  
    /// <param name="sumCount">结果集数量</param>  
    /// <param name="pageSize">页面数量</param>  
    /// <returns></returns>  
    public void getPageCount(int sumCount, int pageSize)
    {
        int page = sumCount / pageSize;
        if (sumCount % pageSize > 0)
        {
            page = page + 1;
        }
        txtTotalPage8.Text = Convert.ToString(page);
        return;
    }
    #endregion

    #region 关闭按钮 
    protected void btnClose_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel3, this.Page.GetType(), "", "closeDialog();", true);
    }
    #endregion
}