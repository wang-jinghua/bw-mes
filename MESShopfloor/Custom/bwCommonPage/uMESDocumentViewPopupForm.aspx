﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESDocumentViewPopupForm.aspx.cs"
  ValidateRequest="false" Inherits="uMESDocumentViewPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>
<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
		<title>文档查看</title>
		<base target="_self" />
        <style type="text/css">
            .astyle
            {
                font-size: 12px !important;
                height:10px !important;
                text-align: center !important;
                color:Blue !important;
             }
        </style>
        <script language="javascript" type="text/javascript">
                function OnViewClick() {
                    var grid = igtbl_getGridById("DocumentsField");
                    var row = igtbl_getRowById(grid.ActiveRow);
                    var cell = row.getCellFromKey('Url');
                    return OpenDocument(cell.getValue(), 'CantFindDocument', 'EmptyDocumentURLLabel')
                }
                function OnAfterRowActivate() {
                    var button = document.getElementById('ViewDocumentsPopupLink');
                    button.disabled = false;
                }

                function ViewDocument(docURL, cantFindLabel, emptyURLLabel) {
                    return OpenDocument(docURL, cantFindLabel, emptyURLLabel);
                }

    </script>
        <script language="javascript" type="text/javascript">
            function openDocUrl(value) {
                //                window.open("http://10.15.7.76/InSiteWebApplication/" + encodeURIComponent(value));
                window.open("http://172.31.0.10/MESShopfloor/Temp/" + encodeURIComponent(value));
            }
            function keepSrollBar() {
                var scrollPos;
                if (typeof window.pageYoffset != 'undefined') { scrollPos = window.pageYoffset; }
                else if (typeof document.body != 'undefined') { scrollPos = document.getElementById('tvTree').scrollTop; }
                document.cookie = "scrollTop=" + scrollPos;
            }
            window.onload = function () {
                if (document.cookie.match(/scrollTop=([^;]+)(;|$)/) != null) {
                    var arr = document.cookie.match(/scrollTop=([^;]+)(;|$)/);
                    document.getElementById('tvTree').scrollTop = parseInt(arr[1]);
                    //将cookie设置为失效
                    var date = new Date();
                    date.setTime(date.getTime() + 1); //失效时间为1毫秒
                    document.cookie = "scrollTop=" + parseInt(arr[1]) + ";expires=" + date.toGMTString();
                } 
            }
            
        </script>
</head>
<body Scroll = "no">
   <form id="formControl" method="post" runat="server">
			<div style="width: 99%;height:470px; margin: 3px 0px 3px 3px; position: relative;">
                <div style="position: relative; float: left;display:none">
                    <gPI:getProductInfo ID="getProductInfo" runat="server" />
                </div>
                <div style="float:left;Width:300px;Height:100%;">
                <asp:Label ID="Label1" runat="server" Text ="产品工艺工序" ></asp:Label>
                <div style="position:relative ;float:left;">
                    <asp:TreeView ID="tvTree" runat="server" Height="470px" Width="300px" ShowLines="True"
                        BorderWidth="1px" Style="z-index: 100; left: 0px; position: relative; top: 0px;overflow:auto 
                        " NodeWrap="True" BorderStyle="Notset"  onselectednodechanged="tvTree_SelectedNodeChanged" >
                    </asp:TreeView> 
                </div>
                <div style="clear:both"></div>
                <div style="width:300px;text-align: left; position: relative; left: 0px;top:0px;display:none;">
                    <uPT:pageTurning ID="upageTurning" runat="server" />
                </div>
                </div>
                <div style="float:left;margin-left:10px;width:350px;height:470px">
                    <asp:Label ID="Label2" runat="server" Text ="文档信息" ></asp:Label>
                    <igtbl:UltraWebGrid ID="wgDoc" runat="server" ColumnLabelsType="Default" Height="450px" 
                        LabelsType="Designer" RowsPerPage="6" Width="100%"   OnClickCellButton="wgDoc_ClickCellButton">
                        <Bands>
                            <igtbl:UltraGridBand>
                               <columns>
                                 <igtbl:UltraGridColumn BaseColumnName="documentname" key="DocumentName" headerText="文档名称" width="200px" >
                                   </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="filepath" key="FilePath" headerText="文档地址" width="120px" hidden="true" >
                                 </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="type" key="Type" headerText="文档存储类型" width="120px" hidden="true">
                                 </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="docName" key="docName" headerText="文档名称" width="120px" hidden="true">
                                 </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="attacheddocid" key="attacheddocid" headerText="attacheddocid" width="120px" hidden="true">
                                 </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="documentrevision" key="documentrevision" headerText="文档版本" width="120px" hidden="true">
                                 </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="" key="BtnDoc" Type="Button" nulltext="查看文档" headerText="查看文档" width="80px" CellButtonDisplay="Always"  >
                                    <cellbuttonstyle cursor="Hand"></cellbuttonstyle>
                                 </igtbl:UltraGridColumn>
                               </columns>
                              <addnewrow visible="NotSet" view="NotSet"></addnewrow>
                            </igtbl:UltraGridBand>
                         </Bands>
                        <DisplayLayout BorderCollapseDefault="Separate" Name="wgContainerInfo" OptimizeCSSClassNamesOutput="False"
                        RowHeightDefault="24px" StationaryMargins="Header" TableLayout="Fixed" Version="4.00"
                        AllowSortingDefault="OnClient" AutoGenerateColumns="False" CellClickActionDefault="CellSelect"
                        SelectTypeRowDefault="Single" AllowColSizingDefault="Free" HeaderClickActionDefault="SortSingle">
                        <FrameStyle BorderColor="#A5ACB2" BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana"
                            Font-Size="8pt" Height="450px" Width="100%">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#86DFD9" CssClass="GridRowAlternateStyle">
                            <Padding Left="0px"></Padding>
                        </RowAlternateStyleDefault>
                        <Pager PageSize="6">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <HeaderStyleDefault BackColor="LightGray" BorderStyle="Solid" CssClass="GridHeaderStyle"
                            Height="100%" Wrap="True">
                            <Padding Top="2px" Bottom="3px"></Padding>
                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault>
                            <Padding Left="0px"></Padding>
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"
                            CssClass="GridRowStyle" Font-Names="Verdana" Font-Size="8pt">
                            <Padding Left="3px"></Padding>
                            <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                        </RowStyleDefault>
                        <SelectedRowStyleDefault CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <AddNewBox>
                            <BoxStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White"></BorderDetails>
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                    </DisplayLayout>
                    </igtbl:UltraWebGrid>
                </div>
            </div> 
        <div style="Z-INDEX: 102;position: relative;left:5px;top:30px">
            <div style=" float:left;">状态信息：</div>
            
            <div style=" padding-left:300px;">
                <asp:Button ID="btnClose" runat="server" Text="关闭" Height="22px"
                            Width="72px" CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
            </div>
            <div style=" clear:both;"></div>
        </div>
        <div style="Z-INDEX: 102;position: relative;left:5px;top:3px"><asp:Label ID="StatusMessage" runat="server" Width="100%"></asp:Label></div>

    </form>
</body>
</html>
