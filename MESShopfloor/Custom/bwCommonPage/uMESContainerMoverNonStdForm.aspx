﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="uMESContainerMoverNonStdForm.aspx.cs" 
        Inherits="uMESContainerMoverNonStdForm" EnableViewState="true" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdRight" style="width: 263px">
                        <div style="width: 48%; height: 27px;">
                            <asp:Label ID="Label1" runat="server" Text=" 扫描" Font-Bold="true" Font-Size="14pt"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged" Width="270px"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 148px">
                        <div style="width: 100%;">
                            <asp:Label ID="Label2" runat="server" Text=" 工作令号" Font-Bold="true" Font-Size="12pt"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 136px">
                        <div style="width: 100%;">
                            <asp:Label ID="Label4" runat="server" Text=" 批次号" Font-Bold="true" Font-Size="12pt"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 150px">
                        <div style="width: 100%;">
                            <asp:Label ID="Label5" runat="server" Text=" 图号/名称" Font-Bold="true" Font-Size="12pt"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 269px">
                        <div style="width: 50%;">
                            <asp:Label ID="Label6" runat="server" Text="计划开始日期" Font-Bold="true" Font-Size="12pt"></asp:Label>
                        </div>
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                        <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td align="left" valign="bottom" class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 8px; width: 100%;"></div>
        <div id="gridDiv" runat="server" style="margin: 5px;">
            <div>
                <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="220px" Width="100%" OnActiveRowChange="ItemGrid_ActiveRowChange">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                    <CellTemplate>
                                        <asp:CheckBox ID="ckSelect" runat="server" />
                                    </CellTemplate>
                                    <Header Caption=""></Header>
                                </igtbl:TemplatedColumn>
                                <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                    <Header Caption="工作令号">
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="150px" AllowGroupBy="No" Hidden="true">
                                    <Header Caption="作业令号">
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                    <Header Caption="批次号">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                    <Header Caption="图号">
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="productrevision" Key="ProductRevision" Width="100px" AllowGroupBy="No" hidden="false">
                                    <Header Caption="图号版本">
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                    <Header Caption="名称">
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="conqty" Key="conqty" Width="80px" AllowGroupBy="No">
                                    <Header Caption="批次数量">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="currentFlow" Key="currentFlow" Width="150px" AllowGroupBy="No">
                                    <Header Caption="当前工艺路线">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="SpecNameDisp" Key="SpecNameDisp" Width="100px" AllowGroupBy="No">
                                    <Header Caption="当前工序">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                  <igtbl:UltraGridColumn BaseColumnName="productchangeDesc" Key="productchangeDesc" Width="120px" AllowGroupBy="No">
                                    <Header Caption="产品变更描述">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                 <igtbl:UltraGridColumn BaseColumnName="workflowchangeDesc" Key="workflowchangeDesc" Width="120px" AllowGroupBy="No">
                                    <Header Caption="工艺变更描述">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                    <Header Caption="ContainerID">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="productid" Hidden="True" Key="productid">
                                    <Header Caption="productid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="workflowid" Hidden="true" Key="workflowid">
                                    <Header Caption="workflowid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="specid" Hidden="True" Key="specid">
                                    <Header Caption="specid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="sequence" Hidden="True" Key="sequence">
                                    <Header Caption="StockQty">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                  <igtbl:UltraGridColumn BaseColumnName="WorkflowStepID" Hidden="True" Key="WorkflowStepID">
                                    <Header Caption="WorkflowStepID">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="220px" Width="100%">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
            </div>
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                            <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                                Text="第  页  共  页"></asp:Label>
                            <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                            <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                            <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                            <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                                OnClick="btnGo_Click" />
                            <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>

            <div style="padding-top: 5px; float: left">
                
                <igtbl:UltraWebGrid ID="wgProduct" runat="server" Height="180px" Width="300px" >
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:UltraGridColumn Key="ckSelect" Type="CheckBox" Width="40px" AllowUpdate="Yes">
                                        </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Hidden="false"
                                    Width="150px">
                                    <Header Caption="产品图号">
                                        <RowLayoutColumnInfo OriginX="1" />
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1" />
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="productrevision" Key="ProductRevision" Hidden="false"
                                    Width="60px">
                                    <Header Caption="版本">
                                        <RowLayoutColumnInfo OriginX="2" />
                                        <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                        <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductId"
                                    Key="ProductId" Width="100px" Hidden="true">
                                    <Header Caption="ProductId">
                                        <RowLayoutColumnInfo OriginX="7" />
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="7" />
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="180px" Width="300px">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
            </div>

            <div style="padding-top: 5px; float: left">
                <igtbl:UltraWebGrid ID="wgWorkFlow" runat="server" Height="180px" Width="300px" OnActiveRowChange="wgWorkFlow_ActiveRowChange">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Hidden="true"
                                    Width="150px">
                                    <Header Caption="产品图号">
                                        <RowLayoutColumnInfo OriginX="1" />
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1" />
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="designrevision" Key="designrevision" Hidden="true"
                                    Width="60px">
                                    <Header Caption="版本">
                                        <RowLayoutColumnInfo OriginX="2" />
                                        <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                        <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="workflowname2" Key="workflowname2" Width="180px">
                                    <Header Caption="工艺规程">
                                        <RowLayoutColumnInfo OriginX="3" />
                                        <RowLayoutColumnInfo OriginX="3"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                        <RowLayoutColumnInfo OriginX="3"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="WorkFlowRevision" Key="WorkFlowRevision" Width="60px">
                                    <Header Caption="版本">
                                        <RowLayoutColumnInfo OriginX="4" />
                                        <RowLayoutColumnInfo OriginX="4"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                        <RowLayoutColumnInfo OriginX="4"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="WorkFlowId"
                                    Key="WorkFlowId" Width="100px" Hidden="true">
                                    <Header Caption="WorkFlowId">
                                        <RowLayoutColumnInfo OriginX="6" />
                                        <RowLayoutColumnInfo OriginX="6"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                        <RowLayoutColumnInfo OriginX="6"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductId"
                                    Key="ProductId" Width="100px" Hidden="true">
                                    <Header Caption="ProductId">
                                        <RowLayoutColumnInfo OriginX="7" />
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="7" />
                                        <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="180px" Width="300px">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
            </div>
            <div style="padding-top: 5px; padding-left: 20px; float: left;">
                <igtbl:UltraWebGrid ID="wgSpec" runat="server" Height="180px" Width="200px">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:UltraGridColumn BaseColumnName="SpecNameDisp" Key="SpecNameDisp" Width="120px">
                                    <Header Caption="工序">
                                    </Header>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="specname" Key="specname" Hidden="true"
                                    Width="150px">
                                    <Header Caption="工序">
                                    </Header>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="workflowstepname" Key="workflowstepname"
                                    Width="150px" Hidden="true">
                                    <Header Caption="工序">
                                    </Header>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="180px" Width="200px">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
            </div>

        </div>
        <div style="height: 8px; width: 100%;"></div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 20%;" colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="调度工艺"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
                          <asp:Button ID="btnSaveProduct" runat="server" Text="调度产品"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSaveProduct_Click" />
                        <asp:Button ID="Button1" runat="server" Text="图纸工艺查看"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
