﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using uMES.LeanManufacturing.ReportBusiness;
using System.Text.RegularExpressions;

public partial class Custom_bwCommonPage_PasswordPopupForm : System.Web.UI.Page
{
    private uMESLoginBusiness ulb = new uMESLoginBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strOprEmployeeID = userInfo["EmployeeID"];
            string strEmployeeName = userInfo["EmployeeName"];
            string strPassword = userInfo["Password"];

            txtUserName.Text = strEmployeeName;
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }
    #endregion

    protected void btnSave_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {
            string strEmployeeName = txtUserName.Text;
            if (strEmployeeName == string.Empty)
            {
                ShowStatusMessage("“登录名”不能为空", false);
                return;
            }

            string strPWD_old = txtPassword_Old.Text.Trim();
            if (strPWD_old == string.Empty)
            {
                ShowStatusMessage("请输入“旧密码”", false);
                return;
            }

            string strPWD = txtPassword.Text.Trim();
            if (strPWD == string.Empty)
            {
                ShowStatusMessage("请输入“新密码”", false);
                return;
            }

            string strPWD2 = txtPassword2.Text.Trim();
            if (strPWD2 == string.Empty)
            {
                ShowStatusMessage("请输入“确认新密码”", false);
                return;
            }

            if (strPWD2 != strPWD)
            {
                ShowStatusMessage("两次输入的新密码不相符，请重新输入", false);
                return;
            }

            if (strPWD.Length > 16)
            {
                ShowStatusMessage("密码最大长度16位，请确认", false);
                return;
            }

            //判断密码复杂度
            string reg = @"(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,16}";
            string result = new Regex(reg).Match(strPWD).Value;

            if (result != string.Empty)
            {
                bool bol = ulb.Vertification(strEmployeeName, strPWD_old);

                if (bol)
                {
                    if (ulb.UpdatePassord(strEmployeeName, strPWD))
                    {
                        ShowStatusMessage("密码修改成功", true);
                    }
                    else
                    {
                        ShowStatusMessage("密码修改时遇到问题，请联系管理员", true);
                    }
                }
                else
                {
                    ShowStatusMessage("输入的旧密码不正确，请重新输入", false);
                }
            }
            else
            {
                ShowStatusMessage("密码不符合复杂度要求", false);
                return;
            }
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
}