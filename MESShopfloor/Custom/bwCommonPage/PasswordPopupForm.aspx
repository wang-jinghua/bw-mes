﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PasswordPopupForm.aspx.cs" Inherits="Custom_bwCommonPage_PasswordPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>修改密码</title>
    <base target="_self" />
    <link href="../../styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="5" cellspacing="0" align="center" valign="middle">
            <tr>
                <td>登录名：</td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>原密码：</td>
                <td>
                    <asp:TextBox ID="txtPassword_Old" runat="server" TextMode="Password" class="stdTextBox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>新密码：</td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" class="stdTextBox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>确认新密码：</td>
                <td>
                    <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" class="stdTextBox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font:6px; color:#FF0000;" align="left">
                    密码复杂度要求：8-16位,包含字母、数字、特殊字符
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnClose" runat="server" Text="关闭"
                        CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
                </td>
            </tr>
        </table>
    </div>
    <div style="height:8px;width:100%;"></div>
    <div>
        <table style="width:100%;">
           <tr>
               <td style="font-size:12px; font-weight:bold;" nowrap="nowrap">状态信息：</td>
               <td style="text-align:left; width:100%;">
                   <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
               </td>
           </tr>
        </table>
    </div>
    </form>
</body>
</html>
