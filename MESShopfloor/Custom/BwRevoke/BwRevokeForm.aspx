﻿<%@ Page Language="C#"  MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="BwRevokeForm.aspx.cs" Inherits="Custom_BwRevoke_BwRevokeForm" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">

    <div>
        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
            <tr>
                <td align="left" colspan="10" class="tdBottom" style="">
                    <div class="ScanLabel">扫描：</div>
                    <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                </td>
                <%-- <td class="tdNoBorder"  colspan="10"  nowrap="nowrap" valign="middle" style="text-align:left;">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>--%>
            </tr>
             <tr>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox" Width="200px" Enabled="false"></asp:TextBox>
                    </td>
                  <td align="left" class="tdRight">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox" Width="200px"  Enabled="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">产品：</div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox" Width="200px"  Enabled="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工序：</div>
                        <asp:TextBox ID="txtStepName" runat="server" class="stdTextBox" Width="200px"  Enabled="false"></asp:TextBox>
                    </td>
                   
                </tr>
        </table>
        <asp:HiddenField runat="server" id="hdContainerID" />
         <asp:HiddenField runat="server" id="hdWorkflowStepID" />
        <asp:HiddenField runat="server" id="hdWorkflowID" />
        <asp:HiddenField runat="server" id="hdSpecID" />
    </div>

    <div style="margin-top:0px;text-align:left;height:380px">
        <div style="float:left;width:400px;height:300px;display:none">
             <div style="font-size:medium;font-family:微软雅黑;margin-bottom:5px">事务记录</div>
            <igtbl:UltraWebGrid ID="transactionGrid" runat="server" Height="100%" Width="100%" >
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                <Header Caption="事务名称">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
        </div>

        <div style="float:left;margin-left:10px;height:300px">
            <div style="font-size:medium;font-family:微软雅黑;margin-bottom:5px">加工数据</div>
            <igtab:UltraWebTab ID="wtInfo" runat="server" Width="100%" Height="100%" BorderColor="#949878"
                BorderStyle="Solid" BorderWidth="1px" Font-Bold="False"
                Font-Italic="False" Font-Overline="False"
                Font-Strikeout="False" Font-Underline="False" SelectedTab="8">
                <RoundedImage NormalImage="[ig_tab_winXP3.gif]" HoverImage="[ig_tab_winXP2.gif]"
                    SelectedImage="[ig_tab_winXP1.gif]" LeftSideWidth="7" RightSideWidth="6" ShiftOfImages="2"
                    FillStyle="LeftMergedWithCenter"></RoundedImage>
                <DefaultTabStyle BackColor="#FEFCFD" Font-Names="Microsoft Sans Serif" Font-Size="12pt" ForeColor="Black" Height="21px">
                    <Padding Bottom="0px" Top="1px" />
                    <Padding Bottom="0px" Top="1px" />
                </DefaultTabStyle>
                <SelectedTabStyle>
                    <Padding Bottom="1px" Top="0px" />
                    <Padding Bottom="1px" Top="0px" />
                </SelectedTabStyle>
                <Tabs>
                    <igtab:Tab AccessKey="1" Key="DispatchTab" Text="派工信息">
                        <ContentTemplate>
                            <%--<div style="width: 100%; margin: 3px 0 3px 3px">--%>
                            <igtbl:UltraWebGrid ID="wgDispatch" runat="server">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="workflowstepname" Key="WorkflowStepName" Width="120px">
                                                <Header Caption="派工工序">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="qty" Hidden="false" Key="Qty" Width="80px">
                                                <Header Caption="派工数量">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="teamname" Key="teamname" Width="150px">
                                                <Header Caption="班组">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="resourcename" Key="resourcename" Width="150px">
                                                <Header Caption="工位/设备">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                <Header Caption="派工人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="deName" Hidden="false" Key="deName">
                                                <Header Caption="派给工人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="dispatchdate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="dispatchdate" Width="120px">
                                                <Header Caption="派工时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="disptype" Hidden="false" Key="disptype" Width="100px">
                                                <Header Caption="派工类型">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="plannedcompletiondate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="plannedcompletiondate" Width="125px">
                                                <Header Caption="要求完成时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="25px" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                            <%--   </div>--%>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab AccessKey="2" Key="ShipRecord" Text="报工信息">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgReport" runat="server" Height="80px">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Key="WorkflowStepName" Width="150px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="qty" Hidden="false" Key="qty" Width="150px">
                                                <Header Caption="报工数量">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="teamname" Hidden="false" Key="teamname" Width="120px">
                                                <Header Caption="班组">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="resourcename" Hidden="false" Key="resourcename">
                                                <Header Caption="工位/设备">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname">
                                                <Header Caption="加工人员">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ReportDate" DataType="Date" Format="yyyy-MM-dd" Key="ReportDate" Width="120px">
                                                <Header Caption="报工时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="rType" Hidden="false" Key="rType" Width="120px">
                                                <Header Caption="报工类型">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab AccessKey="4" Key="QualityRecord" Text="检验信息">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgCheck" runat="server">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Key="WorkflowStepName" Width="150px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="rQty" Hidden="false" Key="rQty" Width="80px">
                                                <Header Caption="报工数量">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="rName" Hidden="false" Key="rName" Width="100px">
                                                <Header Caption="报工人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="reportdate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="reportdate" Width="100px">
                                                <Header Caption="报工时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="EligibilityQty" Hidden="true" Key="EligibilityQty">
                                                <Header Caption="合格数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="NonsenseQty" Hidden="false" Key="NonsenseQty" Width="100px">
                                                <Header Caption="不合格数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="checktype" Hidden="false" Key="checktype" Width="100px">
                                                <Header Caption="检验类型">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="rcheckName" Hidden="false" Key="rcheckName" Width="120px">
                                                <Header Caption="检验人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="checkdate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="checkdate" Width="120px">
                                                <Header Caption="检验时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                     <igtab:Tab AccessKey="6" Key="ProblemRecord" Text="数据采集">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgDataCollect" runat="server" OnInitializeRow="wgDataCollect_InitializeRow">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="checkiteminfoname" Key="checkiteminfoname" Width="200px">
                                                <Header Caption="检测项名称">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="checkitem" Hidden="false" Key="checkitem" Width="450px">
                                                <Header Caption="检测项内容">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="collecttype" Hidden="false" Key="collecttype" Width="80px">
                                                <Header Caption="检查类型">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="productno" Hidden="true" Key="productno" Width="80px">
                                                <Header Caption="产品序号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowUpdate="Yes" BaseColumnName="checkvalue" Hidden="false" Key="checkvalue" Width="150px">
                                                <Header Caption="实测值(工人)">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowUpdate="Yes" BaseColumnName="checkvalue1" Hidden="false" Key="checkvalue1" Width="150px">
                                                <Header Caption="实测值(检验)">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab AccessKey="5" Key="QualityRecord" Text="质量记录">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgQual" runat="server">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Hidden="false" Key="WorkflowStepName" Width="150px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="qualityrecordinfoname" Key="qualityrecordinfoname" Width="150px">
                                                <Header Caption="质量记录单号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                                <Header Caption="不合格数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="disissubmit" Hidden="false" Key="disissubmit" Width="150px">
                                                <Header Caption="是否提交不合格审理">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                <Header Caption="检验人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="SubmitDate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="SubmitDate" Width="120px">
                                                <Header Caption="记录时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab AccessKey="5" Key="QualityRecord" Text="不合格品审理单">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgUnQualified" runat="server" Height="80px">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="rejectappinfoname" Key="rejectappinfoname" Width="150px">
                                                <Header Caption="不合格品审理单号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Hidden="false" Key="WorkflowStepName" Width="150px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="100px">
                                                <Header Caption="不合格数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="rbqty" Hidden="false" Key="rbqty">
                                                <Header Caption="让步使用">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="fixqty" Hidden="false" Key="fixqty" Width="110px">
                                                <Header Caption="返工返修数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="scrapqty" Hidden="false" Key="scrapqty" Width="100px">
                                                <Header Caption="报废数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="SubmitDate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="SubmitDate" Width="120px">
                                                <Header Caption="提交时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab AccessKey="5" Key="QualityRecord" Text="报废信息">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgScrapInfo" runat="server" Height="80px">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ScrapInfoName" Key="ScrapInfoName" Width="150px">
                                                <Header Caption="报废单号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Hidden="false" Key="WorkflowStepName" Width="150px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                                <Header Caption="报废数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="lossreasonname" Hidden="false" Key="lossreasonname">
                                                <Header Caption="报废原因">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="rejectappinfoid" Hidden="true" Key="rejectappinfoid">
                                                <Header Caption="不合格审理单号">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                <Header Caption="报废人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="SubmitDate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="SubmitDate" Width="120px">
                                                <Header Caption="报废时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab AccessKey="5" Key="QualityRecord" Text="返修信息">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgRepair" runat="server" Height="80px">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Hidden="false" Key="WorkflowStepName" Width="150px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="repaircontainername" Key="repaircontainername" Width="150px">
                                                <Header Caption="返修批次">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            
                                            <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                                <Header Caption="返修数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                           
                                            <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                <Header Caption="提交人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="oprdate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="oprdate" Width="120px">
                                                <Header Caption="提交时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                     <igtab:Tab AccessKey="5" Key="QualityRecord" Text="质疑单信息">
                        <ContentTemplate>
                            <igtbl:UltraWebGrid ID="wgQuestion" runat="server" Height="80px">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Hidden="false" Key="WorkflowStepName" Width="150px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="questioninfoname" Key="questioninfoname" Width="150px">
                                                <Header Caption="质疑单号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            
                                            <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="120px">
                                                <Header Caption="质疑数量">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                           
                                            <igtbl:UltraGridColumn BaseColumnName="fullname" Hidden="false" Key="fullname" Width="120px">
                                                <Header Caption="提交人">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="oprdate" DataType="Date" Format="yyyy-MM-dd" Hidden="false" Key="oprdate" Width="120px">
                                                <Header Caption="提交时间">
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="11" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AutoGenerateColumns="False" BorderCollapseDefault="Separate" CellClickActionDefault="RowSelect" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList" RowHeightDefault="18px" ScrollBarView="both" SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True" TableLayout="Fixed" Version="4.00" ViewType="OutlookGroupBy">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="265px" Width="99%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4" StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" CssClass="GridHeaderStyle" Font-Bold="false" Font-Size="16px" Height="100%" HorizontalAlign="Center" Wrap="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Bottom="3px" Top="2px" />
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CssClass="GridRowStyle" Font-Names="Microsoft Sans Serif" Font-Size="14px" Height="30px">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif" Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </ContentTemplate>
                    </igtab:Tab>
                </Tabs>
                <RoundedImage FillStyle="LeftMergedWithCenter" HoverImage="[ig_tab_winXP2.gif]" LeftSideWidth="7" NormalImage="[ig_tab_winXP3.gif]" RightSideWidth="6" SelectedImage="[ig_tab_winXP1.gif]" ShiftOfImages="2" />
            </igtab:UltraWebTab>
            <asp:Button ID="btnRevoke" runat="server" Text="撤销数据" Style="margin:10px -10px;"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnRevoke_Click"  />
        </div>

        <div style="clear:both"></div>
    </div>
     <div style="">
            <table width="100%">
                <tr>
                    <td align="right">
                        
                    </td>
                </tr>
            </table>
     </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
