﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.DBUtility;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Configuration;
using Infragistics.WebUI.UltraWebGrid;

public partial class Custom_BwRevoke_BwRevokeForm : BaseForm
{
    uMESRevokeBusiness revokeBal = new uMESRevokeBusiness();
    uMESCommonBusiness common = new uMESCommonBusiness();
    string webRootDir = HttpRuntime.AppDomainAppPath;
    string webRootUrl = "~";
    string businessName = "撤销平台", parentName = "workflowstep";

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：撤销功能";
        master.strTitle = "撤销功能";
        master.ChangeFrame(true);

        WebPanel = WebAsyncRefreshPanel1;

        ClearMessage();
        if (!IsPostBack)
        {

        }
    }

    #region 事件
    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        try {
            ResetData(false  );

            var re = LoadContainerBaseInfo();
            if (re.IsSuccess == false)
            {
                DisplayMessage(re.Message, false);
                return; }

             //加载业务数据
             LoadDispatchData();//派工
            LoadReportData();//报工
            LoadCheckData();//检验
            LoadDataCollectData();//数据采集
            LoadQualityData();//质量信息
            LoadNonconData();//不合格审理
            LoadScrapData();//报废信息
            LoadRepairData();//返修信息
            LoadQuestionData();//质疑单
        } catch(Exception ex) {
            DisplayMessage(ex.Message,false);
            }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

    }

    protected void btnReSet_Click(object sender, EventArgs e)
    {

    }
    protected void btnRevoke_Click(object sender, EventArgs e)
    {
        try {
            int select = wtInfo.SelectedTabIndex;
            var result = new ResultModel(true,"");
            Dictionary<string, string> para = new Dictionary<string, string>();
            para["ContainerID"] = hdContainerID.Value;
            para["WorkflowStepID"] = hdWorkflowStepID.Value;
            para["RevokeEmployeeID"] = UserInfo["EmployeeID"];
            para.Add("WorkflowID", hdWorkflowID.Value);
            para.Add("SpecID", hdSpecID.Value);

            #region 日志记录对象
            MESAuditLog ml = new MESAuditLog();
            ml.ContainerID= hdContainerID.Value;ml.ContainerName = txtContainerName.Text;
            ml.CreateEmployeeID = UserInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = 0;
            ml.ParentID = hdWorkflowStepID.Value;ml.ParentName = parentName;
            string strMsg = "";
            #endregion

            switch (select) {
                case 0://撤销派工
                    result = RevokeDispatchData(para); strMsg = "派工撤销:"; break;
                case 1://撤销报工
                    result= RevokeReportData(para); strMsg = "报工撤销:" ; break;
                case 2://撤销检验
                    result = RevokeCheckData(para); strMsg = "检验撤销:" ; break;
                case 4://撤销质量记录
                    result = RevokeQualityData(para); strMsg = "质量记录撤销:"; break;
                case 5://不合格审理
                    result= RevokeNonconData(para); strMsg = "不合格审理撤销:"; break;
                case 6://报废数据
                    result = RevokeScrapInfoData(para); strMsg = "报废记录撤销:"; break;
                case 3://撤销数据采集
                    result = RevokeDataCollect(para); strMsg = "数据采集撤销:"; break;
                case 7://返修数据
                    result = new ResultModel(false, "返修后会产生新批次，无法撤销"); break;
                case 8://质疑单
                    result = RevokeQuestionInfoData(para);break;
                default:
                    break;
            }
            strMsg += txtStepName.Text;
            ml.Description = strMsg;
            if (result.IsSuccess)
            {
                common.SaveMESAuditLog(ml);
                 DisplayMessage("撤销成功", result.IsSuccess);
            }
            else
                DisplayMessage(result.Message, result.IsSuccess);
        } catch (Exception ex) {
            DisplayMessage(ex.Message,false);
        }
    }

    #endregion

    #region 方法
    void ResetData(bool isScan) {
        if(isScan)
            txtScan.Text = "";
        txtProcessNo.Text = "";
        txtStepName.Text = "";
        txtContainerName.Text = "";
        txtProductName.Text = "";
        hdContainerID.Value = "";
        hdWorkflowStepID.Value = "";
        hdSpecID.Value = "";
        hdWorkflowID.Value = "";

        wgDispatch.Rows.Clear();
        wgReport.Rows.Clear();
        wgCheck.Rows.Clear();
        wgQual.Rows.Clear();
        wgUnQualified.Rows.Clear();
        wgScrapInfo.Rows.Clear();
        wgDataCollect.Rows.Clear();
    }
    /// <summary>
    /// 加载批次基本信息
    /// </summary>
    /// <returns></returns>
    ResultModel LoadContainerBaseInfo() {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para["ContainerName"] = txtScan.Text;
        DataTable dt = revokeBal.GetContainerBase(para);
        if (dt.Rows.Count == 0)
        {
            return new ResultModel(false, "未找到此批次") ;
        }
        txtProcessNo.Text = dt.Rows[0]["processno"].ToString();
        txtProductName.Text = dt.Rows[0]["productname"].ToString();
        txtStepName.Text = dt.Rows[0]["workflowstepname"].ToString();
        txtContainerName.Text = para["ContainerName"];

        hdContainerID.Value = dt.Rows[0]["containerid"].ToString();
        hdWorkflowStepID.Value = dt.Rows[0]["workflowstepid"].ToString();
        hdWorkflowID.Value = dt.Rows[0]["WorkflowID"].ToString();
        hdSpecID.Value = dt.Rows[0]["SpecID"].ToString();
        return new ResultModel(true, "");
    }

    /// <summary>
    /// 统一验证
    /// </summary>
    /// <param name="wg"></param>
    /// <returns></returns>
    ResultModel CheckData(UltraWebGrid wg) {
        if (wg.Rows.Count == 0)
            return new ResultModel(false,"无数据");
        return new ResultModel(true,"");
    }

    /// <summary>
    /// 加载派工信息
    /// </summary>
    void LoadDispatchData() {
        wgDispatch.Rows.Clear();
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID",hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        DataTable dt = revokeBal.GetDispatchInfo(para);
        
        wgDispatch.DataSource = dt;
        wgDispatch.DataBind();
    }
    //撤销派工信息 派到人的数据
    ResultModel RevokeDispatchData(Dictionary<string,string> para) {
        ResultModel re = CheckData(wgDispatch);
        if (re.IsSuccess == false)
        {
            return re;
        }
        re = revokeBal.RevokeDispatchInfo(para);

        if (re.IsSuccess)
            LoadDispatchData();

        return re;
    }
    //撤销报工信息
    ResultModel RevokeReportData(Dictionary<string, string> para)
    {
        ResultModel re = CheckData(wgReport);
        if (re.IsSuccess == false)
        {
            return re;
        }

        re = revokeBal.RevokeReportInfo(para);

        if (re.IsSuccess)
            LoadReportData();

        return re;
    }

    /// <summary>
    /// 加载报工信息
    /// </summary>
    void LoadReportData() {
        wgReport.Rows.Clear();
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);

        DataTable dt = revokeBal.GetWorkreportInfo(para);

        wgReport.DataSource = dt;
        wgReport.DataBind();
    }
    /// <summary>
    /// 加载检验信息
    /// </summary>
    void LoadCheckData() {
        wgCheck.Rows.Clear();
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        para.Add("WorkflowID", hdWorkflowID.Value);
        para.Add("SpecID", hdSpecID.Value);

        DataTable dt = revokeBal.GetCheckDataInfo(para);
        dt.Columns.Add("WorkflowStepName");

        foreach (DataRow dr in dt.Rows)
        {
            dr["WorkflowStepName"] = txtStepName.Text;
        }

        wgCheck.DataSource = dt;
        wgCheck.DataBind();
    }
    //撤销检验信息
    ResultModel RevokeCheckData(Dictionary<string, string> para)
    {
        ResultModel re = CheckData(wgCheck);
        if (re.IsSuccess == false)
        {
            return re;
        }
        re = revokeBal.RevokeCheckInfo(para);

        if (re.IsSuccess)
            LoadCheckData();

        return re;
    }
    /// <summary>
    /// 加载质量信息
    /// </summary>
    void LoadQualityData() {
        wgQual.Rows.Clear();
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        para.Add("WorkflowID", hdWorkflowID.Value);
        para.Add("SpecID", hdSpecID.Value);

        DataTable dt = revokeBal.GetQualityDataInfo(para);
        dt.Columns.Add("WorkflowStepName");

        foreach (DataRow dr in dt.Rows)
        {
            dr["WorkflowStepName"] = txtStepName.Text;
        }

        wgQual.DataSource = dt;
        wgQual.DataBind();
    }

    /// <summary>
    /// 撤销质量信息
    /// </summary>
    /// <param name="para"></param>
    /// <returns></returns>
    ResultModel RevokeQualityData(Dictionary<string, string> para)
    {
        ResultModel re = CheckData(wgQual);
        if (re.IsSuccess == false)
        {
            return re;
        }
        re = revokeBal.RevokeQualityInfo(para);

        if (re.IsSuccess)
            LoadQualityData();

        return re;
    }

    #region 数据采集
    //数据采集项内容
    void LoadDataCollectData()
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        para.Add("WorkflowID", hdWorkflowID.Value);
        para.Add("SpecID", hdSpecID.Value);
        wgDataCollect.Rows.Clear();
        wgDataCollect.DataSource = revokeBal.GetCollectDataInfo(para);
        wgDataCollect.DataBind();
    }
    protected void wgDataCollect_InitializeRow(object sender, Infragistics.WebUI.UltraWebGrid.RowEventArgs e)
    {
        var row = e.Row;
        //检测项
        string checkItem = row.Cells.FromKey("checkitem").Text;
        string strCheckitemDis = string.Empty;
        ParseCode(checkItem, ref strCheckitemDis);
        row.Cells.FromKey("checkitem").Value = strCheckitemDis;
        //实测值工人
        checkItem = row.Cells.FromKey("checkvalue").Text;
        strCheckitemDis = "";
        ParseCode(checkItem, ref strCheckitemDis);
        row.Cells.FromKey("checkvalue").Value = strCheckitemDis;
        //实测值检验
        checkItem = row.Cells.FromKey("checkvalue1").Text;
        strCheckitemDis = "";
        ParseCode(checkItem, ref strCheckitemDis);
        row.Cells.FromKey("checkvalue1").Value = strCheckitemDis;

    }
     void ParseCode(String strPreviewCode, ref string strPreviewCodeDis)
    {
        try
        {
            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath = webRootDir + ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    strPreviewCodeDis += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir + ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (Session["dtImage"] == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            Session["dtImage"] = dtImage;
                        }
                        else
                        {
                            dtImage = (DataTable)Session["dtImage"];
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        Session["dtImage"] = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        strPreviewCodeDis += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                strPreviewCodeDis += strPreviewCode;
            }
        }
        catch (Exception myError)
        {

        }

    }
    /// <summary>
    /// 撤销数据采集
    /// </summary>
    /// <param name="para"></param>
    /// <returns></returns>
    ResultModel RevokeDataCollect(Dictionary<string, string> para) {
        ResultModel re = CheckData(wgDataCollect);
        if (re.IsSuccess == false)
        {
            return re;
        }
        re = revokeBal.RevokeDataCollectInfo(para);

        if (re.IsSuccess)
            LoadDataCollectData();

        return re;
    }
    #endregion

    /// <summary>
    /// 加载不合格审理信息
    /// </summary>
    void LoadNonconData()
    {
        wgUnQualified.Rows.Clear();
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        para.Add("WorkflowID", hdWorkflowID.Value);
        para.Add("SpecID", hdSpecID.Value);

        DataTable dt = revokeBal.GetNonconDataInfo(para);
        dt.Columns.Add("WorkflowStepName");

        foreach (DataRow dr in dt.Rows)
        {
            dr["WorkflowStepName"] = txtStepName.Text;
        }

        wgUnQualified.DataSource = dt;
        wgUnQualified.DataBind();
    }

    /// <summary>
    /// 撤销不合格审理信息
    /// </summary>
    /// <param name="para"></param>
    /// <returns></returns>
    ResultModel RevokeNonconData(Dictionary<string, string> para)
    {
        ResultModel re = CheckData(wgUnQualified);
        if (re.IsSuccess == false)
        {
            return re;
        }
        re = revokeBal.RevokeNonconInfo(para);

        if (re.IsSuccess)
        {
            LoadNonconData();
            LoadQualityData();
        }

        return re;
    }

    /// <summary>
    /// 加载报废数据
    /// </summary>
    void LoadScrapData() {
        wgScrapInfo.Rows.Clear();

        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        para.Add("WorkflowID", hdWorkflowID.Value);
        para.Add("SpecID", hdSpecID.Value);

        DataTable dt = revokeBal.GetScrapDataInfo(para);
        dt.Columns.Add("WorkflowStepName");

        foreach (DataRow dr in dt.Rows)
        {
            dr["WorkflowStepName"] = txtStepName.Text;
        }

        wgScrapInfo.DataSource = dt;
        wgScrapInfo.DataBind();
    }

    /// <summary>
    /// 撤销报废信息
    /// </summary>
    /// <param name="para"></param>
    /// <returns></returns>
    ResultModel RevokeScrapInfoData(Dictionary<string, string> para)
    {
        ResultModel re = CheckData(wgScrapInfo);
        if (re.IsSuccess == false)
        {
            return re;
        }
        re = revokeBal.RevokeScrapInfo(para);

        if (re.IsSuccess)
            LoadScrapData();

        return re;
    }
    /// <summary>
    /// 返修信息
    /// </summary>
    void LoadRepairData() {
        wgRepair.Rows.Clear();

        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        para.Add("WorkflowID", hdWorkflowID.Value);
        para.Add("SpecID", hdSpecID.Value);

        DataTable dt = revokeBal.GetRepairDataInfo(para);
        dt.Columns.Add("WorkflowStepName");

        foreach (DataRow dr in dt.Rows)
        {
            dr["WorkflowStepName"] = txtStepName.Text;
        }

        wgRepair.DataSource = dt;
        wgRepair.DataBind();
    }

    /// <summary>
    /// 质疑单信息
    /// </summary>
    void LoadQuestionData()
    {
        wgQuestion.Rows.Clear();

        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("WorkflowStepID", hdWorkflowStepID.Value);
        para.Add("ContainerID", hdContainerID.Value);
        para.Add("WorkflowID", hdWorkflowID.Value);
        para.Add("SpecID", hdSpecID.Value);

        DataTable dt = revokeBal.GetQuestionDataInfo(para);
        dt.Columns.Add("WorkflowStepName");

        foreach (DataRow dr in dt.Rows)
        {
            dr["WorkflowStepName"] = txtStepName.Text;
        }

        wgQuestion.DataSource = dt;
        wgQuestion.DataBind();
    }

    /// <summary>
    /// 撤销质疑单信息
    /// </summary>
    /// <param name="para"></param>
    /// <returns></returns>
    ResultModel RevokeQuestionInfoData(Dictionary<string, string> para)
    {
        ResultModel re = CheckData(wgQuestion);
        if (re.IsSuccess == false)
        {
            return re;
        }
        re = revokeBal.RevokeQuestionInfo(para);

        if (re.IsSuccess)
            LoadQuestionData();

        return re;
    }
    #endregion






}