﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESCheckingPlanPopupForm.aspx.cs"
    ValidateRequest="false" Inherits="uMESCheckingPlanPopupForm" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>设备点检计划</title>
    <base target="_self" />
    <link href="../../styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
    <script src="../../../Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery-1.8.2.js" type="text/javascript"></script>
    <script type="text/javascript" src="/MESShopfloor/Scripts/DateControl.js"></script>
    <style type="text/css">
        .auto-style1 {
            float: left;
        }
    </style>
    </head>
<body >
    <form id="form1" runat="server">

        <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
            <div>
                <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="220px" Width="66%" OnActiveRowChange="ItemGrid_ActiveRowChange">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:UltraGridColumn BaseColumnName="description" Key="description" Width="150px" AllowGroupBy="No">
                                    <Header Caption="设备名称">
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="resourcename" Key="resourcename" Width="150px" AllowGroupBy="No">
                                    <Header Caption="设备编号">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="assetnumber" Key="assetnumber" Width="120px" AllowGroupBy="No">
                                    <Header Caption="资产编号">
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="factoryname" Key="factoryname" Width="100px" AllowGroupBy="No">
                                    <Header Caption="所属分厂">
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="teamname" Key="teamname" Width="150px" AllowGroupBy="No">
                                    <Header Caption="所属班组">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                                <igtbl:UltraGridColumn BaseColumnName="checkcycleDis" Key="checkcycleDis" Width="100px" AllowGroupBy="No">
                                    <Header Caption="点检周期">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="plancheckdate" Key="plancheckdate" Width="100px" AllowGroupBy="No" Format="yyyy-MM-dd">
                                    <Header Caption="点检时间">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="checkName" Key="checkName" Width="80px" AllowGroupBy="No">
                                    <Header Caption="点检人">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                                <igtbl:UltraGridColumn BaseColumnName="notes" Key="notes" Width="150px" AllowGroupBy="No">
                                    <Header Caption="备注">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                                <igtbl:UltraGridColumn BaseColumnName="checkcycle" Hidden="true" Key="checkcycle">
                                    <Header Caption="checkcycle">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                                <igtbl:UltraGridColumn BaseColumnName="plancheckerid" Hidden="true" Key="plancheckerid">
                                    <Header Caption="plancheckerid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>

                                <igtbl:UltraGridColumn BaseColumnName="factoryid" Hidden="true" Key="factoryid">
                                    <Header Caption="factoryid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="teamid" Hidden="True" Key="teamid">
                                    <Header Caption="teamid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="resourceid" Hidden="True" Key="resourceid">
                                    <Header Caption="resourceid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="id" Hidden="True" Key="id">
                                    <Header Caption="id">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="420px" Width="100%">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
            </div>
            <div style="width: 100%; background-color: #D6F1FF; height:50px">
                <div style="float: left; margin-left:2px;">
                    <div class="divLabel">设备名称：</div>
                    <asp:TextBox ID="txtName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div class="divLabel">&nbsp; 设备编号：</div>
                    <asp:TextBox ID="txtNum" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div class="divLabel">&nbsp; 资产编号：</div>
                    <asp:TextBox ID="txtAssetNumber" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div class="divLabel">所属分厂：</div>
                    <asp:DropDownList ID="ddlFactory" runat="server" Width="150px" Enabled="true" Height="25px"></asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div class="divLabel">所属班组：</div>
                    <asp:DropDownList ID="ddlTeam" runat="server" Width="150px" Enabled="true" Height="25px"></asp:DropDownList>
                </div>


            </div>
            <div style="clear: both"></div>
            <div style="width: 100%; background-color: #D6F1FF; height:50px;">
                <div style="float: left; margin-left:4px;">
                    <div class="divLabel">点检周期：</div>
                    <asp:DropDownList ID="ddlCheckCycle" runat="server" Width="150px" Height="25px"></asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 22px;">
                    <div class="divLabel">点检人：</div>
                    <asp:DropDownList ID="ddlCheck" runat="server" Width="150px" Height="25px"></asp:DropDownList>
                </div>
                <div style="margin-left: 13px;" class="auto-style1" >
                    <div class="divLabel">&nbsp;&nbsp; 点检时间：</div>
                    <input id="txtCheckDate" runat="server" onclick="this.value = ''; setday(this);" style="width: 150px;height:22px; font-size:larger;" class="" type="text"  />

                </div>
                <div style="float: left; margin-left: 10px;">
                    <div class="divLabel">&nbsp; 备注：</div>
                    <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" ReadOnly="false"></asp:TextBox>
                </div>
            </div>
            <div style="clear: both">&nbsp;&nbsp;&nbsp;&nbsp; </div>
            <div style="margin: 5px;">
                <asp:Button ID="btnSave" runat="server" Text="保存" Style="margin-right: 20px"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
                <asp:Button ID="btnAdd" runat="server" Text="新增" Style="margin-right: 20px"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnAdd_Click" />
                <asp:Button ID="btnDelete" runat="server" Text="删除" Style="margin-right: 20px" OnClientClick=" return ((confirm('确认删除？') == true) ? true :  false) "
                    CssClass="searchButton" EnableTheming="True" OnClick="btnDelete_Click" />
                <asp:Button ID="btnClose" runat="server" Text="关闭"
                    CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
            </div>
            <div style="margin: 5px;">
                <div style="font-size: 12px; font-weight: bold; float: left; padding-top: 5px;">状态信息：</div>

                <div>
                    <asp:Label ID="lStatusMessage" runat="server" Style="font-size: 12px;" Width="90%"></asp:Label>
                </div>
            </div>
            <div style="display: none; visibility: hidden">
                <asp:TextBox ID="txtResourceID" runat="server" class="stdTextBox" ReadOnly="false"></asp:TextBox>
            </div>
        </igmisc:WebAsyncRefreshPanel>
    </form>
</body>
</html>
