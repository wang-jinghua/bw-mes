﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="uMESResourceManagementForm.aspx.cs"
    Inherits="uMESResourceManagementForm" EnableViewState="true" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" valign="bottom" class="tdRight" style="width: 148px">
                        <div class="divLabel">设备名称：</div>
                        <asp:TextBox ID="txtSearchName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 136px">
                        <div class="divLabel">设备编号：</div>
                        <asp:TextBox ID="txtSearchNum" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 150px">
                        <div class="divLabel">资产编号：</div>
                        <asp:TextBox ID="txtSearchAsset" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True"/>
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 8px; width: 100%;"></div>
        <div id="gridDiv" runat="server" style="margin: 5px;">
            <div>
                <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="220px" Width="100%" OnActiveRowChange="ItemGrid_ActiveRowChange">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:UltraGridColumn BaseColumnName="description" Key="description" Width="150px" AllowGroupBy="No">
                                    <Header Caption="设备名称">
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="resourcename" Key="resourcename" Width="150px" AllowGroupBy="No">
                                    <Header Caption="设备编号">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="assetnumber" Key="assetnumber" Width="150px" AllowGroupBy="No">
                                    <Header Caption="资产编号">
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="factoryname" Key="factoryname" Width="120px" AllowGroupBy="No">
                                    <Header Caption="所属分厂">
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="teamname" Key="teamname" Width="150px" AllowGroupBy="No">
                                    <Header Caption="所属班组">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="fullname" Key="fullname" Width="100px" AllowGroupBy="No">
                                    <Header Caption="责任人">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="resourcestatusname" Key="resourcestatusname" Width="100px" AllowGroupBy="No">
                                    <Header Caption="设备状态">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="vendorname" Key="vendorname" Width="200px" AllowGroupBy="No">
                                    <Header Caption="供应商">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="contact" Key="contact" Width="120px" AllowGroupBy="No">
                                    <Header Caption="联系方式">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="notes" Key="notes" Width="150px" AllowGroupBy="No">
                                    <Header Caption="备注">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="vendorid" Hidden="True" Key="vendorid">
                                    <Header Caption="vendorid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="resourcestatusid" Hidden="True" Key="resourcestatusid">
                                    <Header Caption="resourcestatusid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="factoryid" Hidden="true" Key="factoryid">
                                    <Header Caption="factoryid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="teamid" Hidden="True" Key="teamid">
                                    <Header Caption="teamid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="resourceid" Hidden="True" Key="resourceid">
                                    <Header Caption="resourceid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="personliableid" Hidden="True" Key="personliableid">
                                    <Header Caption="personliableid">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="employeename" Hidden="True" Key="employeename">
                                    <Header Caption="employeename">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="nameValue" Hidden="True" Key="nameValue">
                                    <Header Caption="nameValue">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="330px" Width="100%">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
            </div>
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                            <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                            <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                                Text="第  页  共  页"></asp:Label>
                            <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                            <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                            <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                            <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                                OnClick="btnGo_Click" />
                            <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 100%; background-color: #D6F1FF;">
                <div style="float: left;">
                    <div style="width: 150px; text-align: left;" class="divLabel">设备名称：</div>
                    <asp:TextBox ID="txtName" runat="server" class="stdTextBox"></asp:TextBox>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div style="width: 150px; text-align: left;" class="divLabel">设备编号：</div>
                    <asp:TextBox ID="txtNum" runat="server" class="stdTextBox"></asp:TextBox>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div style="width: 150px; text-align: left;" class="divLabel">资产编号：</div>
                    <asp:TextBox ID="txtAssetNumber" runat="server" class="stdTextBox"></asp:TextBox>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div style="width: 150px; text-align: left;" class="divLabel">所属分厂：</div>
                    <asp:DropDownList ID="ddlFactory" runat="server" Width="150px" Height="25px"></asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div style="width: 150px; text-align: left;" class="divLabel">所属班组：</div>
                    <asp:DropDownList ID="ddlTeam" runat="server" Width="150px" Height="25px"></asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div style="width: 150px; text-align: left;" class="divLabel">责任人：</div>
                    <asp:DropDownList ID="ddlPersonLiable" runat="server" Width="150px" Height="25px"></asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <div style="width: 150px; text-align: left;" class="divLabel">设备状态：</div>
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="150px" Height="25px"></asp:DropDownList>
                </div>
            </div>

        </div>
        <div style="clear: both;"></div>
        <div style="width: 100%; background-color: #D6F1FF;">

            <div style="float: left; margin-left: 5px">
                <div style="width: 150px; text-align: left;" class="divLabel">供应商：</div>

                <asp:DropDownList ID="ddlVendor" runat="server" Width="150px" Height="25px"></asp:DropDownList>
            </div>
            <div style="float: left; margin-left: 11px;">
                <div style="width: 160px; text-align: left;" class="divLabel">&nbsp;&nbsp; 联系方式：</div>
                <asp:TextBox ID="txtContact" runat="server" class="stdTextBox"></asp:TextBox>
            </div>
            <div style="float: left; margin-left: 11px;">
                <div style="width: 150px; text-align: left;" class="divLabel">备注：</div>
                <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox"></asp:TextBox>
            </div>
        </div>
        <div style="clear: both; height: 20px;"></div>
        <div>
            <div style="float: left">
                <asp:Button ID="btnSave" runat="server" Text="保存"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnAdd" runat="server" Text="新增"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnAdd_Click" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnAddPlan" runat="server" Text="添加点检计划"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnAddPlan_Click" />
            </div>
            <div style="float: left; margin-left: 10px; visibility:hidden">
                <asp:Button ID="btnAddTask" runat="server" Text="添加点检任务"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnAddTask_Click" />
            </div>
        </div>

    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
