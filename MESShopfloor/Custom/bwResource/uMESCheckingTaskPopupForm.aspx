﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="uMESCheckingTaskPopupForm.aspx.cs"
    ValidateRequest="false" Inherits="uMESCheckingTaskPopupForm" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning"
    TagPrefix="uPT" %>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" valign="bottom" class="tdRight" style="width: 148px">
                            <div class="divLabel">设备名称：</div>
                            <asp:TextBox ID="txtSearchName" runat="server" class="stdTextBox"></asp:TextBox>
                        </td>
                        <td align="left" valign="bottom" class="tdRight" style="width: 136px">
                            <div class="divLabel">设备编号：</div>
                            <asp:TextBox ID="txtSearchNum" runat="server" class="stdTextBox"></asp:TextBox>
                        </td>
                        <td align="left" valign="bottom" class="tdRight" style="width: 150px">
                            <div class="divLabel">资产编号：</div>
                            <asp:TextBox ID="txtSearchAsset" runat="server" class="stdTextBox"></asp:TextBox>
                        </td>
                        <td align="left" valign="bottom" class="tdRight" style="width: 150px">
                            <div class="divLabel">要求点检人：</div>
                            <asp:TextBox ID="txtcheckName" runat="server" class="stdTextBox"></asp:TextBox>
                        </td>
                        <td align="left" nowrap="nowrap" class="tdRight" valign="bottom" style="width: 293px">
                            <div class="divLabel" style="width: 264px">点检周期：</div>
                            <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                            -
                            <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        </td>
                        <td align="left"  class="tdRight" style="width: 150px">
                            <div class="divLabel">点检周期：</div>
                            <asp:DropDownList ID="ddlCheckCycle" runat="server" Width="150px" Height="25px"></asp:DropDownList>
                        </td>
                        <td align="left" class="tdRight" style="width: 209px">
                            <div class="divLabel">是否本人：</div>
                            <asp:CheckBox ID="cbDesignEmployee" runat="server" />
                        </td>
                        <td align="left" valign="bottom" class="tdNoBorder" nowrap="nowrap">
                            <asp:Button ID="btnSearch" runat="server" Text="查询"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" Style="height: 26px" />
                            <asp:Button ID="btnReSet" runat="server" Text="重置"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height: 8px; width: 100%;"></div>
            <div>
                <div>
                    <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="220px" Width="70%" OnActiveRowChange="ItemGrid_ActiveRowChange">
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="ItemGrid"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="220px" Width="100%">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <Padding Top="2px" Bottom="3px"></Padding>
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:UltraGridColumn BaseColumnName="description" Key="description" Width="130px" AllowGroupBy="No">
                                        <Header Caption="设备名称">
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="resourcename" Key="resourcename" Width="130px" AllowGroupBy="No">
                                        <Header Caption="设备编号">
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="3" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="assetnumber" Key="assetnumber" Width="130px" AllowGroupBy="No">
                                        <Header Caption="资产编号">
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="4" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="factoryname" Key="factoryname" Width="100px" AllowGroupBy="No">
                                        <Header Caption="所属分厂">
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="5" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="teamname" Key="teamname" Width="150px" AllowGroupBy="No">
                                        <Header Caption="所属班组">
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>

                                    <igtbl:UltraGridColumn BaseColumnName="checkcycleDis" Key="checkcycleDis" Width="100px" AllowGroupBy="No">
                                        <Header Caption="点检周期">
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="plancheckdate" Key="plancheckdate" Width="110px" AllowGroupBy="No" Format="yyyy-MM-dd">
                                        <Header Caption="要求点检时间">
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="checkName" Key="checkName" Width="100px" AllowGroupBy="No">
                                        <Header Caption="要求点检人">
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>

                                    <igtbl:UltraGridColumn BaseColumnName="notes" Key="notes" Width="150px" AllowGroupBy="No">
                                        <Header Caption="备注">
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="6" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>

                                    <igtbl:UltraGridColumn BaseColumnName="checkcycle" Hidden="true" Key="checkcycle">
                                        <Header Caption="checkcycle">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>

                                    <igtbl:UltraGridColumn BaseColumnName="plancheckerid" Hidden="true" Key="plancheckerid">
                                        <Header Caption="plancheckerid">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>

                                    <igtbl:UltraGridColumn BaseColumnName="factoryid" Hidden="true" Key="factoryid">
                                        <Header Caption="factoryid">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="teamid" Hidden="True" Key="teamid">
                                        <Header Caption="teamid">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="resourceid" Hidden="True" Key="resourceid">
                                        <Header Caption="resourceid">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="id" Hidden="True" Key="id">
                                        <Header Caption="id">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="realcheckdate" Hidden="True" Key="realcheckdate">
                                        <Header Caption="realcheckdate">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="realcheckerid" Hidden="True" Key="realcheckerid">
                                        <Header Caption="realcheckerid">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                    </igtbl:UltraWebGrid>
                    <div style="margin-right: 5px; text-align: right; float: right">
                        <uPT:pageTurning ID="upageTurning" runat="server" />
                    </div>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div style="margin-left: 5px; width: 100%; background-color: #D6F1FF;">
                <div style="float: left">

                    <div>
                        <div style="width: 150px; text-align: left;" class="divLabel">点检人：</div>
                        <asp:DropDownList ID="ddlRealCheck" runat="server" Width="150px"></asp:DropDownList>
                    </div>
                    <div style="margin-left: 1px; padding-top: 20px;">
                        <div style="width: 150px; text-align: left;" class="divLabel">点检时间：</div>
                        <input id="txtCheckDate" runat="server" onclick="this.value = ''; setday(this);" style="width: 150px" class="" type="text" />
                    </div>
                    <div style="margin-left: 1px; padding-top: 20px;">
                        <div style="width: 150px; text-align: left;" class="divLabel">备注：</div>
                        <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" Height="72px"></asp:TextBox>
                    </div>
                </div>
                <div style="float: left; padding-left: 10px">

                    <igtbl:UltraWebGrid ID="uwgAppendix" runat="server" Height="220px" Width="108%" OnActiveRowChange="uwgAppendix_ActiveRowChange">
                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="ItemGrid"
                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy"
                            RowHeightDefault="18px">
                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="150px" Width="500px">
                            </FrameStyle>
                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                            </RowAlternateStyleDefault>
                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                StyleMode="QuickPages">
                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </PagerStyle>
                            </Pager>
                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                            </EditCellStyleDefault>
                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </FooterStyleDefault>
                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                <Padding Top="2px" Bottom="3px"></Padding>
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </HeaderStyleDefault>
                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                <Padding Left="3px" />
                            </RowSelectorStyleDefault>
                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                <Padding Left="3px" />
                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                            </RowStyleDefault>
                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                            </GroupByRowStyleDefault>
                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                            </SelectedRowStyleDefault>
                            <GroupByBox Hidden="True">
                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                </BoxStyle>
                            </GroupByBox>
                            <AddNewBox>
                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </BoxStyle>
                            </AddNewBox>
                            <ActivationObject BorderColor="" BorderWidth="">
                            </ActivationObject>
                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px" Height="420px" Width="200px">
                                    <Padding Left="2px" />
                                </FilterDropDownStyle>
                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                </FilterHighlightRowStyle>
                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                    Font-Size="11px">
                                    <Padding Left="2px" />
                                </FilterOperandDropDownStyle>
                            </FilterOptionsDefault>
                        </DisplayLayout>
                        <Bands>
                            <igtbl:UltraGridBand>
                                <Columns>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="Select" Key="Select" Width="40px" DataType="System.Boolean" Type="CheckBox" AllowUpdate="Yes">
                                        <Header Caption="选择">
                                        </Header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="AppendixName" Key="AppendixName" Width="150px">
                                        <Header Caption="附件">
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="1" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="Description" Key="Description" Width="250px" AllowUpdate="Yes">
                                        <Header Caption="附件描述">
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SavedName" Key="SavedName" Width="250px" Hidden="true" AllowUpdate="Yes">
                                        <Header Caption="数据库中保存的附件名称">
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="2" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="isSuccess" Hidden="True" Key="isSuccess">
                                        <Header Caption="isSuccess">
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Header>
                                        <Footer>
                                            <RowLayoutColumnInfo OriginX="11" />
                                        </Footer>
                                    </igtbl:UltraGridColumn>
                                </Columns>
                                <AddNewRow View="NotSet" Visible="NotSet">
                                </AddNewRow>
                            </igtbl:UltraGridBand>
                        </Bands>
                    </igtbl:UltraWebGrid>
                    <div>
                        <div style="width: 497px; text-align: left;" class="divLabel">附件：</div>

                        <div style="float: left;">
                            <asp:FileUpload ID="fileUpload" runat="server" Width="360px" />
                        </div>
                        <div style="float: left; padding-left: 5px;">
                            <asp:Button ID="btnAdd" runat="server" Text="添加" Style="margin-right: 20px"
                                CssClass="searchButton" EnableTheming="True" Height="25px" OnClick="btnAdd_Click" />
                        </div>
                        <div style="float: left; padding-left: 5px;">
                            <asp:Button ID="btnDel" runat="server" Text="删除" Style="margin-right: 20px"
                                CssClass="searchButton" EnableTheming="True" Height="25px" OnClick="btnDel_Click" />
                        </div>
                    </div>
                </div>
                <div style="float: left; width: 380px; height: 250px; padding-left: 10px">
                    <asp:Image ID="imShow" runat="server" Height="220px" Width="500px" />
                </div>
            </div>
            <div style="float: left">
                <asp:Button ID="btnSave" runat="server" Text="确认"
                    CssClass="searchButton" EnableTheming="True" OnClick="btnConfirm_Click" />

                <asp:Button ID="btnClose" runat="server" Text="关闭" Visible="false"
                    CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
            </div>
            <div style="display: none; visibility: hidden">
                <asp:TextBox ID="txtResourceID" runat="server" class="stdTextBox" ReadOnly="false"></asp:TextBox>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAdd" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
