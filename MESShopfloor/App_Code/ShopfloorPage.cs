﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infragistics.WebUI.Misc;
using Infragistics.WebUI.Shared;
using System.Web.UI;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Class1
/// </summary>
public class ShopfloorPage : System.Web.UI.Page
{
    public ShopfloorPage()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    WebAsyncRefreshPanel warp;
    public WebAsyncRefreshPanel WebPanel
    {
        set
        {
            warp = value;
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected virtual void DisplayMessage(string strMessage, Boolean boolResult)
    {
        //处理特殊字符 add:Wangjh
        strMessage = strMessage.Replace("\"", "");
        strMessage = strMessage.Replace("\r", "");
        strMessage = strMessage.Replace("\n", "");
        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, warp, strScript);
    }

    /// <summary>
    /// 初始化时清除提示信息
    /// </summary>
    protected void ClearMessage_PageLoad()
    {
        string strScript = "<script>ShowMessage(\"\", true);</script>";
        LiteralControl child = new LiteralControl(strScript);
        warp.Controls.Add(child);
    }

    /// <summary>
    /// 清除提示信息
    /// </summary>
    protected void ClearMessage()
    {
        string strScript = "<script>ShowMessage(\"\", true);</script>";
        Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, warp, strScript);
    }
    #endregion

    /// <summary>
    /// 执行JavaScript
    /// </summary>
    /// <param name="strScript">JavaScript需保留开始及结束标签：《script》《/script》</param>
    protected void ExecuteScript(string strScript)
    {
        CallBackManager.AddScriptBlock(Page, warp, strScript);
    }
}