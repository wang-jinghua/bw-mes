﻿//Description:自定义页面基类
//Copyright (c) : 通力凯顿（北京）系统集成有限公司
//Writer:Wangjh
//create Date:2020-04-26
//Rewriter:
//Rewrite Date:
using Infragistics.WebUI.Misc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

/// <summary>
/// BaseForm 的摘要说明
/// </summary>
public class BaseForm: Page
{
    private Dictionary<string, string> userInfo;
    public Dictionary<string, string> UserInfo {
        get { return userInfo; }
    }

    WebAsyncRefreshPanel warp;
    public WebAsyncRefreshPanel WebPanel
    {
        set
        {
            warp = value;
        }
    }

    //弹出页面传值
    public Object PopupData {
        get {

            return Session["PopupData"];
        }
        set {
            Session["PopupData"] = value;
        }
    }

    protected override void OnInit(System.EventArgs e) {
        base.OnInit(e);
        //if (!IsPostBack) {
            string domainName = "";
            string localPath = Request.Url.LocalPath;
            domainName = localPath.Substring(0, localPath.IndexOf('/', 1) + 1);


            HeaderAddLink(domainName + "style/css.css");
            HeaderAddLink(domainName + "style/default.css");
            HeaderAddLink(domainName + "styles/Calendar.css");
            HeaderAddLink(domainName + "styles/MyReportStyle.css");
            HeaderAddLink(domainName + "styles/MESShopfloor.css");

            HeaderAddScript(domainName + "Scripts/jquery.min.js");
            HeaderAddScript(domainName + "Scripts/Form.js");
            //HeaderAddScript(domainName + "Scripts/DateControl.js");
            HeaderAddScript(domainName + "Scripts/Customer.js");
        // }


        userInfo = Session["UserInfo"] as Dictionary<string, string>;
    }

    /// <summary>
    /// 清除消息
    /// </summary>
    protected virtual void ClearMessage() {
        DisplayMessage("",true);
    }

    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected virtual void DisplayMessage(string strMessage, Boolean boolResult)
    {
        //strMessage = strMessage.Trim(new[] { '\r', '\n' });
        //处理特殊字符 add:Wangjh
        strMessage = strMessage.Replace("\"","");
        strMessage = strMessage.Replace("\r","");
        strMessage = strMessage.Replace("\n","");

        string strScript = "<script>ShowMessage(\"" + strMessage + "\", " + boolResult.ToString().ToLower() + ");</script>";
        if(warp==null)
            ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), strScript);
        else
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, warp, strScript);

    }

    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    public void DisplayMessage(System.Web.UI.WebControls.Label label, string strMessage, Boolean boolResult)
    {
        label.Text = strMessage;
        label.Font.Size = 14;
        if (boolResult == true)
        {
            label.ForeColor = Color.Black;
        }
        else
        {
            label.ForeColor = Color.Red;
        }
    }
    /// <summary>
    /// 打开弹出框
    /// </summary>
    /// <param name="pagePath"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="isPopup"></param>
   public  void ShowPopupPage(string pagePath,int width,int height,bool isPopup) {
        string script;
        if (isPopup)
        {
            script = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", pagePath, width, height);
        }
        else {
            script = String.Format("<script>window.open('{0}');</script>", pagePath);
        }
        if(warp==null)
            ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), script);
        else
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, warp, script);
    }

    /// <summary>
    /// 关闭弹出框
    /// </summary>
    /// <param name="notifyParent"></param>
    public void ClosePopupPage(bool notifyParent) {
        string script = "";
        if (notifyParent)
        {
            script = "<script>window.returnValue = true;window.close();</script> ";
            //ClientScript.RegisterStartupScript(this.GetType(), "NotifyParent", "<script>NotifyParentOfPopupClosed(); OnPopupWindowClosed(); window.opener= null;window.open('','_self'); ; window.close();</script> ");
            //Response.Write("<script>OnPopupWindowClosed();window.opener= null;window.close();</script>");
        }
        else
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "NotifyParent", "<script> window.returnValue = false;  window.close();</script> ");
            script = "<script> window.returnValue = false;  window.close();</script> ";
        }
        if(warp==null)
            ClientScript.RegisterStartupScript(this.GetType(), "NotifyParent", script);
        else
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, warp, script);
    }

    /// <summary>
    /// 添加js
    /// </summary>
    /// <param name="src"></param>
    void HeaderAddScript(string src) {
        var script = new HtmlGenericControl("script");
        script.Attributes["language"] = "javascript";
        script.Attributes["Type"] = "text/javascript";
        script.Attributes["Src"] = src;
        Header.Controls.Add(script);
    }

    /// <summary>
    /// 添加css
    /// </summary>
    /// <param name="href"></param>
    void HeaderAddLink(string href)
    {
        var link = new HtmlGenericControl("link");
        link.Attributes["rel"] = "stylesheet";
        link.Attributes["href"] = href;
        Header.Controls.Add(link);
    }

    /// <summary>
    /// 执行js脚本
    /// </summary>
    /// <param name="strScript"></param>
    public void ExcuteScript(string strScript)
    {
        if (warp != null)
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, warp, strScript);
        else
            ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), strScript);
    }


}