﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.SessionState;

/// <summary>
///NormalReportControl 的摘要说明
/// </summary>
public class NormalReportControl
{
    public NormalReportControl()
    {
        //
        //TODO: 在此处添加构造函数逻辑
        //
    }

    Button btnSearch;
    Button btnReset;
    Button btnGo;

    TextBox txtPage;
    TextBox txtTotalPage;
    TextBox txtCurrentPage;

    LinkButton lbtnFirst;
    LinkButton lbtnPrev;
    LinkButton lbtnNext;
    LinkButton lbtnLast;
    Label txtLinkPages;
    INormalReport iNormalOp;

    public INormalReport NormalOperation
    { 
       set 
       {
           iNormalOp = value;
       }
    }
    public string QueryWhere
    {
        set;
        private get;
    }
    public TextBox TxtPage
    {
        set
        {
            //WebPage.Session[QueryWhere].ToString();
            txtPage = value;
        }
    }
    public TextBox TxtTotalPage
    {
        set
        {
            txtTotalPage = value;
        }
    }
    public TextBox TxtCurrentPage
    {
        set
        {
            txtCurrentPage = value;
        }
    }
    public Button BtnGo
    {
        set
        {
            btnGo = value;
            btnGo.Click += new EventHandler(btnGo_Click);
        }
    }
    public Button BtnSearch
    {
        set
        {
            btnSearch = value;
            btnSearch.Click += new EventHandler(btnSearch_Click);
        }
    }
    public Button BtnReset
    {
        set
        {
            btnReset = value;
            btnReset.Click += new EventHandler(btnReset_Click);
        }
    }
    public LinkButton LtnFirst
    {
        set
        {
            lbtnFirst = value;
            lbtnFirst.Click += new EventHandler(lbtnFirst_Click);
        }
    }
    public LinkButton LtnPrev
    {
        set
        {
            lbtnPrev = value;
            lbtnPrev.Click += new EventHandler(lbtnPrev_Click);
        }
    }
    public LinkButton LtnNext
    {
        set
        {
            lbtnNext = value;
            lbtnNext.Click += new EventHandler(lbtnNext_Click);
        }
    }
    public LinkButton LtnLast
    {
        set
        {
            lbtnLast = value;
            lbtnLast.Click += new EventHandler(lbtnLast_Click);
        }
    }
    public Label  LabPages
    {
        set
        {
            txtLinkPages = value;
        }
    }
    HttpSessionState Session
    {
        get
        {
            return iNormalOp.Session;
        }
    }
    void lbtnLast_Click(object sender, EventArgs e)
    {
        if (Session[QueryWhere] != null && !string.IsNullOrEmpty(Session[QueryWhere].ToString()))
        {
            this.txtCurrentPage.Text = this.txtTotalPage.Text;
            Dictionary<string, string> query = Session[QueryWhere] as Dictionary<string, string>;
            QueryData(query);
        }
    }
    void lbtnPrev_Click(object sender, EventArgs e)
    {
        if (Session[QueryWhere] != null && !string.IsNullOrEmpty(Session[QueryWhere].ToString()))
        {
            int currentPage = Convert.ToInt32(this.txtCurrentPage.Text);
            if (currentPage > 1)
            {
                currentPage = currentPage - 1;
            }
            this.txtCurrentPage.Text = currentPage.ToString();
            Dictionary<string, string> query = Session[QueryWhere] as Dictionary<string, string>;
            QueryData(query);
        }
    }
    void lbtnNext_Click(object sender, EventArgs e)
    {
        if (Session[QueryWhere] != null && !string.IsNullOrEmpty(Session[QueryWhere].ToString()))
        {
            int currentPage = Convert.ToInt32(this.txtCurrentPage.Text);
            int totalPage = Convert.ToInt32(this.txtTotalPage.Text);
            if (currentPage < totalPage)
            {
                currentPage = currentPage + 1;
            }
            this.txtCurrentPage.Text = currentPage.ToString();
            Dictionary<string, string> query = Session[QueryWhere] as Dictionary<string, string>;
            QueryData(query);
        }
    }

    private void QueryData(Dictionary<string, string> query)
    {
        iNormalOp.QueryData(query);
    }
    void lbtnFirst_Click(object sender, EventArgs e)
    {
        if (Session[QueryWhere] != null && !string.IsNullOrEmpty(Session[QueryWhere].ToString()))
        {
            this.txtCurrentPage.Text = "1";
            Dictionary<string, string> query = Session[QueryWhere] as Dictionary<string, string>;
            QueryData(query);
        }
    }

    void btnSearch_Click(object sender, EventArgs e)
    {
        Session[QueryWhere] = null;
        var query = iNormalOp.GetQuery();
        Session[QueryWhere] = query;
        this.txtCurrentPage.Text = "1";
        QueryData(query);
    }
    void btnReset_Click(object sender, EventArgs e)
    {
        iNormalOp.ResetQuery();
    }
    void btnGo_Click(object sender, EventArgs e)
    {
        if (Session[QueryWhere] != null && !string.IsNullOrEmpty(Session[QueryWhere].ToString()))
        {
            if (Convert.ToInt32(this.txtPage.Text) > 0 && Convert.ToInt32(this.txtPage.Text) <= Convert.ToInt32(this.txtTotalPage.Text))
            {
                this.txtCurrentPage.Text = this.txtPage.Text;
                Dictionary<string, string> query = Session[QueryWhere] as Dictionary<string, string>;
                QueryData(query);
            }
        }
    }
}