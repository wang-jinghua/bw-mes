﻿using Infragistics.WebUI.UltraWebGrid;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// uMESCommonFunction 的摘要说明
/// </summary>
public static class uMESCommonFunction
{
    public static T CopyObject<T>(this T source)
    {
        if (Object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        var deserializeSettings = new JsonSerializerSettings { ObjectCreationHandling = ObjectCreationHandling.Replace };
        return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source), deserializeSettings);
    }

    /// <summary>
    /// 获取grid选择的行,临时列
    /// </summary>
    /// <param name="wg"></param>
    /// <param name="indexList"></param>
    /// <param name="selectKey"></param>
    /// <returns></returns>
    public static DataTable GetGridChooseInfo(UltraWebGrid wg,List<int> indexList,string selectKey) {
        DataTable dt = new DataTable();

        foreach (UltraGridColumn col in wg.Columns)
        {
            if (dt.Columns.Contains(col.Key))
                continue;
            dt.Columns.Add(col.Key);
        }

        DataRow dr;
        TemplatedColumn temCell = (TemplatedColumn)wg.Columns.FromKey(selectKey);
        foreach (UltraGridRow row in wg.Rows)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[row.Index];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl(selectKey);

            if (ckSelect.Checked == false)
            {
                continue;
            }

            if(indexList!=null)
                indexList.Add(row.Index);//记录选择的索引

            dr = dt.NewRow();

            foreach (UltraGridColumn col in wg.Columns)
            {
                dr[col.Key] = row.Cells.FromKey(col.Key).Value;
            }
            dt.Rows.Add(dr);
        }


        return dt;
    }

    /// <summary>
    /// 获取grid选择的行
    /// </summary>
    /// <param name="wg"></param>
    /// <param name="indexList"></param>
    /// <param name="selectKey"></param>
    /// <returns></returns>
    public static DataTable GetGridChooseInfo2(UltraWebGrid wg, List<int> indexList, string selectKey)
    {
        DataTable dt = new DataTable();

        foreach (UltraGridColumn col in wg.Columns)
        {
            if (dt.Columns.Contains(col.Key))
                continue;
            dt.Columns.Add(col.Key);
        }

        DataRow dr;

        foreach (UltraGridRow row in wg.Rows)
        {
           
            if (Convert.ToBoolean(row.Cells.FromKey(selectKey).Text)  == false)
            {
                continue;
            }

            if (indexList != null)
                indexList.Add(row.Index);//记录选择的索引

            dr = dt.NewRow();

            foreach (UltraGridColumn col in wg.Columns)
            {
                dr[col.Key] = row.Cells.FromKey(col.Key).Value;
            }
            dt.Rows.Add(dr);
        }


        return dt;
    }

    /// <summary>
    /// 将grid的行转化为datatable
    /// </summary>
    /// <param name="rows"></param>
    /// <returns></returns>
    public static DataTable GetGridData(UltraWebGrid wg) {
        DataTable dt = new DataTable();

        foreach (UltraGridColumn col in wg.Columns)
        {
            if (dt.Columns.Contains(col.Key))
                continue;
            dt.Columns.Add(col.Key);
        }

        DataRow dr;

        foreach (UltraGridRow row in wg.Rows)
        {
            
            dr = dt.NewRow();

            foreach (UltraGridColumn col in wg.Columns)
            {
                dr[col.Key] = row.Cells.FromKey(col.Key).Value;
            }
            dt.Rows.Add(dr);
        }

        return dt;
    }

    /// <summary>
    /// 设置表格选择方式，正常列
    /// </summary>
    /// <param name="wg"></param>
    /// <param name="selectKey"></param>
    /// <param name="intType">0全选，1反选，2全不选</param>
    public static void ResetGridCheckStatus(UltraWebGrid wg,string selectKey,int intType) {
        for (int i = 0; i < wg.Rows.Count; i++)
        {
            UltraGridCell ckSelect   = wg.Rows[i].Cells.FromKey(selectKey);

            if (ckSelect.Column.AllowUpdate ==0 || ckSelect.Column.Hidden == true || ckSelect.AllowEditing == AllowEditing.No)
            {
                continue;
            }

            if (intType == 0)
            {
                ckSelect.Value = true;
            }
            else if (intType == 1)
            {
                if (ckSelect.Text.ToLower()=="true")
                    ckSelect.Value = false;
                else
                    ckSelect.Value = true;
            }
            else if (intType == 2)
            {
                ckSelect.Value = false;
            }



            }
    }

    /// <summary>
    /// 设置表格选择方式,临时模板列
    /// </summary>
    /// <param name="wg"></param>
    /// <param name="selectKey"></param>
    /// <param name="intType">0全选，1反选，2全不选</param>
    public static void ResetGridCheckStatus2(UltraWebGrid wg, string selectKey, int intType)
    {
        TemplatedColumn temCell = (TemplatedColumn)wg.Columns.FromKey(selectKey);

        for (int i = 0; i < wg.Rows.Count; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl(selectKey);
            
            if (intType == 0)
            {
                ckSelect.Checked = true;
            }
            else if (intType == 1)
            {
                if ((bool)ckSelect.Checked)
                    ckSelect.Checked = false;
                else
                    ckSelect.Checked = true;
            }
            else if (intType == 2)
            {
                ckSelect.Checked = false;
            }
        }
    }

    /// <summary>
    /// 日期格式验证
    /// </summary>
    public static bool VlidateDateFormat(string txtDate,string strDate,DateTime date,string strMessage) {
        bool result = false;
        

        if(String.IsNullOrWhiteSpace(strDate)){
            strMessage = String.Format("请输入{0}", txtDate);
            return result;        }

        if( DateTime.TryParse(strDate,out date)) {
            strMessage = String.Format("{0}格式错误", txtDate);
            return result;
       }

        result = true;

        return result;

    }



}

