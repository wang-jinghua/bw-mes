﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

public class XMLParse
{
    /// <summary>
    /// 将字符串转化成XmlDocument对象
    /// </summary>
    /// <param name="xml"></param>
    /// <returns></returns>
    public static XmlDocument getDocument(String xml)
    {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(xml);
        return doc;
    }

    /// <summary>
    /// 加载XML文件到XmlDocument对象
    /// </summary>
    /// <param name="filePath"></param>
    /// <returns></returns>
    public static XmlDocument loadDocument(String filePath)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(filePath);
        return doc;
    }

    /// <summary>
    /// 获取节点集合
    /// </summary>
    /// <param name="document"></param>
    /// <param name="nodeXPath"></param>
    /// <returns></returns>
    public static XmlNodeList getNodeList(XmlDocument document, string nodeXPath)
    {
        XmlElement root = document.DocumentElement;
        return root.SelectNodes(nodeXPath);
    }

    /// <summary>
    /// 获取单一节点
    /// </summary>
    /// <param name="document"></param>
    /// <param name="nodeXPath"></param>
    /// <returns></returns>
    public static XmlNode getNode(XmlDocument document, string nodeXPath)
    {
        XmlElement root = document.DocumentElement;
        return root.SelectSingleNode(nodeXPath);
    }

    /// <summary>
    /// 获取单一节点
    /// </summary>
    /// <param name="document"></param>
    /// <param name="nodeXPath"></param>
    /// <returns></returns>
    public static XmlElement getElement(XmlDocument document, string nodeXPath)
    {
        XmlElement root = document.DocumentElement;
        return (XmlElement)root.SelectSingleNode(nodeXPath);
    }
}