﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
///INormalReport 的摘要说明
/// </summary>
public interface INormalReport
{
      System.Web.SessionState.HttpSessionState Session { get; }
      Dictionary<string,string> GetQuery();
      void QueryData(Dictionary<string, string> query);
      void ResetQuery();
      
}