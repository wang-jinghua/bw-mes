﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;


    public class DecryptPasswrodXML
    {
        public List<string> ReadPasswrodXML(string fileName)
        {
            List<string> strList = new List<string>();

            try
            {
                using (TextReader reader = new StreamReader(fileName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(PassWords));
                    PassWords items = (PassWords)serializer.Deserialize(reader);

                    if (items.Items != null)
                    {
                        foreach (PassWord p in items.Items)
                        {
                            strList.Add(p.Name.ToString());                          
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return strList;
        }

        public void WritePassWordXML(List<string> strList, string fileName)
        {
            XmlDocument xd = new XmlDocument();
            using (StringWriter sw = new StringWriter())
            {
                List<PassWord> password_List = new List<PassWord>();
                for (int i = 0; i < strList.Count; i++)
                {
                    string s = strList[i].ToString();
                    PassWord p = new PassWord();
                    p.Name = s;
                    password_List.Add(p);
                }

                PassWords items = new PassWords();
                items.Items = password_List;

                XmlSerializer serializer = new XmlSerializer(typeof(PassWords));
                serializer.Serialize(sw, items);

                // Console.WriteLine(sw.ToString());
                xd.LoadXml(sw.ToString());
                xd.Save("password.xml");  
            }
        }
    }


    [XmlRoot("passwords")]
    public class PassWords
    {
        [XmlElement("password")]
        public List<PassWord> Items
        { get; set; }
    }

    public class PassWord
    {
        [XmlAttribute(AttributeName = "Str")]
        public string Name { get; set; }
    }

