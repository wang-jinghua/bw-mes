﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
///clsBuildCode 的摘要说明
/// </summary>
public class clsBuildCode
{
	public clsBuildCode()
	{
		//
		//TODO: 在此处添加构造函数逻辑
		//
	}

    //生成上下标公差的后台编码
    public String BuildToleranceCode(String strMid, String strUp, String strDown)
    { 
        String strImageHtml = "<Image>" + strMid;
        if ((strUp != "") && (strDown != ""))
        {
            strImageHtml += "<T" + strUp + "!" + strDown + ">";
        }
        else
        {
            if (strUp == "")
            {
                strImageHtml += "<L>" + strDown + "</L>";
            }
            else
            {
                strImageHtml += "<H>" + strUp + "</H>";
            }
        }

       strImageHtml = strImageHtml + "</Image>";
       return strImageHtml;
    }

    //生成粗糙度的后台编码
    public String BuildRoughnessCode(String strLeft, String strRight, int intIndex)
    {
        String strImageHtml;
        switch (intIndex)
        {
            case 0:

                strImageHtml = "<Image><R>" + strLeft + "</R></Image>";

                break;
            case 1:

                strImageHtml = "<Image><√>" + strLeft + "</√>" + strRight + "</Image>";
                break;
            case 2:
                strImageHtml = "<Image><Q></Q></Image>";
                break;
            default: 
                strImageHtml="";
                break;
        }
        return strImageHtml;
    }

}
