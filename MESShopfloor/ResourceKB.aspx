﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResourceKB.aspx.cs" Inherits="ResourceKB" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        body
        {
            padding: 0px;
            margin: 0px;
            background-color: #36648B;
            overflow: hidden;
        }
        html
        {
            padding: 0px;
            margin: 0px;
        }
        .style1
        {
            width: 92%;
            height: 70%;
        }
        .d_zz
        {
            padding: 7px;
            font-family: Microsoft YaHei;
            color: #fff;
            font-size: 20px;
            text-align: center;
        }
        .table
        {
            border-collapse: collapse;
            border: none;
            width: 100%;
        }
        .table td
        {
            border: solid #fff 2px;
            color: #fff;
            height: 30px;
            line-height: 30px;
        }
        #scroll{width:340px; overflow:hidden;margin-top:0px; color:Red; font-family:Microsoft YaHei; font-size:20px; font-weight:bold}
    </style>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="Scripts/highcharts4.0/js/highcharts.js" type="text/javascript"></script>
    <script type="text/javascript">
        var winWidth;
        var winHeight
        $(function () {
            winWidth = document.documentElement.clientWidth;
            winHeight = document.documentElement.clientHeight;
            $("#d_Content").height(winHeight);
            var d_em = $("#d_em").height();
            $("#d_emTable").height(d_em - 40);
            $("#d_emp").height(winHeight);
            tick();
        });
        function tick() {
            var date = new Date();
            var strMin = date.getMinutes();
            var strSec = date.getSeconds();
            if (strMin < 10) {
                strMin = "0" + strMin;
            }
            if (strSec < 10) {
                strSec = "0" + strSec;
            }
            var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "      " + date.getHours() + ":" + strMin + ":" + strSec;
            $("#spanTime").html(str);
            setTimeout(function () {
                tick();
            }, 1000);
        }

        function Dispatch(DispaData) {
            var arryDis = new Array();
            arryDis = DispaData.split("|");
            var arryz = eval('(' + arryDis[0] + ')');
            var arryd = eval('(' + arryDis[1] + ')');
            $('#container').highcharts({
                chart: {
                    type: 'line',
                    backgroundColor: 'rgba(0,0,0,0)'
                },
                title: {
                    text: ''+arryDis[2]+'年'+arryDis[3]+'月班产考核',
                    style: {
                        color: '#fff', //颜色
                        fontSize: '18px',  //字体
                        fontFamily: 'Microsoft YaHei'
                    }

                },
                xAxis: {
                    title: {
                        text: '单位：（日）',
                        style: {
                            color: '#fff', //颜色
                            fontSize: '12px',  //字体
                            fontFamily: 'Microsoft YaHei'
                        }
                    },
                    categories: arryd,
                    labels: {
                        y: 20, //x轴刻度往下移动20px
                        style: {
                            color: '#fff', //颜色
                            fontSize: '14px',  //字体
                            fontFamily: 'Microsoft YaHei'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: '数量（个）',
                        style: {
                            color: '#fff', //颜色
                            fontSize: '12px',  //字体
                            fontFamily: 'Microsoft YaHei'
                        }
                    },
                    labels: {
                        style: {
                            color: '#fff', //颜色
                            fontSize: '14px',  //字体
                            fontFamily: 'Microsoft YaHei'
                        }
                    }
                },
                tooltip: {
                    valueSuffix: '个'
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true,
                            color:"#fff"
                        },
                        enableMouseTracking: true

                    }
                },
                series: [{
                    name: '派工数',
                    data: arryz,
                    color: '#EEAD0E'
                }]
            });
       }

        $(function () {
            AutoInfo("", 0);
        });
        function AutoInfo(resourceName, i) {
            $("tr[ufc='1']").remove();
            $.ajax({
                url: 'Handle/ResourceKbHandle.ashx',
                data: { action: "load" },
                async: false,
                success: function (result) {
                    var arryResource = new Array();
                    arryResource = result.split(',');
                    if (i < arryResource.length) {
                        //当前加工的设备
                        $.ajax({
                            url: 'Handle/ResourceKbHandle.ashx',
                            data: { action: "resourceFirst", cname: "" + arryResource[i] + "" },
                            error: function (a, b) {
                                alert("a:" + a + "  " + "b:" + b);
                            },
                            async: false,
                            success: function (result) {
                                var json = eval('(' + result + ')');
                                if (json != null) {
                                    var currentStatus = "";
                                    $("#d_container").text(json.containername);
                                    $("#d_process").text(json.processno);
                                    $("#d_productName").text(json.productname);
                                    $("#d_description").text(json.description);
                                    $("#d_qty").text(json.qty);
                                    $("#d_specname").text(json.specname);
                                    $("#tr_group").text(json.teamname);
                                    $("#d_emplee").text(json.fullname);
                                    $("#d_xingji").empty();
                                    if (json.xingji == "") {
                                        $("#d_xingji").append("<span style='color:Yellow'></span>☆☆☆☆☆");
                                    }
                                    if (json.xingji == "1") {
                                        $("#d_xingji").append("<span style='color:Yellow'>★</span>☆☆☆☆");
                                    }
                                    if (json.xingji == "2") {
                                        $("#d_xingji").append("<span style='color:Yellow'>★★</span>☆☆☆");
                                    }
                                    if (json.xingji == "3") {
                                        $("#d_xingji").append("<span style='color:Yellow'>★★★</span>☆☆");
                                    }
                                    if (json.xingji == "4") {
                                        $("#d_xingji").append("<span style='color:Yellow'>★★★★</span>☆");
                                    }
                                    if (json.xingji == "5") {
                                        $("#d_xingji").append("<span style='color:Yellow'>★★★★★</span>");
                                    }

                                    $("#d_resourceName").text(json.resourcename);
                                    if (json.matoStatus == "报警" || json.matoStatus == "离线") {
                                        currentStatus = "<span style='color:red'>" + json.matoStatus + "</span>";
                                        if (json.matoStatus == "报警") {
                                            $("#scroll").show();
                                        }
                                        else {
                                            $("#scroll").hide();
                                        }
                                    }
                                    if (json.matoStatus == "在线" || json.matoStatus == "正在运行") {
                                        currentStatus = "" + json.matoStatus + "";
                                        $("#scroll").hide();
                                    }
                                    $("#td_active").html(currentStatus);
                                    $("#spanRunTime").text(json.runTime);
                                    $("#spanRunl").text(json.runl);
                                    $("#spankxl").text(json.kxl);
                                    $("#spankxsj").text(json.kxsj);
                                    $("#spanbjl").text(json.bjl);
                                    $("#spanbjsj").text(json.bjsj);


                                    $("#imgResource").attr("src", "Files/Resource/" + json.resourcename.replace("/", "-") + ".jpg");
                                    $("#imgEmployee").attr("src", "Files/Employee/" + json.employeename + ".jpg");
                                    //$("#imgResource").attr("src", "Files/Resource/62010100019.jpg");
                                    //$("#imgEmployee").attr("src", "Files/Employee/songhongbin.jpg");


                                }
                            }
                        });
                        //待加工的任务
                        $.ajax({
                            url: 'Handle/ResourceKbHandle.ashx',
                            data: { action: "resource", cname: "" + arryResource[i] + "" },
                            error: function (a, b) {
                                alert("a:" + a + "  " + "b:" + b);
                            },
                            async: false,
                            success: function (result) {
                                $("#tr_WaitResource").after(result);
                            }
                        });
                        //班产考核
                        $.ajax({
                            url: 'Handle/ResourceKbHandle.ashx',
                            data: { action: "dispatch", cname: "" + arryResource[i] + "" },
                            error: function (a, b) {
                                alert("a:" + a + "  " + "b:" + b);
                            },
                            async: false,
                            success: function (result) {
                                Dispatch(result);
                            }
                        });
                        setTimeout(function () {
                            var current = i + 1;
                            if (i == arryResource.length - 1) {
                                current = 0;
                            }
                            AutoInfo(arryResource[i], current);
                        }, 5000);
                    }

                }
            })

        }
        //  
    </script>
</head>
<body>
    <!--顶部开始-->
    <div style="height: 70px; width: 100%; line-height: 70px; border-bottom: 2px solid #4876FF;
        overflow: auto">
        <div style="float: left; font-family: Microsoft YaHei; font-size: 40px; color: #fff;
            font-weight: bold; padding-left: 20px">
            设备信息看板
        </div>
        <div style="float: right; font-family: Microsoft YaHei; font-size: 20px; color: #fff;
            padding-right: 20px;">
            当前时间：<span id="spanTime"></span>
        </div>
    </div>
    <!--顶部结束-->
    <div style="width: 80%; border-right: 2px solid #4876FF; float: left" id="d_Content">
        <div style="width: 65%; height: 100%; border-right: 2px solid #4876FF; float: left">
            <div style="height: 42%; border-bottom: 2px solid #4876FF; padding: 20px" id="d_em">
                <table cellpadding="0" cellspacing="0" class="style1" id="d_emTable">
                    <tr>
                        <td rowspan="5" style="width: 332px; text-align:center">
                            <img  style="height: 250px; width: 332px; border:4px solid #fff;"   id="imgResource"/></td>
                        <td style="width: 340px; text-align: left; padding-left: 30px; font-family: Microsoft YaHei;
                            color: #fff; font-size: 30px" id="tr_group" colspan="2">
                            
                        </td>
                    </tr>
                    <tr style="width: 340px; font-family: Microsoft YaHei; color: #00FF00; font-size: 30px">
                        <td style="text-align: left; padding-left: 30px;" id="td_active" colspan="2">
                            
                        </td>
                    </tr>
                    <tr style="width: 340px; font-family: Microsoft YaHei; color: #00FF00; font-size: 20px">
                        <td style="text-align: left; padding-left: 30px;">
                            运行率：<span style="color: #FFB90F; font-size: 30px"><span id="spanRunl"></span>%</span>
                        </td>
                        <td style="text-align: left; padding-left: 30px;">
                            运行时间：<span style="color: #FFB90F; font-size: 30px"><span id="spanRunTime"></span>h</span></td>
                    </tr>
                    <tr style="width: 340px; font-family: Microsoft YaHei; color: #00FF00; font-size: 20px">
                        <td style="text-align: left; padding-left: 30px;">
                            空闲率：<span style="color: #FFB90F; font-size: 30px"><span id="spankxl"></span>%</span>
                        </td>
                        <td style="text-align: left; padding-left: 30px;">
                            空闲时间：<span style="color: #FFB90F; font-size: 30px"><span id="spankxsj"></span>h</span></td>
                    </tr>
                    <tr style="width: 340px; font-family: Microsoft YaHei; color: #00FF00; font-size: 20px">
                        <td style="text-align: left; padding-left: 30px;">
                            报警率：<span style="color: #FFB90F; font-size: 30px"><span id="spanbjl"></span>%</span></td>
                        <td style="text-align: left; padding-left: 30px;">
                             报警时间：<span style="color: #FFB90F; font-size: 30px"><span id="spanbjsj"></span>h</span></td>
                    </tr>
                    <tr>
                        <td align="center" style="height: 40px; line-height: 40px; font-family: 黑体; font-size: 30px;
                            font-weight: bold; color: #FFB90F" id="d_resourceName">
                            
                        </td>
                        <td colspan="2">
                    <div id="scroll" style="display:none">警告：设备发生预警，请及时检查！</div>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height: 40%; text-align: center">
                <div style="padding: 10px; font-family: Microsoft YaHei; color: #EE7600; font-size: 25px;"
                    align="left">
                    待制任务：
                </div>
                <div style="padding: 10px">
                    <table cellpadding="0" cellspacing="0" class="table" cellpadding="0" cellspacing="0">
                        <tr style="background-color: #A1A1A1; color: #fff; font-family: Microsoft YaHei;
                            font-weight: bold" id="tr_WaitResource">
                            <td>
                                序号
                            </td>
                            <td>
                                批次
                            </td>
                            <td>
                                工号
                            </td>
                            <td>
                                产品名称
                            </td>
                            <td>
                                数量
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="width: 34.8%; height: 100%; float: left">
            <div style="height: 42%; border-bottom: 2px solid #4876FF; text-align: left; padding: 20px">
                <div style="padding: 10px; padding-top: 0px; font-family: Microsoft YaHei; color: #EE7600;
                    font-size: 25px;">
                    在制任务：
                </div>
                <div class="d_zz" id="d_process">
                    
                </div>
                <div class="d_zz" id="d_productName">
                 
                </div>
                <div class="d_zz" id="d_description">
                   
                </div>
                <div class="d_zz" id="d_container">
                 
                </div>
                <div class="d_zz" id="d_qty">
                </div>
                <div class="d_zz"  id="d_specname">
                </div>
            </div>
            <div style="height: 40%; text-align: center">
                <div id="container" style="height: 100%">
                </div>
            </div>
        </div>
    </div>
    <div style="float: left; width: 19%;" id="d_emp">
        <div align="center" style="margin-top: 30px; height:60%">
            <img id="imgEmployee" style="width: 280px; height: 100%; border: 4px solid #fff" />
        </div>
        <div align="center" style="margin-top: 20px; font-family: 黑体; font-size: 40px; font-weight: bold;
            color: #FFB90F" id="d_emplee">
            
        </div>
        <div align="center" style="margin-top: 20px; font-family: 黑体; font-size: 30px; font-weight: bold;
            color: #FFF" id="d_xingji">
            
        </div>
    </div>
</body>
</html>

