﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="wucToleranceInput.ascx.cs"
    Inherits="wucToleranceInput" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7DD5C3163F2CD0CB"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<script language="javascript" type="text/javascript">
    function ResumeError() {
        return true;
    }
    window.onerror = ResumeError;

    function insertValue(field) {

        FTB_API['<%=ftbFinalHtml.ClientID%>'].Focus();


        FTB_API['<%=ftbFinalHtml.ClientID%>'].InsertHtml(field);

        window.event.returnValue = false;

        FTB_API['<%=ftbFinalHtml.ClientID%>'].Focus();

    }

    function insertValue2(field) {

        document.getElementById("<%=txtHtmlTemp.ClientID %>").focus();
        document.execCommand("paste", 0, field);
    }

    function insertValue3(field) {


        field = "<img src='" + field + "'>";

        FTB_API['<%=ftbHtmlTemp.ClientID%>'].Focus();

        FTB_API['<%=ftbHtmlTemp.ClientID%>'].InsertHtml(field);

        window.event.returnValue = false;

        FTB_API['<%=ftbHtmlTemp.ClientID%>'].Focus();

    }
    
</script>
<body onload="ResumeError()">
    <div>
        <table>
            <tr>
                <td valign="bottom">
                    <igtab:UltraWebTab ID="uwtMenu" runat="server" Width="440px" Height="200px" Font-Size="10pt" SelectedTab="3">
                        <Tabs>
                            <igtab:Tab Text="尺寸公差">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 50px" rowspan="5">
                                            </td>
                                            <td style="height: 25px">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUp" runat="server" Text="上公差"></asp:Label>
                                                <asp:TextBox ID="txtUp" runat="server" Width="60px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblMid" runat="server" Text="基础数据"></asp:Label>
                                                <asp:DropDownList ID="ddlSpecial" runat="server">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtMid" runat="server" Width="60px"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDown" runat="server" Text="下公差"></asp:Label>
                                                <asp:TextBox ID="txtDown" runat="server" Width="60px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnInsert" runat="server" Text="确定" OnClick="btnInsert_Click" Width="80px"
                                                    CssClass="SubmitButton" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="粗糙度">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 100px; width: 100px; background-repeat: no-repeat;">
                                                <div id="divRoughness1" runat="server" style="background: url('../../images/CheckPointImage/rorghness1.png') center no-repeat;
                                                    width: 100%; height: 100%;">
                                                    <asp:TextBox ID="txtNoMaterialLeft" runat="server" Font-Size="8pt" Style="position: relative;
                                                        top: 32px; left: 22px;" Width="36px"></asp:TextBox>
                                                </div>
                                            </td>
                                            <td style="background-repeat: no-repeat; height: 100px; width: 120px">
                                                <div id="divRoughness0" runat="server" style="background: url('../../images/CheckPointImage/rorghness0.png') center no-repeat;
                                                    width: 100%; height: 100%;">
                                                    <table>
                                                        <tr>
                                                            <td rowspan="2">
                                                                <asp:TextBox ID="txtHaveTechniqueLeft" runat="server" Width="36px" Font-Size="8pt"
                                                                    Style="position: relative; top: 15px; left: 20px;"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtHaveTechniqueUp" runat="server" Width="36px" Font-Size="8pt"
                                                                    Style="position: relative; top: -2px; left: 40px;"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtHaveTechniqueDown" runat="server" Width="36px" Font-Size="8pt"
                                                                    Style="position: relative; top: 1px; left: 40px;"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                            <td style="background-repeat: no-repeat; height: 100px; width: 100px">
                                                <div id="divRoughness2" runat="server" style="background: url('../../images/CheckPointImage/rorghness2.png') center no-repeat;
                                                    width: 100%; height: 100%;">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rbNoMaterial" runat="server" Text="去除材料" AutoPostBack="true"
                                                    OnCheckedChanged="rbNoMaterial_CheckedChanged" Width="120px" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="rbHaveTechnique" runat="server" Text="指定加工方法" AutoPostBack="true"
                                                    OnCheckedChanged="rbHaveTechnique_CheckedChanged" Width="120px" />
                                            </td>
                                            <td>
                                                <asp:RadioButton ID="rbHaveMaterial" runat="server" Text="不去除材料" AutoPostBack="true"
                                                    OnCheckedChanged="rbHaveMaterial_CheckedChanged" Width="120px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="btnDrawRoughness" runat="server" Text="确定" Width="80px" CssClass="SubmitButton"
                                                    OnClick="btnDrawRoughness_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="特殊符号">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn1" runat="server" Text="Φ" Width="40px" />
                                                <asp:Button ID="btn2" runat="server" Text="▽" Width="40px" />
                                                <asp:Button ID="btn3" runat="server" Text="×" Width="40px" />
                                                <asp:Button ID="btn4" runat="server" Text="±" Width="40px" />
                                                <asp:Button ID="btn5" runat="server" Text="°" Width="40px" />
                                                <asp:Button ID="btn6" runat="server" Text="δ" Width="40px" />
                                                <asp:Button ID="btn7" runat="server" Text="~" Width="40px" />
                                                <asp:Button ID="btn8" runat="server" Text="①" Width="40px" />
                                                <asp:Button ID="btn9" runat="server" Text="②" Width="40px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn10" runat="server" Text="③" Width="40px" />
                                                <asp:Button ID="btn11" runat="server" Text="④" Width="40px" />
                                                <asp:Button ID="btn12" runat="server" Text="⑤" Width="40px" />
                                                <asp:Button ID="btn13" runat="server" Text="⑥" Width="40px" />
                                                <asp:Button ID="btn14" runat="server" Text="Ⅰ" Width="40px" />
                                                <asp:Button ID="btn15" runat="server" Text="Ⅱ" Width="40px" />
                                                <asp:Button ID="btn16" runat="server" Text="Ⅲ" Width="40px" />
                                                <asp:Button ID="btn17" runat="server" Text="Ⅳ" Width="40px" />
                                                <asp:Button ID="btn18" runat="server" Text="Ⅴ" Width="40px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btn19" runat="server" Text="℃" Width="40px" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="形位公差符号">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnStart3" runat="server" ImageUrl="~/Images/CheckPointImage/start3.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnStart" runat="server" ImageUrl="~/Images/CheckPointImage/start.png"
                                                    BorderStyle="Solid" BorderWidth="1px" Visible="false" />
                                                <asp:ImageButton ID="btnZhixiandu" runat="server" ImageUrl="~/Images/CheckPointImage/zhixiandu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnPinmiandu" runat="server" ImageUrl="~/Images/CheckPointImage/pinmiandu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" OnClick="btnPinmiandu_Click" />
                                                <asp:ImageButton ID="btnYuandu" runat="server" ImageUrl="~/Images/CheckPointImage/yuandu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnYuanzhudu" runat="server" ImageUrl="~/Images/CheckPointImage/yuanzhudu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnXianlunkuo" runat="server" ImageUrl="~/Images/CheckPointImage/xianlunkuo.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnMianlunkuo" runat="server" ImageUrl="~/Images/CheckPointImage/mianlunkuo.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnQinxiedu" runat="server" ImageUrl="~/Images/CheckPointImage/qinxiedu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                            </td>
                                            <td rowspan="6">
                                                <FTB:FreeTextBox ID="ftbHtmlTemp" runat="Server" ToolbarLayout="..." EnableToolbars="False"
                                                    Height="160px" Width="220px" EnableHtmlMode="False" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnCuizhidu" runat="server" ImageUrl="~/Images/CheckPointImage/cuizhidu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnStart2" runat="server" ImageUrl="~/Images/CheckPointImage/start2.png"
                                                    Visible="false" BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnPinxindu" runat="server" ImageUrl="~/Images/CheckPointImage/pinxindu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnWeizhidu" runat="server" ImageUrl="~/Images/CheckPointImage/weizhidu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnAxis" runat="server" ImageUrl="~/Images/CheckPointImage/axis.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnDuichendu" runat="server" ImageUrl="~/Images/CheckPointImage/duichendu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnYuantiaodong" runat="server" ImageUrl="~/Images/CheckPointImage/yuantiaodong.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnDiameter" runat="server" ImageUrl="~/Images/CheckPointImage/diameter.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnQiuzhijin" runat="server" ImageUrl="~/Images/CheckPointImage/qiuzhijin.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnM" runat="server" ImageUrl="~/Images/CheckPointImage/M.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnL" runat="server" ImageUrl="~/Images/CheckPointImage/L.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnS" runat="server" ImageUrl="~/Images/CheckPointImage/S.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnP" runat="server" ImageUrl="~/Images/CheckPointImage/P.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnT" runat="server" ImageUrl="~/Images/CheckPointImage/T.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnST" runat="server" ImageUrl="~/Images/CheckPointImage/ST.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnF1" runat="server" ImageUrl="~/Images/CheckPointImage/F1.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnE1" runat="server" ImageUrl="~/Images/CheckPointImage/E1.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/CheckPointImage/C.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnStart4" runat="server" ImageUrl="~/Images/CheckPointImage/start4.png"
                                                    Visible="false" BorderStyle="Solid" BorderWidth="1px" Height="20px" />
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/CheckPointImage/B.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/CheckPointImage/D.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/CheckPointImage/E.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/CheckPointImage/G.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/CheckPointImage/f.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/CheckPointImage/h.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/CheckPointImage/zhfu.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/CheckPointImage/du.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="btnQuantiaodong" runat="server" ImageUrl="~/Images/CheckPointImage/quantiaodong.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/CheckPointImage/J.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/CheckPointImage/(.png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                                <asp:ImageButton ID="ImageButton12" runat="server" ImageUrl="~/Images/CheckPointImage/).png"
                                                    BorderStyle="Solid" BorderWidth="1px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtHtmlTemp" runat="server" TextMode="MultiLine" Height="80px" Width="220px"
                                                    Visible="False"></asp:TextBox>
                                                <asp:Button ID="btnCreate" runat="server" Text="确定" Width="80px" CssClass="SubmitButton"
                                                    OnClick="btnCreate_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </igtab:Tab>
                        </Tabs>
                    </igtab:UltraWebTab>
                </td>
                <td valign="bottom">
                    <FTB:FreeTextBox ID="ftbFinalHtml" runat="Server" ToolbarLayout="........" Width="350px"
                        Height="192px" EnableHtmlMode="False" EnableToolbars="False" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtFinalHtml" runat="server" Visible="false"> </asp:TextBox>
                    <asp:Button ID="btnClear" runat="server" Text="清理" OnClick="btnClear_Click" Visible="false" Width="0px" />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
