﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using uMESExternalControl.ToleranceInputLib;
using System.Drawing;

public partial class wucToleranceInput : System.Web.UI.UserControl
{

    string webRootDir = HttpRuntime.AppDomainAppPath;
    string webRootUrl = "~";

    Bitmap objBmp;
    public delegate void inputhandler(object sender, EventArgs e);
    public event inputhandler AfterInputData;

    public void ParseCode(String strPreviewCode)
    {
        try
        {
            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath =webRootDir+ ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    ftbFinalHtml.Text += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir+ ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (Session["dtImage"] == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            Session["dtImage"] = dtImage;
                        }
                        else
                        {
                            dtImage = (DataTable)Session["dtImage"];
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        Session["dtImage"] = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl+ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        ftbFinalHtml.Text += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                ftbFinalHtml.Text += strPreviewCode;
            }
        }
        catch (Exception myError)
        {
            lblMessage.Text = myError.Message;
        }

    }

    public void ParseCode(String strPreviewCode,ref DataTable dtImageAllInfo,ref string strPreviewCodeDis) {
        try
        {
            dtImageAllInfo = new DataTable();
            dtImageAllInfo.Columns.Add("FileName");
            dtImageAllInfo.Columns.Add("HtmlCode");

            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath = webRootDir + ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    strPreviewCodeDis += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir + ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (dtImageAllInfo == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            dtImageAllInfo = dtImage;
                        }
                        else
                        {
                            dtImage = dtImageAllInfo;
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        dtImageAllInfo = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        strPreviewCodeDis += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                strPreviewCodeDis += strPreviewCode;
            }
        }
        catch (Exception myError)
        {
            lblMessage.Text = myError.Message;
        }

    }

    public void ClearImageTemp()
    {

        String strFileName, strFilePath, strFileDoc;
        int i, intCount;
        System.IO.FileInfo oFile;
        DataTable dtImage = new DataTable();
        dtImage = (DataTable)Session["dtImage"];
        // strFileDoc = MapPath("~") + @"\ImageTemp\";
        //clsCon oCon = new clsCon();
        String strImageTempPath;
        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
        strImageTempPath =webRootDir+ ConfigurationManager.AppSettings["ImageTempPath"].ToString();
        strFileDoc = strImageTempPath;
        ftbFinalHtml.Text = "";
        if (dtImage != null)
        {
            if (dtImage.Rows.Count > 0)
            {
                intCount = dtImage.Rows.Count;
                for (i = 0; i < intCount; i++)
                {
                    strFileName = (String)dtImage.Rows[i][0];
                    strFilePath = strFileDoc + strFileName;
                    oFile = new System.IO.FileInfo(strFilePath);
                    oFile.Delete();
                }
                dtImage.Rows.Clear();

            }
        }

    }

    public String GetCode()
    {
        String strHtmlCode, strHtml, strHtmlTemp;
        try
        {
            DataTable dtImage = new DataTable();
            int i, intStartFlag, intEndFlag;
            dtImage = (DataTable)Session["dtImage"];

            strHtml = ftbFinalHtml.Text.Trim();
            strHtmlCode = strHtml;

            intStartFlag = strHtmlCode.IndexOf("<img");
            if (intStartFlag == -1)
            {
                intStartFlag = strHtmlCode.IndexOf("<IMG");
            }
            if (intStartFlag > -1)
            {
                intEndFlag = strHtmlCode.IndexOf(">", intStartFlag);
                ;
            }
            else
            {
                intEndFlag = -1;
            }

            if (dtImage != null)
            {
                if (dtImage.Rows.Count > 0)
                {
                    while (intStartFlag > -1)
                    {
                        strHtmlTemp = strHtmlCode.Substring(intStartFlag, intEndFlag - intStartFlag + 1);
                        for (i = 0; i <= dtImage.Rows.Count - 1; i++)
                        {
                            if (strHtmlTemp.IndexOf(@"/" + dtImage.Rows[i][0]) > -1)
                            {
                                strHtmlCode = strHtmlCode.Replace(strHtmlTemp, (String)dtImage.Rows[i][1]);
                                break;
                            }

                        }

                        intStartFlag = strHtmlCode.IndexOf("<img", intStartFlag + 1);
                        if (intStartFlag == -1)
                        {
                            intStartFlag = strHtmlCode.IndexOf("<IMG", intStartFlag + 1);
                        }
                        if (intStartFlag > -1)
                        {
                            intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                            ;
                        }
                        else
                        {
                            intEndFlag = -1;
                        }
                        //intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                    }

                }

            }

            return strHtmlCode;
        }
        catch (Exception myError)
        {

            lblMessage.Text = myError.Message;
            return "";
        }
    }

    //String GetLocalPath(String strPath)
    //{
    //    String strPathTemp;
    //    if (strPath == "")
    //    {
    //        return "";
    //    }
    //    else
    //    {
    //        strPathTemp = strPath.Trim().Substring(1);
    //        //strPathTemp = strPathTemp.Replace(@"/", @"\");
    //        //clsCon oCon = new clsCon();
    //        String strImagePath;
    //        //strImagePath = oCon.LoadConfigString("ImageGetPath");
    //        strImagePath = ConfigurationManager.AppSettings["ImageGetPath"].ToString();

    //        strImagePath = strImagePath.Substring(0, strImagePath.Length - 8);
    //        strImagePath = strImagePath.Replace(@"\", @"/");
    //        strPathTemp = @strImagePath + @strPathTemp;
    //        return strPathTemp;
    //    }
    //}

    protected String strRorghness0;// = @"C:\Program Files\Camstar\InSite Web Application\InSiteWebApplication\Images\rorghness0.png";
    protected String strRorghness1;// = @"C:\Program Files\Camstar\InSite Web Application\InSiteWebApplication\Images\rorghness1.png";
    protected String strRorghness2;// = @"C:\Program Files\Camstar\InSite Web Application\InSiteWebApplication\Images\rorghness2.png";


    protected void Page_Load(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        try
        {

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");
            strImagePath = ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strRorghness0 = @strImagePath + @"rorghness0.png";
            strRorghness1 = @strImagePath + @"rorghness1.png";
            strRorghness2 = @strImagePath + @"rorghness2.png";
            if (Page.IsPostBack == false)
            {

                ddlSpecial.Items.Clear();
                ddlSpecial.Items.Add("");
                ddlSpecial.Items.Add("±");
                ddlSpecial.Items.Add("Φ");
                ddlSpecial.Items.Add("R");
                ddlSpecial.Items.Add("SR");
                ddlSpecial.Items.Add("M");
                ddlSpecial.SelectedIndex = 0;
                //tabMenu.ActiveTabIndex = 0;
                uwtMenu.SelectedTab = 0;
                Session["intMaxImageIndex"] = 0;

                DataTable dtImage = new DataTable();
                dtImage.Columns.Add("FileName");
                dtImage.Columns.Add("HtmlCode");
                Session["dtImage"] = dtImage;
                //string fieldId = Guid.NewGuid().ToString();

                btn1.Attributes.Add("OnClick", "insertValue('" + btn1.Text + "')");
                btn2.Attributes.Add("OnClick", "insertValue('" + btn2.Text + "')");
                btn3.Attributes.Add("OnClick", "insertValue('" + btn3.Text + "')");
                btn4.Attributes.Add("OnClick", "insertValue('" + btn4.Text + "')");
                btn5.Attributes.Add("OnClick", "insertValue('" + btn5.Text + "')");
                btn6.Attributes.Add("OnClick", "insertValue('" + btn6.Text + "')");
                btn7.Attributes.Add("OnClick", "insertValue('" + btn7.Text + "')");
                btn8.Attributes.Add("OnClick", "insertValue('" + btn8.Text + "')");
                btn9.Attributes.Add("OnClick", "insertValue('" + btn9.Text + "')");
                btn10.Attributes.Add("OnClick", "insertValue('" + btn10.Text + "')");
                btn11.Attributes.Add("OnClick", "insertValue('" + btn11.Text + "')");
                btn12.Attributes.Add("OnClick", "insertValue('" + btn12.Text + "')");
                btn13.Attributes.Add("OnClick", "insertValue('" + btn13.Text + "')");
                btn14.Attributes.Add("OnClick", "insertValue('" + btn14.Text + "')");
                btn15.Attributes.Add("OnClick", "insertValue('" + btn15.Text + "')");
                btn16.Attributes.Add("OnClick", "insertValue('" + btn16.Text + "')");
                btn17.Attributes.Add("OnClick", "insertValue('" + btn17.Text + "')");
                btn18.Attributes.Add("OnClick", "insertValue('" + btn18.Text + "')");
                btn19.Attributes.Add("OnClick", "insertValue('" + btn19.Text + "')");

                String strFilePath;
                strFilePath = btnStart3.ImageUrl.Replace("~", "..");

                //btnStart3.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnStart3.ImageUrl) + "')");
                //btnZhixiandu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnZhixiandu.ImageUrl) + "')");
                //btnPinmiandu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnPinmiandu.ImageUrl) + "')");
                //btnYuandu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnYuandu.ImageUrl) + "')");
                //btnYuanzhudu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnYuanzhudu.ImageUrl) + "')");
                //btnXianlunkuo.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnXianlunkuo.ImageUrl) + "')");
                //btnMianlunkuo.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnMianlunkuo.ImageUrl) + "')");
                //btnQinxiedu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnQinxiedu.ImageUrl) + "')");
                //btnCuizhidu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnCuizhidu.ImageUrl) + "')");
                //btnStart2.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnStart2.ImageUrl) + "')");
                //btnPinxindu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnPinxindu.ImageUrl) + "')");
                //btnWeizhidu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnWeizhidu.ImageUrl) + "')");
                //btnAxis.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnAxis.ImageUrl) + "')");
                //btnDuichendu.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnDuichendu.ImageUrl) + "')");
                //btnYuantiaodong.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnYuantiaodong.ImageUrl) + "')");
                //btnDiameter.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnDiameter.ImageUrl) + "')");
                //btnQiuzhijin.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnQiuzhijin.ImageUrl) + "')");
                //btnM.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnM.ImageUrl) + "')");
                //btnL.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnL.ImageUrl) + "')");
                //btnS.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnS.ImageUrl) + "')");
                //btnP.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnP.ImageUrl) + "')");
                //btnT.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnT.ImageUrl) + "')");
                //btnST.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnST.ImageUrl) + "')");
                //btnF1.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnF1.ImageUrl) + "')");
                //btnE1.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnE1.ImageUrl) + "')");
                //ImageButton1.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton1.ImageUrl) + "')");
                //ImageButton2.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton2.ImageUrl) + "')");
                //ImageButton3.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton3.ImageUrl) + "')");
                //ImageButton4.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton4.ImageUrl) + "')");
                //ImageButton5.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton5.ImageUrl) + "')");
                //ImageButton6.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton6.ImageUrl) + "')");
                //ImageButton7.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton7.ImageUrl) + "')");
                //ImageButton8.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton8.ImageUrl) + "')");
                //ImageButton9.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton9.ImageUrl) + "')");
                //ImageButton10.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton10.ImageUrl) + "')");
                //ImageButton11.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton11.ImageUrl) + "')");
                //ImageButton12.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(ImageButton12.ImageUrl) + "')");
                //btnQuantiaodong.Attributes.Add("OnClick", "insertValue3('" + @GetLocalPath(btnQuantiaodong.ImageUrl) + "')");


                //btnStart3.Attributes.Add("OnClick", "insertValue3('" + btnStart3.ImageUrl.Replace("~", "../../..") + "')");
                //btnStart.Attributes.Add("OnClick", "insertValue3('" + btnStart.ImageUrl.Replace("~", "../../..") + "')");
                //btnZhixiandu.Attributes.Add("OnClick", "insertValue3('" + btnZhixiandu.ImageUrl.Replace("~", "../../..") + "')");
                //btnPinmiandu.Attributes.Add("OnClick", "insertValue3('" + btnPinmiandu.ImageUrl.Replace("~", "../../..") + "')");
                //btnYuandu.Attributes.Add("OnClick", "insertValue3('" + btnYuandu.ImageUrl.Replace("~", "../../..") + "')");
                //btnYuanzhudu.Attributes.Add("OnClick", "insertValue3('" + btnYuanzhudu.ImageUrl.Replace("~", "../../..") + "')");
                //btnXianlunkuo.Attributes.Add("OnClick", "insertValue3('" + btnXianlunkuo.ImageUrl.Replace("~", "../../..") + "')");
                //btnMianlunkuo.Attributes.Add("OnClick", "insertValue3('" + btnMianlunkuo.ImageUrl.Replace("~", "../../..") + "')");
                //btnQinxiedu.Attributes.Add("OnClick", "insertValue3('" + btnQinxiedu.ImageUrl.Replace("~", "../../..") + "')");
                //btnCuizhidu.Attributes.Add("OnClick", "insertValue3('" + btnCuizhidu.ImageUrl.Replace("~", "../../..") + "')");
                //btnStart2.Attributes.Add("OnClick", "insertValue3('" + btnStart2.ImageUrl.Replace("~", "../../..") + "')");
                //btnPinxindu.Attributes.Add("OnClick", "insertValue3('" + btnPinxindu.ImageUrl.Replace("~", "../../..") + "')");
                //btnWeizhidu.Attributes.Add("OnClick", "insertValue3('" + btnWeizhidu.ImageUrl.Replace("~", "../../..") + "')");
                //btnAxis.Attributes.Add("OnClick", "insertValue3('" + btnAxis.ImageUrl.Replace("~", "../../..") + "')");
                //btnDuichendu.Attributes.Add("OnClick", "insertValue3('" + btnDuichendu.ImageUrl.Replace("~", "../../..") + "')");

                //btnYuantiaodong.Attributes.Add("OnClick", "insertValue3('" + btnYuantiaodong.ImageUrl.Replace("~", "../../..") + "')");
                //btnDiameter.Attributes.Add("OnClick", "insertValue3('" + btnDiameter.ImageUrl.Replace("~", "../../..") + "')");
                //btnQiuzhijin.Attributes.Add("OnClick", "insertValue3('" + btnQiuzhijin.ImageUrl.Replace("~", "../../..") + "')");

                //btnM.Attributes.Add("OnClick", "insertValue3('" + btnM.ImageUrl.Replace("~", "../../..") + "')");
                //btnL.Attributes.Add("OnClick", "insertValue3('" + btnL.ImageUrl.Replace("~", "../../..") + "')");
                //btnS.Attributes.Add("OnClick", "insertValue3('" + btnS.ImageUrl.Replace("~", "../../..") + "')");
                //btnP.Attributes.Add("OnClick", "insertValue3('" + btnP.ImageUrl.Replace("~", "../../..") + "')");
                //btnT.Attributes.Add("OnClick", "insertValue3('" + btnT.ImageUrl.Replace("~", "../../..") + "')");
                //btnST.Attributes.Add("OnClick", "insertValue3('" + btnST.ImageUrl.Replace("~", "../../..") + "')");
                //btnF1.Attributes.Add("OnClick", "insertValue3('" + btnF1.ImageUrl.Replace("~", "../../..") + "')");
                //btnE1.Attributes.Add("OnClick", "insertValue3('" + btnE1.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton1.Attributes.Add("OnClick", "insertValue3('" + ImageButton1.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton2.Attributes.Add("OnClick", "insertValue3('" + ImageButton2.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton3.Attributes.Add("OnClick", "insertValue3('" + ImageButton3.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton4.Attributes.Add("OnClick", "insertValue3('" + ImageButton4.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton5.Attributes.Add("OnClick", "insertValue3('" + ImageButton5.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton6.Attributes.Add("OnClick", "insertValue3('" + ImageButton6.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton7.Attributes.Add("OnClick", "insertValue3('" + ImageButton7.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton8.Attributes.Add("OnClick", "insertValue3('" + ImageButton8.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton9.Attributes.Add("OnClick", "insertValue3('" + ImageButton9.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton10.Attributes.Add("OnClick", "insertValue3('" + ImageButton10.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton11.Attributes.Add("OnClick", "insertValue3('" + ImageButton11.ImageUrl.Replace("~", "../../..") + "')");
                //ImageButton12.Attributes.Add("OnClick", "insertValue3('" + ImageButton12.ImageUrl.Replace("~", "../../..") + "')");
                //btnQuantiaodong.Attributes.Add("OnClick", "insertValue3('" + btnQuantiaodong.ImageUrl.Replace("~", "../../..") + "')");

                //string aa;
                //aa = ResolveUrl(btnStart3.ImageUrl);

                btnStart3.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnStart3.ImageUrl) + "')");
                btnStart.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnStart.ImageUrl) + "')");
                btnZhixiandu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnZhixiandu.ImageUrl) + "')");
                btnPinmiandu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnPinmiandu.ImageUrl) + "')");
                btnYuandu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnYuandu.ImageUrl) + "')");
                btnYuanzhudu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnYuanzhudu.ImageUrl) + "')");
                btnXianlunkuo.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnXianlunkuo.ImageUrl) + "')");
                btnMianlunkuo.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnMianlunkuo.ImageUrl) + "')");
                btnQinxiedu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnQinxiedu.ImageUrl) + "')");
                btnCuizhidu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnCuizhidu.ImageUrl) + "')");
                btnStart2.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnStart2.ImageUrl) + "')");
                btnPinxindu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnPinxindu.ImageUrl) + "')");
                btnWeizhidu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnWeizhidu.ImageUrl) + "')");
                btnAxis.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnAxis.ImageUrl) + "')");
                btnDuichendu.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnDuichendu.ImageUrl) + "')");

                btnYuantiaodong.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnYuantiaodong.ImageUrl) + "')");
                btnDiameter.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnDiameter.ImageUrl) + "')");
                btnQiuzhijin.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnQiuzhijin.ImageUrl) + "')");

                btnM.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnM.ImageUrl) + "')");
                btnL.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnL.ImageUrl) + "')");
                btnS.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnS.ImageUrl) + "')");
                btnP.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnP.ImageUrl) + "')");
                btnT.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnT.ImageUrl) + "')");
                btnST.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnST.ImageUrl) + "')");
                btnF1.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnF1.ImageUrl) + "')");
                btnE1.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnE1.ImageUrl) + "')");
                ImageButton1.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton1.ImageUrl) + "')");
                ImageButton2.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton2.ImageUrl) + "')");
                ImageButton3.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton3.ImageUrl) + "')");
                ImageButton4.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton4.ImageUrl) + "')");
                ImageButton5.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton5.ImageUrl) + "')");
                ImageButton6.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton6.ImageUrl) + "')");
                ImageButton7.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton7.ImageUrl) + "')");
                ImageButton8.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton8.ImageUrl) + "')");
                ImageButton9.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton9.ImageUrl) + "')");
                ImageButton10.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton10.ImageUrl) + "')");
                ImageButton11.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton11.ImageUrl) + "')");
                ImageButton12.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(ImageButton12.ImageUrl) + "')");
                btnQuantiaodong.Attributes.Add("OnClick", "insertValue3('" + ResolveUrl(btnQuantiaodong.ImageUrl) + "')");

                divRoughness0.Style["background-image"] = ResolveUrl("~/images/CheckPointImage/rorghness0.png");
                divRoughness1.Style["background-image"] = ResolveUrl("~/images/CheckPointImage/rorghness1.png");
                divRoughness2.Style["background-image"] = ResolveUrl("~/images/CheckPointImage/rorghness2.png");

            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
        finally
        {
        }

    }


    protected void Page_UnLoad(object sender, EventArgs e)
    {


    }

    protected void Page_Disposed(object sender, EventArgs e)
    {

    }

    //===============================
    // Button
    //===============================

    #region btnInsert
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        try
        {
            //生成后台编码
            String strMid, strUp, strDown, strSpecial, strImageHtml;
            strImageHtml = "";
            if (ddlSpecial.SelectedIndex > -1)
            {
                strSpecial = ddlSpecial.SelectedItem.Text.Replace("Φ", "φ");
                strMid = strSpecial + txtMid.Text.Trim(); //"φ22";
            }
            else
            {
                strSpecial = "";
                strMid = txtMid.Text.Trim();
            }

            strUp = txtUp.Text;//"-1";
            strDown = txtDown.Text; // "+1";


            clsBuildCode objBuild = new clsBuildCode();
            strImageHtml = objBuild.BuildToleranceCode(strMid, strUp, strDown);


            // 解析后台编码
            String strImageHtmlTemp, strHtml;
            int intFlag, intFlag2, intStart, intEnd, intImageIndex;
            // strImageHtml= txtHtml.Text.Trim();
            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            intStart = strImageHtml.IndexOf("<Image>");
            intEnd = strImageHtml.IndexOf("</Image>");
            strMid = "";
            strUp = "";
            strDown = "";
            if (intStart > -1)
            {
                strImageHtmlTemp = strImageHtml.Substring(intStart + 7, intEnd - intStart - 7);
                intFlag = strImageHtmlTemp.IndexOf("<T");
                if (intFlag > -1)
                {
                    //strImageHtml = strImageHtmlTemp.Substring(0, strImageHtmlTemp.Length - 1);
                    strMid = strImageHtmlTemp.Substring(0, intFlag);
                    intFlag2 = strImageHtmlTemp.IndexOf("!");
                    strUp = strImageHtmlTemp.Substring(intFlag + 2, intFlag2 - intFlag - 2);
                    strDown = strImageHtmlTemp.Substring(intFlag2 + 1, strImageHtmlTemp.Length - intFlag2 - 2);
                }
                else
                {
                    intFlag = strImageHtmlTemp.IndexOf("<H>");
                    if (intFlag > -1)
                    {
                        intFlag2 = strImageHtmlTemp.IndexOf("</H>");
                        strMid = strImageHtmlTemp.Substring(0, intFlag);
                        strUp = strImageHtmlTemp.Substring(intFlag + 3, intFlag2 - intFlag - 3);
                        strDown = "";
                    }
                    else
                    {
                        intFlag = strImageHtmlTemp.IndexOf("<L>");
                        if (intFlag > -1)
                        {
                            intFlag2 = strImageHtmlTemp.IndexOf("</L>");
                            strMid = strImageHtmlTemp.Substring(0, intFlag);
                            strUp = "";
                            strDown = strImageHtmlTemp.Substring(intFlag + 3, intFlag2 - intFlag - 3);

                        }
                        else
                        {
                            strMid = strImageHtmlTemp;
                            strUp = "";
                            strDown = "";
                        }
                    }

                }
            }
            intImageIndex = (int)Session["intMaxImageIndex"] + 1;

            strHtml = "";
            if (strMid != "")
            {
                objBmp = oDraw.DrawTolerance(strMid, strUp, strDown);
                string strMapPath = MapPath("~");
                String strFileDoc;
                String strImageTempPath;
                strFileDoc =webRootDir+ ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                string strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                string strFileName = "ImageTemp-" + strImageIndex + ".png";
                string strFilePath = strFileDoc + strFileName;

                System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                while (oFile.Exists == true)
                {
                    strImageIndex = strImageIndex + "1";
                    strFileName = "ImageTemp-" + strImageIndex + ".png";
                    strFilePath = strFileDoc + strFileName;
                    oFile = new System.IO.FileInfo(strFilePath);
                }               

                objBmp.Save(strFilePath);

                Session["intMaxImageIndex"] = intImageIndex;
                DataTable dtImage = new DataTable();
                dtImage = (DataTable)Session["dtImage"];
                DataRow dr;
                dr = dtImage.NewRow();
                dr[0] = strFileName;
                dr[1] = strImageHtml;
                dtImage.Rows.Add(dr);
                Session["dtImage"] = dtImage;
                strImageTempPath = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                strHtml = @"<img src='" + strImageTempPath + strFileName + "'>";
                ftbFinalHtml.Text += strHtml;

                //清空控件
                ddlSpecial.SelectedIndex = 0;
                txtMid.Text = "";
                txtUp.Text = "";
                txtDown.Text = "";
            }
            else
            {
                lblMessage.Text = "必须输入基本尺寸！";
            }

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
        finally
        {
            if (AfterInputData != null)
            {
                AfterInputData(sender, e);
            }
        }

    }

    #endregion

    protected void btnDrawRoughness_Click(object sender, EventArgs e)
    {
        try
        {
            //生成后台编码
            String strLeft, strUp, strDown, strRight, strImageHtml;
            int intIndex;
            strImageHtml = "";
            intIndex = -1;

            if (rbNoMaterial.Checked == true)
            {
                intIndex = 0;
            }
            if (rbHaveTechnique.Checked == true)
            {
                intIndex = 1;
            }
            if (rbHaveMaterial.Checked == true)
            {
                intIndex = 2;
            }
            if (intIndex == -1)
            {
                lblMessage.Text = "必须选择一种粗糙度！";
                return;
            }

            switch (intIndex)
            {
                case 0:
                    strLeft = txtNoMaterialLeft.Text.Trim();
                    strUp = "";
                    strDown = "";
                    strRight = "";

                    break;
                case 1:
                    strLeft = txtHaveTechniqueLeft.Text.Trim();
                    strUp = txtHaveTechniqueUp.Text.Trim();
                    strDown = txtHaveTechniqueDown.Text.Trim();
                    strRight = "<" + strUp + "," + strDown + ">";

                    break;
                case 2:
                    strLeft = "";
                    strUp = "";
                    strDown = "";
                    strRight = "";

                    break;
                default:
                    strLeft = "";
                    strUp = "";
                    strDown = "";
                    strRight = "";
                    break;
            }
            clsBuildCode objBuild = new clsBuildCode();
            strImageHtml = objBuild.BuildRoughnessCode(strLeft, strRight, intIndex);


            // 解析后台编码
            String strImageHtmlTemp, strHtml;
            int intFlag, intFlag2, intStart, intEnd, intImageIndex;
            // strImageHtml= txtHtml.Text.Trim();
            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            intStart = strImageHtml.IndexOf("<Image>");
            intEnd = strImageHtml.IndexOf("</Image>");
            strLeft = "";
            strUp = "";
            strDown = "";
            if (intStart > -1)
            {
                strImageHtmlTemp = strImageHtml.Substring(intStart + 7, intEnd - intStart - 7);

                intFlag = strImageHtmlTemp.IndexOf("<√>");
                if (intFlag > -1)
                {
                    intFlag2 = strImageHtmlTemp.IndexOf("</√>");
                    strLeft = strImageHtmlTemp.Substring(3, intFlag2 - 3);
                    strRight = strImageHtmlTemp.Substring(intFlag2 + 4);

                }
                else
                {
                    intFlag = strImageHtmlTemp.IndexOf("<R>");
                    if (intFlag > -1)
                    {
                        intFlag2 = strImageHtmlTemp.IndexOf("</R>");
                        strLeft = strImageHtmlTemp.Substring(3, strImageHtmlTemp.Length - 7);
                        strRight = "";

                    }
                    else
                    {
                        strLeft = "";
                        strRight = "";
                    }
                }



                intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                //Session["intMaxImageIndex"] = intImageIndex;
                strHtml = "";
                //if (strLeft != "")
                //{
                // objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);

                objBmp = oDraw.DrawRoughness(strLeft, strRight);

                //保存
                //objBmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
                String strFileDoc, strFilePath, strFileName, strMapPath;
                strMapPath = MapPath("~");
                // strFileDoc = strMapPath + @"\ImageTemp\";
                //clsCon oCon = new clsCon();
                String strImageTempPath;
                //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                strImageTempPath =webRootDir+ ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                strFileDoc = strImageTempPath;
                string strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                strFileName = "ImageTemp-" + strImageIndex + ".png";
                strFilePath = strFileDoc + strFileName;

                ////intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                ////Session["intMaxImageIndex"] = intImageIndex;
                //strFileName = "ImageTemp-" + intImageIndex + ".png";
                //strFilePath = strFileDoc + strFileName;

                System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                while (oFile.Exists == true)
                {
                    strImageIndex = strImageIndex + "1";
                    //intImageIndex++;
                    strFileName = "ImageTemp-" + strImageIndex + ".png";
                    strFilePath = strFileDoc + strFileName;
                    oFile = new System.IO.FileInfo(strFilePath);

                }

                objBmp.Save(strFilePath);
                Session["intMaxImageIndex"] = intImageIndex;
                DataTable dtImage = new DataTable();
                dtImage = (DataTable)Session["dtImage"];
                DataRow dr;
                dr = dtImage.NewRow();
                dr[0] = strFileName;
                dr[1] = strImageHtml;
                dtImage.Rows.Add(dr);
                Session["dtImage"] = dtImage;

                //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                strFileDoc = ResolveUrl(webRootUrl+ ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                //strHtml = @"<img src='../../../ImageTemp/" + strFileName + "'>";   ResolveUrl
                strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                //strHtml = @"<img src='" + ResolveUrl(strFileDoc + strFileName) + "'>";

                ftbFinalHtml.Text += strHtml;
                //清空控件
                txtNoMaterialLeft.Text = "";
                txtHaveTechniqueLeft.Text = "";
                txtHaveTechniqueUp.Text = "";
                txtHaveTechniqueDown.Text = "";

                // }
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
        finally
        {
        }



    }

    //===============================
    // RadioButton
    //===============================

    protected void rbNoMaterial_CheckedChanged(object sender, EventArgs e)
    {
        rbHaveTechnique.Checked = false;
        rbHaveMaterial.Checked = false;
    }

    protected void rbHaveTechnique_CheckedChanged(object sender, EventArgs e)
    {
        rbNoMaterial.Checked = false;
        rbHaveMaterial.Checked = false;
    }

    protected void rbHaveMaterial_CheckedChanged(object sender, EventArgs e)
    {
        rbHaveTechnique.Checked = false;
        rbNoMaterial.Checked = false;
    }


    protected void Imagebtn_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = sender as ImageButton;
        ftbHtmlTemp.Text += @"<IMG src='" + btn.ImageUrl.Replace("~", "..") + "'>";
        //txtHtmlTemp.Focus();
        ftbHtmlTemp.Focus = true;
    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        clsParseCode oParse = new clsParseCode();

        try
        {
            lblMessage.Text = "";
            String strHtmlTemp, strPreviewCode, strImageCode, strStartCode, strStartCodeTemp, strHtmlStripped;
            int intLength, intFlag, intFlag2;
            strPreviewCode = ftbHtmlTemp.Text.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");
            //strPreviewCode = @strPreviewCode.Replace(@"\", "");
            strHtmlTemp = "";
            // strStartCode = @"<IMG src='../../../Images/start.png'>";

            //if (strPreviewCode.IndexOf(strStartCode) == 0)
            //{
            strHtmlTemp += @"<&70><+>";
            // strPreviewCode = strPreviewCode.Replace(@"<IMG src='../../../Images/start.png'>", "");
            intLength = strPreviewCode.Length;
            while (intLength > 0)
            {
                intFlag = strPreviewCode.IndexOf(@"<img");
                if (intFlag == -1)
                {
                    intFlag = strPreviewCode.IndexOf(@"<IMG");
                }
                if (intFlag > -1)
                {
                    if (intFlag > 0)
                    {
                        strHtmlTemp += strPreviewCode.Substring(0, intFlag);
                        strPreviewCode = strPreviewCode.Substring(intFlag);
                        intLength = strPreviewCode.Length;
                        continue;
                    }
                    else
                    {
                        intFlag2 = strPreviewCode.IndexOf(">");
                        strImageCode = strPreviewCode.Substring(0, intFlag2 + 1);
                        strHtmlTemp += oParse.ParseImageCode(strImageCode);
                        strPreviewCode = strPreviewCode.Substring(intFlag2 + 1);
                        intLength = strPreviewCode.Length;
                        continue;
                    }
                }
                else
                {
                    strHtmlTemp += strPreviewCode;
                    strPreviewCode = "";
                    intLength = strPreviewCode.Length;
                    continue;
                }
            }
            strHtmlTemp += @"<+><&90>";
            txtHtmlTemp.Text = strHtmlTemp;
            strHtmlStripped = ftbHtmlTemp.HtmlStrippedText.Trim().Replace(" ", "");

            String strFileDoc, strFilePath, strFileName, strMapPath, strHtml;
            int intImageIndex;
            strMapPath = MapPath("~");
            //strFileDoc = @strMapPath + @"\Images\";
            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");
            strImagePath =webRootDir+ ConfigurationManager.AppSettings["ImageGetPath"].ToString();
            strFileDoc = strImagePath;
            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            objBmp = oDraw.DrawShapeTolerance(strHtmlTemp, strHtmlStripped, strFileDoc);

            //保存
            // strFileDoc = @strMapPath + @"\ImageTemp\";
            //strFileDoc = oCon.LoadConfigString("ImageTempPath");
            strFileDoc =webRootDir+ ConfigurationManager.AppSettings["ImageTempPath"].ToString();

            //intImageIndex = (int)Session["intMaxImageIndex"] + 1;
            //Session["intMaxImageIndex"] = intImageIndex;
            strHtml = "";
            //strFileName = "ImageTemp-" + intImageIndex + ".png";
            //strFilePath = strFileDoc + strFileName;

            //string strFileDoc = strMapPath + @"\ImageTemp\";

            string strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
            strFileName = "ImageTemp-" + strImageIndex + ".png";
            strFilePath = strFileDoc + strFileName;



            System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
            while (oFile.Exists == true)
            {
                //intImageIndex++;
                strImageIndex = strImageIndex + "1";
                strFileName = "ImageTemp-" + strImageIndex + ".png";
                strFilePath = strFileDoc + strFileName;
                oFile = new System.IO.FileInfo(strFilePath);

            }

            objBmp.Save(strFilePath);
            //Session["intMaxImageIndex"] = intImageIndex;
            DataTable dtImage = new DataTable();
            dtImage = (DataTable)Session["dtImage"];
            DataRow dr;
            dr = dtImage.NewRow();
            dr[0] = strFileName;
            dr[1] = "<Image>" + strHtmlTemp + "</Image>";
            dtImage.Rows.Add(dr);
            Session["dtImage"] = dtImage;

            //strFileDoc = oCon.LoadConfigString("ImageTempPath");
            //strFileDoc =ConfigurationManager.AppSettings["ImageTempPath"].ToString();
            strFileDoc = ResolveUrl(webRootUrl+ConfigurationManager.AppSettings["ImageTempPath"].ToString());

            //strHtml = @"<img src='../../../ImageTemp/" + strFileName + "'>"; ResolveUrl
            strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
            //strHtml = @"<img src='" + ResolveUrl(strFileName) + "'>";

            ftbFinalHtml.Text += strHtml;
            //清空控件
            //txtHtmlTemp.Text = "";
            ftbHtmlTemp.Text = "";

            //}
            //else
            //{
            //    lblMessage.Text = "该公差代码不合法，请重新输入！";
            //}
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
        finally
        {
        }

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            //if (Page.IsPostBack == false)
            //{
            String strFileName, strFilePath, strFileDoc;
            int i, intCount;
            System.IO.FileInfo oFile;
            DataTable dtImage = new DataTable();
            dtImage = (DataTable)Session["dtImage"];
            //strFileDoc = MapPath("~") + @"\ImageTemp\";
            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageTempPath");
            strImagePath =webRootDir+ ConfigurationManager.AppSettings["ImageTempPath"].ToString();
            strFileDoc = strImagePath;
            if (dtImage != null)
            {
                if (dtImage.Rows.Count > 0)
                {
                    intCount = dtImage.Rows.Count;
                    for (i = 0; i < intCount; i++)
                    {
                        strFileName = (String)dtImage.Rows[i][0];
                        strFilePath = strFileDoc + strFileName;
                        oFile = new System.IO.FileInfo(strFilePath);
                        oFile.Delete();

                    }

                }
            }
            //}

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.Message;
        }
        finally
        {
        }
    }



    protected void btnPinmiandu_Click(object sender, ImageClickEventArgs e)
    {

    }
}
