﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GetProductInfo.ascx.vb"
    Inherits="GetProductInfo_ascx" %>
<div>
    <div id="dvProductControl" runat="server" style="width:250px;float:left">
        <asp:TextBox ID="ProductNameOrType" runat="server" Width="99%" 
            Style="position: relative; top: 0px; margin-left: 0px;" />
        <asp:DropDownList ID="ddlProduct" runat="server" height="28px" DataLoadMode="Bound"
            Visible="false" DependsOn="" DisplayMode="DropDown" Width="99%"
            Style="font-size:14px;position: relative; top: 0px; margin-left: 0px; left: 0px;"
            AutoPostBack="True" />
    </div>
    <asp:ImageButton ID="btnProduct" runat="server" DisplayInFrame="false" Text="" Style="position: relative;
        float: left; top:0px; left: 5px; width: 16px;" 
        ImageUrl="~/images/search.gif" />
</div>
