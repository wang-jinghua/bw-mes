﻿Imports uMES.LeanManufacturing.ReportBusiness
Imports System.Data
Imports System.Web.UI.WebControls

Partial Class GetProductInfo_ascx
    Inherits System.Web.UI.UserControl
    Private m_CommonObj As uMESContainerBusiness = New uMESContainerBusiness()
    '消息提示
    Private v_Message As String
    '选择件号的下拉列表
    Private v_Index As String
    '选择件号的名称
    Private v_ProducText As String
    '选择件号的值
    Private v_ProductValue As String
    'ddlProduct是否显示
    Private v_Visable As Boolean

    '查询和关闭查询图表url
    Private search_rul As String = "~/images/search.gif"
    Private close_rul As String = "~/images/closeMod.png"

    '传给ddlProduct的资源表
    Private v_ddlProduct_dt As DataTable

    '控件的下拉列表
    Private v_ddlDropList As Camstar.WebClient.FormsBuilder.WebControls.DropDownList

    '''声明选择件号事件
    Public Event DDlProductDataChanged()
    Sub DDlProductDataChangedCauseEvent()
        ' Raise an event.
        RaiseEvent DDlProductDataChanged()
    End Sub

    '''声明查询件号事件
    Public Event GetProductData()
    Sub GetProductDataCauseEvent()
        ' Raise an event.
        RaiseEvent GetProductData()
    End Sub

    ''' <summary>
    ''' 执行点击查询
    ''' </summary>
    ''' <remarks></remarks>
    Sub DoSearchProductInfo()
        SearchProduct(0)
    End Sub

    ''' <summary>
    ''' 获取下拉列表资源表
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property GetddlProductResoucedt() As DataTable
        Get
            Return v_ddlProduct_dt
        End Get
        Set(ByVal value As DataTable)
            v_ddlProduct_dt = value
        End Set
    End Property

    ''' <summary>
    ''' 给下拉列表增加属性
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property GetProductDDL() As DropDownList
        Get
            Return ddlProduct
        End Get
    End Property

    Public ReadOnly Property GetProductNameOrTypeText As TextBox
        Get
            Return ProductNameOrType
        End Get
    End Property

    ''' <summary>
    ''' 获取控件整个div
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property GetProductControlDiv As HtmlGenericControl
        Get
            Return dvProductControl
        End Get
    End Property

    ''' <summary>
    ''' 获取查询按钮
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property GetSearchBtn As ImageButton
        Get
            Return btnProduct
        End Get
    End Property


    '获取绑定下拉列表值事件
    Sub GetProductDataBindCauseEvent()
        ProductNameOrType.Visible = False
        ddlProduct.Visible = True
        'btnProduct.Text = "<"
        btnProduct.ImageUrl = close_rul
        Dim dt As DataTable = GetddlProductResoucedt
        ddlProduct.Items.Clear()
        ddlProduct.DataTextField = "PRODUCTNAME"
        ddlProduct.DataValueField = "PRODUCTID"
        ddlProduct.DataSource = dt
        ddlProduct.DataBind()
        ddlProduct.Items.Insert(0, "")
        ddlProduct.SelectedValue = dt.Rows(0)("PRODUCTID")
    End Sub

    ''' <summary>
    ''' 初始化控件
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearControl()
        ProductNameOrType.Visible = True
        ddlProduct.Visible = False
        'btnProduct.Text = ">"
        btnProduct.ImageUrl = search_rul
        ProductNameOrType.Text = ""
    End Sub

    ''' <summary>
    ''' 消息提示
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Message() As String
        Get
            Return v_Message
        End Get
    End Property

    ''' <summary>
    ''' 选择件号的文本
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ProducText() As String
        Get
            If ddlProduct.SelectedItem IsNot Nothing Then
                Return ddlProduct.SelectedItem.Text
            Else
                Return ""
            End If

        End Get
    End Property

    ''' <summary>
    ''' 获取输入的查询条件值
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetScanProducNameOrTypeText() As String
        Get
            Return ProductNameOrType.Text
        End Get
    End Property

    ''' <summary>
    ''' 选择件号的值
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ProductValue() As String
        Get
            If ddlProduct.SelectedValue IsNot Nothing Then
                Return ddlProduct.SelectedValue
            Else
                Return ""
            End If
        End Get
    End Property

    ''' <summary>
    ''' 选择件号的索引列值
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Index() As Integer
        Get
            Return ddlProduct.SelectedIndex
        End Get
    End Property

    ''' <summary>
    ''' 件号下拉列表控件是否显示
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DDLProductVisable() As Boolean
        Get
            Return ddlProduct.Visible
        End Get
    End Property

    ''' <summary>
    ''' 恢复控件默认显示样式 add YangJiang 20180726
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property InitControl_() As Boolean
        Get
            Return InitControl()
        End Get
    End Property

    Protected Sub btnProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProduct.Click
        Try
            v_Message = ""
            If Not SearchProduct(0) Then
                v_Message = "没有查询到件号信息"
            End If
            GetProductDataCauseEvent()
        Catch ex As Exception
            v_Message = ex.Message
            GetProductDataCauseEvent()
        End Try
    End Sub

    Protected Sub ddlProduct_DataChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged

        Try
            v_Message = ""
            DDlProductDataChangedCauseEvent()
        Catch ex As Exception
            v_Message = ex.Message
            DDlProductDataChangedCauseEvent()
        End Try
    End Sub

    ''' <summary>
    ''' 查询件号信息
    ''' </summary>
    ''' <param name="value">0 按钮‘>’查询 1查询按钮直接查询</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function SearchProduct(ByVal value As Integer) As Boolean
        SearchProduct = False
        'If btnProduct.Text = ">" Or value = 1 Then
        If btnProduct.ImageUrl.Contains("search") Or value = 1 Then
            Dim productdt As DataTable = m_CommonObj.GetProductInfoLikeNameOrREVISION(ProductNameOrType.Text.Trim())
            For Each dr As DataRow In productdt.Rows
                If dr("PRODUCTID") = dr("REVOFRCDID") Then
                    dr("PRODUCTNAME") += "(默认)"
                End If
            Next
            If productdt.Rows.Count = 0 Then
                Exit Function
            End If
            ddlProduct.DataTextField = "PRODUCTNAME"
            ddlProduct.DataValueField = "PRODUCTID"
            ddlProduct.DataSource = productdt
            ddlProduct.DataBind()
            'btnProduct.Text = "<"
            btnProduct.ImageUrl = close_rul
            ddlProduct.Visible = True
            ProductNameOrType.Visible = False
            ddlProduct.Items.Insert(0, "")
            v_Visable = True
        ElseIf btnProduct.ImageUrl.Contains("closeMod") Then
            'ElseIf btnProduct.Text = "<" Then
            'btnProduct.Text = ">"
            btnProduct.ImageUrl = search_rul
            ddlProduct.Visible = False
            ProductNameOrType.Visible = True
            ddlProduct.Items.Clear()
            v_Visable = False
        End If
        SearchProduct = True
    End Function

    ''' <summary>
    ''' 直接查询件号
    ''' </summary>
    ''' <remarks></remarks>
    Sub DirectSerachProduct()
        If Not String.IsNullOrWhiteSpace(ProductNameOrType.Text.Trim()) Or ddlProduct.Visible Then
            SearchProduct(1)
            Dim mrcount As Integer = 0 '默认次数
            Dim index As Integer = 0
            For Each item As ListItem In ddlProduct.Items
                index += 1
                If item.Text.Contains("默认") Then
                    ddlProduct.SelectedValue = item.Value
                    mrcount += 1
                End If
            Next
            'Modify by YangJiang 20180613
            If index > 0 Then
                If mrcount > 1 Then
                    ddlProduct.SelectedIndex = 0
                Else
                    ddlProduct.Visible = True
                    ProductNameOrType.Visible = False
                End If
            Else
                ddlProduct.Visible = False
                ProductNameOrType.Visible = True
            End If
        End If
    End Sub


    ''' <summary>
    ''' 初始化控件显示样式 add YangJiang 20180726
    ''' </summary>
    ''' <remarks></remarks>
    Function InitControl() As Boolean
        btnProduct.ImageUrl = search_rul
        ddlProduct.Visible = False
        ProductNameOrType.Visible = True
        ddlProduct.Items.Clear()
        v_Visable = False
        Return True
    End Function

End Class
