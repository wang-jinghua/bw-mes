﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GetWorkFlowInfo.ascx.vb"
    Inherits="GetWorkFlowInfo_ascx" %>
<div id="dvWokflowControl" >
    <asp:TextBox ID="WorkFlowName" runat="server"  Width="250px" 
        Style="position: relative; float: left; top: 0px; margin-left: 0px;" />
    <asp:DropDownList ID="ddlWorkFlow" runat="server" Height="28px"  DataLoadMode="Bound"
        Visible="false" DependsOn="" DisplayMode="DropDown" Width="250px"
        Style="font-size:14px;position: relative; top: 0px; float: left; margin-left: 0px; left: 0px;"
        AutoPostBack="True" />
    <asp:ImageButton ID="btnWorkFlow" runat="server" DisplayInFrame="false" Text="" Style="position: relative;
        float: left; top:0px; left: 5px; width: 16px;" 
        ImageUrl="~/images/search.gif" />
    <asp:Label ID="IsThisFactoryWF" runat="server" Visible ="false" ></asp:Label>
</div>
