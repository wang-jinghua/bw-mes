﻿Imports uMES.LeanManufacturing.ReportBusiness
Imports System.Data
Imports System.Web.UI.WebControls

Partial Class GetWorkFlowInfo_ascx
    Inherits System.Web.UI.UserControl
    Private m_UserInfo As New Dictionary(Of String, String)
    Private m_CommonObj As uMESContainerBusiness = New uMESContainerBusiness()
    '消息提示
    Private v_Message As String
    '选择工艺路线的下拉列表
    Private v_Index As String
    '选择工艺路线的名称
    Private Shared v_WorkFlowText As String = String.Empty
    '选择工艺路线的值
    Private Shared v_WorkFlowValue As String = String.Empty
    'ddlWorkFlow是否显示
    Private v_Visable As Boolean

    '查询和关闭查询图表url
    Private search_rul As String = "~/images/search.gif"
    Private close_rul As String = "~/images/closeMod.png"

    '传给ddlWorkFlow的资源表
    Private v_ddlWorkFlow_dt As DataTable

    ''' <summary>
    ''' 获取下拉列表资源表
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property GetIsThisFactoryWF() As String
        Get
            Return IsThisFactoryWF.Text
        End Get
        Set(ByVal value As String)
            IsThisFactoryWF.Text = value
        End Set
    End Property

    '''声明选择工艺路线事件
    Public Event DDlWorkFlowDataChanged()
    Sub DDlWorkFlowDataChangedCauseEvent()
        ' Raise an event.
        RaiseEvent DDlWorkFlowDataChanged()
    End Sub

    '''声明查询工艺路线事件
    Public Event GetWorkFlowData()
    Sub GetWorkFlowDataCauseEvent()
        ' Raise an event.
        RaiseEvent GetWorkFlowData()
    End Sub

    ''' <summary>
    ''' 给下拉列表增加属性
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property GetWFDDL() As DropDownList
        Get
            Return ddlWorkFlow
        End Get
    End Property

    ''' <summary>
    ''' 获取查询按钮
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property GetBtnSearch() As ImageButton
        Get
            Return btnWorkFlow
        End Get
    End Property


    ''' <summary>
    ''' 获取输入框
    ''' </summary>
    ''' <remarks></remarks>
    Public ReadOnly Property GetTextBox() As TextBox
        Get
            Return WorkFlowName
        End Get
    End Property

    ''' <summary>
    ''' 获取下拉列表资源表
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property GetddlWorkFlowResoucedt() As DataTable
        Get
            Return v_ddlWorkFlow_dt
        End Get
        Set(ByVal value As DataTable)
            v_ddlWorkFlow_dt = value
        End Set
    End Property

    ''' <summary>
    ''' 获取下拉列表数量
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetddlWorkFlowcount() As Integer
        Get
            Return ddlWorkFlow.Items.Count
        End Get
    End Property

    '''获取绑定下拉列表值事件
    Sub GetWorkFlowDataBindCauseEvent()
        WorkFlowName.Visible = False
        ddlWorkFlow.Visible = True
        btnWorkFlow.ImageUrl = close_rul
        Dim dt As DataTable = GetddlWorkFlowResoucedt
        ddlWorkFlow.Items.Clear()
        ddlWorkFlow.DataTextField = "WORKFLOWNAME"
        ddlWorkFlow.DataValueField = "WORKFLOWID"
        ddlWorkFlow.DataSource = dt
        ddlWorkFlow.DataBind()
        ddlWorkFlow.Items.Insert(0, "")
        ddlWorkFlow.SelectedValue = dt.Rows(0)("WORKFLOWID")
        Dim strText = ddlWorkFlow.SelectedItem.Text
        Dim strValue = ddlWorkFlow.SelectedValue
        v_WorkFlowValue = strValue
        v_WorkFlowText = strText
    End Sub

    ''' <summary>
    ''' 初始化控件
    ''' </summary>
    ''' <remarks></remarks>
    Sub ClearControl()
        WorkFlowName.Visible = True
        ddlWorkFlow.Visible = False
        btnWorkFlow.ImageUrl = search_rul
        WorkFlowName.Text = ""
    End Sub

    ''' <summary>
    ''' 消息提示
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Message() As String
        Get
            Return v_Message
        End Get
    End Property

    ''' <summary>
    ''' 选择工艺路线的文本
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property WorkFlowText() As String
        Get
            If ddlWorkFlow.SelectedItem IsNot Nothing Then
                Return ddlWorkFlow.SelectedItem.Text
            Else
                Return ""
            End If
        End Get
    End Property

    ''' <summary>
    ''' 获取输入的查询条件值
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetScanWorkFlowName() As String
        Get
            Return WorkFlowName.Text
        End Get
    End Property

    ''' <summary>
    ''' 选择工艺路线的值
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property WorkFlowValue() As String
        Get
            If ddlWorkFlow.SelectedValue IsNot Nothing Then
                Return ddlWorkFlow.SelectedValue
            Else
                Return ""
            End If
        End Get
    End Property

    ''' <summary>
    ''' 选择工艺路线的索引列值
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Index() As Integer
        Get
            Return ddlWorkFlow.SelectedIndex
        End Get
    End Property

    ''' <summary>
    ''' 工艺路线下拉列表控件是否显示
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property DDLWorkFlowVisable() As Boolean
        Get
            Return ddlWorkFlow.Visible
        End Get
    End Property

    ''' <summary>
    ''' 查询工艺路线
    ''' </summary>
    ''' <param name="para"></param>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SearchWorkFlow_(ByVal para As Dictionary(Of String, String)) As Boolean
        Get
            Return SearchWorkFlow(para, 1)
        End Get
    End Property

    Public ReadOnly Property v_WorkFlowName() As String
        Get
            Return v_WorkFlowText
        End Get
    End Property

    Public ReadOnly Property v_WorkFlowID() As String
        Get
            Return v_WorkFlowValue
        End Get
    End Property

    ''' <summary>
    ''' 恢复控件默认显示样式 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property InitControl_() As Boolean
        Get
            Return InitControl()
        End Get
    End Property


    Protected Sub btnWorkFlow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnWorkFlow.Click
        Try
            v_Message = ""
            If Not SearchWorkFlow(New Dictionary(Of String, String), 0) Then
                v_Message = "没有查询到工艺路线信息"
            End If
            GetWorkFlowDataCauseEvent()
        Catch ex As Exception
            v_Message = ex.Message
            GetWorkFlowDataCauseEvent()
        End Try
    End Sub

    Protected Sub ddlWorkFlow_DataChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkFlow.SelectedIndexChanged

        Try
            v_Message = ""
            Dim strText = ddlWorkFlow.SelectedItem.Text
            Dim strValue = ddlWorkFlow.SelectedValue
            v_WorkFlowValue = strValue
            v_WorkFlowText = strText
            DDlWorkFlowDataChangedCauseEvent()
        Catch ex As Exception
            v_Message = ex.Message
            DDlWorkFlowDataChangedCauseEvent()
        End Try
    End Sub

    ''' <summary>
    ''' 查询工艺路线信息
    ''' </summary>
    ''' <param name="value">0 按钮‘>’查询 1查询按钮直接查询</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SearchWorkFlow(ByVal para As Dictionary(Of String, String), ByVal value As Integer) As Boolean
        v_WorkFlowValue = String.Empty
        v_WorkFlowText = String.Empty

        ' m_UserInfo = Session("CurrentUserInfo")
        SearchWorkFlow = False
        Dim dtWorkFlow As New DataTable
        'para("FactoryID") = m_UserInfo.FactoryID
        If Not String.IsNullOrWhiteSpace(WorkFlowName.Text.Trim()) Then
            para("WorkflowName") = WorkFlowName.Text.Trim()
        End If

        If btnWorkFlow.ImageUrl.Contains("search") Or value = 1 Then
            Dim IsThisFactory As Boolean
            If String.IsNullOrWhiteSpace(IsThisFactoryWF.Text) Then
                IsThisFactory = False
            Else
                IsThisFactory = True
            End If
            If para.Keys.Contains("ProductName") Or para.Keys.Contains("ProductRev") Then
                dtWorkFlow = m_CommonObj.GetWorkflowInfoByProductForAscx(para)
            Else
                dtWorkFlow = m_CommonObj.GetWorkflowInfoForAscx(para)
            End If

            If dtWorkFlow.Rows.Count = 0 Then
                Exit Function
            End If
            ddlWorkFlow.DataTextField = "WORKFLOWNAME"
            ddlWorkFlow.DataValueField = "WORKFLOWID"
            ddlWorkFlow.DataSource = dtWorkFlow
            ddlWorkFlow.DataBind()
            ddlWorkFlow.Items.Insert(0, "请选择工艺路线")
            ddlWorkFlow.SelectedIndex = 1
            v_WorkFlowText = ddlWorkFlow.SelectedItem.Text
            v_WorkFlowValue = ddlWorkFlow.SelectedValue
            ddlWorkFlow_DataChanged(Nothing, Nothing)
            btnWorkFlow.ImageUrl = close_rul
            ddlWorkFlow.Visible = True
            WorkFlowName.Visible = False
            v_Visable = True
        ElseIf btnWorkFlow.ImageUrl.Contains("closeMod") Then

            btnWorkFlow.ImageUrl = search_rul
            ddlWorkFlow.Visible = False
            WorkFlowName.Visible = True
            ddlWorkFlow.Items.Clear()
            v_Visable = False
        End If
        SearchWorkFlow = True
    End Function

    ''' <summary>
    ''' 直接查询工艺路线
    ''' </summary>
    ''' <remarks></remarks>
    Sub DirectSerachWorkFlow()
        If Not String.IsNullOrWhiteSpace(WorkFlowName.Text.Trim()) Then
            SearchWorkFlow(New Dictionary(Of String, String), 1)
            Dim mrcount As Integer = 0 '默认次数
            Dim index As Integer = 0
            For Each item As ListItem In ddlWorkFlow.Items
                index += 1
                If item.Text.Contains("默认") Then
                    ddlWorkFlow.SelectedValue = item.Value
                    mrcount += 1
                End If
            Next
            If index > 0 Then
                If mrcount > 1 Then
                    ddlWorkFlow.SelectedIndex = 0
                Else
                    ddlWorkFlow.Visible = True
                    WorkFlowName.Visible = False
                End If
            Else
                ddlWorkFlow.Visible = False
                WorkFlowName.Visible = True
            End If
        End If
    End Sub

    ''' <summary>
    ''' 初始化控件显示样式
    ''' </summary>
    ''' <remarks></remarks>
    Function InitControl() As Boolean
        btnWorkFlow.ImageUrl = search_rul
        ddlWorkFlow.Visible = False
        WorkFlowName.Visible = True
        ddlWorkFlow.Items.Clear()
        v_Visable = False
        WorkFlowName.Text = ""
        Return (True)
    End Function

End Class
