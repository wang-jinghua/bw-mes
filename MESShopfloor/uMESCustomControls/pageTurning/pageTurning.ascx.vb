﻿''' <summary>
''' update:Wangjh 20190226(
''' 存在问题：txtCurrentPage控件记录当前页数，没有清空逻辑。
''' 问题表现形式：第一次查询数据后显示为第一页，点击下一页后显示为第二页，此时txtCurrentPage控件记录页数为2;再次重新查询数据，控件显示为第1页，但是实际txtCurrentPage控件值为2，此时若再点击下一页会直接跳到第3页。
''' 解决方式：增加txtSpareCurrentPage控件，默认加载分页控件时将txtCurrentPage的值赋给txtSpareCurrentPage，然后将txtCurrentPage值置为1（默认当前页码为1）；若有点击首页或上一页或下一页或尾页或go事件时再将txtSpareCurrentPage的值赋给txtCurrentPage（有点击事件时再将当前页码值还原）
''' )
''' </summary>
''' <remarks></remarks>
Partial Class pageTurning
    Inherits System.Web.UI.UserControl


    '''//声明分页事件
    Public Event PageIndexChanged()
    Sub CauseEvent()
        ' Raise an event.
        RaiseEvent PageIndexChanged()
    End Sub


    '总行数
    Private v_TotalRowCount As Integer
    '单页行数
    Private v_RowCountByPage As Integer = 15
    '当前页码
    Private v_CurrentPageIndex As Integer = 1
    '消息提示
    Private v_Message As String
    '总页数
    Private v_TotalPageCount As Integer

    ''' <summary>
    ''' 禁用
    ''' </summary>
    Public Sub Disabled()
        lbtnFirst.Enabled = False
        lbtnPrev.Enabled = False
        lbtnNext.Enabled = False
        lbtnLast.Enabled = False
        lLabel1.Text = "第  页  共  页"
        txtPage.Value = ""
        btnGo.Enabled = False
        txtTotalPage.Text = ""
        txtCurrentPage.Text = 1
        txtSpareCurrentPage.Text = 1
    End Sub


    ''' <summary>
    ''' 启用
    ''' </summary>
    Public Sub Enabled()
        lbtnFirst.Enabled = True
        lbtnPrev.Enabled = True
        lbtnNext.Enabled = True
        lbtnLast.Enabled = True
        btnGo.Enabled = True
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TxtCurrentPageIndex() As TextBox
        Get
            Return txtCurrentPage
        End Get
        Set(ByVal value As TextBox)
            txtCurrentPage = value

        End Set
    End Property

    ''' <summary>
    ''' 总行数
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TotalRowCount() As Integer
        Get
            Return v_TotalRowCount
        End Get
        Set(ByVal value As Integer)
            v_TotalRowCount = value
            If IsNothing(Session("CurrenIndex")) = False Then
                If Session("CurrenIndex") <> "" Then
                    v_CurrentPageIndex = txtCurrentPage.Text
                Else
                    txtCurrentPage.Text = 1
                End If
            End If

            v_TotalPageCount = GetTotalPageCount(TotalRowCount, RowCountByPage)
            txtTotalPage.Text = v_TotalPageCount.ToString()
            Me.lLabel1.Text = "第 " + v_CurrentPageIndex.ToString() + " 页  共 " + v_TotalPageCount.ToString() + " 页"
        End Set
    End Property
    ''' <summary>
    ''' 单页行数
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>页面初始化时赋值</remarks>
    Public Property RowCountByPage() As Integer
        Get
            Return v_RowCountByPage
        End Get
        Set(ByVal value As Integer)
            v_RowCountByPage = value

            If IsNothing(Session("CurrenIndex")) = False Then
                If Session("CurrenIndex") <> "" Then
                    v_CurrentPageIndex = txtCurrentPage.Text
                Else
                    txtCurrentPage.Text = 1
                End If
            End If

            v_TotalPageCount = GetTotalPageCount(TotalRowCount, RowCountByPage)
            txtTotalPage.Text = v_TotalPageCount.ToString()
            Me.lLabel1.Text = "第 " + v_CurrentPageIndex.ToString() + " 页  共 " + v_TotalPageCount.ToString() + " 页"
        End Set
    End Property
    ''' <summary>
    ''' 当前页码
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property CurrentPageIndex() As Integer
        Get
            Return v_CurrentPageIndex
        End Get
    End Property
    ''' <summary>
    ''' 消息提示
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Message() As String
        Get
            Return v_Message
        End Get
    End Property

    ''' <summary>
    ''' 初始化控件
    ''' </summary>
    Sub InitControl()
        lLabel1.Text = "第  页  共  页"
        txtPage.Value = ""
        txtTotalPage.Text = ""
        txtCurrentPage.Text = 1
        txtSpareCurrentPage.Text = 1
        AddHandler PageIndexChanged, Sub()
                                     End Sub

    End Sub
    Protected Sub lbtnFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnFirst.Click
        Try
            'txtCurrentPage.Text = txtSpareCurrentPage.Text
            Dim CurPager As String = FirstPage()
            txtCurrentPage.Text = CurPager

            v_CurrentPageIndex = CurPager
            '引发事件
            CauseEvent()
            Me.lLabel1.Text = "第 " + v_CurrentPageIndex.ToString() + " 页  共 " + v_TotalPageCount.ToString() + " 页"
        Catch ex As Exception
            v_Message = ex.Message
            '引发事件
            CauseEvent()
        End Try
    End Sub

    Protected Sub lbtnPrev_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnPrev.Click
        Try
            'txtCurrentPage.Text = txtSpareCurrentPage.Text
            Dim strInfo As String = ""
            Dim pageTmp As Integer = PrevPage(CInt(txtCurrentPage.Text), CInt(txtTotalPage.Text), strInfo)

            If pageTmp = -1 Then

                v_Message = strInfo
                '引发事件
                CauseEvent()
                Exit Sub
            End If
            Me.txtCurrentPage.Text = pageTmp

            v_CurrentPageIndex = pageTmp
            '引发事件
            CauseEvent()
            Me.lLabel1.Text = "第 " + v_CurrentPageIndex.ToString() + " 页  共 " + v_TotalPageCount.ToString() + " 页"
        Catch ex As Exception
            v_Message = ex.Message
            '引发事件
            CauseEvent()
        End Try
    End Sub

    Public Sub lbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnNext.Click
        Try
            ' txtCurrentPage.Text = txtSpareCurrentPage.Text
            Dim strInfo As String = ""
            Dim pageTmp As Integer = NextPage(CInt(txtCurrentPage.Text), CInt(txtTotalPage.Text), strInfo)
            If pageTmp = -1 Then
                v_Message = strInfo
                '引发事件
                CauseEvent()
                Exit Sub
            End If
            Me.txtCurrentPage.Text = pageTmp
            v_CurrentPageIndex = pageTmp
            '引发事件
            CauseEvent()
            Me.lLabel1.Text = "第 " + v_CurrentPageIndex.ToString() + " 页  共 " + v_TotalPageCount.ToString() + " 页"
        Catch ex As Exception
            v_Message = ex.Message
            '引发事件
            CauseEvent()
        End Try
    End Sub

    Protected Sub lbtnLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnLast.Click
        Try
            'txtCurrentPage.Text = txtSpareCurrentPage.Text
            Dim strInfo As String = ""
            Dim pageTmp As Integer = LastPage(CInt(txtTotalPage.Text), strInfo)
            If pageTmp = -1 Then
                v_Message = strInfo
                '引发事件
                CauseEvent()
                Exit Sub
            End If
            Me.txtCurrentPage.Text = pageTmp
            v_CurrentPageIndex = pageTmp
            '引发事件
            CauseEvent()
            Me.lLabel1.Text = "第 " + v_CurrentPageIndex.ToString() + " 页  共 " + v_TotalPageCount.ToString() + " 页"
        Catch ex As Exception
            v_Message = ex.Message
            '引发事件
            CauseEvent()
        End Try
    End Sub

    Protected Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        'txtCurrentPage.Text = txtSpareCurrentPage.Text
        Dim strPage As String = txtPage.Value.Trim
        txtPage.Value = ""

        If strPage = "" Then
            v_Message = "请输入跳转页数"
            txtPage.Focus()
            '引发事件
            CauseEvent()
            Exit Sub
        End If

        If IsNumeric(strPage) = False Then
            v_Message = "跳转页数必须是数字"
            txtPage.Focus()
            '引发事件
            CauseEvent()
            Exit Sub
        End If

        Try
            Dim strInfo As String = ""
            Dim pageTmp As Integer = GoPages(CInt(strPage), CInt(Me.txtTotalPage.Text), strInfo)
            If pageTmp = -1 Then
                v_Message = strInfo
                '引发事件
                CauseEvent()
                Exit Sub
            End If
            Me.txtCurrentPage.Text = pageTmp
            v_CurrentPageIndex = pageTmp
            '引发事件
            CauseEvent()
            Me.lLabel1.Text = "第 " + v_CurrentPageIndex.ToString() + " 页  共 " + v_TotalPageCount.ToString() + " 页"
        Catch ex As Exception
            v_Message = ex.Message
            '引发事件
            CauseEvent()
        End Try
    End Sub


    Function FirstPage() As Integer
        FirstPage = 1
    End Function

    Function LastPage(ByVal TotalPage As Integer, ByRef strInfo As String) As Integer
        If TotalPage < 1 Then
            strInfo = "总页数小于1"
            LastPage = -1
            Exit Function
        End If
        LastPage = TotalPage
    End Function

    Function NextPage(ByVal CurrentPage As Integer, ByVal TotalPage As Integer, ByRef strInfo As String) As Integer
        If TotalPage < 1 Then
            strInfo = "总页数小于1"
            NextPage = -1
            Exit Function
        End If
        If CurrentPage < 1 Then
            strInfo = "当前页数小于1"
            NextPage = -1
            Exit Function
        End If

        Dim intNextPage As Integer = CurrentPage + 1
        If intNextPage > TotalPage Then
            NextPage = TotalPage
            Exit Function
        End If
        NextPage = intNextPage

    End Function

    Function PrevPage(ByVal CurrentPage As Integer, ByVal TotalPage As Integer, ByRef strInfo As String) As Integer
        If TotalPage < 1 Then
            strInfo = "总页数小于1"
            PrevPage = -1
            Exit Function
        End If
        If CurrentPage < 1 Then
            strInfo = "当前页数小于1"
            PrevPage = -1
            Exit Function
        End If

        Dim intPrevPage As Integer = CurrentPage - 1
        If intPrevPage < 1 Then
            PrevPage = 1
            Exit Function
        End If
        PrevPage = intPrevPage
    End Function

    Function GoPages(ByVal CurrentPage As Integer, ByVal TotalPage As Integer, ByRef strInfo As String) As Integer
        If TotalPage < 1 Then
            strInfo = "总页数小于1"
            GoPages = -1
            Exit Function
        End If
        If CurrentPage < 1 Then
            strInfo = "跳转页数小于1"
            GoPages = 1
            Exit Function
        End If

        If CurrentPage > TotalPage Then
            strInfo = "跳转页大于总页数"
            GoPages = -1
            Exit Function
        End If
        GoPages = CurrentPage
    End Function

    Function GetTotalPageCount(ByVal TotalRowCount As Integer, ByVal RowCountByPage As Integer) As Integer
        Dim totalPageCount As Integer = 0
        Dim surplusPageCount As Integer = 0
        totalPageCount = Math.DivRem(TotalRowCount, RowCountByPage, surplusPageCount)
        If surplusPageCount > 0 Then
            totalPageCount = totalPageCount + 1
        End If
        GetTotalPageCount = totalPageCount
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'txtSpareCurrentPage.Text = txtCurrentPage.Text
        'txtCurrentPage.Text = 1
    End Sub

End Class
