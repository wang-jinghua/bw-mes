﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="pageTurning.ascx.vb" Inherits="pageTurning" %>
<style type="text/css">

a:link{color:black;}
a
{
    color: black;
    font-size:15px;
    text-decoration: none;
}

span, SPAN
{
	font-size:inherit;
}

.SubmitButton, .SubmitButtonHighlight,
.MaintCloseButton, .MaintNewButton, .MaintNewRevButton, .MaintSaveButton, .MaintDeleteButton, 
.MaintDeleteRevButton, .MaintCreateCopyButton, .MaintCreateRevCopyButton, .MaintSaveNewRevButton,
.PopupLinkButton, .MaintDeleteAllButton, .MaintViewAuditButton, .PopupLinkButtonXLarge,
.PopupLinkButtonSmall, .PopupLinkButtonMedium, .PopupLinkButtonLarge,
.SubmitButtonSmall, .SubmitButtonMedium, .SubmitButtonLarge, .SubmitButtonXLarge,
.ShopfloorCloseButton
{    
	cursor: hand;
	filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr='#F8F8F8',EndColorStr='#EBEAEA');
	font-family: Tahoma, Helvetica;
    font-weight: bold; 
    font-size: 12px;
	color:#2F97C4  !important;
    text-decoration:none;
 	padding-top: 2px;
	padding-left: 10px;
	padding-right: 10px;
	border-top: 1px solid #E4E4E4;
	border-left: 1px solid #E4E4E4;
	border-bottom: gray 1px outset;
	border-right: gray 1px outset;
}


.ReportButton
{
    background-color:#D6F1FF;
	border-color:#8EC2F5;
	border-width:1px;
    height: 19px;
}

</style>

 <script type="text/javascript">

   function openwins(tmp) 
            {  
             window.showModalDialog("ck_ContainerforOrder.aspx?ID=" + tmp ,"详细信息","dialogHeight: 280px; dialogWidth: 820px; dialogTop: 100px; dialogLeft: 150px; edge:        Raised; center: No; resizable: NO; status: No;");
             window.event.returnValue=false;
           }
           
     function enterSumbit()
           {  
                var event=arguments.callee.caller.arguments[0]||window.event;//消除浏览器差异  
                if(event.keyCode == 13)
                {  
                   window.event.returnValue=false;
                }  
           }                  

</script>

    <table style="height: 19px;">
    <tr><td align ="right">  
                    <asp:LinkButton ID="lbtnFirst" runat="server" style="z-index:200; font-size :13px ;">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID ="lbtnPrev" runat ="server"  style="z-index:200; font-size:13px ;">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID ="lbtnNext" runat ="server"  style="z-index:200; font-size:13px ;">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID ="lbtnLast" runat ="server"  style="z-index:200; font-size:13px ;">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID ="lLabel1" runat ="server" style="z-index :200; font-size:13px;" ForeColor ="red" Text ="第  页  共  页"></asp:Label>
                    <asp:Label ID ="lLabel2" runat ="server" style="z-index :200; font-size:13px ;" Text ="转到第"></asp:Label>
                    <input ID="txtPage" type="text" runat ="server" style="z-index :200; width:30px;" onkeydown ="enterSumbit();"/>
                    <asp:Label ID ="lLabel3" runat ="server" style="z-index :200; font-size:13px;" Text ="页"></asp:Label>
                    <asp:Button ID ="btnGo" runat ="server" style="z-index :200;" Text ="Go" CssClass="ReportButton"/>
                    <asp:TextBox ID ="txtTotalPage" runat ="server" 
                        style="z-index :200; width:30px;" Visible="False"></asp:TextBox>
                    <asp:TextBox ID ="txtCurrentPage" runat ="server" 
                        style="z-index :200; width:30px;" Width="49px" Visible="false">1</asp:TextBox>
                    <asp:TextBox ID ="txtSpareCurrentPage" runat ="server" 
                        style="z-index :200; width:30px;" Width="49px" Visible="false">1</asp:TextBox></td></tr>
    </table>
     
    
    