﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="MaterialRawApplyPopupForm.aspx.cs" Inherits="MaterialRawApplyPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>物料申请</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
     <script type="text/javascript" src="Scripts/Customer.js"></script>
      <script type="text/javascript">
        function IsDel()
        {
            Debug;
            if(confirm("确定要删除该菜单吗？"))
            {
              
                return true;
            }
            else
            {
                document.getElementById("btnDel").click();
                return false;
            }
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" style="margin-top:0px">
        <table border="0" cellpadding="5" cellspacing="0" width="100%" >
            <tr>
                <td class="tdNoBorder">
                    <div>原材料申请信息</div>
                        <igtbl:UltraWebGrid ID="wgRawMaterial" runat="server" Height="200px" Width="300px" >
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ContainerName" Key="ContainerName" Width="150px">
                                             <Header Caption="批次"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowStepName" Key="WorkflowStepName" Width="150px" Hidden="true">
                                             <Header Caption="工序"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="Qty" Key="Qty" Width="100px">
                                             <Header Caption="数量"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="MaterialName" Key="MaterialName" Width="100px">
                                             <Header Caption="TC原材料名称"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ReplaceMaterialName" Key="ReplaceMaterialName" Width="100px">
                                             <Header Caption="erp代料名称"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ReplaceMaterialQty" Key="ReplaceMaterialQty" Width="100px" Hidden="true">
                                             <Header Caption="erp代料所需数量"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="RawMaterial" Key="RawMaterial" Width="100px">
                                             <Header Caption="原材料信息"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="technicalconditions" Key="technicalconditions" Width="100px" hidden="true">
                                             <Header Caption="技术标准"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="materialguige" Key="materialguige" Width="100px" hidden="true">
                                             <Header Caption="材料规格"></Header>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="materialpaihao" Key="materialpaihao" Width="100px" hidden="true">
                                             <Header Caption="材料牌号"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="processquota" Key="processquota" Width="120px">
                                             <Header Caption="工艺定额信息"></Header>
                                        </igtbl:UltraGridColumn>
                                           <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="numberofblank" Key="numberofblank" Width="120px" Hidden="true">
                                             <Header Caption="毛坯中零件数"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ApplyClQty" Key="ApplyClQty" Width="100px" Hidden="false">
                                             <Header Caption="申请材料数"></Header>
                                        </igtbl:UltraGridColumn>
                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="IsSendErp" Key="IsSendErp" Width="100px" Hidden="false">
                                             <Header Caption="是否推送ERP"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="FLQty" Key="FLQty" Width="100px" Hidden="false">
                                             <Header Caption="ERP发料数"></Header>
                                        </igtbl:UltraGridColumn>
                                          <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ID" Key="ID" Width="100px" Hidden="true">
                                             <Header Caption="DispatchID"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ContainerID" Key="ContainerID" Width="100px" Hidden="true">
                                             <Header Caption="ContainerID"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="SpecID" Key="SpecID" Width="100px" Hidden="true">
                                             <Header Caption="SpecID"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProcessNo" Key="ProcessNo" Width="100px" Hidden="true">
                                             <Header Caption="ContainerID"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="MfgOrderName" Key="MfgOrderName" Width="100px" Hidden="true">
                                             <Header Caption="MfgOrderName"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProductID" Key="ProductID" Width="100px" Hidden="true">
                                             <Header Caption="ProductID"></Header>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="WorkflowID" Key="WorkflowID" Width="100px" Hidden="true">
                                             <Header Caption="MfgOrderName"></Header>
                                        </igtbl:UltraGridColumn>

                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="98%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="false">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                </td>
            </tr>
            <tr style="display:none">
                <td class="tdNoBorder">
                    <div class="divLabel">ERP发料数：</div>
                        <asp:TextBox ID="txtFlQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tdNoBorder">                    
                 <asp:Button ID="btnMaterialApp" runat="server" Text="提交" OnClientClick="disable_btn(this.id)" UseSubmitBehavior="False"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnMaterialApp_Click" />
                   <asp:Button ID="btnClose" runat="server" Text="关闭" style="margin-left:20px;"
                        CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" OnClick="btnClose_Click" />
                </td>
            </tr>
        </table>
        <div id="hdDiv" style="display:none">
            <asp:HiddenField ID="hdPopoupData" runat="server" />
        </div>
         <div style="margin: 5px;">
            <asp:Label runat="server" Style="font-size: 12px; font-weight: bold;">状态信息：</asp:Label>
            <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
        </div>
    </igmisc:WebAsyncRefreshPanel>
        
    </form>
</body>
</html>


