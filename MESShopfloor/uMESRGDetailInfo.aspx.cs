﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class uMESRGDetailInfo : System.Web.UI.Page
{

    uMESContainerPlatoonBusiness bll = new uMESContainerPlatoonBusiness();
    uMESCommonBusiness common = new uMESCommonBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        Dictionary<string, string> para = new Dictionary<string, string>();
        if (Request.QueryString["RGID"] != null)
        {
            para.Add("RGID", Request.QueryString["RGID"]);
            BindDate(para);
        }
    }

    protected void BindDate(Dictionary<string,string> para)
    {
        DataTable dt = (DataTable)Session["PlatoonDetail"];
        string strRGID = para["RGID"].ToString();

        DataRow[] rows = dt.Select(string.Format("ResourceGroupID = '{0}'", strRGID));

        DataTable dtList = dt.Clone();

        for (int i = 0; i < rows.Length; i++)
        {
            dtList.Rows.Add(rows[i].ItemArray);
        }

        dtList.Columns.Add("SpecNameDisp");
        dtList.Columns.Add("dblTotalGS");
        dtList.Columns.Add("RGGS");

        string strStdGS = "0";
        DataTable dtRGGS = (DataTable)Session["PlatoonRGGS"];
        rows = dtRGGS.Select(string.Format("ResourceGroupID = '{0}'", strRGID));
        if (rows.Length == 1)
        {
            strStdGS = (Convert.ToDouble(rows[0]["StdGS"].ToString()) / 60).ToString("0.00");
        }

        for (int i = 0; i < dtList.Rows.Count; i++)
        {
            string strSpecName = dtList.Rows[i]["specname"].ToString();
            dtList.Rows[i]["SpecNameDisp"] = common.GetSpecNameWithOutProdName(strSpecName);

            int intTotalGS = Convert.ToInt32(dtList.Rows[i]["TotalGS"].ToString());
            dtList.Rows[i]["dblTotalGS"] = Convert.ToDouble(intTotalGS / 60).ToString("0.00");

            dtList.Rows[i]["RGGS"] = strStdGS;
        }

        wgDispatch.DataSource = dtList;
        wgDispatch.DataBind();
    }
}