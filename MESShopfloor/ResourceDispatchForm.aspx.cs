﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Web.UI;

public partial class ResourceDispatchForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "ResourceDispatchForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();
    uMESSecondaryWarehouseBusiness stockBal = new uMESSecondaryWarehouseBusiness();

    string businessName = "工序任务管理", parentName = "dispatchinfo";
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：任务指派";
        master.strTitle = "任务指派";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
           // GetResource();
            GetEmployee();
        }
    }

    #region 显示资源当前已派工的任务
    protected void ddlResource_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strResourceID = ddlResource.SelectedValue;
            DataTable DT = dispatch.GetResourceDispatchInfo(strResourceID);

            wgDispatchList.DataSource = DT;
            wgDispatchList.DataBind();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    
    protected void wgDispatchList_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < wgDispatchList.Rows.Count; i++)
        {
            string strSpecName = wgDispatchList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            wgDispatchList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }
    #endregion

    #region 获取资源列表
    protected void GetResource()
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strFactoryID = userInfo["FactoryID"];
        string strTeamID = userInfo["TeamID"];
        DataTable DT = common.GetResource(strFactoryID, strTeamID);

        ddlResource.DataTextField = "ResourceName";
        ddlResource.DataValueField = "ResourceID";
        ddlResource.DataSource = DT;
        ddlResource.DataBind();

        ddlResource.Items.Insert(0, "");
    }
    #endregion

    #region 获取人员列表
    protected void GetEmployee()
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strTeamID = userInfo["TeamID"];
        string strFactoryID = userInfo["FactoryID"];
        DataTable DT = common.GetEmployee(strTeamID);
        if (DT.Rows.Count<=0)
        {
            DT = common.GetEmployeeByFactory(strFactoryID);
        }

        wgEmployee.DataSource = DT;
        wgEmployee.DataBind();
    }
    #endregion

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        if (Session[QueryWhere] == null)
        {
            string strScanContainerName = txtScan.Text.Trim();
            string strProcessNo = txtProcessNo.Text.Trim();
            string strContainerName = txtContainerName.Text.Trim();
            string strProductName = txtProductName.Text.Trim();
            string strSpecName = txtSpecName.Text.Trim();
            string strStartDate = txtStartDate.Value.Trim();
            string strEndDate = txtEndDate.Value.Trim();

            Dictionary<string, string> result = new Dictionary<string, string>();
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strTeamID = userInfo["TeamID"];
            result.Add("TeamID", strTeamID);
            result.Add("DispatchType", "0");
            result.Add("ScanContainerName", strScanContainerName);
            result.Add("ProcessNo", strProcessNo);
            result.Add("ContainerName", strContainerName);
            result.Add("ProductName", strProductName);
            result.Add("SpecName", strSpecName);
            result.Add("StartDate", strStartDate);
            result.Add("EndDate", strEndDate);
            result.Add("Status", "0,10,25");//物料申请在指派前，增加物料申请后的状态25 add:Wangjh
            result.Add("CurrentStepID", "1");

            Session[QueryWhere] = result;

            return result;
        }
        else
        {
            Dictionary<string, string> result = (Dictionary<string, string>)Session[QueryWhere];

            return result;
        }
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        uMESPagingDataDTO result = dispatch.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 9);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;

        ClearDispData();
    }

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion
    
    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if(e.Row.Cells.FromKey("ProcessNo").Value!=null)
            { 
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            txtDispContainerName.Text = strContainerName;
            string strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            txtDispContainerID.Text = strContainerID;

            string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            txtDispProductName.Text = strProductName;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }

            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            string strSpec = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
            txtDispSpecName.Text = strSpec;
            string strWorkflowID = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            txtDispWorkflowID.Text = strWorkflowID;
            string strSpecID = e.Row.Cells.FromKey("SpecID").Value.ToString();
            txtDispSpecID.Text = strSpecID;

            txtDispWFSID.Text = string.Empty;
            if (e.Row.Cells.FromKey("WorkflowStepID").Value != null)
            {
                string strWFSID = e.Row.Cells.FromKey("WorkflowStepID").Value.ToString();
                txtDispWFSID.Text = strWFSID;
            }

            string strTeamName = e.Row.Cells.FromKey("TeamName").Value.ToString();
            txtDispTeamName.Text = strTeamName;
            string strTeamID = e.Row.Cells.FromKey("TeamID").Value.ToString();
            txtDispTeamID.Text = strTeamID;

            txtPlannedCompletionDate.Value = string.Empty;
            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Value.ToString();
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
                txtPlannedCompletionDate.Value = strPlannedCompletionDate;
            }

            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtDispID.Text = strID;

            ddlResource.SelectedValue = string.Empty;

            TemplatedColumn temCell = (TemplatedColumn)wgEmployee.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                ckSelect.Checked = false;
            }

            wgProductNo.Clear();
            DataTable dtProductNo = dispatch.GetWillDispatchProductNo(strID);
            wgProductNo.DataSource = dtProductNo;
            wgProductNo.DataBind();

            txtQty.Text = string.Empty;
            txtDispatchedQty.Text = dispatch.GetDispatchedQty(strID).ToString();
            //if (dtProductNo.Rows.Count == 0)
            //{
                txtQty.Text = (Convert.ToInt32(e.Row.Cells.FromKey("contaiernqty").Value.ToString()) - Convert.ToInt32(txtDispatchedQty.Text)).ToString();
            //}

            wgDispatchList.Clear();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #endregion

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            ResultModel re= SaveData();

            if (re.IsSuccess == false)
            {
                DisplayMessage(re.Message,false);
                return;
            }

            //记录日志
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            var ml = new MESAuditLog();
            ml.ContainerName = txtDispContainerName.Text; ml.ContainerID = txtDispContainerID.Text;
            ml.ParentID = txtDispID.Text; ml.ParentName = parentName;
            ml.CreateEmployeeID = userInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = 0;
            ml.Description = "工序任务指派:" + txtDispSpecName.Text + ",指派给"+re.Data.ToString()+",数量:"+ txtQty.Text;
            common.SaveMESAuditLog(ml);

            QueryData(GetQuery());

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    /// <summary>
    /// 保存数据
    /// </summary>
    /// <returns></returns>
    ResultModel SaveData() {
        ResultModel re= new ResultModel(false,"");

        string strID = txtDispID.Text;
        if (strID == string.Empty)
        {
            re.Message = "请选择要指派的任务记录";
            return re;
        }

        UltraGridRow activeRow = ItemGrid.DisplayLayout.ActiveRow;
        string strQty = string.Empty;
        DataTable dtProductNo = new DataTable();
        string strMessage = string.Empty;
        if (CheckData(out strQty, out dtProductNo, out strMessage) == false)
        {
            re.Message = strMessage;
            return re;
        }

        if (int.Parse(activeRow.Cells.FromKey("SpecSequence").Text) > 1 && activeRow.Cells.FromKey("MoveConfirm").Text != "是")
        {
            re.Message = "移工未确认，无法指派任务";
            return re;
        }

        //查询已进行班组派工的信息
        DataTable dtDispatch = dispatch.GetDispatchInfoNew(txtDispContainerID.Text, txtDispWorkflowID.Text);

        string strDispatchInfoName = DateTime.Now.ToString("yyyyMMddHHmmssfff");
        string strContainerID = txtDispContainerID.Text;
        string strSpecID = txtDispSpecID.Text;
        string strWFSID = txtDispWFSID.Text;
        string strWorkflowID = txtDispWorkflowID.Text;
        string strTeamID = txtDispTeamID.Text;
        string strResourceID = ddlResource.SelectedValue;

        string strTotalgs = string.Empty;
        if (activeRow.Cells.FromKey("totalgs").Value != null)
        {
            strTotalgs = activeRow.Cells.FromKey("totalgs").Value.ToString();
        }

        string strSpecSequence = string.Empty;
        if (activeRow.Cells.FromKey("SpecSequence").Value != null)
        {
            strSpecSequence = activeRow.Cells.FromKey("SpecSequence").Value.ToString();
        }

        string strResourceGroupID = string.Empty;
        if (activeRow.Cells.FromKey("ResourceGroupID").Value != null)
        {
            strResourceGroupID = activeRow.Cells.FromKey("ResourceGroupID").Value.ToString();
        }

        string strIsAPS = string.Empty;
        if (activeRow.Cells.FromKey("IsAPS").Value != null)
        {
            strIsAPS = activeRow.Cells.FromKey("IsAPS").Value.ToString();
        }

        string strState = "1";
        if (activeRow.Cells.FromKey("state").Value != null)
        {
            strState = activeRow.Cells.FromKey("state").Value.ToString();
        }

        //批次数量
        string strContaiernQty = "0";
        if (activeRow.Cells.FromKey("contaiernqty").Value != null)
        {
            strContaiernQty = activeRow.Cells.FromKey("contaiernqty").Value.ToString();
        }
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strDispatchEmployeeID = userInfo["EmployeeID"];

        string strPlannedCompletionDate = txtPlannedCompletionDate.Value;
        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("DispatchInfoName", strDispatchInfoName);
        para.Add("ContainerID", strContainerID);
        para.Add("SpecID", strSpecID);
        para.Add("WorkflowStepID", strWFSID);
        para.Add("DispatchEmployeeID", strDispatchEmployeeID);
        para.Add("DispatchDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
        para.Add("DispatchType", "1");
        para.Add("DispatchToType", "1");
        para.Add("ContainerPhaseID", "");
        para.Add("PlannedStartDate", "");
        para.Add("PlannedCompletionDate", strPlannedCompletionDate);
        para.Add("Qty", strQty);
        para.Add("UOMID", "");
        para.Add("ResourceID", strResourceID);
        para.Add("Status", "0");
        para.Add("ParentID", strID);
        para.Add("WorkflowID", strWorkflowID);
        para.Add("TeamID", strTeamID);
        para.Add("TotalGS", strTotalgs);
        para.Add("SpecSequence", strSpecSequence);
        para.Add("ResourceGroupID", strResourceGroupID);
        para.Add("IsAPS", strIsAPS);
        para.Add("State", strState);


        //加工人员
        DataTable dtEmployee = new DataTable();
        dtEmployee.Columns.Add("EmployeeID");string workerName="";
        TemplatedColumn temCell = (TemplatedColumn)wgEmployee.Columns.FromKey("ckSelect");
        for (int i = 0; i < temCell.CellItems.Count; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

            if (ckSelect.Checked == true)
            {
                DataRow row = dtEmployee.NewRow();
                row["EmployeeID"] = wgEmployee.Rows[i].Cells.FromKey("EmployeeID").Value.ToString();
                workerName= wgEmployee.Rows[i].Cells.FromKey("FullName").Text.ToString();
                dtEmployee.Rows.Add(row);
            }
        }

        if (dtEmployee.Rows.Count==0)
        {
            re.Message = "请选择人员";
            return re;
        }

        //if (dtEmployee.Rows.Count > 1) {
        //    re.Message = "只能指派一人";
        //    return re;
        //}

        //判断该工序是否已指派
        bool isExecute = true;
        if (dtDispatch.Rows.Count > 0)
        {
            DataRow[] drDispatch = dtDispatch.Select("containerid ='" + strContainerID + "' AND workflowid='" + strWorkflowID + "' AND specid='" + strSpecID + "' AND dispatchtype='1' AND dispatchtotype='1'");
            if (drDispatch.Length > 0)
            {
                isExecute = false;
            }
        }
        if (isExecute == true)
        {
            dispatch.AddDispatchInfo(para, dtProductNo, dtEmployee);
        }
        else//此工序已经指派 如何处理？？？ add:Wangjh
        {

        }


        //更新班组派工单状态
        int intDispatchQty = Convert.ToInt32(txtDispQty.Text);
        int intDispatchedQty = Convert.ToInt32(txtDispatchedQty.Text);
        int intQty = Convert.ToInt32(strQty);

        if (intQty + intDispatchedQty >= Convert.ToInt32(strContaiernQty))
        {
            dispatch.ChangeDispatchStatus(strID, 15);
        }
        else
        {
            dispatch.ChangeDispatchStatus(strID, 10);
        }


        re = new ResultModel(true, "保存成功");
        re.Data = workerName;//返回指派的工人参数，后续有用
        return re;
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispTeamName.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;

        txtDispID.Text = string.Empty;
        ddlResource.SelectedValue = string.Empty;
        txtPlannedCompletionDate.Value = string.Empty;
        txtQty.Text = string.Empty;

        wgProductNo.Clear();
        wgDispatchList.Clear();

        TemplatedColumn temCell = (TemplatedColumn)wgEmployee.Columns.FromKey("ckSelect");

        for (int i = 0; i < temCell.CellItems.Count; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

            ckSelect.Checked = false;
        }
    }

    #region 数据验证
    protected Boolean CheckData(out string strDipatchQty, out DataTable dtProductNo, out string strMessage)
    {
        Boolean result = true;

        strMessage = string.Empty;

        string strResourceID = ddlResource.SelectedValue;

        if (strResourceID == "")
        {
           // strMessage = "请选择设备/工位";
           // ddlResource.Focus();
           // result = false;
        }

        int intQty = 0;
        DataTable DT = new DataTable();
        DT.Columns.Add("ContainerID");
        DT.Columns.Add("ContainerName");
        DT.Columns.Add("ProductNo");

        if (wgProductNo.Rows.Count > 0)
        {
            TemplatedColumn temCell = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                //if (ckSelect.Checked == true)
                //{
                    intQty++;

                    DataRow row = DT.NewRow();
                    row["ContainerID"] = wgProductNo.Rows[i].Cells.FromKey("ContainerID").Value.ToString();
                    row["ContainerName"] = wgProductNo.Rows[i].Cells.FromKey("ContainerName").Value.ToString();
                    row["ProductNo"] = wgProductNo.Rows[i].Cells.FromKey("ProductNo").Value.ToString();
                    DT.Rows.Add(row);
                //}
            }

            if (intQty == 0)
            {
                strMessage = "请勾选产品序号";
                result = false;
            }
        }
        else
        {
            string strQty = txtQty.Text.Trim();

            if (strQty == string.Empty)
            {
                strMessage = "请输入数量";
                txtQty.Focus();
                result = false;
            }
            else
            {
                if (strQty.Contains("."))
                {
                    strMessage = "数量应为正数";
                    txtQty.Focus();
                    result = false;
                }
                else
                {
                    try
                    {
                        intQty = Convert.ToInt32(strQty);

                        if (intQty <= 0)
                        {
                            strMessage = "数量应为正整数";
                            txtQty.Focus();
                            result = false;
                        }
                    }
                    catch
                    {
                        strMessage = "数量应为数字";
                        txtQty.Focus();
                        result = false;
                    }
                }
            }
        }

        if (intQty + Convert.ToInt32(txtDispatchedQty.Text) > Convert.ToInt32(txtDispQty.Text))
        {
            strMessage = "总数量不能大于班组派工数量";
            txtQty.Focus();
            result = false;
        }

        strDipatchQty = intQty.ToString();
        dtProductNo = DT.Copy();
        return result;
    }
    #endregion

    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                string strTeamID = userInfo["TeamID"];

                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("TeamID", strTeamID);
                para.Add("DispatchType", "0");
                para.Add("Status", "0,10,25");
                para.Add("CurrentStepID", "1");

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            ClearMessage();

            UltraGridRow uldr = ItemGrid.DisplayLayout.ActiveRow;
            if (uldr == null)
            {
                DisplayMessage("请选择订单记录", false);
                //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script>alert('请选择批次记录')</script>");
                return;
            }

            DataTable poupDt = new DataTable();
            poupDt.Columns.Add("ProductID");
            poupDt.Columns.Add("WorkflowID");
            DataRow newRow = poupDt.NewRow();
            newRow["ProductID"] = uldr.Cells.FromKey("ProductID").Text;
            newRow["WorkflowID"] = uldr.Cells.FromKey("Workflowid").Text;
            poupDt.Columns.Add("ContainerID"); poupDt.Columns.Add("ContainerName");
            newRow["ContainerID"] = uldr.Cells.FromKey("ContainerID").Text;
            newRow["ContainerName"] = uldr.Cells.FromKey("ContainerName").Text;
            poupDt.Rows.Add(newRow);
            Session.Add("ProcessDocument", poupDt);
            //string strScript = string.Empty;

            //strScript = "<script>window.showModalDialog('Custom/bwCommonPage/uMESDocumentViewPopupForm.aspx', '', 'dialogWidth: 700px; dialogHeight: 600px; status = no; center: Yes; resizable: NO; ')</script>";
            //Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);

            var page = "Custom/bwCommonPage/uMESDocumentViewPopupForm.aspx";
            var script = String.Format("<script>OpenPopupWindow(false,'{0}','dialogWidth={1}px;dialogHeight={2}px;status=0');</script>", page, 700, 600);
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, script);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ItemGrid_InitializeRow(object sender, RowEventArgs e)
    {
        try {
            //查询显示线边库信息 add:Wangjh 20200921
            var para = new Dictionary<string, string>();
            para["ContainerID"] = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            para["WorkflowID"] = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            para["SpecID"] = e.Row.Cells.FromKey("SpecID").Value.ToString(); ;
            DataTable tempDt = stockBal.GetSubmittostockinfo(para);
            if (tempDt.Rows.Count == 0)
            {
                e.Row.Cells.FromKey("MoveConfirm").Value = "否";
            }
            else
            {
                // e.Row.Cells.FromKey("FactoryStockName").Value = tempDt.Rows[0]["factorystockname"].ToString(); 
                e.Row.Cells.FromKey("MoveConfirm").Value = "是";
            }
        } catch (Exception ex) {
            DisplayMessage(ex.Message,false);
        }
    }
}