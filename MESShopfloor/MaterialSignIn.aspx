﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="MaterialSignIn.aspx.cs" Inherits="MaterialSignIn" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent"  runat="Server">
    <script type="text/javascript">
        function openproduct(control) {
            var someValue = window.showModalDialog("uMES_ProductListForm.aspx", "", "dialogWidth:420px; dialogHeight:600px; status=no; center: Yes; resizable: NO;");
            if (someValue != "" && someValue != undefined) {
                var array = someValue.split(":");
                control.value = array[0] + ":" + array[3];
                document.getElementById("<%=txtProduct.ClientID%>").value = someValue;
            }
        }
        function openplan(control) {
            var strType = document.getElementById("<%=txtType.ClientID%>").value;

            if (strType == "JLJH") {
                var someValue = window.showModalDialog("uMES_PlanListForm.aspx", "", "dialogWidth:670px; dialogHeight:600px; status=no; center: Yes; resizable: NO;");
                if (someValue != "" && someValue != undefined) {
                    var array = someValue.split(",");
                    control.value = array[0];
                    document.getElementById("<%=txtBillID.ClientID%>").value = array[1];
                    document.getElementById("<%=txtStatus.ClientID%>").value = array[2];

                    var select = document.getElementById("<%=ddlFamily.ClientID%>");

                    for (var i = 0; i < select.options.length; i++) {

                        if (select.options[i].value == array[3]) {

                            select.options[i].selected = true;
                            break;
                        }
                    }
                }
            }

            if (strType == "CCD") {
                var someValue = window.showModalDialog("uMES_CCDListForm.aspx", "", "dialogWidth:670px; dialogHeight:600px; status=no; center: Yes; resizable: NO;");
                if (someValue != "" && someValue != undefined) {
                    var array = someValue.split(",");
                    control.value = array[0];
                    document.getElementById("<%=txtBillID.ClientID%>").value = array[1];
                    document.getElementById("<%=txtStatus.ClientID%>").value = array[2];

                    var select = document.getElementById("<%=ddlFamily.ClientID%>");

                    for (var i = 0; i < select.options.length; i++) {

                        if (select.options[i].value == array[3]) {

                            select.options[i].selected = true;
                            break;
                        }
                    }
                }
            }
        }
    </script>
<table style="border-collapse: collapse; border-color: #8ec2f5" border="1"
            cellpadding="2">
            <tr>
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px; font-weight:bold;" align="left">
                    车型：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlFamily" runat="server" Width="150px" AutoPostBack="false" >
                    </asp:DropDownList>
                </td> 
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    责任单位：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlFactory" runat="server" Width="150px" AutoPostBack="false" >
                    </asp:DropDownList>
                </td> 
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    班组：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlTeam" runat="server" Width="150px" AutoPostBack="false" >
                    </asp:DropDownList>
                </td>   
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px; font-weight:bold;" align="left">
                    台份数：
                </td>
                <td align="left" style="width: 150px">
                    <asp:TextBox ID="txtQty" runat="server" class="ReportTextBox" 
                        Width="150px"></asp:TextBox>
                </td> 
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    类型：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlType" runat="server" Width="150px" AutoPostBack="True" 
                        onselectedindexchanged="ddlType_SelectedIndexChanged" >
                    </asp:DropDownList><br />
                    <asp:TextBox ID="txtType" runat="server" Text="BOM" style="display:none;"></asp:TextBox>
                </td>
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    选择差错单/<br />进料计划：
                </td>
                <td align="left" style="width: 150px">
                    <input type="text" id="txtPlanNoDisp" runat="server" onclick="openplan(this)" readonly="readonly" class="ReportTextBox"  style="width:150px" />
                    <input type="text" id="txtBillID" runat="server" readonly="readonly" class="ReportTextBox"  style="width:150px;display:none;" />
                    <asp:TextBox ID="txtFamilyID" runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="txtFamilyName" runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="txtPlanNo" runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="txtStatus" runat="server" style="display:none;"></asp:TextBox>
                    <asp:TextBox ID="txtCCDParentID" runat="server" style="display:none;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    令号：
                </td>
                <td align="left" style="width: 150px">
                    <asp:TextBox ID="txtProcessNo" runat="server" class="ReportTextBox" 
                        Width="150px"></asp:TextBox>
                </td>                    
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px; font-weight:bold;" align="left">
                    计划日期：
                </td>
                <td align="left" style="width: 150px">
                    <input id="txtPlanDate" runat="server" onclick="this.value='';setday(this)" class="ReportTextBox"
                        style="width: 150px;" type="text" readonly="readonly" />
                </td>
                <td style="color: #07519a; background-color: #D6F1FF; height: 15px;" align="left">
                    配送地点：
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlStation" runat="server" Width="150px" >
                    </asp:DropDownList>
                </td>                        
                <td style="text-align: left;" colspan="6">
                    <asp:Button ID="btnReSet" runat="server" Text="重置" 
                        CssClass="ReportButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    <asp:Button ID="btnRun" runat="server" Text="生成配送单"
                        CssClass="ReportButton" EnableTheming="True" onclick="btnRun_Click" />
                    <asp:Button ID="btnSave" runat="server" Text="保存"
                        CssClass="ReportButton" EnableTheming="True" onclick="btnSave_Click" />
                    <asp:Button ID="btnExport" runat="server" Text="Excel导出" Width="80px" CssClass="ReportButton"
                        EnableTheming="True" onclick="btnExport_Click" />
                </td>
            </tr>
        </table>
  <div id="ItemDiv" runat="server">
        <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="430px" Width="100%">
            <Bands>
                <igtbl:UltraGridBand>
                <Columns>
                <igtbl:TemplatedColumn AllowUpdate="Yes" DataType="System.Boolean" Key="ckSelect"
                                    Type="CheckBox" Width="30px">
                                </igtbl:TemplatedColumn>

                    <igtbl:TemplatedColumn BaseColumnName="FactoryID" Key="FactoryID" Type="DropDownList" AllowUpdate="Yes">
                                    
                                    <Header Caption="责任单位">
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Footer>
                                </igtbl:TemplatedColumn>
                <igtbl:UltraGridColumn Key="Sequence" Width="60px" AllowUpdate="Yes" BaseColumnName="Sequence">
<Header Caption="序号">
<RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
</Header>

<Footer>
<RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
</Footer>
                        </igtbl:UltraGridColumn>
                    <igtbl:TemplatedColumn BaseColumnName="WorkStationID" Key="WorkStationID" Type="DropDownList" AllowUpdate="Yes">
                                    
                                    <Header Caption="配送地点">
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Footer>
                                </igtbl:TemplatedColumn>
                        <igtbl:UltraGridColumn Key = "ProductName" Width ="120px" 
                        BaseColumnName ="ProductName">
                            <header Caption="图号">
                                <RowLayoutColumnInfo OriginX = "2"></RowLayoutColumnInfo>
                            </header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX = "2"></RowLayoutColumnInfo>
                            </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key = "Description" Width ="200px" 
                        BaseColumnName ="Description">
                            <header Caption="名称">
                                <RowLayoutColumnInfo OriginX = "2"></RowLayoutColumnInfo>
                            </header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX = "2"></RowLayoutColumnInfo>
                            </Footer>
                         </igtbl:UltraGridColumn>

                         <igtbl:UltraGridColumn Key ="QtyRequired" Width = "80px" 
                        BaseColumnName = "QtyRequired" AllowUpdate="Yes">
                             <header Caption = "需求数量">
                               <RowLayoutColumnInfo OriginX = "2"/>
                             </header>
                             <Footer>
                             <RowLayoutColumnInfo OriginX = "2" />
                             </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key ="Qty" Width = "80px" 
                        BaseColumnName = "qtyrequired" AllowUpdate="Yes">
                             <header Caption = "配送数量">
                               <RowLayoutColumnInfo OriginX = "2"/>
                             </header>
                             <Footer>
                             <RowLayoutColumnInfo OriginX = "2" />
                             </Footer>
                         </igtbl:UltraGridColumn>
                         <igtbl:UltraGridColumn Key = "WorkflowStr" Width = "200px" 
                        BaseColumnName = "WorkflowStr"  >
                             <header Caption = "路线">
                               <RowLayoutColumnInfo OriginX = "2"/>
                             </header>
                             <Footer>
                              <RowLayoutColumnInfo OriginX = "2"/>
                             </Footer>
                        </igtbl:UltraGridColumn>
                        
                    <igtbl:TemplatedColumn BaseColumnName="TeamID" Key="TeamID" Type="DropDownList" AllowUpdate="Yes">
                                    
                                    <Header Caption="班组">
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1" />
                                    </Footer>
                                </igtbl:TemplatedColumn>
                        <igtbl:UltraGridColumn Key="ProductID" Width="80px" 
                        BaseColumnName="ProductID" Hidden="True">
                            <Header Caption="ProductID">
                                <RowLayoutColumnInfo OriginX="4"></RowLayoutColumnInfo>
                            </Header>
                            <Footer>
                                <RowLayoutColumnInfo OriginX="4"></RowLayoutColumnInfo>
                            </Footer>
                        </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="ID" Hidden="True" Key="ID">
                        <Header Caption="ID">
                            <RowLayoutColumnInfo OriginX="12" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="12" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="DParentID" Hidden="True" Key="ParentID">
                        <Header Caption="ParentID">
                            <RowLayoutColumnInfo OriginX="13" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="13" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="FactoryName" Hidden="True" Key="FactoryName">
                        <Header Caption="FactoryName">
                            <RowLayoutColumnInfo OriginX="13" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="13" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="TeamName" Hidden="True" Key="TeamName">
                        <Header Caption="TeamName">
                            <RowLayoutColumnInfo OriginX="13" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="13" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    <igtbl:UltraGridColumn BaseColumnName="WorkStationName" Hidden="True" Key="WorkStationName">
                        <Header Caption="WorkStationName">
                            <RowLayoutColumnInfo OriginX="13" />
                        </Header>
                        <Footer>
                            <RowLayoutColumnInfo OriginX="13" />
                        </Footer>
                    </igtbl:UltraGridColumn>
                    </Columns>
                    <AddNewRow View="NotSet" Visible="NotSet">
                    </AddNewRow>
                </igtbl:UltraGridBand>       
            </Bands>
            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AllowSortingDefault="OnClient"
                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                RowHeightDefault="18px">
                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="430px"
                    Width="100%">
                </FrameStyle>
                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                </RowAlternateStyleDefault>
                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                    StyleMode="QuickPages">
                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </PagerStyle>
                </Pager>
                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                </EditCellStyleDefault>
                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                </FooterStyleDefault>
                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Bold="True">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    <Padding Bottom="3px" Top="2px" />
                    <Padding Top="2px" Bottom="3px"></Padding>
                </HeaderStyleDefault>
                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                    <Padding Left="3px" />
                </RowSelectorStyleDefault>
                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="25px" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" CssClass="GridRowStyle">
                    <Padding Left="3px" />
                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                </RowStyleDefault>
                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                </GroupByRowStyleDefault>
                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                </SelectedRowStyleDefault>
                <GroupByBox Hidden="True">
                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                    </BoxStyle>
                </GroupByBox>
                <AddNewBox>
                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </BoxStyle>
                </AddNewBox>
                <ActivationObject BorderColor="" BorderWidth="">
                </ActivationObject>
                <FilterOptionsDefault FilterUIType="HeaderIcons">
                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px" Height="420px" Width="200px">
                        <Padding Left="2px" />
                    </FilterDropDownStyle>
                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                    </FilterHighlightRowStyle>
                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px">
                        <Padding Left="2px" />
                    </FilterOperandDropDownStyle>
                </FilterOptionsDefault>
            </DisplayLayout>
        </igtbl:UltraWebGrid>
    </div>
    <div>
        <table style="width: 100%;">
            <tr>
                <td style="text-align :left; width: 50%;">    
                    <asp:Button ID="btnSelectAll" runat="server" Text="全选"
                        CssClass="ReportButton" EnableTheming="True" 
                        onclick="btnSelectAll_Click" />
                    <asp:Button ID="btnReverseSelect" runat="server" Text="反选"
                        CssClass="ReportButton" EnableTheming="True" 
                        onclick="btnReverseSelect_Click" />
                    <asp:Button ID="btnDelete" runat="server" Text="删除"
                        CssClass="ReportButton" EnableTheming="True" onclick="btnDelete_Click" />
                        
                    <input type="text" id="txtProductDisp" runat="server" onclick="openproduct(this)" readonly="readonly" class="ReportTextBox"  style="width:300px" />
                    <input type="text" id="txtProduct" runat="server" readonly="readonly" class="ReportTextBox"  style="width:250px; display:none;" />

                    <asp:Button ID="btnAdd" runat="server" Text="添加"
                        CssClass="ReportButton" EnableTheming="True" onclick="btnAdd_Click" />
                </td>
                <td style="text-align: right; width: 50%;">
                    
                </td>
            </tr>
        </table>
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
  </div>
</asp:Content>
