﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uMESQualityRecordInfoPopupForm.aspx.cs"
  ValidateRequest="false" Inherits="uMESQualityRecordInfoPopupForm" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="uMESCustomControls/ToleranceInput/wucToleranceInput.ascx" TagName="wucToleranceInput" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>质量录入</title>
    <base target="_self" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
    <script type="text/javascript">
        function openrejectappsubmit() {
            var someValue = window.showModalDialog("RejectAppSubmitPopupForm.aspx?v=" + new Date().getTime(), "", "dialogWidth:1200px; dialogHeight:480px; status=no; center: Yes; resizable: NO;");
            if (someValue != "" && someValue != undefined) {
                document.getElementById("<%=btnClosePopup.ClientID%>").click();
            }
        }

        function IsDel() {
            if (confirm("确定要删除该派工记录吗？")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body scroll="no">
    <form id="form1" runat="server">

        <%--        <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" RefreshTargetIDs="WebAsyncRefreshPanel2" Width="1064px">--%>
        <div>
            <table>
                <tr>
                    <td colspan="6">
                        <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="150px" OnActiveRowChange="ItemGrid_ActiveRowChange" Width="100%">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn Width="20px" AllowGroupBy="No" Key="ckSelect" Hidden="false">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption=""></Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn Key="ProcessNo" Width="120px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                            <Header Caption="工作令号">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="120px" AllowGroupBy="No" Hidden="true">
                                            <Header Caption="作业令号">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="120px" AllowGroupBy="No">
                                            <Header Caption="批次号">
                                                <RowLayoutColumnInfo OriginX="3" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="3" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="120px" AllowGroupBy="No">
                                            <Header Caption="图号">
                                                <RowLayoutColumnInfo OriginX="4" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="4" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="120px" AllowGroupBy="No">
                                            <Header Caption="名称">
                                                <RowLayoutColumnInfo OriginX="5" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="5" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SpecDisName" Key="SpecDisName" Width="80px" AllowGroupBy="No" Hidden="true">
                                            <Header Caption="工序">
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="6" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="qty" Hidden="false" Key="qty" Width="60px">
                                            <Header Caption="不合格数量">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="disissubmit" Hidden="false" Key="disissubmit">
                                            <Header Caption="是否提交不合格审理">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="IsSubmit" Hidden="true" Key="IsSubmit">
                                            <Header Caption="是否提交不合格审理">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SubmitDate" Hidden="false" Key="SubmitDate"
                                            DataType="Date" Format="yyyy-MM-dd">
                                            <Header Caption="记录时间">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ID" Hidden="True" Key="ID">
                                            <Header Caption="ID">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="QualityRecordInfoName" Hidden="True" Key="QualityRecordInfoName">
                                            <Header Caption="QualityRecordInfoName">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="RevUnqualProId" Hidden="True" Key="RevUnqualProId">
                                            <Header Caption="RevUnqualProId">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SubmitNotes" Hidden="True" Key="SubmitNotes">
                                            <Header Caption="SubmitNotes">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ReportInfoID" Hidden="True" Key="ReportInfoID">
                                            <Header Caption="ReportInfoID">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="SpecName" Hidden="True" Key="SpecName">
                                            <Header Caption="SpecName">
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="11" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="100%">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="150px" Width="100%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle" Wrap="true">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="10px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
                <tr style="height: 1px;">
                    <td></td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label3" runat="server" Text=" 工作令号" Font-Bold="true" Font-Size="13pt" ForeColor="Black"></asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                        </div>
                    </td>
                    <td align="left" class="tdRightAndBottom" style="display:none">
                        <div style="width: 100%;">
                            <asp:Label ID="Label7" runat="server" Text="作业令号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label8" runat="server" Text="批次号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label9" runat="server" Text="图号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label10" runat="server" Text="名称" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div style="width: 100%;">
                            <asp:Label ID="Label11" runat="server" Text="批次数量" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtConQty" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label12" runat="server" Text="计划开始时间" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtStartTime" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label1" runat="server" Text="计划结束时间" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtEndTime" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label4" runat="server" Text="工序" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtSpecDisName" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label13" runat="server" Text="质量记录单号" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtQualRecordID" runat="server" class="stdTextBox" ReadOnly="true" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div style="width: 100%">
                            <asp:Label ID="Label2" runat="server" Text="不合格数量" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <asp:TextBox ID="txtUnQualQty" runat="server" class="stdTextBox" Font-Size="Medium"></asp:TextBox>
                    </td>
                    <td rowspan="2" style="display:none">
                        <table width="100%">
                            <tr>
                                <td>
                                    <igtbl:UltraWebGrid ID="wgProductNo" runat="server" Height="229px" Width="170px">
                                        <Bands>
                                            <igtbl:UltraGridBand>
                                                <Columns>
                                                    <igtbl:TemplatedColumn Width="20px" AllowGroupBy="No" Key="ckSelect" Hidden="false">
                                                        <CellTemplate>
                                                            <asp:CheckBox ID="ckSelect" runat="server" />
                                                        </CellTemplate>
                                                        <Header Caption=""></Header>
                                                    </igtbl:TemplatedColumn>
                                                    <igtbl:UltraGridColumn Key="productno" Width="100px" BaseColumnName="productno" AllowGroupBy="No">
                                                        <Header Caption="产品序号">
                                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn Key="ContainerID" Width="100px" BaseColumnName="ContainerID" AllowGroupBy="No" Hidden="true">
                                                        <Header Caption="ContainerID">
                                                            <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                    <igtbl:UltraGridColumn Key="ContainerName" Width="100px" BaseColumnName="ContainerName" AllowGroupBy="No" Hidden="true">
                                                        <Header Caption="ContainerName">
                                                            <RowLayoutColumnInfo OriginX="3"></RowLayoutColumnInfo>
                                                        </Header>
                                                        <Footer>
                                                            <RowLayoutColumnInfo OriginX="3"></RowLayoutColumnInfo>
                                                        </Footer>
                                                    </igtbl:UltraGridColumn>
                                                </Columns>
                                                <AddNewRow View="NotSet" Visible="NotSet">
                                                </AddNewRow>
                                            </igtbl:UltraGridBand>
                                        </Bands>
                                        <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" RowSelectorsDefault="No"
                                            BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                            SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                            TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                            CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                            RowHeightDefault="100%">
                                            <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="229px" Width="100%">
                                            </FrameStyle>
                                            <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                            </RowAlternateStyleDefault>
                                            <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                                StyleMode="QuickPages">
                                                <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                </PagerStyle>
                                            </Pager>
                                            <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                            </EditCellStyleDefault>
                                            <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                            </FooterStyleDefault>
                                            <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                                CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                <Padding Bottom="3px" Top="2px" />
                                                <Padding Top="2px" Bottom="3px"></Padding>
                                            </HeaderStyleDefault>
                                            <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                                <Padding Left="3px" />
                                            </RowSelectorStyleDefault>
                                            <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid" Wrap="true"
                                                BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                                <Padding Left="3px" />
                                                <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                            </RowStyleDefault>
                                            <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                            </GroupByRowStyleDefault>
                                            <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                            </SelectedRowStyleDefault>
                                            <GroupByBox Hidden="True">
                                                <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                                </BoxStyle>
                                            </GroupByBox>
                                            <AddNewBox>
                                                <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                                </BoxStyle>
                                            </AddNewBox>
                                            <ActivationObject BorderColor="" BorderWidth="">
                                            </ActivationObject>
                                            <FilterOptionsDefault FilterUIType="HeaderIcons">
                                                <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                                    CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                    Font-Size="11px" Height="420px" Width="200px">
                                                    <Padding Left="2px" />
                                                </FilterDropDownStyle>
                                                <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                                </FilterHighlightRowStyle>
                                                <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                                    BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                                    Font-Size="11px">
                                                    <Padding Left="2px" />
                                                </FilterOperandDropDownStyle>
                                            </FilterOptionsDefault>
                                        </DisplayLayout>
                                    </igtbl:UltraWebGrid>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnAllSelect" runat="server" Text="全选"
                                                    CssClass="searchButton" EnableTheming="True" OnClick="btnAllSelect_Click" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnRevSelect" runat="server" Text="反选"
                                                    CssClass="searchButton" EnableTheming="True" OnClick="btnRevSelect_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td valign="top" colspan="5">
                        <div>
                            <asp:Label ID="Label5" runat="server" Text="质量信息描述" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
                        </div>
                        <uc1:wucToleranceInput ID="MyToleranceInput" runat="server" />
                        <asp:TextBox ID="txtQualityNotes" runat="server" class="stdTextBox" Height="200px" Width="550px" Visible="false"></asp:TextBox>
                    </td>

                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: left; width: 100%;">
                                    <asp:Button ID="btnConfirm" runat="server" Text="确定"
                                        CssClass="searchButton" EnableTheming="True" OnClick="btnConfirm_Click" />

                                </td>
                                <td style="padding-left: 20px;">
                                    <asp:Button ID="btnAdd" runat="server" Text="新增"
                                        CssClass="searchButton" EnableTheming="True" OnClick="btnAdd_Click" />
                                </td>
                                <td style="padding-left: 20px;">
                                    <asp:Button ID="btnUnQulDis" runat="server" Text="提交不合格品审理"
                                        CssClass="searchButton" EnableTheming="True" OnClick="btnUnQulDis_Click" />

                                </td>
                                <td style="display:none;">
                                    <asp:Button ID="btnClosePopup" runat="server" Text="关闭弹出界面后触发" OnClick="btnClosePopup_Click" />
                                </td>
                                <td style="padding-left: 20px;">
                                    <asp:Button ID="btnClose" runat="server" Text="关闭"
                                        CssClass="searchButton" EnableTheming="True" OnClientClick="window.close()" />
                                </td>
                            </tr>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="font-size: 12px; font-weight: bold;" nowrap="nowrap">状态信息：</td>
                                <td style="text-align: left; width: 100%;">
                                    <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="display: none; visibility: hidden">
            <!--跟踪卡ID-->
            <asp:TextBox ID="txtContainerID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--报工单ID-->
            <asp:TextBox ID="txtReportInfoID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--工序ID-->
            <asp:TextBox ID="txtSpecID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--计量单位ID-->
            <asp:TextBox ID="txtUomID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--流水编号-->
            <asp:TextBox ID="txtSerialNumber" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--工艺规程编号 -->
            <asp:TextBox ID="txtWorkflowID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--是否提交不合格品审理-->
            <asp:TextBox ID="txtIsSubmit" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--质量记录ID-->
            <asp:TextBox ID="txtID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--当前选中工序名称含图号-->
            <asp:TextBox ID="txtSSpecName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>

            <!--初始工艺规程编号 -->
            <asp:TextBox ID="txtIWorkflowID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--初始工序ID-->
            <asp:TextBox ID="txtISpecID" runat="server" class="stdTextBox" Height="100px" Width="680px"></asp:TextBox>
            <!--初始工序名称-->
            <asp:TextBox ID="txtISpecName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>
            <!--初始工序名称含图号-->
            <asp:TextBox ID="txtFSpecName" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>

            <!--初始报工单ID-->
            <asp:TextBox ID="txtIReportInfoID" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>

            <!--初始报工单不合格数量-->
            <asp:TextBox ID="txtNonsenseQty" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>

                        <!--初始报工单数量-->
            <asp:TextBox ID="txtReportQty" runat="server" class="stdTextBox" ReadOnly="true" Enabled="false"></asp:TextBox>

            <div>
                <asp:Label ID="Label14" runat="server" Text="备注" Font-Bold="true" Font-Size="12pt" ForeColor="Black"></asp:Label>
            </div>
            <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" Height="200px" Width="120px"></asp:TextBox>
        </div>
        <%--     </igmisc:WebAsyncRefreshPanel>--%>
    </form>
</body>
</html>
