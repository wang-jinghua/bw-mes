﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using uMES.LeanManufacturing.DBUtility;
public partial class top : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            if (userInfo == null)
            {
                Response.Write("<script>window.top.location.replace('/MESShopfloor');</script>");
                return;
            }
            lblEmployee.Text = userInfo["FullName"];
            lblFactory.Text = userInfo["FactoryDesc"];

            //lblRevision.Text = "V7.1.15&nbsp;&nbsp;&nbsp;&nbsp;";

            lblRevision.Text = "V7.1.15&nbsp;";
        }
    }

    protected void lbExit_Click(object sender, EventArgs e)
    {
        if (Session["UserInfo"] != null)//add:Wangjh
        { 
            var userInfo = Session["UserInfo"] as Dictionary<string, string>;
            OracleHelper.ExecuteDataByEntity(new ExcuteEntity("userlogininfo", ExcuteType.update) {
                ExcuteFileds =new List<FieldEntity>() { new FieldEntity("isonline",0,FieldType.Numer)},
                WhereFileds = new List<FieldEntity>() { new FieldEntity("userlogininfoid", userInfo["UserLoginInfoID"],FieldType.Str)}
            });
        }
        Session.Remove("UserInfo");
    }
}
