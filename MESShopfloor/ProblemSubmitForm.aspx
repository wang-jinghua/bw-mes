﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="ProblemSubmitForm.aspx.cs" Inherits="ProblemSubmitForm" EnableViewState="true" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function IsDel() {
            if (confirm("确定要删除该问题记录吗？")) {
                return true;
            }
            else {
                return false;
            }
        }

        function IsClose() {
            if (confirm("确定要关闭该问题记录吗？")) {
                return true;
            }
            else {
                return false;
            }
        }

        function openContainerInfo() {
            var someValue = window.showModalDialog("uMESContainerSelectPopupForm.aspx", "", "dialogWidth:1050px; dialogHeight:600px; status=no; center: Yes; resizable: NO;");
            if (someValue != "" && someValue != undefined) {

                document.getElementById("<%=ipContainer.ClientID%>").value = someValue;
                document.getElementById("<%=Button1.ClientID%>").click();
            }
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="8" class="tdBottom">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left">
                        <div class="divLabel">类型：</div>
                        <asp:DropDownList ID="ddlSearchType" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;">
                        </asp:DropDownList>
                    </td>
                    <td class="tdRightAndBottom" align="left">
                        <div class="divLabel">级别：</div>
                        <asp:DropDownList ID="ddlSearchLevel" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;">
                        </asp:DropDownList>
                    </td>
                    <td class="tdRightAndBottom" align="left">
                        <div class="divLabel">要求处理部门：</div>
                        <asp:DropDownList ID="ddlSearchFactory" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;" AutoPostBack="True" OnSelectedIndexChanged="ddlSearchFactory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="tdRightAndBottom" align="left">
                        <div class="divLabel">要求处理人：</div>
                        <asp:DropDownList ID="ddlSearchEmployee" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;">
                        </asp:DropDownList>
                    </td>
                    <td class="tdBottom" align="left">&nbsp;</td>
                    <td class="tdBottom" align="left">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">图号/名称：</div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">标题：</div>
                        <asp:TextBox ID="txtTitle" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" nowrap="nowrap" class="tdRight">
                        <div class="divLabel">要求处理日期：</div>
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td class="tdNoBorder" style="text-align: left;" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 8px; width: 100%;"></div>

        <div>
            <igtbl:UltraWebGrid ID="wgProblem" runat="server" Height="200px" Width="100%" OnActiveRowChange="wgProblem_ActiveRowChange">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:TemplatedColumn AllowGroupBy="No" Hidden="True" Key="ckSelect" Width="40px">
                                <CellTemplate>
                                    <asp:CheckBox ID="ckSelect" runat="server" />
                                </CellTemplate>
                                <Header Caption="">
                                </Header>
                            </igtbl:TemplatedColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px">
                                <Header Caption="批次号">
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="SpecNameDisp" BaseColumnName="SpecNameDisp" Width="80px">
                                <Header Caption="工序">
                                    <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn AllowGroupBy="No" BaseColumnName="ProblemTitle" Key="ProblemTitle" Width="150px">
                                <Header Caption="标题">
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProblemTypeName" Key="ProblemTypeName" Width="100px">
                                <Header Caption="类型">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProblemLevelName" Key="ProblemLevelName" Width="100px">
                                <Header Caption="级别">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SubmitDate" DataType="System.DateTime" Format="yyyy-MM-dd HH:mm" Key="SubmitDate" Width="120px">
                                <Header Caption="提交时间">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ToFactoryName" Key="ToFactoryName" Width="110px">
                                <Header Caption="要求处理部门">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ToFullName" Key="ToFullName" Width="100px">
                                <Header Caption="要求处理人">
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="PlannedDisposeDate" DataType="System.DateTime" Format="yyyy-MM-dd" Key="PlannedDisposeDate" Width="110px">
                                <Header Caption="要求处理时间">
                                    <RowLayoutColumnInfo OriginX="7" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="DisposeFullName" Key="DisposeFullName" Width="100px">
                                <Header Caption="实际处理人">
                                    <RowLayoutColumnInfo OriginX="8" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="DisposeDate" DataType="System.DateTime" Format="yyyy-MM-dd" Key="DisposeDate" Width="110px">
                                <Header Caption="实际处理时间">
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="StatusDisp" Key="StatusDisp" Width="70px">
                                <Header Caption="状态">
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ID" Hidden="True" Key="ID">
                                <Header Caption="ID">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProblemDetails" Hidden="True" Key="ProblemDetails">
                                <Header Caption="ProblemDetails">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProblemResult" Hidden="True" Key="ProblemResult">
                                <Header Caption="ProblemResult">
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProblemTypeID" Hidden="True" Key="ProblemTypeID">
                                <Header Caption="ProblemTypeID">
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProblemLevelID" Hidden="True" Key="ProblemLevelID">
                                <Header Caption="ProblemLevelID">
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ToFactoryID" Hidden="True" Key="ToFactoryID">
                                <Header Caption="ToFactoryID">
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ToEmployeeID" Hidden="True" Key="ToEmployeeID">
                                <Header Caption="ToEmployeeID">
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Status" Hidden="True" Key="Status">
                                <Header Caption="Status">
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="specid" Hidden="True" Key="specid">
                                <Header Caption="specid">
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="containerid" Hidden="True" Key="containerid">
                                <Header Caption="containerid">
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px" AllowRowNumberingDefault="Continuous">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px"
                        Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                        Text="第  页  共  页"></asp:Label>
                            <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                            <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                            <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                            <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                                OnClick="btnGo_Click" />
                            <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom" style=" display:none">
                        <div class="divLabel">作业令号：</div>
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <input type="button" value="......." class="searchButton" onclick="openContainerInfo()" />
                        <asp:TextBox ID="txtDispContainerID" runat="server" class="stdTextBox" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtDispWorkflowID" runat="server" class="stdTextBox" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">图号：</div>
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispProductID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdBottom" colspan="2">
                        <div class="divLabel">名称：</div>
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">数量：</div>
                        <asp:TextBox ID="txtDispQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtChildCount" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">计划开始日期：</div>
                        <asp:TextBox ID="txtDispPlannedStartDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">计划完成日期：</div>
                        <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td class="tdBottom" colspan="7" align="left" nowrap="nowrap">
                        <div class="divLabel">工序：</div>
                        <asp:DropDownList ID="ddlSpec" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="tdBottom" align="left" nowrap="nowrap" colspan="9">
                        <div class="divLabel">标题：</div>
                        <asp:TextBox ID="txtProblemTitle" runat="server" class="stdTextBox" Width="600px"></asp:TextBox>
                        <asp:TextBox ID="txtID" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtStatus" runat="server" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div class="divLabel">问题类型：</div>
                        <asp:DropDownList ID="ddlProblemType" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;">
                        </asp:DropDownList>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div class="divLabel">问题级别：</div>
                        <asp:DropDownList ID="ddlProblemLevel" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;">
                        </asp:DropDownList>
                    </td>
                    <td class="tdBottom" colspan="5" align="left" nowrap="nowrap">
                        <div class="divLabel">要求处理时间：</div>
                        <input id="txtPlannedDisposeDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" readonly="readonly" />
                    </td>
                </tr>
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div class="divLabel">要求处理部门：</div>
                        <asp:DropDownList ID="ddlToFactory" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;" AutoPostBack="True" OnSelectedIndexChanged="ddlToFactory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                        <div class="divLabel">要求处理人：</div>
                        <asp:DropDownList ID="ddlToEmployee" runat="server" Width="155px" Height="28px"
                            Style="font-size: 16px;">
                        </asp:DropDownList>
                    </td>
                    <td class="tdBottom" align="left" nowrap="nowrap" colspan="5">
                        <div class="divLabel">实际处理人：</div>
                        <asp:TextBox ID="txtDisposeEmployee" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRight" align="left" nowrap="nowrap" colspan="3">
                        <div class="divLabel">问题描述：</div>
                        <asp:TextBox ID="txtProblemDetails" runat="server" class="stdTextBox" TextMode="MultiLine" Height="100px" Width="430px"></asp:TextBox>
                    </td>
                    <td class="tdNoBorder" colspan="5" align="left" nowrap="nowrap">
                        <div class="divLabel">处理结果：</div>
                        <asp:TextBox ID="txtProblemResult" runat="server" class="stdTextBox" ReadOnly="true" TextMode="MultiLine" Height="100px" Width="430px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;" colspan="2">
                        <asp:Button ID="btnNew" runat="server" Text="新建"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnNew_Click" Style="height: 26px" />&nbsp;&nbsp;
                    <asp:Button ID="btnSave" runat="server" Text="保存"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" Style="height: 26px" />&nbsp;&nbsp;
                    <asp:Button ID="btnClose" runat="server" Text="关闭" OnClientClick="return IsClose()"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnClose_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnDelete" runat="server" Text="删除" OnClientClick="return IsDel()"
                        CssClass="searchButton" EnableTheming="True" OnClick="btnDelete_Click" />&nbsp;

                    </td>


                </tr>
            </table>
        </div>
        <div style="display: none">
            <asp:Button ID="Button1" runat="server" Text="关闭" CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />

            <input type="text" id="ipContainer" runat="server" />
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
