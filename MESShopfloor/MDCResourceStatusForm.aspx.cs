﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Configuration;
using System.Xml;
using System.Web.UI;

public partial class MDCResourceStatusForm : Page, INormalReport //ShopfloorPage//
{
    const string QueryWhere = "MDCResourceStatusForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESSysIntegrationBusiness integration = new uMESSysIntegrationBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：读取MDC设备状态";
        master.strTitle = "读取MDC设备状态";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        //WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            //ClearMessage_PageLoad();
            GetFactory();
        }
    }

    #region 获取Factory
    protected void GetFactory()
    {
        DataTable DT = common.GetFactoryNames();

        ddlFactory.DataTextField = "FactoryName";
        ddlFactory.DataValueField = "FactoryID";
        ddlFactory.DataSource = DT;
        ddlFactory.DataBind();

        ddlFactory.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        //
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strResourceNo = txtResourceName.Text.Trim();

        string strFactoryName = string.Empty;
        try
        {
            strFactoryName = ddlFactory.SelectedItem.Text;
        }
        catch{ }

        string strTeamName = string.Empty;
        try
        {
            strTeamName = ddlTeam.SelectedItem.Text;
        }
        catch { }

        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ResourceNo", strResourceNo);
        result.Add("FactoryName", strFactoryName);
        result.Add("TeamName", strTeamName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        //ClearMessage();

        string strCurrentPage = txtCurrentPage.Text;
        if (strCurrentPage == string.Empty)
        {
            strCurrentPage = "1";
        }

        uMESPagingDataDTO result = integration.GetSourceData(query, Convert.ToInt32(strCurrentPage), 9);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;
    }

    public void ResetQuery()
    {
        //ClearMessage();

        Session[QueryWhere] = "";
        txtResourceName.Text = string.Empty;
        ddlFactory.SelectedValue = string.Empty;
        ddlTeam.SelectedValue = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 获取班组列表
    protected DataTable GetTeam(string strFactoryID)
    {
        return common.GetTeam(strFactoryID);
    }
    #endregion

    protected void ddlFactory_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ClearMessage();

        try
        {
            string strFactoryID = ddlFactory.SelectedValue;
            DataTable dtTeam = GetTeam(strFactoryID);

            ddlTeam.DataTextField = "TeamName";
            ddlTeam.DataValueField = "TeamID";
            ddlTeam.DataSource = dtTeam;
            ddlTeam.DataBind();

            ddlTeam.Items.Insert(0, "");
        }
        catch(Exception ex)
        {
            //DisplayMessage(ex.Message, false);
        }
    }

    #region 附件上传
    protected void UploadFile(out string strFilePath)
    {
        string strPath = ConfigurationManager.AppSettings["UploadFilePath"];
        string strFileName = fileUpload.FileName;

        if (strFileName != string.Empty)
        {
            fileUpload.PostedFile.SaveAs(strPath + fileUpload.FileName);
            strFilePath = strPath + fileUpload.FileName;
        }
        else
        {
            strFilePath = string.Empty;
        }
    }
    #endregion

    protected void btnImport_Click(object sender, EventArgs e)
    {
        //ClearMessage();

        try
        {
            string strFilePath = string.Empty;
            UploadFile(out strFilePath);

            XmlDocument doc = XMLParse.loadDocument(strFilePath);

            //将设备状态信息缓存到DataTable中
            XmlNode docNode = XMLParse.getNode(doc, "/ReqMsg/document");
            DataTable dtResourceStatus = GetResourceStatus(docNode);

            integration.AddMDCResourceStatus(dtResourceStatus);

            QueryData(GetQuery());

            //DisplayMessage("导入成功", true);
        }
        catch(Exception ex)
        {
            //DisplayMessage(ex.Message, false);
        }
    }

    #region 转换设备状态列表信息到DataTable
    /// <summary>
    /// 转换设备状态列表信息到DataTable
    /// </summary>
    /// <param name="rsNode"></param>
    /// <returns></returns>
    protected DataTable GetResourceStatus(XmlNode rsNode)
    {
        DataTable DT = new DataTable();
        DT.Columns.Add("ResourceNo");
        DT.Columns.Add("FactoryName");
        DT.Columns.Add("TeamName");
        DT.Columns.Add("ResourceStatus");
        DT.Columns.Add("CollectDate");

        foreach (XmlNode node in rsNode.ChildNodes)
        {
            DataRow row = DT.NewRow();

            row["ResourceNo"] = node.SelectSingleNode("ResourceNo").InnerText.Trim();        //设备编号
            row["FactoryName"] = node.SelectSingleNode("FactoryName").InnerText.Trim();      //车间
            row["TeamName"] = node.SelectSingleNode("TeamName").InnerText.Trim();            //班组
            row["ResourceStatus"] = node.SelectSingleNode("ResourceStatus").InnerText.Trim();//设备状态
            row["CollectDate"] = node.SelectSingleNode("CollectDate").InnerText.Trim();      //采集时间

            DT.Rows.Add(row);
        }

        return DT;
    }
    #endregion
}