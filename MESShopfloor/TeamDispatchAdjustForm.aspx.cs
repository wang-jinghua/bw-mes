﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Web.UI;

public partial class TeamDispatchAdjustForm : ShopfloorPage
{
    const string QueryWhere = "TeamDispatchAdjustForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();
    uMESContainerBusiness containerBal = new uMESContainerBusiness();
    int m_PageSize = 9;
    string businessName = "业务撤销", parentName = "dispatchinfo";
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：班组派工-任务调整";
        master.strTitle = "班组派工-任务调整";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        //normalCntrl.LtnFirst = lbtnFirst;
        //normalCntrl.LtnLast = lbtnLast;
        //normalCntrl.LtnNext = lbtnNext;
        //normalCntrl.LtnPrev = lbtnPrev;
        //normalCntrl.BtnReset = btnReSet;
        //normalCntrl.BtnGo = btnGo;
        //normalCntrl.BtnSearch = btnSearch;
        //normalCntrl.LabPages = lLabel1;
        //normalCntrl.TxtPage = txtPage;
        //normalCntrl.TxtTotalPage = txtTotalPage;
        //normalCntrl.TxtCurrentPage = txtCurrentPage;
        //normalCntrl.NormalOperation = this;
        //normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { QueryData(upageTurning.CurrentPageIndex); });
        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
            GetTeam();
        }
    }

    #region 显示班组当前已派工的任务
    protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strTeamID = ddlTeam.SelectedValue;
            DataTable DT = dispatch.GetTeamDispatchInfo(strTeamID);

            wgDispatchList.DataSource = DT;
            wgDispatchList.DataBind();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    
    protected void wgDispatchList_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < wgDispatchList.Rows.Count; i++)
        {
            string strSpecName = wgDispatchList.Rows[i].Cells.FromKey("SpecName").Text;
            wgDispatchList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }
    #endregion

    #region 获取班组列表
    protected void GetTeam()
    {

        DataTable DT = containerBal.GetTableInfo("Team","","");

        ddlTeam.DataTextField = "TeamName";
        ddlTeam.DataValueField = "TeamID";
        ddlTeam.DataSource = DT;
        ddlTeam.DataBind();

        ddlTeam.Items.Insert(0, "");
    }
    #endregion

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        hdScanCon.Value = string.Empty;
        upageTurning.TotalRowCount = 0;
        ItemGrid.Rows.Clear();
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("Status", "0");
        result.Add("DispatchType", "0");
        result.Add("DispatchToType", "0");

        if (!string.IsNullOrEmpty(hdScanCon.Value))
        {
            result.Add("ScanContainerName", hdScanCon.Value);
        }
        else
        {
            result.Add("ProcessNo", strProcessNo);
            result.Add("ContainerName", strContainerName);
            result.Add("ProductName", strProductName);
            result.Add("SpecName", strSpecName);
            result.Add("StartDate", strStartDate);
            result.Add("EndDate", strEndDate);
        }

        return result;
    }

    public void QueryData(int intIndex)
    {
        ClearMessage();

        uMESPagingDataDTO result = dispatch.GetSourceData(GetQuery(), intIndex, m_PageSize);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        //给分页控件赋值，用于分页控件信息显示
        this.upageTurning.TotalRowCount = int.Parse(result.RowCount);
        this.upageTurning.RowCountByPage = m_PageSize;
        ClearDispData();
    }

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Text;
            ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();


    }
    #endregion

 
    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if(e.Row.Cells.FromKey("ProcessNo").Value!=null)
            { 
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Text;
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Text;
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = e.Row.Cells.FromKey("ContainerName").Text;
            txtDispContainerName.Text = strContainerName;
            string strContainerID = e.Row.Cells.FromKey("ContainerID").Text;
            txtDispContainerID.Text = strContainerID;

            string strProductName = e.Row.Cells.FromKey("ProductName").Text;
            txtDispProductName.Text = strProductName;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Text;
                txtDispDescription.Text = strDescription;
            }

            string strQty = e.Row.Cells.FromKey("Qty").Text;
            txtDispQty.Text = strQty;

            txtDispPlannedStartDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("ContainerStartDate").Value != null)
            {
                string strPlannedStartDate = e.Row.Cells.FromKey("ContainerStartDate").Text;
                strPlannedStartDate = Convert.ToDateTime(strPlannedStartDate).ToString("yyyy-MM-dd");
                txtDispPlannedStartDate.Text = strPlannedStartDate;
            }

            txtDispPlannedCompletionDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("ContainerCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("ContainerCompletionDate").Text;
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
            }

            string strID = e.Row.Cells.FromKey("ID").Text;
            txtDispID.Text = strID;

            string strSpecNameDisp = e.Row.Cells.FromKey("SpecNameDisp").Text;
            txtDispSpecName.Text = strSpecNameDisp;

            string strTeamID = e.Row.Cells.FromKey("TeamID").Text;
            if(!string.IsNullOrWhiteSpace(strTeamID))//add:Wangjh 0928
                ddlTeam.SelectedValue = strTeamID;

            txtPlannedCompletionDate.Value = string.Empty;
            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Text;
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                txtPlannedCompletionDate.Value = strPlannedCompletionDate;
            }

            wgDispatchList.Clear();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #endregion

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtDispID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择要修改的派工记录", false);
                return;
            }

            if (CheckData() == false)
            {
                return;
            }

            string strTeamID = ddlTeam.SelectedValue;
            string strPlannedCompletionDate = txtPlannedCompletionDate.Value;
            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("PlannedCompletionDate", strPlannedCompletionDate);
            para.Add("TeamID", strTeamID);

            string strContainerID = txtDispContainerID.Text;
            DataTable dtProductNo = common.GetProductNo(strContainerID);
            DataTable dtEmployee = new DataTable();
            dispatch.UpdateDispatchInfo(strID, para, dtProductNo, dtEmployee);

            //记录日志
           var ml = new MESAuditLog();
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            ml.ContainerName = txtDispContainerName.Text; ml.ContainerID = strContainerID;
            ml.ParentID = strID; ml.ParentName = parentName;
            ml.CreateEmployeeID = userInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = 1;
            ml.Description = "班组派工调整:工序" + txtDispSpecName.Text+",调整班组为:"+ddlTeam.SelectedItem.Text;
            common.SaveMESAuditLog(ml);
            //

            QueryData(1);

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;

        txtDispID.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        ddlTeam.SelectedValue = string.Empty;
        txtPlannedCompletionDate.Value = string.Empty;
        wgDispatchList.Clear();
    }

    #region 数据验证
    protected Boolean CheckData()
    {
        Boolean result = true;

        string strTeamID = ddlTeam.SelectedValue;

        if (strTeamID == "")
        {
            DisplayMessage("请选择班组", false);
            ddlTeam.Focus();
            result = false;
        }

        return result;
    }
    #endregion

    #endregion

    #region 删除
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtDispID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择要删除的派工记录", false);
                return;
            }

            dispatch.DeleteDispatchInfo(strID);

            QueryData(1);

            DisplayMessage("删除成功", true);
        }
        catch(Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";
            hdScanCon.Value = string.Empty;
            if (strScan != string.Empty)
            {

                hdScanCon.Value = strScan;
                QueryData(1);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    //查询按钮
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {

            hdScanCon.Value = string.Empty;
            QueryData(upageTurning.CurrentPageIndex);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}