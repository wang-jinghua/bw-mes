﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.Configuration;
using System.Web.UI;
using System.Web;
using System.Text.RegularExpressions;

public partial class RejectCheckDisposeForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "RejectCheckDisposeForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerBusiness containerBal = new uMESContainerBusiness();
    uMESRejectAppBusiness rejectapp = new uMESRejectAppBusiness();
    uMESAssemblyRecordBusiness assembly = new uMESAssemblyRecordBusiness();

    string businessName = "质量管理", parentName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：不合格品审理-检验处理";
        master.strTitle = "检验处理";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;

        ClearContainerDisp();
        ClearRejectAppDisp();
    }

    protected void ClearContainerDisp()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispProductID.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispContainerQty.Text = string.Empty;
        txtChildCount.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;
    }

    protected void ClearRejectAppDisp()
    {
        txtID.Text = string.Empty;
        txtStatus.Text = string.Empty;
        txtDispRejectAppInfoName.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSubmitFullName.Text = string.Empty;
        txtDispSubmitDate.Text = string.Empty;
        txtDispQualityNotes.InnerHtml = string.Empty;
        txtDisposeNotes.Text = string.Empty;
        txtDisposeNotes2.Text = string.Empty;
        txtDisposeNotes3.Text = string.Empty;
        txtDisposeNotes4.Text = string.Empty;
        txtRBQty.Text = string.Empty;
        txtFixQty.Text = string.Empty;
        txtScrapQty.Text = string.Empty;

        wgProductNoList.Clear();
    }
    #endregion

    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("SpecName", strSpecName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);
        result.Add("Status", "40");

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        try
        {
            uMESPagingDataDTO result = rejectapp.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 15);
            this.ItemGrid.DataSource = result.DBTable;
            this.ItemGrid.DataBind();
            this.txtTotalPage.Text = result.PageCount;
            if (result.RowCount == "0")
            {
                this.txtCurrentPage.Text = "0";
            }
            lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
            this.txtPage.Text = this.txtCurrentPage.Text;

            ClearContainerDisp();
            ClearRejectAppDisp();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("Status", "40");

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            DataTable DT = assembly.GetContainerInfo(strContainerName);

            if (DT.Rows.Count > 0)
            {
                string strProcessNo = DT.Rows[0]["ProcessNo"].ToString();
                txtDispProcessNo.Text = strProcessNo;

                string strOprNo = DT.Rows[0]["OprNo"].ToString();
                txtDispOprNo.Text = strOprNo;

                txtDispContainerName.Text = strContainerName;

                string strContainerID = DT.Rows[0]["ContainerID"].ToString();
                txtDispContainerID.Text = strContainerID;

                string strWorkflowID = DT.Rows[0]["WorkflowID"].ToString();
                txtDispWorkflowID.Text = strWorkflowID;

                string strProductName = DT.Rows[0]["ProductName"].ToString();
                txtDispProductName.Text = strProductName;
                string strProductID = DT.Rows[0]["ProductID"].ToString();
                txtDispProductID.Text = strProductID;

                string strDescription = DT.Rows[0]["Description"].ToString();
                txtDispDescription.Text = strDescription;

                string strContainerQty = DT.Rows[0]["Qty"].ToString();
                txtDispContainerQty.Text = strContainerQty;

                string strChildCount = DT.Rows[0]["ChildCount"].ToString();
                txtChildCount.Text = strChildCount;

                string strPlannedStartDate = DT.Rows[0]["PlannedStartDate"].ToString();
                if (strPlannedStartDate != string.Empty)
                {
                    txtDispPlannedStartDate.Text = Convert.ToDateTime(strPlannedStartDate).ToString("yyyy-MM-dd");
                }

                string strPlannedCompletionDate = DT.Rows[0]["PlannedCompletionDate"].ToString();
                if (strPlannedCompletionDate != string.Empty)
                {
                    txtDispPlannedCompletionDate.Text = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                }
            }

            txtID.Text = string.Empty;
            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtID.Text = strID;

            txtStatus.Text = string.Empty;
            string strStatus = e.Row.Cells.FromKey("Status").Value.ToString();
            txtStatus.Text = strStatus;

            txtDispRejectAppInfoName.Text = string.Empty;
            string strRejectAppInfoName = e.Row.Cells.FromKey("RejectAppInfoName").Value.ToString();
            txtDispRejectAppInfoName.Text = strRejectAppInfoName;

            txtDispSpecName.Text = string.Empty;
            string strSpecName = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
            txtDispSpecName.Text = strSpecName;

            txtSpecID.Text = string.Empty;
            string strSpecID = e.Row.Cells.FromKey("SpecID").Value.ToString();
            txtSpecID.Text = strSpecID;

            txtDispQty.Text = string.Empty;
            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            txtDispSubmitFullName.Text = string.Empty;
            string strSubmitFullName = e.Row.Cells.FromKey("SubmitFullName").Value.ToString();
            txtDispSubmitFullName.Text = strSubmitFullName;

            //显示不合格信息描述
            txtDispQualityNotes.InnerHtml = string.Empty;
            if (e.Row.Cells.FromKey("QualityNotes").Value != null)
            {
                string strQualityNotes = "";
                ParseCode(e.Row.Cells.FromKey("QualityNotes").Text, ref strQualityNotes);
                txtDispQualityNotes.InnerHtml = strQualityNotes;
            }

            txtDispSubmitDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("SubmitDate").Value != null)
            {
                string strSubmitDate = e.Row.Cells.FromKey("SubmitDate").Value.ToString();
                txtDispSubmitDate.Text = Convert.ToDateTime(strSubmitDate).ToString("yyyy-MM-dd");
            }

            txtDisposeNotes.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeNotes").Value != null)
            {
                string strDisposeNotes = "";
                if (!string.IsNullOrWhiteSpace(e.Row.Cells.FromKey("DesignEmpNotes").Text))
                {
                    strDisposeNotes = "设计：" + e.Row.Cells.FromKey("DesignEmpNotes").Text + "\r\n\r\n";
                }
                strDisposeNotes += "工艺：" + e.Row.Cells.FromKey("DisposeNotes").Text;
                txtDisposeNotes.Text = strDisposeNotes;
            }

            txtDisposeNotes2.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeNotes2").Value != null)
            {
                string strDisposeNotes2 = e.Row.Cells.FromKey("DisposeNotes2").Value.ToString();
                txtDisposeNotes2.Text = strDisposeNotes2;
            }

            txtDisposeNotes3.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeNotes3").Value != null)
            {
                string strDisposeNotes3 = e.Row.Cells.FromKey("DisposeNotes3").Value.ToString();
                txtDisposeNotes3.Text = strDisposeNotes3;
            }

            txtDisposeNotes4.Text = string.Empty;
            if (e.Row.Cells.FromKey("DisposeNotes4").Value != null)
            {
                string strDisposeNotes4 = e.Row.Cells.FromKey("DisposeNotes4").Value.ToString();
                txtDisposeNotes4.Text = strDisposeNotes4;
            }

            txtRBQty.Text = string.Empty;
            string strRBQty = e.Row.Cells.FromKey("RBQty").Value.ToString();
            txtRBQty.Text = strRBQty;

            txtFixQty.Text = string.Empty;
            string strFixQty = e.Row.Cells.FromKey("FixQty").Value.ToString();
            txtFixQty.Text = strFixQty;

            txtScrapQty.Text = string.Empty;
            string strScrapQty = e.Row.Cells.FromKey("ScrapQty").Value.ToString();
            txtScrapQty.Text = strScrapQty;

            DataTable dtProductNo = rejectapp.GetProductNoByRejectApp(strID);
            wgProductNoList.DataSource = dtProductNo;
            wgProductNoList.DataBind();

            if (!string.IsNullOrWhiteSpace(txtChildCount.Text)&&Convert.ToInt32(txtChildCount.Text) > 0)
            {
                wgProductNoList.Columns.FromKey("ProductNo").Hidden = false;
                wgProductNoList.Columns.FromKey("Qty").Hidden = true;
            }
            else
            {
                wgProductNoList.Columns.FromKey("ProductNo").Hidden = true;
                wgProductNoList.Columns.FromKey("Qty").Hidden = false;
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择不合格品审理单", false);
                return;
            }

            Dictionary<string, object> popupData = new Dictionary<string, object>();
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("ContainerQty", txtDispContainerQty.Text);
            popupData.Add("PlannedStartDate", txtDispPlannedStartDate.Text);
            popupData.Add("PlannedCompletionDate", txtDispPlannedCompletionDate.Text);
            popupData.Add("ContainerID", txtDispContainerID.Text);
            popupData.Add("SpecID", txtSpecID.Text);
            popupData.Add("ID", strID);
            popupData.Add("WorkflowID", txtDispWorkflowID.Text);
            popupData.Add("WorkReportID", "");
            popupData.Add("Qty", "0");
            popupData.Add("ChildCount", txtChildCount.Text);
            popupData.Add("SpecName",txtDispSpecName.Text);
            popupData.Add("PageName", "RejectCheckDispose");

            DataTable DT = new DataTable();
            DT.Columns.Add("ID");
            DT.Columns.Add("ContainerID");
            DT.Columns.Add("ContainerName");
            DT.Columns.Add("ProductNo");
            DT.Columns.Add("Qty");
            DT.Columns.Add("DisposeResult");
            DT.Columns.Add("LossReasonID");
            DT.Columns.Add("LossReasonName");

            for (int i = 0; i < wgProductNoList.Rows.Count; i++)
            {
                string strResult = wgProductNoList.Rows[i].Cells.FromKey("DisposeResult").Value.ToString();
                string strStatus = wgProductNoList.Rows[i].Cells.FromKey("Status").Value.ToString();

                if (strResult == "报废" && strStatus == "0")
                {
                    DataRow row = DT.NewRow();

                    row["ID"] = wgProductNoList.Rows[i].Cells.FromKey("ID").Value.ToString();

                    if (wgProductNoList.Rows[i].Cells.FromKey("ContainerID").Value != null)
                    {
                        row["ContainerID"] = wgProductNoList.Rows[i].Cells.FromKey("ContainerID").Value.ToString();
                    }
                    else
                    {
                        row["ContainerID"] = string.Empty;
                    }

                    if (wgProductNoList.Rows[i].Cells.FromKey("ContainerName").Value != null)
                    {
                        row["ContainerName"] = wgProductNoList.Rows[i].Cells.FromKey("ContainerName").Value.ToString();
                    }
                    else
                    {
                        row["ContainerName"] = string.Empty;
                    }

                    if (wgProductNoList.Rows[i].Cells.FromKey("ProductNo").Value != null)
                    {
                        row["ProductNo"] = wgProductNoList.Rows[i].Cells.FromKey("ProductNo").Value.ToString();
                    }
                    else
                    {
                        row["ProductNo"] = string.Empty;
                    }

                    row["Qty"] = wgProductNoList.Rows[i].Cells.FromKey("Qty").Value.ToString();
                    row["DisposeResult"] = wgProductNoList.Rows[i].Cells.FromKey("DisposeResult").Value.ToString();

                    if (wgProductNoList.Rows[i].Cells.FromKey("LossReasonID").Value != null)
                    {
                        row["LossReasonID"] = wgProductNoList.Rows[i].Cells.FromKey("LossReasonID").Value.ToString();
                    }
                    else
                    {
                        row["LossReasonID"] = string.Empty;
                    }

                    if (wgProductNoList.Rows[i].Cells.FromKey("LossReasonName").Value != null)
                    {
                        row["LossReasonName"] = wgProductNoList.Rows[i].Cells.FromKey("LossReasonName").Value.ToString();
                    }
                    else
                    {
                        row["LossReasonName"] = string.Empty;
                    }

                    DT.Rows.Add(row);
                }
            }

            if (DT.Rows.Count == 0)
            {
                DisplayMessage("已没有需要报废的信息", false);
                return;
            }

            popupData.Add("dtProductNo", DT);

            Session["PopupData"] = popupData;

            string strScript = "<script>openscrapsubmit()</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            if (ItemGrid.Rows[i].Cells.FromKey("SpecName").Value != null)
            {
                string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
                ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Value = common.GetSpecNameWithOutProdName(strSpecName);
            }
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            UltraGridRow activeRow = ItemGrid.DisplayLayout.ActiveRow;
            if (activeRow == null)
            {
                DisplayMessage("请选择要打印的不合格品审理单", false);
                return;
            }
            //int intCount = 0;

            //TemplatedColumn temCell = (TemplatedColumn)ItemGrid.Columns.FromKey("ckSelect");

            //for (int i = 0; i < temCell.CellItems.Count; i++)
            //{
            //    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            //    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

            //    if (ckSelect.Checked == true)
            //    {
            //        intCount++;


            int i = activeRow.Index;
            //获取不合格审理历史记录
            DataTable dt = rejectapp.Getrejectapphistory(ItemGrid.Rows[i].Cells.FromKey("id").Value.ToString());
            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("WorkflowID", ItemGrid.Rows[i].Cells.FromKey("WorkflowID").Value.ToString());
            para.Add("SpecID", ItemGrid.Rows[i].Cells.FromKey("SpecID").Value.ToString());
            DataTable dtWorkTime = rejectapp.GetSpecWorkTimeInfo(para);

            DataTable dtShow = new DataTable();
            for (int intCol = 0; intCol < 16; intCol++)
            {
                dtShow.Columns.Add((intCol + 1).ToString());
            }
            //部门
            string strDepartment = string.Empty;
            strDepartment = containerBal.GetTableInfo("factory" , "factoryid", ItemGrid.Rows[i].Cells.FromKey("ResponsibleDepartment").Text).Rows[0]["factoryname"].ToString();
            dtShow.Rows.Add();
            if (dtWorkTime.Rows.Count > 0)
            {
                for (int intRow = 0; intRow < dtWorkTime.Rows.Count; intRow++)
                {
                    dtShow.Rows[0][intRow] = dtWorkTime.Rows[intRow]["operationname"].ToString();
                    dtShow.Rows[0][intRow + 1] = dtWorkTime.Rows[intRow]["unitworktime"].ToString();
                }
               // strDepartment= dtWorkTime.Rows[dtWorkTime.Rows.Count-1]["factoryname"].ToString();
            }

            //获取送检总数
            para.Add("ContainerID", ItemGrid.Rows[i].Cells.FromKey("containerid").Value.ToString());
            string strCheckQty = rejectapp.GetCheckQty(para);

            if (txtSpecName.Text.Contains("下料"))
            {
                strDepartment = "物流采购中心";
            }

            //工序协作信息
            para.Add("ContainerIDList", "'"+ItemGrid.Rows[i].Cells.FromKey("containerid").Value.ToString()+"'");
            DataTable dtSyne = rejectapp.GetSynerAuditInfo(para);
            if (dtSyne.Rows.Count>0)
            {
                string strFilter = string.Format("containerid='{0}' AND workflowid ='{1}' AND specid='{2}' ", ItemGrid.Rows[i].Cells.FromKey("containerid").Value.ToString(), ItemGrid.Rows[i].Cells.FromKey("WorkflowID").Value.ToString(), ItemGrid.Rows[i].Cells.FromKey("specid").Value.ToString());
                DataRow[] dr = dtSyne.Select(strFilter);
                if (dr.Length>0)
                {
                    strDepartment = "生产制造中心";
                }
            }
            
            #region 绘制不合格品审理单

            //创建PDF文档
            Document document = new Document(PageSize.A4.Rotate(), 40, 40, 70, 0);
            string strPath = Server.MapPath(Request.ApplicationPath) + ConfigurationManager.AppSettings["PDFFilePath"];
            string strFileName = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff") + ".pdf";
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(strPath + strFileName, FileMode.Create));
            document.Open();

            BaseFont baseFont = BaseFont.CreateFont("C:\\WINDOWS\\FONTS\\simfang.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            document.NewPage();

            PdfContentByte cb = writer.DirectContent;
            iTextSharp.text.Font font = new iTextSharp.text.Font(baseFont, 28, 1);
            Phrase txt = new Phrase("不 合 格 品 审 理 单", font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 280, 540, 0);

            cb = writer.DirectContent;
            font = new iTextSharp.text.Font(baseFont, 12);
            txt = new Phrase("部门："+strDepartment, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 50, 530, 0);

            cb = writer.DirectContent;
            txt = new Phrase("编号：" + ItemGrid.Rows[i].Cells.FromKey("RejectAppInfoName").Value.ToString(), font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 650, 530, 0);

            #region 主信息
            PdfPTable table = new PdfPTable(new float[] { 63, 89, 63, 89, 70, 89, 63, 89, 63, 82 });
            table.TotalWidth = 760;
            table.LockedWidth = true;

            PdfPCell cell;
            //第一行
            cell = new PdfPCell(new Phrase("工作令号", font));
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strProcessNo = string.Empty;
            if (ItemGrid.Rows[i].Cells.FromKey("ProcessNo").Value != null)
            {
                strProcessNo = ItemGrid.Rows[i].Cells.FromKey("ProcessNo").Value.ToString();
            }
            cell = new PdfPCell(new Phrase(strProcessNo, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("零件图号", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strProductName = string.Empty;
            if (ItemGrid.Rows[i].Cells.FromKey("ProductName").Value != null)
            {
                strProductName = ItemGrid.Rows[i].Cells.FromKey("ProductName").Value.ToString();
            }
            cell = new PdfPCell(new Phrase(strProductName, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("送检数", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(strCheckQty, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("器材名称", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("单价", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            //第二行
            cell = new PdfPCell(new Phrase("工件名称", font));
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strDescription = string.Empty;
            if (ItemGrid.Rows[i].Cells.FromKey("Description").Value != null)
            {
                strDescription = ItemGrid.Rows[i].Cells.FromKey("Description").Value.ToString();
            }
            cell = new PdfPCell(new Phrase(strDescription, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("整件图号", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("不合格品数", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strQty = ItemGrid.Rows[i].Cells.FromKey("Qty").Value.ToString();
            cell = new PdfPCell(new Phrase(strQty, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("牌号规格", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("合计损失", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            document.Add(table);
            #endregion

            #region 不合格信息
            table = new PdfPTable(new float[] { 20, 443, 60, 32, 60, 50, 95 });
            table.TotalWidth = 760;
            table.LockedWidth = true;

            //第一行
            cell = new PdfPCell(new Phrase("不合格特征", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strQualityNotes = txtDispQualityNotes.InnerText;//ItemGrid.Rows[i].Cells.FromKey("QualityNotes").Text;

            string[] strQualityNotess = Regex.Split(strQualityNotes,"<img");
            string httpUrl = @"http://"+ Request.Url.Authority;
            
            var p = new Paragraph();
            foreach (var note in strQualityNotess)
            {
                if (note.Contains("src='"))
                {
                    var imgUrl = note.Replace("src='","");
                    string[] notes2 = Regex.Split(imgUrl, "'>");
                    imgUrl = notes2[0].Trim();
                    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(httpUrl+ imgUrl);
                    p.Add(new Chunk(img, 0, 0,true));
                    if (notes2.Length > 1)
                    {
                        p.Add(new Phrase(notes2[1], font));

                    }
                }
                else
                {
                   p.Add(new Phrase(note, font));
                }
            }

            cell = new PdfPCell(p);
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("重量、尺寸、数量", font));
            cell.Colspan = 3;
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.Colspan = 2;
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("本工序前（包括本工序）工时", font));
            cell.Rowspan = 5;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("工种", font));
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("每件工时", font));
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("工种", font));
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("每件工时", font));
            cell.MinimumHeight = 25;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strColName = string.Empty;
            for (int j = 0; j < 4; j++)
            {
                strColName = (j + 1).ToString();
                if (j > 1)
                {
                    strColName = (j + 7).ToString();
                }


                cell = new PdfPCell(new Phrase("" + dtShow.Rows[0][strColName].ToString(), font));
                cell.MinimumHeight = 25;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                table.AddCell(cell);
            }

            cell = new PdfPCell(new Phrase("不合格原因", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("□原材料问题 □操作问题 □图纸、工艺问题 □工装、设备问题 □外协问题 □设计问题 □其他（必须填写具体）", font));
            cell.Rowspan = 3; //■
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(cell);

            for (int j = 0; j < 12; j++)
            {
                strColName = (j + 3).ToString();
                if (j > 5)
                {
                    strColName = (j + 5).ToString();
                }

                cell = new PdfPCell(new Phrase("" + dtShow.Rows[0][strColName].ToString(), font));
                cell.MinimumHeight = 25;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                table.AddCell(cell);
            }

            document.Add(table);

            string strSubmitFullName = ItemGrid.Rows[i].Cells.FromKey("SubmitFullName").Value.ToString();
            cb = writer.DirectContent;
            txt = new Phrase("发现人员/检验人员：" + strSubmitFullName, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 100, 405, 0);

            string strSubmitDate = ItemGrid.Rows[i].Cells.FromKey("SubmitDate").Value.ToString();
            strSubmitDate = Convert.ToDateTime(strSubmitDate).ToString("yyyy.MM.dd");
            cb = writer.DirectContent;
            txt = new Phrase("日期：" + strSubmitDate, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 350, 405, 0);

            //cb = writer.DirectContent;
            //txt = new Phrase("签名：", font);
            //ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 200, 330, 0);

            //cb = writer.DirectContent;
            //txt = new Phrase("日期：", font);
            //ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 350, 330, 0);

            #endregion

            #region 质量信息
            table = new PdfPTable(new float[] { 20, 228, 20, 195, 92, 205 });
            table.TotalWidth = 760;
            table.LockedWidth = true;

            cell = new PdfPCell(new Phrase("设计/工艺意见", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            //update:Wangjh 增加设计人员意见显示
            string strDisposeNotes = "";
            if (!string.IsNullOrWhiteSpace(ItemGrid.Rows[i].Cells.FromKey("DesignEmpNotes").Text))
            {
                strDisposeNotes = "设计：" + ItemGrid.Rows[i].Cells.FromKey("DesignEmpNotes").Text + "\r\n\r\n";
            }
            strDisposeNotes += "工艺：" + ItemGrid.Rows[i].Cells.FromKey("DisposeNotes").Text;
            //
            cell = new PdfPCell(new Phrase(strDisposeNotes, font));

            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("质量部门意见", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strDisposeNotes2 = ItemGrid.Rows[i].Cells.FromKey("DisposeNotes2").Value.ToString();
            cell = new PdfPCell(new Phrase(strDisposeNotes2, font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("返工返修工时", font));
            cell.MinimumHeight = 35;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("损失总金额", font));
            cell.MinimumHeight = 35;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("会计员：              日期：", font));
            cell.Colspan = 2;
            cell.MinimumHeight = 35;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            document.Add(table);

            string strName1 = string.Empty;
            string strDate1 = string.Empty;

            string strName2 = string.Empty;
            string strDate2 = string.Empty;

            string strAssessmentLevel = string.Empty;

            if (dt.Rows.Count > 0)
            {
                for (int h = 0; h < dt.Rows.Count; h++)
                {
                    if (dt.Rows[h]["oprtype"].ToString() == "20")
                    {
                        strName1 = dt.Rows[h]["fullname"].ToString();
                        strDate1 = Convert.ToDateTime(dt.Rows[h]["oprdate"].ToString()).ToString("yyyy.MM.dd");
                    }

                    if (dt.Rows[h]["oprtype"].ToString() == "40")
                    {
                        strAssessmentLevel = dt.Rows[h]["assessmentlevel"].ToString();
                        strName2 = dt.Rows[h]["fullname"].ToString();
                        strDate2 = Convert.ToDateTime(dt.Rows[h]["oprdate"].ToString()).ToString("yyyy.MM.dd");
                    }
                }
            }

            cb = writer.DirectContent;
            txt = new Phrase("签名：" + strName1, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 110, 230, 0);

            cb = writer.DirectContent;
            txt = new Phrase("日期：" + strDate1, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 190, 230, 0);


            cb = writer.DirectContent;
            txt = new Phrase("签名：" + strName2, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 330, 230, 0);

            cb = writer.DirectContent;
            txt = new Phrase("日期：" + strDate2, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 405, 230, 0);
            #endregion

            #region 处理结论
            table = new PdfPTable(new float[] { 20, 129, 20, 129, 20, 145, 30, 132, 135 });
            table.TotalWidth = 760;
            table.LockedWidth = true;


            string strLevel1 = string.Empty;
            string strLevel2 = string.Empty;
            string strLevel3 = string.Empty;

            if (strAssessmentLevel=="1")
            {
                strLevel1 = ItemGrid.Rows[i].Cells.FromKey("DisposeNotes3").Value.ToString();              
            }

            if (strAssessmentLevel == "2")
            {
                strLevel2 = ItemGrid.Rows[i].Cells.FromKey("DisposeNotes3").Value.ToString();
            }

            if (strAssessmentLevel == "3")
            {
                strLevel3 = ItemGrid.Rows[i].Cells.FromKey("DisposeNotes3").Value.ToString();
            }

            cell = new PdfPCell(new Phrase("三级审理意见", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(strLevel3, font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("二级审理意见", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(strLevel2, font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("一级审理意见", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase(strLevel1, font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_TOP;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("处理结论", font));
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("让步使用", font));
            cell.MinimumHeight = 35;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strRBQty = ItemGrid.Rows[i].Cells.FromKey("RBQty").Value.ToString();
            cell = new PdfPCell(new Phrase(strRBQty, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("返工返修", font));
            cell.MinimumHeight = 35;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strFixQty = ItemGrid.Rows[i].Cells.FromKey("FixQty").Value.ToString();
            cell = new PdfPCell(new Phrase(strFixQty, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("报废", font));
            cell.MinimumHeight = 35;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            string strScrapQty = ItemGrid.Rows[i].Cells.FromKey("ScrapQty").Value.ToString();
            cell = new PdfPCell(new Phrase(strScrapQty, font));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            document.Add(table);

            string strName21 = string.Empty;
            string strDate21 = string.Empty;

            string strName22 = string.Empty;
            string strDate22 = string.Empty;

            string strName23 = string.Empty;
            string strDate23 = string.Empty;

            if (strAssessmentLevel == "1")
            {
                strName21 = strName2;
                strDate21 = strDate2;
            }

            if (strAssessmentLevel == "2")
            {
                strName22 = strName2;
                strDate22 = strDate2;
            }

            if (strAssessmentLevel == "3")
            {
                strName23 = strName2;
                strDate23 = strDate2;
            }

            cb = writer.DirectContent;
            txt = new Phrase("审理人：" + strName23, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 60, 135, 0);

            cb = writer.DirectContent;
            txt = new Phrase("日期：" + strDate23, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 90, 120, 0);


            cb = writer.DirectContent;
            txt = new Phrase("审理人：" + strName22, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 210, 135, 0);

            cb = writer.DirectContent;
            txt = new Phrase("日期：" + strDate22, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 240, 120, 0);

            cb = writer.DirectContent;
            txt = new Phrase("审理人：" + strName21, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 360, 135, 0);

            cb = writer.DirectContent;
            txt = new Phrase("日期：" + strDate21, font);
            ColumnText.ShowTextAligned(cb, Element.ALIGN_LEFT, txt, 400, 120, 0);

            #endregion

            #region 军代表意见
            table = new PdfPTable(new float[] { 76, 387, 30, 267 });
            table.TotalWidth = 760;
            table.LockedWidth = true;

            cell = new PdfPCell(new Phrase("军代表意见", font));
            cell.MinimumHeight = 70;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.MinimumHeight = 70;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("备注", font));
            cell.MinimumHeight = 70;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            cell = new PdfPCell(new Phrase("", font));
            cell.MinimumHeight = 70;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(cell);

            document.Add(table);
            #endregion

            document.Close();
            writer.Close();

            string strScript = "<script>window.open('ExportFile/" + strFileName + "');</script>";
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, strScript);

            #endregion

            #region 记录日志
            var ml = new MESAuditLog();
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            ml.ContainerName = txtDispContainerName.Text; ml.ContainerID = txtDispContainerID.Text;
            ml.ParentID = ""; ml.ParentName = parentName;
            ml.CreateEmployeeID = userInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = -1;
            ml.Description = "不和合格审理:打印";
            common.SaveMESAuditLog(ml);
            #endregion


            //    }
            //}

            //if (intCount == 0)
            //{
            //    DisplayMessage("请选择要打印的不合格品审理单", false);
            //    return;
            //}
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }


    #region 特殊字符转换

    public void ParseCode(String strPreviewCode, ref string strPreviewCodeDis)
    {
        try
        {
            string webRootDir = HttpRuntime.AppDomainAppPath;
            string webRootUrl = "~";

            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath = webRootDir + ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    strPreviewCodeDis += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir + ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (Session["dtImage"] == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            Session["dtImage"] = dtImage;
                        }
                        else
                        {
                            dtImage = (DataTable)Session["dtImage"];
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        Session["dtImage"] = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        strPreviewCodeDis += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                strPreviewCodeDis += strPreviewCode;
            }
        }
        catch (Exception myError)
        {

        }

    }

    public String GetCode(string strNotes)
    {
        String strHtmlCode, strHtml, strHtmlTemp;
        strHtml = strNotes;
        try
        {
            DataTable dtImage = new DataTable();
            int i, intStartFlag, intEndFlag;
            dtImage = (DataTable)Session["dtImage"];

            // strHtml = txtQualityNotes.Text.Trim();
            strHtmlCode = strHtml;

            intStartFlag = strHtmlCode.IndexOf("<img");
            if (intStartFlag == -1)
            {
                intStartFlag = strHtmlCode.IndexOf("<IMG");
            }
            if (intStartFlag > -1)
            {
                intEndFlag = strHtmlCode.IndexOf(">", intStartFlag);
                ;
            }
            else
            {
                intEndFlag = -1;
            }

            if (dtImage != null)
            {
                if (dtImage.Rows.Count > 0)
                {
                    while (intStartFlag > -1)
                    {
                        strHtmlTemp = strHtmlCode.Substring(intStartFlag, intEndFlag - intStartFlag + 1);
                        for (i = 0; i <= dtImage.Rows.Count - 1; i++)
                        {
                            if (strHtmlTemp.IndexOf(@"/" + dtImage.Rows[i][0]) > -1)
                            {
                                strHtmlCode = strHtmlCode.Replace(strHtmlTemp, (String)dtImage.Rows[i][1]);
                                break;
                            }

                        }

                        intStartFlag = strHtmlCode.IndexOf("<img", intStartFlag + 1);
                        if (intStartFlag == -1)
                        {
                            intStartFlag = strHtmlCode.IndexOf("<IMG", intStartFlag + 1);
                        }
                        if (intStartFlag > -1)
                        {
                            intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                            ;
                        }
                        else
                        {
                            intEndFlag = -1;
                        }
                        //intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                    }

                }

            }

            return strHtmlCode;
        }
        catch (Exception myError)
        {
            DisplayMessage(myError.Message, false);
            return "";
        }
    }
    #endregion
}