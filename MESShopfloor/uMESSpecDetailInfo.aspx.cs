﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class uMESSpecDetailInfo : System.Web.UI.Page
{

    uMESContainerPlatoonBusiness bll = new uMESContainerPlatoonBusiness();
    uMESCommonBusiness common = new uMESCommonBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Dictionary<string, string> para = new Dictionary<string, string>();
            if (Request.QueryString["DispatchinfoID"] != null)
            {
                para.Add("DispatchinfoID", Request.QueryString["DispatchinfoID"]);
                BindDate(para);
            }
        }
    }

    protected void BindDate(Dictionary<string, string> para)
    {
        DataTable dtRGGS = (DataTable)Session["PlatoonRGGS"];

        DataTable dt = bll.GetDispatchSpecInfo(para);
        dt.Columns.Add("SpecNameDisp");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string strSpecName = dt.Rows[i]["specname"].ToString();
            dt.Rows[i]["SpecNameDisp"] = common.GetSpecNameWithOutProdName(strSpecName);

            string strRGID = dt.Rows[i]["ResourceGroupID"].ToString();
            string strSpecGS = dt.Rows[i]["TotalGS"].ToString();

            DataRow[] rows = dtRGGS.Select(string.Format("ResourceGroupID = '{0}'", strRGID));

            if (rows.Length == 1)
            {
                string strStdGS = rows[0]["StdGS"].ToString();
                string strDayGS = rows[0]["DayGS"].ToString();

                dt.Rows[i]["productioncapacity"] = Convert.ToDouble(Convert.ToInt32(strStdGS) / 60).ToString("0.00");
                dt.Rows[i]["worktime2"] = Convert.ToDouble(Convert.ToInt32(strDayGS) / 60).ToString("0.00");
            }
            else
            {
                dt.Rows[i]["productioncapacity"] = "0";
                dt.Rows[i]["worktime2"] = "0";
            }

            dt.Rows[i]["worktime1"] = Convert.ToDouble(Convert.ToInt32(strSpecGS) / 60).ToString("0.00");
        }

        wgDispatch.DataSource = dt;
        wgDispatch.DataBind();

        txtStartDate.Value = Convert.ToDateTime(wgDispatch.Rows[0].Cells.FromKey("PlannedStartDate").Text).ToString("yyyy-MM-dd HH:mm");
        txtEndDate.Value = Convert.ToDateTime(wgDispatch.Rows[0].Cells.FromKey("PlannedCompletionDate").Text).ToString("yyyy-MM-dd HH:mm");

        //string strState = dt.Rows[0]["State"].ToString();
        //if (strState == "1" || strState == "10")
        //{
        //    tdMessage.InnerText = "该批次已审核通过或已下发，不允许调整任务时间";
        //    btnSave.Enabled = false;
        //}
        string strStatus = dt.Rows[0]["Status"].ToString();
        if (Convert.ToInt32(strStatus) >= 20)
        {
            tdMessage.InnerText = "该任务已开始加工，不允许调整任务时间";
            btnSave.Enabled = false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string strStartDate = txtStartDate.Value.Trim();
            string strEndDate = txtEndDate.Value.Trim();


            if (strStartDate == string.Empty)
            {
                tdMessage.InnerText = "请选择计划开始时间";
                return;
            }

            if (strEndDate == string.Empty)
            {
                tdMessage.InnerText = "请选择计划完成时间";
                return;
            }

            DateTime dtStartDate = DateTime.MaxValue;
            try
            {
                dtStartDate = Convert.ToDateTime(strStartDate);
            }
            catch
            {
                tdMessage.InnerText = "计划开始时间格式错误";
                return;
            }

            DateTime dtEndDate = DateTime.MinValue;
            try
            {
                dtEndDate = Convert.ToDateTime(strEndDate);
            }
            catch
            {
                tdMessage.InnerText = "计划完成时间格式错误";
                return;
            }

            if (dtStartDate > dtEndDate)
            {
                tdMessage.InnerText = "计划完成时间应晚于计划开始时间";
                return;
            }

            string strID = wgDispatch.Rows[0].Cells.FromKey("ID").Text;

            bll.UpdateDispatchInfo(strID, dtStartDate, dtEndDate);

            tdMessage.InnerText = "更新成功";
        }
        catch (Exception ex)
        {
            tdMessage.InnerText = ex.Message;
        }
    }
}