﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="top.aspx.cs" Inherits="top" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>无标题页</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        body
        {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            font-family: "宋体";
            font-size: 9pt;
            font-weight: bold;
            text-decoration: none;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function exit1() {
            window.top.location = "/MESShopfloor";
        }
        function exit() {
            window.top.location.replace("http://localhost/MESShopfloor/login.aspx");
        }
        function OpenPassword()
        {
            window.showModalDialog("Custom/bwCommonPage/PasswordPopupForm.aspx", "", "dialogWidth:300px; dialogHeight:300px; status=no; center: Yes; resizable: NO;");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td colspan="8" height="70px" style="background-color: #569ffe; background-repeat: no-repeat;
                background-image: url(images/61x8Logo.png); text-align: right; vertical-align: bottom;">
                <div style="padding-bottom: 12px; padding-right: 20px;position:relative;">
                    <div style="text-align:left;color:red;width:500px;font-size:18px;position:absolute;right:-40px;top:-25px">此系统为内部系统，不得存储、传输、处理涉密信息！</div>
                
                    <asp:Label ID="lblRevision" runat="server" Text="" style="font-family: 华文楷体; color:White; font-size: 15px;"></asp:Label>
                    <asp:Label ID="lblEmployee" runat="server" Text="" style="font-family: 华文楷体; color:White; font-size: 15px;"></asp:Label>
                    <asp:Label ID="lblFactory" runat="server" Text="" style="font-family: 华文楷体; color:White; font-size: 15px;"></asp:Label>
                    <span id="time" style="font-family: 微软雅黑; color: White; font-size: 14px;"></span>
                    <asp:LinkButton ID="lbPassword" runat="server" ClientIDMode="Static" OnClientClick="OpenPassword()"
                        style="font-family: 华文楷体; color:White; font-size: 15px;text-decoration:none;" >
                        修改密码
                    </asp:LinkButton>
                    <asp:LinkButton ID="lbExit" runat="server" ClientIDMode="Static" OnClientClick="exit1()" onclick="lbExit_Click"
                        style="font-family: 华文楷体; color:White; font-size: 15px;text-decoration:none;" >
                        退出
                    </asp:LinkButton>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="8" height="20px" style="background-image: url('images/box2_titbg.gif');">
            </td>
        </tr>
    </table>
    </form>
    <script type="text/javascript">
        var count = 1;
        var t = new Date().valueOf();
        showTime();

        function getLocalTime(Sj) {
            //var taa = document.getElementById("time");
            //var t = new Date().getDay();
            //document.getElementById("time").innerHTML = new Date(parseInt(Sj)).toLocaleString() + "  ";  /*+ GetWeek(t);*/
            document.getElementById("time").innerHTML = "                ";
        }
        function showTime() {
            count++;
            if (count >= 300)  //5分钟对一次表
            {
                t = new Date().valueOf();
                count = 0;
                //alert("时间刷新了！");
            }

            t += 1000;
            getLocalTime(t);
            window.setTimeout(showTime, 1000);
        }
        function GetWeek(numberobj) {
            switch (numberobj) {
                case 1: return '周一';
                case 2: return '周二';
                case 3: return '周三';
                case 4: return '周四';
                case 5: return '周五';
                case 6: return '周六';
                case 0: return '周日';
            }
        }


    </script>
</body>
</html>
