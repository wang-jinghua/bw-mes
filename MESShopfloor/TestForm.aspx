﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="TestForm.aspx.cs" Inherits="TestForm" EnableViewState="true" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function openrejectappsubmit() {
            var someValue = window.showModalDialog("RejectAppSubmitPopupForm.aspx", "", "dialogWidth:1200px; dialogHeight:480px; status=no; center: Yes; resizable: NO;");
            if (someValue != "" && someValue != undefined) {
                document.getElementById("<%=btnSearch.ClientID%>").click();
            }
        }

        function IsDel() {
            if (confirm("确定要删除该派工记录吗？")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td class="ScanTD" align="left" nowrap="nowrap">扫描：
                    </td>
                    <td align="left" colspan="10" class="tdBottom">
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRight" align="left" nowrap="nowrap">工作令号：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">批次号：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">图号/名称：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">工序：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtSpecName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">要求完成日期：
                    </td>
                    <td align="left" nowrap="nowrap" class="tdRight">
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div id="ItemDiv" runat="server">
            <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="300px" Width="100%" OnActiveRowChange="ItemGrid_ActiveRowChange" OnDataBound="ItemGrid_DataBound">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                <CellTemplate>
                                    <asp:CheckBox ID="ckSelect" runat="server" />
                                </CellTemplate>
                                <Header Caption=""></Header>
                            </igtbl:TemplatedColumn>
                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                <Header Caption="工作令号">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="150px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="作业令号">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                <Header Caption="批次号">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                <Header Caption="图号">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                <Header Caption="名称">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                <Header Caption="派工数量">
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="SpecNameDisp" AllowGroupBy="No" BaseColumnName="SpecNameDisp" Width="80px">
                                <Header Caption="派工工序">
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="ResourceName" BaseColumnName="ResourceName" Width="150px">
                                <Header Caption="设备/工位">
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="TeamName" BaseColumnName="TeamName" Width="150px" Hidden="True">
                                <Header>
                                    <RowLayoutColumnInfo OriginX="9"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="PlannedCompletionDate" AllowGroupBy="No" BaseColumnName="PlannedCompletionDate" Width="120px" DataType="System.DateTime" Format="yyyy-MM-dd">
                                <Header Caption="班组">
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ID" Key="ID" Hidden="True">
                                <Header Caption="要求完成日期">
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SpecName" Hidden="True" Key="SpecName">
                                <Header Caption="ID">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerQty" Hidden="True" Key="ContainerQty">
                                <Header Caption="SpecName">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerStartDate" Hidden="True" Key="ContainerStartDate">
                                <Header Caption="ContainerQty">
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerCompletionDate" Hidden="True" Key="ContainerCompletionDate">
                                <Header Caption="ContainerStartDate">
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="TeamID" Hidden="True" Key="TeamID">

                                <Header Caption="ContainerCompletionDate">
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                <Header Caption="TeamID">
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Hidden="True" Key="WorkflowID">
                                <Header Caption="ContainerID">
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SpecID" Hidden="True" Key="SpecID">
                                <Header Caption="WorkflowID">
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ParentID" Hidden="True" Key="ParentID">
                                <Header Caption="SpecID">
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                                <Header Caption="ParentID">
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="20" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ResourceID" Hidden="True" Key="ResourceID">
                                <Header Caption="ResourceID">
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="21" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ParentQty" Hidden="True" Key="ParentQty">
                                <Header Caption="ParentQty">
                                    <RowLayoutColumnInfo OriginX="22" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="22" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID">
                                <Header Caption="ProductID">
                                    <RowLayoutColumnInfo OriginX="23" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="23" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: right;">
                        <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                            OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                        Text="第  页  共  页"></asp:Label>
                        <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                        <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                        <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                        <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                            OnClick="btnGo_Click" />
                        <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                            Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;"
                            Visible="False"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">工作令号：
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap" style=" display:none">作业令号：
                    </td>
                    <td align="left" class="tdRightAndBottom" style=" display:none">
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">批次号：
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispContainerID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">图号：
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispProductID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td class="tdRightAndBottom" align="left" nowrap="nowrap">名称：
                    </td>
                    <td align="left" class="tdRightAndBottom" colspan="3">
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="tdRight" align="left" nowrap="nowrap">派工数量：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtDispQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">工序：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtDispSpecName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispWorkflowID" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtDispSpecID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">班组：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtDispTeamName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispTeamID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">设备/工位：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtDispResourceName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispResourceID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td class="tdRight" align="left" nowrap="nowrap">要求完成日期：
                    </td>
                    <td align="left" class="tdRight">
                        <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispID" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtDispParentID" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtParentQty" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtDispatchedQty" runat="server" Visible="False"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <igtbl:UltraWebGrid ID="wgEmployee" runat="server" Height="300px" Width="170px" OnDataBound="wgDispatchList_DataBound">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn Width="30px" AllowGroupBy="No" Key="ckSelect" Hidden="True">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption=""></Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn Key="FullName" Width="100px" BaseColumnName="FullName" AllowGroupBy="No">
                                            <Header Caption="人员">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="EmployeeName" Key="EmployeeName" Hidden="True">
                                            <Header Caption="EmployeeID">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="EmployeeID" Key="EmployeeID" Hidden="True">
                                            <Header Caption="EmployeeName">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="170px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td>
                        <igtbl:UltraWebGrid ID="wgProductNo" runat="server" Height="300px" Width="170px" OnDataBound="wgDispatchList_DataBound">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn Width="30px" AllowGroupBy="No" Key="ckSelect" Hidden="True">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption=""></Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn Key="ProductNo" Width="100px" BaseColumnName="ProductNo" AllowGroupBy="No">
                                            <Header Caption="产品序号">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Hidden="True">
                                            <Header Caption="ContainerName">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerID" Key="ContainerID" Hidden="True">
                                            <Header Caption="ContainerID">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="170px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td style="width: 100%;">
                        <div>
                            <igtbl:UltraWebGrid ID="wgDispatchList" runat="server" Height="300px" Width="100%" OnDataBound="wgDispatchList_DataBound">
                                <Bands>
                                    <igtbl:UltraGridBand>
                                        <Columns>
                                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                                <Header Caption="工作令号">
                                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                </Header>

                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                                <Header Caption="批次号">
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="1" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                                <Header Caption="图号">
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="2" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                                <Header Caption="名称">
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="3" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                                <Header Caption="数量">
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="4" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Key="SpecNameDisp" AllowGroupBy="No" BaseColumnName="SpecNameDisp" Width="80px">
                                                <Header Caption="工序">
                                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                                </Header>

                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="PlannedCompletionDate" Key="PlannedCompletionDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                                <Header Caption="要求完成日期">
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="6" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn BaseColumnName="SpecName" Hidden="True" Key="SpecName">
                                                <Header Caption="SpecName">
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Header>
                                                <Footer>
                                                    <RowLayoutColumnInfo OriginX="7" />
                                                </Footer>
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                        <AddNewRow View="NotSet" Visible="NotSet">
                                        </AddNewRow>
                                    </igtbl:UltraGridBand>
                                </Bands>
                                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                    RowHeightDefault="18px">
                                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="300px" Width="100%">
                                    </FrameStyle>
                                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                    </RowAlternateStyleDefault>
                                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                        StyleMode="QuickPages">
                                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </PagerStyle>
                                    </Pager>
                                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                    </EditCellStyleDefault>
                                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </FooterStyleDefault>
                                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        <Padding Bottom="3px" Top="2px" />
                                        <Padding Top="2px" Bottom="3px"></Padding>
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                        <Padding Left="3px" />
                                    </RowSelectorStyleDefault>
                                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                        <Padding Left="3px" />
                                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                    </RowStyleDefault>
                                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                    </GroupByRowStyleDefault>
                                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                    </SelectedRowStyleDefault>
                                    <GroupByBox Hidden="True">
                                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                        </BoxStyle>
                                    </GroupByBox>
                                    <AddNewBox>
                                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                        </BoxStyle>
                                    </AddNewBox>
                                    <ActivationObject BorderColor="" BorderWidth="">
                                    </ActivationObject>
                                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px" Height="420px" Width="200px">
                                            <Padding Left="2px" />
                                        </FilterDropDownStyle>
                                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                        </FilterHighlightRowStyle>
                                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                            Font-Size="11px">
                                            <Padding Left="2px" />
                                        </FilterOperandDropDownStyle>
                                    </FilterOptionsDefault>
                                </DisplayLayout>
                            </igtbl:UltraWebGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </div>




        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;" colspan="2">
                        <asp:Button ID="btnMaterialAssort" runat="server" Text="提交不合格品审理"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnMaterialAssort_Click" />
                        <asp:Button ID="btnTest" runat="server" Text="测试"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnTest_Click" />
                        <asp:Button ID="btnLoss" runat="server" Text="报废"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnLoss_Click" />
                        <asp:Button ID="Button1" runat="server" Text="测试"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                        <asp:Button ID="Button2" runat="server" Text="测试"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button2_Click" />
                        <asp:Button ID="Button3" runat="server" Text="测试"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button3_Click" />
                        <asp:Button ID="Button4" runat="server" Text="测试"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button4_Click" />
                        <asp:Button ID="Button5" runat="server" Text="测试"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button5_Click" />
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; font-weight: bold;" nowrap="nowrap">状态信息：</td>
                    <td style="text-align: left; width: 100%;">
                        <asp:Label ID="lStatusMessage" runat="server" Width="100%"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
