﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.Web.UI;
using System.Configuration;
using System.Web;

public partial class RejectAppDesignForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "RejectAppDesignForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESRejectAppBusiness rejectapp = new uMESRejectAppBusiness();
    uMESAssemblyRecordBusiness assembly = new uMESAssemblyRecordBusiness();

    string businessName = "质量管理", parentName = "rejectappinfo";
    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：设计员审核";
        master.strTitle = "设计/工艺审核";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            cbDesignEmployee.Checked = true;
            ClearMessage_PageLoad();
            GetDesignEmployee();
        }
    }

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        cbDesignEmployee.Checked = true;
        ClearContainerDisp();
        ClearRejectAppDisp();
    }

    protected void ClearContainerDisp()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispProductID.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispContainerQty.Text = string.Empty;
        txtChildCount.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;
    }

    protected void ClearRejectAppDisp()
    {
        txtID.Text = string.Empty;
        txtStatus.Text = string.Empty;
        txtDispRejectAppInfoName.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSubmitFullName.Text = string.Empty;
        txtDispSubmitDate.Text = string.Empty;
        txtDispQualityNotes.InnerHtml = string.Empty;
        txtDisposeNotes.Text = string.Empty;
    }
    #endregion

    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("SpecName", strSpecName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);
        result.Add("Status", "5");//设计员审核
        //查询指定的工艺/计划
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strOprEmployeeID = userInfo["EmployeeID"];
        if (cbDesignEmployee.Checked==true)
        {
            result.Add("DesignEmpID", strOprEmployeeID);
        }

        Session[QueryWhere] = result;

        return result;
    }

    protected void GetDesignEmployee()
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strFactoryID = userInfo["FactoryID"];
        string strEmployeeID = userInfo["EmployeeID"];
        DataTable DT = rejectapp.GetDesignEmployee(strFactoryID, strEmployeeID);
        //设计员
        ddlDesignEmployee.DataTextField = "FullName";
        ddlDesignEmployee.DataValueField = "EmployeeID";
        ddlDesignEmployee.DataSource = DT;
        ddlDesignEmployee.DataBind();

        ddlDesignEmployee.Items.Insert(0, "");
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        try
        {
            uMESPagingDataDTO result = rejectapp.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 5);
            this.ItemGrid.DataSource = result.DBTable;
            this.ItemGrid.DataBind();
            this.txtTotalPage.Text = result.PageCount;
            if (result.RowCount == "0")
            {
                this.txtCurrentPage.Text = "0";
            }
            lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
            this.txtPage.Text = this.txtCurrentPage.Text;

            ClearContainerDisp();
            ClearRejectAppDisp();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion
    
    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("Status", "10");
                //查询指定的工艺/计划
                Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                string strOprEmployeeID = userInfo["EmployeeID"];
                if (cbDesignEmployee.Checked == true)
                {
                    para.Add("DesignEmployeeID", strOprEmployeeID);
                }


                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();
        ClearContainerDisp();
        try
        {
            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            DataTable DT = assembly.GetContainerInfo(strContainerName);

            if (DT.Rows.Count > 0)
            {
                string strProcessNo = DT.Rows[0]["ProcessNo"].ToString();
                txtDispProcessNo.Text = strProcessNo;

                string strOprNo = DT.Rows[0]["OprNo"].ToString();
                txtDispOprNo.Text = strOprNo;

                txtDispContainerName.Text = strContainerName;

                string strContainerID = DT.Rows[0]["ContainerID"].ToString();
                txtDispContainerID.Text = strContainerID;

                string strWorkflowID = DT.Rows[0]["WorkflowID"].ToString();
                txtDispWorkflowID.Text = strWorkflowID;

                string strProductName = DT.Rows[0]["ProductName"].ToString();
                txtDispProductName.Text = strProductName;
                string strProductID = DT.Rows[0]["ProductID"].ToString();
                txtDispProductID.Text = strProductID;

                string strDescription = DT.Rows[0]["Description"].ToString();
                txtDispDescription.Text = strDescription;

                string strContainerQty = DT.Rows[0]["Qty"].ToString();
                txtDispContainerQty.Text = strContainerQty;

                string strPlannedStartDate = DT.Rows[0]["PlannedStartDate"].ToString();
                if (strPlannedStartDate != string.Empty)
                {
                    txtDispPlannedStartDate.Text = Convert.ToDateTime(strPlannedStartDate).ToString("yyyy-MM-dd");
                }

                string strPlannedCompletionDate = DT.Rows[0]["PlannedCompletionDate"].ToString();
                if (strPlannedCompletionDate != string.Empty)
                {
                    txtDispPlannedCompletionDate.Text = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                }
            }

            txtID.Text = string.Empty;
            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtID.Text = strID;

            txtStatus.Text = string.Empty;
            string strStatus = e.Row.Cells.FromKey("Status").Value.ToString();
            txtStatus.Text = strStatus;

            txtDispRejectAppInfoName.Text = string.Empty;
            string strRejectAppInfoName = e.Row.Cells.FromKey("RejectAppInfoName").Value.ToString();
            txtDispRejectAppInfoName.Text = strRejectAppInfoName;

            txtDispSpecName.Text = string.Empty;
            string strSpecName = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
            txtDispSpecName.Text = strSpecName;

            txtDispQty.Text = string.Empty;
            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            txtDispSubmitFullName.Text = string.Empty;
            string strSubmitFullName = e.Row.Cells.FromKey("SubmitFullName").Value.ToString();
            txtDispSubmitFullName.Text = strSubmitFullName;

            //显示不合格信息描述
            txtDispQualityNotes.InnerHtml = string.Empty;
            if (e.Row.Cells.FromKey("QualityNotes").Value != null)
            {
                string strQualityNotes = "";
                ParseCode(e.Row.Cells.FromKey("QualityNotes").Text, ref strQualityNotes);
                txtDispQualityNotes.InnerHtml = strQualityNotes;
            }

            txtDispSubmitDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("SubmitDate").Value != null)
            {
                string strSubmitDate = e.Row.Cells.FromKey("SubmitDate").Value.ToString();
                txtDispSubmitDate.Text = Convert.ToDateTime(strSubmitDate).ToString("yyyy-MM-dd");
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择要审核的不合格品审理单", false);
                return;
            }
            if (string.IsNullOrWhiteSpace(ddlDesignEmployee.SelectedValue)) {
                DisplayMessage("请选择要审核的工艺员", false);
                return;
            }

            if (CheckData() == false)
            {
                return;
            }

            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strOprEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> paraH = new Dictionary<string, string>();
            paraH.Add("RejectAppInfoID", strID);
            paraH.Add("OprType", "10");
            paraH.Add("OprEmployeeID", strOprEmployeeID);
            paraH.Add("OprDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            paraH.Add("Notes", "");

            string strDisposeNotes = txtDisposeNotes.Text.Trim();
            Dictionary<string, string> paraM = new Dictionary<string, string>();
            paraM.Add("Status", "10");
            paraM.Add("DesignEmpNotes", strDisposeNotes);
            paraM.Add("DesignEmployeeID", ddlDesignEmployee.SelectedValue);//指定工艺员

            rejectapp.AppRejectAppInfo(paraH, paraM, null);

            #region 记录日志
            var ml = new MESAuditLog();
            ml.ContainerName = txtDispContainerName.Text; ml.ContainerID = txtDispContainerID.Text;
            ml.ParentID = strID; ml.ParentName = parentName;
            ml.CreateEmployeeID = userInfo["EmployeeID"];
            ml.BusinessName = businessName; ml.OperationType = 1;
            ml.Description = "不合格审理:" + txtDispSpecName.Text + ",设计员审核";
            common.SaveMESAuditLog(ml);
            #endregion


            QueryData(GetQuery());

            ClearContainerDisp();
            ClearRejectAppDisp();

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 数据验证
    protected Boolean CheckData()
    {
        string strDisposeNotes = txtDisposeNotes.Text.Trim();
        if (strDisposeNotes == string.Empty)
        {
            DisplayMessage("请填写处理意见", false);
            txtDisposeNotes.Focus();
            return false;
        }

        return true;
    }
    #endregion
    #endregion
    
    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            if (ItemGrid.Rows[i].Cells.FromKey("SpecName").Value != null)
            {
                string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
                ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Value = common.GetSpecNameWithOutProdName(strSpecName);
            }
        }
    }

    #region 特殊字符转换

    public void ParseCode(String strPreviewCode, ref string strPreviewCodeDis)
    {
        try
        {
            string webRootDir = HttpRuntime.AppDomainAppPath;
            string webRootUrl = "~";

            String strPreviewCodeTemp, strHtmlStripped, strTemp, strCodeTemp;
            String strFileDoc, strFileTemp, strFilePath, strFileName, strMapPath, strHtml;
            int intStartFlag, intEndFlag, intFlag1, intFlag2, intCodeStartFlag, intCodeEndFlag;
            String strImageIndex;
            clsParseCode oParse = new clsParseCode();
            Bitmap objBmp;
            String strLeft, strUp, strDown, strRight;

            strPreviewCode = strPreviewCode.Trim().Replace("\r\n\t\t", "");
            strPreviewCode = strPreviewCode.Trim().Replace("<P>", "");
            strPreviewCode = strPreviewCode.Trim().Replace("</P>", "");
            strPreviewCode = strPreviewCode.Replace("\"", "'");

            intStartFlag = strPreviewCode.IndexOf("<Image>");
            intEndFlag = strPreviewCode.IndexOf("</Image>");
            strHtmlStripped = "";
            strHtml = "";

            //strMapPath = MapPath("~");
            //strFileTemp = strMapPath + @"\Images\";

            //clsCon oCon = new clsCon();
            String strImagePath;
            //strImagePath = oCon.LoadConfigString("ImageGetPath");

            strImagePath = webRootDir + ConfigurationManager.AppSettings["ImageGetPath"].ToString();

            strFileTemp = strImagePath;

            uMESExternalControl.ToleranceInputLib.clsDrawImage oDraw = new uMESExternalControl.ToleranceInputLib.clsDrawImage();
            DataTable dtImage = new DataTable();

            while (intStartFlag > -1)
            {
                if (intStartFlag > 0)
                {
                    //将纯文本输出
                    strHtml = strPreviewCode.Substring(0, intStartFlag);
                    strPreviewCode = strPreviewCode.Substring(intStartFlag);
                    strPreviewCodeDis += strHtml;
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                    continue;
                }

                else
                {
                    strHtml = "";
                    strPreviewCodeTemp = strPreviewCode.Substring(intStartFlag + 7, intEndFlag - intStartFlag - 7);
                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<&70><+>");
                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("<+><&90>");

                    if (intCodeStartFlag > -1)
                    {
                        //代码为行位公差时
                        strCodeTemp = strPreviewCodeTemp.Replace("<&70><+>", "");
                        strCodeTemp = strCodeTemp.Replace("<+><&90>", "");

                        intFlag1 = strCodeTemp.IndexOf("<");
                        intFlag2 = strCodeTemp.IndexOf(">");
                        while (intFlag1 > -1)
                        {
                            strTemp = strCodeTemp.Substring(intFlag1, intFlag2 - intFlag1 + 1);
                            strCodeTemp = strCodeTemp.Replace(strTemp, "");
                            intFlag1 = strCodeTemp.IndexOf("<");
                            intFlag2 = strCodeTemp.IndexOf(">");
                        }

                        strHtmlStripped = strCodeTemp;
                        //strPreviewCodeTemp = strPreviewCode;
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"<Image>", "");
                        strPreviewCodeTemp = strPreviewCodeTemp.Replace(@"</Image>", "");

                        objBmp = oDraw.DrawShapeTolerance(strPreviewCodeTemp, strHtmlStripped, strFileTemp);
                    }
                    else
                    {
                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<T");
                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("!");
                        if (intCodeStartFlag > -1)
                        {
                            //代码为上下标公差

                            strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                            strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 2, intCodeEndFlag - intCodeStartFlag - 2);
                            strDown = strPreviewCodeTemp.Substring(intCodeEndFlag + 1, strPreviewCodeTemp.Length - intCodeEndFlag - 2);
                            objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                        }
                        else
                        {
                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<H>");
                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</H>");
                            if (intCodeStartFlag > -1)
                            {
                                //代码为只有上公差
                                strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                strUp = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                strDown = "";
                                objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                            }
                            else
                            {
                                intCodeStartFlag = strPreviewCodeTemp.IndexOf("<L>");
                                intCodeEndFlag = strPreviewCodeTemp.IndexOf("</L>");
                                if (intCodeStartFlag > -1)
                                {
                                    //代码为只有下公差
                                    strLeft = strPreviewCodeTemp.Substring(0, intCodeStartFlag);
                                    strUp = "";
                                    strDown = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);
                                    objBmp = oDraw.DrawTolerance(strLeft, strUp, strDown);
                                }
                                else
                                {
                                    intCodeStartFlag = strPreviewCodeTemp.IndexOf("<√>");
                                    intCodeEndFlag = strPreviewCodeTemp.IndexOf("</√>");
                                    if (intCodeStartFlag > -1)
                                    {
                                        //代码为指定加工方法时
                                        strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                        strRight = strPreviewCodeTemp.Substring(intCodeEndFlag + 4);
                                        objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                    }
                                    else
                                    {
                                        intCodeStartFlag = strPreviewCodeTemp.IndexOf("<R>");
                                        intCodeEndFlag = strPreviewCodeTemp.IndexOf("</R>");
                                        if (intCodeStartFlag > -1)
                                        {
                                            //代码为去除材料时
                                            strLeft = strPreviewCodeTemp.Substring(intCodeStartFlag + 3, intCodeEndFlag - intCodeStartFlag - 3);

                                            strRight = "";
                                            objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                        }
                                        else
                                        {
                                            intCodeStartFlag = strPreviewCodeTemp.IndexOf("<Q>");
                                            intCodeEndFlag = strPreviewCodeTemp.IndexOf("</Q>");
                                            if (intCodeStartFlag > -1)
                                            {
                                                //代码为不去除材料时
                                                strLeft = "";

                                                strRight = "";
                                                objBmp = oDraw.DrawRoughness(strLeft, strRight);
                                            }
                                            else
                                            {
                                                objBmp = null;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }

                    //保存
                    if (objBmp != null)
                    {
                        //strFileDoc = strMapPath + @"\ImageTemp\";
                        //clsCon oCon = new clsCon();
                        String strImageTempPath;
                        //strImageTempPath = oCon.LoadConfigString("ImageTempPath");
                        strImageTempPath = webRootDir + ConfigurationManager.AppSettings["ImageTempPath"].ToString();

                        strFileDoc = strImageTempPath;
                        //if (Session["intMaxImageIndex"] == null)
                        //{
                        //    Session["intMaxImageIndex"] = 0;
                        //    intImageIndex = 1;
                        //}
                        //else
                        //{
                        //    intImageIndex = (int)Session["intMaxImageIndex"] + 1;
                        //}

                        strImageIndex = System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
                        strFileName = "ImageTemp-" + strImageIndex + ".png";
                        //strFileName = "ImageTemp-" + intImageIndex + ".png";
                        strFilePath = strFileDoc + strFileName;
                        //System.Threading.Thread.Sleep(1);
                        //System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        //while (oFile.Exists == true)
                        //{
                        //    intImageIndex += 1;
                        //    strFileName = "ImageTemp-" + intImageIndex + ".png";
                        //    strFilePath = strFileDoc + strFileName;
                        //    oFile = new System.IO.FileInfo(strFilePath);
                        //}

                        System.IO.FileInfo oFile = new System.IO.FileInfo(strFilePath);
                        while (oFile.Exists == true)
                        {
                            strImageIndex = strImageIndex + "1";
                            strFileName = "ImageTemp-" + strImageIndex + ".png";
                            strFilePath = strFileDoc + strFileName;
                            oFile = new System.IO.FileInfo(strFilePath);
                        }

                        objBmp.Save(strFilePath);
                        // Session["intMaxImageIndex"] = intImageIndex;
                        if (Session["dtImage"] == null)
                        {
                            dtImage = new DataTable();
                            dtImage.Columns.Add("FileName");
                            dtImage.Columns.Add("HtmlCode");
                            Session["dtImage"] = dtImage;
                        }
                        else
                        {
                            dtImage = (DataTable)Session["dtImage"];
                        }

                        DataRow dr;
                        dr = dtImage.NewRow();
                        dr[0] = strFileName;
                        dr[1] = "<Image>" + strPreviewCodeTemp + "</Image>";
                        dtImage.Rows.Add(dr);
                        Session["dtImage"] = dtImage;

                        //strFileDoc = oCon.LoadConfigString("ImageTempPath");
                        //strFileDoc = ConfigurationManager.AppSettings["ImageTempPath"].ToString();
                        strFileDoc = ResolveUrl(webRootUrl + ConfigurationManager.AppSettings["ImageTempPath"].ToString());

                        // strHtml = "<img src='../../../ImageTemp/" + strFileName + "'>";
                        strHtml = @"<img src='" + strFileDoc + strFileName + "'>";
                        strPreviewCodeDis += strHtml;
                    }

                }
                if (intEndFlag + 8 < strPreviewCode.Length)
                {
                    strPreviewCode = strPreviewCode.Substring(intEndFlag + 8);
                    intStartFlag = strPreviewCode.IndexOf("<Image>");
                    intEndFlag = strPreviewCode.IndexOf("</Image>");
                }
                else
                {
                    intStartFlag = -1;
                }

            }
            intStartFlag = strPreviewCode.IndexOf("<Image>");
            if (strPreviewCode != "" && intStartFlag == -1)
            {
                strPreviewCodeDis += strPreviewCode;
            }
        }
        catch (Exception myError)
        {

        }

    }

    public String GetCode(string strNotes)
    {
        String strHtmlCode, strHtml, strHtmlTemp;
        strHtml = strNotes;
        try
        {
            DataTable dtImage = new DataTable();
            int i, intStartFlag, intEndFlag;
            dtImage = (DataTable)Session["dtImage"];

            // strHtml = txtQualityNotes.Text.Trim();
            strHtmlCode = strHtml;

            intStartFlag = strHtmlCode.IndexOf("<img");
            if (intStartFlag == -1)
            {
                intStartFlag = strHtmlCode.IndexOf("<IMG");
            }
            if (intStartFlag > -1)
            {
                intEndFlag = strHtmlCode.IndexOf(">", intStartFlag);
                ;
            }
            else
            {
                intEndFlag = -1;
            }

            if (dtImage != null)
            {
                if (dtImage.Rows.Count > 0)
                {
                    while (intStartFlag > -1)
                    {
                        strHtmlTemp = strHtmlCode.Substring(intStartFlag, intEndFlag - intStartFlag + 1);
                        for (i = 0; i <= dtImage.Rows.Count - 1; i++)
                        {
                            if (strHtmlTemp.IndexOf(@"/" + dtImage.Rows[i][0]) > -1)
                            {
                                strHtmlCode = strHtmlCode.Replace(strHtmlTemp, (String)dtImage.Rows[i][1]);
                                break;
                            }

                        }

                        intStartFlag = strHtmlCode.IndexOf("<img", intStartFlag + 1);
                        if (intStartFlag == -1)
                        {
                            intStartFlag = strHtmlCode.IndexOf("<IMG", intStartFlag + 1);
                        }
                        if (intStartFlag > -1)
                        {
                            intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                            ;
                        }
                        else
                        {
                            intEndFlag = -1;
                        }
                        //intEndFlag = strHtmlCode.IndexOf(">", intStartFlag + 1);
                    }

                }

            }

            return strHtmlCode;
        }
        catch (Exception myError)
        {
            DisplayMessage(myError.Message, false);
            return "";
        }
    }
    #endregion
}