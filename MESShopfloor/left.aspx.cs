﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using uMES.LeanManufacturing.ReportBusiness;
using System.Web.Services;
using Newtonsoft.Json;

public partial class left : System.Web.UI.Page
{
    public DataTable dtMenu = new DataTable();
    public DataTable dtMenuChild = new DataTable();
    public DataRow[] drs = new DataRow[1000];
    uMESRoleMenuBusiness RoleBusiness = new uMESRoleMenuBusiness();
   static uMESProblemBusiness problem = new uMESProblemBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            if (userInfo == null)
            {
                Response.Write("<script>window.top.location='/MESShopfloor';</script>");
                return;
            }

            string strEmployeeID = userInfo["EmployeeID"]; hdEmployeeID.InnerText = JsonConvert.SerializeObject(userInfo);
            string strEmployeeName = userInfo["EmployeeName"];


            DataTable dt = RoleBusiness.GetEmployeeRole(strEmployeeID);

            string strRoleList = "";
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    string strRole = dt.Rows[i]["roleid"].ToString();
                    if (strRoleList == "")
                    {
                        strRoleList = "'" + strRole + "'";
                    }
                    else
                    {
                        strRoleList += ",'" + strRole + "'";
                    }
                }
            }
            if (strRoleList == "")
            {
                strRoleList = "'$$$$'";
            }
            //主菜单
            DataTable dtAll = RoleBusiness.GetRoleMenuData(strRoleList);
            
            dtMenu = dtAll.Clone();

            foreach (DataRow row in dtAll.Rows)
            {
                string strMenuName = row["MenuName"].ToString();

                DataRow[] rows = dtMenu.Select(string.Format("MenuName = '{0}'", strMenuName));

                if (rows.Length <= 0)
                {
                    dtMenu.Rows.Add(row.ItemArray);
                }
            }

            if (strEmployeeName.ToLower() == "administrator"&& dtMenu.Rows.Count==0)
            {
                //主菜单表
                dtMenu.Columns.Add("menuname");
                dtMenu.Columns.Add("menuid");
                DataRow row = dtMenu.NewRow();
                string strID = Guid.NewGuid().ToString();
                row["menuid"] = strID;
                row["menuname"] = "系统配置";
                dtMenu.Rows.Add(row);

                //子菜单
                dtMenuChild.Columns.Add("sequence");
                dtMenuChild.Columns.Add("mainid");
                dtMenuChild.Columns.Add("menuchildname");
                dtMenuChild.Columns.Add("url");
                DataRow r = dtMenuChild.NewRow(); 
                r["mainid"] = strID;
                r["menuchildname"] = "菜单管理";
                r["sequence"] = 1;
                r["url"] = "Custom/BaseManage/MenuManagerForm.aspx";
                dtMenuChild.Rows.Add(r);

                r = dtMenuChild.NewRow();
                r["mainid"] = strID;
                r["menuchildname"] = "角色人员管理";
                r["sequence"] = 2;
                r["url"] = "Custom/BaseManage/RoleEmployeeForm.aspx";
                dtMenuChild.Rows.Add(r);

                r = dtMenuChild.NewRow();
                r["mainid"] = strID;
                r["menuchildname"] = "角色菜单管理";
                r["sequence"] =3;
                r["url"] = "Custom/BaseManage/RoleMenuForm.aspx";
                dtMenuChild.Rows.Add(r);

                r = dtMenuChild.NewRow();
                r["mainid"] = strID;
                r["menuchildname"] = "人员角色关系导入";
                r["sequence"] = 4;
                r["url"] = "Custom/BaseManage/ImportRoleEmployee.aspx";
                dtMenuChild.Rows.Add(r);
            }
            else
            {
                

                string strMenuList = "";
                if (dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtMenu.Rows.Count - 1; i++)
                    {
                        string strRole = dtMenu.Rows[i]["menuid"].ToString();
                        if (strMenuList == "")
                        {
                            strMenuList = "'" + strRole + "'";
                        }
                        else
                        {
                            strMenuList += ",'" + strRole + "'";
                        }
                    }
                }
                if (strMenuList == "")
                {
                    strMenuList = "'$$$$'";
                }
                dtMenuChild = RoleBusiness.GetMenuChildData(strMenuList);
            }  
        }
    }


   
}
