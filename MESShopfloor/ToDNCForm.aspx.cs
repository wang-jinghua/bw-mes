﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Configuration;
using QRCoder;
using System.IO;
using System.Web.UI;

public partial class ToDNCForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "ToDNCForm";
    public uMESCommonBusiness common = new uMESCommonBusiness();
    uMESWorkReportBusiness workreport = new uMESWorkReportBusiness();
    uMESSysIntegrationBusiness integration = new uMESSysIntegrationBusiness();

    public DataTable dtDispatch;

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：向DNC发送派工单";
        master.strTitle = "向DNC发送派工单";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            QueryData(GetQuery());
            ClearMessage_PageLoad();
        }
    }

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strTeamID = userInfo["TeamID"];
        result.Add("TeamID", strTeamID);
        result.Add("DispatchType", "1");
        result.Add("ScanContainerName", strScanContainerName);
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("SpecName", strSpecName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        uMESPagingDataDTO result = workreport.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 9);

        dtDispatch = result.DBTable;

        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;

        ClearDispData();
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    protected void ClearDispData()
    {
        txtDispID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispResourceName.Text = string.Empty;
    }
    
    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                string strTeamID = userInfo["TeamID"];
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("TeamID", strTeamID);
                para.Add("DispatchType", "1");

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    /// <summary>
    /// 生成二维码
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDataCol_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strDispatchInfoID = txtDispID.Text;
            if (strDispatchInfoID == string.Empty)
            {
                DisplayMessage("请选择派工单", false);
                return;
            }

            string strProductName = txtDispProductName.Text;
            string strDescription = txtDispDescription.Text;
            string strSpec = txtDispSpecName.Text;
            string strSpecNo = common.GetSpecNoFromSpecName(strSpec);
            string strSpecName = common.GetSpecNameFromSpecName(strSpec);
            string strResourceName = txtDispResourceName.Text;

            string popupData = strProductName + "," + strDescription + "," + strSpecNo + "," + strSpecName + "," + strResourceName;

            Session["popupData"] = popupData;

            ClientScript.RegisterStartupScript(GetType(), "", "<script>openQRCodeForm()</script>");

            //保存历史记录
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("DispatchInfoID", strDispatchInfoID);
            para.Add("OprType", "1");
            para.Add("OprEmployeeID", strEmployeeID);
            para.Add("OprDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            para.Add("OprResult", "0");
            para.Add("Notes", "");

            integration.AddToDNCHistory(para);
        }
        catch (Exception Ex)
        {
            DisplayMessage(Ex.Message, false);
        }
    }
    
    #region 生成二维码
    /// <summary>
    /// 生成二维码
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    protected string CreateQRCodeImg(string str, string strDispatchInfoID)
    {
        string strCode =System.Web.HttpUtility.UrlEncode(str) ;//str;
        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        QRCodeData qrCodeData = qrGenerator.CreateQrCode(strCode, QRCodeGenerator.ECCLevel.Q);
        QRCode qrcode = new QRCode(qrCodeData);

        Bitmap qrCodeImage = qrcode.GetGraphic(6, Color.Black, Color.White, null, 5, 1, false);
        MemoryStream ms = new MemoryStream();
        string strPath = ConfigurationManager.AppSettings["DNCQRCodePath"];
        string strImgName = strDispatchInfoID + ".jpg";
        qrCodeImage.Save(strPath + strImgName);

        return strImgName;
    }
    #endregion
}