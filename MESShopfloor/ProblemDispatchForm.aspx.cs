﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System.Drawing;
using System.Web.UI;

public partial class ProblemDispatchForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "ProblemDispatchForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESProblemBusiness problem = new uMESProblemBusiness();
    uMESAssemblyRecordBusiness assembly = new uMESAssemblyRecordBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：问题指派";
        master.strTitle = "问题指派";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
            GetProblemType();
            GetProblemLevel();
            GetFactory();
        }
    }

    #region 获取ProblemType
    protected void GetProblemType()
    {
        DataTable DT = common.GetProblemType();

        ddlProblemType.DataTextField = "ProblemTypeName";
        ddlProblemType.DataValueField = "ProblemTypeID";
        ddlProblemType.DataSource = DT;
        ddlProblemType.DataBind();

        ddlProblemType.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 获取ProblemLevel
    protected void GetProblemLevel()
    {
        DataTable DT = common.GetProblemLevel();

        ddlProblemLevel.DataTextField = "ProblemLevelName";
        ddlProblemLevel.DataValueField = "ProblemLevelID";
        ddlProblemLevel.DataSource = DT;
        ddlProblemLevel.DataBind();

        ddlProblemLevel.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 获取Factory
    protected void GetFactory()
    {
        DataTable DT = common.GetFactoryNames();

        ddlToFactory.DataTextField = "FactoryName";
        ddlToFactory.DataValueField = "FactoryID";
        ddlToFactory.DataSource = DT;
        ddlToFactory.DataBind();

        ddlToFactory.Items.Insert(0, string.Empty);

        ddlToFactory2.DataTextField = "FactoryName";
        ddlToFactory2.DataValueField = "FactoryID";
        ddlToFactory2.DataSource = DT;
        ddlToFactory2.DataBind();

        ddlToFactory2.Items.Insert(0, string.Empty);
    }
    #endregion

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        txtTitle.Text = string.Empty;
        ddlProblemType.SelectedValue = string.Empty;
        ddlProblemLevel.SelectedValue = string.Empty;
        ddlToFactory.SelectedValue = string.Empty;
        ddlToEmployee.SelectedValue = string.Empty;

        ClearContainerDisp();
        ClearProblemDisp();
    }

    protected void ClearContainerDisp()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispProductID.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtChildCount.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;
    }

    protected void ClearProblemDisp()
    {
        txtProblemTitle.Text = string.Empty;
        txtID.Text = string.Empty;
        txtStatus.Text = string.Empty;
        txtDispProblemType.Text = string.Empty;
        txtDispProblemLevel.Text = string.Empty;
        txtDispPlannedDisposeDate.Text = string.Empty;
        txtDispToFactory.Text = string.Empty;
        txtDispToEmployee.Text = string.Empty;
        txtDisposeEmployee.Text = string.Empty;
        txtProblemDetails.Text = string.Empty;
        txtNotes2.Text = string.Empty;

        ddlToFactory2.SelectedValue = string.Empty;
        ddlToEmployee2.SelectedValue = string.Empty;
    }
    #endregion

    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        string strProblemType = ddlProblemType.SelectedValue;
        string strProblemLevel = ddlProblemLevel.SelectedValue;
        string strToFactory = ddlToFactory.SelectedValue;
        string strToEmployee = ddlToEmployee.SelectedValue;

        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);
        result.Add("ProblemTypeID", strProblemType);
        result.Add("ProblemLevelID", strProblemLevel);
        result.Add("ToFactoryID", strToFactory);
        result.Add("ToEmployeeID", strToEmployee);
        result.Add("Status", "0,5");
        result.Add("ProblemTitle", txtTitle.Text);

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        try
        {
            uMESPagingDataDTO result = problem.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 15);
            this.ItemGrid.DataSource = result.DBTable;
            this.ItemGrid.DataBind();
            this.txtTotalPage.Text = result.PageCount;
            if (result.RowCount == "0")
            {
                this.txtCurrentPage.Text = "0";
            }
            lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
            this.txtPage.Text = this.txtCurrentPage.Text;
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        txtTitle.Text = string.Empty;
        ddlProblemType.SelectedValue = string.Empty;
        ddlProblemLevel.SelectedValue = string.Empty;
        ddlToFactory.SelectedValue = string.Empty;
        ddlToEmployee.SelectedValue = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("Status", "0,5");

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();
        try
        {
            string strContainerName = string.Empty;
            if (e.Row.Cells.FromKey("ContainerName").Value != null)
            {
                strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            }

            //清空赋值
            txtDispContainerName.Text = string.Empty;
            txtDispProcessNo.Text = string.Empty;
            txtDispOprNo.Text = string.Empty;
            txtDispContainerID.Text = string.Empty;
            txtDispDescription.Text = string.Empty;
            txtDispWorkflowID.Text = string.Empty;
            txtDispProductName.Text = string.Empty;
            txtDispProductID.Text = string.Empty;
            txtDispQty.Text = string.Empty;
            txtDispPlannedStartDate.Text = string.Empty;
            txtDispPlannedCompletionDate.Text = string.Empty;
            DataTable DT = assembly.GetContainerInfo(strContainerName);

            if (DT.Rows.Count > 0)
            {
                string strProcessNo = DT.Rows[0]["ProcessNo"].ToString();
                txtDispProcessNo.Text = strProcessNo;

                string strOprNo = DT.Rows[0]["OprNo"].ToString();
                txtDispOprNo.Text = strOprNo;

                txtDispContainerName.Text = strContainerName;

                string strContainerID = DT.Rows[0]["ContainerID"].ToString();
                txtDispContainerID.Text = strContainerID;

                string strWorkflowID = DT.Rows[0]["WorkflowID"].ToString();
                txtDispWorkflowID.Text = strWorkflowID;

                string strProductName = DT.Rows[0]["ProductName"].ToString();
                txtDispProductName.Text = strProductName;
                string strProductID = DT.Rows[0]["ProductID"].ToString();
                txtDispProductID.Text = strProductID;

                string strDescription = DT.Rows[0]["Description"].ToString();
                txtDispDescription.Text = strDescription;

                string strQty = DT.Rows[0]["Qty"].ToString();
                txtDispQty.Text = strQty;

                string strPlannedStartDate = DT.Rows[0]["PlannedStartDate"].ToString();
                if (strPlannedStartDate != string.Empty)
                {
                    txtDispPlannedStartDate.Text = Convert.ToDateTime(strPlannedStartDate).ToString("yyyy-MM-dd");
                }

                string strPlannedCompletionDate = DT.Rows[0]["PlannedCompletionDate"].ToString();
                if (strPlannedCompletionDate != string.Empty)
                {
                    txtDispPlannedCompletionDate.Text = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                }

            }

            txtID.Text = string.Empty;
            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtID.Text = strID;

            txtDispSpec.Text = string.Empty;
            if (e.Row.Cells.FromKey("SpecNameDisp").Value != null)
            {
                string strSpec = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
                txtDispSpec.Text = strSpec;
            }

            txtStatus.Text = string.Empty;
            string strStatus = e.Row.Cells.FromKey("Status").Value.ToString();
            txtStatus.Text = strStatus;

            txtProblemTitle.Text = string.Empty;
            string strProblemTitle = e.Row.Cells.FromKey("ProblemTitle").Value.ToString();
            txtProblemTitle.Text = strProblemTitle;

            txtDispProblemType.Text = string.Empty;
            string strProblemType = e.Row.Cells.FromKey("ProblemTypeName").Value.ToString();
            txtDispProblemType.Text = strProblemType;

            txtDispProblemLevel.Text = string.Empty;
            string strProblemLevel = e.Row.Cells.FromKey("ProblemLevelName").Value.ToString();
            txtDispProblemLevel.Text = strProblemLevel;

            txtDispToFactory.Text = string.Empty;
            string strToFactory = e.Row.Cells.FromKey("ToFactoryName").Value.ToString();
            txtDispToFactory.Text = strToFactory;

            txtDispToEmployee.Text = string.Empty;
            if (e.Row.Cells.FromKey("ToFullName").Value != null)
            {
                string strToEmployee = e.Row.Cells.FromKey("ToFullName").Value.ToString();
                txtDispToEmployee.Text = strToEmployee;
            }

            txtProblemDetails.Text = string.Empty;
            string strProblemDetails = e.Row.Cells.FromKey("ProblemDetails").Value.ToString();
            txtProblemDetails.Text = strProblemDetails;

            txtDisposeEmployee.Text = string.Empty;
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strFullName = userInfo["FullName"];
            txtDisposeEmployee.Text = strFullName;

            txtNotes2.Text = string.Empty;
            if (e.Row.Cells.FromKey("Notes2").Value != null)
            {
                string strNotes = e.Row.Cells.FromKey("Notes2").Value.ToString();
                txtNotes2.Text = strNotes;
            }

            ddlToFactory2.SelectedValue = string.Empty;
            if (e.Row.Cells.FromKey("ToFactoryID2").Value != null)
            {
                string strToFactoryID = e.Row.Cells.FromKey("ToFactoryID2").Value.ToString();
                ddlToFactory2.SelectedValue = strToFactoryID;

                ddlToFactory2_SelectedIndexChanged(null, null);
            }

            ddlToEmployee2.SelectedValue = string.Empty;
            if (e.Row.Cells.FromKey("ToEmployeeID2").Value != null)
            {
                string strToEmployeeID = e.Row.Cells.FromKey("ToEmployeeID2").Value.ToString();
                ddlToEmployee2.SelectedValue = strToEmployeeID;
            }

            txtDispPlannedDisposeDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("PlannedDisposeDate").Value != null)
            {
                string strPlannedDisposeDate = e.Row.Cells.FromKey("PlannedDisposeDate").Value.ToString();
                txtDispPlannedDisposeDate.Text = Convert.ToDateTime(strPlannedDisposeDate).ToString("yyyy-MM-dd");
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strID = txtID.Text;
            if (strID == string.Empty)
            {
                DisplayMessage("请选择要指派的问题", false);
                return;
            }

            if (CheckData() == false)
            {
                return;
            }

            string strProblemTitle = txtProblemTitle.Text.Trim();
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strDispatchEmployeeID = userInfo["EmployeeID"];
            string strToFactoryID = ddlToFactory2.SelectedValue;
            string strToEmployeeID = ddlToEmployee2.SelectedValue;
            string strNotes = txtNotes2.Text.Trim();

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ID", strID);
            para.Add("DispatchEmployeeID", strDispatchEmployeeID);
            para.Add("DispatchDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            para.Add("ToFactoryID2", strToFactoryID);
            para.Add("ToEmployeeID2", strToEmployeeID);
            para.Add("Notes2", strNotes);

            problem.DispatchProblem(para);

            QueryData(GetQuery());

            ClearContainerDisp();
            ClearProblemDisp();

            DisplayMessage("保存成功", true);
        }
        catch(Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 数据验证
    protected Boolean CheckData()
    {
        string strToFactoryID = ddlToFactory2.SelectedValue;
        if (strToFactoryID == string.Empty)
        {
            DisplayMessage("请选择指派的处理部门", false);
            ddlToFactory2.Focus();
            return false;
        }
        
        string strToEmployeeID = ddlToEmployee2.SelectedValue;
        if (strToEmployeeID == string.Empty)
        {
            DisplayMessage("请选择指派的处理人", false);
            ddlToEmployee2.Focus();
            return false;
        }

        return true;
    }
    #endregion
    #endregion

    protected void ddlToFactory_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();

        ddlToEmployee.Items.Clear();

        try
        {
            string strFactoryID = ddlToFactory.SelectedValue;
            if (strFactoryID != string.Empty)
            {
                DataTable DT = common.GetEmployeeByFactory(strFactoryID);

                ddlToEmployee.DataTextField = "FullName";
                ddlToEmployee.DataValueField = "EmployeeID";
                ddlToEmployee.DataSource = DT;
                ddlToEmployee.DataBind();

                ddlToEmployee.Items.Insert(0, string.Empty);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            if (ItemGrid.Rows[i].Cells.FromKey("SpecName").Value != null)
            {
                string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
                ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Value = common.GetSpecNameWithOutProdName(strSpecName);
            }
        }
    }

    protected void ddlToFactory2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();

        ddlToEmployee2.Items.Clear();

        try
        {
            string strFactoryID = ddlToFactory2.SelectedValue;
            if (strFactoryID != string.Empty)
            {
                DataTable DT = common.GetEmployeeByFactory(strFactoryID);

                ddlToEmployee2.DataTextField = "FullName";
                ddlToEmployee2.DataValueField = "EmployeeID";
                ddlToEmployee2.DataSource = DT;
                ddlToEmployee2.DataBind();

                ddlToEmployee2.Items.Insert(0, string.Empty);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
}