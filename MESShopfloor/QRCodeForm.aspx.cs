﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QRCoder;
using System.Drawing;
using System.IO;
using System.Configuration;

public partial class QRCodeForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string popupData = Session["popupData"].ToString();

        string strImgName = CreateQRCodeImg(popupData);
        imgQRCode.ImageUrl = "DNCQRCode/" + strImgName;
    }

    #region 生成二维码
    /// <summary>
    /// 生成二维码
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    protected string CreateQRCodeImg(string str)
    {
        string strCode = str;
        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        QRCodeData qrCodeData = qrGenerator.CreateQrCode(strCode, QRCodeGenerator.ECCLevel.Q);
        QRCode qrcode = new QRCode(qrCodeData);

        Bitmap qrCodeImage = qrcode.GetGraphic(10, Color.Black, Color.White, null, 5, 1, false);
        MemoryStream ms = new MemoryStream();
        string strPath = ConfigurationManager.AppSettings["DNCQRCodePath"];
        string strImgName = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff") + ".jpg";
        qrCodeImage.Save(strPath + strImgName);

        return strImgName;
    }
    #endregion
}