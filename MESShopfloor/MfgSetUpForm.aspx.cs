﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Web.UI;

public partial class MfgSetUpForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "MfgSetUpForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();
    uMESMfgSetUpBusiness setup = new uMESMfgSetUpBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：生产准备";
        master.strTitle = "生产准备";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strSpecName = txtSpecName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strTeamID = userInfo["TeamID"];
        result.Add("TeamID", strTeamID);
        result.Add("DispatchType", "1");
        result.Add("ScanContainerName", strScanContainerName);
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("SpecName", strSpecName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        Session[QueryWhere] = result;

        return result;
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        uMESPagingDataDTO result = setup.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 9);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;

        ClearDispData();
    }

    protected void ItemGrid_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < ItemGrid.Rows.Count; i++)
        {
            string strSpecName = ItemGrid.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            ItemGrid.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtSpecName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProcessNo").Value != null)
            {
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            txtDispContainerName.Text = strContainerName;
            string strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            txtDispContainerID.Text = strContainerID;

            string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            txtDispProductName.Text = strProductName;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }

            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            string strSpec = e.Row.Cells.FromKey("SpecNameDisp").Value.ToString();
            txtDispSpecName.Text = strSpec;
            string strWorkflowID = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            txtDispWorkflowID.Text = strWorkflowID;
            string strSpecID = e.Row.Cells.FromKey("SpecID").Value.ToString();
            txtDispSpecID.Text = strSpecID;

            string strTeamName = e.Row.Cells.FromKey("TeamName").Value.ToString();
            txtDispTeamName.Text = strTeamName;
            string strTeamID = e.Row.Cells.FromKey("TeamID").Value.ToString();
            txtDispTeamID.Text = strTeamID;

            string strResourceName = e.Row.Cells.FromKey("ResourceName").Value.ToString();
            txtDispResourceName.Text = strResourceName;
            string strResourceID = e.Row.Cells.FromKey("ResourceID").Value.ToString();
            txtDispResourceID.Text = strResourceID;

            DataTable DT = dispatch.GetResourceDispatchInfo(strResourceID);

            wgDispatchList.DataSource = DT;
            wgDispatchList.DataBind();

            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Value.ToString();
                strPlannedCompletionDate = Convert.ToDateTime(strPlannedCompletionDate).ToString("yyyy-MM-dd");
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
            }

            string strID = e.Row.Cells.FromKey("ID").Value.ToString();
            txtDispID.Text = strID;
            string strParentID = e.Row.Cells.FromKey("ParentID").Value.ToString();
            txtDispParentID.Text = strParentID;

            DataTable dtEmployee = dispatch.GetEmployeeByDispatchID(strID);
            wgEmployee.DataSource = dtEmployee;
            wgEmployee.DataBind();

            wgProductNo.Clear();
            DataTable dt = dispatch.GetProductNoByDispatchID(strID);

            wgProductNo.DataSource = dt;
            wgProductNo.DataBind();

            string strParentQty = e.Row.Cells.FromKey("ParentQty").Value.ToString();
            txtParentQty.Text = strParentQty;

            //生产准备信息
            txtDispSetUpID.Text = string.Empty;
            if (e.Row.Cells.FromKey("SetUpID").Value != null)
            {
                txtDispSetUpID.Text = e.Row.Cells.FromKey("SetUpID").Value.ToString();
            }
            cbDocuments.Checked = false;
            if (e.Row.Cells.FromKey("Documents").Value != null)
            {
                string strDocuments = e.Row.Cells.FromKey("Documents").Value.ToString();
                if (strDocuments == "1")
                {
                    cbDocuments.Checked = true;
                }
            }
            cbTools.Checked = false;
            if (e.Row.Cells.FromKey("Tools").Value != null)
            {
                string strTools = e.Row.Cells.FromKey("Tools").Value.ToString();
                if (strTools == "1")
                {
                    cbTools.Checked = true;
                }
            }
            cbCuttingTools.Checked = false;
            if (e.Row.Cells.FromKey("CuttingTools").Value != null)
            {
                string strCuttingTools = e.Row.Cells.FromKey("CuttingTools").Value.ToString();
                if (strCuttingTools == "1")
                {
                    cbCuttingTools.Checked = true;
                }
            }
            cbNCProgram.Checked = false;
            if (e.Row.Cells.FromKey("NCProgram").Value != null)
            {
                string strNCProgram = e.Row.Cells.FromKey("NCProgram").Value.ToString();
                if (strNCProgram == "1")
                {
                    cbNCProgram.Checked = true;
                }
            }
            cbResources.Checked = false;
            if (e.Row.Cells.FromKey("Resources").Value != null)
            {
                string strResources = e.Row.Cells.FromKey("Resources").Value.ToString();
                if (strResources == "1")
                {
                    cbResources.Checked = true;
                }
            }
            cbMaterial.Checked = false;
            if (e.Row.Cells.FromKey("Material").Value != null)
            {
                string strMaterial = e.Row.Cells.FromKey("Material").Value.ToString();
                if (strMaterial == "1")
                {
                    cbMaterial.Checked = true;
                }
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #endregion

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            int intCount = 0;
            TemplatedColumn temCell = (TemplatedColumn)ItemGrid.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    intCount++;
                }
            }

            if (intCount == 0)
            {
                DisplayMessage("请选择要保存的任务", false);
                return;
            }

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                    string strReceiveEmployeeID = userInfo["EmployeeID"];

                    Dictionary<string, string> para = new Dictionary<string, string>();
                    string strID = ItemGrid.Rows[i].Cells.FromKey("ID").Value.ToString();
                    para.Add("DispatchInfoID", strID);

                    int intStatus = 1;
                    if (cbDocuments.Checked == true)
                    {
                        para.Add("Documents", "1");
                    }
                    else
                    {
                        para.Add("Documents", "0");
                        intStatus = 0;
                    }

                    if (cbTools.Checked == true)
                    {
                        para.Add("Tools", "1");
                    }
                    else
                    {
                        para.Add("Tools", "0");
                        intStatus = 0;
                    }

                    if (cbCuttingTools.Checked == true)
                    {
                        para.Add("CuttingTools", "1");
                    }
                    else
                    {
                        para.Add("CuttingTools", "0");
                        intStatus = 0;
                    }

                    if (cbNCProgram.Checked == true)
                    {
                        para.Add("NCProgram", "1");
                    }
                    else
                    {
                        para.Add("NCProgram", "0");
                        intStatus = 0;
                    }

                    if (cbResources.Checked == true)
                    {
                        para.Add("Resources", "1");
                    }
                    else
                    {
                        para.Add("Resources", "0");
                        intStatus = 0;
                    }

                    if (cbMaterial.Checked == true)
                    {
                        para.Add("Material", "1");
                    }
                    else
                    {
                        para.Add("Material", "0");
                        intStatus = 0;
                    }

                    para.Add("Notes", "");
                    para.Add("SubmitEmployeeID", strReceiveEmployeeID);
                    para.Add("SubmitDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    para.Add("Status", intStatus.ToString());

                    string strSetUpID = string.Empty;
                    if (ItemGrid.Rows[i].Cells.FromKey("SetUpID").Value != null)
                    {
                        strSetUpID = ItemGrid.Rows[i].Cells.FromKey("SetUpID").Value.ToString();
                    }

                    if (strSetUpID != string.Empty)
                    {
                        setup.UpdateMfgSetUpInfo(strSetUpID, para);
                    }
                    else
                    {
                        setup.AddMfgSetUpInfo(para);
                    }
                }
            }

            QueryData(GetQuery());

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispSpecName.Text = string.Empty;
        txtDispTeamName.Text = string.Empty;
        txtDispResourceName.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;

        txtDispID.Text = string.Empty;

        wgEmployee.Clear();
        wgProductNo.Clear();
        wgDispatchList.Clear();

        cbDocuments.Checked = false;
        cbTools.Checked = false;
        cbCuttingTools.Checked = false;
        cbNCProgram.Checked = false;
        cbResources.Checked = false;
        cbMaterial.Checked = false;
    }
    #endregion

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                string strTeamID = userInfo["TeamID"];
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);
                para.Add("TeamID", strTeamID);
                para.Add("DispatchType", "1");

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void wgDispatchList_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < wgDispatchList.Rows.Count; i++)
        {
            string strSpecName = wgDispatchList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            wgDispatchList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }

    #region 强制准备完成
    protected void btnSetUp_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            int intCount = 0;
            TemplatedColumn temCell = (TemplatedColumn)ItemGrid.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    intCount++;
                }
            }

            if (intCount == 0)
            {
                DisplayMessage("请选择要强制准备完成的任务", false);
                return;
            }

            for (int i = 0; i < temCell.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
                    string strReceiveEmployeeID = userInfo["EmployeeID"];

                    Dictionary<string, string> para = new Dictionary<string, string>();
                    string strID = ItemGrid.Rows[i].Cells.FromKey("ID").Value.ToString();
                    para.Add("DispatchInfoID", strID);
                    
                    if (cbDocuments.Checked == true)
                    {
                        para.Add("Documents", "1");
                    }
                    else
                    {
                        para.Add("Documents", "0");
                    }

                    if (cbTools.Checked == true)
                    {
                        para.Add("Tools", "1");
                    }
                    else
                    {
                        para.Add("Tools", "0");
                    }

                    if (cbCuttingTools.Checked == true)
                    {
                        para.Add("CuttingTools", "1");
                    }
                    else
                    {
                        para.Add("CuttingTools", "0");
                    }

                    if (cbNCProgram.Checked == true)
                    {
                        para.Add("NCProgram", "1");
                    }
                    else
                    {
                        para.Add("NCProgram", "0");
                    }

                    if (cbResources.Checked == true)
                    {
                        para.Add("Resources", "1");
                    }
                    else
                    {
                        para.Add("Resources", "0");
                    }

                    if (cbMaterial.Checked == true)
                    {
                        para.Add("Material", "1");
                    }
                    else
                    {
                        para.Add("Material", "0");
                    }

                    para.Add("Notes", "");
                    para.Add("SubmitEmployeeID", strReceiveEmployeeID);
                    para.Add("SubmitDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    para.Add("Status", "1");

                    setup.AddMfgSetUpInfo(para);
                }
            }

            QueryData(GetQuery());

            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }
    #endregion
}