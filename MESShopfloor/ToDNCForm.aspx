﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="ToDNCForm.aspx.cs" Inherits="ToDNCForm" EnableViewState="true" %>
<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function IsDel() {
            if (confirm("确定要删除该派工记录吗？")) {
                return true;
            }
            else {
                return false;
            }
        }

        //二维码
        function openQRCodeForm() {
            window.showModalDialog("QRCodeForm.aspx", "", "dialogWidth:500px; dialogHeight:400px; status=no; center: Yes; sc resizable: NO;");
        }

    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="10" class="tdBottom">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">图号/名称：</div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工序：</div>
                        <asp:TextBox ID="txtSpecName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" nowrap="nowrap" class="tdRight">
                        <div class="divLabel">要求完成日期：</div>
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div id="ItemDiv" runat="server">
            <table border="0" cellpadding="5" cellspacing="0">
                <%
                    int intRowCount = dtDispatch.Rows.Count;
                    for (int i = 0; i < intRowCount; i++)
                    {
                        if (i % 2 == 0)
                        {
                            Response.Write("<tr><td>");
                        }
                        else
                        {
                            Response.Write("<td>");
                        }

                        string strID = dtDispatch.Rows[i]["ID"].ToString();
                        string strProductName = dtDispatch.Rows[i]["ProductName"].ToString();
                        string strDescription = dtDispatch.Rows[i]["Description"].ToString();
                        string strSpec = dtDispatch.Rows[i]["SpecName"].ToString();
                        string strSpecNo = common.GetSpecNoFromSpecName(strSpec);
                        string strSpecName = common.GetSpecNameFromSpecName(strSpec);
                        string strResourceName = dtDispatch.Rows[i]["ResourceName"].ToString();
                        string strDispatchNo = dtDispatch.Rows[i]["DispatchInfoName"].ToString();

                        string popupData = strProductName + "," + strDescription + "," + strSpecNo + "," + strSpecName + "," + strResourceName + "," + strDispatchNo;

                        string strImgName = "DNCQRCode/" + CreateQRCodeImg(popupData, strID);
                %>
                <table class="SearchSectionTable" cellpadding="3" cellspacing="0">
                    <tr>
                        <td class="tdLRAndBottom" style="height: 30px;" nowrap="nowrap">产品图号：</td>
                        <td class="tdRightAndBottom" nowrap="nowrap"><%=strProductName %></td>
                        <td class="tdRight" rowspan="6" style="width: 250px;">
                            <img src="<%=strImgName %>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLRAndBottom" style="height: 30px;" nowrap="nowrap">产品名称：</td>
                        <td class="tdRightAndBottom" nowrap="nowrap"><%=strDescription %></td>
                    </tr>
                    <tr>
                        <td class="tdLRAndBottom" style="height: 30px;" nowrap="nowrap">工序号：</td>
                        <td class="tdRightAndBottom" nowrap="nowrap"><%=strSpecNo %></td>
                    </tr>
                    <tr>
                        <td class="tdLRAndBottom" style="height: 30px;" nowrap="nowrap">工序名称：</td>
                        <td class="tdRightAndBottom" nowrap="nowrap"><%=strSpecName %></td>
                    </tr>
                    <tr>
                        <td class="tdLRAndBottom" style="height: 30px;" nowrap="nowrap">设备编号：</td>
                        <td class="tdRightAndBottom" nowrap="nowrap"><%=strResourceName %></td>
                    </tr>
                    <tr>
                        <td class="tdLeftAndRight" style="height: 30px;">&nbsp;</td>
                        <td class="tdRight">&nbsp;</td>
                    </tr>
                </table>
                <%
                        if (i % 2 == 1)
                        {
                            Response.Write("</td>");
                            Response.Write("<td style='width:60px;'>&nbsp;</td></tr>");
                            Response.Write("<tr><td style='height:60px;'>&nbsp;</td></tr>");
                        }
                        else
                        {
                            Response.Write("</td>");
                            Response.Write("<td style='width:60px;'>&nbsp;</td>");
                        }
                    }

                    //for (int i = 0; i < 2 - intRowCount % 2; i++)
                    if (intRowCount % 2 == 1)
                    {
                %>
                <td>
                <table border="0" cellpadding="3" cellspacing="0">
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td rowspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                    </td>
                <td style='width:60px;'>&nbsp;</td></tr>
                <%
                    }
                %>
            </table>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: right;">
                        <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                            OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                        Text="第  页  共  页"></asp:Label>
                        <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                        <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                        <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                        <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                            OnClick="btnGo_Click" />
                        <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                            Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;" Text="1"
                            Visible="False"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div style="display: none;">
            <asp:TextBox ID="txtDispID" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtDispProductName" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtDispDescription" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtDispSpecName" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtDispResourceName" runat="server"></asp:TextBox>
        </div>
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
