﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="WorkReportForm.aspx.cs" Inherits="WorkReportForm" EnableViewState="true" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function IsDel() {
            if (confirm("确定要删除该派工记录吗？")) {
                return true;
            }
            else {
                return false;
            }
        }
        //数据采集
        function openPopUpForm() {
            window.showModalDialog("Custom/bwDataCollect/uMESDataCollectPopupForm.aspx?v="+new Date().getTime(), "", "dialogWidth:980px; dialogHeight:550px; status=no; center: Yes; sc resizable: NO;");
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" colspan="10" class="tdBottom">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">图号/名称：</div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工序：</div>
                        <asp:TextBox ID="txtSpecName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" nowrap="nowrap" class="tdRight">
                        <div class="divLabel">要求完成日期：</div>
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                        -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div id="ItemDiv" runat="server">
            <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="300px" Width="100%" OnActiveRowChange="ItemGrid_ActiveRowChange" OnDataBound="ItemGrid_DataBound">
                <Bands>
                    <igtbl:UltraGridBand>
                        <Columns>
                            <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect" Hidden="true">
                                <CellTemplate>
                                    <asp:CheckBox ID="ckSelect" runat="server" />
                                </CellTemplate>
                                <Header Caption=""></Header>
                            </igtbl:TemplatedColumn>
                            <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                <Header Caption="工作令号">
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="120px" AllowGroupBy="No" Hidden="true">
                                <Header Caption="作业令号">
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="2" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                <Header Caption="批次号">
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="3" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                <Header Caption="图号">
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="4" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                <Header Caption="名称">
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="5" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                <Header Caption="派工数量">
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="6" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="SpecNameDisp" AllowGroupBy="No" BaseColumnName="SpecNameDisp" Width="80px">
                                <Header Caption="派工工序">
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="7"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                             <igtbl:UltraGridColumn BaseColumnName="unitworktime" Key="unitworktime" Width="80px">
                                <Header Caption="单件工时">
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                             <igtbl:UltraGridColumn BaseColumnName="setupworktime" Key="setupworktime" Width="80px">
                                <Header Caption="准备工时">
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="totalworktime" Key="totalworktime" Width="80px" Hidden="true">
                                <Header Caption="工时总和">
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="1" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="TeamName" BaseColumnName="TeamName" Width="120px">
                                <Header Caption="班组">
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Header>

                                <Footer>
                                    <RowLayoutColumnInfo OriginX="8"></RowLayoutColumnInfo>
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn Key="PlannedCompletionDate" AllowGroupBy="No" BaseColumnName="PlannedCompletionDate" Width="120px" DataType="System.DateTime" Format="yyyy-MM-dd">
                                <Header Caption="要求完成日期">
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="9" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ID" Key="ID" Hidden="True">
                                <Header Caption="ID">
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="10" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SpecName" Hidden="True" Key="SpecName">
                                <Header Caption="SpecName">
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="11" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerQty" Hidden="True" Key="ContainerQty">
                                <Header Caption="ContainerQty">
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="12" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerStartDate" Hidden="True" Key="ContainerStartDate">
                                <Header Caption="ContainerStartDate">
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="13" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerCompletionDate" Hidden="True" Key="ContainerCompletionDate">
                                <Header Caption="ContainerCompletionDate">
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="14" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="TeamID" Hidden="True" Key="TeamID">

                                <Header Caption="TeamID">
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="15" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                <Header Caption="ContainerID">
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="16" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Hidden="True" Key="WorkflowID">
                                <Header Caption="WorkflowID">
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="productid" Hidden="True" Key="productid">
                                <Header Caption="productid">
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="contaiernqty" Hidden="True" Key="contaiernqty">
                                <Header Caption="contaiernqty">
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="17" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="SpecID" Hidden="True" Key="SpecID">
                                <Header Caption="SpecID">
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="18" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="ResourceID" Hidden="True" Key="ResourceID">
                                <Header Caption="ResourceID">
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="mfgordername" Hidden="True" Key="mfgordername">
                                <Header Caption="mfgordername">
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                             <igtbl:UltraGridColumn BaseColumnName="mfgorderid" Hidden="True" Key="mfgorderid">
                                <Header Caption="mfgorderid">
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                            <igtbl:UltraGridColumn BaseColumnName="workflowstepid" Hidden="True" Key="WorkflowStepID">
                                <Header Caption="workflowstepid">
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Header>
                                <Footer>
                                    <RowLayoutColumnInfo OriginX="19" />
                                </Footer>
                            </igtbl:UltraGridColumn>
                        </Columns>
                        <AddNewRow View="NotSet" Visible="NotSet">
                        </AddNewRow>
                    </igtbl:UltraGridBand>
                </Bands>
                <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                    BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                    SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                    TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                    CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                    RowHeightDefault="18px" AllowRowNumberingDefault="Continuous">
                    <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="100px" Width="100%">
                    </FrameStyle>
                    <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                    </RowAlternateStyleDefault>
                    <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                        StyleMode="QuickPages">
                        <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </PagerStyle>
                    </Pager>
                    <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                    </EditCellStyleDefault>
                    <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </FooterStyleDefault>
                    <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                        CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        <Padding Bottom="3px" Top="2px" />
                        <Padding Top="2px" Bottom="3px"></Padding>
                    </HeaderStyleDefault>
                    <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                        <Padding Left="3px" />
                    </RowSelectorStyleDefault>
                    <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                        <Padding Left="3px" />
                        <BorderDetails ColorLeft="Window" ColorTop="Window" />
                    </RowStyleDefault>
                    <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                    </GroupByRowStyleDefault>
                    <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                    </SelectedRowStyleDefault>
                    <GroupByBox Hidden="True">
                        <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                        </BoxStyle>
                    </GroupByBox>
                    <AddNewBox>
                        <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </BoxStyle>
                    </AddNewBox>
                    <ActivationObject BorderColor="" BorderWidth="">
                    </ActivationObject>
                    <FilterOptionsDefault FilterUIType="HeaderIcons">
                        <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                            CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px" Height="420px" Width="200px">
                            <Padding Left="2px" />
                        </FilterDropDownStyle>
                        <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                        </FilterHighlightRowStyle>
                        <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                            Font-Size="11px">
                            <Padding Left="2px" />
                        </FilterOperandDropDownStyle>
                    </FilterOptionsDefault>
                </DisplayLayout>
            </igtbl:UltraWebGrid>
            <div style="text-align: right; float: right">
                <uPT:pageTurning ID="upageTurning" runat="server" />
            </div>
        </div>
        <%--<div style="height: 8px; width: 100%;"></div>--%>

        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtDispProcessNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom" style=" display:none">
                        <div class="divLabel">作业令号：</div>
                        <asp:TextBox ID="txtDispOprNo" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtStartTime" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtEndTime" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtDispContainerName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispContainerID" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtContaiernQty" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRightAndBottom">
                        <div class="divLabel">图号：</div>
                        <asp:TextBox ID="txtDispProductName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtMfgorderName" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdBottom" colspan="2">
                        <div class="divLabel">名称：</div>
                        <asp:TextBox ID="txtDispDescription" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" class="tdRight">
                        <div class="divLabel">派工数量：</div>
                        <asp:TextBox ID="txtDispQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">工序：</div>
                        <asp:TextBox ID="txtDispSpecName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtProductID" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtDispWorkflowID" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtDispSpecID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">班组：</div>
                        <asp:TextBox ID="txtDispTeamName" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                        <asp:TextBox ID="txtDispTeamID" runat="server" Visible="false"></asp:TextBox>
                    </td>
                    <td align="left" class="tdRight">
                        <div class="divLabel">要求完成日期：</div>
                        <asp:TextBox ID="txtDispPlannedCompletionDate" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td align="left" class="tdNoBorder">
                        <div class="divLabel">已报工数量：</div>
                        <asp:TextBox ID="txtDispID" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtReportedQty" runat="server" class="stdTextBox" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>

        <div style="height: 8px; width: 100%;"></div>

        <div>
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 435px;" valign="top" rowspan="3">
                        <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                                    <div class="divLabel">设备/工位：</div>
                                    <asp:DropDownList ID="ddlResource" runat="server" Width="155px" Height="28px"
                                        Style="font-size: 16px;">
                                    </asp:DropDownList>
                                </td>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap" colspan="2">
                                    <asp:CheckBox ID="cbReportType" runat="server" Text="首件报工" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                                    <div class="divLabel">数量：</div>
                                    <asp:TextBox ID="txtQty" runat="server" class="stdTextBox"></asp:TextBox>
                                </td>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap">
                                    <div class="divLabel">互检人：</div>
                                    <asp:DropDownList ID="ddlMutualChecker" runat="server" Width="155px" Height="28px"
                                        Style="font-size: 16px;">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdRightAndBottom" align="left" nowrap="nowrap" colspan="3">
                                    <div class="divLabel">附件：</div>
                                    <asp:FileUpload ID="fileUpload" runat="server" Width="360px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="tdRight" align="left" nowrap="nowrap" colspan="3">
                                    <div class="divLabel">备注：</div>
                                    <asp:TextBox ID="txtNotes" runat="server" class="stdTextBox" Height="50px" Width="360px" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td>
                        <igtbl:UltraWebGrid ID="wgEmployee" runat="server" Height="200px" Width="330px">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn Width="30px" AllowGroupBy="No" Key="ckSelect">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption=""></Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn Key="FullName" Width="100px" BaseColumnName="FullName" AllowGroupBy="No">
                                            <Header Caption="人员">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn Key="WorkHour" Width="60px" BaseColumnName="WorkHour" AllowGroupBy="No" AllowUpdate="Yes" DataType="System.Int32">
                                            <Header Caption="工时(时)">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                         <igtbl:UltraGridColumn Key="WorkMin" Width="60px" BaseColumnName="WorkMin" AllowGroupBy="No" AllowUpdate="Yes" DataType="System.Int32">
                                            <Header Caption="工时(分)">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="EmployeeName" Key="EmployeeName" Hidden="True">
                                            <Header Caption="EmployeeID">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="EmployeeID" Key="EmployeeID" Hidden="True">
                                            <Header Caption="EmployeeName">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="330px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td id="tdproductNo" runat="server" style="display:none">
                        <igtbl:UltraWebGrid ID="wgProductNo" runat="server" Height="250px" Width="200px">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <Columns>
                                        <igtbl:TemplatedColumn Width="30px" AllowGroupBy="No" Key="ckSelect">
                                            <CellTemplate>
                                                <asp:CheckBox ID="ckSelect" runat="server" />
                                            </CellTemplate>
                                            <Header Caption=""></Header>
                                        </igtbl:TemplatedColumn>
                                        <igtbl:UltraGridColumn Key="ProductNo" Width="100px" BaseColumnName="ProductNo" AllowGroupBy="No">
                                            <Header Caption="产品序号">
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Header>

                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Hidden="True">
                                            <Header Caption="ContainerName">
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="2" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                        <igtbl:UltraGridColumn BaseColumnName="ContainerID" Key="ContainerID" Hidden="True">
                                            <Header Caption="ContainerID">
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Header>
                                            <Footer>
                                                <RowLayoutColumnInfo OriginX="1" />
                                            </Footer>
                                        </igtbl:UltraGridColumn>
                                    </Columns>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="18px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="200px" Width="200px">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                    <td style="width: 8px;">&nbsp;&nbsp;</td>
                    <td valign="top" style="width: 100%;" align="left"></td>
                </tr>
               <%-- <tr>
                    <td style="height: 8px;" colspan="6">&nbsp;</td>
                </tr>--%>
                <tr>
                    <td></td>
                    <td align="left">
                        <asp:Button ID="btnSelectAll2" runat="server" Text="全选" OnClick="btnSelectAll2_Click" />&nbsp;
                    <asp:Button ID="btnReverseSelect2" runat="server" Text="反选" OnClick="btnReverseSelect2_Click" />
                    </td>
                    <td></td>
                    <td align="left" style="display:none">
                        <asp:Button ID="btnSelectAll" runat="server" Text="全选" OnClick="btnSelectAll_Click" />&nbsp;
                    <asp:Button ID="btnReverseSelect" runat="server" Text="反选" OnClick="btnReverseSelect_Click" />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;">
                        <asp:Button ID="btnSave" runat="server" Text="保存" UseSubmitBehavior="false" OnClientClick="disable_btn(this.id)" Style="margin-right: 20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSave_Click" />
                        <asp:Button ID="btnDataCol" runat="server" Text="数据采集" Style="margin-right: 20px"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnDataCol_Click" />
                        <asp:Button ID="Button1" runat="server" Text="图纸工艺查看"
                            CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hdScanCon" runat="server" />
    </igmisc:WebAsyncRefreshPanel>

</asp:Content>
