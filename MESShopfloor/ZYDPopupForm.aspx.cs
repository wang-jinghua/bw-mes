﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Data;
using System.Drawing;
using Infragistics.WebUI.UltraWebGrid;

public partial class ZYDPopupForm : System.Web.UI.Page
{
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESRejectAppBusiness rejectapp = new uMESRejectAppBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Dictionary<string, object> popupData = (Dictionary<string, object>)Session["PopupData"];
            txtDispContainerID.Text = popupData["ContainerID"].ToString();
            txtDispProcessNo.Text = popupData["ProcessNo"].ToString();
            txtDispOprNo.Text = popupData["OprNo"].ToString();
            txtDispContainerName.Text = popupData["ContainerName"].ToString();
            txtDispProductName.Text = popupData["ProductName"].ToString();
            txtDispDescription.Text = popupData["Description"].ToString();
            txtDispContainerQty.Text = popupData["ContainerQty".ToString()].ToString();
            txtDispSpecID.Text = popupData["SpecID"].ToString();
            txtDispPlannedStartDate.Text = popupData["PlannedStartDate"].ToString();
            txtDispPlannedCompletionDate.Text = popupData["PlannedCompletionDate"].ToString();
            txtID.Text = popupData["ID"].ToString();
            txtChildCount.Text = popupData["ChildCount"].ToString();
            txtDispWorkflowID.Text = popupData["WorkflowID"].ToString();

            DataTable dtProductNo = (DataTable)(popupData["dtProductNo"]);
            wgProductNoList.DataSource = dtProductNo;
            wgProductNoList.DataBind();

            Session["PopupData"] = null;
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }
    #endregion

    #region 提交按钮
    protected void btnMaterialApp_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {
            //数量和产品序号
            int intQty = 0;
            DataTable dtProductNo = new DataTable();
            dtProductNo.Columns.Add("ID");
            dtProductNo.Columns.Add("ContainerID");
            dtProductNo.Columns.Add("ContainerName");
            dtProductNo.Columns.Add("ProductNo");
            dtProductNo.Columns.Add("Notes");

            TemplatedColumn temCell1 = (TemplatedColumn)wgProductNoList.Columns.FromKey("ckSelect");

            for (int i = 0; i < temCell1.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem1 = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell1.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem1.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    intQty += Convert.ToInt32(wgProductNoList.Rows[i].Cells.FromKey("Qty").Value.ToString());

                    DataRow row = dtProductNo.NewRow();

                    row["ID"] = wgProductNoList.Rows[i].Cells.FromKey("ID").Value.ToString();

                    if (wgProductNoList.Rows[i].Cells.FromKey("ContainerID").Value != null)
                    {
                        row["ContainerID"] = wgProductNoList.Rows[i].Cells.FromKey("ContainerID").Value.ToString();
                    }
                    else
                    {
                        row["ContainerID"] = string.Empty;
                    }

                    if (wgProductNoList.Rows[i].Cells.FromKey("ContainerName").Value != null)
                    {
                        row["ContainerName"] = wgProductNoList.Rows[i].Cells.FromKey("ContainerName").Value.ToString();
                    }
                    else
                    {
                        row["ContainerName"] = string.Empty;
                    }

                    if (wgProductNoList.Rows[i].Cells.FromKey("ProductNo").Value != null)
                    {
                        row["ProductNo"] = wgProductNoList.Rows[i].Cells.FromKey("ProductNo").Value.ToString();
                    }
                    else
                    {
                        row["ProductNo"] = string.Empty;
                    }

                    row["Notes"] = string.Empty;

                    dtProductNo.Rows.Add(row);
                }
            }

            //添加质疑单记录
            //string strQuestionInfoName = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string strQuestionInfoName = txtQuestionInfoName.Text.Trim();
            string strContainerID = txtDispContainerID.Text;
            string strSpecID = txtDispSpecID.Text;
            string strRejectAppInfoID = txtID.Text;
            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strOprEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("QuestionInfoName", strQuestionInfoName);
            para.Add("RejectAppInfoID", strRejectAppInfoID);
            para.Add("Qty", intQty.ToString());
            para.Add("UomID", "");
            para.Add("OprEmployeeID", strOprEmployeeID);
            para.Add("OprDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            para.Add("Notes", txtNotes.Text.Trim());

            rejectapp.AddQuestionInfo(para, dtProductNo);

            Response.Write("<script>parent.window.returnValue='1'; window.close()</script>");
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
    #endregion
}