﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Drawing;
using System.Configuration;
using System.Web.UI;


public partial class uMESContainerSelectPopupForm : System.Web.UI.Page
{
    const string QueryWhere = "uMESSelectContainerInfoPopUpForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESProblemBusiness problem = new uMESProblemBusiness();
    int m_PageSize = 11;

    protected void Page_Load(object sender, EventArgs e)
    {
        upageTurning.PageIndexChanged += new pageTurning.PageIndexChangedEventHandler(() => { QueryData(upageTurning.CurrentPageIndex); });

        //WebPanel = WebAsyncRefreshPanel1;


        if (!IsPostBack)
        {
            // ClearMessage_PageLoad();
        }
    }

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
    }
    #endregion

    #region 数据查询
    public Dictionary<string, string> GetQuery()
    {
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        Session[QueryWhere] = result;

        return result;
    }

    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }

    public void QueryData(int CurrentPageIndex)
    {
        ShowStatusMessage("", true);

        try
        {
            uMESPagingDataDTO result = bll.GetSourceData(GetQuery(), CurrentPageIndex, m_PageSize);
            ItemGrid.DataSource = result.DBTable;
            ItemGrid.DataBind();

            //给分页控件赋值，用于分页控件信息显示
            upageTurning.TotalRowCount = int.Parse(result.RowCount);
            upageTurning.RowCountByPage = m_PageSize;

        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }

    public void ResetQuery()
    {
        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();


    }
    #endregion


    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {

            }
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            for (int i = 0; i < ItemGrid.Rows.Count; i++)
            {
                if (ItemGrid.Rows[i].Activated == true)
                {
                    txtContainerID.Text = ItemGrid.Rows[i].Cells.FromKey("ContainerID").Value.ToString();
                    break;
                }
            }
            if (txtContainerID.Text == null || txtContainerID.Text == "")
            {
                ShowStatusMessage("请选择批次信息！", false);
                return;
            }
 
            Infragistics.WebUI.Shared.CallBackManager.AddScriptBlock(Page, WebAsyncRefreshPanel1, "<script> parent.window.returnValue='" + txtContainerID.Text + "' ; window.close();</script>");
           // Response.Write("<script> parent.window.returnValue='" + txtContainerID.Text + "' ; window.close();</script>");
        }
        catch
        {
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            QueryData(1);
        }
        catch
        {
        }
    }
}