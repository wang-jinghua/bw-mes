﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="ContainerPlatoonPopupForm.aspx.cs" Inherits="ContainerPlatoonPopupForm" EnableViewState="true" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebNavigator.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.UltraWebNavigator" TagPrefix="ignav" %>

<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>

<%@ Register Src="~/uMESCustomControls/ProductInfo/GetProductInfo.ascx" TagName="getProductInfo"
    TagPrefix="gPI" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server" Style="margin-top: -15px">
        <div style="width: 225px; float: left; margin-right: 10px">
            <div>

                <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%" style="table-layout: fixed;">
                    <colgroup>
                        <col width="auto">
                        <col width="50">
                    </colgroup>
                    <tr>
                        <td align="left" class="tdRightAndBottom" colspan="1">
                            <div class="divLabel">工作令号：</div>
                            <div style="height: 28px; padding-right: 2px">
                                <asp:TextBox ID="txtTreeProcessNo" runat="server" class="stdTextBoxFull"></asp:TextBox>
                            </div>
                        </td>


                        <td class="tdRightAndBottom" align="right" valign="bottom" colspan="1">
                            <asp:Button ID="btnTreeSearch" runat="server" Text="查询"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnTreeSearch_Click" />
                            <asp:Button ID="btnTreeReset" runat="server" Text="重置" Visible="false"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnTreeReset_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdRightAndBottom" colspan="2" style="text-align: left">
                            <%--<igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel2" runat="server" Width="100%">--%>
                            <div>产品</div>
                            <gPI:getProductInfo ID="getProductInfo" runat="server" />
                            <%--</igmisc:WebAsyncRefreshPanel>--%>
                        </td>

                    </tr>
                </table>

            </div>
            <ignav:UltraWebTree ID="treeProduct" runat="server" Height="510px" Width="220px" Style="border: 1px solid #333; margin-top: 3px"
                WebTreeTarget="ClassicTree" OnNodeSelectionChanged="treeProduct_NodeSelectionChanged">
                  <SelectedNodeStyle BackColor="#66CCFF" />
                <AutoPostBackFlags NodeChanged="True" NodeChecked="False" NodeCollapsed="False" NodeDropped="False" NodeExpanded="False" />
            </ignav:UltraWebTree>

        </div>


        <div style="height: 100%">

            <div>
                <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%" style="table-layout: fixed; border-bottom: none">
                    <colgroup>
                        <col width="auto">
                        <col width="auto">
                        <col width="auto">
                        <col width="270">
                        <col width="60">
                    </colgroup>
                    <tr style="display:none">
                        <td align="left" colspan="5" class="tdLRAndBottom">
                            <div class="ScanLabel">扫描：</div>
                            <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdLRAndBottom">
                            <div class="divLabel">工作令号：</div>
                            <div style="height: 28px; padding-right: 2px">
                                <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                            </div>
                        </td>
                        <td align="left" class="tdRightAndBottom">
                            <div class="divLabel">批次号：</div>
                            <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBoxFull"></asp:TextBox>
                        </td>
                        <td align="left" class="tdRightAndBottom">
                            <div class="divLabel">图号/名称：</div>
                            <div style="height: 28px; padding-right: 2px">
                                <asp:TextBox ID="txtProductName" runat="server" class="stdTextBoxFull" Enabled="true"></asp:TextBox>
                            </div>
                        </td>
                        <td align="left" nowrap="nowrap" class="tdRightAndBottom">
                            <div class="divLabel">计划开始日期：</div>
                            <div style="height: 28px; padding-right: 2px">
                                <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" style="width: 110px" type="text" />
                                -
                    <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" style="width: 110px" type="text" />
                            </div>
                        </td>
                        <td class="tdRightAndBottom" style="text-align: left;" nowrap="nowrap">
                            <asp:Button ID="btnSearch" runat="server" Text="查询"
                                CssClass="searchButton" EnableTheming="True" />
                            <asp:Button ID="btnReSet" runat="server" Text="重置" Visible="false"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdLRAndBottom" style="display: none">
                            <div class="divLabel">工作令号：</div>
                            <div style="height: 28px; padding-right: 2px">
                                <asp:TextBox ID="txtSrTreeProcessNo" runat="server" class="stdTextBoxFull" Enabled="false"></asp:TextBox>
                            </div>
                        </td>
                        <td align="left" class="tdLRAndBottom">
                            <div class="divLabel">所选图号：</div>
                            <div style="height: 28px; padding-right: 2px">
                                <asp:TextBox ID="txtSrTreeProduct" runat="server" class="stdTextBoxFull" Enabled="false"></asp:TextBox>
                            </div>
                        </td>
                        <td class="tdRightAndBottom" nowrap="nowrap" colspan="4" valign="bottom" align="left">
                            <asp:Button ID="btnSearchAll" runat="server" Text="查询该产品及往下所有" OnClientClick=" return ((confirm('查询往下所有零组件，数据量大效率低，确认继续？') == true) ? true :  false)"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnSearchAll_Click" />
                            <asp:Button ID="Button5" runat="server" Text="重置"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="padding-left: 5px;" colspan="5">
                            <table style="width: 400px">
                                <tr>
                                    <td style="width: 50px">
                                        <asp:TextBox ID="TextBox17" runat="server" BackColor="White" BorderStyle="None"
                                            ReadOnly="True" Style="text-align: center; vertical-align: middle;"
                                            Font-Bold="True" Width="56px">图例：</asp:TextBox>
                                    </td>
                                    <td style="width: 20px">
                                        <asp:TextBox ID="TextBox14" runat="server" BackColor="LightSkyBlue" BorderStyle="None"
                                            ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                    </td>
                                    <td style="width: 50px">
                                        <asp:TextBox ID="TextBox4" runat="server" BackColor="White" BorderStyle="None"
                                            ReadOnly="True" Width="90px">未审核</asp:TextBox>
                                    </td>
                                    <td style="width: 50px;display:none">
                                        <asp:TextBox ID="TextBox11" runat="server" BackColor="LightGreen" BorderStyle="None"
                                            ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                    </td>
                                    <td style="width: 50px;display:none" >
                                        <asp:TextBox ID="TextBox10" runat="server" BackColor="White" BorderStyle="None"
                                            ReadOnly="True" Width="90px">审核通过</asp:TextBox>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:TextBox ID="TextBox2" runat="server" BackColor="LightYellow" BorderStyle="None"
                                            ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                    </td>

                                    <td style="width: 50px">
                                        <asp:TextBox ID="TextBox7" runat="server" BackColor="White" BorderStyle="None"
                                            ReadOnly="True" Width="90px">审核未通过</asp:TextBox>
                                    </td>
                                    <td style="width: 50px;display:none">
                                        <asp:TextBox ID="TextBox13" runat="server" BackColor="LightSeaGreen" BorderStyle="None"
                                            ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                    </td>
                                    <td style="width: 50px;display:none">
                                        <asp:TextBox ID="TextBox12" runat="server" BackColor="White" BorderStyle="None"
                                            ReadOnly="True" Width="90px">已下发</asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <%--        <div style="height: 8px; width: 100%;"></div>--%>

            <div id="ItemDiv" runat="server">
                <igtbl:UltraWebGrid ID="ItemGrid" runat="server" Height="340px" Width="100%" OnDataBound="ItemGrid_DataBound">
                    <Bands>
                        <igtbl:UltraGridBand>
                            <Columns>
                                <igtbl:TemplatedColumn Width="40px" AllowGroupBy="No" Key="ckSelect">
                                    <CellTemplate>
                                        <asp:CheckBox ID="ckSelect" runat="server" />
                                    </CellTemplate>
                                    <Header Caption=""></Header>
                                </igtbl:TemplatedColumn>
                                <igtbl:UltraGridColumn Key="IsAPSDisplay" Width="100px" BaseColumnName="IsAPSDisplay" AllowGroupBy="No">
                                    <Header Caption="是否已派工">
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="state" Width="40px" BaseColumnName="state" AllowGroupBy="No">
                                    <Header Caption="状态">
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Key="ProcessNo" Width="150px" BaseColumnName="ProcessNo" AllowGroupBy="No">
                                    <Header Caption="工作令号">
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Header>

                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="OprNo" Key="OprNo" Width="150px" AllowGroupBy="No" Hidden="true">
                                    <Header Caption="作业令号">
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="2" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" Width="150px" AllowGroupBy="No">
                                    <Header Caption="批次号">
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="3" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductName" Key="ProductName" Width="150px" AllowGroupBy="No">
                                    <Header Caption="图号">
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="4" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="Description" Key="Description" Width="150px" AllowGroupBy="No">
                                    <Header Caption="名称">
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="5" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="Qty" Key="Qty" Width="80px" AllowGroupBy="No">
                                    <Header Caption="数量">
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="6" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="PlannedStartDate" Key="PlannedStartDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                    <Header Caption="计划开始日期">
                                        <RowLayoutColumnInfo OriginX="7" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="7" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="PlannedCompletionDate" Key="PlannedCompletionDate" Width="120px" AllowGroupBy="No" DataType="System.DateTime" Format="yyyy-MM-dd">
                                    <Header Caption="计划完成日期">
                                        <RowLayoutColumnInfo OriginX="8" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="8" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ContainerID" Hidden="True" Key="ContainerID">
                                    <Header Caption="ContainerID">
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="9" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="WorkflowID" Hidden="True" Key="WorkflowID">
                                    <Header Caption="WorkflowID">
                                        <RowLayoutColumnInfo OriginX="10" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="10" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="MaterialName" Hidden="True" Key="MaterialName">
                                    <Header Caption="MaterialName">
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="11" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="MaterialPaiHao" Hidden="True" Key="MaterialPaiHao">
                                    <Header Caption="MaterialPaiHao">
                                        <RowLayoutColumnInfo OriginX="12" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="12" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="MaterialGuiGe" Hidden="True" Key="MaterialGuiGe">
                                    <Header Caption="MaterialGuiGe">
                                        <RowLayoutColumnInfo OriginX="13" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="13" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="IsAPS" Hidden="True" Key="IsAPS">
                                    <Header Caption="IsAPS">
                                        <RowLayoutColumnInfo OriginX="14" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="14" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="Sequence" Hidden="True" Key="Sequence">
                                    <Header Caption="Sequence">
                                        <RowLayoutColumnInfo OriginX="15" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="15" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ProductID" Hidden="True" Key="ProductID">
                                    <Header Caption="ProductID">
                                        <RowLayoutColumnInfo OriginX="17" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="17" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn BaseColumnName="ContainerComments" Key="ContainerComments" Width="150px" AllowGroupBy="No">
                                    <Header Caption="审核意见">
                                        <RowLayoutColumnInfo OriginX="18" />
                                    </Header>
                                    <Footer>
                                        <RowLayoutColumnInfo OriginX="18" />
                                    </Footer>
                                </igtbl:UltraGridColumn>
                            </Columns>
                            <AddNewRow View="NotSet" Visible="NotSet">
                            </AddNewRow>
                        </igtbl:UltraGridBand>
                    </Bands>
                    <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                        BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                        SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                        TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False"
                        CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                        RowHeightDefault="18px">
                        <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="450px"
                            Width="100%">
                        </FrameStyle>
                        <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                        </RowAlternateStyleDefault>
                        <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                            StyleMode="QuickPages">
                            <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </PagerStyle>
                        </Pager>
                        <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                        </EditCellStyleDefault>
                        <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                        </FooterStyleDefault>
                        <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                            CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                            <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            <Padding Bottom="3px" Top="2px" />
                            <Padding Top="2px" Bottom="3px"></Padding>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                            <Padding Left="3px" />
                        </RowSelectorStyleDefault>
                        <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                            <Padding Left="3px" />
                            <BorderDetails ColorLeft="Window" ColorTop="Window" />
                        </RowStyleDefault>
                        <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                        </GroupByRowStyleDefault>
                        <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                        </SelectedRowStyleDefault>
                        <GroupByBox Hidden="True">
                            <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                            </BoxStyle>
                        </GroupByBox>
                        <AddNewBox>
                            <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                            </BoxStyle>
                        </AddNewBox>
                        <ActivationObject BorderColor="" BorderWidth="">
                        </ActivationObject>
                        <FilterOptionsDefault FilterUIType="HeaderIcons">
                            <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px" Height="420px" Width="200px">
                                <Padding Left="2px" />
                            </FilterDropDownStyle>
                            <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                            </FilterHighlightRowStyle>
                            <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                Font-Size="11px">
                                <Padding Left="2px" />
                            </FilterOperandDropDownStyle>
                        </FilterOptionsDefault>
                    </DisplayLayout>
                </igtbl:UltraWebGrid>
                <div>
                    <div style="float:left">
                         <asp:Button ID="btnSelectAll" runat="server" Text="全选"
                            CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnSelectAll_Click"  />
                        <asp:Button ID="btnInRevert" runat="server" Text="反选"
                                    CssClass="searchButton" EnableTheming="True" style="height: 26px" OnClick="btnInRevert_Click" />

                    </div>
                    <div style="float:right">
                         <asp:LinkButton ID="lbtnFirst" runat="server" Style="z-index: 200; font-size: 13px;"
                                OnClick="lbtnFirst_Click">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnPrev" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnPrev_Click">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnNext" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnNext_Click">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID="lbtnLast" runat="server" Style="z-index: 200; font-size: 13px;"
                        OnClick="lbtnLast_Click">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID="lLabel1" runat="server" Style="z-index: 200; font-size: 13px;" ForeColor="red"
                        Text="第  页  共  页"></asp:Label>
                            <asp:Label ID="lLabel2" runat="server" Style="z-index: 200; font-size: 13px;" Text="转到第"></asp:Label>
                            <asp:TextBox ID="txtPage" runat="server" Style="width: 30px;" class="ReportTextBox"></asp:TextBox>
                            <asp:Label ID="lLabel3" runat="server" Style="z-index: 200; font-size: 13px;" Text="页"></asp:Label>
                            <asp:Button ID="btnGo" runat="server" Style="z-index: 200;" Text="Go" CssClass="ReportButton"
                                OnClick="btnGo_Click" />
                            <asp:TextBox ID="txtTotalPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtCurrentPage" runat="server" Style="z-index: 200; width: 30px;"
                                Visible="False"></asp:TextBox>
                    </div>
                </div>
            </div>
            
            <div style="clear:both"></div>
                        
            <div style="width: 95%;float:left;text-align:left;margin-top:5px">
             <asp:Button ID="btnAPS" runat="server" Text="预派工"
                                CssClass="searchButton" EnableTheming="True" OnClick="btnAPS_Click" />
                            <asp:Button ID="Button1" runat="server" Text="计划延迟一天" Visible="false"
                                CssClass="searchButton" EnableTheming="True" OnClick="Button1_Click" />
                            <asp:Button ID="Button2" runat="server" Text="计划延迟一周" Visible="false"
                                CssClass="searchButton" EnableTheming="True" OnClick="Button2_Click" />
                            <asp:Button ID="Button3" runat="server" Text="计划延迟一月" Visible="false"
                                CssClass="searchButton" EnableTheming="True" OnClick="Button3_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;计划
                        <asp:DropDownList ID="ddlTY" runat="server" Width="70px" Height="28px" Style="font-size: 16px;">
                            <asp:ListItem Text="延迟" Value="1"></asp:ListItem>
                            <asp:ListItem Text="提前" Value="-1"></asp:ListItem>
                        </asp:DropDownList>
                            <asp:TextBox ID="txtTS" runat="server" Width="40px" class="stdTextBox"></asp:TextBox>
                            天
                        <asp:Button ID="btnOK" runat="server" Text="确定" CssClass="searchButton" OnClick="btnOK_Click" />
                            <asp:Button ID="Button4" runat="server" Text="图纸工艺查看"
                                CssClass="searchButton" EnableTheming="True" OnClick="Button4_Click" />
        </div>
        </div>

    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
