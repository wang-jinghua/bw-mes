﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;

public partial class TeamDispatchForm : ShopfloorPage, INormalReport
{
    const string QueryWhere = "TeamDispatchForm";
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESContainerPrintBusiness bll = new uMESContainerPrintBusiness();
    uMESDispatchBusiness dispatch = new uMESDispatchBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        uMESMasterPage master = this.Master as uMESMasterPage;
        master.strNavigation = "当前位置：班组派工";
        master.strTitle = "班组派工";
        master.ChangeFrame(true);
        NormalReportControl normalCntrl = new NormalReportControl();
        normalCntrl.LtnFirst = lbtnFirst;
        normalCntrl.LtnLast = lbtnLast;
        normalCntrl.LtnNext = lbtnNext;
        normalCntrl.LtnPrev = lbtnPrev;
        normalCntrl.BtnReset = btnReSet;
        normalCntrl.BtnGo = btnGo;
        normalCntrl.BtnSearch = btnSearch;
        normalCntrl.LabPages = lLabel1;
        normalCntrl.TxtPage = txtPage;
        normalCntrl.TxtTotalPage = txtTotalPage;
        normalCntrl.TxtCurrentPage = txtCurrentPage;
        normalCntrl.NormalOperation = this;
        normalCntrl.QueryWhere = QueryWhere;

        WebPanel = WebAsyncRefreshPanel1;

        if (!IsPostBack)
        {
            ClearMessage_PageLoad();
        }
    }

    #region 显示班组当前已派工的任务
    protected void ddlTeam_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            DropDownList ddlTeam = (DropDownList)sender;
            string strTeamID = ddlTeam.SelectedValue;
            string strTeamIDTemp = txtTeamID.Text;

            if (strTeamID != strTeamIDTemp)
            {
                txtTeamID.Text = strTeamID;
                DataTable DT = dispatch.GetTeamDispatchInfo(strTeamID);

                wgDispatchList.DataSource = DT;
                wgDispatchList.DataBind();
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    protected void wgDispatchList_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < wgDispatchList.Rows.Count; i++)
        {
            string strSpecName = wgDispatchList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            wgDispatchList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);
        }
    }
    #endregion

    #region 获取班组列表
    protected DataTable GetTeam()
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        string strFactoryID = userInfo["FactoryID"];
        return common.GetTeam(strFactoryID);
    }
    #endregion

    #region 数据查询

    #region 重置
    protected void btnReSet_Click(object sender, EventArgs e)
    {
        ClearDispData();
    }
    #endregion
    public Dictionary<string, string> GetQuery()
    {
        string strScanContainerName = txtScan.Text.Trim();
        string strProcessNo = txtProcessNo.Text.Trim();
        string strContainerName = txtContainerName.Text.Trim();
        string strProductName = txtProductName.Text.Trim();
        string strStartDate = txtStartDate.Value.Trim();
        string strEndDate = txtEndDate.Value.Trim();

        Dictionary<string, string> result = new Dictionary<string, string>();
        result.Add("ScanContainerName", strScanContainerName);
        result.Add("ProcessNo", strProcessNo);
        result.Add("ContainerName", strContainerName);
        result.Add("ProductName", strProductName);
        result.Add("StartDate", strStartDate);
        result.Add("EndDate", strEndDate);

        Session[QueryWhere] = result;

        return result;
    }

    protected void txtScan_TextChanged(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            string strScan = txtScan.Text.Trim();
            txtScan.Text = "";

            if (strScan != string.Empty)
            {
                Dictionary<string, string> para = new Dictionary<string, string>();
                para.Add("ScanContainerName", strScan);

                Session[QueryWhere] = para;

                txtCurrentPage.Text = "1";
                QueryData(para);
            }
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    public void QueryData(Dictionary<string, string> query)
    {
        ClearMessage();

        uMESPagingDataDTO result = bll.GetSourceData(query, Convert.ToInt32(this.txtCurrentPage.Text), 10);
        this.ItemGrid.DataSource = result.DBTable;
        this.ItemGrid.DataBind();
        this.txtTotalPage.Text = result.PageCount;
        if (result.RowCount == "0")
        {
            this.txtCurrentPage.Text = "0";
        }
        lLabel1.Text = string.Format("第 {0} 页  共 {1} 页", this.txtCurrentPage.Text, this.txtTotalPage.Text);
        this.txtPage.Text = this.txtCurrentPage.Text;

        ClearDispData();
    }

    protected void ClearDispData()
    {
        txtDispProcessNo.Text = string.Empty;
        txtDispOprNo.Text = string.Empty;
        txtDispContainerName.Text = string.Empty;
        txtDispContainerID.Text = string.Empty;
        txtDispWorkflowID.Text = string.Empty;
        txtDispProductName.Text = string.Empty;
        txtDispDescription.Text = string.Empty;
        txtDispQty.Text = string.Empty;
        txtDispPlannedStartDate.Text = string.Empty;
        txtDispPlannedCompletionDate.Text = string.Empty;

        wgSpecList.Clear();
        txtTeamID.Text = string.Empty;
        wgDispatchList.Clear();
    }

    public void ResetQuery()
    {
        ClearMessage();

        Session[QueryWhere] = "";
        txtScan.Text = string.Empty;
        txtProcessNo.Text = string.Empty;
        txtContainerName.Text = string.Empty;
        txtProductName.Text = string.Empty;
        txtStartDate.Value = string.Empty;
        txtEndDate.Value = string.Empty;
        ItemGrid.Rows.Clear();

        this.txtTotalPage.Text = "";
        this.txtCurrentPage.Text = "";
        this.txtPage.Text = "";
        lLabel1.Text = "第  页  共  页";
    }
    #endregion

    #region 分页按钮
    protected void lbtnFirst_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnPrev_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnNext_Click(object sender, EventArgs e)
    {

    }
    protected void lbtnLast_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    #endregion

    #region 选中批次行
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        ClearMessage();

        try
        {
            txtDispProcessNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("ProcessNo").Value != null)
            {
                string strProcessNo = e.Row.Cells.FromKey("ProcessNo").Value.ToString();
                txtDispProcessNo.Text = strProcessNo;
            }

            txtDispOprNo.Text = string.Empty;
            if (e.Row.Cells.FromKey("OprNo").Value != null)
            {
                string strOprNo = e.Row.Cells.FromKey("OprNo").Value.ToString();
                txtDispOprNo.Text = strOprNo;
            }

            string strContainerName = e.Row.Cells.FromKey("ContainerName").Value.ToString();
            txtDispContainerName.Text = strContainerName;

            string strContainerID = e.Row.Cells.FromKey("ContainerID").Value.ToString();
            txtDispContainerID.Text = strContainerID;

            string strWorkflowID = e.Row.Cells.FromKey("WorkflowID").Value.ToString();
            txtDispWorkflowID.Text = strWorkflowID;

            string strProductName = e.Row.Cells.FromKey("ProductName").Value.ToString();
            txtDispProductName.Text = strProductName;

            txtDispDescription.Text = string.Empty;
            if (e.Row.Cells.FromKey("Description").Value != null)
            {
                string strDescription = e.Row.Cells.FromKey("Description").Value.ToString();
                txtDispDescription.Text = strDescription;
            }

            string strQty = e.Row.Cells.FromKey("Qty").Value.ToString();
            txtDispQty.Text = strQty;

            txtDispPlannedStartDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("PlannedStartDate").Value != null)
            {
                string strPlannedStartDate = e.Row.Cells.FromKey("PlannedStartDate").Value.ToString();
                txtDispPlannedStartDate.Text = strPlannedStartDate;
            }

            txtDispPlannedCompletionDate.Text = string.Empty;
            if (e.Row.Cells.FromKey("PlannedCompletionDate").Value != null)
            {
                string strPlannedCompletionDate = e.Row.Cells.FromKey("PlannedCompletionDate").Value.ToString();
                txtDispPlannedCompletionDate.Text = strPlannedCompletionDate;
            }

            wgSpecList.Clear();

            List<string> listWorkflowID = new List<string>();
            listWorkflowID.Add(strWorkflowID);

            //获取工序列表
            DataTable DT = common.GetSpecListByWorkflowID(listWorkflowID);

            wgSpecList.DataSource = DT;
            wgSpecList.DataBind();
            
            DataTable dtDispatch = dispatch.GetDispatchInfo(strContainerID, strWorkflowID);

            TemplatedColumn temCellSelect = (TemplatedColumn)wgSpecList.Columns.FromKey("ckSelect");
            TemplatedColumn temCell = (TemplatedColumn)wgSpecList.Columns.FromKey("TeamNameDisp");
            TemplatedColumn temCellDate = (TemplatedColumn)wgSpecList.Columns.FromKey("PlannedCompletionDate");

            for (int i = 0; i < wgSpecList.Rows.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                DropDownList ddlTeam = (DropDownList)cellItem.FindControl("ddlTeam");
                string strSpecID = wgSpecList.Rows[i].Cells.FromKey("SpecID").Value.ToString();
                string strSpecName = wgSpecList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
                wgSpecList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);

                cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellDate.CellItems[i];
                System.Web.UI.HtmlControls.HtmlInputText txtDate = (System.Web.UI.HtmlControls.HtmlInputText)cellItem.FindControl("txtPlannedCompletionDate");
                cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                DataRow[] rows = dtDispatch.Select(string.Format("SpecID = '{0}'", strSpecID));

                if (rows.Length > 0)
                {
                    ddlTeam.Enabled = false;
                    txtDate.Disabled = false;
                    ckSelect.Checked = false;
                    ckSelect.Enabled = false;
                }
            }
            

            wgDispatchList.Clear();
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 工序列表数据绑定
    protected void wgSpecList_DataBound(object sender, EventArgs e)
    {
        DataTable dtTeam = GetTeam();
        string strContainerID = txtDispContainerID.Text;
        string strWorkflowID = txtDispWorkflowID.Text;
        DataTable dtDispatch = dispatch.GetDispatchInfo(strContainerID, strWorkflowID);

        TemplatedColumn temCellSelect = (TemplatedColumn)wgSpecList.Columns.FromKey("ckSelect");
        TemplatedColumn temCell = (TemplatedColumn)wgSpecList.Columns.FromKey("TeamNameDisp");
        TemplatedColumn temCellDate = (TemplatedColumn)wgSpecList.Columns.FromKey("PlannedCompletionDate");

        for (int i = 0; i < temCell.CellItems.Count; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
            DropDownList ddlTeam = (DropDownList)cellItem.FindControl("ddlTeam");

            ddlTeam.DataTextField = "TeamName";
            ddlTeam.DataValueField = "TeamID";
            ddlTeam.DataSource = dtTeam;
            ddlTeam.DataBind();

            ddlTeam.Items.Insert(0, "");

            string strSpecID = wgSpecList.Rows[i].Cells.FromKey("SpecID").Value.ToString();
            string strSpecName = wgSpecList.Rows[i].Cells.FromKey("SpecName").Value.ToString();
            wgSpecList.Rows[i].Cells.FromKey("SpecNameDisp").Text = common.GetSpecNameWithOutProdName(strSpecName);

            cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellDate.CellItems[i];
            System.Web.UI.HtmlControls.HtmlInputText txtDate = (System.Web.UI.HtmlControls.HtmlInputText)cellItem.FindControl("txtPlannedCompletionDate");
            cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

            DataRow[] rows = dtDispatch.Select(string.Format("SpecID = '{0}'", strSpecID));

            if (rows.Length >= 1)
            {
                string strTeamID = rows[0]["TeamID"].ToString();
                ddlTeam.SelectedValue = strTeamID;

                string strDate = rows[0]["PlannedCompletionDate"].ToString();

                if (strDate != string.Empty)
                {
                    strDate = Convert.ToDateTime(strDate).ToString("yyyy-MM-dd");
                }

                txtDate.Value = strDate;

                ddlTeam.Enabled = false;
                txtDate.Disabled = true;
                ckSelect.Checked = false;
                ckSelect.Enabled = false;
            }
        }
    }
    #endregion

    #endregion

    #region 保存按钮
    protected void btnSave_Click(object sender, EventArgs e)
    {
        ClearMessage();

        try
        {
            if (CheckData() == false)
            {
                return;
            }

            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;

            TemplatedColumn temCellSelect = (TemplatedColumn)wgSpecList.Columns.FromKey("ckSelect");
            TemplatedColumn temCell = (TemplatedColumn)wgSpecList.Columns.FromKey("TeamNameDisp");
            TemplatedColumn temCellDate = (TemplatedColumn)wgSpecList.Columns.FromKey("PlannedCompletionDate");


            //查询已进行班组派工的信息
            DataTable dtDispatch = dispatch.GetDispatchInfoNew(txtDispContainerID.Text, txtDispWorkflowID.Text);
            int intCount = 0;
            for (int i = 0; i < temCellSelect.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

                if (ckSelect.Checked == true)
                {
                    intCount++;

                    cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                    DropDownList ddlTeam = (DropDownList)cellItem.FindControl("ddlTeam");
                    string strTeamID = ddlTeam.SelectedValue;

                    cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellDate.CellItems[i];
                    System.Web.UI.HtmlControls.HtmlInputText txtDate = (System.Web.UI.HtmlControls.HtmlInputText)cellItem.FindControl("txtPlannedCompletionDate");
                    string strPlannedCompletionDate = txtDate.Value;

                    string strDispatchInfoName = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    string strContainerID = txtDispContainerID.Text;
                    string strSpecID = wgSpecList.Rows[i].Cells.FromKey("SpecID").Value.ToString();
                    string strWorkflowStepID = wgSpecList.Rows[i].Cells.FromKey("WorkflowStepID").Value.ToString();
                    string strDispatchEmployeeID = userInfo["EmployeeID"];
                    string strQty = txtDispQty.Text;
                    string strWorkflowID = txtDispWorkflowID.Text;

                    //保存信息前判断该工序是否已进行班组派工
                    if (dtDispatch.Rows.Count>0)
                    {
                        DataRow[] drDispatch = dtDispatch.Select("containerid ='"+strContainerID+ "' AND workflowid='"+strWorkflowID+ "' AND specid='"+strSpecID+ "' AND dispatchtype='0' AND dispatchtotype='0'");
                        if (drDispatch.Length>0)
                        {
                            break;
                        }
                    }
                    Dictionary<string, string> para = new Dictionary<string, string>();
                    para.Add("DispatchInfoName", strDispatchInfoName);
                    para.Add("ContainerID", strContainerID);
                    para.Add("SpecID", strSpecID);
                    para.Add("WorkflowStepID", strWorkflowStepID);
                    para.Add("DispatchEmployeeID", strDispatchEmployeeID);
                    para.Add("DispatchDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    para.Add("DispatchType", "0");
                    para.Add("DispatchToType", "0");
                    para.Add("ContainerPhaseID", "");
                    para.Add("PlannedStartDate", "");
                    para.Add("PlannedCompletionDate", strPlannedCompletionDate);
                    para.Add("Qty", strQty);
                    para.Add("UOMID", "");
                    para.Add("ResourceID", "");
                    para.Add("Status", "0"); //已派工
                    para.Add("ParentID", "");
                    para.Add("WorkflowID", strWorkflowID);
                    para.Add("TeamID", strTeamID);
                    para.Add("State", "1");

                    //产品序号
                    DataTable dtProductNo = common.GetProductNo(strContainerID);

                    //加工人员
                    DataTable dtEmployee = new DataTable();

                    dispatch.AddDispatchInfo(para, dtProductNo, dtEmployee);
                }
            }

            wgSpecList_DataBound(null, null);
            DisplayMessage("保存成功", true);
        }
        catch (Exception ex)
        {
            DisplayMessage(ex.Message, false);
        }
    }

    #region 数据验证
    protected Boolean CheckData()
    {
        Boolean result = true;

        TemplatedColumn temCellSelect = (TemplatedColumn)wgSpecList.Columns.FromKey("ckSelect");
        TemplatedColumn temCell = (TemplatedColumn)wgSpecList.Columns.FromKey("TeamNameDisp");
        //TemplatedColumn temCellDate = (TemplatedColumn)wgSpecList.Columns.FromKey("PlannedCompletionDate");

        int intCount = 0;
        for (int i = 0; i < temCellSelect.CellItems.Count; i++)
        {
            Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
            CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");

            if (ckSelect.Checked == true)
            {
                intCount++;

                cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCell.CellItems[i];
                DropDownList ddlTeam = (DropDownList)cellItem.FindControl("ddlTeam");
                string strTeamID = ddlTeam.SelectedValue;

                if (strTeamID == "")
                {
                    DisplayMessage("请选择班组", false);
                    ddlTeam.Focus();
                    result = false;
                    break;
                }
            }
        }

        if (intCount == 0)
        {
            DisplayMessage("请选择工序", false);
            result = false;
        }

        return result;
    }
    #endregion

    #endregion
}