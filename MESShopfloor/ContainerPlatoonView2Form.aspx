﻿<%@ Page Language="C#" MasterPageFile="~/uMESMasterPage.master" AutoEventWireup="true" CodeFile="ContainerPlatoonView2Form.aspx.cs" Inherits="ContainerPlatoonView2Form" EnableViewState="true" %>

<%@ Register Assembly="Infragistics2.WebUI.Misc.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" Namespace="Infragistics.WebUI.Misc" TagPrefix="igmisc" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register Src="~/uMESCustomControls/pageTurning/pageTurning.ascx" TagName="pageTurning" TagPrefix="uPT" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>
<asp:Content ContentPlaceHolderID="HeaderContent" runat="Server">
    <script type="text/javascript">
        function openSpecDetailwins(DispatchinfoID) {
            window.showModalDialog("uMESSpecDetailInfo.aspx?DispatchinfoID=" + DispatchinfoID, "", "dialogWidth:850px; dialogHeight:100px; status=no; center: Yes; resizable: NO;");
        }
    </script>
    <igmisc:WebAsyncRefreshPanel ID="WebAsyncRefreshPanel1" runat="server">
        <div>
            <table class="SearchSectionTable" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td align="left" class="tdRight" style="width: 263px">
                        <div class="ScanLabel">扫描：</div>
                        <asp:TextBox ID="txtScan" runat="server" class="ScanTextBox" AutoPostBack="true" OnTextChanged="txtScan_TextChanged" Width="270px"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 148px">
                        <div class="divLabel">工作令号：</div>
                        <asp:TextBox ID="txtProcessNo" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 136px">
                        <div class="divLabel">批次号：</div>
                        <asp:TextBox ID="txtContainerName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 150px">
                        <div class="divLabel">图号/名称：</div>
                        <asp:TextBox ID="txtProductName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 150px">
                        <div class="divLabel">工序：</div>
                        <asp:TextBox ID="txtSpecName" runat="server" class="stdTextBox"></asp:TextBox>
                    </td>
                    <td align="left" valign="bottom" class="tdRight" style="width: 269px">
                        <div class="divLabel">查询日期：</div>
                        <input id="txtStartDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" visible="false" />
                        <%-----%>
                        <input id="txtEndDate" runat="server" onclick="this.value = ''; setday(this);" class="dateTextBox" type="text" />
                    </td>
                    <td align="left" valign="bottom" class="tdNoBorder" nowrap="nowrap">
                        <asp:Button ID="btnSearch" runat="server" Text="查询"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnReSet" runat="server" Text="重置"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnReSet_Click" />
                    </td>
                </tr>
                <tr style="display:none;">
                    <td align="left" style="padding-left: 5px;" colspan="6">
                        <table style="width: 400px">
                            <tr>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox17" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Style="text-align: center; vertical-align: middle;"
                                        Font-Bold="True" Width="56px">图例：</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox4" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="89px">▲质量报警</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox11" runat="server" BackColor="Pink" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox10" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="68px">当前工序</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox2" runat="server" BackColor="Yellow" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>

                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox7" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="50px">已派工</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox13" runat="server" BackColor="#FFD306" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox12" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="50px">已接收</asp:TextBox>
                                </td>

                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox9" runat="server" BackColor="YellowGreen" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox8" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="50px">已报工</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox1" runat="server" BackColor="#00ccff" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>

                                </td>

                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox6" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="50px">已检验</asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox3" runat="server" BackColor="Green" BorderStyle="None"
                                        ReadOnly="True" Style="height: 15px; width: 24px;"></asp:TextBox>
                                </td>
                                <td style="width: 50px">
                                    <asp:TextBox ID="TextBox5" runat="server" BackColor="White" BorderStyle="None"
                                        ReadOnly="True" Width="108px">已完工</asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <igtbl:UltraWebGrid ID="wgResult" runat="server" Height="400px" Width="98%" OnDataBound="wgResult_DataBound">
                            <Bands>
                                <igtbl:UltraGridBand>
                                    <AddNewRow View="NotSet" Visible="NotSet">
                                    </AddNewRow>
                                </igtbl:UltraGridBand>
                            </Bands>
                            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer"
                                BorderCollapseDefault="Separate" HeaderClickActionDefault="NotSet" Name="gdvMfgOrderList"
                                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" RowSelectorsDefault="No"
                                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                                RowHeightDefault="15px">
                                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="400px" Width="98%">
                                </FrameStyle>
                                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                                </RowAlternateStyleDefault>
                                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                                    StyleMode="QuickPages">
                                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </PagerStyle>
                                </Pager>
                                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                                </EditCellStyleDefault>
                                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                </FooterStyleDefault>
                                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Size="16px" Font-Bold="True">
                                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    <Padding Bottom="3px" Top="2px" />
                                    <Padding Top="2px" Bottom="3px"></Padding>
                                </HeaderStyleDefault>
                                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                                    <Padding Left="3px" />
                                </RowSelectorStyleDefault>
                                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="30px" BorderStyle="Solid"
                                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="14px" CssClass="GridRowStyle">
                                    <Padding Left="3px" />
                                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                                </RowStyleDefault>
                                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                                </GroupByRowStyleDefault>
                                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                                </SelectedRowStyleDefault>
                                <GroupByBox Hidden="True">
                                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                                    </BoxStyle>
                                </GroupByBox>
                                <AddNewBox>
                                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                                    </BoxStyle>
                                </AddNewBox>
                                <ActivationObject BorderColor="" BorderWidth="">
                                </ActivationObject>
                                <FilterOptionsDefault FilterUIType="HeaderIcons">
                                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px" Height="420px" Width="200px">
                                        <Padding Left="2px" />
                                    </FilterDropDownStyle>
                                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                                    </FilterHighlightRowStyle>
                                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                                        Font-Size="11px">
                                        <Padding Left="2px" />
                                    </FilterOperandDropDownStyle>
                                </FilterOptionsDefault>
                            </DisplayLayout>
                        </igtbl:UltraWebGrid>
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 8px; width: 100%;"></div>
        <div style="text-align: right; float: right">
            <uPT:pageTurning ID="upageTurning" runat="server" />
        </div>
        <div style="height: 8px; width: 100%;"></div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 100%;" colspan="2">
                        <asp:Button ID="btnAPS" runat="server" Text="下发"
                            CssClass="searchButton" EnableTheming="True" OnClick="btnAPS_Click" style="height: 26px" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hdScanCon" runat="server" />
    </igmisc:WebAsyncRefreshPanel>
</asp:Content>
