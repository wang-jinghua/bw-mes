﻿using System;
using uMES.LeanManufacturing.ReportBusiness;
using System.Collections.Generic;
using System.Net;

public partial class Login : System.Web.UI.Page
{
    private uMESLoginBusiness ulb = new uMESLoginBusiness();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
            cardNumberTxtBox.Focus();
        }

        if (Request.QueryString.Count == 1)
        {
            string strUserName = Request.QueryString[0];
            if (ulb.Vertification(strUserName))
            {
                Dictionary<string, string> userInfo = ulb.GetUserInfo(strUserName);
                Session["UserInfo"] = userInfo;
               InsertUserLogInfo(new Dictionary<string, string>() { { "LoginType", "2"} });//记录登录日志
                Response.Redirect("index.html");
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('无效的账号');window.location='/MESShopfloor'</script>");
            }
        }
    }
    protected void loginButton_Click(object sender, EventArgs e)
    {
        string strUid = userNameTxtBox.Text;
        string strPwd = passwordTxtBox.Text;
        string info = "用户名或密码错误";
        bool bol = ulb.Vertification(strUid, strPwd);
        if (bol)
        {
            Dictionary<string, string> userInfo = ulb.GetUserInfo(strUid);
            Session["UserInfo"] = userInfo;
            InsertUserLogInfo(new Dictionary<string, string>() { { "LoginType", "0" } });//记录登录日志
            Response.Redirect("index.html");
        }
        else
        {
            Response.Write("<script type='text/javascript'>alert('" + info + "');window.location='/MESShopfloor'</script>");
        }
    }

    protected void cardNumberTxtBox_TextChanged(object sender, EventArgs e)
    {
        try
        {
            string strCard = cardNumberTxtBox.Text;
            string strInfo = "";
            Boolean bol = ulb.CheckCardNumber(strCard, out strInfo);

            if(bol)
            {
                Dictionary<string, string> userInfo = ulb.GetUserInfo(strCard);
                Session["UserInfo"] = userInfo;
                InsertUserLogInfo(new Dictionary<string, string>() { { "LoginType", "1" } });//记录登录日志
                Response.Redirect("index.html");
            }
            else
            {
                Response.Write("<script type='text/javascript'>alert('" + strInfo + "');window.location='/MESShopfloor'</script>");
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('" + ex.Message + "')</script>");
        }
    }

    /// <summary>
    /// 插入登录信息
    /// add:Wangjh
    /// </summary>
    /// <param name="para"></param>
    void InsertUserLogInfo(Dictionary<string,string> para) {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
        //var ip= Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        var ip = GetClientIPv4Address();
        para["UserLoginInfoID"] =userInfo["UserLoginInfoID"];
        para["EmployeeName"] = userInfo["EmployeeName"];
        para["Note"] = "";
        para["LoginIp"] = ip;
        ulb.InsertUserLogInfo(para);
    }
    #region 获取ip
    public string GetClientIPv4Address()
    {
        string ipv4 = String.Empty;

        foreach (IPAddress ip in Dns.GetHostAddresses(GetClientIP()))
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                ipv4 = ip.ToString();
                break;
            }
        }

        if (ipv4 != String.Empty)
        {
            return ipv4;
        }
        // 利用 Dns.GetHostEntry 方法，由获取的 IPv6 位址反查 DNS 纪录，
        // 再逐一判断何者为 IPv4 协议，即可转为 IPv4 位址。
        foreach (IPAddress ip in Dns.GetHostEntry(GetClientIP()).AddressList)
        //foreach (IPAddress ip in Dns.GetHostAddresses(Dns.GetHostName()))
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                ipv4 = ip.ToString();
                break;
            }
        }
        return ipv4;
    }

    public string GetClientIP()
    {
        if (null == Request.ServerVariables["HTTP_VIA"])
        {
            return Request.ServerVariables["REMOTE_ADDR"];
        }
        else
        {
            return Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        }
    }
    #endregion

}
