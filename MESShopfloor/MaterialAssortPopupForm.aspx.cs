﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Data;
using System.Drawing;

public partial class MaterialAssortPopupForm : System.Web.UI.Page
{
    uMESMaterialBusiness material = new uMESMaterialBusiness();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Dictionary<string, string> popupData = (Dictionary<string, string>)Session["PopupData"];
            txtDispID.Text = popupData["ID"];
            txtDispProcessNo.Text = popupData["ProcessNo"];
            txtDispContainerName.Text = popupData["ContainerName"];
            txtDispContainerID.Text = popupData["ContainerID"];
            txtDispProductName.Text = popupData["ProductName"];
            txtDispProductID.Text = popupData["ProductID"];
            txtDispDescription.Text = popupData["Description"];
            txtDispQty.Text = popupData["Qty"];
            txtDispSpecName.Text = popupData["SpecName"];
            txtDispSpecID.Text = popupData["SpecID"];
            txtDispResourceName.Text = popupData["ResourceName"];
            txtDispPlannedCompletionDate.Text = popupData["PlannedCompletionDate"];

            Session["PopupData"] = null;

            string strProductID = txtDispProductID.Text;
            string strSpecID = txtDispSpecID.Text;
            int intQty = Convert.ToInt32(txtDispQty.Text);
            DataTable DT = material.GetMaterialListByProductID(strProductID, txtDispProcessNo.Text);
            DT.Columns.Add("RequireQty");
            DT.Columns.Add("StoreQty1");
            DT.Columns.Add("LackQty");

            DT.Columns["RequireQty"].DefaultValue = "0";
            DT.Columns["StoreQty1"].DefaultValue = "0";
            DT.Columns["LackQty"].DefaultValue = "0";

            //从二级库查询库存
            if (DT.Rows.Count > 0)
            {
                DataTable dtStock = material.GetMaterialStockQty(DT);

                foreach (DataRow row in DT.Rows)
                {
                    int intQtyRequired = Convert.ToInt32(row["QtyRequired"]);
                    row["RequireQty"] = intQtyRequired * intQty;

                    string strProdID = row["ProductID"].ToString();
                    DataRow[] rows = dtStock.Select(string.Format("ProductID = '{0}'", strProdID));

                    if (rows.Length > 0)
                    {
                        int intStockQty = Convert.ToInt32(rows[0]["Qty"].ToString());
                        row["StoreQty1"] = intStockQty.ToString();

                        int intLackQty = intQtyRequired * intQty - intStockQty;
                        if (intLackQty < 0)
                        {
                            intLackQty = 0;
                        }

                        row["LackQty"] = intLackQty;
                    }
                    else
                    {
                        row["StoreQty1"] = "0";
                        row["LackQty"] = intQtyRequired * intQty;
                    }
                }
            }

            //从ERP查询物料库存

            wgMaterialList.DataSource = DT;
            wgMaterialList.DataBind();
        }
    }

    #region 提示信息
    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }
    #endregion

    #region 提交按钮
    protected void btnMaterialApp_Click(object sender, EventArgs e)
    {
        ShowStatusMessage("", true);

        try
        {

            //物料列表
            DataTable dtMaterial = new DataTable();
            dtMaterial.Columns.Add("ProductID");
            dtMaterial.Columns.Add("Qty");
            dtMaterial.Columns.Add("UOMID");

            for (int i = 0; i < wgMaterialList.Rows.Count; i++)
            {
                string strProductID = wgMaterialList.Rows[i].Cells.FromKey("ProductID").Value.ToString();
                string strQty = wgMaterialList.Rows[i].Cells.FromKey("RequireQty").Value.ToString();
                int intLackQty = Convert.ToInt32(wgMaterialList.Rows[i].Cells.FromKey("LackQty").Value.ToString());

                if (intLackQty > 0)
                {
                    DataRow row = dtMaterial.NewRow();
                    row["ProductID"] = strProductID;
                    row["Qty"] = strQty;
                    row["UOMID"] = "";
                    dtMaterial.Rows.Add(row);
                }
            }

            if (dtMaterial.Rows.Count == 0)
            {
                ShowStatusMessage("没有缺件数量大于零的物料，不允许提交物料申请", false);
                return;
            }

            string strDispatchInfoID = txtDispID.Text;
            string strContainerID = string.Empty;
            string strContainerName = txtDispContainerName.Text;
            string strSpecID = txtDispSpecID.Text;
            string strProcessNo = txtDispProcessNo.Text;

            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            string strFactoryID = userInfo["FactoryID"];
            string strSubmitEmployeeID = userInfo["EmployeeID"];

            Dictionary<string, string> para = new Dictionary<string, string>();
            string strID = Guid.NewGuid().ToString();
            para.Add("MaterialAppInfoID", strID);
            para.Add("MaterialAppInfoName", DateTime.Now.ToString("yyyyMMddHHmmssfff"));
            para.Add("DispatchInfoID", strDispatchInfoID);
            para.Add("ContainerID", strContainerID);
            para.Add("ContainerName", strContainerName);
            para.Add("SpecID", strSpecID);
            para.Add("SpecNo", "");
            para.Add("SpecName", "");
            para.Add("FactoryID", strFactoryID);
            para.Add("ProcessNo", strProcessNo);
            para.Add("OprNo", "");
            para.Add("Type", "0");
            para.Add("SubmitEmployeeID", strSubmitEmployeeID);
            para.Add("SubmitDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            para.Add("Status", "0");
            para.Add("Notes", "");

            material.AddMaterialAppInfo(para, dtMaterial);

            //向ERP发送物料申请

            Response.Write("<script>parent.window.returnValue='1'; window.close()</script>");
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }
    #endregion
}