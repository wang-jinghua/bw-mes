<%@ Page Language="vb" AutoEventWireup="false" CodeFile="uMES_ProductNoForm.aspx.vb" Inherits="uMES_ProductNoForm" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v11.1, Version=11.1.20111.2158, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>选择产品图号</title>
		<base target="_self" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
        <META HTTP-EQUIV="Expires" CONTENT="0">
    <link href="styles/MyReportStyle.css" type="text/css" rel="Stylesheet" />
    <link href="styles/MESShopfloor.css" type="text/css" rel="Stylesheet" />
  	</head>
    <!-- Caution: modifying the id and runat attributes of the body will affect the integrity of the application. -->
    <body class="FormBody" id="bodyControl" runat="server">
    <!-- Caution: modifying the id and runat attributes of the form will affect the integrity of the application. -->    
        <form id="formControl" method="post" runat="server">
			<table border ="0" cellpadding ="0" cellspacing ="0" style="z-index :200; position :absolute ;left:8px; top:8px;">
            <tr>
                <td>
                    <table border="0" cellpadding="2" cellspacing="1">
                        <tr>
                            <td valign="bottom">
                                <asp:Label ID="lProductNo" runat="server" Text="产品序号" Font-Size="10pt"></asp:Label><br />
                                <asp:TextBox ID="txtProductNo" runat="server" class="stdTextBox" Width="150px"></asp:TextBox>
                            </td>
                            <td valign="bottom">
                                <asp:Button ID="btnSearch" runat="server" Text="查询" CssClass="searchButton" EnableTheming="True" />
                            </td>
                            <td valign="bottom">
                                <asp:Button ID="btnCancel" runat="server" Text="重置" CssClass="searchButton" EnableTheming="True" />
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td>
                                <asp:TextBox ID="txtProductNoHidden" runat="server"/>
                                <asp:TextBox ID="txtParentContainerID" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>   
                </td>
            </tr>
            <tr>
                <td style="height:8px">
                    
                </td>
            </tr>
            <tr>
                <td style="height:8px">
                    
                </td>
            </tr>
            <tr>
                <td>
                     <igtbl:UltraWebGrid ID="wglist" runat="server" Height="430px" Width="360px">
            <Bands>
                <igtbl:UltraGridBand>
                <Columns>
                                    <igtbl:UltraGridColumn BaseColumnName="ContainerName" Key="ContainerName" 
                                        Width="250px">
                                        <header caption="产品序号">
                                        </header>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ContainerID" Key="ContainerID" 
                                        Width="150px" Hidden="True">
                                        <header caption="ContainerID">
<RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            <rowlayoutcolumninfo originx="1" />
                                        </header>
                                        <footer>
<RowLayoutColumnInfo OriginX="1"></RowLayoutColumnInfo>
                                            <rowlayoutcolumninfo originx="1" />
                                        </footer>
                                    </igtbl:UltraGridColumn>
                                    <igtbl:UltraGridColumn BaseColumnName="ProductNo" 
                                        Key="ProductNo" Width="50px" Hidden="True">
                                        <header caption="ProductNo">
<RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                            <rowlayoutcolumninfo originx="2" />
                                        </header>
                                        <footer>
<RowLayoutColumnInfo OriginX="2"></RowLayoutColumnInfo>
                                            <rowlayoutcolumninfo originx="2" />
                                        </footer>
                                    </igtbl:UltraGridColumn>
                                     
                                </Columns>
                    <AddNewRow View="NotSet" Visible="NotSet">
                    </AddNewRow>
                </igtbl:UltraGridBand>       
            </Bands>
            <DisplayLayout AllowColSizingDefault="Free" AllowColumnMovingDefault="OnServer" AllowSortingDefault="OnClient"
                BorderCollapseDefault="Separate" HeaderClickActionDefault="SortSingle" Name="gdvMfgOrderList"
                SelectTypeRowDefault="Single" StationaryMargins="Header" StationaryMarginsOutlookGroupBy="True"
                TableLayout="Fixed" Version="4.00" AutoGenerateColumns="False" AllowRowNumberingDefault="ByDataIsland"
                CellClickActionDefault="RowSelect" ViewType="OutlookGroupBy" ScrollBarView="both"
                RowHeightDefault="18px">
                <FrameStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" Height="430px"
                    Width="360px">
                </FrameStyle>
                <RowAlternateStyleDefault BackColor="#D6F1FF" CssClass="GridRowAlternateStyle">
                </RowAlternateStyleDefault>
                <Pager MinimumPagesForDisplay="2" PageSize="10" Pattern="跳转至[default]页" QuickPages="4"
                    StyleMode="QuickPages">
                    <PagerStyle BackColor="LightGray" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </PagerStyle>
                </Pager>
                <EditCellStyleDefault BorderStyle="None" BorderWidth="0px" CssClass="GridEditCellStyle">
                </EditCellStyleDefault>
                <FooterStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" BorderWidth="1px">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                </FooterStyleDefault>
                <HeaderStyleDefault BackColor="#D6F1FF" BorderStyle="Solid" HorizontalAlign="Center"
                    CssClass="GridHeaderStyle" Height="100%" Wrap="True" Font-Bold="True">
                    <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    <Padding Bottom="3px" Top="2px" />
                    <Padding Top="2px" Bottom="3px"></Padding>
                </HeaderStyleDefault>
                <RowSelectorStyleDefault BorderStyle="Solid" BorderWidth="1px" Height="25px">
                    <Padding Left="3px" />
                </RowSelectorStyleDefault>
                <RowStyleDefault BackColor="White" BorderColor="Silver" Height="25px" BorderStyle="Solid"
                    BorderWidth="1px" Font-Names="Microsoft Sans Serif" Font-Size="8.25pt" CssClass="GridRowStyle">
                    <Padding Left="3px" />
                    <BorderDetails ColorLeft="Window" ColorTop="Window" />
                </RowStyleDefault>
                <GroupByRowStyleDefault BackColor="Control" BorderColor="Window">
                </GroupByRowStyleDefault>
                <SelectedRowStyleDefault BackColor="LightYellow" CssClass="GridSelectedRowStyle">
                </SelectedRowStyleDefault>
                <GroupByBox Hidden="True">
                    <BoxStyle BackColor="ActiveBorder" BorderColor="Window">
                    </BoxStyle>
                </GroupByBox>
                <AddNewBox>
                    <BoxStyle BackColor="Window" BorderColor="InactiveCaption" BorderStyle="Solid" BorderWidth="1px">
                        <BorderDetails ColorLeft="White" ColorTop="White" WidthLeft="1px" WidthTop="1px" />
                    </BoxStyle>
                </AddNewBox>
                <ActivationObject BorderColor="" BorderWidth="">
                </ActivationObject>
                <FilterOptionsDefault FilterUIType="HeaderIcons">
                    <FilterDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px"
                        CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px" Height="420px" Width="200px">
                        <Padding Left="2px" />
                    </FilterDropDownStyle>
                    <FilterHighlightRowStyle BackColor="#151C55" ForeColor="White">
                    </FilterHighlightRowStyle>
                    <FilterOperandDropDownStyle BackColor="White" BorderColor="Silver" BorderStyle="Solid"
                        BorderWidth="1px" CustomRules="overflow:auto;" Font-Names="Verdana,Arial,Helvetica,sans-serif"
                        Font-Size="11px">
                        <Padding Left="2px" />
                    </FilterOperandDropDownStyle>
                </FilterOptionsDefault>
            </DisplayLayout>
        </igtbl:UltraWebGrid>
                </td>
            </tr>
            <tr style="display:none;">
                <td align ="right" style="font-size:10pt;">
                    <asp:LinkButton ID="lbtnFirst" runat="server" Font-Size="10pt">首页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID ="lbtnPrev" runat ="server" Font-Size="10pt">上一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID ="lbtnNext" runat ="server" Font-Size="10pt">下一页</asp:LinkButton>&nbsp;|&nbsp;
                    <asp:LinkButton ID ="lbtnLast" runat ="server" Font-Size="10pt">尾页</asp:LinkButton>&nbsp;
                    <asp:Label ID ="lLabel1" runat ="server" ForeColor ="red" Text ="第  页  共  页"></asp:Label>
                    <asp:Label ID ="lLabel2" runat ="server" Text ="转到第"></asp:Label>
                    <asp:TextBox ID ="txtPage" runat ="server" style="z-index :200; width:30px;"></asp:TextBox>
                    <asp:Label ID ="lLabel3" runat ="server" Text ="页"></asp:Label>
                    <asp:Button ID ="btnGo" runat ="server" Text ="Go" CssClass="ReportButton" EnableTheming="True" />
                    <asp:TextBox ID ="txtTotalPage" runat ="server" Visible="False">0</asp:TextBox>
                    <asp:TextBox ID ="txtCurrentPage" runat ="server" Visible="False">0</asp:TextBox>
                    <asp:TextBox ID="txtWorkflowID" runat="server" Visible="false" />
                </td>
            </tr>
            <tr>
                <td style="height:8px">
                    
                </td>
            </tr>
            <tr>
                <td align ="center" >
                    <asp:Button ID="btnSave" runat="server" Text="确定" CssClass="searchButton" EnableTheming="True" />
			        &nbsp;
                    <asp:Button ID="btnClose" runat="server" Text="取消" CssClass="searchButton" EnableTheming="True" />
		        </td>
            </tr>
            <tr>
                <td id="tdMessage" runat="server">
                    
                </td>
            </tr>
        </table>
        <input type ="hidden" id="pageType" runat ="server" />
        		</form>
	</body>
</html>
