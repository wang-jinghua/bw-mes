﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="left.aspx.cs" Inherits="left" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style/css.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../script/tree.js"></script>
    <script type="text/javascript" src="/MESShopfloor/Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="/MESShopfloor/Scripts/json2.js"></script>
    <style type="text/css">
        .before {
            font-size: 14px;
            color: Black;
            text-decoration: none;
        }

        .after {
            font-size: 14px;
            color: Red;
            text-decoration: none;
        }

        .over {
            font-size: 14px;
            color: Green;
            text-decoration: none;
        }

        .second_bar {
            width: 100%;
            border: 0;
        }

            .second_bar td {
                width: 19;
                height: 20;
            }
    </style>
    <script type="text/javascript">
        var temp;
        var CurrentColor;
        function spanOnclick(obj) {
            if (temp != null) {
                temp.className = CurrentColor;
            }
            CurrentColor = "after";
            temp = obj;
            obj.className = CurrentColor;
        }
        function over(obj) {
            CurrentColor = obj.className;
            obj.className = 'over';
        }
        function out(obj) {
            obj.className = CurrentColor;
        }
    </script>
    <script type="text/javascript">

        function checkTime(me) {
            var rex = new RegExp("^[1-9][0-9]{1,3}$");
            if (!rex.test(me.value)) {
                me.value = 60;
            }
            //  refreshMenu();

        }
        var curNum = 1;

        function refreshMenu(obj, chk) {

            var max = 7;
            if (curNum > max) {
                curNum = 1;
            }
            obj = document.getElementById("selectmenu" + curNum);
            chk = document.getElementById("chkRefresh");

            //  alert(chk.checked);
            if (chk.checked) {
                obj.click();
                curNum++;
            }

            var timeNum = document.getElementById("txtTime").value;
            window.setTimeout(refreshMenu, timeNum * 1000);
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                <td style="text-align: left; padding-left: 15px; display: none;">
                   <%-- <span>时间设定：</span>
                    <input id="txtTime" type="text" style="width: 32px; font-family: 幼圆; text-align: center; color: Red; font-weight: bolder;"
                        onchange="checkTime(this);" value="60" />
                    <span>秒</span>&nbsp;--%>
                <asp:CheckBox ID="chkRefresh" runat="server" Text="自动切换" />
                <span runat="server" ID="hdEmployeeID" ></span>
                    
                </td>
            </tr>

            <tr>
                <td>
                    <br />
                    <table width="205" border="0" cellpadding="0" cellspacing="0" class="bg" align="left">
                        <tr>
                            <td valign="top" class="bg" style="height: 316px; width: 20px;"></td>
                            <td valign="top">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="bg">
                                            <% for (int i = 0; i <= dtMenu.Rows.Count - 1; i++)
                                                {
                                                    string strMenuName = dtMenu.Rows[i]["menuname"].ToString();
                                                    string strMenuID = dtMenu.Rows[i]["menuid"].ToString();
                                                    drs =dtMenuChild.Select("mainid='"+strMenuID +"'","sequence asc");

                                            %>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19" style="height: 20px">
                                                        <img id="<%="top_"+i %>" onclick="headd('<%="menu_"+ i %>','<%="top_"+i %>');" src="images/tree_2_03.gif" width="19"
                                                            height="20" />
                                                    </td>
                                                    <td width="19" style="height: 20px">
                                                        <img src="images/tree_27.gif" width="19" height="20" />
                                                    </td>
                                                    <td nowrap style="height: 20px; font-size :14px"><%=strMenuName  %>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="<%="menu_"+ i %>">
                                                <%  
                                                    for (int j = 0; j <= drs.Length  - 1; j++)
                                                    {
                                                        string strMenuChildName = drs[j]["menuchildname"].ToString();
                                                        string strMenuChildUrl = drs[j]["url"].ToString(); 
                                                %>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td width="19">
                                                            <img src="images/tree_06.gif" width="19" height="20">
                                                        </td>
                                                        <td width="19">
                                                            <img src="images/tree_07.gif" width="19" height="20">
                                                        </td>
                                                        <td width="19">
                                                            <img src="images/tree_08.gif" width="19" height="20">
                                                        </td>
                                                        <td nowrap>

                                                            <a href="<%=strMenuChildUrl%>" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                                class="before" onmouseover="over(this);" onmouseout="out(this);"><%=strMenuChildName  %></span></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%
                                                    }
                                                %>
                                            </div>
                                            <% 
                                                }
                                            %>

                                    

                                            <%--<table runat="server" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="19" style="height: 20px">
                                                    <img alt="" id="top_1" onclick="headd('menu_1','top_1')" src="images/tree_2_03.gif" width="19"
                                                        height="20"/>
                                                </td>
                                                <td width="19" style="height: 20px">
                                                    <img alt="" src="images/tree_27.gif" width="19" height="30"/>
                                                </td>
                                                <td nowrap="nowrap" style="height: 30px; font-size:16px; font-weight:bold;">
                                                    制造执行系统
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="menu_1">

                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BaseManage/MenuManagerForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">菜单创建</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BaseManage/RoleEmployeeForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">人员角色创建</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BaseManage/RoleMenuForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">角色菜单创建</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            


                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwMfgorderImportForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">订单导入</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwMfgorderViewForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">批次创建</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="ContainerPrintForm.aspx" target="mainFrame" id="A1" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">过程检验流程卡打印</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="TeamDispatchForm.aspx" target="mainFrame" id="A3" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">班组派工</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="TeamDispatchAdjustForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">班组任务调整</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="ResourceDispatchForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">任务指派</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="ResourceDispatchAdjustForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">指派任务调整</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="TaskReceiveForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">任务接收</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="MaterialAppForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">物料申请</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="MaterialAssortForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">物料齐套</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="MfgSetUpForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">生产准备</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="AssemblyRecordForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">装配记录</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="AssemblyRemoveForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">装配拆除</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="WorkReportForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">报工管理</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="ProblemSubmitForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">问题上报</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="ProblemDispatchForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">问题指派</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="ProblemDisposeForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">问题处理</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwCheckManage/BwContainerCheckForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">检验平台</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="TestForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">测试</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="RejectApp1Form.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">设计/工艺审核</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="RejectApp2Form.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">质量审核</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="RejectApp3Form.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">领导审核</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="RejectCheckDisposeForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">检验处理</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="RejectGYDisposeForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">工艺处理</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwSynergicInfoManagerForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">外协转出</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwSynergicInfoManagerReturnForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">外协转入</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwSynergicInfoApplyForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">外协申请</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwSynergicInfoAuditForm.aspx?type=1" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">外协审核-生产经理</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwSynergicInfoAuditForm.aspx?type=2" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">外协审核-工艺员</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwSynergicInfoAuditForm.aspx?type=3" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">外协审核-质量部门</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwPlanManager/BwSynergicInfoAuditForm.aspx?type=4" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">外协审核-生产领导</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="Custom/BwCheckItemMaintForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">检测项维护</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="uMESSubmitToSecondaryWarehouseForm.aspx" target="mainFrame" id="A2" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">提交入库</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="uMESSecondaryWarehouseWarehousingForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">入库操作</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="uMESSecondaryWarehouseOutForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">出库操作</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="uMESAdjustingStockQtyApplyForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">库存调整申请</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_06.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_07.gif" width="19" height="25"/>
                                                    </td>
                                                    <td width="19">
                                                        <img alt="" src="images/tree_08.gif" width="19" height="20"/>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <a href="uMESAdjustingStockQtyReviewForm.aspx" target="mainFrame" id="A4" class="a03"><span onclick="spanOnclick(this);"
                                                            class="before" onmouseover="over(this);" onmouseout="out(this);">库存调整审核</span></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            var obj = document.getElementById("selectmenu" + curNum);
            var chk = document.getElementById("chkRefresh");
           // refreshMenu(obj, chk);
        </script>
        <script type="text/javascript">
            function headd(element, img) {
                var $ele = $("#" + element);
                if ($ele.css("display") === "block") {
                    $ele.css("display", "none");
                    $("#" + img).attr("src", "images/tree_33.gif");
                } else {
                    $ele.css("display", "block");
                    $("#" + img).attr("src", "images/tree_2_03.gif");
                }
            }
        </script>
        <script>
            var userInfo;
            function GetProblemCount() {
                if ($('a[href="ProblemDisposeForm.aspx"]').length == 0)
                    return;
                $.ajax({
                    type: "Post",
                    url: "Ashx/MenuTaskCount.ashx?action=GetProblemDispatchFormCount",
                    // contentType: "application/json",
                    //dataType: "json",
                    data: { status: "0,5", searchEmployee: userInfo.EmployeeID },
                    cashe: false,
                    beforeSend: function () {
                        //$("#partTask").addClass("loading");
                    },
                    success: function (data) {
                        //问题处理循环给数目
                        $.each($('a[href="ProblemDisposeForm.aspx"]'), function (i, val) {
                            a = $(val);
                            if (a.find('span').length == 1)
                            { a.append("<span style='color:red' >(" + data + ")</span>"); }
                            else if (a.find('span').length == 2) {
                                a.find('span').eq(1).text("(" + data + ")");
                            }
                        });                        
                        //                      

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(XMLHttpRequest.responseText);
                    }

                });
                setTimeout(function () { GetProblemCount(); }, 30 * 1000);
            }

            $(function () {
                userInfo = JSON.parse($("#hdEmployeeID").text());
                //定时查询问题上报问题数  ProblemDispatchForm.aspx
                GetProblemCount();//30秒刷新一次问题待处理数
               top.document.getElementById("mainFrame").setAttribute("src", $(".bg a").eq(0).attr("href"));
            });
        </script>
    </form>



</body>
</html>
