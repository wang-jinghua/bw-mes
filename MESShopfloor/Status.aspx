﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Status.aspx.cs" Inherits="Status" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function ShowMessage(message, success)
        {
            document.getElementById("tdMessage").innerText = message;

            if (success) {
                document.getElementById("tdMessage").style.color = "#000000";
            }
            else {
                document.getElementById("tdMessage").style.color = "#FF0000";
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table style="width:100%;">
        <tr>
            <td style="width:70px; font-size:12px; font-weight:bold;" nowrap="nowrap">状态信息：</td>
            <td id="tdMessage" style="width:95%; font-size:12px;" align="left"></td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
