﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Infragistics.WebUI.UltraWebGrid;
using uMES.LeanManufacturing.Common;
using System.IO;
using uMES.LeanManufacturing.ReportBusiness;
using uMES.LeanManufacturing.ParameterDTO;
using System.Text;
using System.Text.RegularExpressions;
using uMES.LeanManufacturing.DBUtility;
using System.Drawing;
using System.Data.OracleClient;

public partial class uMESQualityRecordInfoPopupForm : System.Web.UI.Page
{
    uMESCommonBusiness common = new uMESCommonBusiness();
    uMESSecondaryWarehouseBusiness bll = new uMESSecondaryWarehouseBusiness();
    uMESContainerBusiness containerBal = new uMESContainerBusiness();
    string businessName = "质量管理", parentName = "qualityrecordinfo";

    public FreeTextBoxControls.FreeTextBox oHtml;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Session["PopupData"] !=null)
            {
                Dictionary<string, string> para = (Dictionary<string, string>)Session["PopupData"];

                Session["MyData"] = para;
                
                ///界面控件赋值
                BindDataToControls(para);

                txtIWorkflowID.Text = txtWorkflowID.Text;
                txtISpecID.Text = txtSpecID.Text;
                txtISpecName.Text = txtSpecDisName.Text;
                txtIReportInfoID.Text = txtReportInfoID.Text;
                txtISpecName.Text = txtSSpecName.Text;

                //绑定产品序号
                if (txtContainerID.Text !="1")
                {
                    BindProductNo(txtReportInfoID.Text);
                }

                //绑定已保存的质量记录信息
                BindSaveQualInfo(para);

                //获取质量记录单号
                GetQualityRecordInfoName();
            }
        }


    }

    /// <summary>
    /// 界面控件赋值
    /// </summary>
    protected void BindDataToControls(Dictionary<string, string> para)
    {
        try
        {
            //工作令号
            if (para.ContainsKey("ProcessNo"))
            {
                txtDispProcessNo.Text =para["ProcessNo"];
            }

            //作业令号
            if (para.ContainsKey("OprNo"))
            {
                txtDispOprNo.Text = para["OprNo"];
            }

            //批次号
            if (para.ContainsKey("ContainerName"))
            {
                txtDispContainerName.Text = para["ContainerName"];
            }

            //图号
            if (para.ContainsKey("ProductName"))
            {
                txtDispProductName.Text  = para["ProductName"];
            }

            //名称
            if (para.ContainsKey("Description"))
            {
                txtDispDescription.Text = para["Description"];
            }

            //批次数量
            if (para.ContainsKey("ConQty"))
            {
                txtConQty.Text  = para["ConQty"];
            }

            //计划开始时间
            if (para.ContainsKey("StartTime"))
            {
                txtStartTime.Text  = para["StartTime"];
            }

            //计划结束时间
            if (para.ContainsKey("EndTime"))
            {
                txtEndTime.Text = para["EndTime"];
            }

            //工序
            if (para.ContainsKey("SpecDisName"))
            {
                txtSpecDisName.Text = para["SpecDisName"];
            }

            //含图号的工序名称
            if (para.ContainsKey("SpecName"))
            {
                txtSSpecName.Text = para["SpecName"];
            }

            //跟踪卡ID
            if (para.ContainsKey("ContainerID"))
            {
                txtContainerID.Text = para["ContainerID"];
            }

            //工艺规程ID
            if (para.ContainsKey("WorkflowID"))
            {
                txtWorkflowID.Text = para["WorkflowID"];
            }

            //报工单ID
            if (para.ContainsKey("ReportInfoID"))
            {
                txtReportInfoID.Text = para["ReportInfoID"];
            }

            //工序ID
            if (para.ContainsKey("SpecID"))
            {
                txtSpecID.Text = para["SpecID"];
            }

            //计量单位ID
            if (para.ContainsKey("UomID"))
            {
                txtUomID.Text = para["UomID"];
            }

            //报工单不合格数量
            if (para.ContainsKey("NonsenseQty"))
            {
                txtNonsenseQty.Text = para["NonsenseQty"];
            }

            //报工数量 
            if (para.ContainsKey("ReportQty"))
            {
                txtReportQty.Text = para["ReportQty"];
            }

        }
        catch (Exception Ex)
        {

            ShowStatusMessage(Ex.Message, false);
        }
    }

    /// <summary>
    /// 绑定产品序号
    /// </summary>
    /// <param name="strContainerID"></param>
    protected void BindProductNo(string  strReportID)
    {
        wgProductNo.Rows.Clear();

        DataTable dtProductNo = common.GetNoQrProductNoByReportID(strReportID);

        wgProductNo.DataSource = dtProductNo;
        wgProductNo.DataBind();
    }

    //获取已保存的质量记录信息
    protected void BindSaveQualInfo(Dictionary<string, string> para)
    {
        ItemGrid.Rows.Clear();
        DataTable dt = bll.GetSavedQualInfo(para);

        ItemGrid.DataSource = dt;
        ItemGrid.DataBind();
    }

    /// <summary>
    /// 清空界面控件
    /// </summary>
    protected void ClearControlS()
    {
        ////工作令号
        //txtDispProcessNo.Text = string.Empty;
        ////作业令号
        //txtDispOprNo.Text = string.Empty;
        ////批次号
        //txtDispContainerName.Text = string.Empty;
        ////图号
        //txtDispProductName.Text = string.Empty;
        ////名称
        //txtDispDescription.Text = string.Empty;
        ////批次数量
        //txtConQty.Text = string.Empty;
        ////计划开始时间
        //txtStartTime.Text = string.Empty;
        ////计划结束时间
        //txtEndTime.Text = string.Empty;
        ////工序
        //txtSpecDisName.Text = string.Empty;
        ////跟踪卡ID
        //txtContainerID.Text = string.Empty;
        ////报工单ID
        //txtReportInfoID.Text = string.Empty;
        ////工序ID
        //txtSpecID.Text = string.Empty;
        ////计量单位ID
        //txtUomID.Text = string.Empty;
        //质量记录单号
        txtQualRecordID.Text = string.Empty;
        //不合格数量
        txtUnQualQty.Text = string.Empty;
        //备注
        txtNotes.Text = string.Empty;
        //流水编号
        txtSerialNumber.Text = string.Empty;
        //是否提交不合理单
        txtIsSubmit.Text = string.Empty;
        //报工单ID
        //txtReportInfoID.Text = string.Empty;
        //质量信息
        txtQualityNotes.Text = string.Empty;
        //当前选中的含图号的工序名称
        txtSSpecName.Text = string.Empty;
        oHtml = (FreeTextBoxControls.FreeTextBox)MyToleranceInput.FindControl("ftbFinalHtml");
        oHtml.Text = "";
        txtID.Text = string.Empty;

        //撤销已选中的产品序号
        clearSelectedProductNo();
    }

    //获取质量记录单号
    protected void GetQualityRecordInfoName()
    {
        Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;

        Dictionary<string, string> para = new Dictionary<string, string>();
        para.Add("TableName", "QualityRecordInfo");

        para.Add("Prefix", "ZL-"+userInfo["InspectorNo"]);

        int intSerialNumber;
        txtQualRecordID.Text = bll.GetQualityRecordInfoName(para,out intSerialNumber);
        txtSerialNumber.Text = intSerialNumber.ToString();
    }
    /// <summary>
    /// 撤销已选中的产品序号
    /// </summary>
    protected void clearSelectedProductNo()
    {
        if (wgProductNo.Rows.Count>0)
        {
            TemplatedColumn temCellSelect = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");
            for (int i = 0; i < temCellSelect.CellItems.Count; i++)
            {
                Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                ckSelect.Checked = false;
            }
        }
    }

    /// <summary>
    /// 选中已保存的质量记录信息
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ItemGrid_ActiveRowChange(object sender, RowEventArgs e)
    {
        try
        {
            //清空界面控件信息
            ClearControlS();
            ShowStatusMessage("", true);

            oHtml = (FreeTextBoxControls.FreeTextBox)MyToleranceInput.FindControl("ftbFinalHtml");

            //质量记录单号
            if (e.Row.Cells.FromKey("QualityRecordInfoName").Value != null)
            {
                txtQualRecordID.Text = e.Row.Cells.FromKey("QualityRecordInfoName").Value.ToString();
            }

            //不合格数量
            if (e.Row.Cells.FromKey("qty").Value != null)
            {
                txtUnQualQty.Text = e.Row.Cells.FromKey("qty").Value.ToString();
            }

            //备注 
            if (e.Row.Cells.FromKey("SubmitNotes").Value != null)
            {
                txtNotes.Text = e.Row.Cells.FromKey("SubmitNotes").Value.ToString();
            }

            //是否提交不合理单
            if (e.Row.Cells.FromKey("IsSubmit").Value != null)
            {
                txtIsSubmit.Text = e.Row.Cells.FromKey("IsSubmit").Value.ToString();
            }

            //工序名称
            if (e.Row.Cells.FromKey("SpecDisName").Value != null)
            {
                txtSpecDisName.Text = e.Row.Cells.FromKey("SpecDisName").Value.ToString();
            }

            //含图号的工序名称
            if (e.Row.Cells.FromKey("SpecName").Value != null)
            {
                txtSSpecName.Text = e.Row.Cells.FromKey("SpecName").Value.ToString();
            }

            //ID
            if (e.Row.Cells.FromKey("id").Value != null)
            {
                txtID.Text = e.Row.Cells.FromKey("id").Value.ToString();
            }

            //
            if (e.Row.Cells.FromKey("ReportInfoID").Value != null)
            {
                txtReportInfoID.Text = e.Row.Cells.FromKey("ReportInfoID").Value.ToString();
            }

            //查询详细信息
            Dictionary<string, string> para = new Dictionary<string, string>();
            para.Add("ID",txtID.Text);
            DataTable dt = bll.GetSavedDetailQualInfo(para);

            if (dt.Rows.Count>0)
            {
               MyToleranceInput.ParseCode(dt.Rows[0]["QualityNotes"].ToString());

                if (wgProductNo.Rows.Count>0)
                {
                    TemplatedColumn temCellSelect = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");
                    for (int i = 0; i < temCellSelect.CellItems.Count; i++)
                    {
                        Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                        CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                        ckSelect.Checked = false;

                        string strProductNo = wgProductNo.Rows[i].Cells.FromKey("productno").Value.ToString();

                        DataRow[] dr = dt.Select("productno ='"+strProductNo +"'");
                        if (dr.Length>0)
                        {
                            ckSelect.Checked = true;
                        }

                    }
                }
            }
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 全选按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAllSelect_Click(object sender, EventArgs e)
    {
        try
        {
            if (wgProductNo.Rows.Count > 0)
            {
                TemplatedColumn temCellSelect = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");
                for (int i = 0; i < temCellSelect.CellItems.Count; i++)
                {
                    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                    ckSelect.Checked = true;
                }
            }
        }
        catch(Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }

    }

    /// <summary>
    /// 反选按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnRevSelect_Click(object sender, EventArgs e)
    {
        try
        {
            if (wgProductNo.Rows.Count > 0)
            {
                TemplatedColumn temCellSelect = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");
                for (int i = 0; i < temCellSelect.CellItems.Count; i++)
                {
                    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                    if (ckSelect.Checked == true)
                    {
                        ckSelect.Checked = false;
                    }
                    else if (ckSelect.Checked == false)
                    {
                        ckSelect.Checked = true;
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }
    }

    /// <summary>
    /// 确定按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtIsSubmit.Text == "1")
            {
                ShowStatusMessage("该质量记录已提交不合品审理，无法修改！", false);
                return;
            }

            //是否填写不合格数
            int intCount = 0;
            if (wgProductNo.Rows.Count > 0)
            {
                TemplatedColumn temCellSelect = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");
                for (int i = 0; i < temCellSelect.CellItems.Count; i++)
                {
                    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                    if (ckSelect.Checked == true)
                    {
                        intCount += 1;
                    }

                }
                if (intCount == 0)
                {
                    ShowStatusMessage("请勾选不合格零件序号！", false);
                    return;
                }
                txtUnQualQty.Text = intCount.ToString();
            }
            else
            {
                if (txtUnQualQty.Text == "")
                {
                    ShowStatusMessage("请填写不合格数量！", false);
                    return;
                }
                else
                {
                    bool blResult = IsInt(txtUnQualQty.Text);
                    if (blResult==false)
                    {
                        ShowStatusMessage("不合格数量需为正整数！", false);
                        return;
                    }
                }
                  
            }


            oHtml = (FreeTextBoxControls.FreeTextBox)MyToleranceInput.FindControl("ftbFinalHtml");
            //是否填写质量信息
            if (oHtml.Text == "")
            {
                ShowStatusMessage("请填写质量信息描述！", false);
                return;
            }

            ////查询已保存的质量记录
            //Dictionary<string, string> para2 = new Dictionary<string, string>();
            //para2.Add("ContainerID", txtContainerID.Text);
            //para2.Add("SpecName", txtSSpecName.Text);
            //para2.Add("ReportInfoID", txtReportInfoID.Text);

            //string strType = string.Empty;
            //DataTable dtR = bll.GetReportInfo(para2);
            //if (dtR.Rows.Count > 0)
            //{
            //    strType = dtR.Rows[0]["ReportType"].ToString();
            //}
            //DataTable dtQ = bll.GetSavedQualInfo(para2);

            //int intSQty =0;

            //if (dtQ.Rows.Count > 0)
            //{
            //    for (int h = 0; h < dtQ.Rows.Count; h++)
            //    {
            //        if (!string.IsNullOrEmpty(dtQ.Rows[h]["Qty"].ToString()))
            //        {
            //            intSQty = intSQty + Convert.ToInt32(dtQ.Rows[h]["Qty"].ToString());
            //        }

            //    }
            //}

            ////判断是否是首检检验，如果是则不合格数不能大于一，工序检验不合格数不能大于批次数
            //if (strType == "0")
            //{
            //    if (intSQty + Convert.ToInt32(txtUnQualQty.Text)>1)
            //    {
            //        ShowStatusMessage("首检不合格数不能大于1！", false);
            //        return;
            //    }
            //}
            //else 
            //{
            //    int intConQty = 0;
            //    if (!string.IsNullOrEmpty(txtConQty.Text))
            //    {
            //        intConQty = Convert.ToInt32(txtConQty.Text);
            //    }

            //    if (intSQty + Convert.ToInt32(txtUnQualQty.Text) > Convert.ToInt32(intConQty))
            //    {
            //        ShowStatusMessage("质量记录总不合格数不能大于本次报工不合格数量！", false);
            //        return;
            //    }
            //}

            //判断质量记录总不合格数不能大于本次报工不合格数量
            //获取本次报工总不合格数量
            int intNonsenseQty = Convert.ToInt32(txtNonsenseQty.Text);

            //获取质量记录总不合格数量
            int intSQty = 0;
            //与报工单关联的不合格数量
            int intReportQty = 0;
            for (int i = 0; i < ItemGrid.Rows.Count; i++)
            {
                int intQ = Convert.ToInt32(ItemGrid.Rows[i].Cells.FromKey("Qty").Value.ToString());

                intSQty += intQ;
                if (ItemGrid.Rows[i].Cells.FromKey("ReportInfoID").Value !=null)
                {
                    if (ItemGrid.Rows[i].Cells.FromKey("ReportInfoID").Text==txtIReportInfoID.Text)
                    {
                        intReportQty += intQ;
                    }
                }
            }

            //判断工序是否已检验，且存在不合格数量
            if (intNonsenseQty > 0)
            {
                //判断
                if (intReportQty + Convert.ToInt32(txtUnQualQty.Text) > intNonsenseQty)
                {
                    ShowStatusMessage("质量记录总不合格数不能大于本次报工不合格数量", false);
                    return;
                }
            }
            else
            {
                //判断
                if (intSQty + Convert.ToInt32(txtUnQualQty.Text) > intReportQty)
                {
                    ShowStatusMessage("质量记录总不合格数不能大于本次报工数量", false);
                    return;
                }
            }

            Dictionary<string, string> userInfo = Session["UserInfo"] as Dictionary<string, string>;
            //以字典形式传递参数
            Dictionary<string, object> para = new Dictionary<string, object>();
            DataSet ds = new DataSet();
            //日志对象
            var ml = new MESAuditLog();
            //提示信息
            string strMessage = string.Empty;
            string strID = txtID.Text;
            if (txtID.Text != "")
            {
                ml.OperationType = 1;
                ml.Description = "质量记录:" + txtSpecDisName.Text + ",修改质量记录数:" + txtUnQualQty.Text;

                DataTable dtUpdate = new DataTable();
                dtUpdate.Columns.Add("TableName");
                dtUpdate.Columns.Add("ID");
                dtUpdate.Columns.Add("Qty");//不合格数量
                dtUpdate.Columns.Add("SubmitNotes");//检验备注
               
                dtUpdate.Rows.Add();
                int intUpdateRow = dtUpdate.Rows.Count - 1;
                dtUpdate.Rows[intUpdateRow]["TableName"] = "QualityRecordInfo";
                dtUpdate.Rows[intUpdateRow]["ID"] = txtID.Text + "㊣String";//
                dtUpdate.Rows[intUpdateRow]["Qty"] =txtUnQualQty.Text + "㊣String";//入库数量
                dtUpdate.Rows[intUpdateRow]["SubmitNotes"] = txtNotes.Text + "㊣String";//最后一次入库时间

                int iResult = bll.UpdateDataToDatabase(dtUpdate, out strMessage);
                if (iResult == -1)
                {
                    ShowStatusMessage(strMessage, false);
                    return;
                }
            }
            else
            {
                ml.OperationType = 0;
                ml.Description = "质量记录:" + txtSpecDisName.Text + ",添加质量记录数:" + txtUnQualQty.Text;

                DataTable dtMain = new DataTable();
                dtMain.Columns.Add("ID");//唯一标识
                dtMain.Columns.Add("QualityRecordInfoName");//质量记录/不合格品审理单号
                dtMain.Columns.Add("ContainerID");//生产批次ID
                dtMain.Columns.Add("SpecID");//工序ID
                dtMain.Columns.Add("ReportInfoID");//报工单ID
                dtMain.Columns.Add("SubmitEmployeeID");//提交人ID
                dtMain.Columns.Add("SubmitDate");//提交时间
                dtMain.Columns.Add("SubmitNotes");//提交人备注
                dtMain.Columns.Add("DisposeEmployeeID");//处理人ID
                dtMain.Columns.Add("DisposeDate");//处理时间
                dtMain.Columns.Add("DisposeNotes");//处理人备注
                dtMain.Columns.Add("Qty");//数量
                dtMain.Columns.Add("UOMID");//计量单位ID
                dtMain.Columns.Add("Status");//状态
                dtMain.Columns.Add("Notes");//备注
                dtMain.Columns.Add("ContainerName");//生产批次号
                dtMain.Columns.Add("ProductName");//产品/图号
                dtMain.Columns.Add("Description");//名称
                dtMain.Columns.Add("ProcessNo");//工作令号
                dtMain.Columns.Add("OprNo");//作业令号
                //dtMain.Columns.Add("ConQty");//批次数量
                dtMain.Columns.Add("PlanStartDate");//计划开始时间
                dtMain.Columns.Add("PlanEndDate");//计划结束时间
                dtMain.Columns.Add("SpecDisName");//工序名称
                dtMain.Columns.Add("YearAndMonth");//年份以及月份
                dtMain.Columns.Add("SerialNumber");//流水码
                dtMain.Columns.Add("WorkflowID");//工艺路线ID
                dtMain.Columns.Add("IsSubmit");//是否提交不合格审理
                dtMain.Columns.Add("RevUnqualProId");//不合格品审理ID

                dtMain.Rows.Add();
                int intMainRow = dtMain.Rows.Count - 1;
                strID = Guid.NewGuid().ToString();
                dtMain.Rows[intMainRow]["ID"]= strID + "㊣String";
                dtMain.Rows[intMainRow]["QualityRecordInfoName"]=txtQualRecordID.Text + "㊣String";
                dtMain.Rows[intMainRow]["ContainerID"] = txtContainerID.Text + "㊣String";
                dtMain.Rows[intMainRow]["SpecID"] = txtSpecID.Text + "㊣String";
                dtMain.Rows[intMainRow]["ReportInfoID"] = txtReportInfoID.Text + "㊣String"; ;
                dtMain.Rows[intMainRow]["SubmitEmployeeID"] = userInfo["EmployeeID"] + "㊣String";
                dtMain.Rows[intMainRow]["SubmitDate"] = DateTime.Now + "㊣Date";
                dtMain.Rows[intMainRow]["SubmitNotes"] = txtNotes.Text + "㊣String";
                dtMain.Rows[intMainRow]["DisposeEmployeeID"] = "" + "㊣String";
                dtMain.Rows[intMainRow]["DisposeDate"] = "" + "㊣Date";
                dtMain.Rows[intMainRow]["DisposeNotes"] = "" + "㊣String";
                dtMain.Rows[intMainRow]["Qty"] = txtUnQualQty.Text + "㊣Integer";
                dtMain.Rows[intMainRow]["UOMID"] = txtUomID.Text + "㊣String";
                dtMain.Rows[intMainRow]["Status"] = "0" + "㊣Integer";
                dtMain.Rows[intMainRow]["Notes"] = "" + "㊣String";
                dtMain.Rows[intMainRow]["ContainerName"] = txtDispContainerName.Text + "㊣String";
                dtMain.Rows[intMainRow]["ProductName"] = txtDispProductName.Text + "㊣String";
                dtMain.Rows[intMainRow]["Description"] = txtDispProductName.Text + "㊣String";
                dtMain.Rows[intMainRow]["ProcessNo"] = txtDispProcessNo.Text + "㊣String";
                dtMain.Rows[intMainRow]["OprNo"] = txtDispOprNo.Text + "㊣String";
                dtMain.Rows[intMainRow]["PlanStartDate"] = txtStartTime.Text + "㊣Date";
                dtMain.Rows[intMainRow]["PlanEndDate"] = txtEndTime.Text + "㊣Date";
                dtMain.Rows[intMainRow]["SpecDisName"] = txtSpecDisName.Text + "㊣String";
                dtMain.Rows[intMainRow]["YearAndMonth"] = System.DateTime.Now.Year.ToString().Substring(0, 2) + DateTime.Now.Month.ToString("00") + "㊣String";
                dtMain.Rows[intMainRow]["SerialNumber"] = txtSerialNumber.Text + "㊣Integer";
                dtMain.Rows[intMainRow]["WorkflowID"] = txtWorkflowID.Text + "㊣String";
                dtMain.Rows[intMainRow]["IsSubmit"] = "0" + "㊣Integer";
                dtMain.Rows[intMainRow]["RevUnqualProId"] = "" + "㊣String";

                dtMain.Rows.Add();
                dtMain.Rows[dtMain.Rows.Count - 1]["QualityRecordInfoName"] = "" + "㊣String";
                ds.Tables.Add(dtMain);
                ds.Tables[ds.Tables.Count - 1].TableName = "QualityRecordInfo" + "㊣QualityRecordInfoName";
            }

            DataTable dtMine = new DataTable();
            
            dtMine.Columns.Add("ID");//唯一标识
            dtMine.Columns.Add("QualityRecordInfoID");//质量记录/不合格品审理ID
            dtMine.Columns.Add("ContainerID");//ContainerID
            dtMine.Columns.Add("ContainerName");//ContainerName
            dtMine.Columns.Add("ProductNo");//产品序号
            dtMine.Columns.Add("Qty");//数量
            //dtMine.Columns.Add("UOMID");//计量单位ID
            dtMine.Columns.Add("LossReasonID");//报废原因ID
            dtMine.Columns.Add("DisposeAdviceID");//处理意见ID
            dtMine.Columns.Add("DisposeNotes");//处理意见备注
            dtMine.Columns.Add("QualityNotes");//质量信息描述
            dtMine.Columns.Add("Notes");//备注
            if (wgProductNo.Rows.Count>0)
            {
                TemplatedColumn temCellSelect = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");
                for (int i = 0; i < temCellSelect.CellItems.Count; i++)
                {
                    Infragistics.WebUI.UltraWebGrid.CellItem cellItem = (Infragistics.WebUI.UltraWebGrid.CellItem)temCellSelect.CellItems[i];
                    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                    if (ckSelect.Checked == true)
                    {
                        string strContainerID = wgProductNo.Rows[i].Cells.FromKey("ContainerID").Value.ToString();
                        string strContainerName = wgProductNo.Rows[i].Cells.FromKey("ContainerName").Value.ToString();
                        string strProductNo = wgProductNo.Rows[i].Cells.FromKey("productno").Value.ToString();
                        dtMine.Rows.Add();
                        int intMineRow = dtMine.Rows.Count - 1;
                        dtMine.Rows[intMineRow]["ID"]= Guid.NewGuid().ToString() + "㊣String";
                        dtMine.Rows[intMineRow]["QualityRecordInfoID"] =strID +"㊣String";
                        dtMine.Rows[intMineRow]["ContainerID"] = strContainerID + "㊣String";
                        dtMine.Rows[intMineRow]["ContainerName"] = strContainerName + "㊣String";
                        dtMine.Rows[intMineRow]["ProductNo"] = strProductNo + "㊣String";
                        dtMine.Rows[intMineRow]["Qty"]=1 + "㊣Integer";
                        dtMine.Rows[intMineRow]["LossReasonID"] = "" + "㊣String";
                        dtMine.Rows[intMineRow]["DisposeAdviceID"] = "" + "㊣String";
                        dtMine.Rows[intMineRow]["DisposeNotes"] = "" + "㊣String";
                        dtMine.Rows[intMineRow]["QualityNotes"] =MyToleranceInput.GetCode() + "㊣String";
                        dtMine.Rows[intMineRow]["Notes"] = "" + "㊣String";

                    }

                }
            }
            else
            {
                dtMine.Rows.Add();
                int intMineRow = dtMine.Rows.Count - 1;
                dtMine.Rows[intMineRow]["ID"] = Guid.NewGuid().ToString() + "㊣String";
                dtMine.Rows[intMineRow]["QualityRecordInfoID"] = strID + "㊣String";
                dtMine.Rows[intMineRow]["ContainerID"] = "" + "㊣String";
                dtMine.Rows[intMineRow]["ContainerName"] = "" + "㊣String";
                dtMine.Rows[intMineRow]["ProductNo"] = "" + "㊣String";
                dtMine.Rows[intMineRow]["Qty"] = txtUnQualQty.Text + "㊣Integer";
                dtMine.Rows[intMineRow]["LossReasonID"] = "" + "㊣String";
                dtMine.Rows[intMineRow]["DisposeAdviceID"] = "" + "㊣String";
                dtMine.Rows[intMineRow]["DisposeNotes"] = "" + "㊣String";
                dtMine.Rows[intMineRow]["QualityNotes"] = MyToleranceInput.GetCode() + "㊣String";
                dtMine.Rows[intMineRow]["Notes"] = "" + "㊣String";
            }
            dtMine.Rows.Add();
            dtMine.Rows[dtMine.Rows.Count - 1]["QualityRecordInfoID"] = strID + "㊣String";

            ds.Tables.Add(dtMine);
            ds.Tables[ds.Tables.Count - 1].TableName = "QualityRecordProductNoInfo" + "㊣QualityRecordInfoID";

            //保存数据
            para.Add("dsData", ds);
            bool result = bll.SaveDataToDatabaseNew(para, out strMessage);
            if (result == true)
            {
                #region 记录日志
                ml.ContainerName = txtDispContainerName.Text; ml.ContainerID = txtContainerID.Text;
                ml.ParentID = strID; ml.ParentName = parentName;
                ml.CreateEmployeeID = userInfo["EmployeeID"];
                ml.BusinessName = businessName; 
                common.SaveMESAuditLog(ml);
                #endregion

                ClearControlS();
                txtSpecDisName.Text = txtISpecName.Text;
                txtWorkflowID.Text = txtIWorkflowID.Text;
                txtSpecID.Text = txtISpecID.Text;
                txtSSpecName.Text = txtISpecName.Text;
                GetQualityRecordInfoName();
                Dictionary<string, string> para1 = new Dictionary<string, string>();
                para1.Add("ContainerID", txtContainerID.Text);
                para1.Add("SpecName", txtSSpecName.Text);
                para1.Add("ReportInfoID", txtReportInfoID.Text);
                BindSaveQualInfo(para1);
                BindProductNo(txtReportInfoID.Text);
            }

            ShowStatusMessage(strMessage, result);
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }
    }

    /// <summary>
    /// 新增按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            ClearControlS();
            txtReportInfoID.Text = txtIReportInfoID.Text;
            txtSpecDisName.Text = txtISpecName.Text;
            txtWorkflowID.Text = txtIWorkflowID.Text;
            txtSpecID.Text = txtISpecID.Text;
            txtSSpecName.Text = txtISpecName.Text;
            GetQualityRecordInfoName();
        }
        catch (Exception Ex)
        {
            ShowStatusMessage(Ex.Message, false);
        }
    }

    /// <summary>
    /// 提交不合格品审理按钮事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUnQulDis_Click(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, object> popupData = new Dictionary<string, object>();
            popupData.Add("ProcessNo", txtDispProcessNo.Text);
            popupData.Add("OprNo", txtDispOprNo.Text);
            popupData.Add("ContainerName", txtDispContainerName.Text);
            popupData.Add("ProductName", txtDispProductName.Text);
            popupData.Add("Description", txtDispDescription.Text);
            popupData.Add("ContainerQty", txtConQty.Text);
            popupData.Add("PlannedStartDate", txtStartTime.Text);
            popupData.Add("PlannedCompletionDate", txtEndTime.Text);
            popupData.Add("ContainerID", txtContainerID.Text);
            popupData.Add("SpecID", txtSpecID.Text);
            popupData.Add("WorkflowID", txtWorkflowID.Text);
            popupData.Add("SpecName",txtSpecDisName.Text);

            DataTable dtQualityRecord = new DataTable();
            dtQualityRecord.Columns.Add("QRID");
            dtQualityRecord.Columns.Add("Qty");
            
            TemplatedColumn temCellSelect = (TemplatedColumn)ItemGrid.Columns.FromKey("ckSelect");
            for (int i = 0; i < ItemGrid.Rows.Count; i++)
            {
                CellItem cellItem = (CellItem)temCellSelect.CellItems[i];
                CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
                if (ckSelect.Checked == true)
                {
                    //判断质量记录是否已提交不合格品审理
                    string strIsSubmit = string.Empty;
                    if (ItemGrid.Rows[i].Cells.FromKey("IsSubmit").Value != null)
                    {
                        strIsSubmit = ItemGrid.Rows[i].Cells.FromKey("IsSubmit").Value.ToString();
                    }

                    if (strIsSubmit == "1")
                    {
                        ShowStatusMessage("选中的质量记录有已经提交不合格品审理的，请重新选择", false);
                        return;
                    }

                    DataRow row = dtQualityRecord.NewRow();
                    row["QRID"] = ItemGrid.Rows[i].Cells.FromKey("ID").Value.ToString();
                    row["Qty"] = ItemGrid.Rows[i].Cells.FromKey("qty").Value.ToString();
                    dtQualityRecord.Rows.Add(row); //获取质量描述
                    DataTable qualityDetails = containerBal.GetTableInfo("qualityrecordproductnoinfo", "qualityrecordinfoid", ItemGrid.Rows[i].Cells.FromKey("ID").Text);
                    if (qualityDetails.Rows.Count > 0)
                    {
                        popupData["QualityNotes"] = qualityDetails.Rows[0]["QualityNotes"].ToString();
                    }
                }
               
            }

            if (dtQualityRecord.Rows.Count != 1)
            {
                ShowStatusMessage("请选择一条质量记录", false);
                return;
            }

            popupData.Add("dtQualityRecord", dtQualityRecord);
                       

            ////产品序号
            //DataTable dtProductNo = new DataTable();
            //dtProductNo.Columns.Add("ContainerID");
            //dtProductNo.Columns.Add("ContainerName");
            //dtProductNo.Columns.Add("ProductNo");
            //dtProductNo.Columns.Add("Qty");
            //dtProductNo.Columns.Add("UOMID");
            //dtProductNo.Columns.Add("DisposeResult");
            //dtProductNo.Columns.Add("LossReasonID");
            //dtProductNo.Columns.Add("DisposeAdviceID");
            //dtProductNo.Columns.Add("DisposeNotes");
            //dtProductNo.Columns.Add("QualityNotes");
            //dtProductNo.Columns.Add("Notes");

            //TemplatedColumn temCell = (TemplatedColumn)wgProductNo.Columns.FromKey("ckSelect");
            //for (int i = 0; i < wgProductNo.Rows.Count; i++)
            //{
            //    CellItem cellItem = (CellItem)temCell.CellItems[i];
            //    CheckBox ckSelect = (CheckBox)cellItem.FindControl("ckSelect");
            //    if (ckSelect.Checked == true)
            //    {
            //        DataRow row = dtProductNo.NewRow();
            //        row["ContainerID"] = wgProductNo.Rows[i].Cells.FromKey("ContainerID").Value.ToString();
            //        row["ContainerName"] = wgProductNo.Rows[i].Cells.FromKey("ContainerName").Value.ToString();
            //        row["ProductNo"] = wgProductNo.Rows[i].Cells.FromKey("ProductNo").Value.ToString();
            //        row["Qty"] = "1";
            //        row["DisposeResult"] = "0";
            //        dtProductNo.Rows.Add(row);
            //    }
            //}

            //popupData.Add("dtProductNo", dtProductNo);

            Session["PopupData"] = popupData;

            ClientScript.RegisterStartupScript(GetType(), "", "<script>openrejectappsubmit()</script>");
        }
        catch (Exception ex)
        {
            ShowStatusMessage(ex.Message, false);
        }
    }


    /// <summary>
    /// 提示信息
    /// </summary>
    /// <param name="strMessage"></param>
    /// <param name="boolResult"></param>
    protected void ShowStatusMessage(string strMessage, Boolean boolResult)
    {
        lStatusMessage.Text = strMessage;

        if (boolResult == true)
        {
            lStatusMessage.ForeColor = Color.Black;
        }
        else
        {
            lStatusMessage.ForeColor = Color.Red;
        }
    }

    /// <summary>
    /// 验证是否是正整数
    /// </summary>
    /// <param name="inString"></param>
    /// <returns></returns>
    public static bool IsInt(string inString)
    {
        Regex regex = new Regex("^[0-9]*[1-9][0-9]*$");
        return regex.IsMatch(inString.Trim());
    }


    protected void btnClosePopup_Click(object sender, EventArgs e)
    {
        Dictionary<string, string> para = (Dictionary<string, string>)Session["MyData"];
        //绑定已保存的质量记录信息
        BindSaveQualInfo(para);
    }
}