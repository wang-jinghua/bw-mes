﻿//Description:审计日志类
//Copyright (c) : 通力凯顿（北京）系统集成有限公司
//Writer:Wangjh
//create Date:2020-4-16
//Rewriter:
//Rewrite Date:
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uMES.LeanManufacturing.ParameterDTO
{
    public class MESAuditLog
    {
        public MESAuditLog() {
            id = Guid.NewGuid().ToString();
            CreateDate = DateTime.Now;
        }
        public string  id { get; set; }

        public DateTime CreateDate { get; set; }

        public string CreateEmployeeID { get; set; }

        public string ParentID { get; set; }//操作表的ID

        public string ParentName { get; set; }//表名

        public string BusinessName { get; set; }

        public string Description { get; set; }

        public string Notes { get; set; }
        public int OperationType { get; set; }//操作类型 0：添加 1：修改 2：删除

        public string ContainerID { get; set; }

        public string ContainerName { get; set; }
    }
}
