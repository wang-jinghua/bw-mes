﻿//Description:批次数据模型
//Copyright (c) : 通力凯顿（北京）系统集成有限公司
//Writer:Wangjh
//create Date:2020-4-16
//Rewriter:
//Rewrite Date:
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uMES.LeanManufacturing.ParameterDTO
{
    public class ContainerStartModel
    {
        public string ContainerName { get; set; }

        public string Level { get; set; }
        
        public string StartEmployee { get; set; }

        public string StartReason { get; set; }

        public string Owner { get; set; }

        public string Mfgorder { get; set; }

        public string ProductName { get; set; }

        public string ProductRev { get; set; }

        public string WorkflowName { get; set; }

        public string WorkflowRev { get; set; }

        public string OriginalFactory { get; set; }

        public int Qty { get; set; }

        public string Uom { get; set; }

        public string Priority { get; set; }

        public string PlannedStartDate { get; set; }

        public string PlannedCompletionDate { get; set; }

        public string ContainerComment { get; set; }

        /// <summary>
        /// 子批次
        /// </summary>
        public DataTable ChildList { get; set; }

        /// <summary>
        /// 批次属性
        /// </summary>
        public DataTable AttributeList { get; set; }

    }
}
