﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace uMES.LeanManufacturing.ParameterDTO
{
    public class uMESPagingDataDTO
    {

        private string m_PageCount ;

        public string PageCount
        {
            get { return m_PageCount; }
            set { m_PageCount = value; }
        }

        private string m_RowCount;

        public string RowCount
        {
            get { return m_RowCount; }
            set { m_RowCount = value; }
        }

        private DataTable m_DBTable=new DataTable();

        public System.Data.DataTable DBTable
        {
            get { return m_DBTable; }
            set { m_DBTable = value; }
        }

        private DataSet m_DBset = new DataSet();

        public DataSet DBset
        {
            get { return m_DBset; }
            set { m_DBset = value; }
        }
    }
}
