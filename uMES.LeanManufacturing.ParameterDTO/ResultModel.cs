﻿//Description:结果模型类
//Copyright (c) : 通力凯顿（北京）系统集成有限公司
//Writer:Wangjh
//create Date:2020-4-16
//Rewriter:
//Rewrite Date:
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uMES.LeanManufacturing.ParameterDTO
{
    public class ResultModel
    {
        public ResultModel() {}
        public ResultModel(bool isSuccess,string message) {
            IsSuccess = isSuccess;
            Message = message;
        }
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }
    }
    

}
