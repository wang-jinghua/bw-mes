using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using System.Web;

namespace uMES.LeanManufacturing.Common
{
    public class WriteLog
    {
        private static  ILog myLog = log4net.LogManager.GetLogger("loginfo");// LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// 配置application对象启动的时候加载log4net配置
        /// </summary>
        public static void Config()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
        public static void Info(object message)
        {
            myLog.Info(message);
        }
        public static void Error(object message, Exception exception)
        {
            myLog.Error(message, exception);
        }
        public static void Debug(object message, Exception exception)
        {
            myLog.Debug(message, exception);
        }
        public static void Warn(object message, Exception exception)
        {
            myLog.Warn(message, exception);
        }

        public static void WriteLog2(string message) {
            var path = HttpRuntime.AppDomainAppPath;

            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path + "/log/ServiceLog.TXT", true))
            {
                sw.WriteLine("{0} Recive {1}", DateTime.Now.ToString("yyyy-MM-dd:HH:mm:ss"), message);
                sw.WriteLine();
            }
        }
     
    }
}
