﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Drawing;

/// <summary>
///clsDrawImage 的摘要说明
////****************************************************** 
//FileName:文件名称
//Description: 该类内容为绘制图形
//Copyright (c) : 通力凯顿（北京）系统集成有限公司
//Writer:陆迪
//create Date:2012-12-28 16:48
//******************************************************/

/// 
/// </summary>
/// 
namespace uMESExternalControl.ToleranceInputLib
{
    public class clsDrawImage
    {
        public clsDrawImage()
        {

        }

        //绘制上下标图形
        public Bitmap DrawTolerance(String strMid, String strUp, String strDown)
        {
            Font mFont = new Font("宋体", 12);   //绘制文本字体
            Font mFont2 = new Font("宋体", 8);   //绘制上下标文本字体

            Graphics g = Graphics.FromImage(new Bitmap(1, 1));

            int intLength, intMidLength, intUpLength, intDownLength, intMaxLength, intStart;
            float fXPoint, fYPoint;

            fXPoint = 0;
            fYPoint = 0;
            intMidLength = Convert.ToInt32(g.MeasureString(strMid, mFont).Width);
            intUpLength = Convert.ToInt32(g.MeasureString(strUp, mFont2).Width);
            intDownLength = Convert.ToInt32(g.MeasureString(strDown, mFont2).Width);

            if (intUpLength >= intDownLength)
            {
                intMaxLength = intUpLength;
            }
            else
            {
                intMaxLength = intDownLength;
            }
            intLength = intMidLength + intMaxLength;
            intStart = intMidLength;

            //创建位图并获取绘图设备
            Bitmap objBmp = new Bitmap(intLength, 18);
            Graphics objGraph = Graphics.FromImage(objBmp);

            //背景默认为黑色，需要用白色替换一下
            objGraph.Clear(Color.White);

            //创建绘图用的画笔和画刷
            Pen objPen = new Pen(Color.Black);
            Brush FillBrush = new SolidBrush(Color.SandyBrown); //填充色
            Brush TextBrush = Brushes.Black;   //文本颜色

            objGraph.DrawString(strMid, mFont, TextBrush, fXPoint, fYPoint);

            if (strUp != "")
            {
                objGraph.DrawString(strUp, mFont2, TextBrush, fXPoint + intStart, fYPoint);
            }
            if (strDown != "")
            {
                objGraph.DrawString(strDown, mFont2, TextBrush, fXPoint + intStart, fYPoint + 8);
            }

            ////保存
            return objBmp;
        }

        //绘制粗糙度图形
        public Bitmap DrawRoughness(String strLeft, String strRight)
        {
            int intFontSize, intFontSize2;
            intFontSize = 9;
            intFontSize2 = 5;
            Font mFont = new Font("宋体", 8);   //绘制文本字体
            Font mFont2 = new Font("宋体", 10);   //绘制右侧上下文本字体

            int intLength, intLeftLength, intRightLength;
            float fXPoint, fYPoint;

            fXPoint = 0;
            fYPoint = 0;

            intLeftLength = strLeft.Length;
            //strRight = "<" + strUp + "," + strDown + ">";
            intRightLength = strRight.Length;
            Point point1 = new Point(16, 10);
            Point point2 = new Point(0, 10);
            Point point3 = new Point(8, 20);
            Point point4 = new Point(24, 0);
            Point point5 = new Point(31, 0);


            if (strLeft != "")
            {
                if (strRight != "")
                {
                    //指定加工方法时
                    intLength = 3 * intFontSize2 + (intRightLength) * intFontSize;

                    //创建位图并获取绘图设备
                    Bitmap objBmp = new Bitmap(intLength, 22);
                    Graphics objGraph = Graphics.FromImage(objBmp);

                    //背景默认为黑色，需要用白色替换一下
                    objGraph.Clear(Color.White);

                    //创建绘图用的画笔和画刷
                    Pen objPen = new Pen(Color.Black);
                    Brush FillBrush = new SolidBrush(Color.SandyBrown); //填充色
                    Brush TextBrush = Brushes.Black;   //文本颜色

                    objGraph.DrawString(strLeft, mFont, TextBrush, fXPoint, fYPoint);
                    Point[] PointLine = { point1, point2, point3, point4, point5 };
                    objGraph.DrawLines(objPen, PointLine);
                    objGraph.DrawString(strRight, mFont2, TextBrush, 13, 10);

                    return objBmp;


                }
                else
                {
                    //去除材料时
                    intLength = 25;

                    //创建位图并获取绘图设备
                    Bitmap objBmp = new Bitmap(intLength, 22);
                    Graphics objGraph = Graphics.FromImage(objBmp);

                    //背景默认为黑色，需要用白色替换一下
                    objGraph.Clear(Color.White);

                    //创建绘图用的画笔和画刷
                    Pen objPen = new Pen(Color.Black);
                    Brush FillBrush = new SolidBrush(Color.SandyBrown); //填充色
                    Brush TextBrush = Brushes.Black;   //文本颜色

                    objGraph.DrawString(strLeft, mFont, TextBrush, fXPoint, fYPoint);
                    Point[] PointLine = { point1, point2, point3, point4 };
                    objGraph.DrawLines(objPen, PointLine);

                    return objBmp;
                }
            }
            else
            {
                //不去除材料时
                intLength = 25;

                //创建位图并获取绘图设备
                Bitmap objBmp = new Bitmap(intLength, 22);
                Graphics objGraph = Graphics.FromImage(objBmp);

                //背景默认为黑色，需要用白色替换一下
                objGraph.Clear(Color.White);

                //创建绘图用的画笔和画刷
                Pen objPen = new Pen(Color.Black);
                Brush FillBrush = new SolidBrush(Color.SandyBrown); //填充色
                Brush TextBrush = Brushes.Black;   //文本颜色

                //objGraph.DrawString(strLeft, mFont, TextBrush, fXPoint, fYPoint);
                objGraph.DrawEllipse(objPen, 4, 10, 8, 8);

                Point[] PointLine = { point2, point3, point4 };
                objGraph.DrawLines(objPen, PointLine);

                return objBmp;
            }

        }

        //绘制形位公差图形
        public Bitmap DrawShapeTolerance(String strHtml, String strHtmlStripped, String strFileDoc)
        {
            clsParseCode oParse = new clsParseCode();
            int intFontSize, intFontSize2;
            String strHtmlTemp, strDrawTemp, strCodeTemp, strFileNameTemp, strFilePathTemp;
            intFontSize = 10;
            intFontSize2 = 5;
            Font mFont = new Font("宋体", 12);   //绘制文本字体
            //strFileDoc = strFileDoc.Replace("\\", @"\");

            int intLength, intMaxLength, intCurX, intCount, intStartFlag, intEndFlag, intImageWidth, intImageHeigh, intStart;
            float fXPoint, fYPoint;
            System.Drawing.Image imgTemp;
            Bitmap objBmpTemp;
            Point pointStart, pointEnd;

            fXPoint = 0;
            fYPoint = 10;
            strHtmlTemp = strHtml;
            strHtmlTemp = strHtmlTemp.Replace("<&70><+>", "");
            strHtmlTemp = strHtmlTemp.Replace("<+><&90>", "");
            intLength = 6;

            //算出分隔符的个数
            intCount = (strHtmlTemp.Length - strHtmlTemp.Replace("<+>", "").Length) / 3;
            intLength += intCount * 3;

            //取得特殊符号的个数
            strHtmlTemp = strHtmlTemp.Replace("<+>", "");
            intCount = 0;
            intCount = strHtmlTemp.Length - strHtmlTemp.Replace("<", "").Length;
            intLength += intCount * 20;

            //获取大图标的个数
            intCount = (strHtmlTemp.Length - strHtmlTemp.Replace("<&15>", "").Length) / 5;
            intLength += intCount * 7;
            intCount = (strHtmlTemp.Length - strHtmlTemp.Replace("<J>", "").Length) / 3;
            intLength += intCount * 7;
            intCount = (strHtmlTemp.Length - strHtmlTemp.Replace("<SO>", "").Length) / 4;
            intLength += intCount * 4;

            //获取普通文字的长度

            System.Text.ASCIIEncoding n = new System.Text.ASCIIEncoding();
            byte[] bytTemp = n.GetBytes(strHtmlStripped);
            int intLenthTemp = 0;  // l 为字符串之实际长度
            for (int i = 0; i <= bytTemp.Length - 1; i++)
            {
                if (bytTemp[i] == 63)  //判断是否为汉字或全脚符号
                {
                    intLenthTemp++;
                }
                intLenthTemp++;
            }

            intLength += intLenthTemp * (intFontSize + 1);
            //intLength += strHtmlStripped.Length * (intFontSize) + 1;

            intStart = strHtml.IndexOf("<&70>");
            if (intStart > -1)
            {

                Point ptStart = new Point(0, 10);

                //创建位图并获取绘图设备
                Bitmap objBmp = new Bitmap(intLength, 22);
                Graphics objGraph = Graphics.FromImage(objBmp);

                //背景默认为黑色，需要用白色替换一下
                objGraph.Clear(Color.White);

                //创建绘图用的画笔和画刷
                Pen objPen = new Pen(Color.Black);
                Brush FillBrush = new SolidBrush(Color.SandyBrown); //填充色
                Brush TextBrush = Brushes.Black;   //文本颜色

                //先画开头的线
                //pointStart= new Point(0, 10);
                //pointEnd= new Point(4, 10);

                //objGraph.DrawLine(objPen, pointStart, pointEnd);

                pointStart = new Point(0, 1);
                pointEnd = new Point(0, 20);
                objGraph.DrawLine(objPen, pointStart, pointEnd);

                intCurX = 1;
                strHtmlTemp = strHtml;
                strHtmlTemp = strHtmlTemp.Replace("<&70><+>", "");
                strHtmlTemp = strHtmlTemp.Replace("<+><&90>", "");
                intMaxLength = strHtmlTemp.Length;

                objGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                objGraph.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;


                while (intMaxLength > 0)
                {
                    intStartFlag = strHtmlTemp.IndexOf("<");
                    if (intStartFlag == -1)
                    {
                        strDrawTemp = strHtmlTemp;
                        objGraph.DrawString(strDrawTemp, mFont, TextBrush, intCurX + 1, 0);

                        bytTemp = n.GetBytes(strDrawTemp);
                        intLenthTemp = 0;
                        for (int i = 0; i <= bytTemp.Length - 1; i++)
                        {
                            if (bytTemp[i] == 63)
                            {
                                intLenthTemp++;
                            }
                            intLenthTemp++;
                        }
                        intCurX += intLenthTemp * (intFontSize - 2) + 6;
                        //intCurX += strDrawTemp.Length * (intFontSize -2) + 6;
                        strHtmlTemp = "";
                        intMaxLength = strHtmlTemp.Length;
                    }
                    else
                    {
                        if (intStartFlag > 0)
                        {
                            strDrawTemp = strHtmlTemp.Substring(0, intStartFlag);
                            objGraph.DrawString(strDrawTemp, mFont, TextBrush, intCurX + 1, 0);

                            bytTemp = n.GetBytes(strDrawTemp);
                            intLenthTemp = 0;
                            for (int i = 0; i <= bytTemp.Length - 1; i++)
                            {
                                if (bytTemp[i] == 63)
                                {
                                    intLenthTemp++;
                                }
                                intLenthTemp++;
                            }
                            intCurX += intLenthTemp * (intFontSize - 2) + 6;

                            //intCurX += strDrawTemp.Length * (intFontSize -2) + 6;
                            strHtmlTemp = strHtmlTemp.Substring(intStartFlag);
                            intMaxLength = strHtmlTemp.Length;

                        }
                        else
                        {
                            intEndFlag = strHtmlTemp.IndexOf(">");
                            strCodeTemp = strHtmlTemp.Substring(0, intEndFlag + 1);
                            strHtmlTemp = strHtmlTemp.Substring(intEndFlag + 1);
                            intMaxLength = strHtmlTemp.Length;

                            if (strCodeTemp == "<+>")
                            {

                                pointStart = new Point(intCurX + 1, 1);
                                pointEnd = new Point(intCurX + 1, 20);
                                objGraph.DrawLine(objPen, pointStart, pointEnd);
                                intCurX += 1;
                            }
                            else
                            {
                                strFileNameTemp = oParse.ParseImageFile(strCodeTemp);

                                if (strFileNameTemp != "")
                                {
                                    strFilePathTemp = strFileDoc + strFileNameTemp;
                                    objBmpTemp = new Bitmap(strFilePathTemp);
                                    intImageWidth = objBmpTemp.Size.Width;
                                    imgTemp = objBmpTemp;
                                    if (strFileNameTemp == "quantiaodong.png" || strFileNameTemp == "J.png")
                                    {
                                        objGraph.DrawImage(imgTemp, intCurX + 1, 1, 25, 19);
                                    }
                                    else
                                    {
                                        objGraph.DrawImage(imgTemp, intCurX + 1, 1, 19, 19);
                                    }

                                    intCurX += intImageWidth + 1;

                                }
                            }

                        }
                    }
                }
                pointStart = new Point(intCurX + 2, 0);
                pointEnd = new Point(intCurX + 2, 20);
                objGraph.DrawLine(objPen, pointStart, pointEnd);

                pointStart = new Point(0, 1);
                pointEnd = new Point(intCurX + 2, 1);
                objGraph.DrawLine(objPen, pointStart, pointEnd);

                pointStart = new Point(0, 21);
                pointEnd = new Point(intCurX + 2, 21);
                objGraph.DrawLine(objPen, pointStart, pointEnd);

                // objGraph.DrawString(strMid, mFont, TextBrush, fXPoint, fYPoint);
                // objBmpTemp = new Bitmap(strFileDoc + @"\weizhidu.png");

                // imgTest = objBmpTemp;

                //objGraph.DrawImage(imgTest, 0, 0);


                return objBmp;
            }
            else
            {
                return null;
            }


        }

        //绘制普通文本图形
        public Bitmap DrawString(String strInput)
        {
            int intFontSize;
            intFontSize = 10;

            Font mFont = new Font("宋体", 10);   //绘制文本字体

            int intLength, intMidLength, intMaxLength, intStart;
            float fXPoint, fYPoint;

            fXPoint = 0;
            fYPoint = 0;
            strInput = strInput.Replace("&nbsp", " ");
            intMidLength = System.Text.Encoding.Default.GetBytes(strInput).Length; //strInput.Length;

            intLength = intMidLength * intFontSize;

            intStart = (intMidLength) * (intFontSize - 1) + 12;
            intLength = intLength + 5;



            //创建位图并获取绘图设备
            Bitmap objBmp = new Bitmap(intLength, 18);
            Graphics objGraph = Graphics.FromImage(objBmp);

            //背景默认为黑色，需要用白色替换一下
            objGraph.Clear(Color.White);

            //创建绘图用的画笔和画刷
            Pen objPen = new Pen(Color.Black);
            Brush FillBrush = new SolidBrush(Color.SandyBrown); //填充色
            Brush TextBrush = Brushes.Black;   //文本颜色

            objGraph.DrawString(strInput, mFont, TextBrush, fXPoint, fYPoint);

            ////保存
            ////objBmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
            //String strFileDoc, strFilePath, strFileName;
            //strFileDoc = @"C:\Ludi\VS2008\ToleranceTest\ToleranceTest\Images\";
            ////intImageIndex = (int)Session["intMaxImageIndex"] + 1;
            ////Session["intMaxImageIndex"] = intImageIndex;
            //strFileName = "ImageTem-" + intMaxImageIndex + ".png";
            //strFilePath = strFileDoc + strFileName;
            //objBmp.Save(strFilePath);
            //return @"<img src='../Images/" + strFileName + "'>";
            return objBmp;
        }

        //在原有图形上增加新图形
        public Bitmap DrawImageAdd(System.Drawing.Image ImgOld, System.Drawing.Image ImgAdd)
        {
            int intFontSize;
            intFontSize = 9;

            Font mFont = new Font("宋体", 12);   //绘制文本字体

            int intLength, intMidLength, intMaxLength, intStart;
            float fXPoint, fYPoint;

            fXPoint = 0;
            fYPoint = 0;

            int intImgOldWidth, intImgAddWidth;
            intImgOldWidth = ImgOld.Width;
            intImgAddWidth = ImgAdd.Width;
            intLength = intImgOldWidth + intImgAddWidth + 1;
            //创建位图并获取绘图设备
            Bitmap objBmp = new Bitmap(intLength, 22);
            Graphics objGraph = Graphics.FromImage(objBmp);

            //背景默认为黑色，需要用白色替换一下
            objGraph.Clear(Color.White);

            //创建绘图用的画笔和画刷
            Pen objPen = new Pen(Color.Black);
            Brush FillBrush = new SolidBrush(Color.SandyBrown); //填充色
            Brush TextBrush = Brushes.Black;   //文本颜色

            objGraph.DrawImage(ImgOld, 1, 1, intImgOldWidth, 19);
            objGraph.DrawImage(ImgAdd, intImgOldWidth + 1, 1, intImgAddWidth, 19);

            //objGraph.DrawString(strInput, mFont, TextBrush, fXPoint, fYPoint);

            ////保存
            ////objBmp.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
            //String strFileDoc, strFilePath, strFileName;
            //strFileDoc = @"C:\Ludi\VS2008\ToleranceTest\ToleranceTest\Images\";
            ////intImageIndex = (int)Session["intMaxImageIndex"] + 1;
            ////Session["intMaxImageIndex"] = intImageIndex;
            //strFileName = "ImageTem-" + intMaxImageIndex + ".png";
            //strFilePath = strFileDoc + strFileName;
            //objBmp.Save(strFilePath);
            //return @"<img src='../Images/" + strFileName + "'>";
            return objBmp;
        }


    }
}